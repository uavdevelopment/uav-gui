﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET;
using System.IO;
using System.Windows.Forms;
using MasterClasses;
using UAV_GUI.Utilities;
using GLobal;
using FileClasses.INI_Files;
using GUtilities;
namespace UAV_GUI.FileClasses.FlyFiles
{

    public struct failurePatterns
    {
        public string[] failurePattern ;
        public int patternNum;
        public failurePatterns(int X)
        {
            failurePattern = new string[7];
            patternNum = X;
        }
    }
    class FlyFileParser
    {
        public bool save = true;
        public bool load = false;
        List<WayPoint> wayPointList;  
        public  List<Command> commandsList_toValidate;
        string erorrMsg = "";
        public string unitConvesion = "";
        SaveFileDialog saveFileDialog1;
        string WrongCommandArgs = "";
        bool navigationCommandNotInThread0 = false;
        public  bool validFile = true;
        public string Unit_ConversionsCombo = "";
        // Home Location :
        public  double orlong =INIData.longitude;  // default value
        public double orlat = INIData.latitude;  // default value .
        public double oralt; 
        public List<Command> FailerPatterns = new List<Command>(); // for parssing
        public string failuerPatterns = "";
        public bool safeLlocationSetted = false;
        string UnitConversion_Calculation = "";
        String sourceFile;
       // StreamWriter objWritter;
        System.IO.StreamWriter objWritter;
        public failurePatterns FP = new failurePatterns(-1);

        public string[] filePatterns = new string[16];

        public string[] saved_fName;
        public FlyFileParser() {
             wayPointList = new List<WayPoint>();
             
              }

        public  List<double> CommandArg = new List<double>();

        public bool validateFlyFile(){ //String fileName) {
            int Patterncount = 0;
            int threadCount = 1;
            bool fixed_ = false;
            int thread = 0;
            List<double> pattern_num_ofStartPattern = new List<double>();
            List<double> pattern_num_ofDefinPattern = new List<double>();
          //  List<double> numOfThread = new List<double>();
            int noOf_return_repeat_forPatterns = 0;
          
            // file without Unit Conversions
            //if (commandsList_toValidate[0].name != CommandName.metric || commandsList_toValidate[0].name != CommandName.imperial || commandsList_toValidate[0].name != CommandName.defaultUnits)
            //    return false;
            // file without tackoff command
            if (commandsList_toValidate[1].name != CommandName.Takeoff)
            {
                erorrMsg = "file without tackoff command";
                return false;
            }
            //for (int i = 0; i < commandsList_toValidate.Count; i++)
            //{
            //    if (commandsList_toValidate[i].name == CommandName.thread)
            //        thread++;
            //}
            for (int i = 0; i <commandsList_toValidate.Count; i++)
            
            {                       
                if (commandsList_toValidate[i].name == CommandName.startpattern)
                    pattern_num_ofStartPattern.Add(commandsList_toValidate[i].CommandArgs[0]);
                if (commandsList_toValidate[i].name == CommandName.definePattern)
                {
                    // definePattern befor fixed
                    if (fixed_ == false)
                    {
                        erorrMsg = "definePattern befor fixed";
                        return false;
                    }
                    
                    pattern_num_ofDefinPattern.Add(commandsList_toValidate[i].CommandArgs[0]);
                  //  order of definPattern
                    if (commandsList_toValidate[i].CommandArgs[0] != Patterncount)
                    {
                        erorrMsg = "wrong order of definPattern";
                        return false;
                    }
                    Patterncount++;
                    noOf_return_repeat_forPatterns++;
                }
                if ((commandsList_toValidate[i].name == CommandName.repeat || commandsList_toValidate[i].name == CommandName.return_) && fixed_ && thread !=1)
                {
                    
                    noOf_return_repeat_forPatterns--;
                }
                if (commandsList_toValidate[i].name == CommandName.fixed_)
                    fixed_ = true;
                if (commandsList_toValidate[i].name == CommandName.thread)
                {
                    thread = 1;
                    // thread befor fixed
                    if (fixed_ == false)
                    {
                        erorrMsg = "thread befor fixed";
                        return false;
                    }
                    // thread ordring
                    if (commandsList_toValidate[i].CommandArgs[0] != threadCount)
                    {
                        erorrMsg = "wrong thread ordring";
                        return false;
                    }
                    threadCount++;
                   // numOfThread.Add(commandsList_toValidate[i].CommandArgs[0]);
                 //   noOf_return_repeat_forPatterns++;
                }
            }

            // more than 16 pattern 
            if (pattern_num_ofDefinPattern.Count > 16)
            {
                erorrMsg = "more than 16 pattern";
                return false;
            }
     
            //  startpattern dosen't match with any definePattern
            if (countPatterns(pattern_num_ofStartPattern,pattern_num_ofDefinPattern) == false)
            {
                erorrMsg = "startpattern dosen't match with any definePattern";
                return false;
            }
            // definePattern without return or repeat
            // 3mltlha comment 3shan momken yeb2a fe repeat command gwa el thread aw el definPattern :/
            //if (noOf_return_repeat_forPatterns != 0)
            //{
            //    erorrMsg = " not found return or repeat";
            //    return false;
            //}
            // no of args validatin 
            if (!noOfArgsValidation())  // ta2kod en kol el commands bet3mel keda  for (int c = 0; c < p.Count; c++)                                                                                  //  CommandArg.Add(p[c]);
            {
                erorrMsg = "you have command with wrong no of args ,  "+ WrongCommandArgs;

                return false;
            }
            if (navigationCommandNotInThread0)
            {
                erorrMsg = "navigation command not in thread 1";
                return false;
            }

            return true; 
        }


        private bool noOfArgsValidation()
        {
            Command cmd = new Command();
            for (int i = 0; i < commandsList_toValidate.Count; i++)
            {
                for (int j = 0; j < cmd.CommandsArgsValidationList.Count; j++)
                {
                    if (commandsList_toValidate[i].name == cmd.CommandsArgsValidationList[j].name)
                    {
                        if (commandsList_toValidate[i].CommandArgs.Count > cmd.CommandsArgsValidationList[j].maxArgs
                            || commandsList_toValidate[i].CommandArgs.Count < cmd.CommandsArgsValidationList[j].minArgs)
                        {
                            WrongCommandArgs = commandsList_toValidate[i].name.ToString() + " has  " + commandsList_toValidate[i].CommandArgs.Count.ToString() + " args";
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public bool countPatterns(List<double> StartPattern , List<double> DefinPattern )
        {
            bool order = false;
            for (int i = 0; i < StartPattern.Count; i++)
            {
                for (int j= 0; j < DefinPattern.Count; j++)
                {
                    if (StartPattern[i] == DefinPattern[j])
                        order = true;
                }
                if (order == false)
                    return false;
                order = false;
            }
            return true;         
        }

        public Pattern[] getAllPattern(String fileName) {
            return new Pattern[1];
        }

        #region Assistant fns
       
        /// <summary>
        /// 
        /// </summary>
        public void setPattern_WPToMap()
        {

            for (int u = 0; u < wayPointList.Count; u++)
            {
               int  PatternNum =-1;
               // for (int m = 0; m < wayPointList[u].patternList.Count; m++)
                for (int d= 0; d < wayPointList[u].commandsList.Count; d++)
                {
                    if (wayPointList[u].commandsList[d].name == CommandName.startpattern)
                    {
                        PatternNum++;
                        WayPoint PatternWP;
                        // if the first command  in the pattern add new wp so we draw this WP 
                        // (in code its 2nd 'cause the first command is definePattern)
                        
                        if (wayPointList[u].patternList[PatternNum].patternCommands[1].name == CommandName.flyTo)
                        {
                            PatternWP = new WayPoint(wayPointList[u].patternList[PatternNum].patternCommands[1].CommandArgs[1],
                                                      wayPointList[u].patternList[PatternNum].patternCommands[1].CommandArgs[0]);
                        }
                        else if (wayPointList[u].patternList[PatternNum].patternCommands[1].name == CommandName.fromTo)
                        {
                            PatternWP = new WayPoint(wayPointList[u].longt,wayPointList[u].lat);
                        }
                        // else it will assosiate with the startpattern
                        else
                        {
                            PatternWP = new WayPoint(wayPointList[u].longt, wayPointList[u].lat);
                        }
                        wayPointList[u].patternList[PatternNum].WP_On_Map = PatternWP;
                       // drawPatterWP_OnMap(PatternWP, PatternNum);
                    }
                }
            }
        }

        /// <summary>
        /// /
        /// </summary>
        /// <param name="patternNum"></param>
        /// <param name="cmd"></param>
        public void addToPatternList(int patternNum, Command cmd)
        {
          bool  breakFlag = false;
     
            for (int c = 0; c < wayPointList.Count; c++)
            {
                
                    for (int d = 0; d < wayPointList[c].patternList.Count; d++)
                        if (wayPointList[c].patternList[d].pattern_num == patternNum)
                        {    
                                wayPointList[c].patternList[d].patternCommands.Add(cmd);
                                breakFlag = true;
                                break;                      
                        }
                
                if (breakFlag)
                    break;
            }
            if (breakFlag == false)  // means that pattren is not used " there is no startpattern command"
            {
                #region need to handel
                //create new pattren
                Pattern patt;
                for (int c = 0; c < wayPointList.Count; c++)
                {
                    for (int d = 0; d < wayPointList[c].commandsList.Count; d++)
                    {
                        if (wayPointList[c].commandsList[d].name == CommandName.definePattern)
                        {
                            patt = new Pattern(patternNum, c);
                            wayPointList[c].patternList.Add(patt);
                            wayPointList[c].patternList[0].patternCommands.Add(cmd);
                            breakFlag = true;

                        }
                    }
                }
            }
            if (breakFlag == false)
            {
                wayPointList.Add(new WayPoint());
                wayPointList[wayPointList.Count-1].patternList.Add(new Pattern(patternNum,wayPointList.Count-1));
                addToPatternList(patternNum,cmd) ;
            }
                #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public String removeComments(String str)
        {
            var blockComments = @"/\*(.*?)\*/";
            var lineComments = @"//(.*?)\r?\n";
            var strings = @"""((\\[^\n]|[^""\n])*)""";
            var verbatimStrings = @"@(""[^""]*"")+";
            String s = Regex.Replace(str,
                 blockComments + "|" + lineComments + "|" + strings + "|" + verbatimStrings,
                     me =>
                     {
                         if (me.Value.StartsWith("/*") || me.Value.StartsWith("//"))
                             return me.Value.StartsWith("//") ? Environment.NewLine : "";
                         // Keep the literal strings
                         return me.Value;
                     },
                RegexOptions.Singleline);
            return Regex.Replace(s, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
        }

       

     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>

        private double  Wp_Calculations_On_UnitConversion()
        {
            if (unitConvesion != "")
                UnitConversion_Calculation = unitConvesion;
            else
                UnitConversion_Calculation = Unit_ConversionsCombo;

            // Tariq Write Your Code Here :) 
            // And Your unitConversion in this var UnitConversion_Calculation

            double UC= 0.0;
            // And You can call this func and use UC in  :
            // getlatitudeRelative & getLongitudeRelative
            // getLatitudeAbs & getLongitudeAbs
            return UC;
        }

        public double getlatitudeRelative(int x)
        {
            // orlat = 39.4666666597666;
            double temp = 71.38 * 3280.84;
            return ((x / 1.0 / 364829.408) + orlat);
        }
     

        public double getLongitudeRelative(int y )//, string UnitConv)
        {
           // double orlong = -76.1681666929581;
            double temp = 285.5 * 3280.84;
            return ((y / 1.0 / 234186.3592) + orlong); 
                 
        }

        public double getLatitudeAbs(double x)
        {
         //   double orlat = 39.4666666597666;
            return ((x - orlat) * 364829.408 );
        }
        public double getLongitudeAbs(double y)
        {
       //     double orlong = -76.1681666929581;
            return ((y - orlong) * 234186.3592 );
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public WayPoint getWpFromRel(int lat, int longt)
        {
            //return new WayPoint(getLongitudeRelative(longt), getlatitudeRelative(lat));
            WayPoint rWp = new WayPoint(AutoPilotUnits.toLongitudeAbsoluteOR(orlong, orlat, longt),
                AutoPilotUnits.toLatitudeAbsoluteOR(orlong,orlat, lat));
           return rWp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        /// 
        public void addCommandToPreviosWP(Command cmd)
        {
            if (wayPointList.Count == 0) 
            {
                //WayPoint ss =  new WayPoint(39.4666666597666, -76.1681666929581);
                //ss.commandsList.Add(cmd);
                //    wayPointList.Add(ss);
            }

            else if (wayPointList.Count > 1)
            {
                wayPointList[wayPointList.Count - 1].commandsList.Add(cmd);

            }
            else
            {
                wayPointList[0].commandsList.Add(cmd);

            }
        }

        void appendToThreadCommand(Command cmd)
        {
            wayPointList[wayPointList.Count - 1].commandsList.Add(cmd);
        }

        public List<int> getNumFromString(String str)
        {
            List<int> s = new List<int>();
          

                str = str.Trim();
                Match m = Regex.Match(str, @"[+-]?\d+");
                while (m.Success)
                {
                    int number = Convert.ToInt32(m.Value);
                    s.Add(number);
                    GlobalData.LogConsole("1.txt"+ number + "\n");
                    m = m.NextMatch();
                }
                return s;
          
        }

        public double ConvertDegreeAngleToDouble(string point)
        {
            //Example: 17.21.18S

            int multiplier = (point.Contains("s") || point.Contains("w")) ? -1 : 1; //handle south and west

            point = Regex.Replace(point, "[^0-9.]", ""); //remove the characters

            String[] pointArray = point.Split('.'); //split the string.

            //Decimal degrees = 
            //   whole number of degrees, 
            //   plus minutes divided by 60, 
            //   plus seconds divided by 3600
            double degrees = 0;
            double minutes = 0;
            double seconds = 0;
            for (int i = 0; i < pointArray.Length; i++)
            {
                if (i == 0)
                    degrees = Double.Parse(pointArray[0]);
                if (i == 1)
                    minutes = Double.Parse(pointArray[1]) / 60;
                if (i == 2)
                    seconds = Double.Parse(pointArray[2]) / 3600;
            }

            return (degrees + minutes + seconds) * multiplier;
        }

        public double ConvertDegreeAngleToDouble_2(string point)
        {
            //Example: 26:7.3986W

            int multiplier = (point.Contains("s") || point.Contains("w")) ? -1 : 1; //handle south and west

           // point = Regex.Replace(point, "[^0-9.]", ""); //remove the characters
            point = point.Substring(0, point.Length - 2);
            Regex rgx = new Regex("[^a-zA-Z0-9.:]");
            point = rgx.Replace(point, "");

            String[] pointArray = point.Split(':'); //split the string.

            //Decimal degrees = 
            //   whole number of degrees, 
            //   plus minutes divided by 60, 
            //   plus seconds divided by 3600
            double degrees = 0;
            double minutes = 0;
            double seconds = 0;
            for (int i = 0; i < pointArray.Length; i++)
            {
                if (i == 0 && pointArray[0] != "")
                    degrees = Double.Parse(pointArray[0]);
                if (i == 1 && pointArray[1]!="")
                    minutes = Double.Parse(pointArray[1]) / 60;
                if (i == 2 && pointArray[2] != "")
                    seconds = Double.Parse(pointArray[2]) / 3600;
            }

            return (degrees + minutes + seconds) * multiplier;
        }

        private List<double> parsAbs_1(string str)
        {
            List<double> p = new List<double>();
            Match m = Regex.Match(str, @"\(.*\)");
            str = m.ToString();
           
            str = str.Substring(1, str.Length - 2);
            //   str.Remove(1);
            //   str.Remove(str.Length-1, 1);
            String[] points = str.Split(',');
            p.Add(ConvertDegreeAngleToDouble(points[0]));
            p.Add(ConvertDegreeAngleToDouble(points[1]));
            return p;
        }

        private List<double> parsAbs_2(string str)
        {
            List<double> p = new List<double>();
            Match m = Regex.Match(str, @"\(.*\)");
            str = m.ToString();
            str = str.Substring(1, str.Length - 2);
            //   str.Remove(1);
            //   str.Remove(str.Length-1, 1);
            String[] points = str.Split(',');

            p.Add(ConvertDegreeAngleToDouble_2(points[0]));
            p.Add(ConvertDegreeAngleToDouble_2(points[1]));
            return p;
        }

        private List<double> ParssingAnyTypeOfPoints(string str)
        {
            List<double> points = new List<double>();
            int a;
            if (str.Contains(':'))  // 26:7.3986W
            {
                points = parsAbs_2(str);
                points[0] = getLongitudeAbs(points[0]);
                a = (int)points[0];
                points[0] = a;
                points[1] = getLatitudeAbs(points[1]);
                a = (int)points[1];
                points[1] = a;
            }
            else if ((str.Contains('n') && str.Contains('e'))   // 26.5W
            || (str.Contains('n') && str.Contains('w'))
            || (str.Contains('s') && str.Contains('e'))
            || (str.Contains('s') || str.Contains('w')))  
            {

                points = parsAbs_1(str);
                points[0] = getLongitudeAbs(points[0]);
                a = (int)points[0];
                points[0] = a;
                points[1] = getLatitudeAbs(points[1]);
                a = (int)points[1];
                points[1] = a;
            }
           
            else        // 1000
            {
              points =  getNumFromString2(str);

            }
            

            return points;
        }

        public List<double> getNumFromString2(String str)
        {
            List<int> s = new List<int>();


            str = str.Trim();
            Match m = Regex.Match(str, @"[+-]?\d+");
            while (m.Success)
            {
                int number = Convert.ToInt32(m.Value);
                s.Add(number);
                GlobalData.LogConsole("1.txt"+ number + "\n");
                m = m.NextMatch();
            }
            List<double> d = new List<double>();
            for (int i = 0; i < s.Count; i++)
                d.Add((double)s[i]);
            return d;

        }

        public void setSafeLocation()
        {
            safeLlocationSetted = true;
        }

        public bool failurePatternFunc(string line)
        {
            #region Failure Patterns
          Match  m = Regex.Match(line, @"contralfailed");
            if (m.Success)
            {
                return true;
            }
            m = Regex.Match(line, @"fatalerrorfailed");
            if (m.Success)
            {
                return true;
            }
            m = Regex.Match(line, @"gpsfailed");
            if (m.Success)
            {
                return true;
            }
            m = Regex.Match(line, @"enginefailed");
            if (m.Success)
            {
                return true;
            }
            m = Regex.Match(line, @"batvfailed");
            if (m.Success)
            {
                return true;
            }
            m = Regex.Match(line, @"rcfailed");
            if (m.Success)
            {

                return true;
            }
            m = Regex.Match(line, @"gcsfailed");
            if (m.Success)
            {

                return true;
            }
            #endregion

            return false;
        }

        private bool checkDataField(string line)
        {
            Match m = Regex.Match(line, @"absloc");
         if (m.Success)
             return true;
          m = Regex.Match(line, @"relloc");
         if (m.Success)
             return true;
          m = Regex.Match(line, @"rwyloc");
         if (m.Success)
             return true;
          m = Regex.Match(line, @"prevwpt");
         if (m.Success)
             return true;
          m = Regex.Match(line, @"nextwpt");
         if (m.Success)
             return true;
          m = Regex.Match(line, @"home");
         if (m.Success)
             return true;
          m = Regex.Match(line, @"gcsHomegpspos");
         if (m.Success)
             return true;
          m = Regex.Match(line, @"firstwptoffset");
         if (m.Success)
             return true;

         return false;

        }
        #endregion

        public List<WayPoint> parseFlyFile(String fileName)
        {
            wayPointList = new List<WayPoint>();
       
            sourceFile = fileName;
            commandsList_toValidate = new List<Command>();
                
            CommandArg = new List<double>();
           bool fixed_ = false;
            String fWithNoCommentsNorEmptyLine = removeComments(System.IO.File.ReadAllText(@fileName)).Trim();
            String[] lines = fWithNoCommentsNorEmptyLine.Split('\n');

            bool notAddedClimbFlag = false;
            List<Command> notAddedClimbCmd = new List<Command>();
            bool appendToPatternCommands = false;
            bool appendToTheadCommands = false;
            int patternID = -1;
            int threadID = -1;
            bool failPattern = false;
            bool failurePattern = false;
            bool climb = true, firstClimb = true;
          //  Command ValidationCommand;
            bool filePattern = false;
            int filePatternNum = -1;
            for (int i = 0; i < lines.Length; i++)
            {
               
                lines[i]=lines[i].Trim();
                String OriginalLineBeforeTrim = lines[i];
                lines[i] = lines[i].ToLower();
               
                if (filePatternNum != 16)
                {
                    Match a = Regex.Match(lines[i], @"definepattern");
                    if (a.Success)
                    {
                        filePatternNum++;
                        filePattern = true;  // emta 2a3mlha false
                        continue;
                    }
                    else if (filePattern)
                    {
                        filePatterns[filePatternNum] += OriginalLineBeforeTrim + "\r\n";
                        a = Regex.Match(lines[i], @"repeat");
                        if ((filePatternNum == 15 && lines[i].Equals("return")) || (filePatternNum == 15 && a.Success))
                            filePatternNum++;
                    }
                }

                if (failurePattern)
                {
              Match   a = Regex.Match(lines[i], @"home");
                    if (a.Success)
                    {
                        setSafeLocation();
                       // continue;
                    }
                    if (failurePatternFunc(lines[i]))
                    {
                        failurePattern = false;
                    }
                    //   Match ma = Regex.Match(lines[i], @"repeat.[-+]?\d");
                    //   if (ma.Success)
                    //      failurePattern = false;
                    else
                    {
                        FP.failurePattern[FP.patternNum] += lines[i];
                        FP.failurePattern[FP.patternNum] += "\r\n";
                        continue;
                    }
                }

                #region    Start of Basic Commands
                Match m = Regex.Match(lines[i], @"flyto.*");
                if (m.Success) /*This is  a flyTo Command */
                {
                    //m = Regex.Match(lines[i], @"home");
                    //if (failPattern && m.Success)
                    //{
                    //    setSafeLocation();
                    //    continue;
                    //}
                    if (checkDataField(lines[i]))
                    {
                        Command cmd = new Command(lines[i]);
                        addCommandToPreviosWP(cmd);
                        continue;
                    }

                    WayPoint ss =new WayPoint();
                    GlobalData.LogConsole(lines[i]);
                   
                    //List<int> p = getNumFromString(lines[i]);
                    //if (p.Count != 2)
                    //{
                    //    validFile = false;
                    //    erorrMsg = "FlyTo with wrong number of args";
                    //    break;
                    //}
                    CommandArg = new List<double>();
                    CommandArg = ParssingAnyTypeOfPoints(lines[i]);
                    if (CommandArg.Count == 2)
                        ss = getWpFromRel((int)CommandArg[1], (int)CommandArg[0]);
                    else
                    {
                        // flyTo [home] or wrong args
                    }
                    //for (int c = 0; c < p.Count; c++)
                    //    CommandArg.Add(p[c]);
                   Command comd = new Command(CommandType.basic, CommandName.flyTo, CommandArg);
                   if (appendToPatternCommands)
                   {
                       addToPatternList(patternID, comd);
                       commandsList_toValidate.Add(comd);
                       continue;
                   }
                   else if (appendToTheadCommands )
                   {
                       if (threadID != 1)
                           navigationCommandNotInThread0 = true;
                       else
                       appendToThreadCommand(comd);
                       continue;
                   }
                   else
                   {
                       if (notAddedClimbFlag == true)
                       {
                           for (int a = 0; a < notAddedClimbCmd.Count; a++)
                           {
                               ss.commandsList.Add(notAddedClimbCmd[a]);
                               //  ValidationCommand = notAddedClimbCmd;
                               commandsList_toValidate.Add(notAddedClimbCmd[a]);
                           }
                           notAddedClimbFlag = false;
                           notAddedClimbCmd.Clear();
                       }

                       ss.commandsList.Add(comd);
                       wayPointList.Add(ss);

                       commandsList_toValidate.Add(comd);

                   }
                    

                    continue;
                }

                // circuit associated with its WP if found else associated with origin
                //m = Regex.Match(lines[i], @"circbggguit.*");
                //if (m.Success) /*This is  a circuit Command */
                //{
                //    GlobalData.LogConsole(lines[i]);
                //    List<int> p = getNumFromString(lines[i]);
                //    WayPoint ss = new WayPoint();
                //    CommandArg = new List<double>();
                //    for (int c = 0; c < p.Count; c++)
                //        CommandArg.Add(p[c]);
                //    Command comd = new Command(CommandType.basic, CommandName.circuit, CommandArg);
                //    if (appendToPatternCommands)
                //    {
                //        addToPatternList(patternID, comd);
                //        commandsList_toValidate.Add(comd);
                //    }
                //    else if (appendToTheadCommands)
                //    {
                //        if (threadID != 1)
                //            navigationCommandNotInThread0 = true;
                //        else
                //            appendToThreadCommand(comd);
                //    }
                //    else
                //    {
                //        if (p.Count == 4) // circuit associated with its WP
                //        {
                //            // p[0] we p[1] heya el wp lat & lng
                //            // hadwar 3aleha fe el wp list we 2a7otha 
                //            // bas mesh haynfa3 3shan lazem ne7wel el awel le absoulte
                //        }
                //        else // circut associated with origin
                //        {
                //            // how come :D 
                //            // check 3ala el climb
                //        }



                //        //ss.commandsList.Add(comd);

                //    }
                //    continue;
                //}

                //waitclimb requires a climb command to initiate the climb. 
                m = Regex.Match(lines[i], @"waitclimb.*\d");
                if (m.Success) /*This is  a waitClimb Command */
                {
                    GlobalData.LogConsole(lines[i]);
                    List<int> p = getNumFromString(lines[i]);
                    CommandArg = new List<double>();
                    for(int a=0; a<p.Count;a++)
                    CommandArg.Add(p[a]);
                    Command comd = new Command(CommandType.basic, CommandName.waitclimb, CommandArg);

                    if (appendToPatternCommands)
                    {
                        addToPatternList(patternID, comd);

                    }
                    else if (appendToTheadCommands)
                    {
                        if (threadID != 1)
                            navigationCommandNotInThread0 = true;
                        else
                            appendToThreadCommand(comd);
                    }
                    else
                    {
                        notAddedClimbFlag = true;
                        notAddedClimbCmd.Add(comd);
                    }
                    commandsList_toValidate.Add(comd);
                    continue;
                }
                 
                m = Regex.Match(lines[i], @"pclimb.*\d");
                if (m.Success) /*This is  a pclimb  Command */
                {
                    GlobalData.LogConsole(lines[i]);
                    List<int> p = getNumFromString(lines[i]);
                    CommandArg = new List<double>();
                    for (int a = 0; a < p.Count; a++)
                        CommandArg.Add(p[a]);
                    Command comd = new Command(CommandType.basic, CommandName.pclimb, CommandArg);
                    if (appendToPatternCommands)
                    {
                        addToPatternList(patternID, comd);
                       
                    }
                    else if (appendToTheadCommands)
                    {
                        if (threadID != 1)
                            navigationCommandNotInThread0 = true;
                        else
                            appendToThreadCommand(comd);
                    }
                    else
                    addCommandToPreviosWP(comd);
                    commandsList_toValidate.Add(comd);
                    continue;
                }
                m = Regex.Match(lines[i], @"climb.*\d");
                if (m.Success) /*This is  a climb Command */
                {
                    if (firstClimb)
                    {
                        firstClimb = false;
                        continue;
                    }
                    // hena na2es eno law mesh ba3dha flyto hay7otha fen ? 
                    // hena el mafrod 2a3mel add le el command da fe el wp el fe el flyTo command el ba3dha
                    // bas lazem 2at2aked eno law mesh ba3dha flyTo we 2ablha takeoff 
                    // el climb command da hayb2a associated with the origin
                    GlobalData.LogConsole(lines[i]);
                    List<int> p = getNumFromString(lines[i]);
                    CommandArg = new List<double>();
                    for (int j = 0; j < p.Count; j++)
                        CommandArg.Add(p[j]);
                    Command comd = new Command(CommandType.basic, CommandName.climb, CommandArg);
                    if (climb)
                    {
                        oralt = p[0];
                        climb = false;
                    }
                    if (appendToPatternCommands)
                    {
                        addToPatternList(patternID, comd);      
                    }
                    else if (appendToTheadCommands)
                    {
                        if (threadID != 1)
                            navigationCommandNotInThread0 = true;
                        else
                            appendToThreadCommand(comd);
                    }
                    else
                    {
                        notAddedClimbFlag = true;
                        notAddedClimbCmd.Add(comd);
                    }
                    commandsList_toValidate.Add(comd);
                    continue;
                }
                m = Regex.Match(lines[i], @"initfromto");//.*[-+]?\d.\d.");
                if (m.Success) /*This is  a Initfromto Command */
                {
                //    #region   // NOT Handeld yet
                //    //GlobalData.LogConsole(lines[i]);
                //    //List<int> p = getNumFromString(lines[i]);
                //    //WayPoint ss;
                //    //// adding the destnation point in ss
                //    ////  and if there is source point it exist in CommandArg

                //    //if (p.Count == 4)
                //    //    ss = new WayPoint(p[3], p[2]);
                //    //else
                //    //    ss = new WayPoint(p[1], p[0]);
                //    //ss.print();
                //    //CommandArg = new List<double>();
                //    //for (int c = 0; c < p.Count; c++)
                //    //    CommandArg.Add(p[c]); /////////////////////
                //    //Command comd = new Command(CommandType.basic, CommandName.Initfromto, CommandArg);
                //    //ss.commandsList.Add(comd);
                //    //wayPointList.Add(ss);
                   
                    #endregion
                    continue;
                }
                m = Regex.Match(lines[i], @"waitfromto");
                if (m.Success) /*This is  a waitfromto Command */
                {
                //    #region   // NOT Handeld yet
                //    //GlobalData.LogConsole(lines[i]);
                //    //List<int> p = getNumFromString(lines[i]);
                //    //WayPoint ss = new WayPoint();
                //    //CommandArg = new List<double>();
                //    //Command comd = new Command(CommandType.basic, CommandName.waitfromto, CommandArg);
                //    //ss.commandsList.Add(comd);
                //    //wayPointList.Add(ss);
                    
                //    #endregion
                    continue;
                }
                m = Regex.Match(lines[i], @"fromto.*");
                if (m.Success) /*This is  a fromto Command */
                {

                    if (checkDataField(lines[i]))
                    {
                        Command cmd = new Command(lines[i]);
                        addCommandToPreviosWP(cmd);
                        continue;
                    }
                    // adding to new WP 
                    GlobalData.LogConsole(lines[i]);
                    //List<int> p = getNumFromString(lines[i]);
                    //if (p.Count != 2 || p.Count != 4)
                    //{
                    //    validFile = false;
                    //    erorrMsg = "FromTo command with wrong number of args";
                    //    break;
                    //}
                    WayPoint ss = new WayPoint();
                    // adding the destnation point in ss
                    //  and if there is source point it exist in CommandArg
                    CommandArg = new List<double>();
                    CommandArg = ParssingAnyTypeOfPoints(lines[i]);
                    if (CommandArg.Count == 2)
                        ss = getWpFromRel((int)CommandArg[1], (int)CommandArg[0]);
                    if (CommandArg.Count == 4)
                        ss = getWpFromRel((int)CommandArg[3], (int)CommandArg[2]);
                    //if (p.Count == 4)
                    //  //  ss = new WayPoint(p[3], p[2]);
                    //       ss = getWpFromRel(p[3], p[2]); 
                    //else if (p.Count == 2)
                    //   // ss = new WayPoint(p[1], p[0]);
                    // ss = getWpFromRel(p[1], p[0]); 
                   // ss.print();
                    
                    //for (int c = 0; c < p.Count; c++)
                    //    CommandArg.Add(p[c]);
                    Command comd = new Command(CommandType.basic, CommandName.fromTo, CommandArg);
                    if (appendToPatternCommands)
                    {
                        addToPatternList(patternID, comd);
                        continue;
                    }
                    else if (appendToTheadCommands)
                    {
                        if (threadID != 1)
                            navigationCommandNotInThread0 = true;
                        else
                            appendToThreadCommand(comd);
                        continue;
                    }
                    else
                    {
                        if (notAddedClimbFlag == true)
                        {
                            for (int a = 0; a < notAddedClimbCmd.Count; a++)
                            {
                                ss.commandsList.Add(notAddedClimbCmd[a]);
                                //  ValidationCommand = notAddedClimbCmd;
                                commandsList_toValidate.Add(notAddedClimbCmd[a]);
                            }
                            notAddedClimbFlag = false;
                            notAddedClimbCmd.Clear();
                        }
                        ss.commandsList.Add(comd);
                        wayPointList.Add(ss);
                    }
                    commandsList_toValidate.Add(comd);
                    continue;
                }
                m = Regex.Match(lines[i], @"hoverat.*");
                if (m.Success) /*This is  a fromto Command */
                {
                    if (checkDataField(lines[i]))
                    {
                        Command cmd = new Command(lines[i]);
                        addCommandToPreviosWP(cmd);
                        continue;
                    }
                    // adding to new WP 
                    GlobalData.LogConsole(lines[i]);
                    List<int> p = getNumFromString(lines[i]);
                    //if (p.Count != 2 )
                    //{
                    //    validFile = false;
                    //    erorrMsg = "hoverAt command with wrong number of args";
                    //    break;
                    //}
                    WayPoint ss = new WayPoint();
                    // adding the destnation point in ss
                    //  and if there is source point it exist in CommandArg

                    CommandArg = new List<double>();
                    CommandArg = ParssingAnyTypeOfPoints(lines[i]);
                    if (CommandArg.Count == 2)
                        ss = getWpFromRel((int)CommandArg[1], (int)CommandArg[0]);
                    // ss.print();
                    //for (int c = 0; c < p.Count; c++)
                    //    CommandArg.Add(p[c]);
                    Command comd = new Command(CommandType.basic, CommandName.hoverAt, CommandArg);
                    if (appendToPatternCommands)
                    {
                        addToPatternList(patternID, comd);
                        continue;
                    }
                    else if (appendToTheadCommands)
                    {
                        if (threadID != 1)
                            navigationCommandNotInThread0 = true;
                        else
                            appendToThreadCommand(comd);
                        continue;
                    }

                    else if (notAddedClimbFlag == true)
                    {
                        for (int a = 0; a < notAddedClimbCmd.Count; a++)
                        {
                            ss.commandsList.Add(notAddedClimbCmd[a]);
                            //  ValidationCommand = notAddedClimbCmd;
                            commandsList_toValidate.Add(notAddedClimbCmd[a]);
                        }
                        notAddedClimbFlag = false;
                        notAddedClimbCmd.Clear();
                    }
                   // else
                   // {
                        ss.commandsList.Add(comd);
                        wayPointList.Add(ss);

                   // }
                    commandsList_toValidate.Add(comd);
                    continue;
                }

                m = Regex.Match(lines[i], @"apprggoach.*");
                if (m.Success) /*This is  a approach Command */
                {
                    #region   // NOT Handeld yet
                    //GlobalData.LogConsole(lines[i]);
                    //List<int> p = getNumFromString(lines[i]);
                    //WayPoint ss;
                    //// adding the destnation point in ss
                    ////  and if there is source point it exist in CommandArg

                    //if (p.Count == 4)
                    //    ss = new WayPoint(p[3], p[2]);
                    //else
                    //    ss = new WayPoint(p[1], p[0]);
                    //ss.print();
                    //CommandArg = new List<double>();
                    //for (int c = 0; c < p.Count; c++)
                    //    CommandArg.Add(p[c]);
                    //Command comd = new Command(CommandType.basic, CommandName.approach, CommandArg);
                    //ss.commandsList.Add(comd);
                    //wayPointList.Add(ss);
                    continue;
                    #endregion
                    continue;
                }
                m = Regex.Match(lines[i], @"flaggre");
                if (m.Success) /*This is  a flare  Command */
                {
                    #region   // NOT Handeld yet
                    //GlobalData.LogConsole(lines[i]);
                    //List<int> p = getNumFromString(lines[i]);
                    //WayPoint ss = new WayPoint();
                    //CommandArg = new List<double>();
                    //Command comd = new Command(CommandType.basic, CommandName.flare, CommandArg);
                    //ss.commandsList.Add(comd);
                    //wayPointList.Add(ss);
                    //continue;
                    #endregion
                    continue;
                }

              //  #endregion  End the Basic Command

                #region Start of Thread and Pattern Commands         

                m = Regex.Match(lines[i], @"startpattern.\d");
                if (m.Success) /*This is  a startpattern  Command */
                {
                    GlobalData.LogConsole(lines[i]);
                    List<int> p = getNumFromString(lines[i]);
                    CommandArg = new List<double>();
                    for (int a = 0; a < p.Count; a++)
                        CommandArg.Add(p[a]);
                    Command comd = new Command(CommandType.basic, CommandName.startpattern, CommandArg);

                    if (wayPointList.Count >1)
                    {
                        wayPointList[wayPointList.Count-1].commandsList.Add(comd); // add to the previos_WP
                        Pattern patt = new Pattern(p[0], wayPointList.Count - 1);
                        wayPointList[wayPointList.Count - 1].patternList.Add(patt);
                    }
                    else
                    {
                        wayPointList[0].commandsList.Add(comd); // add to the previos_WP
                        Pattern patt = new Pattern(p[0], 0);
                        wayPointList[0].patternList.Add(patt);
                    }
                    commandsList_toValidate.Add(comd);
                    continue;
                }
                m = Regex.Match(lines[i], @"definepattern.\d");
                if (m.Success) /*This is  a definepattern  Command */
                {
                    if (!fixed_)
                    {
                        validFile = false;
                        erorrMsg = "Define pattern befor Fixed region";
                        break;
                    }
                    GlobalData.LogConsole(lines[i]);
                    List<int> p = getNumFromString(lines[i]);
                    CommandArg = new List<double>();
                    for (int a = 0; a < p.Count; a++)
                        CommandArg.Add(p[a]);
                    Command comd = new Command(CommandType.basic, CommandName.definePattern, CommandArg);
                    appendToPatternCommands = true;
                    patternID = p[0];
                    addToPatternList(patternID , comd);

                    commandsList_toValidate.Add(comd);
                    continue;
                }
                m = Regex.Match(lines[i], @"repeat.[-+]?\d");
                if (m.Success)
                {
                    List<int> p = getNumFromString(lines[i]);
                    CommandArg = new List<double>();
                    for (int a = 0; a < p.Count; a++)
                        CommandArg.Add(p[a]);
                    Command comd = new Command(CommandType.basic, CommandName.repeat, CommandArg);
                    if (appendToTheadCommands)
                    {
                        appendToThreadCommand(comd);
                        appendToTheadCommands = false;
                        commandsList_toValidate.Add(comd);
                        continue;
                    }
                    else if (appendToPatternCommands == false)
                    {
                        addCommandToPreviosWP(comd);
                        commandsList_toValidate.Add(comd);
                        continue;
                    }
                    addToPatternList(patternID, comd);
                    appendToPatternCommands = false;
                    
                    patternID = -1;
                    commandsList_toValidate.Add(comd);
                    continue;
                }
                m = Regex.Match(lines[i], @"return");
                if (m.Success)
                {
                    List<int> p = getNumFromString(lines[i]);
                    CommandArg = new List<double>();
                    for (int a = 0; a < p.Count; a++)
                        CommandArg.Add(p[a]);
                    Command comd = new Command(CommandType.basic, CommandName.repeat, CommandArg);
                    if (appendToPatternCommands == false)
                    {
                        addCommandToPreviosWP(comd);
                        continue;
                    }
                    addToPatternList(patternID, comd);
                    appendToPatternCommands = false;
                    patternID = -1;
                    commandsList_toValidate.Add(comd);
                    continue;
                }
                
                m = Regex.Match(lines[i], @"thread.\d");
                if (m.Success) /*This is  a thread   Command */
                 {
                     
                     GlobalData.LogConsole(lines[i]);
                     List<int> p = getNumFromString(lines[i]);
                     CommandArg = new List<double>();
                    for(int a=0;a<p.Count;a++)
                     CommandArg.Add(p[a]);
                     Command comd = new Command(CommandType.basic, CommandName.thread, CommandArg);
                    WayPoint ss = new WayPoint();
                    ss.commandsList.Add(comd);
                    wayPointList.Add(ss);       
                     appendToTheadCommands = true;
                     threadID = p[0];
                    commandsList_toValidate.Add(comd);
                    continue;
                 }
                #endregion END of Thread and Pattern Commands

                #region    Start of Advanced Commands
                // circleLeft
                m = Regex.Match(lines[i], @"circleleft.*\d*");
                if (m.Success) /*This is  a circleLeft Command */
                {
                    if (checkDataField(lines[i]))
                    {
                        Command cmd = new Command(lines[i]);
                        addCommandToPreviosWP(cmd);
                        continue;
                    }
                    // if the optional wp found its associated with it else with origin
                    GlobalData.LogConsole(lines[i]);
                    WayPoint ss = new WayPoint();
                 //   List<int> p = getNumFromString(lines[i]);
                    CommandArg = new List<double>();
                    string [] str = lines[i].Split(',');
                    CommandArg = new List<double>();
                    string str1 = "";
                    for (int z = 0; z < str.Length - 1; z++)
                    {
                        if (z == 1)
                            str1 += ",";
                        str1 += str[z];
                    }
                    CommandArg = ParssingAnyTypeOfPoints(str1);
                    CommandArg.Add(double.Parse(str[str.Length-1]));
                  
                  
                    //for (int c = 0; c < p.Count; c++)
                    //    CommandArg.Add(p[c]);

                    Command comd = new Command(CommandType.basic, CommandName.circleLeft, CommandArg);
                    if (appendToPatternCommands)
                    {
                        addToPatternList(patternID, comd);
                        continue;
                    }
                    else if (appendToTheadCommands)
                    {
                        if (threadID != 1)
                            navigationCommandNotInThread0 = true;
                        else
                            appendToThreadCommand(comd);
                        continue;
                    }
                    else
                    {

                        string s = "(";
                        if (lines[i].Contains(s)) // circle left associated with its WP
                        {
                            
                            if (CommandArg.Count >2)
                            ss = getWpFromRel((int)CommandArg[1], (int)CommandArg[0]);
                   
                                if (notAddedClimbFlag == true)
                         {
                        for (int a = 0; a < notAddedClimbCmd.Count; a++)
                        {
                            ss.commandsList.Add(notAddedClimbCmd[a]);
                            //  ValidationCommand = notAddedClimbCmd;
                            commandsList_toValidate.Add(notAddedClimbCmd[a]);
                        }
                        notAddedClimbFlag = false;
                        notAddedClimbCmd.Clear();
                    }
                            ss.commandsList.Add(comd);
                            wayPointList.Add(ss);

                        }
                        else  // circle left associated with origin
                        {
                            // ?? 
                        }

                    }
                    commandsList_toValidate.Add(comd);

                    continue;
                }
                // circleRight
                m = Regex.Match(lines[i], @"circleright.*\d*");
                if (m.Success) /*This is  a circleLeft Command */
                {
                    if (checkDataField(lines[i]))
                    {
                        Command cmd = new Command(lines[i]);
                        addCommandToPreviosWP(cmd);
                        continue;
                    }
                    // if the optional wp found its associated with it else with origin
                    GlobalData.LogConsole(lines[i]);
                    WayPoint ss = new WayPoint();
                    //   List<int> p = getNumFromString(lines[i]);
                    CommandArg = new List<double>();
                    string[] str = lines[i].Split(',');
                    CommandArg = new List<double>();
                    string str1 = "";
                    for (int z = 0; z < str.Length - 1; z++)
                    {
                        if (z == 1)
                            str1 += ",";
                        str1 += str[z];
                    }
                    CommandArg = ParssingAnyTypeOfPoints(str1);
                    CommandArg.Add(double.Parse(str[str.Length - 1]));


                    //for (int c = 0; c < p.Count; c++)
                    //    CommandArg.Add(p[c]);

                    Command comd = new Command(CommandType.basic, CommandName.circleRight, CommandArg);
                    if (appendToPatternCommands)
                    {
                        addToPatternList(patternID, comd);
                        continue;
                    }
                    else if (appendToTheadCommands)
                    {
                        if (threadID != 1)
                            navigationCommandNotInThread0 = true;
                        else
                            appendToThreadCommand(comd);
                        continue;
                    }
                    else
                    {

                        string s = "(";
                        if (lines[i].Contains(s)) // circle left associated with its WP
                        {

                            if (CommandArg.Count > 2)
                                ss = getWpFromRel((int)CommandArg[1], (int)CommandArg[0]);

                            if (notAddedClimbFlag == true)
                            {
                                for (int a = 0; a < notAddedClimbCmd.Count; a++)
                                {
                                    ss.commandsList.Add(notAddedClimbCmd[a]);
                                    //  ValidationCommand = notAddedClimbCmd;
                                    commandsList_toValidate.Add(notAddedClimbCmd[a]);
                                }
                                notAddedClimbFlag = false;
                                notAddedClimbCmd.Clear();
                            }
                            ss.commandsList.Add(comd);
                            wayPointList.Add(ss);

                        }
                        else  // circle left associated with origin
                        {
                            // ?? 
                        }

                    }
                    commandsList_toValidate.Add(comd);

                    continue;
                }
                // relative waypoint command

                #region not handeled
                //m = Regex.Match(lines[i], @"buildrelativewaypoint.*");
                //if (m.Success) 
                //{
                //    //GlobalData.LogConsole(lines[i]);
                //    //List<int> p = getNumFromString(lines[i]);
                //    //WayPoint ss = new WayPoint();
                //    //CommandArg = new List<double>();
                //    //ss = getWpFromRel(p[1], p[0]);
                //    //ss.print();
                //    //CommandArg.Add(ss.lat);
                //    //CommandArg.Add(ss.longt);
                //    //Command comd = new Command(CommandType.basic, CommandName.buildRelativeWaypoint, CommandArg);
                //    //ss.commandsList.Add(comd);
                //    //wayPointList.Add(ss);
                   
                //    continue;
                //}
                //// rotateRelativeWaypoint command
                //m = Regex.Match(lines[i], @"rotaterelativewaypoint.*\d");
                //if (m.Success)
                //{
                //    //GlobalData.LogConsole(lines[i]);
                //    //List<int> p = getNumFromString(lines[i]);
                //    //WayPoint ss = new WayPoint();
                //    //CommandArg = new List<double>();
                //    //ss = getWpFromRel(p[1], p[0]);
                //    //ss.print();
                //    //CommandArg.Add(ss.lat);
                //    //CommandArg.Add(ss.longt);
                //    //CommandArg.Add(p[2]); // rotation
                //    //Command comd = new Command(CommandType.basic, CommandName.rotateRelativeWaypoint, CommandArg);
                //    //ss.commandsList.Add(comd);
                //    //wayPointList.Add(ss);
                //    continue;
                //}
                ////setcontrol  command
                //m = Regex.Match(lines[i], @"setcontrol.*");
                //if (m.Success)
                //{
                //    //GlobalData.LogConsole(lines[i]);
                //    //List<int> p = getNumFromString(lines[i]);
                //    //WayPoint ss = new WayPoint();
                //    //CommandArg = new List<double>();
                //    //CommandArg.Add(p[0]); // type
                //    //CommandArg.Add(p[1]); // parameter
                //    //Command comd = new Command(CommandType.basic, CommandName.setcontrol, CommandArg);
                //    //ss.commandsList.Add(comd);
                //    //wayPointList.Add(ss);
                //    continue;
                //}
                //// setorigin command
                //m = Regex.Match(lines[i], @"setorigin.*");
                //if (m.Success)
                //{
                //    //GlobalData.LogConsole(lines[i]);
                //    //List<int> p = getNumFromString(lines[i]);
                //    //WayPoint ss = new WayPoint();
                //    //CommandArg = new List<double>();
                //    //ss = getWpFromRel(p[1], p[0]);
                //    //ss.print();
                //    //CommandArg.Add(ss.lat);
                //    //CommandArg.Add(ss.longt);
                //    //Command comd = new Command(CommandType.basic, CommandName.setorigin, CommandArg);
                //    //ss.commandsList.Add(comd);
                //    //wayPointList.Add(ss);
                //    continue;
                //}
                //// turn command
                //m = Regex.Match(lines[i], @"turn.*");
                //if (m.Success)
                //{
                //    //GlobalData.LogConsole(lines[i]);
                //    //List<int> p = getNumFromString(lines[i]);
                //    //WayPoint ss = new WayPoint();
                //    //CommandArg = new List<double>();
                //    //if (CommandArg.Count>=2){
                //    //CommandArg.Add(p[0]); // heading
                //    //CommandArg.Add(p[1]); // bank
                //    //}
                //    //Command comd = new Command(CommandType.basic, CommandName.turn, CommandArg);
                //    //ss.commandsList.Add(comd);
                //    //wayPointList.Add(ss);
                //    continue;
                //}

                #endregion
                #endregion END Advanced Commands



                m = Regex.Match(lines[i],@"fixed");
                if (m.Success) {
                    List<int> p = getNumFromString(lines[i]);
                    CommandArg = new List<double>();
                    for (int a = 0; a < p.Count; a++)
                        CommandArg.Add(p[a]);
                    Command comd = new Command(CommandType.basic, CommandName.fixed_, CommandArg);
                    //addCommandToPreviosWP(comd);
                    WayPoint ss = new WayPoint();
                    ss.commandsList.Add(comd);
                    wayPointList.Add(ss);
                    commandsList_toValidate.Add(comd);
                    fixed_ =true;
                    continue;
                }

                // Unit Conversions
                m = Regex.Match(lines[i], @"imperial");
                 if (m.Success)
                 {
                     CommandArg = new List<double>();
                     Command comd = new Command(CommandType.basic, CommandName.imperial, CommandArg);
                     commandsList_toValidate.Add(comd);
                     unitConvesion = "imperial";
                     continue;
                 }
                 m = Regex.Match(lines[i], @"metric");
                 if (m.Success)
                 {
                     CommandArg = new List<double>();
                     Command comd = new Command(CommandType.basic, CommandName.metric, CommandArg);
                     commandsList_toValidate.Add(comd);
                     unitConvesion = "metric";
                     continue;
                 }
                 m = Regex.Match(lines[i], @"defaultunits");
                 if (m.Success)
                 {
                     CommandArg = new List<double>();
                     Command comd = new Command(CommandType.basic, CommandName.defaultUnits, CommandArg);
                     commandsList_toValidate.Add(comd);
                     unitConvesion = "defaultUnits";
                     continue;
                 }
                 m = Regex.Match(lines[i], @"takeoff");
                 if (m.Success)
                 {
                     List<int> p = getNumFromString(lines[i]);
                     CommandArg = new List<double>();
                     for (int a = 0; a < p.Count; a++)
                         CommandArg.Add(p[a]);
                     
                     Command comd = new Command(CommandType.basic, CommandName.Takeoff, CommandArg);
                     commandsList_toValidate.Add(comd);
                     continue;
                 }

                 #region Failure Patterns
                 m = Regex.Match(lines[i], @"controlfailed");
                 if (m.Success)
                 {
                     failPattern = true;
                     failurePattern = true;
                     FP.patternNum = 0;
                     Command comd = new Command(CommandType.basic, CommandName.fatalErrorFailed, CommandArg);
                     FailerPatterns.Add(comd);
                     commandsList_toValidate.Add(comd);
                     //FP.failurePattern[FP.patternNum] += lines[i];
                     //FP.failurePattern[FP.patternNum] += "\r\n";
                     continue;
                 }
                 m = Regex.Match(lines[i], @"fatalerrorfailed");
                 if (m.Success)
                 {
                    
                     failurePattern = true;
                     FP.patternNum = 1;
                     //FP.failurePattern[FP.patternNum] += lines[i];
                     //FP.failurePattern[FP.patternNum] += "\r\n";
                     continue;
                 }
                 m = Regex.Match(lines[i], @"gpsfailed");
                 if (m.Success)
                 {

                     failurePattern = true;
                     FP.patternNum = 2;
                     //FP.failurePattern[FP.patternNum] += lines[i];
                     //FP.failurePattern[FP.patternNum] += "\r\n";
                     continue;
                 }
                 m = Regex.Match(lines[i], @"enginefailed");
                 if (m.Success)
                 {

                     failurePattern = true;
                     FP.patternNum = 3;
                     //FP.failurePattern[FP.patternNum] += lines[i];
                     //FP.failurePattern[FP.patternNum] += "\r\n";
                     continue;
                 }
                 m = Regex.Match(lines[i], @"batvfailed");
                 if (m.Success)
                 {

                     failurePattern = true;
                     FP.patternNum = 4;
                     //FP.failurePattern[FP.patternNum] += lines[i];
                     //FP.failurePattern[FP.patternNum] += "\r\n";
                     continue;
                 }
                 m = Regex.Match(lines[i], @"rcfailed");
                 if (m.Success)
                 {

                     failurePattern = true;
                     FP.patternNum = 5;
                     //FP.failurePattern[FP.patternNum] += lines[i];
                     //FP.failurePattern[FP.patternNum] += "\r\n";
                     continue;
                 }
                 m = Regex.Match(lines[i], @"gcsfailed");
                 if (m.Success)
                 {

                     failurePattern = true;
                     FP.patternNum = 6;
                     //FP.failurePattern[FP.patternNum] += lines[i];
                     //FP.failurePattern[FP.patternNum] += "\r\n";
                     continue;
                 }
#endregion

                if(fixed_ ==false)
                {                  
                Command cmd = new Command(lines[i]);
                addCommandToPreviosWP(cmd);
                }
            }   
           

            //setPattern_WPToMap();

            //if (!validateFlyFile())
            //{
            //    MessageBox.Show("Invalid File : " + erorrMsg, "Fly File Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    validFile = false;
            //}
            //else
            if (!validFile)
            {
                MessageBox.Show("Invalid File : " + erorrMsg, "Fly File Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


          //  patternFill(wayPointList);
          

            return wayPointList;
        }

        void patternFill(List<WayPoint> wp)
        {
            for (int i = 0; i < wp.Count; i++)
            {
                if(wp[i].patternList.Count!=0)
                {
                    //  filePatterns.Add(wp[i].patternList.
                }
            }
        }

        #region Generate Commands & Write in File


        public void flyFileGeneration2(Edit_wayPoint_W edit_WayPoint, List<WayPoint> WPList, string title , Dictionary<int,string> wittenPattren)
        {
            saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "txt files (*.fly)|*.fly|All files (*.*)|*.*";
            saveFileDialog1.RestoreDirectory = true;

            // saveFileDialog1.FileName
            //if (saveFileDialog1.ShowDialog() != DialogResult.OK)
            //    return;
            string[] t = title.Split('.');
            saveFileDialog1.FileName = t[0] ;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                save = true;
                //if (File.Exists(sourceFile))
                //{
                //    File.Delete(sourceFile);
                //}
                // File.WriteAllText(saveFileDialog1.FileName, textBox_ListDestination.Text);
               
                //  fileName = System.IO.Path.GetFileName(s);
                // destFile = System.IO.Path.Combine(sourceFile, saveFileDialog1.FileName);
                //       System.IO.File.Copy(sourceFile, saveFileDialog1.FileName, true);
                StreamReader objReader;
                if (sourceFile.Equals(saveFileDialog1.FileName))
                {
                    if (System.IO.File.Exists("temp.fly"))
                    {
                        System.IO.File.Delete("temp.fly");
                    }
                        string text_temp =System.IO.File.ReadAllText(sourceFile) ;
                    System.IO.File.WriteAllText("temp.fly",
                       text_temp);
                    sourceFile = "temp.fly";
                }
                File.WriteAllText(saveFileDialog1.FileName, String.Empty);

                 objReader = new System.IO.StreamReader(sourceFile, true);
                
                objWritter = new System.IO.StreamWriter(saveFileDialog1.FileName, true);
               
                saved_fName = saveFileDialog1.FileName.Split('\\');
                bool remove = false;
                //   string r="";
                int first = 1;
                string line;
                bool climb = true, _fixed = false, thread = false, disableGcsCmds = false,
                loadedWP = false;
                while ((line = objReader.ReadLine()) != "//RC Transmitter Lost - Fly towards home until RC returns")
                {

                    //if (line.Equals("fixed") || line.Equals("Fixed"))
                    //{
                    //    for (int i = 0; i < WPList.Count; i++)  // for manual adding WP
                    //    {
                    //        if (WPList[i].manualAddedWP)
                    //        {
                    //            foreach (Command cmmmand in WPList[i].commandsList)
                    //            {
                    //                writeInFile(generateCommands(cmmmand));
                    //            }
                    //        }
                    //    }
                    //}
                    if (!disableGcsCmds)
                    {
                      
                        objWritter.WriteLine(line);
                    }
                      if (line == "[disableGcsCmds] = 0")
                    {
                        disableGcsCmds = true;
                    }
                
                    if(line.Equals("fixed"))
                    {
                       
                        _fixed = true;
                    }
                    Match mm = Regex.Match(line, @"thread");
                    if (mm.Success)
                    {
                        thread = true;
                    }
                    if(thread)
                        objWritter.WriteLine(line);

                    if (remove && _fixed == false)
                        objWritter.WriteLine(line);


                  
                    else if (line == "repeat -1" && first != 3)
                    {
                        if(!loadedWP)
                          for (int i = 0; i < WPList.Count; i++)  // for manual adding WP
                            {
                            
                                foreach (Command cmmmand in WPList[i].commandsList)
                                {
                                    if (cmmmand.name == CommandName.repeat)
                                    //  if (cmmmand.name == CommandName.fixed_)
                                    {
                                        loadedWP = true;
                                        break;
                                    }
                                    writeInFile(generateCommands(cmmmand));
                                }
                                if (loadedWP)
                                    break;
                            }

                        for (int i = 0; i < WPList.Count; i++)  // for manual adding WP
                        {
                            if (WPList[i].manualAddedWP)
                            {
                                foreach (Command cmmmand in WPList[i].commandsList)
                                {
                                    //if (cmmmand.name == CommandName.fixed_)
                                    //    continue;
                                    writeInFile(generateCommands(cmmmand));
                                }
                            }
                        }

                       writeInFile(line);
                       writeInFile("fixed");
                        for (int m = 0; m < filePatterns.Length; m++)
                        {
                            writeInFile("definePattern " + m.ToString());
                            writeInFile(filePatterns[m]);
                        }
                       
                        //Match l = Regex.Match(line, @"definepattern");
                        //bool found = false;
                        //if (l.Success)
                        //{
                        //  List<int> patternNum =  getNumFromString(line);
                        //  for (int aa = 0; aa < wittenPattren.Count; aa++)
                        //  {
                        //      if (patternNum[0] == wittenPattren.Keys.ToList()[aa])
                        //      {
                        //          found = true;
                        //          writeInFile("definePattern " + patternNum[0].ToString());
                        //          writeInFile(wittenPattren.Values.ToList()[aa]);

                        //      }
                        //  }
                        //}
                        //if(!found)
                        //objWritter.WriteLine(line);
                        remove = false;
                        first = 3;

                    }
                    else if (remove && first == 1)
                    {
                        //write wayPoint list
                        //  int index = 0;
                        first = 2;
                        bool BREAK = false, write = false;
                        foreach (WayPoint wp in WPList)
                        {
                            foreach (Command cmd in wp.commandsList)
                            {

                                if (cmd.name == CommandName.repeat)
                                {
                                    BREAK = true;
                                    break;

                                }
                                if (cmd.name == CommandName.fixed_)
                                {
                                    BREAK = true;
                                    break;

                                }
                                else if (cmd.name == CommandName.climb && climb)
                                {
                                    //  write = true;
                                    climb = false;
                                    continue;
                                }

                                //  if (write)
                                {
                               //     writeInFile(generateCommands(cmd));
                                }

                            }
                            if (BREAK)
                            {
                                //   remove = false;
                               
                                break;
                            }
                        }
                    }
                }


                writeInFile("//RC Transmitter Lost - Fly towards home until RC returns");
                // Write failure patterns
                for (int i = 0; i < FP.failurePattern.Length; i++)
                {
                    if (FP.failurePattern[i] != null)
                    {
                        writeInFile(getFailurePatternName(i));
                        writeInFile(FP.failurePattern[i]);
                    }
                }

                objWritter.Close();
            }

            else
            {
                save = false;
                return;
            }


        }

        private string getFailurePatternName(int j)
        {
            if (j == 0)
                return "pattern controlFailed";
            else if (j == 1)
                return "pattern fatalErrorFailed";
            else if (j == 2)
                return "pattern gpsFailed ";
            else if (j == 3)
                return "pattern engineFailed";
            else if (j == 4)
                return "pattern batVFailed ";
            else if (j == 5)
                return "pattern rcFailed ";
            else
                return "pattern gcsFailed ";
        }


        public void flyFileGeneration(Edit_wayPoint_W edit_WayPoint, List<WayPoint> WPList, string title ,  Dictionary<int,string> wittenPattren)
        {
            UAV_GUI.Forms.Flight_Plan.PredefinedPatterns.fillDictionaryToWrite();
            saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "txt files (*.fly)|*.fly|All files (*.*)|*.*";
            saveFileDialog1.RestoreDirectory = true;

            string[] t = title.Split('.');
            saveFileDialog1.FileName = t[0];
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                save = true;
                // File.WriteAllText(saveFileDialog1.FileName, textBox_ListDestination.Text);
                File.WriteAllText(saveFileDialog1.FileName, String.Empty);
                objWritter = new System.IO.StreamWriter(saveFileDialog1.FileName, true);
                saved_fName = saveFileDialog1.FileName.Split('\\');
                //for (int i = 0; i < WPList.Count; i++)
                //    wayPointList.Add(WPList[i]);

                // Section1 Unit Conversions 
                //  if (unitConvesion.Equals(""))
                writeInFile(Unit_ConversionsCombo);
                writeInFile("[disableGcsCmds] = 0xFFFFFFFF");

                //else
                //    writeInFile(unitConvesion);
                // empty line
                // Section2 Takeoff (is it need to specifiy in the UI ?? )
                writeInFile("takeoff");
                writeInFile("[disableGcsCmds] = 0");
                // Section3 Main navigation commands
                int index = 0;
                bool BREAK = false;
                foreach (WayPoint wp in WPList)  // for manual adding WP
                {
                    foreach (Command cmd in wp.commandsList)
                    {
                        if (cmd.name == CommandName.fixed_)
                        {

                            BREAK = true;
                            break;

                        }
                        if (BREAK)
                            break;
                        writeInFile(generateCommands(cmd));
                        index++;
                    }
                }
                writeInFile("circuit (0, 0), 0, 0 \n repeat -1");
                writeInFile("fixed");
                bool found = false;
                // written pattern 
                for (int i = 0; i < 16; i++)
                {
                    found = false;
                    if (!(filePatterns[i]==(null)))
                    {
                        writeInFile("definePattern " + i.ToString());
                        writeInFile(filePatterns[i]);

                        found = true;
                      
                    }
                    if (!found)
                    {
                        if (i < 12)
                        {
                            writeInFile("definePattern " + i.ToString());
                            writeInFile(UAV_GUI.Forms.Flight_Plan.PredefinedPatterns.preDefinedPattrensToWrite.Values.ToList()[i]);

                        }
                    }

                }

                writeInFile("//RC Transmitter Lost - Fly towards home until RC returns");
                // Write failure patterns
                for (int i = 0; i < FP.failurePattern.Length; i++)
                {
                    if (FP.failurePattern[i] != null)
                    {
                        writeInFile(getFailurePatternName(i));
                        writeInFile(FP.failurePattern[i]);
                    }
                }

                objWritter.Close();
                #region
                //  writeInFile(generateCommands(edit_WayPoint.CommandList));
                // repeat OR return ( is it must ?? )
                // number of empty lines 

                // Section4 Fixed 
                //     I doesn't need this line if I load the file but I need if I generate
                // if (Mode.Generate)
                //index++;
                //for (int i=index ; i < WPList.Count;i++ )  // for manual adding WP
                //{
                //    if (WPList[i].manualAddedWP)
                //    {
                //        foreach (Command cmd in WPList[i].commandsList)
                //        {
                //            writeInFile(generateCommands(cmd));
                //        }
                //    }
                //}


                //    foreach (WayPoint wp in WPList)
                //    {
                //        for (int c = 0; c < wp.patternList.Count; c++)
                //        {
                //            for (int n = 0; n < wp.patternList[c].patternCommands.Count; n++)
                //            {
                //                writeInFile(generateCommands(wp.patternList[c].patternCommands[n]));
                //            }
                //        }
                //    }
                //}
                //bool threadFound = false;
                //for (int i = 0; i < wayPointList.Count; i++)
                //{
                //    for (int c = 0; c < wayPointList[i].commandsList.Count; c++)
                //    {
                //        if (wayPointList[i].commandsList[c].name == CommandName.thread)
                //        {        
                //            threadFound = true;
                //        }
                //        if(threadFound)
                //            writeInFile(generateCommands(wayPointList[i].commandsList[c]));
                //    }
                //}

                // Failuer Patterns :
                //if (failuerPatterns != "")
                //    writeInFile(failuerPatterns);
                #endregion
            }

            else
            {
                save = false;
                return;
            }
        }

       string generateCommands(Command commandData)
        {
            

            string command;
            if (commandData.name == CommandName.UnParsing)
                return commandData.comd;
            if (commandData.name == CommandName.fixed_)
            {
                command = "fixed";
            }
            else
                command = commandData.name.ToString();  // command name
            if (commandData.CommandArgs.Count > 0)
            {
                #region Basic Commands :

                if (commandData.name.ToString().Equals("flyTo") || commandData.name.ToString().Equals("hoverAt"))
                {
                    command = command + " (" + commandData.CommandArgs[0] + "," + commandData.CommandArgs[1] + ")";

                }
                else if (commandData.name.ToString().Equals("climb"))
                {
                    command = command + " " + commandData.CommandArgs[0];

                    if (commandData.CommandArgs.Count > 1)  // there is an optional paramter
                        command = command + "," + commandData.CommandArgs[1];
                }
                else if (commandData.name.ToString().Equals("waitclimb") || commandData.name.ToString().Equals("pclimb"))
                {
                    command = command + " " + commandData.CommandArgs[0];
                }
                else if (commandData.name.ToString().Equals("fromTo") || commandData.name.ToString().Equals("Initfromto") || commandData.name.ToString().Equals("approach"))
                {
                    command = command + " (" + commandData.CommandArgs[0] + "," + commandData.CommandArgs[1] + ")";
                    if (commandData.CommandArgs.Count == 4)
                        command = command + "," + "(" + commandData.CommandArgs[2] + "," + commandData.CommandArgs[3] + ")";
                }
                else if (commandData.name.ToString().Equals("circuit"))
                {
                    if (commandData.CommandArgs.Count == 4)
                        command = command + " (" + commandData.CommandArgs[1] + "," + commandData.CommandArgs[2] + ")"
                            + "," + commandData.CommandArgs[2] + "," + commandData.CommandArgs[3];
                    else if (commandData.CommandArgs.Count == 2)
                    {
                        command = command + " " + commandData.CommandArgs[0] + "," + commandData.CommandArgs[1];
                    }
                }

                #endregion end basic

                #region Advanced commands :
                else if (commandData.name.ToString().Equals("circleLeft") || commandData.name.ToString().Equals("circleRight"))
                {
                    if (commandData.CommandArgs.Count == 6)
                        command = command + " (" + commandData.CommandArgs[1] + "," + commandData.CommandArgs[0] + ")"
                            + "," + commandData.CommandArgs[2] + "," + "(" + commandData.CommandArgs[3] + ","
                            + commandData.CommandArgs[4] + "," + commandData.CommandArgs[5] + ")";

                    else if (commandData.CommandArgs.Count == 4)
                    {
                        command = command + " " + commandData.CommandArgs[0] + "," + "(" + commandData.CommandArgs[1] + ","
                            + commandData.CommandArgs[2] + "," + commandData.CommandArgs[3] + ")";
                    }
                    else if (commandData.CommandArgs.Count == 1)
                    {
                        command = command + " " + commandData.CommandArgs[0];
                    }
                    // 
                    if (commandData.CommandArgs.Count == 3)
                    {
                        command = command + " (" + commandData.CommandArgs[0] + "," + commandData.CommandArgs[1] + ")"
                            + "," + commandData.CommandArgs[2];
                    }
                }
                else if (commandData.name.ToString().Equals("buildRelativeWaypoint"))
                {
                    command = command + " (" + commandData.CommandArgs[0] + "," + commandData.CommandArgs[1] + ")";

                }
                else if (commandData.name.ToString().Equals("buildRelativeWaypoint"))
                {
                    command = command + " (" + commandData.CommandArgs[0] + "," + commandData.CommandArgs[1] + ")";

                }
                else if (commandData.name.ToString().Equals("rotateRelativeWaypoint "))
                {
                    command = command + " (" + commandData.CommandArgs[0] + "," + commandData.CommandArgs[1] + ")"
                        + "," + commandData.CommandArgs[2];

                }
                else if (commandData.name.ToString().Equals("setcontrol"))
                {
                    command = command + "" + commandData.CommandArgs[0] + "," + commandData.CommandArgs[1];

                }
                else if (commandData.name.ToString().Equals("setorigin"))
                {
                    command = command + " (" + commandData.CommandArgs[0] + "," + commandData.CommandArgs[1] + ")";

                }
                else if (commandData.name.ToString().Equals("turn"))
                {
                    command = command + "" + commandData.CommandArgs[0] + "," + commandData.CommandArgs[1];

                }
                #endregion end advanced

                #region Calculation Commands
                if (commandData.name.ToString().Equals("push") || commandData.name.ToString().Equals("sub")
                    || commandData.name.ToString().Equals("div") || commandData.name.ToString().Equals("mul"))
                {
                    command = command + " " + commandData.CommandArgs[0];

                }
                if (commandData.name.ToString().Equals("pop") || commandData.name.ToString().Equals("add"))
                {
                    command = command + " [" + commandData.CommandArgs[0] + "]";

                }
                if (commandData.name.ToString().Equals("Assignment"))
                {
                    //[field] = value, (mask) 

                    // EX :
                    //[cruiseSpd] = 50   
                    //[climbSpeed] = [cruiseSpd]
                    // WITH mask : [1567] = 128, 128   

                }

                #endregion

                #region Thread and Pattern Commands

                if (commandData.name.ToString().Equals("thread") || commandData.name.ToString().Equals("wake")
                  || commandData.name.ToString().Equals("int_") || commandData.name.ToString().Equals("int_Negative1")
                  || commandData.name.ToString().Equals("definePattern") || commandData.name.ToString().Equals("pattern")
                  || commandData.name.ToString().Equals("startpattern") || commandData.name.ToString().Equals("switchPattern")
                  || commandData.name.ToString().Equals("repeat") || commandData.name.ToString().Equals("dim"))
                {
                    command = command + " " + commandData.CommandArgs[0];
                    if (commandData.CommandArgs.Count == 2)
                        command = command + "," + commandData.CommandArgs[1];
                }

                if (commandData.name.ToString().Equals("save"))
                {
                    command = command + " [" + commandData.CommandArgs[0] + "]";
                    if (commandData.CommandArgs.Count == 2)
                        command = command + "," + "[" + commandData.CommandArgs[1] + "]";
                }
                #endregion Thread and Pattern Commands

                #region Skip commands:

                #endregion

            }

            return command;

        }

        private void writeInFile(string command)
        {        
           // objWritter = new System.IO.StreamWriter(saveFileDialog1.FileName, true);
            objWritter.WriteLine(command);
          //  objWritter.Close();

        }


        #endregion
}
       
}
