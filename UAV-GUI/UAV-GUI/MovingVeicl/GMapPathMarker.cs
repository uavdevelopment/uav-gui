﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using GMap.NET.WindowsForms;
using GMap.NET;

namespace UAV_GUI
{
    public class GMapPathMarker : GMapMarkerImage
    {
        List<PointLatLng> actualPath = new List<PointLatLng>();
        int windowsCount =1; 
        public GMapRoute route ;//= new GMapRoute(path, "pathRoute");
        int currentWindow =1 ;
        int windowsiZe; 
        public List<PointLatLng> ActualPath
        {
            get { return actualPath; }
        }

        List<PointLatLng> path = new List<PointLatLng>();

        public List<PointLatLng> Path
        {
            get { return path; }
        }

        int lmt;

        public int Limit
        {
            get { return lmt; }
            set { lmt = value; }
        }
 
        public PointLatLng LogPosition
        {
            get { return Position; }
            set
            {
                this.Position = value;
                if (((int)Position.Lat) == 0 && ((int)Position.Lng) == 0)
                    return;
                this.actualPath.Add(value);
                if (this.path.Count < this.lmt)
                {
                    this.route.Points.Add(value);
                    this.path.Add(value);
                }
                else {
                    int toBeRemoved = new Random().Next(currentWindow,currentWindow+ windowsiZe);
                    Console.WriteLine(toBeRemoved);
                    this.path.RemoveAt(toBeRemoved);
                    this.route.Points.RemoveAt(toBeRemoved);
                    this.route.Points.Add(value);
                    this.path.Add(value);
                    currentWindow = ((currentWindow +(windowsiZe))%(lmt-windowsiZe))+windowsiZe;
                }
            }
        }

 
       

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="p">The position of the marker</param>
        public GMapPathMarker(PointLatLng p, Image image, int limit,int windowsCount_)
            : base(p,image)
        {
            lmt = limit;
            windowsCount = windowsCount_;
            windowsiZe = lmt / windowsCount;
            route= new GMapRoute(path, "pathRoute"+p.Lat.ToString());
            route.Stroke.Width = 2;
            route.Stroke.Color = Color.SeaGreen;
        }

        public GMapPathMarker(PointLatLng p, Image image, float heading,int limit,int windowsCount_)
            : base(p,image,heading)
        {
            lmt = limit;
            windowsCount = windowsCount_;
            windowsiZe = lmt / windowsCount;
            route= new GMapRoute(path, "pathRoute");
            route.Stroke.Width = 2;
            route.Stroke.Color = Color.SeaGreen;
        }

        

        
    }
}