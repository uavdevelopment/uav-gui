﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using GMap.NET.WindowsForms;
using GMap.NET;

namespace UAV_GUI
{
    public class GMapMarkerImage : GMap.NET.WindowsForms.GMapMarker
    {
        private Image img;
        private float hdg;
        public float Heading
        {
            get { return hdg; }
            set { hdg = value;
                //Overlay.Control.Invalidate() ;
            }
        }

        /// <summary>
        /// The image to display as a marker.
        /// </summary>
        public Image MarkerImage
        {
            get
            {
                return img;
            }
            set
            {
                img = value;    
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="p">The position of the marker</param>
        public GMapMarkerImage(PointLatLng p, Image image)
            : base(p)
        {
            img = image;
            Size = img.Size;
            this.hdg = 0;
            Offset = new System.Drawing.Point(-Size.Width / 2, -Size.Height / 2);
            this.ToolTipMode = MarkerTooltipMode.Always;
        
        }

        public GMapMarkerImage(PointLatLng p, Image image,float heading)
            : base(p)
        {
            img = image;
            Size = img.Size;
            this.hdg = heading;
            Offset = new System.Drawing.Point(-Size.Width / 2, -Size.Height / 2);
            this.ToolTipMode = MarkerTooltipMode.Always;
        
        }

        public override void OnRender(Graphics g)
        {
            g.DrawImage(RotateImage(img,hdg), LocalPosition.X, LocalPosition.Y, Size.Width, Size.Height);
        }

        public Bitmap RotateImage(Image image, float angle)
        {
            Bitmap rotatedBmp = new Bitmap(image.Width, image.Height);
            rotatedBmp.SetResolution(image.HorizontalResolution, image.VerticalResolution);
            Graphics g = Graphics.FromImage(rotatedBmp);
            PointF offset = new PointF(image.Width / 2, image.Height / 2);
            g.TranslateTransform(offset.X, offset.Y);
            g.RotateTransform(angle);
            g.TranslateTransform(-offset.X, -offset.Y);
            g.DrawImage(image, new PointF(0, 0));
            return rotatedBmp;
        }
        
    }
}