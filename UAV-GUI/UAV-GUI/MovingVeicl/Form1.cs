﻿using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UAV_GUI
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            timer1.Start();
        }
        int count = 1;
        GMapPathMarker markera;
        GMapPathMarker markerb;
        private void Form1_Load(object sender, EventArgs e)
        {
            gmap.MapProvider = GMap.NET.MapProviders.GoogleMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;
            gmap.Position = new PointLatLng(31, 31);
            Image mya = Image.FromFile("imgs/a.png");
           // Image myb = Image.FromFile("b.png");
            GMapOverlay markersOverlay = new GMapOverlay("markers");

            markera = new GMapPathMarker(new PointLatLng(31, 31),
            mya,30,50,10);
            markerb = new GMapPathMarker(new PointLatLng(31.01, 31.01),
           mya,0,50,10);
            markersOverlay.Markers.Add(markera);
            markersOverlay.Markers.Add(markerb);
            gmap.Overlays.Add(markersOverlay);
            GMapOverlay routesOverlay = new GMapOverlay("routes");
            GMapOverlay routesOverlaya = new GMapOverlay("routles");
            
            routesOverlaya.Routes.Add(markera.route);
            routesOverlay.Routes.Add(markerb.route);
            markerb.route.Stroke.Color = Color.Red;
            gmap.Overlays.Add(routesOverlay);
            gmap.Overlays.Add(routesOverlaya);
            //markerb.Heading = 45;
            
            
        }

        private void gmap_Load(object sender, EventArgs e)
        {

        }
        int initHdg = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            
          // markerb.Heading = initHdg +=10;
          //markera.Heading = -1*initHdg;
            Random rnd = new Random();
            int month = rnd.Next(5, 10);
            int n = rnd.Next(1, 5);
            markera.LogPosition = new PointLatLng(markera.LogPosition.Lat + (0.001 * n), markera.LogPosition.Lng + (0.001 * month));
            markerb.LogPosition = new PointLatLng(markera.LogPosition.Lat + (0.001 * n), markera.LogPosition.Lng + (0.001 * month));
            count++;
            
            gmap.UpdateRouteLocalPosition(markera.route);
            if (count == 15)
            {
                markerb.route.Stroke.Color = Color.Beige;
                gmap.UpdateRouteLocalPosition(markerb.route);
            } this.label2.Text = "" + markera.ActualPath.Count;
            this.label1.Text = "" + markera.Path.Count;
            this.label3.Text = "" + markera.route.Points.Count;
            
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        
    }
}
