﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterClasses ;
using System.IO;
using UAV_GUI.Utilities;

namespace UAV_GUI
{
    static class NavStartData
    {
       public static double currentLat = 0;
       public static double currentLong = 0;
       public static bool GPSFix = false;
       public static bool toMP = false;
       public static String nav = "";
    }
}
