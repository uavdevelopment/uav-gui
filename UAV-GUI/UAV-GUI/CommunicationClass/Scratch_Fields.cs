﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAV_GUI
{
    public enum Scratch_Fields
    {
        RightPacketsCount = 1550 , 
        TotalPacketsCount = 1549 ,
        TotalRate = 1558 , 
        latitude = 1551 , 
        longitude = 1552, 
        altitude = 1553 , 
        GPSStatus = 1560, 
        NAvigationMode = 1561 ,

        VELE = 1562 , 
        VELN = 1563 , 
        VELU = 1564,
 
        ROLL = 1555 , 
        PITCH = 1556, 
        AZIMUTH = 1557 
    }
}
