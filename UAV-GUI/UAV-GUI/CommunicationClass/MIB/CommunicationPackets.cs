﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace UAV_GUI
{
    class CommunicationPackets
    {


        public static byte[] headerSynchBytesArray = { (byte)'T', (byte)'P', (byte)'I' };

        public static byte HEADER_SIZE_IN_BYTES = (byte)Marshal.SizeOf(typeof(Header));
        public static uint CHECKSUM_SIZE_IN_BYTES = 2;

        public static ushort COMMAND_NAV_START_PAYLOAD_SIZE_IN_BYTES                = (ushort)Marshal.SizeOf(typeof(Command_NAV_START.Payload));
        public static ushort COMMAND_ZERO_PAYLOAD_PAYLOAD_SIZE_IN_BYTES             = 0;
        public static ushort COMMAND_MAG_PRECALIB_START_PAYLOAD_SIZE_IN_BYTES       = (ushort)Marshal.SizeOf(typeof(Command_MAG_PRECALIB_START.Payload));
        public static ushort COMMAND_SET_IMU_SPECS_PAYLOAD_SIZE_IN_BYTES            = (ushort)Marshal.SizeOf(typeof(Command_SET_IMU_SPECS.Payload));
        public static ushort COMMAND_SET_SENSORS_USED_PAYLOAD_SIZE_IN_BYTES         = (ushort)Marshal.SizeOf(typeof(Command_SET_SENSORS_USED.Payload));
        public static ushort COMMAND_SET_OUTPUT_MESSAGES_TYPE_PAYLOAD_SIZE_IN_BYTES = (ushort)Marshal.SizeOf(typeof(Command_SET_OUTPUT_MESSAGES_TYPE.Payload));
        public static ushort COMMAND_MAG_PRECALIB_TEST_START_PAYLOAD_SIZE_IN_BYTES  = (ushort)Marshal.SizeOf(typeof(Command_MAG_PRECALIB_TEST_START.Payload));

        public static ushort RESPONSE_ACKNOWLEDGE_PAYLOAD_SIZE_IN_BYTES = (ushort)Marshal.SizeOf(typeof(Response_ACKNOWLEDGE.Payload));
        public static ushort RESPONSE_ACKNOWLEDGE_PACKET_SIZE_IN_BYTES  = (ushort)Marshal.SizeOf(typeof(Response_ACKNOWLEDGE));

        public static ushort RESPONSE_STM32_UID_PAYLOAD_SIZE_IN_BYTES   = (ushort)Marshal.SizeOf(typeof(Response_STM32_UID.Payload));
        public static ushort RESPONSE_STM32_UID_PACKET_SIZE_IN_BYTES    = (ushort)Marshal.SizeOf(typeof(Response_STM32_UID));

        public static ushort RESPONSE_NAVIGATION_PAYLOAD_SIZE_IN_BYTES  = (ushort)Marshal.SizeOf(typeof(Response_NAVIGATION.Payload));
        public static ushort RESPONSE_NAVIGATION_PACKET_SIZE_IN_BYTES   = (ushort)Marshal.SizeOf(typeof(Response_NAVIGATION));

        public static ushort RESPONSE_ACTIVITY_PAYLOAD_SIZE_IN_BYTES    = (ushort)Marshal.SizeOf(typeof(Response_ACTIVITY.Payload));
        public static ushort RESPONSE_ACTIVITY_PACKET_SIZE_IN_BYTES     = (ushort)Marshal.SizeOf(typeof(Response_ACTIVITY));

        public static ushort RESPONSE_MAG_PRECALIB_PAYLOAD_SIZE_IN_BYTES = (ushort)Marshal.SizeOf(typeof(Response_MAG_PRECALIB.Payload));
        public static ushort RESPONSE_MAG_PRECALIB_PACKET_SIZE_IN_BYTES  = (ushort)Marshal.SizeOf(typeof(Response_MAG_PRECALIB));

        //added by M.gaafar
        public static ushort RESPONSE_MAG_PRECALIB_PARAMETERS_PAYLOAD_SIZE_IN_BYTES = (ushort)Marshal.SizeOf(typeof(Response_MAG_PRECALIB_PARAMETERS.Payload));
        public static ushort RESPONSE_MAG_PRECALIB_PARAMETERS_PACKET_SIZE_IN_BYTES = (ushort)Marshal.SizeOf(typeof(Response_MAG_PRECALIB_PARAMETERS));

        public static ushort RESPONSE_MAG_PRECALIB_TEST_START_PAYLOAD_SIZE_IN_BYTES = (ushort)Marshal.SizeOf(typeof(Response_MAG_PRECALIB_TEST_START.Payload));
        public static ushort RESPONSE_MAG_PRECALIB_TEST_START_PACKET_SIZE_IN_BYTES  = (ushort)Marshal.SizeOf(typeof(Response_MAG_PRECALIB_TEST_START));

        public static ushort RESPONSE_BIAS_SETUP_START_PAYLOAD_SIZE_IN_BYTES = (ushort)Marshal.SizeOf(typeof(Response_BIAS_SETUP_START.Payload));
        public static ushort RESPONSE_BIAS_SETUP_START_PACKET_SIZE_IN_BYTES  = (ushort)Marshal.SizeOf(typeof(Response_BIAS_SETUP_START));

        public const ushort COMMAND_MAG_PRECALIB_START_ID       = 0x0001;
        public const ushort COMMAND_MAG_PRECALIB_STOP_ID 	    = 0x0002;
        public const ushort COMMAND_NAV_START_ID  			    = 0x0003;
        public const ushort COMMAND_NAV_STOP_ID 	 		    = 0x0004;
        public const ushort COMMAND_APP_KILL_ID				    = 0x0006;
        public const ushort COMMAND_ZUPT_CALIB_START_ID 	    = 0x0007;
        public const ushort COMMAND_ZUPT_CALIB_STOP_ID  	    = 0x0009;
        public const ushort COMMAND_ANTENNA_LINE_MISALIGNMENT_COMPUTATION_START_ID	= 0x0008;
        public const ushort COMMAND_ANTENNA_LINE_MISALIGNMENT_COMPUTATION_STOP_ID 	= 0x000A;
        public const ushort COMMAND_HEADING_LOG_START_ID  	    = 0x000B;
        public const ushort COMMAND_HEADING_LOG_STOP_ID 	    = 0x000C;
        public const ushort COMMAND_ACKNOWLEDGE_ID  		    = 0x000E;
        public const ushort COMMAND_GET_VERSION_ID  		    = 0x000F;
        public const ushort COMMAND_GET_STM32_UID_ID            = 0x0011;

        public const ushort COMMAND_GET_IMU_SPECS_ID            = 0x0012;
        public const ushort COMMAND_SET_IMU_SPECS_ID            = 0x0013;

        public const ushort COMMAND_GET_SENSORS_USED_ID         = 0x0014;
        public const ushort COMMAND_SET_SENSORS_USED_ID         = 0x0015;

        public const ushort COMMAND_SET_OUTPUT_MESSAGES_TYPE_ID = 0x0016;

        public const ushort COMMAND_MAG_PRECALIB_RESET_ID       = 0x0017;

        public const ushort COMMAND_MAG_PRECALIB_TEST_START_ID  = 0x0018;
        public const ushort COMMAND_MAG_PRECALIB_TEST_STOP_ID   = 0x0019;

        public const ushort COMMAND_BIAS_SETUP_START_ID         = 0x001A;
        public const ushort COMMAND_BIAS_SETUP_STOP_ID          = 0x001B;

        public const ushort RESPONSE_ACKNOWLEDGE_ID			    = COMMAND_ACKNOWLEDGE_ID;
        public const ushort RESPONSE_VERSION_ID                 = COMMAND_GET_VERSION_ID;
        public const ushort RESPONSE_NAVIGATION_ID              = COMMAND_NAV_START_ID;
        public const ushort RESPONSE_ACTIVITY_ID                = 0x0010;
        public const ushort RESPONSE_STM32_UID_ID               = COMMAND_GET_STM32_UID_ID;
        public const ushort RESPONSE_GET_IMU_SPECS_ID           = COMMAND_GET_IMU_SPECS_ID;
        public const ushort RESPONSE_GET_SENSORS_USED_ID        = COMMAND_GET_SENSORS_USED_ID;
        public const ushort RESPONSE_MAG_PRECALIB_ID            = COMMAND_MAG_PRECALIB_START_ID;
        public const ushort RESPONSE_MAG_PRECALIB_PARAMETERS_ID = COMMAND_MAG_PRECALIB_STOP_ID;
        public const ushort RESPONSE_MAG_PRECALIB_TEST_START_ID  = COMMAND_MAG_PRECALIB_TEST_START_ID;
        public const ushort RESPONSE_BIAS_SETUP_START_ID        = COMMAND_BIAS_SETUP_START_ID;

        public static Command_NAV_START                 command_NAV_START ;
        public static Command_MAG_PRECALIB_START        command_MAG_PRECALIB_START;
        public static Command_ZERO_PAYLOAD              command_ACKNOWLEDGEMENT;
        public static Command_ZERO_PAYLOAD              command_GET_VERSION;
        public static Command_ZERO_PAYLOAD              command_GET_STM32_UID;
        public static Command_ZERO_PAYLOAD              command_NAV_STOP ;
        public static Command_ZERO_PAYLOAD              command_GET_IMU_SPECS;
        public static Command_ZERO_PAYLOAD              command_GET_SENSORS_USED;
        public static Command_ZERO_PAYLOAD              command_MAG_PRECALIB_STOP;
        public static Command_ZERO_PAYLOAD              command_MAG_PRECALIB_RESET;
        public static Command_SET_IMU_SPECS             command_SET_IMU_SPECS;
        public static Command_SET_SENSORS_USED          command_SET_SENSORS_USED;
        public static Command_SET_OUTPUT_MESSAGES_TYPE  command_SET_OUTPUT_MESSAGES_TYPE;
        public static Command_MAG_PRECALIB_TEST_START   command_MAG_PRECALIB_TEST_START;
        public static Command_ZERO_PAYLOAD              command_MAG_PRECALIB_TEST_STOP;
        public static Command_ZERO_PAYLOAD              command_BIAS_SETUP_START;
        public static Command_ZERO_PAYLOAD              command_BIAS_SETUP_STOP;

        public static Response_ACKNOWLEDGE              response_ACKNOWLEDGE;
        public static Response_NAVIGATION               response_NAVIGATION;
        public static Response_STM32_UID                response_STM32_UID;
        public static Response_GET_IMU_SPECS            response_GET_IMU_SPECS;
        public static Response_GET_SENSORS_USED         response_GET_SENSORS_USED;
        public static Response_MAG_PRECALIB             response_MAG_PRECALIB;
        public static Response_ACTIVITY                 response_ACTIVITY;
        public static Response_MAG_PRECALIB_PARAMETERS  response_MAG_PRECALIB_PARAMETERS;
        public static Response_MAG_PRECALIB_TEST_START  response_MAG_PRECALIB_TEST_START;
        public static Response_BIAS_SETUP_START         response_BIAS_SETUP_START;
        public static Response_GET_VERSION              response_GET_VERSION;

        private CommunicationPackets()
        {}

        public static void initialize(){

            command_NAV_START.header.synchByteA = headerSynchBytesArray[0];
            command_NAV_START.header.synchByteB = headerSynchBytesArray[1];
            command_NAV_START.header.synchByteC = headerSynchBytesArray[2];
            command_NAV_START.header.payloadLength = COMMAND_NAV_START_PAYLOAD_SIZE_IN_BYTES;
            command_NAV_START.header.packetId = COMMAND_NAV_START_ID;
            command_NAV_START.payload.initialLatitude_degrees = +51.0834; //input text box of GUI
            command_NAV_START.payload.initialLongitude_degrees = -114.1309; //input text box of GUI
            command_NAV_START.payload.initialAltitude_meters = +1000.00; //input text box of GUI
            command_NAV_START.payload.initialHeading_degrees = 0.0; //input text box of GUI
            insertChecksum(ref command_NAV_START);

            command_MAG_PRECALIB_START.header.synchByteA = headerSynchBytesArray[0];
            command_MAG_PRECALIB_START.header.synchByteB = headerSynchBytesArray[1];
            command_MAG_PRECALIB_START.header.synchByteC = headerSynchBytesArray[2];
            command_MAG_PRECALIB_START.header.payloadLength = COMMAND_MAG_PRECALIB_START_PAYLOAD_SIZE_IN_BYTES;
            command_MAG_PRECALIB_START.header.packetId = COMMAND_MAG_PRECALIB_START_ID;
            command_MAG_PRECALIB_START.payload.useLatitudeAndLongitudeValues = 0; //input text box of GUI
            command_MAG_PRECALIB_START.payload.initialLatitude_degrees = command_NAV_START.payload.initialLatitude_degrees; //input text box of GUI
            command_MAG_PRECALIB_START.payload.initialLongitude_degrees = command_NAV_START.payload.initialLongitude_degrees; //input text box of GUI
            command_MAG_PRECALIB_START.payload.useDeclinationAngleValue = 0; //input text box of GUI
            command_MAG_PRECALIB_START.payload.initialDeclinationAngle_degrees = 0; //input text box of GUI
            insertChecksum(ref command_MAG_PRECALIB_START);


            command_ACKNOWLEDGEMENT.header.synchByteA = headerSynchBytesArray[0];
            command_ACKNOWLEDGEMENT.header.synchByteB = headerSynchBytesArray[1];
            command_ACKNOWLEDGEMENT.header.synchByteC = headerSynchBytesArray[2];
            command_ACKNOWLEDGEMENT.header.payloadLength = COMMAND_ZERO_PAYLOAD_PAYLOAD_SIZE_IN_BYTES;
            command_ACKNOWLEDGEMENT.header.packetId = COMMAND_ACKNOWLEDGE_ID;
            insertChecksum(ref command_ACKNOWLEDGEMENT);

            command_GET_VERSION.header.synchByteA = headerSynchBytesArray[0];
            command_GET_VERSION.header.synchByteB = headerSynchBytesArray[1];
            command_GET_VERSION.header.synchByteC = headerSynchBytesArray[2];
            command_GET_VERSION.header.payloadLength = COMMAND_ZERO_PAYLOAD_PAYLOAD_SIZE_IN_BYTES;
            command_GET_VERSION.header.packetId = COMMAND_GET_VERSION_ID;
            insertChecksum(ref command_GET_VERSION);

            command_GET_STM32_UID.header.synchByteA = headerSynchBytesArray[0];
            command_GET_STM32_UID.header.synchByteB = headerSynchBytesArray[1];
            command_GET_STM32_UID.header.synchByteC = headerSynchBytesArray[2];
            command_GET_STM32_UID.header.payloadLength = COMMAND_ZERO_PAYLOAD_PAYLOAD_SIZE_IN_BYTES;
            command_GET_STM32_UID.header.packetId = COMMAND_GET_STM32_UID_ID;
            insertChecksum(ref command_GET_STM32_UID );

            command_NAV_STOP.header.synchByteA = headerSynchBytesArray[0];
            command_NAV_STOP.header.synchByteB = headerSynchBytesArray[1];
            command_NAV_STOP.header.synchByteC = headerSynchBytesArray[2];
            command_NAV_STOP.header.payloadLength = COMMAND_ZERO_PAYLOAD_PAYLOAD_SIZE_IN_BYTES;
            command_NAV_STOP.header.packetId = COMMAND_NAV_STOP_ID;
            insertChecksum(ref command_NAV_STOP);

            command_GET_IMU_SPECS.header.synchByteA = headerSynchBytesArray[0];
            command_GET_IMU_SPECS.header.synchByteB = headerSynchBytesArray[1];
            command_GET_IMU_SPECS.header.synchByteC = headerSynchBytesArray[2];
            command_GET_IMU_SPECS.header.payloadLength = COMMAND_ZERO_PAYLOAD_PAYLOAD_SIZE_IN_BYTES;
            command_GET_IMU_SPECS.header.packetId = COMMAND_GET_IMU_SPECS_ID;
            insertChecksum(ref command_GET_IMU_SPECS);

            command_GET_SENSORS_USED.header.synchByteA = headerSynchBytesArray[0];
            command_GET_SENSORS_USED.header.synchByteB = headerSynchBytesArray[1];
            command_GET_SENSORS_USED.header.synchByteC = headerSynchBytesArray[2];
            command_GET_SENSORS_USED.header.payloadLength = COMMAND_ZERO_PAYLOAD_PAYLOAD_SIZE_IN_BYTES;
            command_GET_SENSORS_USED.header.packetId = COMMAND_GET_SENSORS_USED_ID;
            insertChecksum(ref command_GET_SENSORS_USED);

            command_SET_IMU_SPECS.header.synchByteA = headerSynchBytesArray[0];
            command_SET_IMU_SPECS.header.synchByteB = headerSynchBytesArray[1];
            command_SET_IMU_SPECS.header.synchByteC = headerSynchBytesArray[2];
            command_SET_IMU_SPECS.header.payloadLength = COMMAND_SET_IMU_SPECS_PAYLOAD_SIZE_IN_BYTES ;
            command_SET_IMU_SPECS.header.packetId = COMMAND_SET_IMU_SPECS_ID;
            command_SET_IMU_SPECS.payload.valuesToUse = 0; //input text box of GUI
            command_SET_IMU_SPECS.payload.gyroscopeBiasInstability = 0.0; //input text box of GUI
            command_SET_IMU_SPECS.payload.gyroscopeAngularRandomWalk = 0.0; //input text box of GUI
            command_SET_IMU_SPECS.payload.gyroscopeBiasCorrelationTime = 0.0; //input text box of GUI
            command_SET_IMU_SPECS.payload.accelerometerBiasInstability = 0.0; //input text box of GUI
            command_SET_IMU_SPECS.payload.accelerometerVelocityRandomWalk = 0.0; //input text box of GUI
            command_SET_IMU_SPECS.payload.accelerometerBiasCorrelationTime = 0.0; //input text box of GUI
            insertChecksum(ref command_SET_IMU_SPECS);

            command_SET_SENSORS_USED.header.synchByteA = headerSynchBytesArray[0];
            command_SET_SENSORS_USED.header.synchByteB = headerSynchBytesArray[1];
            command_SET_SENSORS_USED.header.synchByteC = headerSynchBytesArray[2];
            command_SET_SENSORS_USED.header.payloadLength = COMMAND_SET_SENSORS_USED_PAYLOAD_SIZE_IN_BYTES;
            command_SET_SENSORS_USED.header.packetId = COMMAND_SET_SENSORS_USED_ID;
            command_SET_SENSORS_USED.payload.byteA  = 0x00;
            command_SET_SENSORS_USED.payload.byteB  = 0x00;
            insertChecksum(ref command_SET_SENSORS_USED);

            command_SET_OUTPUT_MESSAGES_TYPE.header.synchByteA = headerSynchBytesArray[0];
            command_SET_OUTPUT_MESSAGES_TYPE.header.synchByteB = headerSynchBytesArray[1];
            command_SET_OUTPUT_MESSAGES_TYPE.header.synchByteC = headerSynchBytesArray[2];
            command_SET_OUTPUT_MESSAGES_TYPE.header.payloadLength = COMMAND_SET_OUTPUT_MESSAGES_TYPE_PAYLOAD_SIZE_IN_BYTES ;
            command_SET_OUTPUT_MESSAGES_TYPE.header.packetId = COMMAND_SET_OUTPUT_MESSAGES_TYPE_ID ;
            command_SET_OUTPUT_MESSAGES_TYPE.payload.byteA = 0x00;
            insertChecksum(ref command_SET_OUTPUT_MESSAGES_TYPE);

            command_MAG_PRECALIB_STOP.header.synchByteA = headerSynchBytesArray[0];
            command_MAG_PRECALIB_STOP.header.synchByteB = headerSynchBytesArray[1];
            command_MAG_PRECALIB_STOP.header.synchByteC = headerSynchBytesArray[2];
            command_MAG_PRECALIB_STOP.header.payloadLength = COMMAND_ZERO_PAYLOAD_PAYLOAD_SIZE_IN_BYTES;
            command_MAG_PRECALIB_STOP.header.packetId = COMMAND_MAG_PRECALIB_STOP_ID;
            insertChecksum(ref command_MAG_PRECALIB_STOP);

            command_MAG_PRECALIB_RESET.header.synchByteA = headerSynchBytesArray[0];
            command_MAG_PRECALIB_RESET.header.synchByteB = headerSynchBytesArray[1];
            command_MAG_PRECALIB_RESET.header.synchByteC = headerSynchBytesArray[2];
            command_MAG_PRECALIB_RESET.header.payloadLength = COMMAND_ZERO_PAYLOAD_PAYLOAD_SIZE_IN_BYTES;
            command_MAG_PRECALIB_RESET.header.packetId = COMMAND_MAG_PRECALIB_RESET_ID;
            insertChecksum(ref command_MAG_PRECALIB_RESET);

            command_MAG_PRECALIB_TEST_START.header.synchByteA = headerSynchBytesArray[0];
            command_MAG_PRECALIB_TEST_START.header.synchByteB = headerSynchBytesArray[1];
            command_MAG_PRECALIB_TEST_START.header.synchByteC = headerSynchBytesArray[2];
            command_MAG_PRECALIB_TEST_START.header.payloadLength = COMMAND_MAG_PRECALIB_TEST_START_PAYLOAD_SIZE_IN_BYTES;
            command_MAG_PRECALIB_TEST_START.header.packetId = COMMAND_MAG_PRECALIB_TEST_START_ID;
            insertChecksum(ref command_MAG_PRECALIB_TEST_START);

            command_MAG_PRECALIB_TEST_STOP.header.synchByteA = headerSynchBytesArray[0];
            command_MAG_PRECALIB_TEST_STOP.header.synchByteB = headerSynchBytesArray[1];
            command_MAG_PRECALIB_TEST_STOP.header.synchByteC = headerSynchBytesArray[2];
            command_MAG_PRECALIB_TEST_STOP.header.payloadLength = COMMAND_ZERO_PAYLOAD_PAYLOAD_SIZE_IN_BYTES;
            command_MAG_PRECALIB_TEST_STOP.header.packetId = COMMAND_MAG_PRECALIB_TEST_STOP_ID;
            insertChecksum(ref command_MAG_PRECALIB_TEST_STOP);

            command_BIAS_SETUP_START.header.synchByteA = headerSynchBytesArray[0];
            command_BIAS_SETUP_START.header.synchByteB = headerSynchBytesArray[1];
            command_BIAS_SETUP_START.header.synchByteC = headerSynchBytesArray[2];
            command_BIAS_SETUP_START.header.payloadLength = COMMAND_ZERO_PAYLOAD_PAYLOAD_SIZE_IN_BYTES;
            command_BIAS_SETUP_START.header.packetId = COMMAND_BIAS_SETUP_START_ID;
            insertChecksum(ref command_BIAS_SETUP_START);

            command_BIAS_SETUP_STOP.header.synchByteA = headerSynchBytesArray[0];
            command_BIAS_SETUP_STOP.header.synchByteB = headerSynchBytesArray[1];
            command_BIAS_SETUP_STOP.header.synchByteC = headerSynchBytesArray[2];
            command_BIAS_SETUP_STOP.header.payloadLength = COMMAND_ZERO_PAYLOAD_PAYLOAD_SIZE_IN_BYTES;
            command_BIAS_SETUP_STOP.header.packetId = COMMAND_BIAS_SETUP_STOP_ID;
            insertChecksum(ref command_BIAS_SETUP_STOP);
        }

        public static byte[] structToByteArray<T> ( T structure ) where T : struct{

            //System.Diagnostics.Debug.WriteLine("T : " + typeof(T));

            int size = Marshal.SizeOf(structure);
            byte[] byteArray = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.StructureToPtr(structure, ptr, true);
            Marshal.Copy(ptr, byteArray, 0, size);
            Marshal.FreeHGlobal(ptr);

            return byteArray;
        }

        public static T byteArrayToStruct<T>( byte[] byteArray , T structureType ) where T:struct{

            T structure = new T() ;

            if (Marshal.SizeOf(structureType) != byteArray.Length) {
                return structure;
            }

            int size = Marshal.SizeOf(structure);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(byteArray, 0, ptr, size);

            structure = (T)Marshal.PtrToStructure(ptr, structureType.GetType());
            Marshal.FreeHGlobal(ptr);

            return structure;

        }
        //tariq Modify acces
        public static byte [] computeChecksum<T>(T structure) where T:struct {

            byte[] checksum = {0,0};
            byte[] byteArray = structToByteArray(structure);

            /*
            for (int iCount = headerSynchBytesArray.Length ; iCount < byteArray.Length - CHECKSUM_SIZE_IN_BYTES ; iCount++) {
                checksum[0] = (byte) (checksum[0] + byteArray[iCount]);
                checksum[1] = (byte) (checksum[1] + checksum[0]);
            }*/
            checksum = computeChecksum ( byteArray );

            return checksum;
        }

        private static byte [] computeChecksum ( byte[] byteArray )
        {

            byte[] checksum = { 0, 0 };

            for (int iCount = headerSynchBytesArray.Length; iCount < byteArray.Length - CHECKSUM_SIZE_IN_BYTES; iCount++)
            {
                checksum[0] = (byte) ((checksum[0] + byteArray[iCount]));
                checksum[1] = (byte) ((checksum[1] + checksum[0])      );
            }

            return checksum;
        }

        public static bool verifyChecksum<T>(T structure) where T : struct {

            byte[] byteArray = structToByteArray(structure);
            byte[] checksum = computeChecksum(structure);

            if ((checksum[0] == byteArray[byteArray.Length - 2])
                &&
                (checksum[1] == byteArray[byteArray.Length - 1])){

                return true;
            } else {
                return false;
            }

            //-- TODO : Should I Null the byteArray and checksum objectReferences.

        }

        public static bool verifyPacket<T>(T structure) where T : struct
        {
            byte[] byteArray = structToByteArray(structure);
            if (byteArray[0] == headerSynchBytesArray[0]
                &&
                byteArray[1] == headerSynchBytesArray[1]
                &&
                byteArray[2] == headerSynchBytesArray[2]
                &&
                verifyChecksum(structure)
                )
            {
                return true;
            }
            else 
            {
                return false;
            }

        }

        public static bool verifyPacket(byte[] byteArray)
        {
            if (byteArray[0] == headerSynchBytesArray[0]
                &&
                byteArray[1] == headerSynchBytesArray[1]
                &&
                byteArray[2] == headerSynchBytesArray[2]
                &&
                verifyChecksum(byteArray)
                )
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool verifyChecksum (byte [] byteArray)
        {

            byte[] checksum = computeChecksum(byteArray);

            if ((checksum[0] == byteArray[byteArray.Length - 2])
                &&
                (checksum[1] == byteArray[byteArray.Length - 1]))
            {

                return true;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("{0,4:X4},{1,4:X4}:{2,4:X4},{3,4:X4}",
                                                    byteArray[byteArray.Length - 2],
                                                    byteArray[byteArray.Length - 1],
                                                    checksum[0],
                                                    checksum[1]);
                return false;
            }

            //-- TODO : Should I Null the byteArray and checksum objectReferences.

        }

        public static void insertChecksum<T>(ref T structure) where T : struct {

            byte[] byteArray = structToByteArray(structure);

            byte[] checksum = computeChecksum(structure);

            byteArray[byteArray.Length - 2] = checksum[0];
            byteArray[byteArray.Length - 1] = checksum[1];

            structure = byteArrayToStruct(byteArray,structure);

        }

        public static void insertChecksum ( ref byte [] byteArray )
        {
            byte[] checksum = computeChecksum(byteArray);

            byteArray[byteArray.Length - 2] = checksum[0];
            byteArray[byteArray.Length - 1] = checksum[1];

        }

        public static void invalidatePacket<T>(ref T structure) where T : struct
        {
            byte[] byteArray = structToByteArray(structure);

            byteArray[0] = 0;
            byteArray[1] = 0;
            byteArray[2] = 0;

            structure = byteArrayToStruct(byteArray, structure);
        }

        //-- HEADERS

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Header
        {
            public byte  synchByteA ;
            public byte  synchByteB ;
            public byte  synchByteC ;
            public ushort payloadLength ;
            public ushort packetId ;
            
        } ;

        //-- COMMANDS

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Command_NAV_START
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public double initialLatitude_degrees;
                public double initialLongitude_degrees;
                public double initialAltitude_meters;
                public double initialHeading_degrees;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Command_MAG_PRECALIB_START
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public byte useLatitudeAndLongitudeValues;
                public double initialLatitude_degrees;
                public double initialLongitude_degrees;
                public byte useDeclinationAngleValue;
                public double initialDeclinationAngle_degrees;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Command_MAG_PRECALIB_TEST_START
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public byte useLatitudeAndLongitudeValues;
                public double initialLatitude_degrees;
                public double initialLongitude_degrees;
                public byte useDeclinationAngleValue;
                public double initialDeclinationAngle_degrees;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Command_SET_IMU_SPECS
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public byte valuesToUse;
                public double gyroscopeBiasInstability;
                public double gyroscopeAngularRandomWalk;
                public double gyroscopeBiasCorrelationTime;
                public double accelerometerBiasInstability;
                public double accelerometerVelocityRandomWalk;
                public double accelerometerBiasCorrelationTime;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Command_SET_SENSORS_USED
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public byte byteA;
                public byte byteB;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Command_SET_OUTPUT_MESSAGES_TYPE
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public byte byteA;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Command_ZERO_PAYLOAD
        {
            public Header header;
            public byte checksumA;
            public byte checksumB;

        };

        //-- RESPONSES

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Response_ACKNOWLEDGE
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public byte acknowledgeByte;
                public byte gpsConfigurationStatus;
                public byte sensorsConfigurationStatus;
                public byte sensorsUniqueIdVerificationStatus;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Response_STM32_UID
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public byte uniqueIdAvailable;
                public uint uniqueIdA;
                public uint uniqueIdB;
                public uint uniqueIdC;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Response_NAVIGATION
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
	            public byte status2;
	            public double GPSTimeTag;
	            public double Roll;//degrees
	            public double Pitch;//degrees
	            public double Azimuth;//degrees
	            public float  Roll_std;//degrees
	            public float Pitch_std;//degrees
	            public float Azimuth_std;//degrees
	            public double CorrectedGyroX;
	            public double CorrectedGyroY;
	            public double CorrectedGyroZ;
	            public float gyro_bias_err_x;
	            public float gyro_bias_err_y;
	            public float gyro_bias_err_z;
	            public double Latitude;//degrees
	            public double Longitute;//degrees
	            public double Altitude;//degrees
	            public float NorthPos_std;//meters
	            public float EastPos_std;//meters
	            public float DownPos_std;//meters
	            public float EastVel;//m/s
	            public float NorthVel;//m/s
	            public float DownVel;//m/s
	            public float CorrectedAccX;
	            public float CorrectedAccY;
	            public float CorrectedAccZ;
	            //double NorthVel_std;//m/s
	            //double EastVel_std;//m/s
	            //double DownVel_std;//m/s
	            //float Mag_Declination;//degrees
	            public byte status;
	            //double acc_bias_err_x;
	            //double acc_bias_err_y;
	            //double acc_bias_err_z;
                //#ifdef WRITE_DEBUG_INFO
	            //IMU_Data IMUData;
	            public double imuGPSTimeTag;
	            public byte    sensor_status;
	            public double gyro_x;                       // in rad/s    roll gyro    -- rightside-down positive
	            public double gyro_y;                       // in rad/s    pitch gyro   -- head-up positive
	            public double gyro_z;                       // in rad/s    heading gyro -- north to east (counterclockwise) positive
	            public double acc_x;                        // in m/s^2    longitudinal -- forward positive
	            public double acc_y;                        // in m/s^2    lateral      -- rightward positive
	            public double acc_z;                        // in m/s^2    vertical     -- down positive
	            public uint   imuSequenceID;
	            //GPS_Data GpsData;
	            public ushort   GPSweek;
	            public double GpsTimeTag_Pos;              // in sec
	            public double GpsTimeTag_Vel;              // in sec
	            public byte    gpsFixStatus;
	            public byte    gpsNumSV;
	            public double gpslat;                  // in rad
	            public double gpslon;                  // in rad
	            public double gpsalt;                  // in m
	            public double gpsv_north;              // in m/s
	            public double gpsv_east;
	            public double gpsv_up;
	            public double gpsPos_Std_north;           // in m
	            public double gpsPos_Std_east;
	            public double gpsPos_Std_up;
	            public float  gpsVel_Std_north;          // in m/s
	            public float  gpsVel_Std_east;
	            public float  gpsVel_Std_up;
                public float  gpsPDOP;
	            public float  gpsGDOP;
                public float  gpsVDOP;
	            public float  gpsHDOP;
	            public double M1;
	            public double M2;
	            public double Vf;
	            public uint   gpsSequenceID;

	            //MAG_Data MagData;
	            public double magGPSTimeTag;
	            public double Mag_x;                       // in Gauss
	            public double Mag_y;                       // in Gauss
	            public double Mag_z;                       // in Gauss
	            public double magHeading;
	            public double magHeadingstd;
	            public uint   magSequenceID;

	            //DA_Data  DaData;
	            public double daGPSTimeTag;
	            public double daHeading;                     // in rad
	            public double daHeadingStd;                  // in rad
	            public double daRoll;
	            public double daRollStd;
	            public double daPitch;
	            public double daPitchStd;
	            public uint   daSequenceID;

	            public float NorthVel_std;//m/s
	            public float EastVel_std;//m/s
	            public float DownVel_std;//m/s

	            public float acc_bias_err_x;
	            public float acc_bias_err_y;
	            public float acc_bias_err_z;

	            public float Mag_Declination;//degrees

	            //BARO_Data BaroData;
	            public double  	barGPSTimeTag;
	            public double 	barPressure;		//-- hpa [equivalent to mbar]
	            public double   	barTemperature;	//-- Degree Celsius
	            public double 	barHeight;			//-- meter
	            public double 	barHeightStd;		//-- meter
	            public uint   	barSequenceID;

	            public double EstimatedGyroBiasX;        // in deg/s
                public double EstimatedGyroBiasY;        // in deg/s
                public double EstimatedGyroBiasZ;        // in deg/s

	            public double EstimatedAccelBiasX;       // in m/s^2
                public double EstimatedAccelBiasY;       // in m/s^2
                public double EstimatedAccelBiasZ;       // in m/s^2

                public double speedGpsTimeTag;
                public double speedSensorSpeed;
                public byte speedReverse;
                public uint speedSequenceId;

                public short step;
                public double stepTime1;
                public double signalValue;
                public double strideLengthDistance;
                public double misalignment;
                public double leveledSignal;
                public double strideLengthVelocity;
                public double pdrLatitude;
                public double pdrLongitude;
                public double deviceHeading;
                public double pdrStrideLengthMethod1;

            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Response_MAG_PRECALIB
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public byte pointCollectionStatus;
                public uint numberOfPointsCollected;
                public uint numberOfSeconds;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Response_GET_IMU_SPECS
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public double gyroscopeBiasInstability;
                public double gyroscopeAngularRandomWalk;
                public double gyroscopeBiasCorrelationTime;
                public double accelerometerBiasInstability;
                public double accelerometerVelocityRandomWalk;
                public double accelerometerBiasCorrelationTime;                
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Response_GET_SENSORS_USED
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public byte byteA;
                public byte byteB;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Response_ACTIVITY
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public byte type;
                public byte mode;
                public uint sequenceId;
                public uint imuSequenceId;
                public uint gpsSequenceId;
                public uint magSequenceId;
                public uint baroSequenceId;
                public uint daSequenceId;
                public uint speedSequenceId;
                public byte bufferingIsActive;
                public byte magUpdateIndex;
                public byte baroUpdateIndex;
                public byte currentImuBufferSize;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Response_MAG_PRECALIB_PARAMETERS
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public byte parametersAreValid;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Response_MAG_PRECALIB_TEST_START
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public byte precalibrationParametersAreValid;
                public double magHeading_degrees;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Response_BIAS_SETUP_START
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public uint numberOfSeconds;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct Response_GET_VERSION
        {
            [StructLayout(LayoutKind.Sequential, Pack = 1)]
            public struct Payload
            {
                public ushort tpp_system;
                public byte application_version_major;
                public byte application_version_minor;
                public byte application_version_patch;
                public byte application_version_patch_internal;
                public double reserved_1_;
                public double reserved_2_;
            };

            public Header header;
            public Payload payload;
            public byte checksumA;
            public byte checksumB;

        };
    }
}
