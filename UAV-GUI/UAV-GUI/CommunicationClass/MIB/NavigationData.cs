﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAV_GUI
{
   public  class NavigationData
    {
        
        public static double timeTag;

        public static double roll;
        public static double pitch;
        public static double azimuth;
        
        public static float rollStd;
        public static float pitchStd;
        public static float azimuthStd;
        
        public static double latitude;
        public static double longitude; 
        public static double altitude;

        public static float nPStd;
        public static float ePStd;
        public static float dPStd;

        public static float eastVelocity;
        public static float northVelocity;
        public static float downVelocity;

        public static byte statusB; 


    }
}
