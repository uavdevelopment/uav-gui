﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace UAV_GUI
{
    [Serializable()] 
    public class InitParams : ISerializable
    {
        public double latitude;
        public double longitude;
        public double altitude;
        public double heading;
        public double accel_bias_instability;
        public double accel_vrw;
        public double accel_bias_correlation_time;
        public double gyro_bias_instability;
        public double gyro_arw;
        public double gyro_bias_correlation_time;
        public bool bChkbxAdvSetEnabled;
        public bool bChkbxMagEnabled;
        public bool bChkbxBaroEnabled;

        public InitParams()
        {
            latitude = 51.0834;
            longitude = -114.1309;
            altitude = 1000.0;
            heading = 0.0;
            accel_bias_instability = 500;
            accel_vrw = 0.1;
            accel_bias_correlation_time = 1.0;
            gyro_bias_instability = 15;
            gyro_arw = 2.0;
            gyro_bias_correlation_time = 1.0;
            bChkbxAdvSetEnabled = false;
            bChkbxBaroEnabled = true;
            bChkbxMagEnabled = false;
        }

		//Deserialization constructor.
        public InitParams(SerializationInfo info, StreamingContext ctxt)
        {
	        //Get the values from info and assign them to the appropriate properties
            latitude = (double)info.GetValue("Latitude", typeof(double));
            longitude = (double)info.GetValue("Longitude", typeof(double));
            altitude = (double)info.GetValue("Altitude", typeof(double));
            heading = (double)info.GetValue("Heading", typeof(double));
            accel_bias_instability = (double)info.GetValue("AccelBiasInstability", typeof(double));
            accel_vrw = (double)info.GetValue("AccelVRW", typeof(double));
            accel_bias_correlation_time = (double)info.GetValue("AccelBiasCorrelationTime", typeof(double));
            gyro_bias_instability = (double)info.GetValue("GyroBiasInstability", typeof(double));
            gyro_arw = (double)info.GetValue("GyroARW", typeof(double));
            gyro_bias_correlation_time = (double)info.GetValue("GyroBiasCorrelationTime", typeof(double));
            bChkbxAdvSetEnabled = (bool)info.GetValue("CheckBoxAdvancedSettings", typeof(bool));
            bChkbxBaroEnabled = (bool)info.GetValue("CheckBoxBaroEnabled", typeof(bool));
            bChkbxMagEnabled = (bool)info.GetValue("CheckBoxMagEnabled", typeof(bool));
        }
		
        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("Latitude", latitude);
            info.AddValue("Longitude", longitude);
            info.AddValue("Altitude", altitude);
            info.AddValue("Heading", heading);
            info.AddValue("AccelBiasInstability", accel_bias_instability);
            info.AddValue("AccelVRW", accel_vrw);
            info.AddValue("AccelBiasCorrelationTime", accel_bias_correlation_time);
            info.AddValue("GyroBiasInstability", gyro_bias_instability);
            info.AddValue("GyroARW", gyro_arw);
            info.AddValue("GyroBiasCorrelationTime", gyro_bias_correlation_time);
            info.AddValue("CheckBoxAdvancedSettings", bChkbxAdvSetEnabled);
            info.AddValue("CheckBoxBaroEnabled", bChkbxBaroEnabled);
            info.AddValue("CheckBoxMagEnabled", bChkbxMagEnabled);
        }
    }
}
