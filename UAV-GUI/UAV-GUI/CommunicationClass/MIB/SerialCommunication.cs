﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.IO;
using Error_Reporting;
using GLobal;
using FileClasses.INI_Files;
namespace UAV_GUI
{
    public class SerialCommunication
    {
        public SerialPort serialPortObject = new SerialPort();
        private bool isPortOpen;
        int numberOfBytesRead;
        CommunicationPackets.Header header = new CommunicationPackets.Header();
        byte[] packetByteArray;
        int counter = 0;
        Thread readDataThread;
        bool quitReadThread = false;

        public void closeReadThread(){
            readDataThread.Abort();
            readDataThread = null;
        }
        public SerialCommunication( string serialPortName )
        {
            if (OpenPort(serialPortName))
            {
                SetupDeviceParametersForMTNCommunication();

            }
            else { throw new BusinessException(1511, "unab;e to open the Com Port"); }
           
        }

        public SerialCommunication(string serialPortName,int baudRate)
        {
            if (OpenPort(serialPortName))
            {
                SetupDeviceParametersForMTNCommunication(baudRate);

            }
            else { throw new BusinessException(1511, "unab;e to open the Com Port"); }

        }

        public SerialCommunication(int serialPortNumber) : this("COM" + serialPortNumber)
        {}

        public uint Write(byte[] byteArray)
        {
            UInt32 numBytesWritten = 1;
            serialPortObject.Write(byteArray, 0,byteArray.Length);
            return numBytesWritten;
        }

        public uint Write<T>(T structure) where T : struct
        {
            byte[] byteArray = CommunicationPackets.structToByteArray(structure);
            return ( Write( byteArray ) );
        }

        public void Close()
        {
            try
            {
                serialPortObject.Close();
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        public bool isPortOpened()
        {
            return serialPortObject.IsOpen;
        }

        public bool OpenPort(string serialPortName)
        {
            try {
            
            /*if (serialPortObject.IsOpen)
            {
                
                return true;
            }*/
           // else
           // {
                serialPortObject = new SerialPort(serialPortName);
                //System.Windows.Forms.MessageBox.Show("sssss"+serialPortName);
                return true;
            //}
            }catch (Exception e){
                GlobalData.LogConsole(e.StackTrace);
                return false;            
            }
        }

        public bool OpenPort(int serialPortNumber) 
        {
            return OpenPort("COM" + serialPortNumber);
        }

        public bool SetupDeviceParametersForMTNCommunication(int baudrate=-1)
        {
            //-- Set up device data parameters --//
           if (serialPortObject == null)
            {
                return false;
            }

           try { 
            //-- Set Baud rate
            serialPortObject.BaudRate=(baudrate == -1 ?(int)INIData.baudRate:baudrate);
            
            //-- Set data characteristics - Data bits, Stop bits, Parity
            serialPortObject.StopBits= StopBits.One;
            serialPortObject.Parity= Parity.None; 
            serialPortObject.DataBits = 8 ; 
            
            //-- Set read timeout to 2 seconds, write timeout to 1 second
            serialPortObject.ReadTimeout = 3000; 
            serialPortObject.WriteTimeout=2000;
            serialPortObject.ReadBufferSize = 4096;
            GlobalData.LogConsole(serialPortObject.ReadBufferSize);
            serialPortObject.Open();
             }catch (Exception e ){
                return false ; 
            }
            return true;
        }

        public bool Read(byte[] responseBytes,int offset_, int length, ref int numBytesRead)
        {
            numBytesRead = length;
            int offset = offset_, bytesRead;
            while (length > 0 &&
              (bytesRead = serialPortObject.Read(responseBytes, offset, length)) > 0)
            {
                offset += bytesRead;
                length -= bytesRead;
            }
            return length == 0;
        }
        public bool Read_(byte[] byteArray, int offset, int length , ref int numBytesRead)
        {
            numBytesRead = 0;
            
            
            if ( length > 0)
            {
                numBytesRead = serialPortObject.Read(byteArray, (int)offset, (int)length);
                return true;
            }
            else {
                return false; 
            }

        }

        public int Read_(byte[] byteArray)
        {
            int numBytesRead = 0;
            bool ftdiStatus = false;
            try
            {
                if (byteArray == null)
                {
                    return numBytesRead;
                }
                else
                {
                    numBytesRead = serialPortObject.Read(byteArray, 0, (int)byteArray.Length);
                    GlobalData.LogConsole("Reading, ,, " +byteArray.Length+"   " + numBytesRead);
                }
            }
            catch (Exception e) {
                return numBytesRead;
            
            }
            return numBytesRead;
        }
        public int Read(byte[] byteArray)
        {
            int numBytesRead = 0;
            bool ftdiStatus = false;
            try
            {
                if (byteArray == null)
                {
                    return numBytesRead;
                }
                else
                {
                   // numBytesRead = serialPortObject.Read(byteArray, 0, (int)byteArray.Length);
                   bool test =  Read(byteArray, 0, (int)byteArray.Length, ref numBytesRead);
                   GlobalData.LogConsole("Reading, ,, " + byteArray.Length + "   " + test + " " + numBytesRead);
                }
            }
            catch (Exception e)
            {
                GlobalData.LogConsole(e.Message+"Error in Serial Port Settings");
                return numBytesRead;

            }
            return numBytesRead;
        }

        public int Read<T>(ref T structure) where T : struct
        {
            byte[] byteArray = CommunicationPackets.structToByteArray(structure);
            int numberOfBytesRead = Read(byteArray);
            structure = CommunicationPackets.byteArrayToStruct(byteArray, structure);
            return numberOfBytesRead;
        }

        public void ResetPort()
        {
            Thread.Sleep(1000);
           serialPortObject.Close();
           Thread.Sleep(100);
           serialPortObject.Open();
           
        }
        
        public void StartReadThread(BinaryWriter rawDataBinaryFileObject, BinaryWriter activityBinaryFileObject)
        {
            readDataThread = new Thread(() => ReadPackets(rawDataBinaryFileObject, activityBinaryFileObject));
            readDataThread.Start();
        }

        private void ReadPackets(BinaryWriter rawDataBinaryFileObjectReference, BinaryWriter activityBinaryFileObjectReference)
        {
            byte[] RESPONSE_HEADER_byteArray = new byte[CommunicationPackets.HEADER_SIZE_IN_BYTES];
            byte[] RESPONSE_NAVIGATION_byteArray = new byte[CommunicationPackets.RESPONSE_NAVIGATION_PACKET_SIZE_IN_BYTES];
            byte[] RESPONSE_ACTIVITY_byteArray = new byte[CommunicationPackets.RESPONSE_ACTIVITY_PACKET_SIZE_IN_BYTES];
            CommunicationPackets.Header header = new CommunicationPackets.Header();
            int SenT = 0;
            Int32 numBytesRead = 0;
            int counter = 0;
            bool ftdiStatus;

            //for(;;)
            while (quitReadThread == false)
            {
                ftdiStatus = this.Read(RESPONSE_HEADER_byteArray, 0,(int)RESPONSE_HEADER_byteArray.Length, ref numBytesRead);
                header = CommunicationPackets.byteArrayToStruct(RESPONSE_HEADER_byteArray, header);

                if (header.packetId == CommunicationPackets.RESPONSE_NAVIGATION_ID)
                {
                   // GlobalData.LogConsole("Navigation...........................");
                    Array.Copy(RESPONSE_HEADER_byteArray, RESPONSE_NAVIGATION_byteArray, CommunicationPackets.HEADER_SIZE_IN_BYTES);
                    ftdiStatus = Read(RESPONSE_NAVIGATION_byteArray, (int)CommunicationPackets.HEADER_SIZE_IN_BYTES, (int)(RESPONSE_NAVIGATION_byteArray.Length - RESPONSE_HEADER_byteArray.Length), ref numBytesRead);

                    //ftdiStatus = ftdiDevice.Read(RESPONSE_NAVIGATION_byteArray, (uint)RESPONSE_NAVIGATION_byteArray.Length, ref numBytesRead);


                    if (ftdiStatus != true)
                    {
                        // Wait for a key press
                        GlobalData.LogConsole("Failed to read data (error " + ftdiStatus.ToString() + ")");
                        return;
                    }

                    if (ftdiStatus == true && numBytesRead == 0)
                    {
                        GlobalData.LogConsole("[INFO] Timeout");
                    }

                    if (CommunicationPackets.verifyPacket(RESPONSE_NAVIGATION_byteArray) == true)
                    {
                        GlobalData.LogConsole("[INFO] Checksum is correct");
                        string res1 = String.Join(" ", RESPONSE_NAVIGATION_byteArray);
                        res1 += "\n";
                        //System.IO.File.AppendAllText("D:\\StopNavigation2.txt", res1);
                        //GlobalData.LogConsole(res1);
                        if (rawDataBinaryFileObjectReference == null)
                        { }
                        else
                        {
                            rawDataBinaryFileObjectReference.Write(RESPONSE_NAVIGATION_byteArray, CommunicationPackets.HEADER_SIZE_IN_BYTES, CommunicationPackets.RESPONSE_NAVIGATION_PAYLOAD_SIZE_IN_BYTES);
                        }
                    }
                    else
                    {
                       // string res1 = String.Join(" ", RESPONSE_NAVIGATION_byteArray);
                       // res1 += "\n";
                       // GlobalData.LogConsole("StopNavigation1.txt"+res1);
                        //GlobalData.LogConsole(res1);
                        //GlobalData.LogConsole("[ERRR] Checksum is WRONG");
                        continue;
                    }

                    CommunicationPackets.response_NAVIGATION = CommunicationPackets.byteArrayToStruct(RESPONSE_NAVIGATION_byteArray,
                                                                                  CommunicationPackets.response_NAVIGATION);
                    if ((((int)CommunicationPackets.response_NAVIGATION.payload.status2 & 0x01) == 0x01)
                         &&
                      CommunicationPackets.response_NAVIGATION.payload.GPSTimeTag > 2 && SenT >= 0)
                    {
                        GlobalData.LogConsole("GPS Fix  " + CommunicationPackets.response_NAVIGATION.payload.Latitude);
                        NavStartData.currentLat = CommunicationPackets.response_NAVIGATION.payload.Latitude;
                        NavStartData.currentLong = CommunicationPackets.response_NAVIGATION.payload.Longitute;
                        NavStartData.GPSFix = true;
                        NavStartData.toMP = false;
                        

                        GlobalData.LogConsole("GPS Fix  " + (Int32)(BitConverter.ToDouble(RESPONSE_NAVIGATION_byteArray, 8)) );
                       // System.IO.File.AppendAllText(@"D:\tst", CommunicationPackets.response_NAVIGATION.payload.Longitute + " , " + CommunicationPackets.response_NAVIGATION.payload.Latitude+"\r\n");
                        byte[] mySwitch = { 84, 80, 73, 0, 0, 0x0e, 0x0e, 14, 28 };

                        if (SenT == 25) {
                            //this.Write(mySwitch);
                            //Thread.Sleep(500);
                            this.Write(mySwitch);
                            SenT = 26;
                            this.Close();
                            NavStartData.toMP = true;
                            quitReadThread = true;
                            System.Windows.Forms.MessageBox.Show("System is now navigating Now", "Info");
                        }
                        SenT++;
                       
                    }
                    else {
                        GlobalData.LogConsole("GPS No No Fix");
                        NavStartData.currentLat = 0;
                        NavStartData.currentLong = 0;
                        NavStartData.GPSFix = false;
                        NavStartData.toMP = false;
                    }
                    /* if (CommunicationPackets.response_NAVIGATION.payload.GPSTimeTag > 30 && SenT == 0) {
                        byte[]  mySwitch = { 84, 80, 73, 0, 0, 0x0e, 0x0e, 14, 28 };
                       // for (int i = 0; i < 25;i++ )
                        this.Write(mySwitch);
                        Thread.Sleep(500);
                        this.Write(mySwitch);
                        SenT = 1;
                    }*/
                    counter++;
                    if ((((int)CommunicationPackets.response_NAVIGATION.payload.status2 & 0x20) == 0x20))
                    {
                        counter = 2;
                        GlobalData.LogConsole("[INFO] OBDII Speed is available");
                    }

                    if (counter % 100 == 1 || (((int)CommunicationPackets.response_NAVIGATION.payload.status2 & 0x20) == 0x20))
                    {
                        UInt32 numberOfBytesAvailable = 0;
                       numBytesRead = serialPortObject.BytesToRead;
                        GlobalData.LogConsole("[INFO] Number Of Bytes Available in the Serial Port Buffer : " + numberOfBytesAvailable +
                                          ",Packet Counter : " + counter +
                                          ",Number Of Bytes Read : " + numBytesRead + "\n");
                        CommunicationPackets.response_NAVIGATION = CommunicationPackets.byteArrayToStruct(RESPONSE_NAVIGATION_byteArray,
                                                                                                          CommunicationPackets.response_NAVIGATION);

                        GlobalData.LogConsole("response received");
                        //counter = 0;
                    }

                }
                else if (header.packetId == CommunicationPackets.RESPONSE_ACTIVITY_ID)
                {
                    GlobalData.LogConsole("ACtivity");
                    Array.Copy(RESPONSE_HEADER_byteArray, RESPONSE_ACTIVITY_byteArray, CommunicationPackets.HEADER_SIZE_IN_BYTES);
                    ftdiStatus = Read(RESPONSE_ACTIVITY_byteArray, (int)CommunicationPackets.HEADER_SIZE_IN_BYTES, (int)(RESPONSE_ACTIVITY_byteArray.Length - RESPONSE_HEADER_byteArray.Length), ref numBytesRead);

                    if (CommunicationPackets.verifyPacket(RESPONSE_ACTIVITY_byteArray) == true)
                    {
                        //GlobalData.LogConsole("[INFO] Checksum is correct");
                        if (activityBinaryFileObjectReference == null)
                        { }
                        else
                        {
                            activityBinaryFileObjectReference.Write(RESPONSE_ACTIVITY_byteArray, CommunicationPackets.HEADER_SIZE_IN_BYTES, CommunicationPackets.RESPONSE_ACTIVITY_PAYLOAD_SIZE_IN_BYTES);
                        }
                    }
                    else
                    {
                        GlobalData.LogConsole("[ERRR] Checksum is WRONG");
                        continue;
                    }
                }
                else {
                  //  GlobalData.LogConsole("Not Registry Not Id ");
                
                }

                //GlobalData.LogConsole("[INFO] Number of data bytes read = " + numBytesRead);


            }

        }

            }

        }

