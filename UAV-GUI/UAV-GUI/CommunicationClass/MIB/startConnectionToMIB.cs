﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using System.Windows.Forms;
using FileClasses.INI_Files;
using GLobal;

namespace UAV_GUI
{
    class startConnectionToMIB
    {
        InitParams initialParametersObjectReference;
     
        public SerialCommunication serialCommunicationChannel;
        Thread oThread;
        byte[] command_Stop_Nav;
        public Setup formSetup_; 

        public startConnectionToMIB(Setup formSetup) {
            formSetup_ = formSetup;
            CommunicationPackets.initialize();
        }
        public startConnectionToMIB()
        {
          
            CommunicationPackets.initialize();
        }

        public void FormBiasSetupInitialization() {
            using (FormBiasSetup dialog = new FormBiasSetup(serialCommunicationChannel))
            {
                DialogResult result = dialog.ShowDialog();

                switch (result)
                {
                    case DialogResult.OK:
                        break;
                    case DialogResult.Cancel:
                        return;
                    default:
                        return;
                }

            }
        
        }

        public void FormMagCalibInitialization()
        {
            using (FormMagCalibration dialog = new FormMagCalibration(serialCommunicationChannel))
            {
                DialogResult result = dialog.ShowDialog();

                switch (result)
                {
                    case DialogResult.OK:
                        break;
                    case DialogResult.Cancel:
                        return;
                    default:
                        return;
                }

            }

        }

        public int StartNavigationForm() {
            
                // Open Initial Parameters Dialog Box
            initialParametersObjectReference = new InitParams();
                using (FormInitParams dialog = new FormInitParams(initialParametersObjectReference))
                {
                    DialogResult result = dialog.ShowDialog();

                    switch (result)
                    {
                        case DialogResult.OK:

                            CommunicationPackets.command_NAV_START.payload.initialLatitude_degrees = dialog.initParams.latitude;
                            CommunicationPackets.command_NAV_START.payload.initialLongitude_degrees = dialog.initParams.longitude;
                            CommunicationPackets.command_NAV_START.payload.initialAltitude_meters = dialog.initParams.altitude;
                            if (dialog.useHeadingCheckBoxValue == true)
                            {
                                CommunicationPackets.command_NAV_START.payload.initialHeading_degrees = dialog.initParams.heading;
                            }
                            else
                            {
                                CommunicationPackets.command_NAV_START.payload.initialHeading_degrees = -1010101.1010 * 180.0 / (3.14159265358979);
                            }
                            CommunicationPackets.insertChecksum(ref CommunicationPackets.command_NAV_START);

                            CommunicationPackets.command_SET_IMU_SPECS.payload.accelerometerBiasInstability = dialog.initParams.accel_bias_instability;
                            CommunicationPackets.command_SET_IMU_SPECS.payload.accelerometerVelocityRandomWalk = dialog.initParams.accel_vrw;
                            CommunicationPackets.command_SET_IMU_SPECS.payload.accelerometerBiasCorrelationTime = dialog.initParams.accel_bias_correlation_time;

                            CommunicationPackets.command_SET_IMU_SPECS.payload.gyroscopeBiasInstability = dialog.initParams.gyro_bias_instability;
                            CommunicationPackets.command_SET_IMU_SPECS.payload.gyroscopeAngularRandomWalk = dialog.initParams.gyro_arw;
                            CommunicationPackets.command_SET_IMU_SPECS.payload.gyroscopeBiasCorrelationTime = dialog.initParams.gyro_bias_correlation_time;
                            CommunicationPackets.insertChecksum(ref CommunicationPackets.command_SET_IMU_SPECS);

                            CommunicationPackets.command_SET_SENSORS_USED.payload.byteA = 0x00;
                            CommunicationPackets.command_SET_SENSORS_USED.payload.byteA |= (byte)((dialog.initParams.bChkbxMagEnabled) ? 0x04 : 0x00);
                            CommunicationPackets.command_SET_SENSORS_USED.payload.byteA |= (byte)((dialog.initParams.bChkbxBaroEnabled) ? 0x08 : 0x00);
                            CommunicationPackets.command_SET_SENSORS_USED.payload.byteB = 0x00;
                            CommunicationPackets.insertChecksum(ref CommunicationPackets.command_SET_SENSORS_USED);
                           break;
                         
                        case DialogResult.Cancel:
                            return -1;
                        default:
                            return -1 ;
                    }

                }
              //  byte[] d = {0x54 ,0x50 ,0x49 ,0x00 , 0x00 , 0x0E ,0x0E ,14 ,28};
            //serialCommunicationChannel.Write(d);
            //Thread.Sleep(20);
            serialCommunicationChannel.Write(CommunicationPackets.command_SET_IMU_SPECS);
            Thread.Sleep(20);
            serialCommunicationChannel.Write(CommunicationPackets.command_SET_SENSORS_USED);
            serialCommunicationChannel.ResetPort();
            Thread.Sleep(20);
            //-- Sending "Start Navigation" command to the system
            serialCommunicationChannel.Write(CommunicationPackets.command_NAV_START);
            Thread.Sleep(25);
            serialCommunicationChannel.StartReadThread(null, null);
            return 0;
         
        
        
        }

        public void startConnectionWithMIB()
        {
            
            /*initialize Packets */
             if( formSetup_.connectButton.Text.Equals("Connect") )
            {
               
                {
                    if (serialCommunicationChannel != null && serialCommunicationChannel.isPortOpened() == true)
                    {
                        serialCommunicationChannel.Close();
                        serialCommunicationChannel = null;
                        Thread.Sleep(150);
                    }

                    try
                    {
                        serialCommunicationChannel = new SerialCommunication(INIData.compPort);
                        
                    }
                    catch (Exception exception)
                    {
                        String ErrTitle = "COM Error";
                        String ErrMsg = "Unable to open connection to COM Port.";
                        GlobalData.LogConsole(exception.Message);
                        MessageBox.Show(ErrMsg, ErrTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                   initialParametersObjectReference = new InitParams();

                    //-- Sending stop command
                    serialCommunicationChannel.Write(CommunicationPackets.command_NAV_STOP);
                    serialCommunicationChannel.ResetPort();
                    Thread.Sleep(20);
                    //-- Acknowleding System
                    serialCommunicationChannel.Write(CommunicationPackets.command_ACKNOWLEDGEMENT);
                    Thread.Sleep(20);
                    serialCommunicationChannel.Read(ref CommunicationPackets.response_ACKNOWLEDGE);
                    if (CommunicationPackets.verifyPacket(CommunicationPackets.response_ACKNOWLEDGE))
                    {
                        GlobalData.LogConsole("[INFO] ACK Packet Response checksum verified");
                        if (CommunicationPackets.response_ACKNOWLEDGE.payload.acknowledgeByte == 0x07)
                        {
                            MessageBox.Show("Invalid Acknowlegment from the System", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            if (CommunicationPackets.response_ACKNOWLEDGE.payload.gpsConfigurationStatus == 0x00)
                            {
                                MessageBox.Show("GNSS receiver is not configured properly", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            else if (CommunicationPackets.response_ACKNOWLEDGE.payload.sensorsConfigurationStatus == 0x00)
                            {
                                MessageBox.Show("SENSORS are not configured properly", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            else if (CommunicationPackets.response_ACKNOWLEDGE.payload.sensorsConfigurationStatus == 0x02)
                            {
                                MessageBox.Show("Invalid OBDII connection for an OBDII-based system", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            else if (CommunicationPackets.response_ACKNOWLEDGE.payload.sensorsUniqueIdVerificationStatus == 0x00)
                            {
                                MessageBox.Show("Unit Verification failed. Please contact Trusted Positioning Inc. for more information", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            else 
                            {
                                return;
                            }
                        }
                        else if (CommunicationPackets.response_ACKNOWLEDGE.payload.acknowledgeByte == 0x06)
                        {
                        }
                        CommunicationPackets.invalidatePacket(ref CommunicationPackets.response_ACKNOWLEDGE);
                    }
                    else
                    {
                        MessageBox.Show("Invalid Acknowledgement F     rom System", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    //-- Grabbing STM32-UID on board.
                    serialCommunicationChannel.Write(CommunicationPackets.command_GET_STM32_UID);
                    Thread.Sleep(20);
                    serialCommunicationChannel.Read(ref CommunicationPackets.response_STM32_UID);

                    if (CommunicationPackets.verifyPacket(CommunicationPackets.response_STM32_UID))
                    {}

                    //-- Sending "TEST commands" command to the system
                    serialCommunicationChannel.Write(CommunicationPackets.command_GET_IMU_SPECS);
                    Thread.Sleep(20);
                    serialCommunicationChannel.Read(ref CommunicationPackets.response_GET_IMU_SPECS);

                    if (CommunicationPackets.verifyPacket(CommunicationPackets.response_GET_IMU_SPECS))
                    {
                        GlobalData.LogConsole("[INFO] GET-IMU-SPECS Packet Response verified");

                        initialParametersObjectReference.gyro_bias_instability = CommunicationPackets.response_GET_IMU_SPECS.payload.gyroscopeBiasInstability;
                        initialParametersObjectReference.gyro_arw = CommunicationPackets.response_GET_IMU_SPECS.payload.gyroscopeAngularRandomWalk;
                        initialParametersObjectReference.gyro_bias_correlation_time = CommunicationPackets.response_GET_IMU_SPECS.payload.gyroscopeBiasCorrelationTime;
                        initialParametersObjectReference.accel_bias_instability = CommunicationPackets.response_GET_IMU_SPECS.payload.accelerometerBiasInstability;
                        initialParametersObjectReference.accel_vrw = CommunicationPackets.response_GET_IMU_SPECS.payload.accelerometerVelocityRandomWalk;
                        initialParametersObjectReference.accel_bias_correlation_time = CommunicationPackets.response_GET_IMU_SPECS.payload.accelerometerBiasCorrelationTime;

                        CommunicationPackets.invalidatePacket(ref CommunicationPackets.response_GET_IMU_SPECS);
                    }
                    else
                    {
                        MessageBox.Show("Invalid Response (GET-IMU-SPECS) From Navigator", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    serialCommunicationChannel.Write(CommunicationPackets.command_GET_SENSORS_USED);
                    Thread.Sleep(20);

                    serialCommunicationChannel.Read(ref CommunicationPackets.response_GET_SENSORS_USED);

                    if (CommunicationPackets.verifyPacket(CommunicationPackets.response_GET_SENSORS_USED))
                    {
                        GlobalData.LogConsole("[INFO] GET-SENSORS-USED Packet Response verified");
                        initialParametersObjectReference.bChkbxMagEnabled = (CommunicationPackets.response_GET_SENSORS_USED.payload.byteA & 0x04) == 0x04;
                        initialParametersObjectReference.bChkbxBaroEnabled = (CommunicationPackets.response_GET_SENSORS_USED.payload.byteA & 0x08) == 0x08;

                        CommunicationPackets.invalidatePacket(ref CommunicationPackets.response_GET_SENSORS_USED);
                    }
                    else
                    {
                        MessageBox.Show("Invalid Response (GET-SENSORS-USED) From Navigator", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                  
           
                    formSetup_.button6.Enabled = true;
                    formSetup_.btnStartNav.Enabled = true;
                    formSetup_.button4.Enabled = true;
               formSetup_.connectButton.Text = "Disconnect";
                
            }
           }
            else 
            {
             
                serialCommunicationChannel.Close();
                formSetup_.button6.Enabled = false;
                formSetup_.btnStartNav.Enabled = false;
                formSetup_.button4.Enabled = false;
                formSetup_.connectButton.Text = "Connect";
              
            }
        }

        public int AutomaticStartConnectionWithMIB(UAV_GUI.Forms.AutoConnectWizard myWizard)
        {

            /*initialize Packets */
         
               
                    if (serialCommunicationChannel != null && serialCommunicationChannel.isPortOpened() == true)
                    {
                        serialCommunicationChannel.Close();
                        serialCommunicationChannel = null;
                    }

                    try
                    {
                        serialCommunicationChannel = new SerialCommunication(INIData.compPort);

                    }
                    catch (Exception exception)
                    {
                        String ErrTitle = "COM Error";
                        String ErrMsg = "Unable to open connection to COM Port.";
                        GlobalData.LogConsole(exception.Message);
                        MessageBox.Show(ErrMsg, ErrTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return -1 ;
                    }

                    initialParametersObjectReference = new InitParams();

                    //-- Sending stop command
                    serialCommunicationChannel.Write(CommunicationPackets.command_NAV_STOP);
                    serialCommunicationChannel.ResetPort();
                    Thread.Sleep(20);
                    //-- Acknowleding System
                    serialCommunicationChannel.Write(CommunicationPackets.command_ACKNOWLEDGEMENT);
                    Thread.Sleep(20);
                    serialCommunicationChannel.Read(ref CommunicationPackets.response_ACKNOWLEDGE);
                    if (CommunicationPackets.verifyPacket(CommunicationPackets.response_ACKNOWLEDGE))
                    {
                        GlobalData.LogConsole("[INFO] ACK Packet Response checksum verified");
                        myWizard.addTextTothLogger("[INFO] ACK Packet Response checksum verified"); 
                        if (CommunicationPackets.response_ACKNOWLEDGE.payload.acknowledgeByte == 0x07)
                        {
                            MessageBox.Show("Invalid Acknowlegment from the System", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                           
                            if (CommunicationPackets.response_ACKNOWLEDGE.payload.gpsConfigurationStatus == 0x00)
                            {
                                MessageBox.Show("GNSS receiver is not configured properly", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return -1 ;
                            }
                            else if (CommunicationPackets.response_ACKNOWLEDGE.payload.sensorsConfigurationStatus == 0x00)
                            {
                                MessageBox.Show("SENSORS are not configured properly", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return -1 ;
                            }
                            else if (CommunicationPackets.response_ACKNOWLEDGE.payload.sensorsConfigurationStatus == 0x02)
                            {
                                MessageBox.Show("Invalid OBDII connection for an OBDII-based system", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return -1;
                            }
                            else if (CommunicationPackets.response_ACKNOWLEDGE.payload.sensorsUniqueIdVerificationStatus == 0x00)
                            {
                                MessageBox.Show("Unit Verification failed. Please contact Trusted Positioning Inc. for more information", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return -1 ;
                            }
                            else
                            {
                                return -1;
                            }
                        }
                        else if (CommunicationPackets.response_ACKNOWLEDGE.payload.acknowledgeByte == 0x06)
                        {
                        }
                        CommunicationPackets.invalidatePacket(ref CommunicationPackets.response_ACKNOWLEDGE);
                    }
                    else
                    {
                        MessageBox.Show("Invalid Acknowledgement F     rom System", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return -1 ;
                    }

                    //-- Grabbing STM32-UID on board.
                    serialCommunicationChannel.Write(CommunicationPackets.command_GET_STM32_UID);
                    Thread.Sleep(20);
                    serialCommunicationChannel.Read(ref CommunicationPackets.response_STM32_UID);

                    if (CommunicationPackets.verifyPacket(CommunicationPackets.response_STM32_UID))
                    { }

                    //-- Sending "TEST commands" command to the system
                    serialCommunicationChannel.Write(CommunicationPackets.command_GET_IMU_SPECS);
                    Thread.Sleep(20);
                    serialCommunicationChannel.Read(ref CommunicationPackets.response_GET_IMU_SPECS);

                    if (CommunicationPackets.verifyPacket(CommunicationPackets.response_GET_IMU_SPECS))
                    {
                        GlobalData.LogConsole("[INFO] GET-IMU-SPECS Packet Response verified");
                        myWizard.addTextTothLogger("[INFO] GET-IMU-SPECS Packet Response verified");
                        initialParametersObjectReference.gyro_bias_instability = CommunicationPackets.response_GET_IMU_SPECS.payload.gyroscopeBiasInstability;
                        initialParametersObjectReference.gyro_arw = CommunicationPackets.response_GET_IMU_SPECS.payload.gyroscopeAngularRandomWalk;
                        initialParametersObjectReference.gyro_bias_correlation_time = CommunicationPackets.response_GET_IMU_SPECS.payload.gyroscopeBiasCorrelationTime;
                        initialParametersObjectReference.accel_bias_instability = CommunicationPackets.response_GET_IMU_SPECS.payload.accelerometerBiasInstability;
                        initialParametersObjectReference.accel_vrw = CommunicationPackets.response_GET_IMU_SPECS.payload.accelerometerVelocityRandomWalk;
                        initialParametersObjectReference.accel_bias_correlation_time = CommunicationPackets.response_GET_IMU_SPECS.payload.accelerometerBiasCorrelationTime;

                        CommunicationPackets.invalidatePacket(ref CommunicationPackets.response_GET_IMU_SPECS);
                    }
                    else
                    {
                        MessageBox.Show("Invalid Response (GET-IMU-SPECS) From Navigator", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return -1 ;
                    }

                    serialCommunicationChannel.Write(CommunicationPackets.command_GET_SENSORS_USED);
                    Thread.Sleep(20);

                    serialCommunicationChannel.Read(ref CommunicationPackets.response_GET_SENSORS_USED);

                    if (CommunicationPackets.verifyPacket(CommunicationPackets.response_GET_SENSORS_USED))
                    {
                        GlobalData.LogConsole("[INFO] GET-SENSORS-USED Packet Response verified");
                         myWizard.addTextTothLogger("[INFO] GET-SENSORS-USED Packet Response verified");
                        initialParametersObjectReference.bChkbxMagEnabled = (CommunicationPackets.response_GET_SENSORS_USED.payload.byteA & 0x04) == 0x04;
                        initialParametersObjectReference.bChkbxBaroEnabled = (CommunicationPackets.response_GET_SENSORS_USED.payload.byteA & 0x08) == 0x08;

                        CommunicationPackets.invalidatePacket(ref CommunicationPackets.response_GET_SENSORS_USED);
                    }
                    else
                    {
                        MessageBox.Show("Invalid Response (GET-SENSORS-USED) From Navigator", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return -1 ;
                    }

                    
                    return 0;        
            
        }

             
        public void writeToSerialCom(byte[] buffer, int offset, int count)
        {
            serialCommunicationChannel.Write(buffer);
        }


        public byte[] readFromSerialCom(int responseLength)
        {
            int count = 0;
            byte[] values=null;
            //values = readResponse(responseLength, ref count);
            return values;
        }

        public byte[] readFromSerialCom(int responseLength,int offset)
        {
            int count = 0;
            byte[] values =null;
            //values = readResponse(responseLength, ref count,offset);
            return values;
        }


        public void resetSerialCom()
        {
            //portComm.DiscardInBuffer();
        }

        public void closeSerialCom()
        {
            //portComm.Close();
        }

        public void openSerialCom()
        {
           // portComm.Open();
        }

    }
 }

