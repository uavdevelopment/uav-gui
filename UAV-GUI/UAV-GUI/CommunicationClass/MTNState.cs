﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAV_GUI
{
    public enum Navigation_Status {
  Alignment,  Navigation  
    }; 
    static class MTNState
    {
        public static double latitude = 0;
        public static double longitude = 0;
        public static double Altitude = 0;

        public static double VelocityE = 0;
        public static double VelocityN = 0;
        public static double VelocityU = 0;

        public static double pitch = 0;
        public static double roll = 0;
        public static double yaw = 0;

        public static long rightpackets = 0;
        public static long Totalpackets = 0;
        public static long RateTotalpackets = 0;
        public static int Gps_State = 0;
        public static Navigation_Status NaV_State = Navigation_Status.Alignment;
    }
}
