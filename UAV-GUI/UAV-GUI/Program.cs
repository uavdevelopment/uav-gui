﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UAV_GUI
{
    static class Program
    {
        /// < ad]
        /// 
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            SplashForm.ShowSplashScreen();
            NewMainWindow mainForm = new NewMainWindow(); //this takes ages
            SplashForm.CloseForm();
            Application.Run(mainForm);
           
        }
    }
}
