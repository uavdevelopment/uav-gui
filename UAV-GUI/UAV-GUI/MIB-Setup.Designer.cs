﻿namespace UAV_GUI
{
    partial class MIB_Setup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bRateList = new System.Windows.Forms.ComboBox();
            this.CurrentBaudText = new System.Windows.Forms.TextBox();
            this.CheckBRateButton = new System.Windows.Forms.Button();
            this.changBRateButton = new System.Windows.Forms.Button();
            this.connDirectionBut = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.chConnDirectButton = new System.Windows.Forms.Button();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.SuspendLayout();
            // 
            // bRateList
            // 
            this.bRateList.FormattingEnabled = true;
            this.bRateList.Items.AddRange(new object[] {
            "2400",
            "4800",
            "9600",
            "19200",
            "38400"});
            this.bRateList.Location = new System.Drawing.Point(135, 62);
            this.bRateList.Name = "bRateList";
            this.bRateList.Size = new System.Drawing.Size(118, 21);
            this.bRateList.TabIndex = 0;
            // 
            // CurrentBaudText
            // 
            this.CurrentBaudText.Location = new System.Drawing.Point(135, 24);
            this.CurrentBaudText.Name = "CurrentBaudText";
            this.CurrentBaudText.ReadOnly = true;
            this.CurrentBaudText.Size = new System.Drawing.Size(118, 20);
            this.CurrentBaudText.TabIndex = 1;
            // 
            // CheckBRateButton
            // 
            this.CheckBRateButton.Location = new System.Drawing.Point(12, 24);
            this.CheckBRateButton.Name = "CheckBRateButton";
            this.CheckBRateButton.Size = new System.Drawing.Size(117, 23);
            this.CheckBRateButton.TabIndex = 2;
            this.CheckBRateButton.Text = "Check Baud Rate";
            this.CheckBRateButton.UseVisualStyleBackColor = true;
            this.CheckBRateButton.Click += new System.EventHandler(this.CheckBRateButton_Click);
            // 
            // changBRateButton
            // 
            this.changBRateButton.Location = new System.Drawing.Point(12, 62);
            this.changBRateButton.Name = "changBRateButton";
            this.changBRateButton.Size = new System.Drawing.Size(117, 23);
            this.changBRateButton.TabIndex = 3;
            this.changBRateButton.Text = "Change Baud Rate";
            this.changBRateButton.UseVisualStyleBackColor = true;
            this.changBRateButton.Click += new System.EventHandler(this.changBRateButton_Click);
            // 
            // connDirectionBut
            // 
            this.connDirectionBut.Location = new System.Drawing.Point(12, 141);
            this.connDirectionBut.Name = "connDirectionBut";
            this.connDirectionBut.Size = new System.Drawing.Size(117, 23);
            this.connDirectionBut.TabIndex = 4;
            this.connDirectionBut.Text = "Connection Direction";
            this.connDirectionBut.UseVisualStyleBackColor = true;
            this.connDirectionBut.Visible = false;
            this.connDirectionBut.Click += new System.EventHandler(this.connDirectionBut_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(135, 141);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(118, 20);
            this.textBox1.TabIndex = 5;
            this.textBox1.Visible = false;
            // 
            // chConnDirectButton
            // 
            this.chConnDirectButton.Location = new System.Drawing.Point(12, 141);
            this.chConnDirectButton.Name = "chConnDirectButton";
            this.chConnDirectButton.Size = new System.Drawing.Size(241, 23);
            this.chConnDirectButton.TabIndex = 6;
            this.chConnDirectButton.Text = "Switch Direction from GCS to AutoPilot";
            this.chConnDirectButton.UseVisualStyleBackColor = true;
            this.chConnDirectButton.Click += new System.EventHandler(this.chConnDirectButton_Click);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(973, 502);
            this.shapeContainer1.TabIndex = 7;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lineShape1.BorderWidth = 3;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 5;
            this.lineShape1.X2 = 261;
            this.lineShape1.Y1 = 118;
            this.lineShape1.Y2 = 118;
            // 
            // MIB_Setup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 502);
            this.Controls.Add(this.chConnDirectButton);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.connDirectionBut);
            this.Controls.Add(this.changBRateButton);
            this.Controls.Add(this.CheckBRateButton);
            this.Controls.Add(this.CurrentBaudText);
            this.Controls.Add(this.bRateList);
            this.Controls.Add(this.shapeContainer1);
            this.MaximizeBox = false;
            this.Name = "MIB_Setup";
            this.Text = "Multi Interface Board Setup";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MIB_Setup_FormClosing);
            this.Load += new System.EventHandler(this.MIB_Setup_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox bRateList;
        private System.Windows.Forms.TextBox CurrentBaudText;
        private System.Windows.Forms.Button CheckBRateButton;
        private System.Windows.Forms.Button changBRateButton;
        private System.Windows.Forms.Button connDirectionBut;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button chConnDirectButton;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
    }
}