﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GLobal;
using FileClasses.INI_Files;
using System.Threading;
using System.IO;
namespace UAV_GUI
{
    public partial class MIB_Setup : Form
    {
        Boolean reading=false; 
        Thread searchStartThread;
        protected ManualResetEvent threadStop = new ManualResetEvent(false);
        delegate void SetCallbackForSearchingStart(string updateString);

        startConnectionToMIB connection = new startConnectionToMIB();
        public MIB_Setup()
        {
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
        }

        private void MIB_Setup_Load(object sender, EventArgs e)
        {
            this.BringToFront();
            this.bRateList.SelectedIndex = 0;
        }
      
        private void CheckBRateButton_Click(object sender, EventArgs e)
        {
            /*
             To Be added Survey button on a simple connection.
             */
            if (CheckBRateButton.Text == "Stop Checking")
            {
                try
                {
                    
                    /*Closing the Thread If already Opened*/
                    threadStop.Set();
                    Thread.Sleep(250);
                    if (searchStartThread != null)
                    {
                        Thread.Sleep(500);
                        /*searchStartThread.Abort();
                        Thread.Sleep(250);
                        searchStartThread = null;*/
                    }
                    /*Dispose the Serial Object*/
                    if (connection.serialCommunicationChannel != null && connection.serialCommunicationChannel.isPortOpened() == true)
                    {
                        GlobalData.LogConsole("Resetting Serial Port 3... ");
                        connection.serialCommunicationChannel.Close();
                        System.Threading.Thread.Sleep(500);
                        connection.serialCommunicationChannel = null;
                        System.Threading.Thread.Sleep(50);
                    }
                    CheckBRateButton.Text = "Check Baud Rate";
                }
                catch (Exception exception)
                {
                    
                    MessageBox.Show("Please wait .. Threads can't be closed");
                    GlobalData.LogConsole(exception.Message);
                    return;
                }
            }
            else {
                CheckBRateButton.Text = "Stop Checking";
                searchStartThread = new Thread(() => scanForValidBaudRate());
                threadStop.Reset();
                searchStartThread.Start();
            
            }
            
        }

        private void updateBaudRateTexBox(string updateString)
        {
            try
            {
                if (this.CurrentBaudText.InvokeRequired)
                {
                    SetCallbackForSearchingStart d = new SetCallbackForSearchingStart(updateBaudRateTexBox);
                    this.Invoke(d, new object[] { updateString });
                    CurrentBaudText.Text = updateString;
                }
                else
                {
                    CurrentBaudText.Text = updateString;
                }
            }
            catch (Exception e) {

                Console.WriteLine(e.GetType().ToString());
                return;
            }
        }
    
        private void scanForValidBaudRate()
        
        {
            String updateString="";            
            int i = 0 ;
            int[] rates = { 38400, 9600, 19200, 4800, 2400 };
            for ( i = 0; i < rates.Length*9; i++)
            {
                
                if (threadStop.WaitOne(0))
                    return;
            if (connection.serialCommunicationChannel != null && connection.serialCommunicationChannel.isPortOpened() == true)
            {
                GlobalData.LogConsole("Resetting Serial Port 4... ");
                connection.serialCommunicationChannel.Close();
                connection.serialCommunicationChannel = null;
                System.Threading.Thread.Sleep(50);
            }

            try
            {
                //System.Threading.Thread.Sleep(50);
                Console.WriteLine("--------------->" + (int)i / 9);
                connection.serialCommunicationChannel = new SerialCommunication(INIData.compPort, rates[(int)i / 9]);
                updateString = "Scanning at .. " + rates[(int)i / 9] + " (" + (int)i % 9 + ")";
                updateBaudRateTexBox(updateString);
                byte[] mySwitch = { 84, 80, 73, 0, 0, 0x0c, 0x0c, 14, 28 };
                GlobalData.LogConsole("Sending The Switch Packet [1] ... ");
                connection.serialCommunicationChannel.Write(mySwitch);
                byte[] responseArray = new byte[10];
                int num_bytes_read = 0;
                connection.serialCommunicationChannel.Read(responseArray, 0, 9, ref num_bytes_read);
                PrintByteArray(responseArray);
                //Need to add checkSum Verfication
                //Console.WriteLine(num_bytes_read);
                //PrintByteArray(responseArray);
                if (num_bytes_read != 9 || responseArray[3] != 0 || responseArray[5] != 0xc || responseArray[6] != 0xc)
                {
                    Console.WriteLine("wrong" + num_bytes_read);
                }
                else
                {
                    MessageBox.Show("Successful Connection at " + rates[(int)i / 9].ToString(), "[[InFo]]");
                    updateString = "Success at .." + rates[(int)i / 9];
                    updateBaudRateTexBox(updateString);
                    CheckBRateButton.Text = "Check Baud Rate";
                    return;

                }
                connection.serialCommunicationChannel.Close();
            }
            catch (TimeoutException exception)
            {
                GlobalData.LogConsole(exception.GetType().ToString());
                GlobalData.LogConsole("Checking the Next Baud RateChecking the Next Baud Rate");
                //MessageBox.Show("Checking the Next Baud RateChecking the Next Baud Rate", "[Info]", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                continue;
            }

            catch (ThreadAbortException e) {
                CheckBRateButton.Text = "Check Baud Rate";
                return;
            }
            catch (IOException e)
            {
                updateString = "Not found ..";
                updateBaudRateTexBox(updateString);
                MessageBox.Show("Connection Has been Reset", "[Info]", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                CheckBRateButton.Text = "Check Baud Rate";
                return;
            }
            catch (Exception exception)
            {
                updateString = "Not found ..";
                updateBaudRateTexBox(updateString);
                GlobalData.LogConsole(exception.GetType().ToString());
                GlobalData.LogConsole(exception.Message);
                MessageBox.Show("Error In Connection", "[Info]", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                CheckBRateButton.Text = "Check Baud Rate";
                return;
            }
            } if (i >= rates.Length * 9) {
                updateString = "Not found ..";
                updateBaudRateTexBox(updateString);
                MessageBox.Show("Error In Connection", "[Info]", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                CheckBRateButton.Text = "Check Baud Rate";
            }
         
        }

        private void connDirectionBut_Click(object sender, EventArgs e)
        {
            /*
             To Be added Survey button on a simple connection.
             */

            MessageBox.Show("Not Supported in the current Firmware","[[INFO]]");
        }

        private void chConnDirectButton_Click(object sender, EventArgs e)
        {
            if (connection.serialCommunicationChannel != null && connection.serialCommunicationChannel.isPortOpened() == true)
            {
                GlobalData.LogConsole("Resetting Serial Port 1... ");
                connection.serialCommunicationChannel.Close();
                connection.serialCommunicationChannel = null;
                System.Threading.Thread.Sleep(150);
            }

            try
            {
                connection.serialCommunicationChannel = new SerialCommunication(INIData.compPort);

                byte[] mySwitch = { 84, 80, 73, 0, 0, 0x0e, 0x0e, 14, 28 };
                GlobalData.LogConsole("Sending The Switch Packet [1] ... ");
                connection.serialCommunicationChannel.Write(mySwitch);
                System.Threading.Thread.Sleep(500);
                GlobalData.LogConsole("Sending The Switch Packet [2] ... ");
                connection.serialCommunicationChannel.Write(mySwitch);
                System.Threading.Thread.Sleep(500);
                GlobalData.LogConsole("Sending The Switch Packet [3] ... ");
                connection.serialCommunicationChannel.Write(mySwitch);
                connection.serialCommunicationChannel.Close();
                GlobalData.LogConsole("Ground Station is Now Connected To AutoPilot ... ");
                GlobalData.LogConsole("Reset the system to back to the navigation board if needed ... ");
                System.Windows.Forms.MessageBox.Show("Ground Station is Now Connected To AutoPilot", "Info");
                
            }
            catch (Exception exception)
            {
                String ErrTitle = "COM Error";
                GlobalData.LogConsole("Error Occured ... ");
                String ErrMsg = "Unable to open connection to COM Port.";
                GlobalData.LogConsole(exception.Message);
                MessageBox.Show(ErrMsg, ErrTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }
        
        public void PrintByteArray(byte[] bytes)
        {
            var sb = new StringBuilder("new byte[] { ");
            foreach (var b in bytes)
            {
                sb.Append(b + ", ");
            }
            sb.Append("}");
            Console.WriteLine(sb.ToString());
        }
        
        private void changBRateButton_Click(object sender, EventArgs e)
        {
            if (connection.serialCommunicationChannel != null && connection.serialCommunicationChannel.isPortOpened() == true)
            {
                GlobalData.LogConsole("Resetting Serial Port 2... ");
                connection.serialCommunicationChannel.Close();
                connection.serialCommunicationChannel = null;
                System.Threading.Thread.Sleep(150);
            }

            try
            {
                connection.serialCommunicationChannel = new SerialCommunication(INIData.compPort);

                byte[] mySwitch = {84, 80, 73, 1, 0, 0xd, 0xd, 0x5, 14, 28 };
               
                if (this.bRateList.SelectedItem.ToString().Equals("2400")) 
                {
                    mySwitch[7] = 0x0;
                }
                else if (this.bRateList.SelectedItem.ToString().Equals("4800"))
                {
                    mySwitch[7] = 0x1;
                }
                else if (this.bRateList.SelectedItem.ToString().Equals("9600"))
                {
                    mySwitch[7] = 0x2;
                }
                else if (this.bRateList.SelectedItem.ToString().Equals("19200"))
                {
                    mySwitch[7] = 0x3;
                }
                else if (this.bRateList.SelectedItem.ToString().Equals("38400"))
                {
                    mySwitch[7] = 0x4;
                }
                else 
                {
                    MessageBox.Show("Invalid Baud Rate", "[[INFO]]");
                }


                GlobalData.LogConsole("Sending The Switch Packet [1] ... ");
                connection.serialCommunicationChannel.Write(mySwitch);
                //System.Threading.Thread.Sleep();
                byte [] responseArray = new byte[11];
                int num_bytes_read =0 ;
                connection.serialCommunicationChannel.Read(responseArray, 0, 10,ref num_bytes_read);
                //Need to add checkSum Verfication
                //Console.WriteLine(num_bytes_read);
                //PrintByteArray(responseArray);
                if (num_bytes_read != 10 || responseArray[7] != 0 || responseArray[3] != 1 || responseArray[5] != 0xd || responseArray[6] != 0xd)
                {
                    GlobalData.LogConsole("Error in Communication");
                    MessageBox.Show("Baud Rate have not changed, Try Again","[Error]",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    return;
                }
                connection.serialCommunicationChannel.Close();
                GlobalData.LogConsole("Baud Rate of the MIB have been Changed ... ");
                GlobalData.LogConsole("Reset the system  ... ");
                System.Windows.Forms.MessageBox.Show("Baud Rate of the MIB have been Changed, Changes will take effect after the next restart ... ", "Info");

            }
            catch (Exception exception)
            {
                String ErrTitle = "COM Error";
                GlobalData.LogConsole("Error Occured ... ");
                String ErrMsg = "Unable to open connection to COM Port.";
                GlobalData.LogConsole(exception.Message);
                MessageBox.Show(ErrMsg, ErrTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }

        private void MIB_Setup_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {

                /*Closing the Thread If already Opened*/
                threadStop.Set();
                Thread.Sleep(250);
                if (searchStartThread != null)
                {
                    Thread.Sleep(500);
                    /*searchStartThread.Abort();
                    Thread.Sleep(250);
                    searchStartThread = null;*/
                }
                /*Dispose the Serial Object*/
                if (connection.serialCommunicationChannel != null && connection.serialCommunicationChannel.isPortOpened() == true)
                {
                    GlobalData.LogConsole("Resetting Serial Port 3... ");
                    connection.serialCommunicationChannel.Close();
                    System.Threading.Thread.Sleep(500);
                    connection.serialCommunicationChannel = null;
                    System.Threading.Thread.Sleep(50);
                }
                CheckBRateButton.Text = "Check Baud Rate";
            }
            catch (Exception exception)
            {

                MessageBox.Show("Please wait .. Threads can't be closed");
                GlobalData.LogConsole(exception.Message);
                return;
            }
        }
    }
}
