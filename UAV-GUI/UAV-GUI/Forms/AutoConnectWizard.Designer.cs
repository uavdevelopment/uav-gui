﻿namespace UAV_GUI.Forms
{
    partial class AutoConnectWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.connectWizard = new AeroWizard.WizardControl();
            this.wizardPage2 = new AeroWizard.WizardPage();
            this.chooseConnectionTextBox = new System.Windows.Forms.TextBox();
            this.switchToAutopilotButton = new System.Windows.Forms.Button();
            this.connectNavigationBoard = new System.Windows.Forms.Button();
            this.wizardPage3 = new AeroWizard.WizardPage();
            this.inckBox = new System.Windows.Forms.CheckBox();
            this.biasinitPanel = new System.Windows.Forms.Panel();
            this.wizardPage4 = new AeroWizard.WizardPage();
            this.retryNavButton = new System.Windows.Forms.Button();
            this.navStartDatBox = new System.Windows.Forms.TextBox();
            this.ticksForIniti = new System.Windows.Forms.Timer(this.components);
            this.navStartTicks = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.connectWizard)).BeginInit();
            this.wizardPage2.SuspendLayout();
            this.wizardPage3.SuspendLayout();
            this.wizardPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // connectWizard
            // 
            this.connectWizard.BackColor = System.Drawing.Color.White;
            this.connectWizard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.connectWizard.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectWizard.Location = new System.Drawing.Point(0, 0);
            this.connectWizard.Name = "connectWizard";
            this.connectWizard.Pages.Add(this.wizardPage2);
            this.connectWizard.Pages.Add(this.wizardPage3);
            this.connectWizard.Pages.Add(this.wizardPage4);
            this.connectWizard.Size = new System.Drawing.Size(527, 390);
            this.connectWizard.TabIndex = 0;
            this.connectWizard.Text = "Auto Connect Wizard";
            this.connectWizard.Title = "Connection Wizard";
            this.connectWizard.Finished += new System.EventHandler(this.connectWizard_Finished);
            // 
            // wizardPage2
            // 
            this.wizardPage2.AllowNext = false;
            this.wizardPage2.Controls.Add(this.chooseConnectionTextBox);
            this.wizardPage2.Controls.Add(this.switchToAutopilotButton);
            this.wizardPage2.Controls.Add(this.connectNavigationBoard);
            this.wizardPage2.Name = "wizardPage2";
            this.wizardPage2.NextPage = this.wizardPage3;
            this.wizardPage2.Size = new System.Drawing.Size(480, 236);
            this.wizardPage2.TabIndex = 1;
            this.wizardPage2.Text = "Select Connection Type ...";
            this.wizardPage2.Commit += new System.EventHandler<AeroWizard.WizardPageConfirmEventArgs>(this.wizardPage2_Commit_1);
            this.wizardPage2.Initialize += new System.EventHandler<AeroWizard.WizardPageInitEventArgs>(this.wizardPage2_Initialize);
            // 
            // chooseConnectionTextBox
            // 
            this.chooseConnectionTextBox.Location = new System.Drawing.Point(12, 135);
            this.chooseConnectionTextBox.Multiline = true;
            this.chooseConnectionTextBox.Name = "chooseConnectionTextBox";
            this.chooseConnectionTextBox.ReadOnly = true;
            this.chooseConnectionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.chooseConnectionTextBox.Size = new System.Drawing.Size(420, 98);
            this.chooseConnectionTextBox.TabIndex = 2;
            this.chooseConnectionTextBox.Text = "Waiting for User Input ....";
            // 
            // switchToAutopilotButton
            // 
            this.switchToAutopilotButton.Location = new System.Drawing.Point(12, 64);
            this.switchToAutopilotButton.Name = "switchToAutopilotButton";
            this.switchToAutopilotButton.Size = new System.Drawing.Size(420, 36);
            this.switchToAutopilotButton.TabIndex = 1;
            this.switchToAutopilotButton.Text = "Switch The Connection to the Autopilot";
            this.switchToAutopilotButton.UseVisualStyleBackColor = true;
            this.switchToAutopilotButton.Click += new System.EventHandler(this.switchToAutopilotButton_Click);
            // 
            // connectNavigationBoard
            // 
            this.connectNavigationBoard.Location = new System.Drawing.Point(12, 12);
            this.connectNavigationBoard.Name = "connectNavigationBoard";
            this.connectNavigationBoard.Size = new System.Drawing.Size(420, 36);
            this.connectNavigationBoard.TabIndex = 0;
            this.connectNavigationBoard.Text = "Connect to the Navigation Board ";
            this.connectNavigationBoard.UseVisualStyleBackColor = true;
            this.connectNavigationBoard.Click += new System.EventHandler(this.button1_Click);
            // 
            // wizardPage3
            // 
            this.wizardPage3.AllowBack = false;
            this.wizardPage3.AllowNext = false;
            this.wizardPage3.Controls.Add(this.inckBox);
            this.wizardPage3.Controls.Add(this.biasinitPanel);
            this.wizardPage3.Name = "wizardPage3";
            this.wizardPage3.NextPage = this.wizardPage4;
            this.wizardPage3.Size = new System.Drawing.Size(480, 236);
            this.wizardPage3.TabIndex = 2;
            this.wizardPage3.Text = "System Initialization";
            // 
            // inckBox
            // 
            this.inckBox.AutoSize = true;
            this.inckBox.Location = new System.Drawing.Point(3, 214);
            this.inckBox.Name = "inckBox";
            this.inckBox.Size = new System.Drawing.Size(192, 19);
            this.inckBox.TabIndex = 0;
            this.inckBox.Text = "The System is already initialized";
            this.inckBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.inckBox.UseVisualStyleBackColor = true;
            this.inckBox.CheckedChanged += new System.EventHandler(this.inckBox_CheckedChanged);
            // 
            // biasinitPanel
            // 
            this.biasinitPanel.Location = new System.Drawing.Point(3, 3);
            this.biasinitPanel.Name = "biasinitPanel";
            this.biasinitPanel.Size = new System.Drawing.Size(449, 207);
            this.biasinitPanel.TabIndex = 3;
            // 
            // wizardPage4
            // 
            this.wizardPage4.Controls.Add(this.retryNavButton);
            this.wizardPage4.Controls.Add(this.navStartDatBox);
            this.wizardPage4.IsFinishPage = true;
            this.wizardPage4.Name = "wizardPage4";
            this.wizardPage4.Size = new System.Drawing.Size(480, 236);
            this.wizardPage4.TabIndex = 3;
            this.wizardPage4.Text = "Start Navigation ";
            this.wizardPage4.Commit += new System.EventHandler<AeroWizard.WizardPageConfirmEventArgs>(this.wizardPage4_Commit);
            this.wizardPage4.Initialize += new System.EventHandler<AeroWizard.WizardPageInitEventArgs>(this.wizardPage4_Initialize);
            // 
            // retryNavButton
            // 
            this.retryNavButton.Location = new System.Drawing.Point(189, 210);
            this.retryNavButton.Name = "retryNavButton";
            this.retryNavButton.Size = new System.Drawing.Size(75, 23);
            this.retryNavButton.TabIndex = 1;
            this.retryNavButton.Text = "Try Again";
            this.retryNavButton.UseVisualStyleBackColor = true;
            this.retryNavButton.Click += new System.EventHandler(this.retryNavButton_Click);
            // 
            // navStartDatBox
            // 
            this.navStartDatBox.Location = new System.Drawing.Point(13, 3);
            this.navStartDatBox.Multiline = true;
            this.navStartDatBox.Name = "navStartDatBox";
            this.navStartDatBox.ReadOnly = true;
            this.navStartDatBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.navStartDatBox.Size = new System.Drawing.Size(451, 201);
            this.navStartDatBox.TabIndex = 0;
            // 
            // ticksForIniti
            // 
            this.ticksForIniti.Enabled = true;
            this.ticksForIniti.Interval = 1000;
            this.ticksForIniti.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // navStartTicks
            // 
            this.navStartTicks.Enabled = true;
            this.navStartTicks.Interval = 200;
            this.navStartTicks.Tick += new System.EventHandler(this.navStartTicks_Tick);
            // 
            // AutoConnectWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 390);
            this.Controls.Add(this.connectWizard);
            this.Name = "AutoConnectWizard";
            this.Text = "Auto Connect Wizard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AutoConnectWizard_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.connectWizard)).EndInit();
            this.wizardPage2.ResumeLayout(false);
            this.wizardPage2.PerformLayout();
            this.wizardPage3.ResumeLayout(false);
            this.wizardPage3.PerformLayout();
            this.wizardPage4.ResumeLayout(false);
            this.wizardPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private AeroWizard.WizardControl connectWizard;
        private AeroWizard.WizardPage wizardPage2;
        private System.Windows.Forms.Button connectNavigationBoard;
        private System.Windows.Forms.Button switchToAutopilotButton;
        private System.Windows.Forms.TextBox chooseConnectionTextBox;
        private AeroWizard.WizardPage wizardPage3;
        private System.Windows.Forms.Panel biasinitPanel;
        private System.Windows.Forms.Timer ticksForIniti;
        private System.Windows.Forms.Timer navStartTicks;
        private AeroWizard.WizardPage wizardPage4;
        private System.Windows.Forms.TextBox navStartDatBox;
        private System.Windows.Forms.CheckBox inckBox;
        private System.Windows.Forms.Button retryNavButton;
    }
}