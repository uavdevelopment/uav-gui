﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Error_Reporting;
using GUtilities;
using MP_Lib;
using GLobal;
namespace UAV_GUI.Forms
{
    public partial class ServoTest : Form
    {
        public UAV myUav;
        int servo1_position;
        int servo2_position;
        int servo3_position;
        int servo4_position;
        int servo5_position;
        int servo6_position;
        int servo7_position;
        int servo8_position;

        int servo1_zero;
        int servo2_zero;
        int servo3_zero;
        int servo4_zero;
        int servo5_zero;
        int servo6_zero;
        int servo7_zero;
        int servo8_zero;


        int servo1_travel;
        int servo2_travel;
        int servo3_travel;
        int servo4_travel;
        int servo5_travel;
        int servo6_travel;
        int servo7_travel;
        int servo8_travel;

        int testType = 1;
        public ServoTest(UAV myUAV_)
        {
            myUav = myUAV_;
            servo1_position = 0;
            servo2_position = 0;
            servo3_position = 0;
            servo4_position = 0;
            InitializeComponent();
           
            ApplicationLookAndFeel.UseTheme(this);

            if(GlobalData.apConnected == true)
            {
                int value = 0;
                int retval = myUav.connect_servoform(ref value, false);
                connection.Text = "Disconnect";
                if (retval == 0)
                {

                    servo1_zero = myUav.getJoy(17);
                    servo2_zero = myUav.getJoy(18);
                    servo3_zero = myUav.getJoy(19);
                    servo4_zero = myUav.getJoy(20);
                    servo5_zero = myUav.getJoy(21);
                    servo6_zero = myUav.getJoy(22);
                    servo7_zero = myUav.getJoy(23);
                    servo8_zero = myUav.getJoy(24);


                    servo1_travel = myUav.getJoy(50);
                    servo2_travel = myUav.getJoy(51);
                    servo3_travel = myUav.getJoy(52);
                    servo4_travel = myUav.getJoy(53);
                    servo5_travel = myUav.getJoy(54);
                    servo6_travel = myUav.getJoy(55);
                    servo7_travel = myUav.getJoy(56);
                    servo8_travel = myUav.getJoy(57);
                    connection.Text = "Disconnect";
                    switch (value)
                    {
                        case 0:
                            #region

                            Servo_Slider1.Visible = true;
                            s1.Text = "Aileron";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Elevator";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Rudder";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            Servo5value.Visible = true;
                            s5.Visible = false;
                            Servo_Slider6.Visible = true;
                            Servo6value.Visible = true;
                            s6.Visible = false;
                            Servo_Slider7.Visible = true;
                            Servo7value.Visible = true;
                            s7.Visible = false;
                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;
                            this.Height = 10;
                            break;
                            #endregion

                        case 1:
                            #region
                            this.Height = 63 * 6;
                            Servo_Slider1.Visible = true;
                            s1.Text = "Aileron";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Elevator";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Rudder";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            s5.Text = "Flaps";
                            Servo_Slider6.Visible = true;
                            Servo6value.Visible = true;
                            s6.Visible = false;
                            Servo_Slider7.Visible = true;
                            Servo7value.Visible = true;
                            s7.Visible = false;
                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;
                            break;
                            #endregion
                        case 2:
                            #region
                            this.Height = 63 * 7;
                            Servo_Slider1.Visible = true;
                            s1.Text = "Left Aileron";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Elevator";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Rudder";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            Servo5value.Visible = true;
                            s5.Visible = false;
                            Servo_Slider6.Visible = true;
                            s6.Text = "Right Aileron";
                            Servo_Slider7.Visible = true;
                            Servo7value.Visible = false;
                            s7.Visible = false;
                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;
                            break;

                            #endregion
                        case 3:
                            #region
                            this.Height = 63 * 8;
                            Servo_Slider1.Visible = true;
                            s1.Text = "Left Aileron";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Elevator";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Rudder";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            s5.Text = "Left Flap";
                            Servo_Slider6.Visible = true;
                            s6.Text = "Right Aileron";
                            Servo_Slider7.Visible = true;
                            s7.Text = "Right Flap";

                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;
                            break;

                            #endregion
                        case 4:
                            #region
                            this.Height = 63 * 5;
                            Servo_Slider1.Visible = true;
                            s1.Text = "Left Elevon";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Right Elevon";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Rudder";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            Servo5value.Visible = true;
                            s5.Visible = false;
                            Servo_Slider6.Visible = true;
                            Servo6value.Visible = true;
                            s6.Visible = false;
                            Servo_Slider7.Visible = true;
                            Servo7value.Visible = true;
                            s7.Visible = false;
                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;

                            break;
                            #endregion
                        case 5:
                            #region
                            this.Height = 63 * 8;
                            Servo_Slider1.Visible = true;
                            s1.Text = "Left Aileron";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Elevator";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Rudder";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            Servo5value.Visible = true;
                            s5.Visible = false;
                            Servo_Slider6.Visible = true;
                            s6.Text = "Right Aileron";
                            Servo_Slider7.Visible = true;
                            s7.Text = "Right Flap";

                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;
                            break;

                            #endregion
                        case 6:
                            #region
                            this.Height = 63 * 5;
                            Servo_Slider1.Visible = true;
                            s1.Text = "Left Fin";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Right Fin";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Bottom Fin";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            Servo5value.Visible = true;
                            s5.Visible = false;
                            Servo_Slider6.Visible = true;
                            Servo6value.Visible = true;
                            s6.Visible = false;
                            Servo_Slider7.Visible = true;
                            Servo7value.Visible = true;
                            s7.Visible = false;
                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;
                            break;
                            #endregion
                        case 7:
                            #region
                            this.Size = new Size(this.Width, 63 * 5);
                            Servo_Slider1.Visible = true;
                            s1.Text = "Alieron";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Elevator";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Rudder";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            Servo5value.Visible = true;
                            s5.Visible = false;
                            Servo_Slider6.Visible = true;
                            Servo6value.Visible = true;
                            s6.Visible = false;
                            Servo_Slider7.Visible = true;
                            Servo7value.Visible = true;
                            s7.Visible = false;
                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;
                            break;
                            #endregion

                    }


                }
                else
                {
                    connection.Text = "Connect";
                    throw new BusinessException(1200, "Error With Connection With Number \r\n [Hint] Disconnect from thr main screen" + retval);
                    // display message for try to connect the the auto pilot 

                }
            }

        }

        private void Servo1_Slider(object sender, ScrollEventArgs e)
        {
            Servo1value.Text = Servo_Slider1.Value.ToString();

            //Servo2Value.Text = Convert.ToString(servo1_position);
            servo1_position = AutoPilotUnits.toCoarseServo((int)Servo_Slider1.Value,servo1_zero,servo1_travel);

            myUav.change_servo(servo1_position, 1);
        }

        private void Servo2_Slider(object sender, ScrollEventArgs e)
        {
           Servo2Value.Text = Servo_Slider2.Value.ToString();
           servo2_position = AutoPilotUnits.toCoarseServo((int)Servo_Slider2.Value, servo2_zero, servo2_travel);
           myUav.change_servo(servo2_position, 2);
        }

        private void Servo3_Slider(object sender, ScrollEventArgs e)
        {
            Servo3Value.Text = Servo_Slider3.Value.ToString();
            servo3_position = AutoPilotUnits.toCoarseServo((int)Servo_Slider3.Value, servo3_zero, servo3_travel);
            myUav.change_servo(servo3_position, 3);
        
        }

        private void Servo4_Slider(object sender, ScrollEventArgs e)
        {
            Servo4Value.Text = Servo_Slider4.Value.ToString();
            servo4_position = AutoPilotUnits.toCoarseServo((int)Servo_Slider4.Value, servo4_zero, servo4_travel);
            myUav.change_servo(servo4_position, 4);
          
        }

        private void Servo5_Slider(object sender, ScrollEventArgs e)
        {
            Servo5value.Text = Servo_Slider5.Value.ToString();
            servo5_position =AutoPilotUnits.toCoarseServo((int)Servo_Slider5.Value, servo5_zero, servo5_travel);
            myUav.change_servo(servo5_position, 5);
        }

        private void Servo6_Slider(object sender, ScrollEventArgs e)
        {
            Servo6value.Text = Servo_Slider6.Value.ToString();
           servo6_position=  AutoPilotUnits.toCoarseServo((int)Servo_Slider6.Value, servo6_zero, servo6_travel);
            myUav.change_servo(servo6_position, 6);
        }
        private void Servo7_Slider(object sender, ScrollEventArgs e)
        {
            Servo7value.Text = Servo_Slider7.Value.ToString();
            servo7_position=AutoPilotUnits.toCoarseServo((int)Servo_Slider7.Value, servo7_zero, servo7_travel);
            myUav.change_servo(servo7_position, 7);
        }
        private void Servo8_Slider(object sender, ScrollEventArgs e)
        {
            Servo8value.Text = Servo_Slider8.Value.ToString();
            servo8_position=AutoPilotUnits.toCoarseServo((int)Servo_Slider8.Value, servo8_zero, servo8_travel);
            myUav.change_servo(servo8_position, 8);
        }

        private void Connect(object sender, EventArgs e)
        {
            Button senders = sender as Button;
            if (senders.Text.Equals("Connect"))
            {
                foreach (Control x in this.Controls)
                {
                    if (x is HScrollBar)
                    {
                        ((HScrollBar)x).Value = 0;
                        x.Enabled = true;
                    }
                }
                int value = 0;
                int retval = -1;
                retval = myUav.connect_servoform(ref value,false);

                if (retval == 0)
                {

                      servo1_zero=myUav.getJoy(17);
                      servo2_zero=myUav.getJoy(18);
                      servo3_zero=myUav.getJoy(19);
                      servo4_zero=myUav.getJoy(20);
                      servo5_zero=myUav.getJoy(21);
                      servo6_zero=myUav.getJoy(22);
                      servo7_zero=myUav.getJoy(23);
                      servo8_zero=myUav.getJoy(24);


                      servo1_travel=myUav.getJoy(50);
                      servo2_travel=myUav.getJoy(51);
                      servo3_travel=myUav.getJoy(52);
                      servo4_travel=myUav.getJoy(53);
                      servo5_travel=myUav.getJoy(54);
                      servo6_travel=myUav.getJoy(55);
                      servo7_travel=myUav.getJoy(56);
                      servo8_travel = myUav.getJoy(57);
                    senders.Text = "Disconnect";
                    switch (value)
                    {
                        case 0:
                            #region
                            
                            Servo_Slider1.Visible = true;
                            s1.Text = "Aileron";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Elevator";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Rudder";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            Servo5value.Visible = true;
                            s5.Visible = false;
                            Servo_Slider6.Visible = true;
                            Servo6value.Visible = true;
                            s6.Visible = false;
                            Servo_Slider7.Visible = true;
                            Servo7value.Visible = true;
                            s7.Visible = false;
                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;
                            this.Height = 10 ;
                            break;
                            #endregion

                        case 1:
                            #region
                            this.Height = 63 * 6;
                            Servo_Slider1.Visible = true;
                            s1.Text = "Aileron";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Elevator";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Rudder";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            s5.Text = "Flaps";
                            Servo_Slider6.Visible = true;
                            Servo6value.Visible = true;
                            s6.Visible = false;
                            Servo_Slider7.Visible = true;
                            Servo7value.Visible = true;
                            s7.Visible = false;
                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;
                            break;
                            #endregion
                        case 2:
                            #region
                            this.Height = 63 * 7;
                            Servo_Slider1.Visible = true;
                            s1.Text = "Left Aileron";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Elevator";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Rudder";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            Servo5value.Visible = true;
                            s5.Visible = false;
                            Servo_Slider6.Visible = true;
                            s6.Text = "Right Aileron";
                            Servo_Slider7.Visible = true;
                            Servo7value.Visible = false;
                            s7.Visible = false;
                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;
                            break;

                            #endregion
                        case 3:
                            #region
                            this.Height = 63 * 8;
                            Servo_Slider1.Visible = true;
                            s1.Text = "Left Aileron";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Elevator";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Rudder";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            s5.Text = "Left Flap";
                            Servo_Slider6.Visible = true;
                            s6.Text = "Right Aileron";
                            Servo_Slider7.Visible = true;
                            s7.Text = "Right Flap";

                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;
                            break;

                            #endregion
                        case 4:
                            #region
                            this.Height = 63 * 5;
                            Servo_Slider1.Visible = true;
                            s1.Text = "Left Elevon";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Right Elevon";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Rudder";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            Servo5value.Visible = true;
                            s5.Visible = false;
                            Servo_Slider6.Visible = true;
                            Servo6value.Visible = true;
                            s6.Visible = false;
                            Servo_Slider7.Visible = true;
                            Servo7value.Visible = true;
                            s7.Visible = false;
                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;

                            break;
                            #endregion
                        case 5:
                            #region
                            this.Height = 63 * 8;
                            Servo_Slider1.Visible = true;
                            s1.Text = "Left Aileron";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Elevator";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Rudder";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            Servo5value.Visible = true;
                            s5.Visible = false;
                            Servo_Slider6.Visible = true;
                            s6.Text = "Right Aileron";
                            Servo_Slider7.Visible = true;
                            s7.Text = "Right Flap";

                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;
                            break;

                            #endregion
                        case 6:
                            #region
                            this.Height = 63 * 5;
                            Servo_Slider1.Visible = true;
                            s1.Text = "Left Fin";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Right Fin";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Bottom Fin";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            Servo5value.Visible = true;
                            s5.Visible = false;
                            Servo_Slider6.Visible = true;
                            Servo6value.Visible = true;
                            s6.Visible = false;
                            Servo_Slider7.Visible = true;
                            Servo7value.Visible = true;
                            s7.Visible = false;
                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;
                            break;
                            #endregion
                        case 7:
                            #region
                            this.Size =  new Size (this.Width,63 * 5);
                            Servo_Slider1.Visible = true;
                            s1.Text = "Alieron";
                            Servo_Slider2.Visible = true;
                            s3.Text = "Elevator";
                            Servo_Slider3.Visible = true;
                            s2.Text = "Rudder";
                            Servo_Slider4.Visible = true;
                            s4.Text = "Throttle";
                            Servo_Slider5.Visible = true;
                            Servo5value.Visible = true;
                            s5.Visible = false;
                            Servo_Slider6.Visible = true;
                            Servo6value.Visible = true;
                            s6.Visible = false;
                            Servo_Slider7.Visible = true;
                            Servo7value.Visible = true;
                            s7.Visible = false;
                            Servo_Slider8.Visible = true;
                            Servo8value.Visible = true;
                            s8.Visible = false;
                            break;
                            #endregion

                    }


                }
                else
                {
                    connection.Text = "Connect";
                    throw new BusinessException(1200, "Error With Connection With Number" + retval);
                    // display message for try to connect the the auto pilot 

                }
            }
            else {
                myUav.enableAutoCalculationForServos();
                myUav.disConnect();
                senders.Text = "Connect";
                foreach (Control x in this.Controls)
                {
                    if (x is HScrollBar)
                    {
                        x.Enabled = false;
                    }
                }
            
            }
        }

        private void ServoTest_Load(object sender, EventArgs e)
        {
            foreach (Control x in this.Controls) 
            {
                if (x is HScrollBar) {
                    x.Enabled = false; 
                }
            }
        }

        private void ServoTest_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (connection.Text.Equals("Disconnect"))
            {
                myUav.enableAutoCalculationForServos();
                myUav.disConnect();
               
            }
        }

      

       

        

        

        

        

       
    }
}
