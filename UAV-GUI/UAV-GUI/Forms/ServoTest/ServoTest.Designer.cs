﻿namespace UAV_GUI.Forms
{
    partial class ServoTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used. 
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.s1 = new System.Windows.Forms.Label();
            this.s3 = new System.Windows.Forms.Label();
            this.s2 = new System.Windows.Forms.Label();
            this.s4 = new System.Windows.Forms.Label();
            this.Servo_Slider1 = new System.Windows.Forms.HScrollBar();
            this.Servo_Slider2 = new System.Windows.Forms.HScrollBar();
            this.Servo_Slider3 = new System.Windows.Forms.HScrollBar();
            this.Servo_Slider4 = new System.Windows.Forms.HScrollBar();
            this.Servo1value = new System.Windows.Forms.Label();
            this.Servo2Value = new System.Windows.Forms.Label();
            this.Servo3Value = new System.Windows.Forms.Label();
            this.Servo4Value = new System.Windows.Forms.Label();
            this.connection = new System.Windows.Forms.Button();
            this.s5 = new System.Windows.Forms.Label();
            this.s6 = new System.Windows.Forms.Label();
            this.s8 = new System.Windows.Forms.Label();
            this.s7 = new System.Windows.Forms.Label();
            this.Servo_Slider7 = new System.Windows.Forms.HScrollBar();
            this.Servo_Slider5 = new System.Windows.Forms.HScrollBar();
            this.Servo_Slider8 = new System.Windows.Forms.HScrollBar();
            this.Servo_Slider6 = new System.Windows.Forms.HScrollBar();
            this.Servo8value = new System.Windows.Forms.Label();
            this.Servo7value = new System.Windows.Forms.Label();
            this.Servo6value = new System.Windows.Forms.Label();
            this.Servo5value = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // s1
            // 
            this.s1.AutoSize = true;
            this.s1.Location = new System.Drawing.Point(26, 49);
            this.s1.Name = "s1";
            this.s1.Size = new System.Drawing.Size(46, 13);
            this.s1.TabIndex = 0;
            this.s1.Text = "Elevator";
            // 
            // s3
            // 
            this.s3.AutoSize = true;
            this.s3.Location = new System.Drawing.Point(26, 164);
            this.s3.Name = "s3";
            this.s3.Size = new System.Drawing.Size(39, 13);
            this.s3.TabIndex = 1;
            this.s3.Text = "Aileron";
            // 
            // s2
            // 
            this.s2.AutoSize = true;
            this.s2.Location = new System.Drawing.Point(26, 109);
            this.s2.Name = "s2";
            this.s2.Size = new System.Drawing.Size(42, 13);
            this.s2.TabIndex = 2;
            this.s2.Text = "Rudder";
            // 
            // s4
            // 
            this.s4.AutoSize = true;
            this.s4.Location = new System.Drawing.Point(26, 220);
            this.s4.Name = "s4";
            this.s4.Size = new System.Drawing.Size(43, 13);
            this.s4.TabIndex = 3;
            this.s4.Text = "Throttle";
            // 
            // Servo_Slider1
            // 
            this.Servo_Slider1.LargeChange = 0;
            this.Servo_Slider1.Location = new System.Drawing.Point(171, 49);
            this.Servo_Slider1.Minimum = -100;
            this.Servo_Slider1.Name = "Servo_Slider1";
            this.Servo_Slider1.Size = new System.Drawing.Size(159, 17);
            this.Servo_Slider1.SmallChange = 0;
            this.Servo_Slider1.TabIndex = 4;
            this.Servo_Slider1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Servo1_Slider);
            // 
            // Servo_Slider2
            // 
            this.Servo_Slider2.LargeChange = 0;
            this.Servo_Slider2.Location = new System.Drawing.Point(171, 105);
            this.Servo_Slider2.Minimum = -100;
            this.Servo_Slider2.Name = "Servo_Slider2";
            this.Servo_Slider2.Size = new System.Drawing.Size(159, 17);
            this.Servo_Slider2.SmallChange = 0;
            this.Servo_Slider2.TabIndex = 5;
            this.Servo_Slider2.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Servo2_Slider);
            // 
            // Servo_Slider3
            // 
            this.Servo_Slider3.LargeChange = 0;
            this.Servo_Slider3.Location = new System.Drawing.Point(171, 160);
            this.Servo_Slider3.Minimum = -100;
            this.Servo_Slider3.Name = "Servo_Slider3";
            this.Servo_Slider3.Size = new System.Drawing.Size(159, 17);
            this.Servo_Slider3.SmallChange = 0;
            this.Servo_Slider3.TabIndex = 6;
            this.Servo_Slider3.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Servo3_Slider);
            // 
            // Servo_Slider4
            // 
            this.Servo_Slider4.LargeChange = 0;
            this.Servo_Slider4.Location = new System.Drawing.Point(171, 216);
            this.Servo_Slider4.Minimum = -100;
            this.Servo_Slider4.Name = "Servo_Slider4";
            this.Servo_Slider4.Size = new System.Drawing.Size(159, 17);
            this.Servo_Slider4.SmallChange = 0;
            this.Servo_Slider4.TabIndex = 7;
            this.Servo_Slider4.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Servo4_Slider);
            // 
            // Servo1value
            // 
            this.Servo1value.AutoSize = true;
            this.Servo1value.Location = new System.Drawing.Point(243, 78);
            this.Servo1value.Name = "Servo1value";
            this.Servo1value.Size = new System.Drawing.Size(13, 13);
            this.Servo1value.TabIndex = 8;
            this.Servo1value.Text = "0";
            // 
            // Servo2Value
            // 
            this.Servo2Value.AutoSize = true;
            this.Servo2Value.Location = new System.Drawing.Point(243, 134);
            this.Servo2Value.Name = "Servo2Value";
            this.Servo2Value.Size = new System.Drawing.Size(13, 13);
            this.Servo2Value.TabIndex = 9;
            this.Servo2Value.Text = "0";
            // 
            // Servo3Value
            // 
            this.Servo3Value.AutoSize = true;
            this.Servo3Value.Location = new System.Drawing.Point(243, 192);
            this.Servo3Value.Name = "Servo3Value";
            this.Servo3Value.Size = new System.Drawing.Size(13, 13);
            this.Servo3Value.TabIndex = 10;
            this.Servo3Value.Text = "0";
            // 
            // Servo4Value
            // 
            this.Servo4Value.AutoSize = true;
            this.Servo4Value.Location = new System.Drawing.Point(243, 248);
            this.Servo4Value.Name = "Servo4Value";
            this.Servo4Value.Size = new System.Drawing.Size(13, 13);
            this.Servo4Value.TabIndex = 11;
            this.Servo4Value.Text = "0";
            // 
            // connection
            // 
            this.connection.Location = new System.Drawing.Point(321, 12);
            this.connection.Name = "connection";
            this.connection.Size = new System.Drawing.Size(75, 23);
            this.connection.TabIndex = 12;
            this.connection.Text = "Connect";
            this.connection.UseVisualStyleBackColor = true;
            this.connection.Click += new System.EventHandler(this.Connect);
            // 
            // s5
            // 
            this.s5.AutoSize = true;
            this.s5.Location = new System.Drawing.Point(26, 284);
            this.s5.Name = "s5";
            this.s5.Size = new System.Drawing.Size(93, 13);
            this.s5.TabIndex = 13;
            this.s5.Text = "Right Split Rudder";
            // 
            // s6
            // 
            this.s6.AutoSize = true;
            this.s6.Location = new System.Drawing.Point(26, 348);
            this.s6.Name = "s6";
            this.s6.Size = new System.Drawing.Size(62, 13);
            this.s6.TabIndex = 14;
            this.s6.Text = "Right V Tail";
            // 
            // s8
            // 
            this.s8.AutoSize = true;
            this.s8.Location = new System.Drawing.Point(26, 468);
            this.s8.Name = "s8";
            this.s8.Size = new System.Drawing.Size(86, 13);
            this.s8.TabIndex = 15;
            this.s8.Text = "Left Split Rudder";
            // 
            // s7
            // 
            this.s7.AutoSize = true;
            this.s7.Location = new System.Drawing.Point(26, 408);
            this.s7.Name = "s7";
            this.s7.Size = new System.Drawing.Size(94, 13);
            this.s7.TabIndex = 16;
            this.s7.Text = "Lower Right X Tail";
            // 
            // Servo_Slider7
            // 
            this.Servo_Slider7.LargeChange = 0;
            this.Servo_Slider7.Location = new System.Drawing.Point(171, 404);
            this.Servo_Slider7.Minimum = -100;
            this.Servo_Slider7.Name = "Servo_Slider7";
            this.Servo_Slider7.Size = new System.Drawing.Size(154, 17);
            this.Servo_Slider7.SmallChange = 0;
            this.Servo_Slider7.TabIndex = 17;
            this.Servo_Slider7.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Servo7_Slider);
            // 
            // Servo_Slider5
            // 
            this.Servo_Slider5.LargeChange = 0;
            this.Servo_Slider5.Location = new System.Drawing.Point(171, 284);
            this.Servo_Slider5.Minimum = -100;
            this.Servo_Slider5.Name = "Servo_Slider5";
            this.Servo_Slider5.Size = new System.Drawing.Size(154, 17);
            this.Servo_Slider5.SmallChange = 0;
            this.Servo_Slider5.TabIndex = 18;
            this.Servo_Slider5.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Servo5_Slider);
            // 
            // Servo_Slider8
            // 
            this.Servo_Slider8.LargeChange = 0;
            this.Servo_Slider8.Location = new System.Drawing.Point(171, 464);
            this.Servo_Slider8.Minimum = -100;
            this.Servo_Slider8.Name = "Servo_Slider8";
            this.Servo_Slider8.Size = new System.Drawing.Size(154, 17);
            this.Servo_Slider8.SmallChange = 0;
            this.Servo_Slider8.TabIndex = 19;
            this.Servo_Slider8.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Servo8_Slider);
            // 
            // Servo_Slider6
            // 
            this.Servo_Slider6.LargeChange = 0;
            this.Servo_Slider6.Location = new System.Drawing.Point(171, 344);
            this.Servo_Slider6.Minimum = -100;
            this.Servo_Slider6.Name = "Servo_Slider6";
            this.Servo_Slider6.Size = new System.Drawing.Size(154, 17);
            this.Servo_Slider6.SmallChange = 0;
            this.Servo_Slider6.TabIndex = 20;
            this.Servo_Slider6.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Servo6_Slider);
            // 
            // Servo8value
            // 
            this.Servo8value.AutoSize = true;
            this.Servo8value.Location = new System.Drawing.Point(243, 494);
            this.Servo8value.Name = "Servo8value";
            this.Servo8value.Size = new System.Drawing.Size(13, 13);
            this.Servo8value.TabIndex = 21;
            this.Servo8value.Text = "0";
            // 
            // Servo7value
            // 
            this.Servo7value.AutoSize = true;
            this.Servo7value.Location = new System.Drawing.Point(243, 431);
            this.Servo7value.Name = "Servo7value";
            this.Servo7value.Size = new System.Drawing.Size(13, 13);
            this.Servo7value.TabIndex = 22;
            this.Servo7value.Text = "0";
            // 
            // Servo6value
            // 
            this.Servo6value.AutoSize = true;
            this.Servo6value.Location = new System.Drawing.Point(243, 373);
            this.Servo6value.Name = "Servo6value";
            this.Servo6value.Size = new System.Drawing.Size(13, 13);
            this.Servo6value.TabIndex = 23;
            this.Servo6value.Text = "0";
            // 
            // Servo5value
            // 
            this.Servo5value.AutoSize = true;
            this.Servo5value.Location = new System.Drawing.Point(243, 312);
            this.Servo5value.Name = "Servo5value";
            this.Servo5value.Size = new System.Drawing.Size(13, 13);
            this.Servo5value.TabIndex = 24;
            this.Servo5value.Text = "0";
            // 
            // ServoTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(408, 530);
            this.Controls.Add(this.Servo5value);
            this.Controls.Add(this.Servo6value);
            this.Controls.Add(this.Servo7value);
            this.Controls.Add(this.Servo8value);
            this.Controls.Add(this.Servo_Slider6);
            this.Controls.Add(this.Servo_Slider8);
            this.Controls.Add(this.Servo_Slider5);
            this.Controls.Add(this.Servo_Slider7);
            this.Controls.Add(this.s7);
            this.Controls.Add(this.s8);
            this.Controls.Add(this.s6);
            this.Controls.Add(this.s5);
            this.Controls.Add(this.connection);
            this.Controls.Add(this.Servo4Value);
            this.Controls.Add(this.Servo3Value);
            this.Controls.Add(this.Servo2Value);
            this.Controls.Add(this.Servo1value);
            this.Controls.Add(this.Servo_Slider4);
            this.Controls.Add(this.Servo_Slider3);
            this.Controls.Add(this.Servo_Slider2);
            this.Controls.Add(this.Servo_Slider1);
            this.Controls.Add(this.s4);
            this.Controls.Add(this.s2);
            this.Controls.Add(this.s3);
            this.Controls.Add(this.s1);
            this.Name = "ServoTest";
            this.Text = "Servos Test";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ServoTest_FormClosing);
            this.Load += new System.EventHandler(this.ServoTest_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label s1;
        private System.Windows.Forms.Label s3;
        private System.Windows.Forms.Label s2;
        private System.Windows.Forms.Label s4;
        private System.Windows.Forms.HScrollBar Servo_Slider1;
        private System.Windows.Forms.HScrollBar Servo_Slider2;
        private System.Windows.Forms.HScrollBar Servo_Slider3;
        private System.Windows.Forms.HScrollBar Servo_Slider4;
        private System.Windows.Forms.Label Servo1value;
        private System.Windows.Forms.Label Servo2Value;
        private System.Windows.Forms.Label Servo3Value;
        private System.Windows.Forms.Label Servo4Value;
        private System.Windows.Forms.Button connection;
        private System.Windows.Forms.Label s5;
        private System.Windows.Forms.Label s6;
        private System.Windows.Forms.Label s8;
        private System.Windows.Forms.Label s7;
        private System.Windows.Forms.HScrollBar Servo_Slider7;
        private System.Windows.Forms.HScrollBar Servo_Slider5;
        private System.Windows.Forms.HScrollBar Servo_Slider8;
        private System.Windows.Forms.HScrollBar Servo_Slider6;
        private System.Windows.Forms.Label Servo8value;
        private System.Windows.Forms.Label Servo7value;
        private System.Windows.Forms.Label Servo6value;
        private System.Windows.Forms.Label Servo5value;
    }
}