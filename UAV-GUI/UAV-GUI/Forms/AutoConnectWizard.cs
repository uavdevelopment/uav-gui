﻿using FileClasses.INI_Files;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GLobal;
namespace UAV_GUI.Forms
{
    public partial class AutoConnectWizard : Form
    {
        BiasinitMTNPanelForm myForm;
        public void addTextTothLogger(String message) {
            this.chooseConnectionTextBox.Text += "\r\n" +"[INFO:] "+ message;

        }
        public void addTextTotheNavStartLogger(String message)
        {
            this.navStartDatBox.Text += "\r\n" + message;
            navStartDatBox.SelectionStart = navStartDatBox.Text.Length;
            navStartDatBox.ScrollToCaret();
        }

         public void addTextTothInitLogger(String message) {
           // this.initiziationTextLogger.Text += "\r\n" + message;

        }
        startConnectionToMIB connection = new startConnectionToMIB();
        public AutoConnectWizard()
        {
            InitializeComponent();
            
        }

        private void wizardPage1_Commit(object sender, AeroWizard.WizardPageConfirmEventArgs e)
        {

        }

        

        private void button1_Click(object sender, EventArgs e)

        {
           // connection.formSetup_ = new UAV_GUI.Setup ();
            this.chooseConnectionTextBox.Text += "\r\n" + "Trying To Connect Waiting For a resposnes ... ";
            
            int result = connection.AutomaticStartConnectionWithMIB(this);
            if (result == 0)
            {
                this.addTextTothLogger("Connection established > Go for Initization ... ");
                this.connectWizard.SelectedPage.AllowNext = true;
                this.connectNavigationBoard.Enabled = false; 
            }
            else {
                this.addTextTothLogger("Connection not established > retry ... ");
                this.connectWizard.SelectedPage.AllowNext = false;
            }
           
            
            }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FormBiasSetup myForm = new FormBiasSetup(connection.serialCommunicationChannel);
            myForm.TopLevel = false;
            myForm.AutoScroll = true;
            this.biasinitPanel.Controls.Add(myForm);
            myForm.Show();
        }

        private void wizardPage2_Commit_1(object sender, AeroWizard.WizardPageConfirmEventArgs e)
        {
             myForm = new BiasinitMTNPanelForm(connection.serialCommunicationChannel);
            myForm.TopLevel = false;
            myForm.AutoScroll = true;
            this.biasinitPanel.Controls.Add(myForm);
            myForm.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
           
                if (this.connectWizard.SelectedPage.Equals(this.wizardPage3))
                {
                if (myForm.inited)
                {
                   
                    this.connectWizard.SelectedPage.AllowNext = true;
                }
                else {
                    
                    this.connectWizard.SelectedPage.AllowNext = false;
                }
            
            }
        }

        private void navStartTicks_Tick(object sender, EventArgs e)
        {
            if (this.connectWizard.SelectedPage.Equals(this.wizardPage4))
            {
                if (NavStartData.toMP==false)
                {

                    this.connectWizard.SelectedPage.AllowNext = false;
                    addTextTotheNavStartLogger("[INFO]:To the Nav Board with gps state "+NavStartData.GPSFix+" , " );
                    addTextTotheNavStartLogger("Lat = " + NavStartData.currentLat + " , Longt = " + NavStartData.currentLong ); 
                }
                else
                {

                    this.connectWizard.SelectedPage.AllowNext = true;
                }

            }
        }

        private void wizardPage4_Commit(object sender, AeroWizard.WizardPageConfirmEventArgs e)
        {
            
        }

        private void wizardPage4_Initialize(object sender, AeroWizard.WizardPageInitEventArgs e)
        {
            if (connection.StartNavigationForm() == 0)
            {
                navStartTicks.Enabled = true; 

            }else{
                navStartTicks.Enabled = false; 
                addTextTotheNavStartLogger("The Process was interrupted ... ");
            }
        }

        private void inckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (inckBox.Checked)
            {

               
                ticksForIniti.Enabled = false;
            }
            else {
                this.connectWizard.SelectedPage.AllowNext = inckBox.Checked;
                ticksForIniti.Enabled = true;
            }
            this.connectWizard.SelectedPage.AllowNext = inckBox.Checked;
        }

        private void wizardPage2_Initialize(object sender, AeroWizard.WizardPageInitEventArgs e)
        {
            this.connectWizard.SelectedPage.AllowNext = false;
        }

        private void retryNavButton_Click(object sender, EventArgs e)
        {
            wizardPage4_Initialize(null, null);
        }

        private void AutoConnectWizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                connection.serialCommunicationChannel.closeReadThread();
                connection.serialCommunicationChannel.Close();
            }
            catch
            {
                return;
            }
        }

        private void connectWizard_Finished(object sender, EventArgs e)
        {
            this.Close();
        }

        private void switchToAutopilotButton_Click(object sender, EventArgs e)
        {
           
            this.connectWizard.SelectedPage.AllowNext = true;
            this.connectNavigationBoard.Enabled = false;
            this.addTextTothLogger("Openning Serial Port ... ");
            if (connection.serialCommunicationChannel != null && connection.serialCommunicationChannel.isPortOpened() == true)
            {
                this.addTextTothLogger("Resetting Serial Port ... ");
                connection.serialCommunicationChannel.Close();
                connection.serialCommunicationChannel = null;
                System.Threading.Thread.Sleep(150);
            }

            try
            {
                connection.serialCommunicationChannel = new SerialCommunication(INIData.compPort);

                byte[] mySwitch = { 84, 80, 73, 0, 0, 0x0e, 0x0e, 14, 28 };
                      this.addTextTothLogger("Sending The Switch Packet [1] ... ");
                     connection.serialCommunicationChannel.Write(mySwitch);
                      System.Threading.Thread.Sleep(500);
                      this.addTextTothLogger("Sending The Switch Packet [2] ... ");
                     connection.serialCommunicationChannel.Write(mySwitch);
                      System.Threading.Thread.Sleep(500);
                      this.addTextTothLogger("Sending The Switch Packet [3] ... ");
                     connection.serialCommunicationChannel.Write(mySwitch);
                   connection.serialCommunicationChannel.Close();
                    NavStartData.toMP = true;
                    this.addTextTothLogger("Ground Station is Now Connected To AutoPilot ... ");
                    this.addTextTothLogger("Reset the system to back to the navigation board if needed ... ");    
                System.Windows.Forms.MessageBox.Show("Ground Station is Now Connected To AutoPilot", "Info");
                this.connectWizard.SelectedPage.AllowNext = true;
                this.connectWizard.SelectedPage.IsFinishPage = true;
            }
            catch (Exception exception)
            {
                String ErrTitle = "COM Error";
                this.addTextTothLogger("Error Occured ... ");
                String ErrMsg = "Unable to open connection to COM Port.";
                GlobalData.LogConsole(exception.Message);
                MessageBox.Show(ErrMsg, ErrTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.connectWizard.SelectedPage.AllowNext = false;
                return;
            }

        }
    }
}
