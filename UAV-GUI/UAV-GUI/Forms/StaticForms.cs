﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UAV_GUI.Forms.Main_Screen;
using UAV_GUI.Forms.Setup;
namespace UAV_GUI.Forms
{
    public static class StaticForms
    {
        public static FailurePatternSimulation myFailurePatternsToolstrip;
        public static TransmitFlyFile myFlyFileTransmisionForm;
        public static TransmitVRSFile myVRSReceiveForm;
        public static TransmitVRSFile myVRSTransmitForm;
        public static AGLForm myAglForm;
        public static SettingsMainForm SettingsForm;
        public static ToolsWindow toolsForm;
    }
}
