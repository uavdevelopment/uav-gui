﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET;
using Error_Reporting;
using MasterClasses;

namespace UAV_GUI
{
    public partial class Edit_wayPoint_W : Form
    {
        WayPoint wp_currnt;
        PointLatLng p_current;
       public List<double> RelativeWP;
       // public string Unit_Conversions ;
        public List<string> CommandList;
        public bool LatFlag = false;
        public bool LngFlag = false;
        public double climbAltitude;
        public double redius;
        public bool altitudeFlag = false;
        public bool rediusFlag = false;
        public string SelectedCommand = "";
        public bool OK = false;
        internal WayPoint Wp_currnt
        {
            get { return wp_currnt; }
            set { wp_currnt = value; }
           
        }



        public Edit_wayPoint_W(PointLatLng p)
        {
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
            p_current = p;
            toolBoxesUpdate();
         //   Unit_Conversions = "metric";
            textBox14.Text = "";
            textBox3.Text = ""; // redius
            RelativeWP = new List<double>();
        }

        public void setRelativeWP()
        {
            textBox1.Text = RelativeWP[0].ToString();
            textBox2.Text = RelativeWP[1].ToString();
        }

        public Edit_wayPoint_W()
        {
            InitializeComponent();
            
          //  Unit_Conversions = "metric";
            textBox14.Text = "";
            textBox3.Text = ""; // redius
            
        }

        void toolBoxesUpdate()
        {

            textBox7.Text = p_current.Lng.ToString();
            textBox8.Text = p_current.Lat.ToString();
           
        }

        private void Edit_wayPoint_W_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        void toFlyFileFn_Generation()
        {
            CommandList.Add(CommandsComboBox.Text);
            CommandList.Add(p_current.Lat.ToString());
            CommandList.Add(p_current.Lng.ToString());
        }

       

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                if (textBox1.Text.Contains(".") || textBox2.Text.Contains(".") || textBox7.Text.Contains(".") || textBox8.Text.Contains("."))
                {
                    decimal.Parse(textBox1.Text);
                    decimal.Parse(textBox2.Text);
                    decimal.Parse(textBox7.Text);
                    decimal.Parse(textBox8.Text);
                }
                else
                {
                    int.Parse(textBox1.Text);
                    int.Parse(textBox2.Text);
                    int.Parse(textBox7.Text);
                    int.Parse(textBox8.Text);
                }
            }
            catch (Exception ev)
            {
                throw new BusinessException(1001, "Can Accepts Numbers Only");
            }
           
            if (textBox1.Text == string.Empty || textBox2.Text == string.Empty || textBox7.Text == string.Empty || textBox8.Text == string.Empty)
            {
                throw new BusinessException(1002, "Values Can not be Empty");
            }
            if (CommandsComboBox.Text.Equals("circleLeft") || CommandsComboBox.Text.Equals("circleRight"))
            {
                if (textBox3.Text == string.Empty)
                { throw new BusinessException(1003, "Value of Radius Can not be Empty"); }
                try { 
                    if (textBox3.Text.Contains("."))
                    decimal.Parse(textBox3.Text);
                    else
                        int.Parse(textBox3.Text);

                }
                catch (Exception ev)
                {
                    throw new BusinessException(1004, "Can Accepts Numbers Only");
                }
            }
            if (checkBox1.Checked)
            {
                if (textBox14.Text == string.Empty)
                { throw new BusinessException(1005, "Value of Altitude Can not be Empty"); }
                else
                {
                    try
                    {
                        if (textBox14.Text.Contains("."))
                            decimal.Parse(textBox3.Text);
                        else
                            int.Parse(textBox14.Text);

                    }
                    catch (Exception ev)
                    {
                        throw new BusinessException(1006, "Can Accepts Numbers Only");
                    }
                }
            }
                     
           

                if (button1.Enabled) // ok button
                {
                    CommandList = new List<string>();
                    //  this.Unit_Conversions = UnitConversionsComboBox.Text;
                    this.toFlyFileFn_Generation();

                    if (textBox14.Text != "")
                    {
                        climbAltitude = double.Parse(textBox14.Text);
                        altitudeFlag = true;
                    }
                    if (textBox3.Text != "")
                    {
                        redius = double.Parse(textBox3.Text);
                        rediusFlag = true;
                    }

                    OK = true;
                    SelectedCommand = CommandsComboBox.Text;
                    this.Close();
                }
            
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
           if (button2.Enabled) // Cancel button
              {
                this.Close();
              }
        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            
            if (CommandsComboBox.Text.Equals("FlyTo") || CommandsComboBox.Text.Equals("fromTo") || CommandsComboBox.Text.Equals("HoverAt"))
            {
                checkBox5.Visible = false;
                checkBox6.Visible = false;
            }
            else if (CommandsComboBox.Text.Equals("circleLeft"))
            {
                checkBox5.Visible = true;
                checkBox6.Visible = true;
                checkBox5.Checked = true;
            }
            else if (CommandsComboBox.Text.Equals("circleRight"))
            {
                checkBox5.Visible = true;
                checkBox6.Visible = true;
                checkBox6.Checked = true;
            }
            

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            LngFlag = true;
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                groupBox3.Visible = true;
            }
            else if (! radioButton1.Checked)
            {
                groupBox3.Visible = false;
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                groupBox1.Visible = true;
            }
            if (!radioButton2.Checked)
            {
                groupBox1.Visible = false;
            }
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                textBox14.Visible = true;
            }
            else if (! checkBox1.Checked)
            {
                textBox14.Visible = false;
            }
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox5.Checked)
            {
                checkBox6.Checked = false;
            }
            if (checkBox5.Checked)
            {
                //comboBox1.Text.Equals("circleLeft");
                CommandsComboBox.CanSelect.Equals("circleLeft");
            }
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox6.Checked)
            {
                checkBox5.Checked = false;
            }
            if (checkBox6.Checked)
            {
                CommandsComboBox.Text.Equals("circleRight");
                

            }
        }

       
        private void textBox8_TextChanged(object sender, EventArgs e)
        {
           // if(textBox8.TextChanged)
            LatFlag = true;
        }

        private void textBox14_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }


     
        //private void textChangedEventHandler(object sender, TextChangedEventArgs args)
        //{

        //}

    }
}
