
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET;
using System.IO;
using System.Text.RegularExpressions;
using UAV_GUI.FileClasses;
using UAV_GUI.Forms;
using MasterClasses;
using GMap.NET.MapProviders;
using Error_Reporting;
using UAV_GUI.FileClasses.FlyFiles;
using UAV_GUI.FlightClasses;



namespace UAV_GUI
{

    public partial class Form2 : Form
    {
        #region Global Variables : 

        bool righTMousePressed = false;
        GMapMarker selected = null;
        bool isMouseDown = false;
        bool isMouseDraging = false;
        GMapOverlay markersOverlay;
        List<PointLatLng> points;
        GMapRoute route ;
        GMapOverlay routesOverlay;
        ToolStripMenuItem Delete_WP = new ToolStripMenuItem("Delete WP");
        ToolStripMenuItem Clear_Mission = new ToolStripMenuItem("Clear Mission");
        ToolStripMenuItem Insert_WP = new ToolStripMenuItem("Insert WP");
        List<GMapMarker> markersList = new List<GMapMarker>();
        List<GMapRoute> routeList = new List<GMapRoute>();
        bool selectedMarkerDeleted = false;
        Edit_wayPoint_W edit_WayPoint =new Edit_wayPoint_W();
        PointLatLng deleteClickPosition;
        bool selectedMarkerFlag = false;
        GMapMarker selectedMarker = null;
        Point dragPoint = Point.Empty;
        bool append = false;
        List<string> txtLines;
        List<WayPoint> wayPointList;
         List<Command> commandsList;
         List<double> CommandArg = new List<double>();
         FlyFileParser f;
         public PointLatLng origin;
         public string Unit_Conversions = "metric";
         public FlightFailuerPatterns_W FlightFailuerPatterns  = new FlightFailuerPatterns_W();
         int trackBarValue = 14;
         public TextBox latTooltip = new TextBox();
         public TextBox lngTooltip = new TextBox();
         bool saveFlag = true;
         UAV_GUI.Forms.Flight_Plan.Predefined_Patterns_W predefinedPatternForm;
         public Dictionary<int, string> writtenPattrensFP = new Dictionary<int, string>();
         bool seconed = false;
        #endregion

        public Form2()
        {
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
            ApplicationLookAndFeel.ApplyTheme(WayPointPanel);
            this.gmap.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.map1_MouseClick_);
            markersOverlay = new GMapOverlay("markers");
            gmap.Overlays.Add(markersOverlay);
            button8.Click += new EventHandler(this.Edit_Click);
            gmap.MouseMove += new MouseEventHandler(MainMap_MouseMove);
            gmap.OnMarkerClick += new MarkerClick(this.on_marker_click);
            gmap.OnMarkerEnter += new MarkerEnter(this.on_marker_enter);
            gmap.OnMarkerLeave += new MarkerLeave(this.on_marker_leave);
            gmap.MouseDown += new MouseEventHandler(MainMap_MouseDown);
            gmap.MouseUp += new MouseEventHandler(MainMap_MouseUp);
            points = new List<PointLatLng>();
            route = new GMapRoute(points, "route");
            contextMenuStrip1.Items.AddRange(new ToolStripItem[] { Delete_WP, Clear_Mission, Insert_WP });
         
            Insert_WP.Click += new System.EventHandler(this.Insert_WP_Click);
            trackBar1.ValueChanged += new System.EventHandler(TrackBar1_ValueChanged);
             f = new FlyFileParser();
             textBox7.ReadOnly = true;
             textBox8.ReadOnly = true;
             textBox1.Text = f.orlat.ToString();
             textBox2.Text = f.oralt.ToString();
             textBox3.Text = f.orlong.ToString();
             mapProviderComboBox.DataSource =   GMapProviders.List.ToArray();
             WindowState = FormWindowState.Maximized;
           //  predefinedPatternForm= new UAV_GUI.Forms.Flight_Plan.Predefined_Patterns_W(f.filePatterns);
             UnitConversionsComboBox.SelectedIndex = 1;
        }

        private void Form2_Load(object sender, EventArgs e)
        {

           gmap.MapProvider = GMap.NET.MapProviders.GoogleTerrainMapProvider.Instance;
           mapProviderComboBox.SelectedItem = gmap.MapProvider;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerOnly;
            gmap.Position = new PointLatLng(f.orlat, f.orlong);
            origin = gmap.Position;
            gmap.Zoom = 14;

            routesOverlay = new GMapOverlay("route");
            routesOverlay.Routes.Add(route);
            gmap.Overlays.Add(routesOverlay);
            wayPointList = new List<WayPoint>();
            commandsList = new List<Command>();

            trackBar1.Value = (int)gmap.Zoom;
            trackBar1.Minimum = 14;
            trackBar1.Maximum = 30;
        }

        private void map1_MouseClick_(object sender, MouseEventArgs e) {

            if (textBox1.Text == "" || textBox3.Text == "")
                MessageBox.Show(" Can't Add WP Befor Setting the Home Location");
            else
            {
                saveFlag = false;
                PointLatLng p = gmap.FromLocalToLatLng(e.X, e.Y);
               
                WayPoint n = MapOptions.addWP(p, ref route, ref routeList,
                                        ref markersOverlay, ref markersList, ref gmap);
                CommandArg = new List<double>();
                int RelativePoint = (int)f.getLongitudeAbs(p.Lng);
                CommandArg.Add(RelativePoint);
                RelativePoint = (int)f.getLatitudeAbs(p.Lat);
                CommandArg.Add(RelativePoint);
                Command comd = new Command(MasterClasses.CommandType.basic, CommandName.flyTo, CommandArg);
                n.commandsList.Add(comd);
                n.manualAddedWP = true;
                wayPointList.Add(n);
            }

        }

        void MainMap_MouseMove(object sender, MouseEventArgs e)
        {
     
            //drag
            if (e.Button == MouseButtons.Left && isMouseDown && Control.ModifierKeys != Keys.Alt)
            {
                PointLatLng point = gmap.FromLocalToLatLng(e.X, e.Y);

                isMouseDraging = true;
                if (selectedMarker != null && selectedMarkerFlag)
                {
                    saveFlag = false;
                    List<int> index = updateLatLngByDrag_inFile();
                    MapOptions.Drag(ref  route, ref  selectedMarker,  point, ref  gmap);
                    updateToolBoxes();
                    updateWPList(index, point);
                    assignTxt_onSelectedMarker(ref selectedMarker);

                } 
            }
        }

        private void TrackBar1_ValueChanged(object sender, System.EventArgs e)
        {
            

            MapOptions.mapZooming(ref sender, ref gmap);
         
        }

      void  updateWPList( List<int> index ,PointLatLng point)
        {

            if (index.Count > 1)
            {
                wayPointList[index[0]].marker.Position = selectedMarker.Position;
                if (wayPointList[index[0]].commandsList[index[1]].CommandArgs.Count > 1)
                {
                    wayPointList[index[0]].commandsList[index[1]].CommandArgs[1] = point.Lat;
                    wayPointList[index[0]].commandsList[index[1]].CommandArgs[0] = point.Lng;
                    int RelativePoint = (int)f.getLongitudeAbs(wayPointList[index[0]].commandsList[index[1]].CommandArgs[0]);
                    wayPointList[index[0]].commandsList[index[1]].CommandArgs[0] = RelativePoint;
                      RelativePoint = (int)f.getLatitudeAbs(wayPointList[index[0]].commandsList[index[1]].CommandArgs[1]);
                      wayPointList[index[0]].commandsList[index[1]].CommandArgs[1] = RelativePoint;
                }
                wayPointList[index[0]].lat = point.Lat;
                wayPointList[index[0]].longt = point.Lng;
            }
        }

        List<int> updateLatLngByDrag_inFile()
        {
            List<int> WP_INDEX = new List<int>();
            for (int i = 0; i < wayPointList.Count; i++)
            {
                if (wayPointList[i].commandsList[0].name == CommandName.fixed_)
                    break;
                
                    if (wayPointList[i].marker.Position == selectedMarker.Position)
                    {

                        for (int j = 0; j < wayPointList[i].commandsList.Count; j++)
                        {
                            //commands that add new WP 
                            if (wayPointList[i].commandsList[j].name == CommandName.flyTo
                                || wayPointList[i].commandsList[j].name == CommandName.fromTo
                                || wayPointList[i].commandsList[j].name == CommandName.circleLeft
                                || wayPointList[i].commandsList[j].name == CommandName.circleRight
                                || wayPointList[i].commandsList[j].name == CommandName.hoverAt)
                            {
                                WP_INDEX.Add(i);
                                WP_INDEX.Add(j);
                                break;
                            }

                        }
                        if (WP_INDEX.Count != 0)
                            break;
                    }
                }
            if (WP_INDEX.Count != 0)
                return WP_INDEX;

            for (int i = 0; i < wayPointList.Count; i++)
            {
                if (wayPointList[i].manualAddedWP)
                {
                    if (wayPointList[i].marker.Position == selectedMarker.Position)
                    {

                        for (int j = 0; j < wayPointList[i].commandsList.Count; j++)
                        {
                            //commands that add new WP 
                            if (wayPointList[i].commandsList[j].name == CommandName.flyTo
                                || wayPointList[i].commandsList[j].name == CommandName.fromTo
                                || wayPointList[i].commandsList[j].name == CommandName.circleLeft
                                || wayPointList[i].commandsList[j].name == CommandName.circleRight
                                || wayPointList[i].commandsList[j].name == CommandName.hoverAt)
                            {
                                WP_INDEX.Add(i);
                                WP_INDEX.Add(j);
                                break;
                            }

                        }
                        if (WP_INDEX.Count != 0)
                            break;
                    }
                }
            }
            
            return WP_INDEX;
        }

       void MainMap_MouseDown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;
        }

        void MainMap_MouseUp(object sender, MouseEventArgs e)
        {
            righTMousePressed = false;
            isMouseDraging = false;
            isMouseDown = false;
        }


        void on_marker_click(GMapMarker sender, MouseEventArgs e)
        {   
            if (e.Button == MouseButtons.Left && Control.ModifierKeys == Keys.Alt)
            {
                
                MapOptions.MarkerSelection(ref  selectedMarkerFlag, ref  selectedMarker,
                                  ref  sender, ref  selectedMarkerDeleted,
                                 ref  WayPointPanel, ref  markersOverlay, ref  markersList);
                if (selectedMarker != null)
                {
                    updateToolBoxes();
                    assignTxt_onSelectedMarker(ref selectedMarker);
                }
              
            } 
        }

        void assignTxt_onSelectedMarker(ref GMapMarker marker) 
        {
            ToolTip toolTip1 = new ToolTip();
            toolTip1.BackColor = System.Drawing.Color.Blue;
   
             double lat =  Math.Round(f.getLatitudeAbs(double.Parse(textBox7.Text)));
             latTooltip.Text = lat.ToString();
           
            double lng = Math.Round(f.getLongitudeAbs(double.Parse(textBox8.Text)));
            lngTooltip.Text = lng.ToString();
            latTooltip.Visible =false;
            lngTooltip.Visible =false;
            toolTip1.SetToolTip(latTooltip, latTooltip.Text); // lat txtbox
            toolTip1.SetToolTip(lngTooltip, lngTooltip.Text); // lng txtbox
            // way point details
            marker.ToolTip = new GMap.NET.WindowsForms.ToolTips.GMapBaloonToolTip(marker);
            marker.ToolTipMode = MarkerTooltipMode.Always;
            marker.ToolTip.Font = new Font("Arial", 8, FontStyle.Regular);
            //marker.ToolTip.Foreground 

            //marker.ToolTip.color
            
            if(edit_WayPoint.altitudeFlag == true)
            {

                marker.ToolTipText = "Lat =" + latTooltip.Text + " lng = " + lngTooltip.Text + " altitude " + edit_WayPoint.textBox14.Text;
            }
            else if (edit_WayPoint.altitudeFlag == false)
            {
                marker.ToolTipText = "Lat ="  + latTooltip.Text + " lng = " +lngTooltip.Text;// marker.Position.ToString();
            }

        }

        void setAltitude_toWP(ref GMapMarker marker) 
        {
            marker.ToolTip = new GMap.NET.WindowsForms.ToolTips.GMapBaloonToolTip(marker);
            marker.ToolTipText = " altitude " + edit_WayPoint.textBox14.Text;
        }

        void addingClimpCommandToList()
        {
           
            for (int i = 0; i < wayPointList.Count; i++)
            {
                if (wayPointList[i].marker != null)
                {
                    if (wayPointList[i].marker.Position == selectedMarker.Position)
                  
                    {

                        List<double> args = new List<double>();
                        args.Add(edit_WayPoint.climbAltitude);
                        Command cmd = new Command(MasterClasses.CommandType.basic, CommandName.climb, args);

                        int j;
                        for (j = wayPointList[i].commandsList.Count - 1; j >= 0; j--)
                        {

                            if (wayPointList[i].commandsList[j].name == CommandName.flyTo
                                || wayPointList[i].commandsList[j].name == CommandName.fromTo
                                || wayPointList[i].commandsList[j].name == CommandName.circleLeft
                                || wayPointList[i].commandsList[j].name == CommandName.circleRight
                                || wayPointList[i].commandsList[j].name == CommandName.hoverAt)
                                break;
                        }

                        insertAt(i, j - 1, cmd);

                        break;

                    }
                }
            }
                }

        void addingAdvancedCommands_circleLR_ToList()
        {
            List<double> args = new List<double>();
            args.Add(edit_WayPoint.redius);
            Command cmd = new Command(MasterClasses.CommandType.Advanced, CommandName.circleLeft, args);
        }

        void insertAt(int WP_listIndex, int index , Command cmd)
        {
            List<Command> tmpList = new List<Command>();
            bool BREAK = false;
            Command tmp;
            if (index < 0)
                index = 0;         
                tmp = wayPointList[WP_listIndex].commandsList[index];
                if (tmp.name == CommandName.climb)
                {
                    wayPointList[WP_listIndex].commandsList[index].CommandArgs.Clear();
                    wayPointList[WP_listIndex].commandsList[index].CommandArgs = cmd.CommandArgs;
                    BREAK = true;
                }
                else if (tmp.name == CommandName.waitclimb)
                {
                    if (index != 0)
                    {
                        if (wayPointList[WP_listIndex].commandsList[index - 1].name == CommandName.climb)
                        {
                            wayPointList[WP_listIndex].commandsList[index - 1].CommandArgs.Clear();
                            wayPointList[WP_listIndex].commandsList[index - 1].CommandArgs = cmd.CommandArgs;
                            BREAK = true;
                        }
                    }
                }

                if (!BREAK)
                {
                    for (int i = 0; i < wayPointList[WP_listIndex].commandsList.Count; i++)
                    {


                        if (i == index)
                        {
                            tmpList.Add(cmd);
                        }
                        tmpList.Add(wayPointList[WP_listIndex].commandsList[i]);
                    }
                    wayPointList[WP_listIndex].commandsList = tmpList;
                }
        }

        void updateMap_OnLatLng_Changed()
        {
            // edit in the map
            double lat = double.Parse(edit_WayPoint.textBox8.Text);
            double lng = double.Parse(edit_WayPoint.textBox7.Text);
            PointLatLng point = new PointLatLng(lat, lng);
            route.Points[route.Points.IndexOf(selectedMarker.Position)] = point;
            selectedMarker.Position = point;
            gmap.UpdateRouteLocalPosition(route);
        }

        void updateToolBoxes()
        {
            edit_WayPoint = new Edit_wayPoint_W(selectedMarker.Position);
            this.textBox7.Text = selectedMarker.Position.Lat.ToString();
            this.textBox8.Text = selectedMarker.Position.Lng.ToString();
            edit_WayPoint.RelativeWP.Add(f.getLatitudeAbs(selectedMarker.Position.Lat));
            edit_WayPoint.RelativeWP.Add(f.getLongitudeAbs(selectedMarker.Position.Lng));
            edit_WayPoint.setRelativeWP();
        }  

        private void on_marker_enter(GMapMarker sender)
        {
                 selected = (GMapMarker)sender;
            if (sender is GMapMarkerRect)
            {
                GMapMarkerRect rc = (GMapMarkerRect)sender;
                rc.Pen.Color = Color.Red;
             
            }
            
        }

        private void on_marker_leave(GMapMarker sender)
        {
         
        }



        private void Clear_Map()
        {
           MapOptions.clearMap( ref  markersList,ref  markersOverlay,
                              ref  route, ref  routeList,
                              ref  gmap, ref  selectedMarker, 
                             ref  selectedMarkerFlag , ref wayPointList);
           f.load = false;
           f.FP.failurePattern = new string[7];
           FlightFailuerPatterns.MmultiLineFailuerText.FP = new string [7];
           FlightFailuerPatterns.FP = new string[7];
          
        }

        private void Delete_Click_1(object sender, EventArgs e)
        {
            MapOptions.deleteMarker(ref  selectedMarker, ref wayPointList
                                 , ref  route, ref routeList
                                 , ref  markersOverlay, ref markersList,
                                  ref  gmap, ref  selectedMarkerDeleted
                                  , ref  selectedMarkerFlag, ref  WayPointPanel);
     
        }

        private void Insert_WP_Click(object sender, EventArgs e)
        {


        }

        void setCommandComboBox()
        {
            for (int i = 0; i < wayPointList.Count; i++)
            {
                
                if (wayPointList[i].marker != null)
                {
                    if (wayPointList[i].marker.Position == selectedMarker.Position)
                    {

                        for (int j = 0; j < wayPointList[i].commandsList.Count; j++)
                        {

                            if (wayPointList[i].commandsList[j].name == CommandName.flyTo)
                            {
                                edit_WayPoint.CommandsComboBox.Text = "flyTo";
                                edit_WayPoint.SelectedCommand = "flyTo";
                            }
                            else if (wayPointList[i].commandsList[j].name == CommandName.fromTo)
                            {
                                edit_WayPoint.CommandsComboBox.Text = "fromTo";
                                edit_WayPoint.SelectedCommand = "fromTo";
                            }
                            else if (wayPointList[i].commandsList[j].name == CommandName.circleLeft)
                            {
                                edit_WayPoint.CommandsComboBox.Text = "circleLeft";
                                edit_WayPoint.SelectedCommand = "circleLeft";
                                edit_WayPoint.textBox3.Text = wayPointList[i].commandsList[j].CommandArgs[2].ToString();
                            }
                            else if (wayPointList[i].commandsList[j].name == CommandName.circleRight)
                            {
                                edit_WayPoint.CommandsComboBox.Text = "circleRight";
                                edit_WayPoint.SelectedCommand = "circleRight";
                                edit_WayPoint.textBox3.Text = wayPointList[i].commandsList[j].CommandArgs[2].ToString();
                            }
                            else if (wayPointList[i].commandsList[j].name == CommandName.hoverAt)
                            {
                                edit_WayPoint.CommandsComboBox.Text = "hoverAt";
                                edit_WayPoint.SelectedCommand = "hoverAt";
                            }

                        }
                    }
                }
            }
        }

        private void Edit_Click(object sender, EventArgs e)
        {
            setCommandComboBox();
            edit_WayPoint.ShowDialog();

            
           List<int> index =  updateLatLngByDrag_inFile();

            handelTheSelectedCommands();

            if (edit_WayPoint.altitudeFlag == true && selectedMarker != null)
            {
                MessageBox.Show("climb");
                selectedMarker.ToolTipText +=" altitude " + edit_WayPoint.textBox14.Text;
                addingClimpCommandToList();

            }

            // Check if the value of lat or lng is changed to update the map
            if ((edit_WayPoint.LatFlag || edit_WayPoint.LngFlag) & selectedMarker != null && edit_WayPoint.OK)
            {
                updateMap_OnLatLng_Changed();
                PointLatLng point = new PointLatLng(double.Parse(edit_WayPoint.textBox8.Text), double.Parse(edit_WayPoint.textBox7.Text));
                updateWPList(index , point);
            }
            if (edit_WayPoint.rediusFlag) 
            {
                addingAdvancedCommands_circleLR_ToList();
            }



            updateToolBoxes();
            assignTxt_onSelectedMarker(ref selectedMarker);

            saveFlag = false; 
        }

        private void SaveFlyFile_Click(object sender, EventArgs e)
        {
            //writtenPattrensFP = predefinedPatternForm.writtenPattrens;
            SAVE();
           
        }

        void SAVE()
        {
            saveFlag = true;
            this.Unit_Conversions = UnitConversionsComboBox.Text;
            f.Unit_ConversionsCombo = Unit_Conversions;
            if (f.load)
                f.flyFileGeneration2(edit_WayPoint, wayPointList, this.Text, writtenPattrensFP);
            else
                f.flyFileGeneration(edit_WayPoint, wayPointList, this.Text, writtenPattrensFP);

            if (f.save == false)
            {
                saveFlag = false;
                MessageBox.Show(" File not Saved ");
            }
            else
            {
                MessageBox.Show(" File Saved Successfully ");
                this.Text = f.saved_fName[f.saved_fName.Length - 1] + " - Flight Plan "; 
            }
        }

        #region NOT Used Functions :

        private void gmap_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void SetHomeLocation_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text.Contains(".") || textBox2.Text.Contains(".") || textBox3.Text.Contains("."))
                {
                    decimal.Parse(textBox1.Text);
                    decimal.Parse(textBox2.Text);
                    decimal.Parse(textBox3.Text);
                }
                else
                {
                    int.Parse(textBox1.Text);
                    int.Parse(textBox2.Text);
                    int.Parse(textBox3.Text);
                }
            }
            catch (Exception ev)
            {
                throw new BusinessException(1008, "Can Accept Numbers Only");
            }

            if (textBox1.Text == string.Empty || textBox2.Text == string.Empty || textBox3.Text == string.Empty)
            {
                throw new BusinessException(1009, "Value Can not be Empty");
            }
            else
            {
                originSet();
                MessageBox.Show("Home Location Set Sucssfully");
            }
            
               
        }

        private void panel1_CloseClick(object sender, EventArgs e)
        {

        }

        private void flickerFreePanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {

        }



        private void menuStrip1_ItemClicked(object sender, ToolStripItem ItemClicked, EventArgs e)
        {

        }
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void label10_Click (object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            WayPointPanel.Visible = false;
            Clear_Map();
          

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        #endregion

        void originSet()
        {
            if (!textBox1.Text.Equals(""))
                f.orlat = double.Parse(textBox1.Text);
            if (!textBox2.Text.Equals(""))
                f.oralt = double.Parse(textBox2.Text);
            if (!textBox3.Text.Equals(""))
                f.orlong = double.Parse(textBox3.Text);
            if (textBox1.Text != "" && textBox3.Text != "")
            {
                gmap.Position = new PointLatLng(f.orlat, f.orlong);
               
            }
        }

       public void checkForSaving()
        {
            if(!saveFlag)
                if (MessageBox.Show("Do You want to Save Map Changes ? ", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    SAVE();
                }
            
        }

        private void button1_Click(object sender, EventArgs e) {

            if (!saveFlag )
                checkForSaving();

            
           string file="";
          
           
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                f = new FlyFileParser();
                originSet();
                file = openFileDialog1.FileName;
                string [] fName = openFileDialog1.FileName.Split('\\');
                Clear_Map();
                f.load = true;
                saveFlag = false;
                this.Text = fName[fName.Length-1] + " - Flight Plan ";
                string text = File.ReadAllText(file);

            }
            else
                return;

            wayPointList.AddRange(f.parseFlyFile(@file));
            if (f.validFile == false)
                return ;
            if (f.safeLlocationSetted)
            {
               // textBox4.Text = origin.Lng.ToString();
                //textBox6.Text = origin.Lat.ToString();
            }
            textBox2.Text = f.oralt.ToString();
           
            bool fixed_ = false;
            for (int u = 0; u < wayPointList.Count; u++)
            {

              
                for (int m = 0; m < wayPointList[u].commandsList.Count; m++)
                {
                    if (wayPointList[u].commandsList[m].name == CommandName.fixed_)
                        fixed_ = true;
                }
                if (fixed_)
                    break;

                    System.IO.File.AppendAllText("1.txt", u + "\n");
                    PointLatLng p = new PointLatLng(wayPointList[u].lat, wayPointList[u].longt);
                    GMarkerGoogle nMarker = new GMarkerGoogle(p, GMarkerGoogleType.blue_small);
                    markersOverlay.Markers.Add(nMarker);
                    markersList.Add(nMarker);
                    route.Points.Add(p);
                    routeList.Add(route);
                    gmap.UpdateRouteLocalPosition(route);
                    route.Stroke.Color = Color.Turquoise;
                    route.Stroke.Width = 1;
                    righTMousePressed = true;
                    wayPointList[u].marker = nMarker;
             
                
            }
           
        }

        void drawPatterWP_OnMap(WayPoint PatternWP ,int patternNUM)
        {
           
            PointLatLng p = new PointLatLng(PatternWP.lat, PatternWP.longt);
            GMarkerGoogle nMarker = new GMarkerGoogle(p, GMarkerGoogleType.orange_small);
            markersOverlay.Markers.Add(nMarker);
            markersList.Add(nMarker);
            route.Points.Add(p);
            routeList.Add(route);
            gmap.UpdateRouteLocalPosition(route);
            route.Stroke.Color = Color.Turquoise;
            route.Stroke.Width = 1;
            righTMousePressed = true;
            nMarker.ToolTip = new GMap.NET.WindowsForms.ToolTips.GMapBaloonToolTip(nMarker);
            nMarker.ToolTipMode = MarkerTooltipMode.Always;
            nMarker.ToolTipText = "Pattern#"+patternNUM;
        }

        void setPattern_WPToMap()
        {

            for (int u = 0; u < wayPointList.Count; u++)
            {
               int  PatternNum =-1;
               
                for (int d= 0; d < wayPointList[u].commandsList.Count; d++)
                {
                    if (wayPointList[u].commandsList[d].name == CommandName.startpattern)
                    {
                        PatternNum++;
                        WayPoint PatternWP;
                        // if the first command  in the pattern add new wp so we draw this WP 
                        // (in code its 2nd 'cause the first command is definePattern)
                        
                        if (wayPointList[u].patternList[PatternNum].patternCommands[1].name == CommandName.flyTo)
                        {
                            PatternWP = new WayPoint(wayPointList[u].patternList[PatternNum].patternCommands[1].CommandArgs[1],
                                                      wayPointList[u].patternList[PatternNum].patternCommands[1].CommandArgs[0]);
                        }
                        else if (wayPointList[u].patternList[PatternNum].patternCommands[1].name == CommandName.fromTo)
                        {
                            PatternWP = new WayPoint(wayPointList[u].longt,wayPointList[u].lat);
                        }
                        // else it will assosiate with the startpattern
                        else
                        {
                            PatternWP = new WayPoint(wayPointList[u].longt, wayPointList[u].lat);
                        }
                        wayPointList[u].patternList[PatternNum].WP_On_Map = PatternWP;
                        drawPatterWP_OnMap(PatternWP, PatternNum);
                    }
                }
            }
        }

      
       

        private void chnageCommandName(string selectedCommand, int i, int j)
        {
            CommandArg = new List<double>();
            CommandArg.Add (wayPointList[i].commandsList[j].CommandArgs[0]);
            CommandArg.Add(wayPointList[i].commandsList[j].CommandArgs[1]);
            if (selectedCommand.Equals("circleLeft") || selectedCommand.Equals("circleRight"))
            {

                CommandArg.Add(edit_WayPoint.redius);
                if (selectedCommand.Equals("circleLeft"))
                    wayPointList[i].commandsList[j] = new Command(MasterClasses.CommandType.Advanced, CommandName.circleLeft, CommandArg);
                else if (selectedCommand.Equals("circleRight"))
                    wayPointList[i].commandsList[j] = new Command(MasterClasses.CommandType.Advanced, CommandName.circleRight, CommandArg);
                // ToolTip
                selectedMarker.ToolTip = new GMap.NET.WindowsForms.ToolTips.GMapBaloonToolTip(selectedMarker);
                selectedMarker.ToolTipMode = MarkerTooltipMode.Always;
                selectedMarker.ToolTipText = "Raduis:" + edit_WayPoint.redius;
                

            }
            else
            {
                if (selectedCommand.Equals("flyTo"))
                    wayPointList[i].commandsList[j] = new Command(MasterClasses.CommandType.Advanced, CommandName.flyTo, CommandArg);
                else if (selectedCommand.Equals("fromTo"))
                    wayPointList[i].commandsList[j] = new Command(MasterClasses.CommandType.Advanced, CommandName.fromTo, CommandArg);
                else if (selectedCommand.Equals("hoverAt"))
                    wayPointList[i].commandsList[j] = new Command(MasterClasses.CommandType.Advanced, CommandName.hoverAt, CommandArg);


            }
        }

        void handelTheSelectedCommands()
        {

            string selectedCommand = edit_WayPoint.SelectedCommand;
            bool BREAK = false;
            for (int i = 0; i < wayPointList.Count; i++)
            {
                
                if (wayPointList[i].marker != null)
                {
                    if (wayPointList[i].marker.Position == selectedMarker.Position)
                    {

                        for (int j = 0; j < wayPointList[i].commandsList.Count; j++)
                        {

                            if (wayPointList[i].commandsList[j].name == CommandName.flyTo
                                || wayPointList[i].commandsList[j].name == CommandName.fromTo
                                || wayPointList[i].commandsList[j].name == CommandName.circleLeft
                                || wayPointList[i].commandsList[j].name == CommandName.circleRight
                                || wayPointList[i].commandsList[j].name == CommandName.hoverAt)
                            {
                                chnageCommandName(selectedCommand, i, j);
                              
                                BREAK = true;
                                break;

                            }
                            
                        }

                    }
                    if (BREAK)
                        break;

                }

            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

       
        private void button10_Click(object sender, EventArgs e)
        {
          
            // this for to add the patterns in multilineText 
            for (int j = 0; j < f.FP.failurePattern.Length; j++)
            {
                if (f.FP.failurePattern[j] != null)
           
                    FlightFailuerPatterns.FP[j] = String.Copy(f.FP.failurePattern[j]);
            }

            FlightFailuerPatterns.MmultiLineFailuerText.FP = FlightFailuerPatterns.FP;
            FlightFailuerPatterns.ShowDialog();

            // this for for setting FP in file parser after doing changes in FlightFailuerPatterns form
            // to use it in wrtting the file
            for (int j = 0; j < f.FP.failurePattern.Length; j++)
            {   
                if (FlightFailuerPatterns.FP[j] != null)
                {     
                    f.FP.failurePattern[j] = String.Copy(FlightFailuerPatterns.FP[j]);
                }
            }

            
                saveFlag = false;  
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
              gmap.MapProvider = (GMapProvider)mapProviderComboBox.SelectedItem;
        }


        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            //if (seconed == false)
            //{
                predefinedPatternForm = new UAV_GUI.Forms.Flight_Plan.Predefined_Patterns_W(f.filePatterns);
              //  seconed = true;
            //}
            predefinedPatternForm.Show();
         f.filePatterns =   predefinedPatternForm.filePattern;
        }

        private void FlightPlan_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}

