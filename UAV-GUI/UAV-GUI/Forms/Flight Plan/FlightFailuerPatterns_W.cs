﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UAV_GUI.Forms;

namespace UAV_GUI.Forms
{
    public partial class FlightFailuerPatterns_W : Form
    {
        public MmultiLineFailuerText_W MmultiLineFailuerText = new MmultiLineFailuerText_W();
        public string AllFailuerPatterns = "";
        public string[] FP;// = new string [7] ;
        int patternNum ;//= -1;
        public FlightFailuerPatterns_W()
        {
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
            this.FormClosed += new FormClosedEventHandler(f_FormClosed);
            this.Controls.Add(MmultiLineFailuerText);
            MmultiLineFailuerText.Location = new Point(250, 70);
            MmultiLineFailuerText.Enabled = false;
            MmultiLineFailuerText.label1.Text = "Type Failure Pattren Here ...";
            FP = new string[7];
            patternNum = -1;
            MmultiLineFailuerText.Enabled = true;
            MmultiLineFailuerText.Save.MouseClick += new MouseEventHandler(saveClicked);
        }

        void f_FormClosed(object sender, FormClosedEventArgs e)
        {
            FP = MmultiLineFailuerText.FP;
           
            this.Close();
        }

        void saveClicked(object sender , MouseEventArgs args)
        {
           
            FP[MmultiLineFailuerText.currntPatternNum] = MmultiLineFailuerText.multiLineBox.Text;
            MmultiLineFailuerText.Enabled = false;
            MessageBox.Show("Failure Pattern Saved Successfully");
        }

        private void FlightFailuerPatterns_Load(object sender, EventArgs e)
        {

        }

        private void addFailuerPattern()
        {
            MmultiLineFailuerText.Enabled = true;
            MmultiLineFailuerText.multiLineBox.Text = FP[patternNum];
            MmultiLineFailuerText.currntPatternNum = patternNum;
       
        }

        private void button1_Click(object sender, EventArgs e)
        {

            patternNum = 0;
            addFailuerPattern();
           
        }

        private void button2_Click(object sender, EventArgs e)
        {

            patternNum = 1;
            addFailuerPattern();
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
    
            patternNum = 2;
            addFailuerPattern();
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
         
            patternNum = 3;
            addFailuerPattern();
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
    
            patternNum = 4;
            addFailuerPattern();
           
        }

        private void button6_Click(object sender, EventArgs e)
        {
   
            patternNum = 5;
            addFailuerPattern();
           
        }

        private void button7_Click(object sender, EventArgs e)
        {
      
            patternNum = 6;
            addFailuerPattern();
          
        }
       
    }
}
