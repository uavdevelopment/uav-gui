﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UAV_GUI.Forms.Flight_Plan
{
    public partial class Predefined_Patterns_W : Form
    {
        public MmultiLineFailuerText_W MmultiLineFailuerText = new MmultiLineFailuerText_W();
        
      public  string[] filePattern = new string[16];
       public Dictionary<int, string> writtenPattrens = new Dictionary<int, string>();
        public Predefined_Patterns_W(string[] filePattern)
        {
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
            this.Controls.Add(MmultiLineFailuerText);
            MmultiLineFailuerText.Visible = false;
            MmultiLineFailuerText.Location = new Point(10, 90);
            MmultiLineFailuerText.label1.Text = "";
            PredefinedPatterns.fillDictionaryToWrite();
            this.filePattern = filePattern;
          //  preDefinedPattremComboBox.SelectedIndex =0;
            pattrensComboBox.SelectedIndex = 0;
            MmultiLineFailuerText.Save.MouseClick += new MouseEventHandler(saveClick);
        }

       
       void saveClick(object sender, MouseEventArgs args)
       {
           
           if(! MmultiLineFailuerText.multiLineBox.Equals( ""))
           {
               filePattern[pattrensComboBox.SelectedIndex] = MmultiLineFailuerText.multiLineBox.Text;
             //  writtenPattrens.Add(pattrensComboBox.SelectedIndex , MmultiLineFailuerText.multiLineBox.Text);
               MessageBox.Show("Pattern Saved Successfully");
               MmultiLineFailuerText.Enabled = false;
           }
          // MmultiLineFailuerText.Visible = false;
         
          
       }

        private void Predefined_Patterns_Load(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (preDefinedPattremComboBox.SelectedIndex == 12)
                MmultiLineFailuerText.Visible = true;
            else
                MmultiLineFailuerText.Visible = false;
                 }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            MmultiLineFailuerText.Enabled = true;
            MmultiLineFailuerText.multiLineBox.Text = filePattern[pattrensComboBox.SelectedIndex];
   
            bool Found = false;
            if (filePattern[pattrensComboBox.SelectedIndex]==null) {
            preDefinedPattremComboBox.SelectedIndex=12; 
                return;
            }
            for (int i = 0; i < PredefinedPatterns.preDefinedPattrensToWrite.Count; i++)
            {

                if (filePattern[pattrensComboBox.SelectedIndex].Equals(PredefinedPatterns.preDefinedPattrensToWrite[preDefinedPattremComboBox.GetItemText(preDefinedPattremComboBox.Items[i])]))
                {
                    preDefinedPattremComboBox.SelectedIndex = i;
                    Found = true; 
                    break;
                }
                
            }
            if (Found == false)
            {
                preDefinedPattremComboBox.SelectedIndex = 12;
              //  MmultiLineFailuerText.Visible = false;
            }
                 
        }
    }
}
