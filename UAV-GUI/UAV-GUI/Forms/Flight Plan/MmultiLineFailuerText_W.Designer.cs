﻿namespace UAV_GUI.Forms
{
    partial class MmultiLineFailuerText_W
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.multiLineBox = new System.Windows.Forms.TextBox();
            this.Save = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // multiLineBox
            // 
            this.multiLineBox.Location = new System.Drawing.Point(80, 34);
            this.multiLineBox.Name = "multiLineBox";
            this.multiLineBox.Size = new System.Drawing.Size(237, 20);
            this.multiLineBox.TabIndex = 0;
            this.multiLineBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(95, 249);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(96, 27);
            this.Save.TabIndex = 1;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(211, 249);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 27);
            this.button2.TabIndex = 2;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Type The Failuer Pattern .... ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // MmultiLineFailuerText_W
            // 
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.multiLineBox);
            this.Size = new System.Drawing.Size(400, 288);
            this.Text = "Failuer Pattern";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox multiLineBox;
        public System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button button2;
        public System.Windows.Forms.Label label1;
    }
}