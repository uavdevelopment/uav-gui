﻿namespace UAV_GUI.Forms.Flight_Plan
{
    partial class Predefined_Patterns_W
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pattrensComboBox = new System.Windows.Forms.ComboBox();
            this.preDefinedPattremComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // pattrensComboBox
            // 
            this.pattrensComboBox.FormattingEnabled = true;
            this.pattrensComboBox.Items.AddRange(new object[] {
            "Pattern 0",
            "Pattern 1",
            "Pattern 2",
            "Pattern 3",
            "Pattern 4",
            "Pattern 5",
            "Pattern 6",
            "Pattern 7",
            "Pattern 8",
            "Pattern 9",
            "Pattern 10",
            "Pattern 11",
            "Pattern 12",
            "Pattern 13",
            "Pattern 14",
            "Pattern 15"});
            this.pattrensComboBox.Location = new System.Drawing.Point(31, 63);
            this.pattrensComboBox.Name = "pattrensComboBox";
            this.pattrensComboBox.Size = new System.Drawing.Size(125, 21);
            this.pattrensComboBox.TabIndex = 0;
            this.pattrensComboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // preDefinedPattremComboBox
            // 
            this.preDefinedPattremComboBox.FormattingEnabled = true;
            this.preDefinedPattremComboBox.Items.AddRange(new object[] {
            "Fly Home",
            "Orbit Right",
            "Orbit Left",
            "Figure8",
            "Take Picture",
            "Hover Here",
            "Descend Here",
            "Circle Right",
            "Circle Left",
            "Stop Engine",
            "Land at Home",
            "Land Here",
            "Else"});
            this.preDefinedPattremComboBox.Location = new System.Drawing.Point(241, 63);
            this.preDefinedPattremComboBox.Name = "preDefinedPattremComboBox";
            this.preDefinedPattremComboBox.Size = new System.Drawing.Size(125, 21);
            this.preDefinedPattremComboBox.TabIndex = 1;
            this.preDefinedPattremComboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Patterns";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(247, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Pre-defined Patterns";
            // 
            // Predefined_Patterns
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 354);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.preDefinedPattremComboBox);
            this.Controls.Add(this.pattrensComboBox);
            this.Name = "Predefined_Patterns";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Predefined_Patterns";
            this.Load += new System.EventHandler(this.Predefined_Patterns_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox pattrensComboBox;
        private System.Windows.Forms.ComboBox preDefinedPattremComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}