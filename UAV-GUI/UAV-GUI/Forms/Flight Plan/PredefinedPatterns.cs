﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAV_GUI.Forms.Flight_Plan
{
    static class PredefinedPatterns
    {
        public static Dictionary<string, string> preDefinedPattrens = new Dictionary<string, string>();
        public static Dictionary<string, string> preDefinedPattrensToWrite = new Dictionary<string, string>();

       //public static void  fillDictionary()

       // {

       //     preDefinedPattrens.Add("Orbit Right", "[rotatepattern]=[currentheading]flyto ( 500, 500)flyto ( 500,-500)flyto (-500,-500)flyto (-500, 500)repeat -4");
       //     preDefinedPattrens.Add("Orbit Left", "[rotatepattern]=[currentheading]flyto (-500, 500)flyto (-500,-500)flyto ( 500,-500)flyto ( 500, 500)repeat -4");
       //     preDefinedPattrens.Add("Figure8", "[rotatepattern]=[currentheading]flyto ( 500, 500)flyto (   0,   0)flyto (-500,-500)flyto ( 500,-500)flyto (   0,   0)flyto (-500, 500)repeat -6");
       //     preDefinedPattrens.Add("Take Picture", "[fservo9] = 32767wait 3[fservo9] = 0return");
       //     preDefinedPattrens.Add("Hover Here", "hoverat (0, 0)repeat -1");
       //     preDefinedPattrens.Add("Descend Here", "climb 100flyto (0, 0)repeat -1");
       //     preDefinedPattrens.Add("Circle Right", "skipequal [helicopter], 0, 3hoverat (0, 0)repeat -1[rotatepattern]=[currentheading]repeat -1");
       //     preDefinedPattrens.Add("Circle Left", "skipequal [helicopter], 0, 3hoverat (0, 0)repeat -1[rotatepattern]=[currentheading]repeat -1");
       //     preDefinedPattrens.Add("Fly Home", "hoverat [home]repeat -1");
       //     preDefinedPattrens.Add("Stop Engine", "flyto (0,0)repeat -1");
       //     preDefinedPattrens.Add("Land at Home", "circuitreturn");
       //     preDefinedPattrens.Add("Land Here", "circuit (0, 0), 0, 0return");

       //     fillDictionaryToWrite();
       // }
       public static void fillDictionaryToWrite()
       {
           preDefinedPattrensToWrite = new Dictionary<string, string>();
           preDefinedPattrensToWrite.Add("Orbit Right", "[rotatepattern]=[currentheading]\r\nflyto ( 500, 500)\r\nflyto ( 500,-500)\r\nflyto (-500,-500)\r\nflyto (-500, 500)\r\nrepeat -4\r\n");
           preDefinedPattrensToWrite.Add("Orbit Left", "[rotatepattern]=[currentheading]\r\nflyto (-500, 500)\r\nflyto (-500,-500)\r\nflyto ( 500,-500)\r\nflyto ( 500, 500)\r\nrepeat -4\r\n");  
           preDefinedPattrensToWrite.Add("Figure8", "[rotatepattern]=[currentheading]\r\nflyto ( 500, 500)\r\nflyto (   0,   0)\r\nflyto (-500,-500)\r\nflyto ( 500,-500)\r\nflyto (   0,   0)\r\nflyto (-500, 500)\r\nrepeat -6\r\n");
           preDefinedPattrensToWrite.Add("Take Picture", "[fservo9] = 32767\r\nwait 3\r\n[fservo9] = 0\r\nreturn\r\n");
           preDefinedPattrensToWrite.Add("Hover Here", "hoverat (0, 0)\r\nrepeat -1\r\n");
           preDefinedPattrensToWrite.Add("Descend Here", "climb 100\r\nflyto (0, 0)\r\nrepeat -1\r\n");
           preDefinedPattrensToWrite.Add("Circle Right", "skipequal [helicopter], 0, 3\r\nhoverat (0, 0)\r\nrepeat -1\r\n[rotatepattern]=[currentheading]\r\ncircleright (500, 0), 500\r\nrepeat -1\r\n");
           preDefinedPattrensToWrite.Add("Circle Left", "skipequal [helicopter], 0, 3\r\nhoverat (0, 0)\r\nrepeat -1\r\n[rotatepattern]=[currentheading]\r\ncircleleft (-500, 0), 500\r\nrepeat -1\r\n");
           preDefinedPattrensToWrite.Add("Fly Home", "hoverat [home]\r\nrepeat -1\r\n");
           preDefinedPattrensToWrite.Add("Stop Engine", "flyto (0,0)\r\nrepeat -1\r\n");
           preDefinedPattrensToWrite.Add("Land at Home", "circuit\r\nreturn\r\n");
           preDefinedPattrensToWrite.Add("Land Here", "circuit (0, 0), 0, 0\r\nreturn\r\n");

       }

    }
}
