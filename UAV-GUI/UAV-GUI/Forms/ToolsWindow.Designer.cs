﻿namespace UAV_GUI.Forms
{
    partial class ToolsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AGLB = new System.Windows.Forms.Button();
            this.cachingB = new System.Windows.Forms.Button();
            this.chParB = new System.Windows.Forms.Button();
            this.FailPB = new System.Windows.Forms.Button();
            this.TestServoB = new System.Windows.Forms.Button();
            this.AcVRSB = new System.Windows.Forms.Button();
            this.TVRSB = new System.Windows.Forms.Button();
            this.FlyFB = new System.Windows.Forms.Button();
            this.JoyStB = new System.Windows.Forms.Button();
            this.FlMB = new System.Windows.Forms.Button();
            this.AutoInitB = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AGLB
            // 
            this.AGLB.Location = new System.Drawing.Point(674, 7);
            this.AGLB.Name = "AGLB";
            this.AGLB.Size = new System.Drawing.Size(208, 51);
            this.AGLB.TabIndex = 62;
            this.AGLB.Text = "AGL Monitor";
            this.AGLB.UseVisualStyleBackColor = true;
            this.AGLB.Click += new System.EventHandler(this.AGLB_Click);
            // 
            // cachingB
            // 
            this.cachingB.Location = new System.Drawing.Point(674, 92);
            this.cachingB.Name = "cachingB";
            this.cachingB.Size = new System.Drawing.Size(208, 51);
            this.cachingB.TabIndex = 61;
            this.cachingB.Text = "Caching";
            this.cachingB.UseVisualStyleBackColor = true;
            this.cachingB.Click += new System.EventHandler(this.cachingB_Click);
            // 
            // chParB
            // 
            this.chParB.Location = new System.Drawing.Point(345, 7);
            this.chParB.Name = "chParB";
            this.chParB.Size = new System.Drawing.Size(208, 51);
            this.chParB.TabIndex = 60;
            this.chParB.Text = "Change Parameters";
            this.chParB.UseVisualStyleBackColor = true;
            this.chParB.Click += new System.EventHandler(this.chParB_Click);
            // 
            // FailPB
            // 
            this.FailPB.Location = new System.Drawing.Point(345, 271);
            this.FailPB.Name = "FailPB";
            this.FailPB.Size = new System.Drawing.Size(208, 51);
            this.FailPB.TabIndex = 59;
            this.FailPB.Text = "Failure Pattern Simulation";
            this.FailPB.UseVisualStyleBackColor = true;
            this.FailPB.Click += new System.EventHandler(this.FailPB_Click);
            // 
            // TestServoB
            // 
            this.TestServoB.Location = new System.Drawing.Point(345, 92);
            this.TestServoB.Name = "TestServoB";
            this.TestServoB.Size = new System.Drawing.Size(208, 51);
            this.TestServoB.TabIndex = 57;
            this.TestServoB.Text = "Test Servos";
            this.TestServoB.UseVisualStyleBackColor = true;
            this.TestServoB.Click += new System.EventHandler(this.TestServoB_Click);
            // 
            // AcVRSB
            // 
            this.AcVRSB.Location = new System.Drawing.Point(6, 92);
            this.AcVRSB.Name = "AcVRSB";
            this.AcVRSB.Size = new System.Drawing.Size(208, 51);
            this.AcVRSB.TabIndex = 56;
            this.AcVRSB.Text = "Acquire VRS File";
            this.AcVRSB.UseVisualStyleBackColor = true;
            this.AcVRSB.Click += new System.EventHandler(this.AcVRSB_Click);
            // 
            // TVRSB
            // 
            this.TVRSB.Location = new System.Drawing.Point(6, 187);
            this.TVRSB.Name = "TVRSB";
            this.TVRSB.Size = new System.Drawing.Size(208, 51);
            this.TVRSB.TabIndex = 55;
            this.TVRSB.Text = "Transmit VRS File";
            this.TVRSB.UseVisualStyleBackColor = true;
            this.TVRSB.Click += new System.EventHandler(this.TVRSB_Click);
            // 
            // FlyFB
            // 
            this.FlyFB.Location = new System.Drawing.Point(6, 271);
            this.FlyFB.Name = "FlyFB";
            this.FlyFB.Size = new System.Drawing.Size(208, 51);
            this.FlyFB.TabIndex = 54;
            this.FlyFB.Text = "Fly File Utility";
            this.FlyFB.UseVisualStyleBackColor = true;
            this.FlyFB.Click += new System.EventHandler(this.FlyFB_Click);
            // 
            // JoyStB
            // 
            this.JoyStB.Location = new System.Drawing.Point(674, 187);
            this.JoyStB.Name = "JoyStB";
            this.JoyStB.Size = new System.Drawing.Size(208, 51);
            this.JoyStB.TabIndex = 53;
            this.JoyStB.Text = "Joystick Controller";
            this.JoyStB.UseVisualStyleBackColor = true;
            this.JoyStB.Click += new System.EventHandler(this.JoyStB_Click);
            // 
            // FlMB
            // 
            this.FlMB.Location = new System.Drawing.Point(345, 187);
            this.FlMB.Name = "FlMB";
            this.FlMB.Size = new System.Drawing.Size(208, 51);
            this.FlMB.TabIndex = 52;
            this.FlMB.Text = "Flight Monitor";
            this.FlMB.UseVisualStyleBackColor = true;
            this.FlMB.Click += new System.EventHandler(this.FlMB_Click);
            // 
            // AutoInitB
            // 
            this.AutoInitB.Location = new System.Drawing.Point(6, 7);
            this.AutoInitB.Name = "AutoInitB";
            this.AutoInitB.Size = new System.Drawing.Size(204, 51);
            this.AutoInitB.TabIndex = 51;
            this.AutoInitB.Text = "Automatic Initialization";
            this.AutoInitB.UseVisualStyleBackColor = true;
            this.AutoInitB.Click += new System.EventHandler(this.AutoInitB_Click);
            // 
            // ToolsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 377);
            this.Controls.Add(this.AGLB);
            this.Controls.Add(this.cachingB);
            this.Controls.Add(this.chParB);
            this.Controls.Add(this.FailPB);
            this.Controls.Add(this.TestServoB);
            this.Controls.Add(this.AcVRSB);
            this.Controls.Add(this.TVRSB);
            this.Controls.Add(this.FlyFB);
            this.Controls.Add(this.JoyStB);
            this.Controls.Add(this.FlMB);
            this.Controls.Add(this.AutoInitB);
            this.Name = "ToolsWindow";
            this.Text = "ToolsWindow";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button AGLB;
        private System.Windows.Forms.Button cachingB;
        private System.Windows.Forms.Button chParB;
        private System.Windows.Forms.Button FailPB;
        private System.Windows.Forms.Button TestServoB;
        private System.Windows.Forms.Button AcVRSB;
        private System.Windows.Forms.Button TVRSB;
        private System.Windows.Forms.Button FlyFB;
        private System.Windows.Forms.Button JoyStB;
        private System.Windows.Forms.Button FlMB;
        private System.Windows.Forms.Button AutoInitB;
    }
}