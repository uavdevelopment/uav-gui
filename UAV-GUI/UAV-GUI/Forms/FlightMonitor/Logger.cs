﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UAV_GUI.MasterClasses;
using UAV_GUI.FlightMonitor;

namespace UAV_GUI


{

    public struct errorGrid
    {
        public FatalError error { get; set; }
        public string time { get; set; }
     
    }
    
    public  partial  class Logger : Form
    {
        public Logger()
        {
            InitializeComponent();
        }

        void collapseChanged(object sender, EventArgs arg)
        {
            collapse(ErrorsGridView1);
            collapse(warnGridView2);
        }

        

        void collapse(object sender)
        {
            DataGridView DataGridViewObj = sender as DataGridView;
            if (DataGridViewObj.RowCount <= 0) {
                return;
            }
            if (Collapse.Checked)
            {
                int max = (int)DataGridViewObj.Rows[0].Cells[0].Value;

                // get max code in ErrorsGridView1
                for (int i = 1; i < DataGridViewObj.RowCount; i++)
                {

                    if ((int)DataGridViewObj.Rows[i].Cells[0].Value > max)
                        max = (int)DataGridViewObj.Rows[i].Cells[0].Value;
                }

                //creat a list with max code
                List<errorGrid> errorList = new List<errorGrid>();
                errorGrid EG;
                for (int i = 0; i < max + 1; i++)
                {
                    EG = new errorGrid();
                    EG.error = new FatalError(0, "");
                    errorList.Add(EG);
                }

                // fill the list
                int cellValue;
                for (int i = 0; i < DataGridViewObj.RowCount; i++)
                {
                    cellValue = (int)DataGridViewObj.Rows[i].Cells[0].Value;
                    errorList[cellValue].error.Code++;
                    errorList[cellValue].error.Description = (string)DataGridViewObj.Rows[i].Cells[2].Value;
                    errorList[cellValue] = new errorGrid { error = errorList[cellValue].error, time = (string)DataGridViewObj.Rows[i].Cells[1].Value };
                }

                // rewrite the grid rows
                DataGridViewObj.Rows.Clear();


                for (int i = 0; i < errorList.Count; i++)
                {
                    if (errorList[i].error.Code != 0)
                    {
                        DataGridViewObj.Rows.Add(i, errorList[i].time, errorList[i].error.Description, errorList[i].error.Code);
                    }
                }

            }

            else
            {
                int rowCount = DataGridViewObj.RowCount;
                int code;
                string time;
                string desc;
                for (int i = 0; i < rowCount; i++)
                {
                    int x = (int)DataGridViewObj.Rows[i].Cells[3].Value;
                    if (x != 1)
                    {
                        for (int j = 0; j < x - 1; j++)
                        {
                            DataGridViewObj.Rows[i].Cells[3].Value = 1;
                            code = (int)DataGridViewObj.Rows[i].Cells[0].Value;
                            time = (string)DataGridViewObj.Rows[i].Cells[1].Value;
                            desc = (string)DataGridViewObj.Rows[i].Cells[2].Value;

                            DataGridViewObj.Rows.Add(code, time, desc, 1);
                        }
                    }
                }
            }
        }



        public void WriteError(int Code = -1, String Desc = "")
        {
            if (Collapse.Checked)
            {
                int index = findFirstIndexOfCode(Code, ErrorsGridView1);
                if (index == -1)
                {
                    ErrorsGridView1.Rows.Add(Code, DateTime.Now.ToString(), Desc, 1);
                }
                else { 
                     int prevValue = (int)ErrorsGridView1.Rows[index].Cells[3].Value;
                     ErrorsGridView1.Rows[index].Cells[3].Value = prevValue + 1; 
                }
            }
            else {
                ErrorsGridView1.Rows.Add(Code, DateTime.Now.ToString(), Desc, 1);
            }
          
        }
        private int findFirstIndexOfCode(int code,DataGridView current) {
           
            for (int i = 0; i < current.RowCount; i++) {
                if (current.Rows[i].Cells[0].Value == null) {
                    continue;
                }
                if (code == (int)current.Rows[i].Cells[0].Value) {
                    return i;
                }

            }
            return -1;
        }
        public void WriteError(FatalError error)
        {

            WriteError(error.Code, error.Description);

        }
        public void WriteWarning(int Code = -1, String Desc = "")
        {
            if (Collapse.Checked)
            {
                int index = findFirstIndexOfCode(Code, warnGridView2);
                if (index == -1)
                {
                    warnGridView2.Rows.Add(Code, DateTime.Now.ToString(), Desc, 1);
                }
                else {
                    int prevValue = (int)warnGridView2.Rows[index].Cells[3].Value;
                    warnGridView2.Rows[index].Cells[3].Value = prevValue + 1; 
                }
            }
            else {
                warnGridView2.Rows.Add(Code, DateTime.Now.ToString(), Desc, 1);
            }
            
        }
        public void WriteWarning(Warning warning)
        {
            WriteWarning(warning.Code, warning.Description);

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            ApplicationLookAndFeel.UseTheme(this);
            Collapse.Appearance = Appearance.Button;
            //Collapse.CheckedChanged += new EventHandler(collapseChanged);
           
            ErrorsGridView1.AllowUserToAddRows = false;
            warnGridView2.AllowUserToAddRows = false;
           
        }

        private void Logger_FormClosing(object sender, FormClosingEventArgs e)
        {
            
              this.Hide();
              e.Cancel = true; // this cancels the close event.

        }

        
    
      
    }
}
