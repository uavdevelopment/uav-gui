﻿namespace UAV_GUI
{
    partial class RC_Sensors_Monitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ch5_bar = new System.Windows.Forms.ProgressBar();
            this.th_bar = new System.Windows.Forms.ProgressBar();
            this.Ai_bar = new System.Windows.Forms.ProgressBar();
            this.El_bar = new System.Windows.Forms.ProgressBar();
            this.RU_bar = new System.Windows.Forms.ProgressBar();
            this.ch5_label = new System.Windows.Forms.Label();
            this.ai_label = new System.Windows.Forms.Label();
            this.el_label = new System.Windows.Forms.Label();
            this.ru_label = new System.Windows.Forms.Label();
            this.th_label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // ch5_bar
            // 
            this.ch5_bar.Location = new System.Drawing.Point(145, 33);
            this.ch5_bar.Name = "ch5_bar";
            this.ch5_bar.Size = new System.Drawing.Size(260, 23);
            this.ch5_bar.TabIndex = 0;
            // 
            // th_bar
            // 
            this.th_bar.Location = new System.Drawing.Point(145, 71);
            this.th_bar.Name = "th_bar";
            this.th_bar.Size = new System.Drawing.Size(260, 23);
            this.th_bar.TabIndex = 1;
            // 
            // Ai_bar
            // 
            this.Ai_bar.Location = new System.Drawing.Point(145, 111);
            this.Ai_bar.Name = "Ai_bar";
            this.Ai_bar.Size = new System.Drawing.Size(260, 23);
            this.Ai_bar.TabIndex = 2;
            // 
            // El_bar
            // 
            this.El_bar.Location = new System.Drawing.Point(145, 150);
            this.El_bar.Name = "El_bar";
            this.El_bar.Size = new System.Drawing.Size(260, 23);
            this.El_bar.TabIndex = 3;
            // 
            // RU_bar
            // 
            this.RU_bar.Location = new System.Drawing.Point(145, 191);
            this.RU_bar.Name = "RU_bar";
            this.RU_bar.Size = new System.Drawing.Size(260, 23);
            this.RU_bar.TabIndex = 4;
            // 
            // ch5_label
            // 
            this.ch5_label.AutoSize = true;
            this.ch5_label.Location = new System.Drawing.Point(260, 39);
            this.ch5_label.Name = "ch5_label";
            this.ch5_label.Size = new System.Drawing.Size(13, 13);
            this.ch5_label.TabIndex = 5;
            this.ch5_label.Text = "0";
            // 
            // ai_label
            // 
            this.ai_label.AutoSize = true;
            this.ai_label.Location = new System.Drawing.Point(259, 117);
            this.ai_label.Name = "ai_label";
            this.ai_label.Size = new System.Drawing.Size(13, 13);
            this.ai_label.TabIndex = 6;
            this.ai_label.Text = "0";
            // 
            // el_label
            // 
            this.el_label.AutoSize = true;
            this.el_label.Location = new System.Drawing.Point(258, 156);
            this.el_label.Name = "el_label";
            this.el_label.Size = new System.Drawing.Size(13, 13);
            this.el_label.TabIndex = 7;
            this.el_label.Text = "0";
            // 
            // ru_label
            // 
            this.ru_label.AutoSize = true;
            this.ru_label.Location = new System.Drawing.Point(257, 196);
            this.ru_label.Name = "ru_label";
            this.ru_label.Size = new System.Drawing.Size(13, 13);
            this.ru_label.TabIndex = 8;
            this.ru_label.Text = "0";
            // 
            // th_label
            // 
            this.th_label.AutoSize = true;
            this.th_label.Location = new System.Drawing.Point(259, 77);
            this.th_label.Name = "th_label";
            this.th_label.Size = new System.Drawing.Size(13, 13);
            this.th_label.TabIndex = 9;
            this.th_label.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Throttle";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 196);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Rudder";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Elevator";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 117);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Aileron";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 39);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "CH5";
            // 
            // timer1
            // 
            this.timer1.Interval = 300;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // RC_Sensors_Monitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 247);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.th_label);
            this.Controls.Add(this.ru_label);
            this.Controls.Add(this.el_label);
            this.Controls.Add(this.ai_label);
            this.Controls.Add(this.ch5_label);
            this.Controls.Add(this.RU_bar);
            this.Controls.Add(this.El_bar);
            this.Controls.Add(this.Ai_bar);
            this.Controls.Add(this.th_bar);
            this.Controls.Add(this.ch5_bar);
            this.Name = "RC_Sensors_Monitor";
            this.Text = "RC Sensors Monitor";
            this.Load += new System.EventHandler(this.RC_Sensors_Monitor_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar ch5_bar;
        private System.Windows.Forms.ProgressBar th_bar;
        private System.Windows.Forms.ProgressBar Ai_bar;
        private System.Windows.Forms.ProgressBar El_bar;
        private System.Windows.Forms.ProgressBar RU_bar;
        private System.Windows.Forms.Label ch5_label;
        private System.Windows.Forms.Label ai_label;
        private System.Windows.Forms.Label el_label;
        private System.Windows.Forms.Label ru_label;
        private System.Windows.Forms.Label th_label;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Timer timer1;
    }
}