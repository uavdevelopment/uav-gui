﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GLobal;
using UAV_GUI.Forms.Main_Screen;
using FileClasses.VRS_Files;
using UAV_GUI.Forms.Set_Variable;
using UAV_GUI.Forms;
using UAV_GUI.FlightClasses;
namespace UAV_GUI.Forms
{
    public partial class ToolsWindow : Form
    {
        Flight myFlight;
        public ToolsWindow(Flight myFlight_)
        {
            InitializeComponent();
            myFlight = myFlight_;
            ApplicationLookAndFeel.UseTheme(this);
        }

        private void AGLB_Click(object sender, EventArgs e)
        {
            StaticForms.myAglForm = new AGLForm(myFlight.flyUAV);
            StaticForms.myAglForm.StartPosition = FormStartPosition.CenterScreen;
            StaticForms.myAglForm.Show();
        }

        private void cachingB_Click(object sender, EventArgs e)
        {
            CachingForm form = new CachingForm();
            form.Text = "Caching Properties";
            //form.FormClosed += new FormClosedEventHandler(f_FormClosed);
            form.Show();
        }

        private void JoyStB_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("joystick.exe");
        }

        private void chParB_Click(object sender, EventArgs e)
        {
            VrsFileHandler vrsHandler = new VrsFileHandler(UAV_GUI.Forms.MicroPilotSetup.fileName);
            setvariable set_variable = new setvariable(myFlight.flyUAV);
            set_variable.ShowDialog();
        }

        private void TestServoB_Click(object sender, EventArgs e)
        {
            ServoTest servotest = new ServoTest(myFlight.flyUAV);
            servotest.ShowDialog();
        }

        private void FlMB_Click(object sender, EventArgs e)
        {
            GlobalData.myLogger.Show();

        }

        private void FailPB_Click(object sender, EventArgs e)
        {
            StaticForms.myFailurePatternsToolstrip.StartPosition = FormStartPosition.CenterScreen;
            StaticForms.myFailurePatternsToolstrip.Show();
        }

        private void FlyFB_Click(object sender, EventArgs e)
        {
            StaticForms.myFlyFileTransmisionForm.ShowDialog();
        }

        private void TVRSB_Click(object sender, EventArgs e)
        {
            StaticForms.myVRSTransmitForm.ShowDialog();
        }

        private void AcVRSB_Click(object sender, EventArgs e)
        {
            StaticForms.myVRSReceiveForm.ShowDialog();
        }

        private void AutoInitB_Click(object sender, EventArgs e)
        {
            UAV_GUI.Forms.AutoConnectWizard myConnectionWiz = new Forms.AutoConnectWizard();
            myConnectionWiz.ShowDialog();
        }

       
      
    }
}
