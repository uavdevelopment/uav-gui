﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MP_Lib;
using UAV_GUI.Utilities;
namespace UAV_GUI
{
    public partial class RC_Sensors_Monitor : Form
    {

        public UAV myUAV;

        public RC_Sensors_Monitor()
        {
            InitializeComponent();
        }

        public RC_Sensors_Monitor(UAV myUav_)
        {
            myUAV = myUav_;
            InitializeComponent();
            timer1.Start();
            ApplicationLookAndFeel.UseTheme(this);
        }


        private void RC_Sensors_Monitor_Load(object sender, EventArgs e)
        {

        }
        static float map(float x, int in_min, int in_max, int out_min, int out_max)
        {
            double va = (x - in_min) * (out_max - out_min);
            double val = (in_max - in_min);
            val = va / val + out_min;
            return (float) val;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {

            float th =  myUAV.getJoy(1155) / 2000f;;
            float ai =myUAV.getJoy(1153) / 2000f; 
            float ch5 =myUAV.getJoy(1419) / 2000f;
            float ru =  myUAV.getJoy(1154)/2000f;
            float el =  myUAV.getJoy(1152) / 2000f;

            ru_label.Text = "" + ru.ToString("0.00"); ;
            th_label.Text = "" + th.ToString("0.00"); ;
            ai_label.Text = "" + ai.ToString("0.00"); ;
            ch5_label.Text = "" + ch5.ToString("0.00"); ;
            el_label.Text = "" + el.ToString("0.00"); ;

            if (th < 1.00)
            {
                th_bar.Value= 0 ;
            }
            else if(th>2.00) {
                th_bar.Value = 100;
            }
            else { 
            th_bar.Value = (int)map(th, 1, 2, 0, 100);
            
            }

            if (ru < 1.00)
            {
                RU_bar.Value = 0;
            }
            else if (ru > 2.00)
            {
                RU_bar.Value = 100;
            }
            else
            {
                RU_bar.Value = (int)map(ru, 1, 2, 0, 100);
            }

            if (ai < 1.00)
            {
                Ai_bar.Value = 0;
            }
            else if (ai > 2.00)
            {
                Ai_bar.Value = 100;
            }
            else
            {
                Ai_bar.Value = (int)map(ai, 1, 2, 0, 100);

            }

            if (el < 1.00)
            {
                El_bar.Value = 0;
            }
            else if (el > 2.00)
            {
                El_bar.Value = 100;
            }
            else
            {
                El_bar.Value = (int)map(el, 1, 2, 0, 100);

            }

            if (ch5 < 1.00)
            {
                ch5_bar.Value = 0;
            }
            else if (ch5 > 2.00)
            {
                ch5_bar.Value = 100;
            }
            else
            {
                ch5_bar.Value = (int)map(ch5, 1, 2, 0, 100);

            }
            
        
        }
    }
}
