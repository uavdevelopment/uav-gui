﻿namespace UAV_GUI
{
    partial class BiasinitMTNPanelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.startStopBiasSetupBtn = new System.Windows.Forms.Button();
            this.loggerBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(6, 22);
            this.progressBar1.Maximum = 60;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(379, 23);
            this.progressBar1.TabIndex = 3;
            // 
            // startStopBiasSetupBtn
            // 
            this.startStopBiasSetupBtn.Location = new System.Drawing.Point(152, 54);
            this.startStopBiasSetupBtn.Name = "startStopBiasSetupBtn";
            this.startStopBiasSetupBtn.Size = new System.Drawing.Size(75, 23);
            this.startStopBiasSetupBtn.TabIndex = 2;
            this.startStopBiasSetupBtn.Text = "Start";
            this.startStopBiasSetupBtn.UseVisualStyleBackColor = true;
            this.startStopBiasSetupBtn.Click += new System.EventHandler(this.startStopBiasSetupBtn_Click);
            // 
            // loggerBox
            // 
            this.loggerBox.Location = new System.Drawing.Point(6, 90);
            this.loggerBox.Multiline = true;
            this.loggerBox.Name = "loggerBox";
            this.loggerBox.ReadOnly = true;
            this.loggerBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.loggerBox.Size = new System.Drawing.Size(379, 89);
            this.loggerBox.TabIndex = 4;
            // 
            // BiasinitMTNPanelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 204);
            this.ControlBox = false;
            this.Controls.Add(this.loggerBox);
            this.Controls.Add(this.startStopBiasSetupBtn);
            this.Controls.Add(this.progressBar1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BiasinitMTNPanelForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Initialization";
            this.Load += new System.EventHandler(this.FormBiasSetup_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button startStopBiasSetupBtn;
        private System.Windows.Forms.TextBox loggerBox;
    }
}