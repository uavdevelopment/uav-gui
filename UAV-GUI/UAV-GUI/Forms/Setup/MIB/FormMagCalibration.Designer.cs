﻿namespace UAV_GUI
{
    partial class FormMagCalibration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.resetCalibrationButton = new System.Windows.Forms.Button();
            this.txtbxMagCalibrationTimer = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtbxLatitude = new System.Windows.Forms.TextBox();
            this.txtbxLongtitude = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.startStopMagnetometerCalibrationButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtbxMagHeading = new System.Windows.Forms.TextBox();
            this.txtbxDualAntennaHeading = new System.Windows.Forms.TextBox();
            this.testCalibratedMagnetometerButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(473, 159);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 30);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(39, 154);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(500, 2);
            this.label1.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.resetCalibrationButton);
            this.groupBox1.Controls.Add(this.txtbxMagCalibrationTimer);
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Controls.Add(this.startStopMagnetometerCalibrationButton);
            this.groupBox1.Location = new System.Drawing.Point(13, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(265, 142);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Calibrate";
            // 
            // resetCalibrationButton
            // 
            this.resetCalibrationButton.Location = new System.Drawing.Point(7, 109);
            this.resetCalibrationButton.Name = "resetCalibrationButton";
            this.resetCalibrationButton.Size = new System.Drawing.Size(123, 23);
            this.resetCalibrationButton.TabIndex = 3;
            this.resetCalibrationButton.Text = "Reset Calibration";
            this.resetCalibrationButton.UseVisualStyleBackColor = true;
            this.resetCalibrationButton.Click += new System.EventHandler(this.resetCalibrationButton_Click);
            // 
            // txtbxMagCalibrationTimer
            // 
            this.txtbxMagCalibrationTimer.Location = new System.Drawing.Point(149, 82);
            this.txtbxMagCalibrationTimer.Name = "txtbxMagCalibrationTimer";
            this.txtbxMagCalibrationTimer.ReadOnly = true;
            this.txtbxMagCalibrationTimer.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtbxMagCalibrationTimer.Size = new System.Drawing.Size(100, 20);
            this.txtbxMagCalibrationTimer.TabIndex = 2;
            this.txtbxMagCalibrationTimer.Text = "00:00:00";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.txtbxLatitude, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtbxLongtitude, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(7, 20);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(243, 54);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // txtbxLatitude
            // 
            this.txtbxLatitude.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtbxLatitude.Location = new System.Drawing.Point(140, 3);
            this.txtbxLatitude.Name = "txtbxLatitude";
            this.txtbxLatitude.Size = new System.Drawing.Size(100, 20);
            this.txtbxLatitude.TabIndex = 0;
            // 
            // txtbxLongtitude
            // 
            this.txtbxLongtitude.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtbxLongtitude.Location = new System.Drawing.Point(140, 30);
            this.txtbxLongtitude.Name = "txtbxLongtitude";
            this.txtbxLongtitude.Size = new System.Drawing.Size(100, 20);
            this.txtbxLongtitude.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Latitude";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Longitude";
            // 
            // startStopMagnetometerCalibrationButton
            // 
            this.startStopMagnetometerCalibrationButton.Location = new System.Drawing.Point(7, 80);
            this.startStopMagnetometerCalibrationButton.Name = "startStopMagnetometerCalibrationButton";
            this.startStopMagnetometerCalibrationButton.Size = new System.Drawing.Size(123, 23);
            this.startStopMagnetometerCalibrationButton.TabIndex = 0;
            this.startStopMagnetometerCalibrationButton.Text = "Start Calibration";
            this.startStopMagnetometerCalibrationButton.UseVisualStyleBackColor = true;
            this.startStopMagnetometerCalibrationButton.Click += new System.EventHandler(this.startStopMagnetometerCalibrationButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            this.groupBox2.Controls.Add(this.testCalibratedMagnetometerButton);
            this.groupBox2.Location = new System.Drawing.Point(284, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(264, 142);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Test";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtbxMagHeading, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtbxDualAntennaHeading, 1, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(7, 20);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(251, 54);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Magnetometer Heading";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Enabled = false;
            this.label6.Location = new System.Drawing.Point(3, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Dual Antenna Heading";
            // 
            // txtbxMagHeading
            // 
            this.txtbxMagHeading.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtbxMagHeading.Location = new System.Drawing.Point(148, 3);
            this.txtbxMagHeading.Name = "txtbxMagHeading";
            this.txtbxMagHeading.ReadOnly = true;
            this.txtbxMagHeading.Size = new System.Drawing.Size(100, 20);
            this.txtbxMagHeading.TabIndex = 2;
            // 
            // txtbxDualAntennaHeading
            // 
            this.txtbxDualAntennaHeading.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtbxDualAntennaHeading.Location = new System.Drawing.Point(148, 30);
            this.txtbxDualAntennaHeading.Name = "txtbxDualAntennaHeading";
            this.txtbxDualAntennaHeading.ReadOnly = true;
            this.txtbxDualAntennaHeading.Size = new System.Drawing.Size(100, 20);
            this.txtbxDualAntennaHeading.TabIndex = 3;
            // 
            // testCalibratedMagnetometerButton
            // 
            this.testCalibratedMagnetometerButton.Location = new System.Drawing.Point(6, 80);
            this.testCalibratedMagnetometerButton.Name = "testCalibratedMagnetometerButton";
            this.testCalibratedMagnetometerButton.Size = new System.Drawing.Size(117, 23);
            this.testCalibratedMagnetometerButton.TabIndex = 0;
            this.testCalibratedMagnetometerButton.Text = "Test Magnetometer";
            this.testCalibratedMagnetometerButton.UseVisualStyleBackColor = true;
            this.testCalibratedMagnetometerButton.Click += new System.EventHandler(this.testCalibratedMagnetometerButton_Click);
            // 
            // FormMagCalibration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 201);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOK);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMagCalibration";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Magnetometer Calibration";
            this.Load += new System.EventHandler(this.FormMagCalibration_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox txtbxLatitude;
        private System.Windows.Forms.TextBox txtbxLongtitude;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button startStopMagnetometerCalibrationButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button testCalibratedMagnetometerButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox txtbxMagCalibrationTimer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtbxMagHeading;
        private System.Windows.Forms.TextBox txtbxDualAntennaHeading;
        private System.Windows.Forms.Button resetCalibrationButton;
    }
}