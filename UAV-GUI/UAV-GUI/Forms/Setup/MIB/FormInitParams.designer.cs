﻿namespace UAV_GUI
{
    partial class FormInitParams
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbxLatitude = new System.Windows.Forms.TextBox();
            this.txtbxLongtitude = new System.Windows.Forms.TextBox();
            this.txtbxAltitude = new System.Windows.Forms.TextBox();
            this.txtbxHeading = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.chkbxAdvancedSettings = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkbxUseHeading = new System.Windows.Forms.CheckBox();
            this.chkbxMagEnable = new System.Windows.Forms.CheckBox();
            this.chkbxBaroEnable = new System.Windows.Forms.CheckBox();
            this.groupBxAccel = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtbxAccelBiasInstability = new System.Windows.Forms.TextBox();
            this.txtbxAccelVRW = new System.Windows.Forms.TextBox();
            this.txtbxAccelBiasCT = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtbxGyroBiasInstability = new System.Windows.Forms.TextBox();
            this.txtbxGyroARW = new System.Windows.Forms.TextBox();
            this.txtbxGyroBiasCT = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBxAccel.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.12903F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.87097F));
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtbxLatitude, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtbxLongtitude, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtbxAltitude, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtbxHeading, 1, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(7, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(162, 101);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Latitude:";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Longtitude:";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Altitude:";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Heading:";
            // 
            // txtbxLatitude
            // 
            this.txtbxLatitude.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtbxLatitude.Location = new System.Drawing.Point(69, 3);
            this.txtbxLatitude.Name = "txtbxLatitude";
            this.txtbxLatitude.Size = new System.Drawing.Size(90, 20);
            this.txtbxLatitude.TabIndex = 7;
            this.txtbxLatitude.GotFocus += new System.EventHandler(this.txtbxLatitude_GotFocus);
            this.txtbxLatitude.LostFocus += new System.EventHandler(this.txtbxLatitude_LostFocus);
            // 
            // txtbxLongtitude
            // 
            this.txtbxLongtitude.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtbxLongtitude.Location = new System.Drawing.Point(69, 28);
            this.txtbxLongtitude.Name = "txtbxLongtitude";
            this.txtbxLongtitude.Size = new System.Drawing.Size(90, 20);
            this.txtbxLongtitude.TabIndex = 8;
            this.txtbxLongtitude.GotFocus += new System.EventHandler(this.txtbxLongtitude_GotFocus);
            this.txtbxLongtitude.LostFocus += new System.EventHandler(this.txtbxLongtitude_LostFocus);
            // 
            // txtbxAltitude
            // 
            this.txtbxAltitude.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtbxAltitude.Location = new System.Drawing.Point(69, 53);
            this.txtbxAltitude.Name = "txtbxAltitude";
            this.txtbxAltitude.Size = new System.Drawing.Size(90, 20);
            this.txtbxAltitude.TabIndex = 9;
            this.txtbxAltitude.GotFocus += new System.EventHandler(this.txtbxAltitude_GotFocus);
            this.txtbxAltitude.LostFocus += new System.EventHandler(this.txtbxAltitude_LostFocus);
            // 
            // txtbxHeading
            // 
            this.txtbxHeading.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtbxHeading.Location = new System.Drawing.Point(69, 78);
            this.txtbxHeading.Name = "txtbxHeading";
            this.txtbxHeading.Size = new System.Drawing.Size(90, 20);
            this.txtbxHeading.TabIndex = 10;
            this.txtbxHeading.GotFocus += new System.EventHandler(this.txtbxHeading_GotFocus);
            this.txtbxHeading.LostFocus += new System.EventHandler(this.txtbxHeading_LostFocus);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(8, -619);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 34);
            this.button1.TabIndex = 0;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(99, -619);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 34);
            this.button2.TabIndex = 1;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Sans Unicode", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Confirm Initial Parameters:";
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(8, 262);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(431, 2);
            this.label7.TabIndex = 2;
            // 
            // chkbxAdvancedSettings
            // 
            this.chkbxAdvancedSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkbxAdvancedSettings.AutoSize = true;
            this.chkbxAdvancedSettings.Location = new System.Drawing.Point(8, -692);
            this.chkbxAdvancedSettings.Name = "chkbxAdvancedSettings";
            this.chkbxAdvancedSettings.Size = new System.Drawing.Size(129, 17);
            this.chkbxAdvancedSettings.TabIndex = 3;
            this.chkbxAdvancedSettings.Text = "Advanced Options >>";
            this.chkbxAdvancedSettings.UseVisualStyleBackColor = true;
            this.chkbxAdvancedSettings.Visible = false;
            this.chkbxAdvancedSettings.CheckedChanged += new System.EventHandler(this.chkbxAdvancedSettings_CheckChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkbxUseHeading);
            this.groupBox2.Controls.Add(this.tableLayoutPanel1);
            this.groupBox2.Location = new System.Drawing.Point(8, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(190, 130);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            // 
            // chkbxUseHeading
            // 
            this.chkbxUseHeading.AutoSize = true;
            this.chkbxUseHeading.Location = new System.Drawing.Point(172, 103);
            this.chkbxUseHeading.Name = "chkbxUseHeading";
            this.chkbxUseHeading.Size = new System.Drawing.Size(15, 14);
            this.chkbxUseHeading.TabIndex = 10;
            this.chkbxUseHeading.UseVisualStyleBackColor = true;
            // 
            // chkbxMagEnable
            // 
            this.chkbxMagEnable.AutoSize = true;
            this.chkbxMagEnable.Location = new System.Drawing.Point(210, 242);
            this.chkbxMagEnable.Name = "chkbxMagEnable";
            this.chkbxMagEnable.Size = new System.Drawing.Size(116, 17);
            this.chkbxMagEnable.TabIndex = 6;
            this.chkbxMagEnable.Text = "Use Magnetometer";
            this.chkbxMagEnable.UseVisualStyleBackColor = true;
            this.chkbxMagEnable.Visible = false;
            // 
            // chkbxBaroEnable
            // 
            this.chkbxBaroEnable.AutoSize = true;
            this.chkbxBaroEnable.Location = new System.Drawing.Point(340, 242);
            this.chkbxBaroEnable.Name = "chkbxBaroEnable";
            this.chkbxBaroEnable.Size = new System.Drawing.Size(96, 17);
            this.chkbxBaroEnable.TabIndex = 7;
            this.chkbxBaroEnable.Text = "Use Barometer";
            this.chkbxBaroEnable.UseVisualStyleBackColor = true;
            this.chkbxBaroEnable.Visible = false;
            // 
            // groupBxAccel
            // 
            this.groupBxAccel.Controls.Add(this.tableLayoutPanel2);
            this.groupBxAccel.Location = new System.Drawing.Point(204, 27);
            this.groupBxAccel.Name = "groupBxAccel";
            this.groupBxAccel.Size = new System.Drawing.Size(238, 107);
            this.groupBxAccel.TabIndex = 8;
            this.groupBxAccel.TabStop = false;
            this.groupBxAccel.Text = "Accelerometer";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.66142F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.33858F));
            this.tableLayoutPanel2.Controls.Add(this.txtbxAccelBiasInstability, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtbxAccelVRW, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtbxAccelBiasCT, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 2);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(223, 73);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // txtbxAccelBiasInstability
            // 
            this.txtbxAccelBiasInstability.Location = new System.Drawing.Point(133, 3);
            this.txtbxAccelBiasInstability.Name = "txtbxAccelBiasInstability";
            this.txtbxAccelBiasInstability.Size = new System.Drawing.Size(87, 20);
            this.txtbxAccelBiasInstability.TabIndex = 0;
            // 
            // txtbxAccelVRW
            // 
            this.txtbxAccelVRW.Location = new System.Drawing.Point(133, 29);
            this.txtbxAccelVRW.Name = "txtbxAccelVRW";
            this.txtbxAccelVRW.Size = new System.Drawing.Size(87, 20);
            this.txtbxAccelVRW.TabIndex = 1;
            // 
            // txtbxAccelBiasCT
            // 
            this.txtbxAccelBiasCT.Location = new System.Drawing.Point(133, 55);
            this.txtbxAccelBiasCT.Name = "txtbxAccelBiasCT";
            this.txtbxAccelBiasCT.Size = new System.Drawing.Size(87, 20);
            this.txtbxAccelBiasCT.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Bias Instability:";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Velocity Random Walk:";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 58);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Bias Correlation Time:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel3);
            this.groupBox3.Location = new System.Drawing.Point(204, 140);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(238, 96);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Gyroscope";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.26772F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.73228F));
            this.tableLayoutPanel3.Controls.Add(this.txtbxGyroBiasInstability, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtbxGyroARW, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtbxGyroBiasCT, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label11, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 2);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 18);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(226, 72);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // txtbxGyroBiasInstability
            // 
            this.txtbxGyroBiasInstability.Location = new System.Drawing.Point(134, 3);
            this.txtbxGyroBiasInstability.Name = "txtbxGyroBiasInstability";
            this.txtbxGyroBiasInstability.Size = new System.Drawing.Size(89, 20);
            this.txtbxGyroBiasInstability.TabIndex = 0;
            // 
            // txtbxGyroARW
            // 
            this.txtbxGyroARW.Location = new System.Drawing.Point(134, 29);
            this.txtbxGyroARW.Name = "txtbxGyroARW";
            this.txtbxGyroARW.Size = new System.Drawing.Size(89, 20);
            this.txtbxGyroARW.TabIndex = 1;
            // 
            // txtbxGyroBiasCT
            // 
            this.txtbxGyroBiasCT.Location = new System.Drawing.Point(134, 55);
            this.txtbxGyroBiasCT.Name = "txtbxGyroBiasCT";
            this.txtbxGyroBiasCT.Size = new System.Drawing.Size(89, 20);
            this.txtbxGyroBiasCT.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Bias Instability:";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Angular Random Walk:";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Bias Correlation Time:";
            // 
            // button3
            // 
            this.button3.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button3.Location = new System.Drawing.Point(54, 228);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 10;
            this.button3.Text = "OK";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // FormInitParams
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(454, 311);
            this.ControlBox = false;
            this.Controls.Add(this.button3);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBxAccel);
            this.Controls.Add(this.chkbxBaroEnable);
            this.Controls.Add(this.chkbxMagEnable);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.chkbxAdvancedSettings);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(470, 350);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(210, 350);
            this.Name = "FormInitParams";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "  ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormInitParams_FormClosing);
            this.Load += new System.EventHandler(this.FormInitParams_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBxAccel.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtbxLatitude;
        private System.Windows.Forms.TextBox txtbxLongtitude;
        private System.Windows.Forms.TextBox txtbxAltitude;
        private System.Windows.Forms.TextBox txtbxHeading;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkbxAdvancedSettings;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkbxMagEnable;
        private System.Windows.Forms.CheckBox chkbxBaroEnable;
        private System.Windows.Forms.GroupBox groupBxAccel;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox txtbxAccelBiasInstability;
        private System.Windows.Forms.TextBox txtbxAccelVRW;
        private System.Windows.Forms.TextBox txtbxAccelBiasCT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TextBox txtbxGyroBiasInstability;
        private System.Windows.Forms.TextBox txtbxGyroARW;
        private System.Windows.Forms.TextBox txtbxGyroBiasCT;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox chkbxUseHeading;
        private System.Windows.Forms.Button button3;
    }
}