﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Globalization;
using System.Windows;
using GLobal;
namespace UAV_GUI
{
    public partial class FormInitParams : Form
    {
        private int MINWINDOWSIZE_X = 210;
        private int MINWINDOWSIZE_Y = 350;
        private int MAXWINDOWSIZE_X = 470;
        private int MAXWINDOWSIZE_Y = 350;
        /*
        public double lat;
        public double lon;
        public double alt;
        public double heading;
        public double accel_bias_instability;
        public double accel_arw;
        public double accel_bias_correlation_time;
        public double gyro_bias_instability;
        public double gyro_arw;
        public double gyro_bias_correlation_time;
         */ 
        public InitParams initParams = null;
        public bool useHeadingCheckBoxValue = false;
        Stream stream = null;
        BinaryFormatter bformatter = null;
        bool bCanceled = false;
        string accel_bias_instability_unitsString       = " *1E-5";
        string accel_vrw_unitsString                    = " /60";
        string accel_bias_correlation_time_unitsString  = " *3600";
        string gyro_bias_instability_unitsString        = " /3600";
        string gyro_arw_unitsString                     = " /60";
        string gyro_bias_correlation_time_unitsString   = " *3600";

        public FormInitParams()
        {
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
        }

        public FormInitParams( InitParams initialParametersObjectReference ) : this()
        {
            /*
            initParams.latitude     =   initialParametersObjectReference.latitude;
            initParams.longitude    =   initialParametersObjectReference.longitude;
            initParams.altitude     =   initialParametersObjectReference.altitude;
            initParams.heading      =   initialParametersObjectReference.heading;

            initParams.bChkbxBaroEnabled    = initialParametersObjectReference.bChkbxBaroEnabled;
            initParams.bChkbxMagEnabled     = initialParametersObjectReference.bChkbxMagEnabled;
            */
            initParams = new InitParams();
            initParams.gyro_bias_instability        =   initialParametersObjectReference.gyro_bias_instability  ;
            initParams.gyro_arw                     =   initialParametersObjectReference.gyro_arw               ;
            initParams.gyro_bias_correlation_time   =   initialParametersObjectReference.gyro_bias_correlation_time ;
            initParams.accel_bias_instability       =   initialParametersObjectReference.accel_bias_instability     ;
            initParams.accel_vrw                    =   initialParametersObjectReference.accel_vrw              ;
            initParams.accel_bias_correlation_time  =   initialParametersObjectReference.accel_bias_correlation_time;
            initParams.bChkbxMagEnabled             =   initialParametersObjectReference.bChkbxMagEnabled;
            initParams.bChkbxBaroEnabled            =   initialParametersObjectReference.bChkbxBaroEnabled;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (initParams == null)
            {
                initParams = new InitParams();
            }

            this.Size = this.Size = new Size(MINWINDOWSIZE_X, MINWINDOWSIZE_Y);
            label7.Size = new Size(MINWINDOWSIZE_X, 2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;

            //-- Those values are filled without the use of the files to force the values that TPI use.
            chkbxMagEnable.Checked          = initParams.bChkbxMagEnabled;
            chkbxBaroEnable.Checked         = initParams.bChkbxBaroEnabled;
            txtbxAccelBiasInstability.Text  = initParams.accel_bias_instability.ToString("0.0")             ;//+ accel_bias_instability_unitsString;
            txtbxAccelVRW.Text              = initParams.accel_vrw.ToString("0.0")                          ;// +accel_vrw_unitsString;
            txtbxAccelBiasCT.Text           = initParams.accel_bias_correlation_time.ToString("0.0")        ;// +accel_bias_correlation_time_unitsString;
            txtbxGyroBiasInstability.Text   = initParams.gyro_bias_instability.ToString("0.0")              ;// +gyro_bias_instability_unitsString;
            txtbxGyroARW.Text               = initParams.gyro_arw.ToString("0.0")                           ;// +gyro_arw_unitsString;                    
            txtbxGyroBiasCT.Text            = initParams.gyro_bias_correlation_time.ToString("0.0")         ;// +gyro_bias_correlation_time_unitsString;    
            chkbxUseHeading.Checked         = false;
            try
            {
                //-- Open the file written above and read values from it.
                stream = File.Open("InitParams.tpi", FileMode.Open);
                bformatter = new BinaryFormatter();

                GlobalData.LogConsole("Reading Initial Parameters Information");
                initParams = (InitParams)bformatter.Deserialize(stream);
                stream.Close();
            }
            catch (Exception exception) 
            {
                stream.Close();
                GlobalData.LogConsole(exception.Message);
                
            }

            GlobalData.LogConsole("Lat: {0}"+ initParams.latitude.ToString());
            GlobalData.LogConsole("Lon: {0}"+ initParams.longitude.ToString());
            GlobalData.LogConsole("Alt: {0}"+ initParams.altitude.ToString());

            txtbxLatitude.Text = initParams.latitude.ToString() + " °";
            txtbxLongtitude.Text = initParams.longitude.ToString() + " °";
            txtbxAltitude.Text = initParams.altitude.ToString() + " m";
            txtbxHeading.Text = initParams.heading.ToString() + " °";
            chkbxAdvancedSettings.Checked = false; //initParams.bChkbxAdvSetEnabled;
            /*
            chkbxMagEnable.Checked = initParams.bChkbxMagEnabled;
            chkbxBaroEnable.Checked = initParams.bChkbxBaroEnabled;
            txtbxAccelBiasInstability.Text = initParams.accel_bias_instability.ToString();
            txtbxAccelVRW.Text = initParams.accel_vrw.ToString();
            txtbxAccelBiasCT.Text = initParams.accel_bias_correlation_time.ToString();
            txtbxGyroBiasInstability.Text = initParams.gyro_bias_instability.ToString();
            txtbxGyroARW.Text = initParams.gyro_arw.ToString();
            txtbxGyroBiasCT.Text = initParams.gyro_bias_correlation_time.ToString();
             */ 
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if(!bCanceled)
            {
                initParams = new InitParams();
                bool bInvalidValue = false;

                //Remove ending units
                txtbxLatitude.Text = txtbxLatitude.Text.Replace("°", "");
                txtbxLongtitude.Text = txtbxLongtitude.Text.Replace("°", "");
                txtbxAltitude.Text = txtbxAltitude.Text.Replace("m", "");
                txtbxHeading.Text = txtbxHeading.Text.Replace("°", "");

                //Check Validity of input values
                bool bInvalidLat = false;
                bInvalidLat |= !(double.TryParse(txtbxLatitude.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out initParams.latitude));
                bInvalidLat |= ((initParams.latitude > 90 )|| (initParams.latitude < -90))?true:false;
                if(bInvalidLat)
                {
                    e.Cancel = true;
                    MessageBox.Show("Invalid latitude value. Please enter a correct numeric value between -90° and 90°.");
                }
                bool bInvalidLon = false;
                bInvalidLon |= !(double.TryParse(txtbxLongtitude.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out initParams.longitude));
                bInvalidLon |= ((initParams.longitude > 180) || (initParams.longitude < -180)) ? true : false;
                if (bInvalidLon)
                {
                    e.Cancel = true;
                    MessageBox.Show("Invalid longtitude value. Please enter a correct numeric value between -180° and 180°.");
                }
                bInvalidValue |= !(double.TryParse(txtbxAltitude.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out initParams.altitude));
                bool bInvalidHeading = false;
                bInvalidHeading |= !(double.TryParse(txtbxHeading.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out initParams.heading));
                bInvalidHeading |= ((initParams.heading > 180) || (initParams.heading < -180)) ? true : false;
                if (bInvalidHeading)
                {
                    e.Cancel = true;
                    MessageBox.Show("Invalid heading value. Please enter a correct numeric value between -180° and 180°.");
                }

                initParams.bChkbxAdvSetEnabled = chkbxAdvancedSettings.Checked;
                initParams.bChkbxBaroEnabled = chkbxBaroEnable.Checked;
                initParams.bChkbxMagEnabled = chkbxMagEnable.Checked;
                bInvalidValue |= !(double.TryParse(txtbxGyroBiasInstability.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out initParams.gyro_bias_instability));
                bInvalidValue |= !(double.TryParse(txtbxGyroBiasCT.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out initParams.gyro_bias_correlation_time));
                bInvalidValue |= !(double.TryParse(txtbxGyroARW.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out initParams.gyro_arw));
                bInvalidValue |= !(double.TryParse(txtbxAccelBiasInstability.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out initParams.accel_bias_instability));
                bInvalidValue |= !(double.TryParse(txtbxAccelBiasCT.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out initParams.accel_bias_correlation_time));
                bInvalidValue |= !(double.TryParse(txtbxAccelVRW.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out initParams.accel_vrw));
                useHeadingCheckBoxValue = chkbxUseHeading.Checked;

                //Cancel form closing if values are invalid
                if (bInvalidValue)
                {
                    e.Cancel = true;
                    MessageBox.Show("Invalid value. Please enter a correct numeric value.");
                }

                /*
                this.lat = initParams.latitude;
                this.lon = initParams.longtitude;
                this.alt = initParams.altitude;
                this.heading = initParams.heading;
                */

                //Serialize Initial Parameter Class
                /*stream = File.Open("InitParams.tpi", FileMode.Create);
                bformatter = new BinaryFormatter();
                GlobalData.LogConsole("Writing Initial Parameters Information");
                bformatter.Serialize(stream, initParams);
                */stream.Close();
            }
        }

        private void chkbxAdvancedSettings_CheckChanged(object sender, EventArgs e)
        {
            if (chkbxAdvancedSettings.Checked)
            {
                this.Size = new Size(MAXWINDOWSIZE_X, MAXWINDOWSIZE_Y);
                label7.Size = new Size(MAXWINDOWSIZE_X-30, 2);
            }
            else
            {
                this.Size = new Size(MINWINDOWSIZE_X, MINWINDOWSIZE_Y);
                label7.Size = new Size(MINWINDOWSIZE_X, 2);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bCanceled = true;
        }

        private void txtbxLatitude_GotFocus(object sender, EventArgs e)
        {
            txtbxLatitude.Text = txtbxLatitude.Text.Replace("°", "");
        }

        private void txtbxLatitude_LostFocus(object sender, EventArgs e)
        {
            txtbxLatitude.Text = txtbxLatitude.Text + "°";
        }

        private void txtbxLongtitude_GotFocus(object sender, EventArgs e)
        {
            txtbxLongtitude.Text = txtbxLongtitude.Text.Replace("°", "");
        }

        private void txtbxLongtitude_LostFocus(object sender, EventArgs e)
        {
            txtbxLongtitude.Text = txtbxLongtitude.Text + "°";
        }

        private void txtbxAltitude_GotFocus(object sender, EventArgs e)
        {
            txtbxAltitude.Text = txtbxAltitude.Text.Replace("m", "");
        }

        private void txtbxAltitude_LostFocus(object sender, EventArgs e)
        {
            txtbxAltitude.Text = txtbxAltitude.Text + "m";
        }

        private void txtbxHeading_GotFocus(object sender, EventArgs e)
        {
            txtbxHeading.Text = txtbxHeading.Text.Replace("°", "");
        }

        private void txtbxHeading_LostFocus(object sender, EventArgs e)
        {
            txtbxHeading.Text = txtbxHeading.Text + "°";
        }

        private void txtbxAccelBiasInstability_GotFocus(object sender, EventArgs e)
        {
            txtbxAccelBiasInstability.Text = txtbxAccelBiasInstability.Text.Replace(accel_bias_instability_unitsString, "");
        }

        private void txtbxAccelBiasInstability_LostFocus(object sender, EventArgs e)
        {
            txtbxAccelBiasInstability.Text = txtbxAccelBiasInstability.Text + accel_bias_instability_unitsString;
        }

        private void txtbxAccelVRW_GotFocus(object sender, EventArgs e)
        {
            txtbxAccelVRW.Text = txtbxAccelVRW.Text.Replace(accel_vrw_unitsString, "");
        }

        private void txtbxAccelVRW_LostFocus(object sender, EventArgs e)
        {
            txtbxAccelVRW.Text = txtbxAccelVRW.Text + accel_vrw_unitsString;
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void FormInitParams_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void FormInitParams_FormClosing(object sender, FormClosingEventArgs e)
        {

        }


    }
}
