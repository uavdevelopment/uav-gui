﻿namespace UAV_GUI
{
    partial class Setup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnStartNav = new System.Windows.Forms.Button();
            this.connectButton = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.LightGray;
            this.tabPage3.Controls.Add(this.btnStartNav);
            this.tabPage3.Controls.Add(this.connectButton);
            this.tabPage3.Controls.Add(this.button4);
            this.tabPage3.Controls.Add(this.button6);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1201, 607);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Sensors";
            // 
            // btnStartNav
            // 
            this.btnStartNav.Location = new System.Drawing.Point(573, 200);
            this.btnStartNav.Name = "btnStartNav";
            this.btnStartNav.Size = new System.Drawing.Size(235, 93);
            this.btnStartNav.TabIndex = 4;
            this.btnStartNav.Text = "Start";
            this.btnStartNav.UseVisualStyleBackColor = true;
            this.btnStartNav.Click += new System.EventHandler(this.btnStartNav_Click);
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(281, 45);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(266, 56);
            this.connectButton.TabIndex = 3;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(299, 200);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(229, 93);
            this.button4.TabIndex = 2;
            this.button4.Text = "Magnetometer Calibration";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(49, 200);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(198, 93);
            this.button6.TabIndex = 2;
            this.button6.Text = "System Intialization (Bias setup)";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(3, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1209, 633);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Tag = "NotImplemented";
            // 
            // Setup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1254, 631);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "Setup";
            this.Text = "Setup";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Setup_FormClosing);
            this.Load += new System.EventHandler(this.Setup_Load);
            this.tabPage3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage3;
        public System.Windows.Forms.Button btnStartNav;
        public System.Windows.Forms.Button connectButton;
        public System.Windows.Forms.Button button4;
        public System.Windows.Forms.Button button6;
        private System.Windows.Forms.TabControl tabControl1;


    }
}