﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Globalization;
using System.Threading;
using GLobal;
namespace UAV_GUI
{
    public partial class FormMagCalibration : Form
    {
        protected ManualResetEvent threadStop = new ManualResetEvent(false);
        InitParams initParams = null;
        Stream stream = null;
        BinaryFormatter bformatter = null;
        SerialCommunication serialCommunicationChannel;
        Thread readMagResponseThread;
        delegate void SetCallbackForMagCalibrationTimer(string updateString) ;
        delegate void SetCallbackForMagHeadingTest(string updateString);


        public FormMagCalibration()
        {
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
        }

        public FormMagCalibration(SerialCommunication serialCommunicationObjectReference) : this()
        {
            serialCommunicationChannel = serialCommunicationObjectReference;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            initParams = new InitParams();

            try
            {
                //Open the file written above and read values from it.
                stream = File.Open("InitParams.tpi", FileMode.Open);
                bformatter = new BinaryFormatter();

                GlobalData.LogConsole("Reading Initial Parameters Information");
                initParams = (InitParams)bformatter.Deserialize(stream);
                stream.Close();
            }
            catch( Exception exception )
            {
                GlobalData.LogConsole(exception.Message);
            }

            GlobalData.LogConsole("Lat: {0}"+ initParams.latitude.ToString());
            GlobalData.LogConsole("Lon: {0}"+ initParams.longitude.ToString());

            txtbxLatitude.Text = initParams.latitude.ToString();
            txtbxLongtitude.Text = initParams.longitude.ToString();
        }

        private void FormMagCalibration_Load(object sender, EventArgs e)
        {

        }

        private void startStopMagnetometerCalibrationButton_Click(object sender, EventArgs e)
        {
            if(startStopMagnetometerCalibrationButton.Text.Equals("Start Calibration"))
            {
                bool bInvalidValue = false;
                bInvalidValue |= !(double.TryParse(txtbxLatitude.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out initParams.latitude));
                bInvalidValue |= !(double.TryParse(txtbxLongtitude.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out initParams.longitude));

                CommunicationPackets.command_MAG_PRECALIB_START.payload.useLatitudeAndLongitudeValues   = 1 ;
                CommunicationPackets.command_MAG_PRECALIB_START.payload.initialLatitude_degrees         = initParams.latitude;
                CommunicationPackets.command_MAG_PRECALIB_START.payload.initialLongitude_degrees        = initParams.longitude;
                CommunicationPackets.insertChecksum(ref CommunicationPackets.command_MAG_PRECALIB_START);

                serialCommunicationChannel.ResetPort();

                readMagResponseThread = new Thread(() => ReadMagResponse() );
        
                serialCommunicationChannel.Write(CommunicationPackets.command_MAG_PRECALIB_START);
                Thread.Sleep(25);
                threadStop.Reset();
                readMagResponseThread.Start();
                startStopMagnetometerCalibrationButton.Text = "Stop Calibration";
                resetCalibrationButton.Enabled = false;
                testCalibratedMagnetometerButton.Enabled = false;
            }
            else if (startStopMagnetometerCalibrationButton.Text.Equals("Stop Calibration"))
            {
                try
                {
                    threadStop.Set();
                }
                catch (Exception exception)
                {
                    GlobalData.LogConsole(exception.Message);
                }
                //Thread.Sleep(1000);
                serialCommunicationChannel.ResetPort();

                serialCommunicationChannel.Write(CommunicationPackets.command_MAG_PRECALIB_STOP);
                Thread.Sleep(50);
                serialCommunicationChannel.Read(ref CommunicationPackets.response_MAG_PRECALIB_PARAMETERS);

                if (CommunicationPackets.verifyPacket(CommunicationPackets.response_MAG_PRECALIB_PARAMETERS) == true)
                {
                    if (CommunicationPackets.response_MAG_PRECALIB_PARAMETERS.payload.parametersAreValid == 1)
                    {
                        MessageBox.Show("Valid Magnetometer Calibration","INFORMATION",MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Invalid Magnetometer Calibration","ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    GlobalData.LogConsole("[ERRR] Checksum is WRONG");
                }

                startStopMagnetometerCalibrationButton.Text = "Start Calibration";
                resetCalibrationButton.Enabled = true;
                testCalibratedMagnetometerButton.Enabled = true;
            }
            else 
            {
                GlobalData.LogConsole("[ERRR] Invalid button state");
            }
        }

        private void ReadMagResponse()
        {
            byte[] RESPONSE_HEADER_byteArray = new byte[CommunicationPackets.HEADER_SIZE_IN_BYTES];
            byte[] RESPONSE_MAG_PRECALIB_byteArray = new byte[CommunicationPackets.RESPONSE_MAG_PRECALIB_PACKET_SIZE_IN_BYTES];
            byte[] RESPONSE_MAG_PRECALIB_TEST_START_byteArray = new byte[CommunicationPackets.RESPONSE_MAG_PRECALIB_TEST_START_PACKET_SIZE_IN_BYTES];
            CommunicationPackets.Header header = new CommunicationPackets.Header();

            Int32 numBytesRead = 0;
            bool ftdiStatus;

            for (; ; )
            {
                numBytesRead = serialCommunicationChannel.Read(RESPONSE_HEADER_byteArray);
                header = CommunicationPackets.byteArrayToStruct(RESPONSE_HEADER_byteArray, header);
                String res__ = String.Join(", ", RESPONSE_HEADER_byteArray);
                GlobalData.LogConsole("setIMUSpecs.txt "+ res__);
                if (header.packetId == CommunicationPackets.RESPONSE_MAG_PRECALIB_ID)
                {
                    Array.Copy(RESPONSE_HEADER_byteArray, RESPONSE_MAG_PRECALIB_byteArray, CommunicationPackets.HEADER_SIZE_IN_BYTES);
                    ftdiStatus = serialCommunicationChannel.Read(RESPONSE_MAG_PRECALIB_byteArray, (int)CommunicationPackets.HEADER_SIZE_IN_BYTES, (int)(RESPONSE_MAG_PRECALIB_byteArray.Length - RESPONSE_HEADER_byteArray.Length), ref numBytesRead);
                    CommunicationPackets.response_MAG_PRECALIB = CommunicationPackets.byteArrayToStruct(RESPONSE_MAG_PRECALIB_byteArray, CommunicationPackets.response_MAG_PRECALIB);
                    res__ = String.Join(", ", RESPONSE_MAG_PRECALIB_byteArray);
                    GlobalData.LogConsole("setIMUSpecs.txt "+res__);
                    if (!ftdiStatus )
                    {
                        // Wait for a key press
                        GlobalData.LogConsole("Failed to read data (error " + ftdiStatus.ToString() + ")");
                        return;
                    }

                    if (ftdiStatus && numBytesRead == 0)
                    {
                        GlobalData.LogConsole("[INFO] Timeout");
                    }

                    if (CommunicationPackets.verifyPacket(RESPONSE_MAG_PRECALIB_byteArray) == true)
                    {
                        GlobalData.LogConsole("[INFO] CommunicationPackets.response_MAG_PRECALIB : " 
                                           + CommunicationPackets.response_MAG_PRECALIB.payload.pointCollectionStatus
                                           + " , "
                                           + CommunicationPackets.response_MAG_PRECALIB.payload.numberOfPointsCollected 
                                           + " , "
                                           + CommunicationPackets.response_MAG_PRECALIB.payload.numberOfSeconds);

                        updateMagCalibrationTimerTextBox( CommunicationPackets.response_MAG_PRECALIB.payload.numberOfSeconds.ToString() );

                        if (CommunicationPackets.response_MAG_PRECALIB.payload.pointCollectionStatus == 1)
                        {
                            updateMagCalibrationTimerTextBox("LIMIT");
                        }
                    }
                    else
                    {
                        GlobalData.LogConsole("[ERRR] Checksum is WRONG");
                        continue;
                    }

                }
                else if (header.packetId == CommunicationPackets.RESPONSE_MAG_PRECALIB_TEST_START_ID)
                {
                    Array.Copy(RESPONSE_HEADER_byteArray, RESPONSE_MAG_PRECALIB_TEST_START_byteArray, CommunicationPackets.HEADER_SIZE_IN_BYTES);
                    ftdiStatus = serialCommunicationChannel.Read(RESPONSE_MAG_PRECALIB_TEST_START_byteArray, (int)CommunicationPackets.HEADER_SIZE_IN_BYTES, (int)(RESPONSE_MAG_PRECALIB_TEST_START_byteArray.Length - RESPONSE_HEADER_byteArray.Length), ref numBytesRead);
                    CommunicationPackets.response_MAG_PRECALIB_TEST_START = CommunicationPackets.byteArrayToStruct(RESPONSE_MAG_PRECALIB_TEST_START_byteArray, CommunicationPackets.response_MAG_PRECALIB_TEST_START);

                    if (!ftdiStatus)
                    {
                        // Wait for a key press
                        GlobalData.LogConsole("Failed to read data (error " + ftdiStatus.ToString() + ")");
                        return;
                    }

                    if (ftdiStatus && numBytesRead == 0)
                    {
                        GlobalData.LogConsole("[INFO] Timeout");
                    }

                    if (CommunicationPackets.verifyPacket(CommunicationPackets.response_MAG_PRECALIB_TEST_START) == true)
                    {
                        GlobalData.LogConsole("[INFO] CommunicationPackets.response_MAG_PRECALIB_TEST_START : "
                                           + CommunicationPackets.response_MAG_PRECALIB_TEST_START.payload.precalibrationParametersAreValid
                                           + " , "
                                           + CommunicationPackets.response_MAG_PRECALIB_TEST_START.payload.magHeading_degrees);

                        updateMagHeadingTestTexBox(CommunicationPackets.response_MAG_PRECALIB_TEST_START.payload.magHeading_degrees.ToString("0.000")
                                                    + "  ("
                                                    + CommunicationPackets.response_MAG_PRECALIB_TEST_START.payload.precalibrationParametersAreValid.ToString()
                                                    + ")"
                                                    );
                    }
                    else
                    {
                        GlobalData.LogConsole("[ERRR] Checksum is WRONG");
                        continue;
                    }
                }
                else
                {
                    GlobalData.LogConsole("[ERRR] Checks ");
                    //MessageBox.Show("sss3");
                    Thread.Sleep(20);
                    continue;
                }
                if (threadStop.WaitOne(0))
                    return;
            }
        }

        private void updateMagCalibrationTimerTextBox(string updateString)
        {
            if (this.txtbxMagCalibrationTimer.InvokeRequired)
            {
                SetCallbackForMagCalibrationTimer d = new SetCallbackForMagCalibrationTimer(updateMagCalibrationTimerTextBox);
                this.Invoke(d, new object[] { updateString });
                txtbxMagCalibrationTimer.Text = updateString;
            }
            else
            {
                txtbxMagCalibrationTimer.Text = updateString;
            }
        }

        private void resetCalibrationButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to reset the calibration ?", "Confirm Calibration Reset", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                // a 'DialogResult.Yes' value was returned from the MessageBox
                // proceed with your deletion
                serialCommunicationChannel.Write(CommunicationPackets.command_MAG_PRECALIB_RESET);
            }
            
        }

        private void testCalibratedMagnetometerButton_Click(object sender, EventArgs e)
        {
            if (testCalibratedMagnetometerButton.Text.Equals("Test Magnetometer") )
            {
                bool bInvalidValue = false;
                bInvalidValue |= !(double.TryParse(txtbxLatitude.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out initParams.latitude));
                bInvalidValue |= !(double.TryParse(txtbxLongtitude.Text, NumberStyles.Any, CultureInfo.InvariantCulture, out initParams.longitude));

                CommunicationPackets.command_MAG_PRECALIB_TEST_START.payload.useLatitudeAndLongitudeValues = 1;
                CommunicationPackets.command_MAG_PRECALIB_TEST_START.payload.initialLatitude_degrees = initParams.latitude;
                CommunicationPackets.command_MAG_PRECALIB_TEST_START.payload.initialLongitude_degrees = initParams.longitude;
                CommunicationPackets.insertChecksum(ref CommunicationPackets.command_MAG_PRECALIB_TEST_START);

                serialCommunicationChannel.ResetPort();

                readMagResponseThread = new Thread(() => ReadMagResponse());
                serialCommunicationChannel.Write(CommunicationPackets.command_MAG_PRECALIB_TEST_START);

                Thread.Sleep(25);
                threadStop.Reset();
                readMagResponseThread.Start(); 
                testCalibratedMagnetometerButton.Text = "Stop Test";
                resetCalibrationButton.Enabled = false;
                startStopMagnetometerCalibrationButton.Enabled = false;
            }
            else if (testCalibratedMagnetometerButton.Text.Equals("Stop Test"))
            {
                try
                {
                    threadStop.Set();
                }
                catch (Exception exception)
                {
                    GlobalData.LogConsole(exception.Message);
                }

                serialCommunicationChannel.ResetPort();

                serialCommunicationChannel.Write(CommunicationPackets.command_MAG_PRECALIB_TEST_STOP);
                testCalibratedMagnetometerButton.Text = "Test Magnetometer";
                resetCalibrationButton.Enabled = true;
                startStopMagnetometerCalibrationButton.Enabled = true;
            }
            else 
            {
                GlobalData.LogConsole("[ERRR] Invalid button state");
            }
        }

        private void updateMagHeadingTestTexBox(string updateString)
        {
            if (this.txtbxMagHeading.InvokeRequired)
            {
                SetCallbackForMagHeadingTest d = new SetCallbackForMagHeadingTest(updateMagHeadingTestTexBox);
                this.Invoke(d, new object[] { updateString });
                txtbxMagHeading.Text = updateString;
            }
            else
            {
                txtbxMagHeading.Text = updateString;
            }
        }


    }
}
