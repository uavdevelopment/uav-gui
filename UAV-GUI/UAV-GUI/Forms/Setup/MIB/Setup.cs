﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;

namespace UAV_GUI
{
    public partial class Setup : Form
    {
        startConnectionToMIB connection = new startConnectionToMIB();
        Thread readNavigationThread;

        public Setup()
        {
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
            
            button6.Enabled = false;
            btnStartNav.Enabled = false;
            button4.Enabled = false;
               }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {
            MessageBox.Show("Sorry, This Feature is not yet implemented.");
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {

            connection.FormBiasSetupInitialization();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            connection.formSetup_ = this;
            connection.startConnectionWithMIB();


        }


        private void button4_Click(object sender, EventArgs e)
        {
            connection.FormMagCalibInitialization();
        }

        private void btnStartNav_Click(object sender, EventArgs e)
        {
           
            connection.StartNavigationForm();
          
          }


                  
        private void ReadNavigationResponse()
        {
            byte[] RESPONSE_HEADER_byteArray = new byte[CommunicationPackets.HEADER_SIZE_IN_BYTES];
            byte[] RESPONSE_NAVIGATION_byteArray = new byte[CommunicationPackets.RESPONSE_NAVIGATION_PACKET_SIZE_IN_BYTES];
            CommunicationPackets.Header header = new CommunicationPackets.Header();

            UInt32 numBytesRead = 0;

            for (;;)
            {


                // read the response of the packet
                RESPONSE_NAVIGATION_byteArray = connection.readFromSerialCom(RESPONSE_NAVIGATION_byteArray.Length);
                //get the header in seperate array
                Array.Copy(RESPONSE_NAVIGATION_byteArray, RESPONSE_HEADER_byteArray, CommunicationPackets.HEADER_SIZE_IN_BYTES);
                header = CommunicationPackets.byteArrayToStruct(RESPONSE_HEADER_byteArray, header);


                    CommunicationPackets.response_NAVIGATION = CommunicationPackets.byteArrayToStruct(RESPONSE_NAVIGATION_byteArray, CommunicationPackets.response_NAVIGATION);

                    if (header.packetId == CommunicationPackets.RESPONSE_NAVIGATION_ID)
                    {

                        if (CommunicationPackets.verifyPacket(RESPONSE_NAVIGATION_byteArray) == true)
                        {
                            MessageBox.Show("got the navigation data");
                        }

                        else
                        {
                            MessageBox.Show("[ERRR] Checksum is WRONG");
                            continue;
                        }

                    }


                    else if (header.packetId == CommunicationPackets.RESPONSE_ACTIVITY_ID)
                    {
                        MessageBox.Show("ACTIVITY ID");
                    }



                    else
                    {
                        MessageBox.Show("unknown ID");
                    }





                }
            }

        private void Setup_Load(object sender, EventArgs e)
        {
            this.tabControl1.SelectedIndex = 2;
        }

        private void Setup_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                connection.serialCommunicationChannel.Close();
            }
            catch {
                return;
            }
        }
        }

    }

