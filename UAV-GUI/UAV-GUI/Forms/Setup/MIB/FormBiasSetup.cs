﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using GLobal;
namespace UAV_GUI
{
    public partial class FormBiasSetup : Form
    {
        protected ManualResetEvent threadStop = new ManualResetEvent(false);
        SerialCommunication serialCommunicationChannel;
        private const String startBiasSetupButtonText = "Start";
        private const String stopBiasSetupButtonText  = "Stop";
        uint totalSeconds;
        Thread readBiasSetupStartThread;
        delegate void SetCallbackForBiasSetupStart(string updateString);
        delegate void SetCallbackForOKButton(bool flag);
        public const int numberOfSecondsForBiasSetup = 60;

        public FormBiasSetup()
        {
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
        }

        public FormBiasSetup (SerialCommunication serialCommunicationObjectReference) : this()
        {
            serialCommunicationChannel = serialCommunicationObjectReference;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.startStopBiasSetupBtn.Text = startBiasSetupButtonText;
            this.startStopBiasSetupBtn.BackgroundImage = global::UAV_GUI.Properties.Resources.start_64;
            this.startStopBiasSetupBtn.BackgroundImage = global::UAV_GUI.Properties.Resources.start_64;
        }

        private void resetBiasSetupBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to reset the bias setup ?", "Confirm Bias Reset", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //-- A 'DialogResult.Yes' value was returned from the MessageBox proceed with your deletion
                //serialCommunicationChannel.Write(CommunicationPackets.);
            }
        }

        private void startStopBiasSetupBtn_Click(object sender, EventArgs e)
        {
            if (startStopBiasSetupBtn.Text.Equals(startBiasSetupButtonText))
            {
                startStopBiasSetupBtn.Text = stopBiasSetupButtonText;
                this.startStopBiasSetupBtn.BackgroundImage = global::UAV_GUI.Properties.Resources.stop_64;
                startStopBiasSetupBtn.Enabled = true;
                btnOK.Enabled = false;

                serialCommunicationChannel.ResetPort();



                readBiasSetupStartThread = new Thread(() => ReadBiasSetupStartResponse());
                
                serialCommunicationChannel.Write(CommunicationPackets.command_BIAS_SETUP_START);
                Thread.Sleep(25);
                threadStop.Reset();
                readBiasSetupStartThread.Start();
               

                //btnOK.Enabled = true;
                //startStopBiasSetupBtn.Enabled = true;

            }
            else if (startStopBiasSetupBtn.Text.Equals(stopBiasSetupButtonText))
            {
                if (DialogResult.Yes == MessageBox.Show(this,
                                                        "Stop Bias Setup?",
                                                        "Stop Bias Setup?",
                                                         MessageBoxButtons.YesNo,
                                                         MessageBoxIcon.Question,
                                                         MessageBoxDefaultButton.Button2))
                {
                    try
                    {
                        threadStop.Set();
                        //readBiasSetupStartThread.Abort();
                    }
                    catch (Exception exception)
                    {
                        GlobalData.LogConsole(exception.Message);
                    }

                    serialCommunicationChannel.ResetPort();

                    startStopBiasSetupBtn.Text = startBiasSetupButtonText;
                    this.startStopBiasSetupBtn.BackgroundImage = global::UAV_GUI.Properties.Resources.start_64;
                    btnOK.Enabled = true;

                    serialCommunicationChannel.ResetPort();
                    serialCommunicationChannel.Write(CommunicationPackets.command_BIAS_SETUP_STOP);
                }
            }
            else 
            {}
        }

        private void ReadBiasSetupStartResponse()
        {
            byte[] RESPONSE_HEADER_byteArray = new byte[CommunicationPackets.HEADER_SIZE_IN_BYTES];
            byte[] RESPONSE_BIAS_SETUP_START_byteArray = new byte[CommunicationPackets.RESPONSE_BIAS_SETUP_START_PACKET_SIZE_IN_BYTES];

            CommunicationPackets.Header header = new CommunicationPackets.Header();

            Int32 numBytesRead = 0;
            bool ftdiStatus;
            string updateString = "Initializing ";
            int numberOfInitializationPointsToDraw = 0;

            for (; ; )
            {
                //
                
                numBytesRead = serialCommunicationChannel.Read(RESPONSE_HEADER_byteArray);
                header = CommunicationPackets.byteArrayToStruct(RESPONSE_HEADER_byteArray, header);
                GlobalData.LogConsole("Packet ID = " + header.packetId);
                if (header.packetId == CommunicationPackets.RESPONSE_BIAS_SETUP_START_ID)
                {
                    Array.Copy(RESPONSE_HEADER_byteArray, RESPONSE_BIAS_SETUP_START_byteArray, CommunicationPackets.HEADER_SIZE_IN_BYTES);
           
                    ftdiStatus = serialCommunicationChannel.Read(RESPONSE_BIAS_SETUP_START_byteArray, (int)CommunicationPackets.HEADER_SIZE_IN_BYTES, (int)(RESPONSE_BIAS_SETUP_START_byteArray.Length - RESPONSE_HEADER_byteArray.Length), ref numBytesRead);
                    CommunicationPackets.response_BIAS_SETUP_START = CommunicationPackets.byteArrayToStruct(RESPONSE_BIAS_SETUP_START_byteArray, CommunicationPackets.response_BIAS_SETUP_START);

                    if (ftdiStatus != true)
                    {
                        // Wait for a key press
                        GlobalData.LogConsole("Failed to read data (error " + ftdiStatus.ToString() + ")");

                        MessageBox.Show("Failed");
                        return;
                    }

                    if (ftdiStatus && numBytesRead == 0)
                    {
                        MessageBox.Show("sss1");
                        GlobalData.LogConsole("[INFO] Timeout");
                    }

                    if (CommunicationPackets.verifyPacket(RESPONSE_BIAS_SETUP_START_byteArray) == true)
                    {
                        totalSeconds = CommunicationPackets.response_BIAS_SETUP_START.payload.numberOfSeconds;

                        GlobalData.LogConsole("[INFO] CommunicationPackets.response_MAG_PRECALIB : " + totalSeconds);

                        
                        
                        if (totalSeconds >= numberOfSecondsForBiasSetup)
                        {
                            updateString = "System Initialized";
                            updateNumberOfSecondsTestTexBox(updateString);
                            progressBar1.Value = (int)totalSeconds;
                            enableDisableOkButton(true);
                            serialCommunicationChannel.ResetPort();
                            serialCommunicationChannel.Write(CommunicationPackets.command_BIAS_SETUP_STOP);
                            MessageBox.Show("System Initialized");
                            this.startStopBiasSetupBtn.Text = "Start";
                            this.startStopBiasSetupBtn.BackgroundImage = global::UAV_GUI.Properties.Resources.start_64;
                            break;
                        }

                        else if (totalSeconds != numberOfSecondsForBiasSetup)
                        {

                            progressBar1.Value = (int)totalSeconds;
                            if (numberOfInitializationPointsToDraw == 4)
                            {
                                updateString = "Initializing ";
                                updateNumberOfSecondsTestTexBox(updateString);
                                numberOfInitializationPointsToDraw = 1;
                            }
                            else
                            {
                                numberOfInitializationPointsToDraw++;
                                updateString = updateString + ".";
                                updateNumberOfSecondsTestTexBox(updateString);
                            }
                        } 

                    }
                    else
                    {
                        GlobalData.LogConsole("[ERRR] Checksum is WRONG");
                        //MessageBox.Show("sss");
                        Thread.Sleep(25);
                        continue;
                    }
                    

                }
                else
                {
                    GlobalData.LogConsole("[ERRR] Checks ");
                    //MessageBox.Show("sss3");
                    Thread.Sleep(25);
                    continue;
                }
                if (threadStop.WaitOne(0))
                    return;
            }
        }

        private void updateNumberOfSecondsTestTexBox(string updateString)
        {
            if (this.txtBoxNumberOfSeconds.InvokeRequired)
            {
                SetCallbackForBiasSetupStart d = new SetCallbackForBiasSetupStart(updateNumberOfSecondsTestTexBox);
                this.Invoke(d, new object[] { updateString });
                txtBoxNumberOfSeconds.Text = updateString;
            }
            else
            {
                txtBoxNumberOfSeconds.Text = updateString;
            }
        }

        private void enableDisableOkButton(bool enableDisableFlag)
        {
            if (this.txtBoxNumberOfSeconds.InvokeRequired)
            {
                SetCallbackForOKButton d = new SetCallbackForOKButton(enableDisableOkButton);
                this.Invoke(d, new object[] { enableDisableFlag });
                btnOK.Enabled = enableDisableFlag;
            }
            else
            {
                btnOK.Enabled = enableDisableFlag;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {

        }

        private void FormBiasSetup_Load(object sender, EventArgs e)
        {

        }
    }
}
