﻿namespace UAV_GUI
{
    partial class FormBiasSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txtBoxNumberOfSeconds = new System.Windows.Forms.TextBox();
            this.startStopBiasSetupBtn = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.progressBar1);
            this.groupBox1.Controls.Add(this.txtBoxNumberOfSeconds);
            this.groupBox1.Controls.Add(this.startStopBiasSetupBtn);
            this.groupBox1.Location = new System.Drawing.Point(12, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(233, 123);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Setup";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(45, 34);
            this.progressBar1.Maximum = 60;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(136, 23);
            this.progressBar1.TabIndex = 3;
            // 
            // txtBoxNumberOfSeconds
            // 
            this.txtBoxNumberOfSeconds.Location = new System.Drawing.Point(0, 69);
            this.txtBoxNumberOfSeconds.Name = "txtBoxNumberOfSeconds";
            this.txtBoxNumberOfSeconds.ReadOnly = true;
            this.txtBoxNumberOfSeconds.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBoxNumberOfSeconds.Size = new System.Drawing.Size(10, 20);
            this.txtBoxNumberOfSeconds.TabIndex = 2;
            this.txtBoxNumberOfSeconds.Text = "Initializing";
            this.txtBoxNumberOfSeconds.Visible = false;
            // 
            // startStopBiasSetupBtn
            // 
            this.startStopBiasSetupBtn.BackgroundImage = global::UAV_GUI.Properties.Resources.start_64;
            this.startStopBiasSetupBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.startStopBiasSetupBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.startStopBiasSetupBtn.Location = new System.Drawing.Point(85, 69);
            this.startStopBiasSetupBtn.Name = "startStopBiasSetupBtn";
            this.startStopBiasSetupBtn.Size = new System.Drawing.Size(64, 51);
            this.startStopBiasSetupBtn.TabIndex = 0;
            this.startStopBiasSetupBtn.UseVisualStyleBackColor = true;
            this.startStopBiasSetupBtn.Click += new System.EventHandler(this.startStopBiasSetupBtn_Click);
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(97, 144);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(64, 30);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // FormBiasSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(259, 181);
            this.ControlBox = false;
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupBox1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormBiasSetup";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Initialization";
            this.Load += new System.EventHandler(this.FormBiasSetup_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtBoxNumberOfSeconds;
        private System.Windows.Forms.Button startStopBiasSetupBtn;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}