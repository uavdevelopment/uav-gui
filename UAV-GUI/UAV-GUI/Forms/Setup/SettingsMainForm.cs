﻿using FileClasses.VRS_Files;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UAV_GUI;
using UAV_GUI.FlightClasses;
namespace UAV_GUI.Forms.Setup
{
    public partial class SettingsMainForm : Form
    {
        static MIB_Setup myMibForm;
        UAV_GUI.Setup setupForm;
        UAV_GUI.Forms.MicroPilotSetup MicroPilotSetupForm;
        UAV_GUI.Forms.Setup.MP.GroundSetup GroundSetupForm;
        TabControl sideTab = new TabControl();
        public SettingsMainForm(Flight myFlight , VrsFileHandler vrsfile)
        {
            InitializeComponent();
            sideTab.Controls.Clear();
            //sideTab.Alignment = TabAlignment.Left;
            //sideTab.SizeMode = TabSizeMode.Fixed;
            //sideTab.ItemSize = new Size(25, 100);
            //sideTab.DrawMode = TabDrawMode.OwnerDrawFixed;
            //sideTab.DrawItem += new DrawItemEventHandler(sideTab_DrawItem);
            sideTab.Dock = DockStyle.Fill;

            //General Tab Page
            TabPage general = new TabPage("General");
            setupForm = new UAV_GUI.Setup();
            setupForm.TopLevel = false;
            general.Controls.Add(setupForm);
            setupForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            setupForm.Dock = DockStyle.Fill;
            setupForm.Show();

            //Auto Pilot Tab Page
            TabPage autoPilot = new TabPage("Auto Pilot");
             MicroPilotSetupForm = new UAV_GUI.Forms.MicroPilotSetup(ref vrsfile, myFlight);
            MicroPilotSetupForm.TopLevel = false;
            autoPilot.Controls.Add(MicroPilotSetupForm);
            MicroPilotSetupForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            MicroPilotSetupForm.Dock = DockStyle.Fill;
            MicroPilotSetupForm.AutoSize = true;
            MicroPilotSetupForm.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            MicroPilotSetupForm.Show();

            //Ground Tab Page
            TabPage ground = new TabPage("Ground");
             GroundSetupForm = new UAV_GUI.Forms.Setup.MP.GroundSetup();
            GroundSetupForm.TopLevel = false;
            ground.Controls.Add(GroundSetupForm);
            GroundSetupForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            GroundSetupForm.Dock = DockStyle.Fill;
            GroundSetupForm.Show();

            //MIB Tab Page
            TabPage MIB = new TabPage("MIB");
            myMibForm = new MIB_Setup();
            myMibForm.TopLevel = false;
            MIB.Controls.Add(myMibForm);
            myMibForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            myMibForm.Dock = DockStyle.Fill;
            myMibForm.Show();

            //Add Tab Pages to Tab Control
            sideTab.Controls.Add(general);
            sideTab.Controls.Add(autoPilot);
            sideTab.Controls.Add(ground);
            sideTab.Controls.Add(MIB);
            this.Controls.Add(sideTab);
        }

        private void SettingsMainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
             myMibForm.Close();
             setupForm.Close();
             MicroPilotSetupForm.Close();
             GroundSetupForm.Close();
        }
    }
}
