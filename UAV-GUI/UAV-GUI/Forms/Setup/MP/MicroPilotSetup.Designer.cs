﻿namespace UAV_GUI.Forms
{
    partial class MicroPilotSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MicroPilotSetup));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label52 = new System.Windows.Forms.Label();
            this.maxFlightAltT = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.maxFlightDistT = new System.Windows.Forms.TextBox();
            this.minTargetSpeedT = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.minTargetAltT = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.onGroundSpeedT = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.stallSpeedT = new System.Windows.Forms.TextBox();
            this.maxTargetSpeedT = new System.Windows.Forms.TextBox();
            this.maxTargrtAltT = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.invertFenceEWcheckBox = new System.Windows.Forms.CheckBox();
            this.invertFenceNScheckBox = new System.Windows.Forms.CheckBox();
            this.southFenceT = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.westFenceT = new System.Windows.Forms.TextBox();
            this.eastFenceT = new System.Windows.Forms.TextBox();
            this.northFenceT = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.fatalErrorSlider = new System.Windows.Forms.HScrollBar();
            this.slewLimitSlider = new System.Windows.Forms.HScrollBar();
            this.onGroundSlider = new System.Windows.Forms.HScrollBar();
            this.takeoffSlider = new System.Windows.Forms.HScrollBar();
            this.descentSlider = new System.Windows.Forms.HScrollBar();
            this.climbSlider = new System.Windows.Forms.HScrollBar();
            this.approachSlider = new System.Windows.Forms.HScrollBar();
            this.cruiseSlider = new System.Windows.Forms.HScrollBar();
            this.onGroundL = new System.Windows.Forms.Label();
            this.slweLimitL = new System.Windows.Forms.Label();
            this.fatalErrorMaxL = new System.Windows.Forms.Label();
            this.approachL = new System.Windows.Forms.Label();
            this.climbL = new System.Windows.Forms.Label();
            this.descentL = new System.Windows.Forms.Label();
            this.takeoffL = new System.Windows.Forms.Label();
            this.cruiseL = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.maxLunchPlatformSpeedT = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.flyTosFromTosLineUp = new System.Windows.Forms.CheckBox();
            this.fromTosToFlyTos = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.alwaysUseLineClimb = new System.Windows.Forms.CheckBox();
            this.offGroundAlgoT = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.lockedPichtT = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.takeoffPichT = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.enableDeadcheckBox = new System.Windows.Forms.CheckBox();
            this.levelFlightModecomboBox = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.wpReduceTurnT = new System.Windows.Forms.TextBox();
            this.wpDiaT = new System.Windows.Forms.TextBox();
            this.cimbMarginT = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.descentTypeT = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.allowNnavigationAirSpeedcheckBox = new System.Windows.Forms.CheckBox();
            this.descentRateT = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.climbSpeedT = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.descentSpeedT = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.maxSpeedIncreasT = new System.Windows.Forms.TextBox();
            this.cruiseSpeedT = new System.Windows.Forms.TextBox();
            this.rotationSpeedT = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.CompatiblityCheckBox = new System.Windows.Forms.CheckBox();
            this.CalculateAlironCheckBox = new System.Windows.Forms.CheckBox();
            this.SelectedPIDLloopCombo = new System.Windows.Forms.ComboBox();
            this.label96 = new System.Windows.Forms.Label();
            this.feedForwardInputMinT = new System.Windows.Forms.TextBox();
            this.feedForwardInputMaxT = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.feedForward3T = new System.Windows.Forms.TextBox();
            this.feedForward2T = new System.Windows.Forms.TextBox();
            this.feedForward1T = new System.Windows.Forms.TextBox();
            this.feedForward0T = new System.Windows.Forms.TextBox();
            this.DifferentialInputMinT = new System.Windows.Forms.TextBox();
            this.DifferentialInputMaxT = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.Differential3T = new System.Windows.Forms.TextBox();
            this.Differential2T = new System.Windows.Forms.TextBox();
            this.Differential1T = new System.Windows.Forms.TextBox();
            this.Differential0T = new System.Windows.Forms.TextBox();
            this.ResAntiWindupMinT = new System.Windows.Forms.TextBox();
            this.ResAntiWindupMaxT = new System.Windows.Forms.TextBox();
            this.inteqralInputMinT = new System.Windows.Forms.TextBox();
            this.inteqralInputMaxT = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.inteqral3T = new System.Windows.Forms.TextBox();
            this.inteqral2T = new System.Windows.Forms.TextBox();
            this.inteqral1T = new System.Windows.Forms.TextBox();
            this.inteqral0T = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.inputMin = new System.Windows.Forms.TextBox();
            this.inputMax = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Propotional3T = new System.Windows.Forms.TextBox();
            this.Propotional2T = new System.Windows.Forms.TextBox();
            this.Propotional1T = new System.Windows.Forms.TextBox();
            this.Propotional0T = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Range2T = new System.Windows.Forms.TextBox();
            this.Range1T = new System.Windows.Forms.TextBox();
            this.Range3T = new System.Windows.Forms.TextBox();
            this.Range0T = new System.Windows.Forms.TextBox();
            this.Sch3T = new System.Windows.Forms.TextBox();
            this.Sch2T = new System.Windows.Forms.TextBox();
            this.Sch1T = new System.Windows.Forms.TextBox();
            this.Sch0T = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.SlewLimitsT = new System.Windows.Forms.TextBox();
            this.ResultMinT = new System.Windows.Forms.TextBox();
            this.ResultMaxT = new System.Windows.Forms.TextBox();
            this.ScheduledT = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.GainEffects = new System.Windows.Forms.RadioButton();
            this.RealUnites = new System.Windows.Forms.RadioButton();
            this.AutoPilotUnites = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.EnableYawDamper = new System.Windows.Forms.CheckBox();
            this.SmoothGainScheduling = new System.Windows.Forms.CheckBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.TO = new System.Windows.Forms.ComboBox();
            this.SpoilerRCInChannelT = new System.Windows.Forms.TextBox();
            this.FlapsRCInChannelT = new System.Windows.Forms.TextBox();
            this.SpoilerSlewLlimitT = new System.Windows.Forms.TextBox();
            this.InputCalculationT = new System.Windows.Forms.TextBox();
            this.DisableMixingLimit = new System.Windows.Forms.CheckBox();
            this.DisableRStaffServo = new System.Windows.Forms.CheckBox();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.lockAileronSlider = new System.Windows.Forms.HScrollBar();
            this.lockRudderSlider = new System.Windows.Forms.HScrollBar();
            this.lockElavatorSlider = new System.Windows.Forms.HScrollBar();
            this.lockRudderL = new System.Windows.Forms.Label();
            this.lockAileronL = new System.Windows.Forms.Label();
            this.lockElvatorL = new System.Windows.Forms.Label();
            this.DisengageLockSpeedT = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.EnableSevoLock = new System.Windows.Forms.CheckBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.GyroControlServoT = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.SpoilerServoT = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.NoiseWheelServoT = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.ReleaseServoT = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.ParachuteServoT = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.throttleIdel = new System.Windows.Forms.HScrollBar();
            this.t8 = new System.Windows.Forms.HScrollBar();
            this.t7 = new System.Windows.Forms.HScrollBar();
            this.t6 = new System.Windows.Forms.HScrollBar();
            this.t5 = new System.Windows.Forms.HScrollBar();
            this.t4 = new System.Windows.Forms.HScrollBar();
            this.t3 = new System.Windows.Forms.HScrollBar();
            this.t2 = new System.Windows.Forms.HScrollBar();
            this.t1 = new System.Windows.Forms.HScrollBar();
            this.z8 = new System.Windows.Forms.HScrollBar();
            this.z7 = new System.Windows.Forms.HScrollBar();
            this.z6 = new System.Windows.Forms.HScrollBar();
            this.z5 = new System.Windows.Forms.HScrollBar();
            this.z4 = new System.Windows.Forms.HScrollBar();
            this.z3 = new System.Windows.Forms.HScrollBar();
            this.z2 = new System.Windows.Forms.HScrollBar();
            this.z1 = new System.Windows.Forms.HScrollBar();
            this.label62 = new System.Windows.Forms.Label();
            this.s6t = new System.Windows.Forms.Label();
            this.s7t = new System.Windows.Forms.Label();
            this.st5 = new System.Windows.Forms.Label();
            this.s8t = new System.Windows.Forms.Label();
            this.s5t = new System.Windows.Forms.Label();
            this.s7 = new System.Windows.Forms.Label();
            this.s4 = new System.Windows.Forms.Label();
            this.s3 = new System.Windows.Forms.Label();
            this.s2 = new System.Windows.Forms.Label();
            this.s1 = new System.Windows.Forms.Label();
            this.p9 = new System.Windows.Forms.Label();
            this.p8 = new System.Windows.Forms.Label();
            this.p7 = new System.Windows.Forms.Label();
            this.p6 = new System.Windows.Forms.Label();
            this.p5 = new System.Windows.Forms.Label();
            this.p4 = new System.Windows.Forms.Label();
            this.p3 = new System.Windows.Forms.Label();
            this.p2 = new System.Windows.Forms.Label();
            this.p1 = new System.Windows.Forms.Label();
            this.init8 = new System.Windows.Forms.ComboBox();
            this.init7 = new System.Windows.Forms.ComboBox();
            this.init6 = new System.Windows.Forms.ComboBox();
            this.init5 = new System.Windows.Forms.ComboBox();
            this.init4 = new System.Windows.Forms.ComboBox();
            this.init3 = new System.Windows.Forms.ComboBox();
            this.init2 = new System.Windows.Forms.ComboBox();
            this.init1 = new System.Windows.Forms.ComboBox();
            this.TIPercentage = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.swap8 = new System.Windows.Forms.CheckBox();
            this.swap7 = new System.Windows.Forms.CheckBox();
            this.swap6 = new System.Windows.Forms.CheckBox();
            this.swap5 = new System.Windows.Forms.CheckBox();
            this.swap4 = new System.Windows.Forms.CheckBox();
            this.swap3 = new System.Windows.Forms.CheckBox();
            this.swap2 = new System.Windows.Forms.CheckBox();
            this.swap1 = new System.Windows.Forms.CheckBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.s8 = new System.Windows.Forms.Label();
            this.s6 = new System.Windows.Forms.Label();
            this.s5 = new System.Windows.Forms.Label();
            this.s4t = new System.Windows.Forms.Label();
            this.s3t = new System.Windows.Forms.Label();
            this.s2t = new System.Windows.Forms.Label();
            this.s1t = new System.Windows.Forms.Label();
            this.StaffServoCICTimeoutT = new System.Windows.Forms.TextBox();
            this.FlapSlewLimitT = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.servoRudderComboBox = new System.Windows.Forms.ComboBox();
            this.ServoTypeComboBox = new System.Windows.Forms.ComboBox();
            this.ServoNumComboBox = new System.Windows.Forms.ComboBox();
            this.EchoServoAdjustment = new System.Windows.Forms.ComboBox();
            this.label59 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.customuarts = new System.Windows.Forms.Button();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.SLRCenterfailure = new System.Windows.Forms.CheckBox();
            this.SLRCsecondary = new System.Windows.Forms.CheckBox();
            this.MLRCenterfailure = new System.Windows.Forms.CheckBox();
            this.LRCuselrc = new System.Windows.Forms.CheckBox();
            this.LRCsecodarycomport = new System.Windows.Forms.ComboBox();
            this.LRConlyreadservos = new System.Windows.Forms.CheckBox();
            this.label136 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.Lr = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.UART2Rxchannel = new System.Windows.Forms.TextBox();
            this.UART2disablebinding = new System.Windows.Forms.CheckBox();
            this.UART2Txprotocolfield = new System.Windows.Forms.ComboBox();
            this.UART2Txparity = new System.Windows.Forms.TextBox();
            this.UART2Txtimeout = new System.Windows.Forms.TextBox();
            this.UART2Txdatabits = new System.Windows.Forms.TextBox();
            this.UART2Txbaud = new System.Windows.Forms.TextBox();
            this.UART2Txchannel = new System.Windows.Forms.TextBox();
            this.UART2Rxparity = new System.Windows.Forms.TextBox();
            this.UART2Rxdatabits = new System.Windows.Forms.TextBox();
            this.UART2Rxbaud = new System.Windows.Forms.TextBox();
            this.UART2protocol = new System.Windows.Forms.ComboBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.UART1Rxchannel = new System.Windows.Forms.TextBox();
            this.UART1disablebinding = new System.Windows.Forms.CheckBox();
            this.UART1protocolfield = new System.Windows.Forms.ComboBox();
            this.UART1Txparity = new System.Windows.Forms.TextBox();
            this.UART1Txtimeout = new System.Windows.Forms.TextBox();
            this.UART1Txbatabits = new System.Windows.Forms.TextBox();
            this.UART1Txbaud = new System.Windows.Forms.TextBox();
            this.UART1Txchannel = new System.Windows.Forms.TextBox();
            this.UART1Rxparity = new System.Windows.Forms.TextBox();
            this.UART1Rxdatabits = new System.Windows.Forms.TextBox();
            this.UART1Rxbaud = new System.Windows.Forms.TextBox();
            this.UART1protocol = new System.Windows.Forms.ComboBox();
            this.label116 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.EnableBindingfeature = new System.Windows.Forms.CheckBox();
            this.label139 = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.Vrsheadercommands = new System.Windows.Forms.TextBox();
            this.Vrsfootercommands = new System.Windows.Forms.CheckBox();
            this.label138 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.Gcschecksum = new System.Windows.Forms.CheckBox();
            this.GcsParity = new System.Windows.Forms.ComboBox();
            this.Gcsflowcontrol = new System.Windows.Forms.ComboBox();
            this.Gcsbaud = new System.Windows.Forms.TextBox();
            this.GcsStrenghtchannel = new System.Windows.Forms.TextBox();
            this.Gcsenabledialing = new System.Windows.Forms.TextBox();
            this.Gcsphonesecond = new System.Windows.Forms.TextBox();
            this.Gcsphonefirst = new System.Windows.Forms.TextBox();
            this.Gcsidentifier = new System.Windows.Forms.TextBox();
            this.GcsStrenghtminimum = new System.Windows.Forms.TextBox();
            this.GcsCommandrepeat = new System.Windows.Forms.TextBox();
            this.GcsPadCharacter = new System.Windows.Forms.TextBox();
            this.Gcsintialreport = new System.Windows.Forms.TextBox();
            this.Gcstimeount = new System.Windows.Forms.TextBox();
            this.lable34 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.lable12 = new System.Windows.Forms.Label();
            this.lable33 = new System.Windows.Forms.Label();
            this.lable44 = new System.Windows.Forms.Label();
            this.lable66 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.lable3 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.lable = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.GPS = new System.Windows.Forms.TabPage();
            this.Novatelgroup = new System.Windows.Forms.GroupBox();
            this.Novatelbasestation = new System.Windows.Forms.CheckBox();
            this.Novatelenablediableothe = new System.Windows.Forms.CheckBox();
            this.Novatelminimumspeed = new System.Windows.Forms.CheckBox();
            this.Novatelusel1flaot = new System.Windows.Forms.CheckBox();
            this.Novatelenablecorrection = new System.Windows.Forms.CheckBox();
            this.Novatelenablediasblenovatel = new System.Windows.Forms.CheckBox();
            this.SBASgroup = new System.Windows.Forms.GroupBox();
            this.SBASsatellite = new System.Windows.Forms.ComboBox();
            this.SBASnumberofchannel = new System.Windows.Forms.ComboBox();
            this.SBASenterPRNs = new System.Windows.Forms.TextBox();
            this.label150 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.SBASlable = new System.Windows.Forms.Label();
            this.SBASusetestmode = new System.Windows.Forms.CheckBox();
            this.SBASapplyintegrity = new System.Windows.Forms.CheckBox();
            this.SBASenable = new System.Windows.Forms.CheckBox();
            this.Ubloxgroup = new System.Windows.Forms.GroupBox();
            this.ubloxdynamicmodel = new System.Windows.Forms.ComboBox();
            this.label146 = new System.Windows.Forms.Label();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.GPS2stopbits = new System.Windows.Forms.TextBox();
            this.GPS2bits = new System.Windows.Forms.TextBox();
            this.GPS2baud = new System.Windows.Forms.TextBox();
            this.GPS2flowcontrol = new System.Windows.Forms.ComboBox();
            this.GPS2parity = new System.Windows.Forms.ComboBox();
            this.label145 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.GPS1hemisphere = new System.Windows.Forms.CheckBox();
            this.GPS1kalmanfilter = new System.Windows.Forms.CheckBox();
            this.GPS1usegpsspeed = new System.Windows.Forms.CheckBox();
            this.GPS1usegpsaltitude = new System.Windows.Forms.CheckBox();
            this.GPS1fakegps = new System.Windows.Forms.CheckBox();
            this.GPS1fixrate = new System.Windows.Forms.TextBox();
            this.GPS1bits = new System.Windows.Forms.TextBox();
            this.GPS1baud = new System.Windows.Forms.TextBox();
            this.GPS1parity = new System.Windows.Forms.ComboBox();
            this.GPS1type = new System.Windows.Forms.ComboBox();
            this.kalmanfilter = new System.Windows.Forms.Label();
            this.hemisphere = new System.Windows.Forms.Label();
            this.fakegps = new System.Windows.Forms.Label();
            this.usegpsaltitude = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.usegpsspeed = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.Datalogdonoterase = new System.Windows.Forms.CheckBox();
            this.Datalogenablecustom = new System.Windows.Forms.CheckBox();
            this.Dataloglogging = new System.Windows.Forms.CheckBox();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.button26 = new System.Windows.Forms.Button();
            this.Datalogcustomheadfltext8 = new System.Windows.Forms.TextBox();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.Datalogcustomheadfltext1 = new System.Windows.Forms.TextBox();
            this.Datalogcustomheadfltext2 = new System.Windows.Forms.TextBox();
            this.Datalogcustomheadfltext3 = new System.Windows.Forms.TextBox();
            this.Datalogcustomheadfltext4 = new System.Windows.Forms.TextBox();
            this.Datalogcustomheadfltext5 = new System.Windows.Forms.TextBox();
            this.Datalogcustomheadfltext6 = new System.Windows.Forms.TextBox();
            this.Datalogcustomheadfltext7 = new System.Windows.Forms.TextBox();
            this.Datalogcustomheadflable1 = new System.Windows.Forms.Label();
            this.Datalogcustomheadflable2 = new System.Windows.Forms.Label();
            this.Datalogcustomheadflable3 = new System.Windows.Forms.Label();
            this.Datalogcustomheadflable4 = new System.Windows.Forms.Label();
            this.Datalogcustomheadflable5 = new System.Windows.Forms.Label();
            this.Datalogcustomheadflable6 = new System.Windows.Forms.Label();
            this.Datalogcustomheadflable7 = new System.Windows.Forms.Label();
            this.Datalogcustomheadflable8 = new System.Windows.Forms.Label();
            this.Datalogform1 = new System.Windows.Forms.GroupBox();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Datalogcustomftext14 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext15 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext16 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext17 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext18 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext19 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext20 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext21 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext22 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext23 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext24 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext13 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext2 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext3 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext4 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext5 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext6 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext7 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext8 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext9 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext10 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext11 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext12 = new System.Windows.Forms.TextBox();
            this.Datalogcustomftext1 = new System.Windows.Forms.TextBox();
            this.Datalogcustomflable18 = new System.Windows.Forms.Label();
            this.Datalogcustomflable17 = new System.Windows.Forms.Label();
            this.Datalogcustomflable19 = new System.Windows.Forms.Label();
            this.Datalogcustomflable20 = new System.Windows.Forms.Label();
            this.Datalogcustomflable21 = new System.Windows.Forms.Label();
            this.Datalogcustomflable22 = new System.Windows.Forms.Label();
            this.Datalogcustomflable23 = new System.Windows.Forms.Label();
            this.Datalogcustomflable24 = new System.Windows.Forms.Label();
            this.Datalogcustomflable15 = new System.Windows.Forms.Label();
            this.Datalogcustomflable14 = new System.Windows.Forms.Label();
            this.Datalogcustomflable13 = new System.Windows.Forms.Label();
            this.Datalogcustomflable16 = new System.Windows.Forms.Label();
            this.Datalogcustomflable4 = new System.Windows.Forms.Label();
            this.Datalogcustomflable5 = new System.Windows.Forms.Label();
            this.Datalogcustomflable6 = new System.Windows.Forms.Label();
            this.Datalogcustomflable7 = new System.Windows.Forms.Label();
            this.Datalogcustomflable8 = new System.Windows.Forms.Label();
            this.Datalogcustomflable9 = new System.Windows.Forms.Label();
            this.Datalogcustomflable10 = new System.Windows.Forms.Label();
            this.Datalogcustomflable11 = new System.Windows.Forms.Label();
            this.Datalogcustomflable12 = new System.Windows.Forms.Label();
            this.Datalogcustomflable2 = new System.Windows.Forms.Label();
            this.Datalogcustomflable1 = new System.Windows.Forms.Label();
            this.Datalogcustomflable3 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.agllowthrottleswitch = new System.Windows.Forms.TextBox();
            this.agllowthrottlesetting = new System.Windows.Forms.TextBox();
            this.aglswitchaltitude = new System.Windows.Forms.TextBox();
            this.aglmode = new System.Windows.Forms.ComboBox();
            this.aglsensor = new System.Windows.Forms.ComboBox();
            this.label140 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.saveVrsFile = new System.Windows.Forms.Button();
            this.Save = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.open_vrsfile = new System.Windows.Forms.Button();
            this.label147 = new System.Windows.Forms.Label();
            this.vrsname = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.GPS.SuspendLayout();
            this.Novatelgroup.SuspendLayout();
            this.SBASgroup.SuspendLayout();
            this.Ubloxgroup.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.Datalogform1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.GPS);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(2, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(920, 773);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox9);
            this.tabPage1.Controls.Add(this.groupBox8);
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(912, 747);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Flight";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label52);
            this.groupBox9.Controls.Add(this.maxFlightAltT);
            this.groupBox9.Controls.Add(this.label51);
            this.groupBox9.Controls.Add(this.maxFlightDistT);
            this.groupBox9.Controls.Add(this.minTargetSpeedT);
            this.groupBox9.Controls.Add(this.label46);
            this.groupBox9.Controls.Add(this.label49);
            this.groupBox9.Controls.Add(this.minTargetAltT);
            this.groupBox9.Controls.Add(this.label48);
            this.groupBox9.Controls.Add(this.onGroundSpeedT);
            this.groupBox9.Controls.Add(this.label44);
            this.groupBox9.Controls.Add(this.label45);
            this.groupBox9.Controls.Add(this.stallSpeedT);
            this.groupBox9.Controls.Add(this.maxTargetSpeedT);
            this.groupBox9.Controls.Add(this.maxTargrtAltT);
            this.groupBox9.Controls.Add(this.label47);
            this.groupBox9.Controls.Add(this.invertFenceEWcheckBox);
            this.groupBox9.Controls.Add(this.invertFenceNScheckBox);
            this.groupBox9.Controls.Add(this.southFenceT);
            this.groupBox9.Controls.Add(this.label43);
            this.groupBox9.Controls.Add(this.label41);
            this.groupBox9.Controls.Add(this.label40);
            this.groupBox9.Controls.Add(this.westFenceT);
            this.groupBox9.Controls.Add(this.eastFenceT);
            this.groupBox9.Controls.Add(this.northFenceT);
            this.groupBox9.Controls.Add(this.label42);
            this.groupBox9.Location = new System.Drawing.Point(386, 247);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(420, 253);
            this.groupBox9.TabIndex = 4;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Operational Limits";
            this.groupBox9.Enter += new System.EventHandler(this.groupBox9_Enter);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(211, 213);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(110, 13);
            this.label52.TabIndex = 47;
            this.label52.Text = "Max Flight Altitude (m)";
            // 
            // maxFlightAltT
            // 
            this.maxFlightAltT.Location = new System.Drawing.Point(338, 213);
            this.maxFlightAltT.Name = "maxFlightAltT";
            this.maxFlightAltT.Size = new System.Drawing.Size(63, 20);
            this.maxFlightAltT.TabIndex = 46;
            this.maxFlightAltT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(13, 216);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(117, 13);
            this.label51.TabIndex = 44;
            this.label51.Text = "Max Flight Distance (m)";
            // 
            // maxFlightDistT
            // 
            this.maxFlightDistT.Location = new System.Drawing.Point(142, 210);
            this.maxFlightDistT.Name = "maxFlightDistT";
            this.maxFlightDistT.Size = new System.Drawing.Size(63, 20);
            this.maxFlightDistT.TabIndex = 43;
            this.maxFlightDistT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // minTargetSpeedT
            // 
            this.minTargetSpeedT.Location = new System.Drawing.Point(338, 154);
            this.minTargetSpeedT.Name = "minTargetSpeedT";
            this.minTargetSpeedT.Size = new System.Drawing.Size(63, 20);
            this.minTargetSpeedT.TabIndex = 42;
            this.minTargetSpeedT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(211, 155);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(126, 13);
            this.label46.TabIndex = 41;
            this.label46.Text = "Min Target Speed (km/h)";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(7, 155);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(129, 13);
            this.label49.TabIndex = 40;
            this.label49.Text = "Max Target Speed (km/h)";
            // 
            // minTargetAltT
            // 
            this.minTargetAltT.Location = new System.Drawing.Point(338, 128);
            this.minTargetAltT.Name = "minTargetAltT";
            this.minTargetAltT.Size = new System.Drawing.Size(63, 20);
            this.minTargetAltT.TabIndex = 39;
            this.minTargetAltT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(215, 131);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(113, 13);
            this.label48.TabIndex = 38;
            this.label48.Text = "Min Target Altitude (m)";
            // 
            // onGroundSpeedT
            // 
            this.onGroundSpeedT.Location = new System.Drawing.Point(338, 181);
            this.onGroundSpeedT.Name = "onGroundSpeedT";
            this.onGroundSpeedT.Size = new System.Drawing.Size(63, 20);
            this.onGroundSpeedT.TabIndex = 37;
            this.onGroundSpeedT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(215, 181);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(119, 13);
            this.label44.TabIndex = 36;
            this.label44.Text = "On Ground  Speed (km)";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(31, 184);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(84, 13);
            this.label45.TabIndex = 35;
            this.label45.Text = "Stall Speed (km)";
            // 
            // stallSpeedT
            // 
            this.stallSpeedT.Location = new System.Drawing.Point(142, 178);
            this.stallSpeedT.Name = "stallSpeedT";
            this.stallSpeedT.Size = new System.Drawing.Size(63, 20);
            this.stallSpeedT.TabIndex = 33;
            this.stallSpeedT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // maxTargetSpeedT
            // 
            this.maxTargetSpeedT.Location = new System.Drawing.Point(142, 152);
            this.maxTargetSpeedT.Name = "maxTargetSpeedT";
            this.maxTargetSpeedT.Size = new System.Drawing.Size(63, 20);
            this.maxTargetSpeedT.TabIndex = 32;
            this.maxTargetSpeedT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // maxTargrtAltT
            // 
            this.maxTargrtAltT.Location = new System.Drawing.Point(142, 128);
            this.maxTargrtAltT.Name = "maxTargrtAltT";
            this.maxTargrtAltT.Size = new System.Drawing.Size(63, 20);
            this.maxTargrtAltT.TabIndex = 31;
            this.maxTargrtAltT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(7, 131);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(116, 13);
            this.label47.TabIndex = 30;
            this.label47.Text = "Max Target Altitude (m)";
            // 
            // invertFenceEWcheckBox
            // 
            this.invertFenceEWcheckBox.AutoSize = true;
            this.invertFenceEWcheckBox.Location = new System.Drawing.Point(253, 77);
            this.invertFenceEWcheckBox.Name = "invertFenceEWcheckBox";
            this.invertFenceEWcheckBox.Size = new System.Drawing.Size(112, 17);
            this.invertFenceEWcheckBox.TabIndex = 29;
            this.invertFenceEWcheckBox.Text = "Invert E/W Fence";
            this.invertFenceEWcheckBox.UseVisualStyleBackColor = true;
            this.invertFenceEWcheckBox.CheckedChanged += new System.EventHandler(this.invertFenceEWcheckBox_CheckedChanged);
            // 
            // invertFenceNScheckBox
            // 
            this.invertFenceNScheckBox.AutoSize = true;
            this.invertFenceNScheckBox.Location = new System.Drawing.Point(253, 35);
            this.invertFenceNScheckBox.Name = "invertFenceNScheckBox";
            this.invertFenceNScheckBox.Size = new System.Drawing.Size(109, 17);
            this.invertFenceNScheckBox.TabIndex = 27;
            this.invertFenceNScheckBox.Text = "Invert N/S Fence";
            this.invertFenceNScheckBox.UseVisualStyleBackColor = true;
            this.invertFenceNScheckBox.CheckedChanged += new System.EventHandler(this.invertFenceNScheckBox_CheckedChanged);
            // 
            // southFenceT
            // 
            this.southFenceT.Location = new System.Drawing.Point(141, 97);
            this.southFenceT.Name = "southFenceT";
            this.southFenceT.Size = new System.Drawing.Size(63, 20);
            this.southFenceT.TabIndex = 26;
            this.southFenceT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(55, 100);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(68, 13);
            this.label43.TabIndex = 25;
            this.label43.Text = "South Fence";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(55, 77);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(65, 13);
            this.label41.TabIndex = 24;
            this.label41.Text = "West Fence";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(55, 51);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(61, 13);
            this.label40.TabIndex = 23;
            this.label40.Text = "East Fence";
            // 
            // westFenceT
            // 
            this.westFenceT.Location = new System.Drawing.Point(141, 74);
            this.westFenceT.Name = "westFenceT";
            this.westFenceT.Size = new System.Drawing.Size(63, 20);
            this.westFenceT.TabIndex = 22;
            this.westFenceT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // eastFenceT
            // 
            this.eastFenceT.Location = new System.Drawing.Point(141, 48);
            this.eastFenceT.Name = "eastFenceT";
            this.eastFenceT.Size = new System.Drawing.Size(63, 20);
            this.eastFenceT.TabIndex = 21;
            this.eastFenceT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // northFenceT
            // 
            this.northFenceT.Location = new System.Drawing.Point(141, 24);
            this.northFenceT.Name = "northFenceT";
            this.northFenceT.Size = new System.Drawing.Size(63, 20);
            this.northFenceT.TabIndex = 20;
            this.northFenceT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(55, 27);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(66, 13);
            this.label42.TabIndex = 17;
            this.label42.Text = "North Fence";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.fatalErrorSlider);
            this.groupBox8.Controls.Add(this.slewLimitSlider);
            this.groupBox8.Controls.Add(this.onGroundSlider);
            this.groupBox8.Controls.Add(this.takeoffSlider);
            this.groupBox8.Controls.Add(this.descentSlider);
            this.groupBox8.Controls.Add(this.climbSlider);
            this.groupBox8.Controls.Add(this.approachSlider);
            this.groupBox8.Controls.Add(this.cruiseSlider);
            this.groupBox8.Controls.Add(this.onGroundL);
            this.groupBox8.Controls.Add(this.slweLimitL);
            this.groupBox8.Controls.Add(this.fatalErrorMaxL);
            this.groupBox8.Controls.Add(this.approachL);
            this.groupBox8.Controls.Add(this.climbL);
            this.groupBox8.Controls.Add(this.descentL);
            this.groupBox8.Controls.Add(this.takeoffL);
            this.groupBox8.Controls.Add(this.cruiseL);
            this.groupBox8.Controls.Add(this.label95);
            this.groupBox8.Controls.Add(this.label39);
            this.groupBox8.Controls.Add(this.label38);
            this.groupBox8.Controls.Add(this.label37);
            this.groupBox8.Controls.Add(this.label36);
            this.groupBox8.Controls.Add(this.label35);
            this.groupBox8.Controls.Add(this.label34);
            this.groupBox8.Controls.Add(this.label33);
            this.groupBox8.Controls.Add(this.label32);
            this.groupBox8.Location = new System.Drawing.Point(12, 247);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(339, 253);
            this.groupBox8.TabIndex = 3;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Throttle Setting";
            // 
            // fatalErrorSlider
            // 
            this.fatalErrorSlider.Location = new System.Drawing.Point(130, 220);
            this.fatalErrorSlider.Maximum = 109;
            this.fatalErrorSlider.Name = "fatalErrorSlider";
            this.fatalErrorSlider.Size = new System.Drawing.Size(110, 18);
            this.fatalErrorSlider.TabIndex = 95;
            this.fatalErrorSlider.Scroll += new System.Windows.Forms.ScrollEventHandler(this.fatalErrorSlider_Scroll);
            // 
            // slewLimitSlider
            // 
            this.slewLimitSlider.Location = new System.Drawing.Point(130, 188);
            this.slewLimitSlider.Maximum = 109;
            this.slewLimitSlider.Name = "slewLimitSlider";
            this.slewLimitSlider.Size = new System.Drawing.Size(110, 18);
            this.slewLimitSlider.TabIndex = 94;
            this.slewLimitSlider.Scroll += new System.Windows.Forms.ScrollEventHandler(this.slewLimitSlider_Scroll);
            // 
            // onGroundSlider
            // 
            this.onGroundSlider.Location = new System.Drawing.Point(130, 155);
            this.onGroundSlider.Maximum = 109;
            this.onGroundSlider.Name = "onGroundSlider";
            this.onGroundSlider.Size = new System.Drawing.Size(110, 18);
            this.onGroundSlider.TabIndex = 93;
            this.onGroundSlider.Scroll += new System.Windows.Forms.ScrollEventHandler(this.onGroundSlider_Scroll);
            // 
            // takeoffSlider
            // 
            this.takeoffSlider.Location = new System.Drawing.Point(130, 123);
            this.takeoffSlider.Maximum = 109;
            this.takeoffSlider.Name = "takeoffSlider";
            this.takeoffSlider.Size = new System.Drawing.Size(110, 18);
            this.takeoffSlider.TabIndex = 92;
            this.takeoffSlider.Scroll += new System.Windows.Forms.ScrollEventHandler(this.takeoffSlider_Scroll);
            // 
            // descentSlider
            // 
            this.descentSlider.Location = new System.Drawing.Point(130, 97);
            this.descentSlider.Maximum = 109;
            this.descentSlider.Name = "descentSlider";
            this.descentSlider.Size = new System.Drawing.Size(110, 18);
            this.descentSlider.TabIndex = 91;
            this.descentSlider.Scroll += new System.Windows.Forms.ScrollEventHandler(this.descentSlider_Scroll);
            // 
            // climbSlider
            // 
            this.climbSlider.Location = new System.Drawing.Point(130, 72);
            this.climbSlider.Maximum = 109;
            this.climbSlider.Name = "climbSlider";
            this.climbSlider.Size = new System.Drawing.Size(110, 18);
            this.climbSlider.TabIndex = 90;
            this.climbSlider.Scroll += new System.Windows.Forms.ScrollEventHandler(this.climbSlider_Scroll);
            // 
            // approachSlider
            // 
            this.approachSlider.Location = new System.Drawing.Point(130, 48);
            this.approachSlider.Maximum = 109;
            this.approachSlider.Name = "approachSlider";
            this.approachSlider.Size = new System.Drawing.Size(110, 18);
            this.approachSlider.TabIndex = 89;
            this.approachSlider.Scroll += new System.Windows.Forms.ScrollEventHandler(this.approachSlider_Scroll);
            // 
            // cruiseSlider
            // 
            this.cruiseSlider.Location = new System.Drawing.Point(130, 22);
            this.cruiseSlider.Maximum = 109;
            this.cruiseSlider.Name = "cruiseSlider";
            this.cruiseSlider.Size = new System.Drawing.Size(110, 18);
            this.cruiseSlider.TabIndex = 88;
            this.cruiseSlider.Scroll += new System.Windows.Forms.ScrollEventHandler(this.cruiseSlider_Scroll);
            // 
            // onGroundL
            // 
            this.onGroundL.AutoSize = true;
            this.onGroundL.Location = new System.Drawing.Point(289, 156);
            this.onGroundL.Name = "onGroundL";
            this.onGroundL.Size = new System.Drawing.Size(21, 13);
            this.onGroundL.TabIndex = 54;
            this.onGroundL.Text = "0%";
            // 
            // slweLimitL
            // 
            this.slweLimitL.AutoSize = true;
            this.slweLimitL.Location = new System.Drawing.Point(289, 188);
            this.slweLimitL.Name = "slweLimitL";
            this.slweLimitL.Size = new System.Drawing.Size(21, 13);
            this.slweLimitL.TabIndex = 53;
            this.slweLimitL.Text = "0%";
            // 
            // fatalErrorMaxL
            // 
            this.fatalErrorMaxL.AutoSize = true;
            this.fatalErrorMaxL.Location = new System.Drawing.Point(289, 220);
            this.fatalErrorMaxL.Name = "fatalErrorMaxL";
            this.fatalErrorMaxL.Size = new System.Drawing.Size(21, 13);
            this.fatalErrorMaxL.TabIndex = 52;
            this.fatalErrorMaxL.Text = "0%";
            // 
            // approachL
            // 
            this.approachL.AutoSize = true;
            this.approachL.Location = new System.Drawing.Point(289, 48);
            this.approachL.Name = "approachL";
            this.approachL.Size = new System.Drawing.Size(21, 13);
            this.approachL.TabIndex = 51;
            this.approachL.Text = "0%";
            // 
            // climbL
            // 
            this.climbL.AutoSize = true;
            this.climbL.Location = new System.Drawing.Point(289, 74);
            this.climbL.Name = "climbL";
            this.climbL.Size = new System.Drawing.Size(21, 13);
            this.climbL.TabIndex = 50;
            this.climbL.Text = "0%";
            // 
            // descentL
            // 
            this.descentL.AutoSize = true;
            this.descentL.Location = new System.Drawing.Point(289, 100);
            this.descentL.Name = "descentL";
            this.descentL.Size = new System.Drawing.Size(21, 13);
            this.descentL.TabIndex = 49;
            this.descentL.Text = "0%";
            // 
            // takeoffL
            // 
            this.takeoffL.AutoSize = true;
            this.takeoffL.Location = new System.Drawing.Point(289, 128);
            this.takeoffL.Name = "takeoffL";
            this.takeoffL.Size = new System.Drawing.Size(21, 13);
            this.takeoffL.TabIndex = 48;
            this.takeoffL.Text = "0%";
            // 
            // cruiseL
            // 
            this.cruiseL.AutoSize = true;
            this.cruiseL.Location = new System.Drawing.Point(289, 27);
            this.cruiseL.Name = "cruiseL";
            this.cruiseL.Size = new System.Drawing.Size(21, 13);
            this.cruiseL.TabIndex = 47;
            this.cruiseL.Text = "0%";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(289, 24);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(0, 13);
            this.label95.TabIndex = 46;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 217);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(78, 13);
            this.label39.TabIndex = 37;
            this.label39.Text = "Fatal Erorr Max";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(19, 188);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(54, 13);
            this.label38.TabIndex = 36;
            this.label38.Text = "Slew Limit";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(19, 161);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(59, 13);
            this.label37.TabIndex = 35;
            this.label37.Text = "On Ground";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(19, 129);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(44, 13);
            this.label36.TabIndex = 34;
            this.label36.Text = "Takeoff";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(19, 100);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(47, 13);
            this.label35.TabIndex = 33;
            this.label35.Text = "Descent";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(19, 74);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(32, 13);
            this.label34.TabIndex = 32;
            this.label34.Text = "Climb";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(19, 48);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 13);
            this.label33.TabIndex = 31;
            this.label33.Text = "Approach";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(19, 25);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(36, 13);
            this.label32.TabIndex = 30;
            this.label32.Text = "Cruise";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label31);
            this.groupBox7.Controls.Add(this.maxLunchPlatformSpeedT);
            this.groupBox7.Controls.Add(this.label30);
            this.groupBox7.Controls.Add(this.flyTosFromTosLineUp);
            this.groupBox7.Controls.Add(this.fromTosToFlyTos);
            this.groupBox7.Controls.Add(this.button1);
            this.groupBox7.Controls.Add(this.alwaysUseLineClimb);
            this.groupBox7.Controls.Add(this.offGroundAlgoT);
            this.groupBox7.Controls.Add(this.label29);
            this.groupBox7.Controls.Add(this.lockedPichtT);
            this.groupBox7.Controls.Add(this.label28);
            this.groupBox7.Controls.Add(this.takeoffPichT);
            this.groupBox7.Controls.Add(this.label27);
            this.groupBox7.Location = new System.Drawing.Point(386, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(343, 238);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Miscellaneous";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(99, 218);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(63, 13);
            this.label31.TabIndex = 29;
            this.label31.Text = "Speed (ft/s)";
            // 
            // maxLunchPlatformSpeedT
            // 
            this.maxLunchPlatformSpeedT.Location = new System.Drawing.Point(181, 211);
            this.maxLunchPlatformSpeedT.Name = "maxLunchPlatformSpeedT";
            this.maxLunchPlatformSpeedT.Size = new System.Drawing.Size(63, 20);
            this.maxLunchPlatformSpeedT.TabIndex = 28;
            this.maxLunchPlatformSpeedT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(64, 202);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(116, 13);
            this.label30.TabIndex = 27;
            this.label30.Text = "Max Launch Platform   ";
            // 
            // flyTosFromTosLineUp
            // 
            this.flyTosFromTosLineUp.AutoSize = true;
            this.flyTosFromTosLineUp.Location = new System.Drawing.Point(102, 127);
            this.flyTosFromTosLineUp.Name = "flyTosFromTosLineUp";
            this.flyTosFromTosLineUp.Size = new System.Drawing.Size(142, 17);
            this.flyTosFromTosLineUp.TabIndex = 21;
            this.flyTosFromTosLineUp.Text = "Fly-tos From-tos[Lline up]";
            this.flyTosFromTosLineUp.UseVisualStyleBackColor = true;
            this.flyTosFromTosLineUp.CheckedChanged += new System.EventHandler(this.flyTosFromTosLineUp_CheckedChanged);
            // 
            // fromTosToFlyTos
            // 
            this.fromTosToFlyTos.AutoSize = true;
            this.fromTosToFlyTos.Location = new System.Drawing.Point(102, 150);
            this.fromTosToFlyTos.Name = "fromTosToFlyTos";
            this.fromTosToFlyTos.Size = new System.Drawing.Size(111, 17);
            this.fromTosToFlyTos.TabIndex = 22;
            this.fromTosToFlyTos.Text = "From-tos to Fly-tos";
            this.fromTosToFlyTos.UseVisualStyleBackColor = true;
            this.fromTosToFlyTos.CheckedChanged += new System.EventHandler(this.fromTosToFlyTos_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(-340, -104);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 37);
            this.button1.TabIndex = 3;
            this.button1.Text = "Restore Defaults";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(-340, -104);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 37);
            this.button1.TabIndex = 3;
            this.button1.Text = "Restore Defaults";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // alwaysUseLineClimb
            // 
            this.alwaysUseLineClimb.AutoSize = true;
            this.alwaysUseLineClimb.Location = new System.Drawing.Point(102, 173);
            this.alwaysUseLineClimb.Name = "alwaysUseLineClimb";
            this.alwaysUseLineClimb.Size = new System.Drawing.Size(138, 17);
            this.alwaysUseLineClimb.TabIndex = 23;
            this.alwaysUseLineClimb.Text = "Alwayes Use Line Climb";
            this.alwaysUseLineClimb.UseVisualStyleBackColor = true;
            this.alwaysUseLineClimb.CheckedChanged += new System.EventHandler(this.alwaysUseLineClimb_CheckedChanged);
            // 
            // offGroundAlgoT
            // 
            this.offGroundAlgoT.Location = new System.Drawing.Point(181, 98);
            this.offGroundAlgoT.Name = "offGroundAlgoT";
            this.offGroundAlgoT.Size = new System.Drawing.Size(63, 20);
            this.offGroundAlgoT.TabIndex = 26;
            this.offGroundAlgoT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(64, 27);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(95, 13);
            this.label29.TabIndex = 21;
            this.label29.Text = "Takeoff Pich (deg)";
            // 
            // lockedPichtT
            // 
            this.lockedPichtT.Location = new System.Drawing.Point(181, 60);
            this.lockedPichtT.Name = "lockedPichtT";
            this.lockedPichtT.Size = new System.Drawing.Size(63, 20);
            this.lockedPichtT.TabIndex = 25;
            this.lockedPichtT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(64, 63);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(94, 13);
            this.label28.TabIndex = 22;
            this.label28.Text = "Locked Pich (deg)";
            // 
            // takeoffPichT
            // 
            this.takeoffPichT.Location = new System.Drawing.Point(181, 24);
            this.takeoffPichT.Name = "takeoffPichT";
            this.takeoffPichT.Size = new System.Drawing.Size(63, 20);
            this.takeoffPichT.TabIndex = 24;
            this.takeoffPichT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(64, 101);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(105, 13);
            this.label27.TabIndex = 23;
            this.label27.Text = "Off Ground Algorithm";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.enableDeadcheckBox);
            this.groupBox6.Controls.Add(this.levelFlightModecomboBox);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.wpReduceTurnT);
            this.groupBox6.Controls.Add(this.wpDiaT);
            this.groupBox6.Controls.Add(this.cimbMarginT);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Location = new System.Drawing.Point(6, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(345, 238);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Cruise/Navigation";
            this.groupBox6.Enter += new System.EventHandler(this.groupBox6_Enter);
            // 
            // enableDeadcheckBox
            // 
            this.enableDeadcheckBox.AutoSize = true;
            this.enableDeadcheckBox.Location = new System.Drawing.Point(68, 176);
            this.enableDeadcheckBox.Name = "enableDeadcheckBox";
            this.enableDeadcheckBox.Size = new System.Drawing.Size(146, 17);
            this.enableDeadcheckBox.TabIndex = 20;
            this.enableDeadcheckBox.Text = "Enable Dead Reckorning";
            this.enableDeadcheckBox.UseVisualStyleBackColor = true;
            this.enableDeadcheckBox.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // levelFlightModecomboBox
            // 
            this.levelFlightModecomboBox.FormattingEnabled = true;
            this.levelFlightModecomboBox.Items.AddRange(new object[] {
            "Elevator Controls Airspeed",
            "Elevator Controls Altitude",
            "Elevator Controls Alt. Th. Fixed"});
            this.levelFlightModecomboBox.Location = new System.Drawing.Point(136, 135);
            this.levelFlightModecomboBox.Name = "levelFlightModecomboBox";
            this.levelFlightModecomboBox.Size = new System.Drawing.Size(183, 21);
            this.levelFlightModecomboBox.TabIndex = 18;
            this.levelFlightModecomboBox.SelectedIndexChanged += new System.EventHandler(this.levelFlightModecomboBox_SelectedIndexChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(24, 138);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(91, 13);
            this.label26.TabIndex = 17;
            this.label26.Text = "Level Flight Mode";
            // 
            // wpReduceTurnT
            // 
            this.wpReduceTurnT.Location = new System.Drawing.Point(136, 97);
            this.wpReduceTurnT.Name = "wpReduceTurnT";
            this.wpReduceTurnT.Size = new System.Drawing.Size(63, 20);
            this.wpReduceTurnT.TabIndex = 16;
            this.wpReduceTurnT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // wpDiaT
            // 
            this.wpDiaT.Location = new System.Drawing.Point(136, 59);
            this.wpDiaT.Name = "wpDiaT";
            this.wpDiaT.Size = new System.Drawing.Size(63, 20);
            this.wpDiaT.TabIndex = 15;
            this.wpDiaT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // cimbMarginT
            // 
            this.cimbMarginT.Location = new System.Drawing.Point(136, 23);
            this.cimbMarginT.Name = "cimbMarginT";
            this.cimbMarginT.Size = new System.Drawing.Size(63, 20);
            this.cimbMarginT.TabIndex = 14;
            this.cimbMarginT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(19, 100);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(96, 13);
            this.label23.TabIndex = 13;
            this.label23.Text = "Wpt. Reduce Turn";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(19, 62);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(92, 13);
            this.label24.TabIndex = 12;
            this.label24.Text = "WayPoint Dia. (m)";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(19, 26);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(84, 13);
            this.label25.TabIndex = 11;
            this.label25.Text = "Climb Margin (m)";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.descentTypeT);
            this.groupBox5.Controls.Add(this.label58);
            this.groupBox5.Controls.Add(this.allowNnavigationAirSpeedcheckBox);
            this.groupBox5.Controls.Add(this.descentRateT);
            this.groupBox5.Controls.Add(this.label50);
            this.groupBox5.Controls.Add(this.label53);
            this.groupBox5.Controls.Add(this.climbSpeedT);
            this.groupBox5.Controls.Add(this.label54);
            this.groupBox5.Controls.Add(this.descentSpeedT);
            this.groupBox5.Controls.Add(this.label55);
            this.groupBox5.Controls.Add(this.label56);
            this.groupBox5.Controls.Add(this.maxSpeedIncreasT);
            this.groupBox5.Controls.Add(this.cruiseSpeedT);
            this.groupBox5.Controls.Add(this.rotationSpeedT);
            this.groupBox5.Controls.Add(this.label57);
            this.groupBox5.Location = new System.Drawing.Point(97, 506);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(597, 135);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Refrence Speed";
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // descentTypeT
            // 
            this.descentTypeT.Location = new System.Drawing.Point(424, 106);
            this.descentTypeT.Name = "descentTypeT";
            this.descentTypeT.Size = new System.Drawing.Size(63, 20);
            this.descentTypeT.TabIndex = 58;
            this.descentTypeT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(306, 109);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(74, 13);
            this.label58.TabIndex = 57;
            this.label58.Text = "Descent Type";
            // 
            // allowNnavigationAirSpeedcheckBox
            // 
            this.allowNnavigationAirSpeedcheckBox.AutoSize = true;
            this.allowNnavigationAirSpeedcheckBox.Location = new System.Drawing.Point(124, 109);
            this.allowNnavigationAirSpeedcheckBox.Name = "allowNnavigationAirSpeedcheckBox";
            this.allowNnavigationAirSpeedcheckBox.Size = new System.Drawing.Size(151, 17);
            this.allowNnavigationAirSpeedcheckBox.TabIndex = 56;
            this.allowNnavigationAirSpeedcheckBox.Text = "Allow Navigation AirSpeed";
            this.allowNnavigationAirSpeedcheckBox.UseVisualStyleBackColor = true;
            this.allowNnavigationAirSpeedcheckBox.CheckedChanged += new System.EventHandler(this.allowNnavigationAirSpeedcheckBox_CheckedChanged);
            // 
            // descentRateT
            // 
            this.descentRateT.Location = new System.Drawing.Point(424, 50);
            this.descentRateT.Name = "descentRateT";
            this.descentRateT.Size = new System.Drawing.Size(63, 20);
            this.descentRateT.TabIndex = 55;
            this.descentRateT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(301, 53);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(111, 13);
            this.label50.TabIndex = 54;
            this.label50.Text = "Descent Rate (m/min)";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(93, 51);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(104, 13);
            this.label53.TabIndex = 53;
            this.label53.Text = "Cruise Speed (km/h)";
            // 
            // climbSpeedT
            // 
            this.climbSpeedT.Location = new System.Drawing.Point(424, 24);
            this.climbSpeedT.Name = "climbSpeedT";
            this.climbSpeedT.Size = new System.Drawing.Size(63, 20);
            this.climbSpeedT.TabIndex = 52;
            this.climbSpeedT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(301, 27);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(100, 13);
            this.label54.TabIndex = 51;
            this.label54.Text = "Climb Speed (km/h)";
            // 
            // descentSpeedT
            // 
            this.descentSpeedT.Location = new System.Drawing.Point(424, 77);
            this.descentSpeedT.Name = "descentSpeedT";
            this.descentSpeedT.Size = new System.Drawing.Size(63, 20);
            this.descentSpeedT.TabIndex = 50;
            this.descentSpeedT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(301, 77);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(115, 13);
            this.label55.TabIndex = 49;
            this.label55.Text = "Descent Speed (km/h)";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(74, 80);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(144, 13);
            this.label56.TabIndex = 48;
            this.label56.Text = "Max Speed Increases (km/h)";
            // 
            // maxSpeedIncreasT
            // 
            this.maxSpeedIncreasT.Location = new System.Drawing.Point(228, 74);
            this.maxSpeedIncreasT.Name = "maxSpeedIncreasT";
            this.maxSpeedIncreasT.Size = new System.Drawing.Size(63, 20);
            this.maxSpeedIncreasT.TabIndex = 47;
            this.maxSpeedIncreasT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // cruiseSpeedT
            // 
            this.cruiseSpeedT.Location = new System.Drawing.Point(228, 48);
            this.cruiseSpeedT.Name = "cruiseSpeedT";
            this.cruiseSpeedT.Size = new System.Drawing.Size(63, 20);
            this.cruiseSpeedT.TabIndex = 46;
            this.cruiseSpeedT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // rotationSpeedT
            // 
            this.rotationSpeedT.Location = new System.Drawing.Point(228, 24);
            this.rotationSpeedT.Name = "rotationSpeedT";
            this.rotationSpeedT.Size = new System.Drawing.Size(63, 20);
            this.rotationSpeedT.TabIndex = 45;
            this.rotationSpeedT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(93, 27);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(118, 13);
            this.label57.TabIndex = 44;
            this.label57.Text = "Rotation  Speed (km/h)";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(925, 703);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "PID";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click_1);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.CompatiblityCheckBox);
            this.groupBox4.Controls.Add(this.CalculateAlironCheckBox);
            this.groupBox4.Controls.Add(this.SelectedPIDLloopCombo);
            this.groupBox4.Controls.Add(this.label96);
            this.groupBox4.Controls.Add(this.feedForwardInputMinT);
            this.groupBox4.Controls.Add(this.feedForwardInputMaxT);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.feedForward3T);
            this.groupBox4.Controls.Add(this.feedForward2T);
            this.groupBox4.Controls.Add(this.feedForward1T);
            this.groupBox4.Controls.Add(this.feedForward0T);
            this.groupBox4.Controls.Add(this.DifferentialInputMinT);
            this.groupBox4.Controls.Add(this.DifferentialInputMaxT);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.Differential3T);
            this.groupBox4.Controls.Add(this.Differential2T);
            this.groupBox4.Controls.Add(this.Differential1T);
            this.groupBox4.Controls.Add(this.Differential0T);
            this.groupBox4.Controls.Add(this.ResAntiWindupMinT);
            this.groupBox4.Controls.Add(this.ResAntiWindupMaxT);
            this.groupBox4.Controls.Add(this.inteqralInputMinT);
            this.groupBox4.Controls.Add(this.inteqralInputMaxT);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.inteqral3T);
            this.groupBox4.Controls.Add(this.inteqral2T);
            this.groupBox4.Controls.Add(this.inteqral1T);
            this.groupBox4.Controls.Add(this.inteqral0T);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.inputMin);
            this.groupBox4.Controls.Add(this.inputMax);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.Propotional3T);
            this.groupBox4.Controls.Add(this.Propotional2T);
            this.groupBox4.Controls.Add(this.Propotional1T);
            this.groupBox4.Controls.Add(this.Propotional0T);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.Range2T);
            this.groupBox4.Controls.Add(this.Range1T);
            this.groupBox4.Controls.Add(this.Range3T);
            this.groupBox4.Controls.Add(this.Range0T);
            this.groupBox4.Controls.Add(this.Sch3T);
            this.groupBox4.Controls.Add(this.Sch2T);
            this.groupBox4.Controls.Add(this.Sch1T);
            this.groupBox4.Controls.Add(this.Sch0T);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Location = new System.Drawing.Point(71, 233);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(714, 400);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "PID Feedback Loops";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // CompatiblityCheckBox
            // 
            this.CompatiblityCheckBox.AutoSize = true;
            this.CompatiblityCheckBox.Location = new System.Drawing.Point(573, 377);
            this.CompatiblityCheckBox.Name = "CompatiblityCheckBox";
            this.CompatiblityCheckBox.Size = new System.Drawing.Size(112, 17);
            this.CompatiblityCheckBox.TabIndex = 58;
            this.CompatiblityCheckBox.Text = "Compatiblity Mode";
            this.CompatiblityCheckBox.UseVisualStyleBackColor = true;
            this.CompatiblityCheckBox.CheckedChanged += new System.EventHandler(this.CompatiblityCheckBox_CheckedChanged);
            // 
            // CalculateAlironCheckBox
            // 
            this.CalculateAlironCheckBox.AutoSize = true;
            this.CalculateAlironCheckBox.Location = new System.Drawing.Point(29, 377);
            this.CalculateAlironCheckBox.Name = "CalculateAlironCheckBox";
            this.CalculateAlironCheckBox.Size = new System.Drawing.Size(312, 17);
            this.CalculateAlironCheckBox.TabIndex = 57;
            this.CalculateAlironCheckBox.Text = "Calculate Aliron From Roll Rate and Roll Rate From Roll Error";
            this.CalculateAlironCheckBox.UseVisualStyleBackColor = true;
            this.CalculateAlironCheckBox.CheckedChanged += new System.EventHandler(this.CalculateAlironCheckBox_CheckedChanged);
            // 
            // SelectedPIDLloopCombo
            // 
            this.SelectedPIDLloopCombo.FormattingEnabled = true;
            this.SelectedPIDLloopCombo.Items.AddRange(new object[] {
            "Aliron From Desired Roll ",
            "Elevator From Desired Pitch",
            "Rudder From Y Acceleremoter ",
            "Rudder from heading",
            "Throttle from speed ",
            "Throttle from Z velocity",
            "Pitch from altitude ",
            "Pitch from AGL Altitude",
            "Pitch from airspeed ",
            "Roll from heading ",
            "Heading from Cross Track error ",
            "Pitch from Descent Rate",
            "Roll from Radius ",
            "Roll Rate From Error (DISABLED)",
            "Pitch Rate From Error (DISABLED)",
            "Yaw Rate From Error (DISABLED)"});
            this.SelectedPIDLloopCombo.Location = new System.Drawing.Point(160, 24);
            this.SelectedPIDLloopCombo.Name = "SelectedPIDLloopCombo";
            this.SelectedPIDLloopCombo.Size = new System.Drawing.Size(527, 21);
            this.SelectedPIDLloopCombo.TabIndex = 56;
            this.SelectedPIDLloopCombo.SelectedIndexChanged += new System.EventHandler(this.SelectedPIDLloopCombo_SelectedIndexChanged);
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(56, 27);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(97, 13);
            this.label96.TabIndex = 55;
            this.label96.Text = "Selected PID Loop";
            // 
            // feedForwardInputMinT
            // 
            this.feedForwardInputMinT.Location = new System.Drawing.Point(609, 267);
            this.feedForwardInputMinT.Name = "feedForwardInputMinT";
            this.feedForwardInputMinT.Size = new System.Drawing.Size(63, 20);
            this.feedForwardInputMinT.TabIndex = 54;
            this.feedForwardInputMinT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // feedForwardInputMaxT
            // 
            this.feedForwardInputMaxT.Location = new System.Drawing.Point(609, 230);
            this.feedForwardInputMaxT.Name = "feedForwardInputMaxT";
            this.feedForwardInputMaxT.Size = new System.Drawing.Size(63, 20);
            this.feedForwardInputMaxT.TabIndex = 53;
            this.feedForwardInputMaxT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(613, 54);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 13);
            this.label22.TabIndex = 52;
            this.label22.Text = "Feed Forward";
            // 
            // feedForward3T
            // 
            this.feedForward3T.Location = new System.Drawing.Point(609, 195);
            this.feedForward3T.Name = "feedForward3T";
            this.feedForward3T.Size = new System.Drawing.Size(63, 20);
            this.feedForward3T.TabIndex = 51;
            this.feedForward3T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // feedForward2T
            // 
            this.feedForward2T.Location = new System.Drawing.Point(609, 158);
            this.feedForward2T.Name = "feedForward2T";
            this.feedForward2T.Size = new System.Drawing.Size(63, 20);
            this.feedForward2T.TabIndex = 50;
            this.feedForward2T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // feedForward1T
            // 
            this.feedForward1T.Location = new System.Drawing.Point(609, 116);
            this.feedForward1T.Name = "feedForward1T";
            this.feedForward1T.Size = new System.Drawing.Size(63, 20);
            this.feedForward1T.TabIndex = 49;
            this.feedForward1T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // feedForward0T
            // 
            this.feedForward0T.Location = new System.Drawing.Point(609, 80);
            this.feedForward0T.Name = "feedForward0T";
            this.feedForward0T.Size = new System.Drawing.Size(63, 20);
            this.feedForward0T.TabIndex = 48;
            this.feedForward0T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // DifferentialInputMinT
            // 
            this.DifferentialInputMinT.Location = new System.Drawing.Point(493, 267);
            this.DifferentialInputMinT.Name = "DifferentialInputMinT";
            this.DifferentialInputMinT.Size = new System.Drawing.Size(63, 20);
            this.DifferentialInputMinT.TabIndex = 47;
            this.DifferentialInputMinT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // DifferentialInputMaxT
            // 
            this.DifferentialInputMaxT.Location = new System.Drawing.Point(493, 230);
            this.DifferentialInputMaxT.Name = "DifferentialInputMaxT";
            this.DifferentialInputMaxT.Size = new System.Drawing.Size(63, 20);
            this.DifferentialInputMaxT.TabIndex = 46;
            this.DifferentialInputMaxT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(497, 54);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(57, 13);
            this.label21.TabIndex = 45;
            this.label21.Text = "Differential";
            // 
            // Differential3T
            // 
            this.Differential3T.Location = new System.Drawing.Point(493, 195);
            this.Differential3T.Name = "Differential3T";
            this.Differential3T.Size = new System.Drawing.Size(63, 20);
            this.Differential3T.TabIndex = 44;
            this.Differential3T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // Differential2T
            // 
            this.Differential2T.Location = new System.Drawing.Point(493, 158);
            this.Differential2T.Name = "Differential2T";
            this.Differential2T.Size = new System.Drawing.Size(63, 20);
            this.Differential2T.TabIndex = 43;
            this.Differential2T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // Differential1T
            // 
            this.Differential1T.Location = new System.Drawing.Point(493, 116);
            this.Differential1T.Name = "Differential1T";
            this.Differential1T.Size = new System.Drawing.Size(63, 20);
            this.Differential1T.TabIndex = 42;
            this.Differential1T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // Differential0T
            // 
            this.Differential0T.Location = new System.Drawing.Point(493, 80);
            this.Differential0T.Name = "Differential0T";
            this.Differential0T.Size = new System.Drawing.Size(63, 20);
            this.Differential0T.TabIndex = 41;
            this.Differential0T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // ResAntiWindupMinT
            // 
            this.ResAntiWindupMinT.Location = new System.Drawing.Point(358, 337);
            this.ResAntiWindupMinT.Name = "ResAntiWindupMinT";
            this.ResAntiWindupMinT.Size = new System.Drawing.Size(63, 20);
            this.ResAntiWindupMinT.TabIndex = 40;
            this.ResAntiWindupMinT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // ResAntiWindupMaxT
            // 
            this.ResAntiWindupMaxT.Location = new System.Drawing.Point(358, 300);
            this.ResAntiWindupMaxT.Name = "ResAntiWindupMaxT";
            this.ResAntiWindupMaxT.Size = new System.Drawing.Size(63, 20);
            this.ResAntiWindupMaxT.TabIndex = 39;
            this.ResAntiWindupMaxT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // inteqralInputMinT
            // 
            this.inteqralInputMinT.Location = new System.Drawing.Point(358, 267);
            this.inteqralInputMinT.Name = "inteqralInputMinT";
            this.inteqralInputMinT.Size = new System.Drawing.Size(63, 20);
            this.inteqralInputMinT.TabIndex = 38;
            this.inteqralInputMinT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // inteqralInputMaxT
            // 
            this.inteqralInputMaxT.Location = new System.Drawing.Point(358, 230);
            this.inteqralInputMaxT.Name = "inteqralInputMaxT";
            this.inteqralInputMaxT.Size = new System.Drawing.Size(63, 20);
            this.inteqralInputMaxT.TabIndex = 37;
            this.inteqralInputMaxT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(362, 54);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 13);
            this.label20.TabIndex = 36;
            this.label20.Text = "Inteqral (trim)";
            // 
            // inteqral3T
            // 
            this.inteqral3T.Location = new System.Drawing.Point(358, 195);
            this.inteqral3T.Name = "inteqral3T";
            this.inteqral3T.Size = new System.Drawing.Size(63, 20);
            this.inteqral3T.TabIndex = 35;
            this.inteqral3T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // inteqral2T
            // 
            this.inteqral2T.Location = new System.Drawing.Point(358, 158);
            this.inteqral2T.Name = "inteqral2T";
            this.inteqral2T.Size = new System.Drawing.Size(63, 20);
            this.inteqral2T.TabIndex = 34;
            this.inteqral2T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // inteqral1T
            // 
            this.inteqral1T.Location = new System.Drawing.Point(358, 116);
            this.inteqral1T.Name = "inteqral1T";
            this.inteqral1T.Size = new System.Drawing.Size(63, 20);
            this.inteqral1T.TabIndex = 33;
            this.inteqral1T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // inteqral0T
            // 
            this.inteqral0T.Location = new System.Drawing.Point(358, 80);
            this.inteqral0T.Name = "inteqral0T";
            this.inteqral0T.Size = new System.Drawing.Size(63, 20);
            this.inteqral0T.TabIndex = 32;
            this.inteqral0T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(221, 340);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(118, 13);
            this.label19.TabIndex = 31;
            this.label19.Text = "Result Anti-Windup Min";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(221, 303);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(121, 13);
            this.label18.TabIndex = 30;
            this.label18.Text = "Result Anti-Windup Max";
            // 
            // inputMin
            // 
            this.inputMin.Location = new System.Drawing.Point(247, 267);
            this.inputMin.Name = "inputMin";
            this.inputMin.Size = new System.Drawing.Size(63, 20);
            this.inputMin.TabIndex = 29;
            this.inputMin.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // inputMax
            // 
            this.inputMax.Location = new System.Drawing.Point(247, 230);
            this.inputMax.Name = "inputMax";
            this.inputMax.Size = new System.Drawing.Size(63, 20);
            this.inputMax.TabIndex = 28;
            this.inputMax.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(187, 270);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 13);
            this.label17.TabIndex = 27;
            this.label17.Text = "Input Min";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(183, 233);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "Input Max";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(251, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "Propotional";
            // 
            // Propotional3T
            // 
            this.Propotional3T.Location = new System.Drawing.Point(247, 195);
            this.Propotional3T.Name = "Propotional3T";
            this.Propotional3T.Size = new System.Drawing.Size(63, 20);
            this.Propotional3T.TabIndex = 24;
            this.Propotional3T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // Propotional2T
            // 
            this.Propotional2T.Location = new System.Drawing.Point(247, 158);
            this.Propotional2T.Name = "Propotional2T";
            this.Propotional2T.Size = new System.Drawing.Size(63, 20);
            this.Propotional2T.TabIndex = 23;
            this.Propotional2T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // Propotional1T
            // 
            this.Propotional1T.Location = new System.Drawing.Point(247, 116);
            this.Propotional1T.Name = "Propotional1T";
            this.Propotional1T.Size = new System.Drawing.Size(63, 20);
            this.Propotional1T.TabIndex = 22;
            this.Propotional1T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // Propotional0T
            // 
            this.Propotional0T.Location = new System.Drawing.Point(247, 80);
            this.Propotional0T.Name = "Propotional0T";
            this.Propotional0T.Size = new System.Drawing.Size(63, 20);
            this.Propotional0T.TabIndex = 21;
            this.Propotional0T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(117, 198);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "to";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(117, 161);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "to";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(117, 123);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "to";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(84, 54);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 13);
            this.label14.TabIndex = 17;
            this.label14.Text = "AirSpeed (km/h)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(24, 54);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(26, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "Sch";
            // 
            // Range2T
            // 
            this.Range2T.Location = new System.Drawing.Point(140, 154);
            this.Range2T.Name = "Range2T";
            this.Range2T.Size = new System.Drawing.Size(63, 20);
            this.Range2T.TabIndex = 15;
            this.Range2T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // Range1T
            // 
            this.Range1T.Location = new System.Drawing.Point(140, 116);
            this.Range1T.Name = "Range1T";
            this.Range1T.Size = new System.Drawing.Size(63, 20);
            this.Range1T.TabIndex = 14;
            this.Range1T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // Range3T
            // 
            this.Range3T.Location = new System.Drawing.Point(140, 195);
            this.Range3T.Name = "Range3T";
            this.Range3T.Size = new System.Drawing.Size(63, 20);
            this.Range3T.TabIndex = 13;
            this.Range3T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // Range0T
            // 
            this.Range0T.Location = new System.Drawing.Point(140, 83);
            this.Range0T.Name = "Range0T";
            this.Range0T.Size = new System.Drawing.Size(63, 20);
            this.Range0T.TabIndex = 12;
            this.Range0T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // Sch3T
            // 
            this.Sch3T.Location = new System.Drawing.Point(43, 195);
            this.Sch3T.Name = "Sch3T";
            this.Sch3T.Size = new System.Drawing.Size(63, 20);
            this.Sch3T.TabIndex = 11;
            this.Sch3T.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            // 
            // Sch2T
            // 
            this.Sch2T.Location = new System.Drawing.Point(43, 158);
            this.Sch2T.Name = "Sch2T";
            this.Sch2T.Size = new System.Drawing.Size(63, 20);
            this.Sch2T.TabIndex = 10;
            this.Sch2T.TextChanged += new System.EventHandler(this.Sch2T_TextChanged);
            // 
            // Sch1T
            // 
            this.Sch1T.Location = new System.Drawing.Point(43, 118);
            this.Sch1T.Name = "Sch1T";
            this.Sch1T.Size = new System.Drawing.Size(63, 20);
            this.Sch1T.TabIndex = 9;
            this.Sch1T.TextChanged += new System.EventHandler(this.Sch1T_TextChanged);
            // 
            // Sch0T
            // 
            this.Sch0T.Location = new System.Drawing.Point(43, 80);
            this.Sch0T.Name = "Sch0T";
            this.Sch0T.Size = new System.Drawing.Size(63, 20);
            this.Sch0T.TabIndex = 8;
            this.Sch0T.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(117, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "to";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 198);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 161);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 119);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "1";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "0";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.SlewLimitsT);
            this.groupBox3.Controls.Add(this.ResultMinT);
            this.groupBox3.Controls.Add(this.ResultMaxT);
            this.groupBox3.Controls.Add(this.ScheduledT);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(130, 121);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(601, 92);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "PID Calculations and Result Control";
            // 
            // SlewLimitsT
            // 
            this.SlewLimitsT.Location = new System.Drawing.Point(138, 65);
            this.SlewLimitsT.Name = "SlewLimitsT";
            this.SlewLimitsT.Size = new System.Drawing.Size(79, 20);
            this.SlewLimitsT.TabIndex = 7;
            this.SlewLimitsT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // ResultMinT
            // 
            this.ResultMinT.Location = new System.Drawing.Point(430, 62);
            this.ResultMinT.Name = "ResultMinT";
            this.ResultMinT.Size = new System.Drawing.Size(79, 20);
            this.ResultMinT.TabIndex = 6;
            this.ResultMinT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // ResultMaxT
            // 
            this.ResultMaxT.Location = new System.Drawing.Point(430, 24);
            this.ResultMaxT.Name = "ResultMaxT";
            this.ResultMaxT.Size = new System.Drawing.Size(79, 20);
            this.ResultMaxT.TabIndex = 5;
            this.ResultMaxT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // ScheduledT
            // 
            this.ScheduledT.Location = new System.Drawing.Point(138, 27);
            this.ScheduledT.Name = "ScheduledT";
            this.ScheduledT.Size = new System.Drawing.Size(79, 20);
            this.ScheduledT.TabIndex = 4;
            this.ScheduledT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(65, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Slew Limits";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(352, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Result Min";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(352, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Result Max";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Scheduled";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.GainEffects);
            this.groupBox2.Controls.Add(this.RealUnites);
            this.groupBox2.Controls.Add(this.AutoPilotUnites);
            this.groupBox2.Location = new System.Drawing.Point(397, 11);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(348, 104);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Display";
            // 
            // GainEffects
            // 
            this.GainEffects.AutoSize = true;
            this.GainEffects.Location = new System.Drawing.Point(247, 46);
            this.GainEffects.Name = "GainEffects";
            this.GainEffects.Size = new System.Drawing.Size(83, 17);
            this.GainEffects.TabIndex = 2;
            this.GainEffects.TabStop = true;
            this.GainEffects.Text = "Gain Effects";
            this.GainEffects.UseVisualStyleBackColor = true;
            this.GainEffects.CheckedChanged += new System.EventHandler(this.GainEffects_CheckedChanged);
            // 
            // RealUnites
            // 
            this.RealUnites.AutoSize = true;
            this.RealUnites.Location = new System.Drawing.Point(142, 46);
            this.RealUnites.Name = "RealUnites";
            this.RealUnites.Size = new System.Drawing.Size(80, 17);
            this.RealUnites.TabIndex = 1;
            this.RealUnites.TabStop = true;
            this.RealUnites.Text = "Real Unites";
            this.RealUnites.UseVisualStyleBackColor = true;
            this.RealUnites.CheckedChanged += new System.EventHandler(this.RealUnites_CheckedChanged);
            // 
            // AutoPilotUnites
            // 
            this.AutoPilotUnites.AutoSize = true;
            this.AutoPilotUnites.Location = new System.Drawing.Point(6, 46);
            this.AutoPilotUnites.Name = "AutoPilotUnites";
            this.AutoPilotUnites.Size = new System.Drawing.Size(100, 17);
            this.AutoPilotUnites.TabIndex = 0;
            this.AutoPilotUnites.TabStop = true;
            this.AutoPilotUnites.Text = "AutoPilot Unites";
            this.AutoPilotUnites.UseVisualStyleBackColor = true;
            this.AutoPilotUnites.CheckedChanged += new System.EventHandler(this.AutoPilotUnites_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.EnableYawDamper);
            this.groupBox1.Controls.Add(this.SmoothGainScheduling);
            this.groupBox1.Location = new System.Drawing.Point(81, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(266, 104);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Common PID Setting";
            // 
            // EnableYawDamper
            // 
            this.EnableYawDamper.AutoSize = true;
            this.EnableYawDamper.Location = new System.Drawing.Point(50, 67);
            this.EnableYawDamper.Name = "EnableYawDamper";
            this.EnableYawDamper.Size = new System.Drawing.Size(123, 17);
            this.EnableYawDamper.TabIndex = 3;
            this.EnableYawDamper.Text = "Enable Yaw Damper";
            this.EnableYawDamper.UseVisualStyleBackColor = true;
            this.EnableYawDamper.CheckedChanged += new System.EventHandler(this.EnableYawDamper_CheckedChanged);
            // 
            // SmoothGainScheduling
            // 
            this.SmoothGainScheduling.AutoSize = true;
            this.SmoothGainScheduling.Location = new System.Drawing.Point(50, 30);
            this.SmoothGainScheduling.Name = "SmoothGainScheduling";
            this.SmoothGainScheduling.Size = new System.Drawing.Size(143, 17);
            this.SmoothGainScheduling.TabIndex = 2;
            this.SmoothGainScheduling.Text = "Smooth Gain Scheduling";
            this.SmoothGainScheduling.UseVisualStyleBackColor = true;
            this.SmoothGainScheduling.CheckedChanged += new System.EventHandler(this.SmoothGainScheduling_CheckedChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox13);
            this.tabPage3.Controls.Add(this.groupBox12);
            this.tabPage3.Controls.Add(this.groupBox11);
            this.tabPage3.Controls.Add(this.groupBox10);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(912, 747);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Servos";
            this.tabPage3.UseVisualStyleBackColor = true;
            this.tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.TO);
            this.groupBox13.Controls.Add(this.SpoilerRCInChannelT);
            this.groupBox13.Controls.Add(this.FlapsRCInChannelT);
            this.groupBox13.Controls.Add(this.SpoilerSlewLlimitT);
            this.groupBox13.Controls.Add(this.InputCalculationT);
            this.groupBox13.Controls.Add(this.DisableMixingLimit);
            this.groupBox13.Controls.Add(this.DisableRStaffServo);
            this.groupBox13.Controls.Add(this.label100);
            this.groupBox13.Controls.Add(this.label99);
            this.groupBox13.Controls.Add(this.label94);
            this.groupBox13.Controls.Add(this.label93);
            this.groupBox13.Controls.Add(this.label92);
            this.groupBox13.Location = new System.Drawing.Point(6, 554);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(769, 78);
            this.groupBox13.TabIndex = 3;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Miscellaneous";
            // 
            // TO
            // 
            this.TO.FormattingEnabled = true;
            this.TO.Items.AddRange(new object[] {
            "None",
            "RC Control",
            "RC Max",
            "GCS",
            "fServo4"});
            this.TO.Location = new System.Drawing.Point(102, 23);
            this.TO.Name = "TO";
            this.TO.Size = new System.Drawing.Size(70, 21);
            this.TO.TabIndex = 48;
            this.TO.SelectedIndexChanged += new System.EventHandler(this.TO_SelectedIndexChanged);
            // 
            // SpoilerRCInChannelT
            // 
            this.SpoilerRCInChannelT.Location = new System.Drawing.Point(548, 18);
            this.SpoilerRCInChannelT.Name = "SpoilerRCInChannelT";
            this.SpoilerRCInChannelT.Size = new System.Drawing.Size(57, 20);
            this.SpoilerRCInChannelT.TabIndex = 26;
            this.SpoilerRCInChannelT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // FlapsRCInChannelT
            // 
            this.FlapsRCInChannelT.Location = new System.Drawing.Point(327, 44);
            this.FlapsRCInChannelT.Name = "FlapsRCInChannelT";
            this.FlapsRCInChannelT.Size = new System.Drawing.Size(57, 20);
            this.FlapsRCInChannelT.TabIndex = 25;
            this.FlapsRCInChannelT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // SpoilerSlewLlimitT
            // 
            this.SpoilerSlewLlimitT.Location = new System.Drawing.Point(548, 44);
            this.SpoilerSlewLlimitT.Name = "SpoilerSlewLlimitT";
            this.SpoilerSlewLlimitT.Size = new System.Drawing.Size(57, 20);
            this.SpoilerSlewLlimitT.TabIndex = 24;
            this.SpoilerSlewLlimitT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // InputCalculationT
            // 
            this.InputCalculationT.Location = new System.Drawing.Point(327, 18);
            this.InputCalculationT.Name = "InputCalculationT";
            this.InputCalculationT.Size = new System.Drawing.Size(57, 20);
            this.InputCalculationT.TabIndex = 23;
            this.InputCalculationT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // DisableMixingLimit
            // 
            this.DisableMixingLimit.AutoSize = true;
            this.DisableMixingLimit.Location = new System.Drawing.Point(634, 17);
            this.DisableMixingLimit.Name = "DisableMixingLimit";
            this.DisableMixingLimit.Size = new System.Drawing.Size(118, 17);
            this.DisableMixingLimit.TabIndex = 21;
            this.DisableMixingLimit.Text = "Disable Mixing Limit";
            this.DisableMixingLimit.UseVisualStyleBackColor = true;
            // 
            // DisableRStaffServo
            // 
            this.DisableRStaffServo.AutoSize = true;
            this.DisableRStaffServo.Location = new System.Drawing.Point(634, 46);
            this.DisableRStaffServo.Name = "DisableRStaffServo";
            this.DisableRStaffServo.Size = new System.Drawing.Size(128, 17);
            this.DisableRStaffServo.TabIndex = 20;
            this.DisableRStaffServo.Text = "Disable R Staff Servo";
            this.DisableRStaffServo.UseVisualStyleBackColor = true;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(415, 44);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(94, 13);
            this.label100.TabIndex = 19;
            this.label100.Text = "Spoiler  Slew Llimit";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(415, 21);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(126, 13);
            this.label99.TabIndex = 18;
            this.label99.Text = "Spoiler RC Input Channel";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(192, 48);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(119, 13);
            this.label94.TabIndex = 13;
            this.label94.Text = "Flaps RC Input Channel";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(192, 21);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(86, 13);
            this.label93.TabIndex = 12;
            this.label93.Text = "Input Calculation";
            this.label93.Click += new System.EventHandler(this.label93_Click);
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(6, 26);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(86, 13);
            this.label92.TabIndex = 11;
            this.label92.Text = "Throttle Override";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.lockAileronSlider);
            this.groupBox12.Controls.Add(this.lockRudderSlider);
            this.groupBox12.Controls.Add(this.lockElavatorSlider);
            this.groupBox12.Controls.Add(this.lockRudderL);
            this.groupBox12.Controls.Add(this.lockAileronL);
            this.groupBox12.Controls.Add(this.lockElvatorL);
            this.groupBox12.Controls.Add(this.DisengageLockSpeedT);
            this.groupBox12.Controls.Add(this.label91);
            this.groupBox12.Controls.Add(this.label90);
            this.groupBox12.Controls.Add(this.label89);
            this.groupBox12.Controls.Add(this.label88);
            this.groupBox12.Controls.Add(this.EnableSevoLock);
            this.groupBox12.Location = new System.Drawing.Point(6, 476);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(863, 72);
            this.groupBox12.TabIndex = 2;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Servo Locking";
            // 
            // lockAileronSlider
            // 
            this.lockAileronSlider.Location = new System.Drawing.Point(529, 50);
            this.lockAileronSlider.Maximum = 109;
            this.lockAileronSlider.Minimum = -100;
            this.lockAileronSlider.Name = "lockAileronSlider";
            this.lockAileronSlider.Size = new System.Drawing.Size(83, 18);
            this.lockAileronSlider.TabIndex = 106;
            this.lockAileronSlider.Scroll += new System.Windows.Forms.ScrollEventHandler(this.lockAileronSlider_Scroll);
            // 
            // lockRudderSlider
            // 
            this.lockRudderSlider.Location = new System.Drawing.Point(731, 46);
            this.lockRudderSlider.Maximum = 109;
            this.lockRudderSlider.Minimum = -100;
            this.lockRudderSlider.Name = "lockRudderSlider";
            this.lockRudderSlider.Size = new System.Drawing.Size(83, 18);
            this.lockRudderSlider.TabIndex = 105;
            this.lockRudderSlider.Scroll += new System.Windows.Forms.ScrollEventHandler(this.lockRudderSlider_Scroll);
            // 
            // lockElavatorSlider
            // 
            this.lockElavatorSlider.Location = new System.Drawing.Point(324, 48);
            this.lockElavatorSlider.Maximum = 109;
            this.lockElavatorSlider.Minimum = -100;
            this.lockElavatorSlider.Name = "lockElavatorSlider";
            this.lockElavatorSlider.Size = new System.Drawing.Size(83, 18);
            this.lockElavatorSlider.TabIndex = 104;
            this.lockElavatorSlider.Scroll += new System.Windows.Forms.ScrollEventHandler(this.lockElavatorSlider_Scroll_1);
            // 
            // lockRudderL
            // 
            this.lockRudderL.AutoSize = true;
            this.lockRudderL.Location = new System.Drawing.Point(826, 46);
            this.lockRudderL.Name = "lockRudderL";
            this.lockRudderL.Size = new System.Drawing.Size(21, 13);
            this.lockRudderL.TabIndex = 70;
            this.lockRudderL.Text = "0%";
            // 
            // lockAileronL
            // 
            this.lockAileronL.AutoSize = true;
            this.lockAileronL.Location = new System.Drawing.Point(631, 48);
            this.lockAileronL.Name = "lockAileronL";
            this.lockAileronL.Size = new System.Drawing.Size(21, 13);
            this.lockAileronL.TabIndex = 67;
            this.lockAileronL.Text = "0%";
            // 
            // lockElvatorL
            // 
            this.lockElvatorL.AutoSize = true;
            this.lockElvatorL.Location = new System.Drawing.Point(420, 51);
            this.lockElvatorL.Name = "lockElvatorL";
            this.lockElvatorL.Size = new System.Drawing.Size(21, 13);
            this.lockElvatorL.TabIndex = 66;
            this.lockElvatorL.Text = "0%";
            // 
            // DisengageLockSpeedT
            // 
            this.DisengageLockSpeedT.Location = new System.Drawing.Point(167, 48);
            this.DisengageLockSpeedT.Name = "DisengageLockSpeedT";
            this.DisengageLockSpeedT.Size = new System.Drawing.Size(73, 20);
            this.DisengageLockSpeedT.TabIndex = 10;
            this.DisengageLockSpeedT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(663, 48);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(69, 13);
            this.label91.TabIndex = 4;
            this.label91.Text = "Lock Rudder";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(462, 51);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(66, 13);
            this.label90.TabIndex = 3;
            this.label90.Text = "Lock Aileron";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(255, 51);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(67, 13);
            this.label89.TabIndex = 2;
            this.label89.Text = "Lock Elvator";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(13, 51);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(153, 13);
            this.label88.TabIndex = 1;
            this.label88.Text = "Disengage Lock Speed (km/h)";
            // 
            // EnableSevoLock
            // 
            this.EnableSevoLock.AutoSize = true;
            this.EnableSevoLock.Location = new System.Drawing.Point(89, 19);
            this.EnableSevoLock.Name = "EnableSevoLock";
            this.EnableSevoLock.Size = new System.Drawing.Size(128, 17);
            this.EnableSevoLock.TabIndex = 0;
            this.EnableSevoLock.Text = "Enable Sevo Locking";
            this.EnableSevoLock.UseVisualStyleBackColor = true;
            this.EnableSevoLock.CheckedChanged += new System.EventHandler(this.EnableSevoLock_CheckedChanged);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.GyroControlServoT);
            this.groupBox11.Controls.Add(this.label87);
            this.groupBox11.Controls.Add(this.SpoilerServoT);
            this.groupBox11.Controls.Add(this.label86);
            this.groupBox11.Controls.Add(this.NoiseWheelServoT);
            this.groupBox11.Controls.Add(this.label85);
            this.groupBox11.Controls.Add(this.ReleaseServoT);
            this.groupBox11.Controls.Add(this.label84);
            this.groupBox11.Controls.Add(this.ParachuteServoT);
            this.groupBox11.Controls.Add(this.label83);
            this.groupBox11.Location = new System.Drawing.Point(6, 407);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(856, 63);
            this.groupBox11.TabIndex = 1;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Assign Servo";
            // 
            // GyroControlServoT
            // 
            this.GyroControlServoT.Location = new System.Drawing.Point(777, 31);
            this.GyroControlServoT.Name = "GyroControlServoT";
            this.GyroControlServoT.Size = new System.Drawing.Size(73, 20);
            this.GyroControlServoT.TabIndex = 9;
            this.GyroControlServoT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(680, 34);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(96, 13);
            this.label87.TabIndex = 8;
            this.label87.Text = "Gyro Control Servo";
            this.label87.Click += new System.EventHandler(this.label87_Click);
            // 
            // SpoilerServoT
            // 
            this.SpoilerServoT.Location = new System.Drawing.Point(601, 28);
            this.SpoilerServoT.Name = "SpoilerServoT";
            this.SpoilerServoT.Size = new System.Drawing.Size(73, 20);
            this.SpoilerServoT.TabIndex = 7;
            this.SpoilerServoT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(525, 31);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(70, 13);
            this.label86.TabIndex = 6;
            this.label86.Text = "Spoiler Servo";
            // 
            // NoiseWheelServoT
            // 
            this.NoiseWheelServoT.Location = new System.Drawing.Point(441, 28);
            this.NoiseWheelServoT.Name = "NoiseWheelServoT";
            this.NoiseWheelServoT.Size = new System.Drawing.Size(73, 20);
            this.NoiseWheelServoT.TabIndex = 5;
            this.NoiseWheelServoT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(339, 31);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(97, 13);
            this.label85.TabIndex = 4;
            this.label85.Text = "Nose Wheel Servo";
            // 
            // ReleaseServoT
            // 
            this.ReleaseServoT.Location = new System.Drawing.Point(260, 28);
            this.ReleaseServoT.Name = "ReleaseServoT";
            this.ReleaseServoT.Size = new System.Drawing.Size(73, 20);
            this.ReleaseServoT.TabIndex = 3;
            this.ReleaseServoT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(178, 31);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(77, 13);
            this.label84.TabIndex = 2;
            this.label84.Text = "Release Servo";
            // 
            // ParachuteServoT
            // 
            this.ParachuteServoT.Location = new System.Drawing.Point(99, 28);
            this.ParachuteServoT.Name = "ParachuteServoT";
            this.ParachuteServoT.Size = new System.Drawing.Size(73, 20);
            this.ParachuteServoT.TabIndex = 1;
            this.ParachuteServoT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(6, 31);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(87, 13);
            this.label83.TabIndex = 0;
            this.label83.Text = "Parachute Servo";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.throttleIdel);
            this.groupBox10.Controls.Add(this.t8);
            this.groupBox10.Controls.Add(this.t7);
            this.groupBox10.Controls.Add(this.t6);
            this.groupBox10.Controls.Add(this.t5);
            this.groupBox10.Controls.Add(this.t4);
            this.groupBox10.Controls.Add(this.t3);
            this.groupBox10.Controls.Add(this.t2);
            this.groupBox10.Controls.Add(this.t1);
            this.groupBox10.Controls.Add(this.z8);
            this.groupBox10.Controls.Add(this.z7);
            this.groupBox10.Controls.Add(this.z6);
            this.groupBox10.Controls.Add(this.z5);
            this.groupBox10.Controls.Add(this.z4);
            this.groupBox10.Controls.Add(this.z3);
            this.groupBox10.Controls.Add(this.z2);
            this.groupBox10.Controls.Add(this.z1);
            this.groupBox10.Controls.Add(this.label62);
            this.groupBox10.Controls.Add(this.s6t);
            this.groupBox10.Controls.Add(this.s7t);
            this.groupBox10.Controls.Add(this.st5);
            this.groupBox10.Controls.Add(this.s8t);
            this.groupBox10.Controls.Add(this.s5t);
            this.groupBox10.Controls.Add(this.s7);
            this.groupBox10.Controls.Add(this.s4);
            this.groupBox10.Controls.Add(this.s3);
            this.groupBox10.Controls.Add(this.s2);
            this.groupBox10.Controls.Add(this.s1);
            this.groupBox10.Controls.Add(this.p9);
            this.groupBox10.Controls.Add(this.p8);
            this.groupBox10.Controls.Add(this.p7);
            this.groupBox10.Controls.Add(this.p6);
            this.groupBox10.Controls.Add(this.p5);
            this.groupBox10.Controls.Add(this.p4);
            this.groupBox10.Controls.Add(this.p3);
            this.groupBox10.Controls.Add(this.p2);
            this.groupBox10.Controls.Add(this.p1);
            this.groupBox10.Controls.Add(this.init8);
            this.groupBox10.Controls.Add(this.init7);
            this.groupBox10.Controls.Add(this.init6);
            this.groupBox10.Controls.Add(this.init5);
            this.groupBox10.Controls.Add(this.init4);
            this.groupBox10.Controls.Add(this.init3);
            this.groupBox10.Controls.Add(this.init2);
            this.groupBox10.Controls.Add(this.init1);
            this.groupBox10.Controls.Add(this.TIPercentage);
            this.groupBox10.Controls.Add(this.label82);
            this.groupBox10.Controls.Add(this.label81);
            this.groupBox10.Controls.Add(this.label79);
            this.groupBox10.Controls.Add(this.label78);
            this.groupBox10.Controls.Add(this.label77);
            this.groupBox10.Controls.Add(this.label76);
            this.groupBox10.Controls.Add(this.swap8);
            this.groupBox10.Controls.Add(this.swap7);
            this.groupBox10.Controls.Add(this.swap6);
            this.groupBox10.Controls.Add(this.swap5);
            this.groupBox10.Controls.Add(this.swap4);
            this.groupBox10.Controls.Add(this.swap3);
            this.groupBox10.Controls.Add(this.swap2);
            this.groupBox10.Controls.Add(this.swap1);
            this.groupBox10.Controls.Add(this.label75);
            this.groupBox10.Controls.Add(this.label74);
            this.groupBox10.Controls.Add(this.label73);
            this.groupBox10.Controls.Add(this.label72);
            this.groupBox10.Controls.Add(this.label71);
            this.groupBox10.Controls.Add(this.label70);
            this.groupBox10.Controls.Add(this.s8);
            this.groupBox10.Controls.Add(this.s6);
            this.groupBox10.Controls.Add(this.s5);
            this.groupBox10.Controls.Add(this.s4t);
            this.groupBox10.Controls.Add(this.s3t);
            this.groupBox10.Controls.Add(this.s2t);
            this.groupBox10.Controls.Add(this.s1t);
            this.groupBox10.Controls.Add(this.StaffServoCICTimeoutT);
            this.groupBox10.Controls.Add(this.FlapSlewLimitT);
            this.groupBox10.Controls.Add(this.label61);
            this.groupBox10.Controls.Add(this.label60);
            this.groupBox10.Controls.Add(this.servoRudderComboBox);
            this.groupBox10.Controls.Add(this.ServoTypeComboBox);
            this.groupBox10.Controls.Add(this.ServoNumComboBox);
            this.groupBox10.Controls.Add(this.EchoServoAdjustment);
            this.groupBox10.Controls.Add(this.label59);
            this.groupBox10.Location = new System.Drawing.Point(15, 15);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(805, 387);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Sevo Adjustment";
            this.groupBox10.Enter += new System.EventHandler(this.groupBox10_Enter);
            // 
            // throttleIdel
            // 
            this.throttleIdel.Location = new System.Drawing.Point(315, 371);
            this.throttleIdel.Maximum = 109;
            this.throttleIdel.Name = "throttleIdel";
            this.throttleIdel.Size = new System.Drawing.Size(110, 18);
            this.throttleIdel.TabIndex = 103;
            this.throttleIdel.Scroll += new System.Windows.Forms.ScrollEventHandler(this.throttleIdel_Scroll_1);
            // 
            // t8
            // 
            this.t8.Location = new System.Drawing.Point(312, 348);
            this.t8.Maximum = 109;
            this.t8.Name = "t8";
            this.t8.Size = new System.Drawing.Size(110, 18);
            this.t8.TabIndex = 102;
            this.t8.Scroll += new System.Windows.Forms.ScrollEventHandler(this.t8_Scroll);
            // 
            // t7
            // 
            this.t7.Location = new System.Drawing.Point(312, 323);
            this.t7.Maximum = 109;
            this.t7.Name = "t7";
            this.t7.Size = new System.Drawing.Size(110, 18);
            this.t7.TabIndex = 101;
            this.t7.Scroll += new System.Windows.Forms.ScrollEventHandler(this.t7_Scroll);
            // 
            // t6
            // 
            this.t6.Location = new System.Drawing.Point(312, 293);
            this.t6.Maximum = 109;
            this.t6.Name = "t6";
            this.t6.Size = new System.Drawing.Size(110, 18);
            this.t6.TabIndex = 100;
            this.t6.Scroll += new System.Windows.Forms.ScrollEventHandler(this.t6_Scroll);
            // 
            // t5
            // 
            this.t5.Location = new System.Drawing.Point(312, 261);
            this.t5.Maximum = 109;
            this.t5.Name = "t5";
            this.t5.Size = new System.Drawing.Size(110, 18);
            this.t5.TabIndex = 99;
            this.t5.Scroll += new System.Windows.Forms.ScrollEventHandler(this.t5_Scroll);
            // 
            // t4
            // 
            this.t4.Location = new System.Drawing.Point(312, 233);
            this.t4.Maximum = 109;
            this.t4.Name = "t4";
            this.t4.Size = new System.Drawing.Size(110, 18);
            this.t4.TabIndex = 98;
            this.t4.Scroll += new System.Windows.Forms.ScrollEventHandler(this.t4_Scroll);
            // 
            // t3
            // 
            this.t3.Location = new System.Drawing.Point(312, 201);
            this.t3.Maximum = 109;
            this.t3.Name = "t3";
            this.t3.Size = new System.Drawing.Size(110, 18);
            this.t3.TabIndex = 97;
            this.t3.Scroll += new System.Windows.Forms.ScrollEventHandler(this.t3_Scroll);
            // 
            // t2
            // 
            this.t2.Location = new System.Drawing.Point(312, 175);
            this.t2.Maximum = 109;
            this.t2.Name = "t2";
            this.t2.Size = new System.Drawing.Size(110, 18);
            this.t2.TabIndex = 96;
            this.t2.Scroll += new System.Windows.Forms.ScrollEventHandler(this.t2_Scroll);
            // 
            // t1
            // 
            this.t1.Location = new System.Drawing.Point(312, 142);
            this.t1.Maximum = 109;
            this.t1.Name = "t1";
            this.t1.Size = new System.Drawing.Size(110, 18);
            this.t1.TabIndex = 95;
            this.t1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.t1_Scroll);
            // 
            // z8
            // 
            this.z8.Location = new System.Drawing.Point(172, 347);
            this.z8.Maximum = 109;
            this.z8.Name = "z8";
            this.z8.Size = new System.Drawing.Size(110, 18);
            this.z8.TabIndex = 94;
            this.z8.Scroll += new System.Windows.Forms.ScrollEventHandler(this.z8_Scroll);
            // 
            // z7
            // 
            this.z7.Location = new System.Drawing.Point(168, 317);
            this.z7.Maximum = 109;
            this.z7.Name = "z7";
            this.z7.Size = new System.Drawing.Size(110, 18);
            this.z7.TabIndex = 93;
            this.z7.Scroll += new System.Windows.Forms.ScrollEventHandler(this.z7_Scroll);
            // 
            // z6
            // 
            this.z6.Location = new System.Drawing.Point(168, 289);
            this.z6.Maximum = 109;
            this.z6.Name = "z6";
            this.z6.Size = new System.Drawing.Size(110, 18);
            this.z6.TabIndex = 92;
            this.z6.Scroll += new System.Windows.Forms.ScrollEventHandler(this.z6_Scroll);
            // 
            // z5
            // 
            this.z5.Location = new System.Drawing.Point(168, 258);
            this.z5.Maximum = 109;
            this.z5.Name = "z5";
            this.z5.Size = new System.Drawing.Size(110, 18);
            this.z5.TabIndex = 91;
            this.z5.Scroll += new System.Windows.Forms.ScrollEventHandler(this.z5_Scroll);
            // 
            // z4
            // 
            this.z4.Location = new System.Drawing.Point(168, 229);
            this.z4.Maximum = 109;
            this.z4.Name = "z4";
            this.z4.Size = new System.Drawing.Size(110, 18);
            this.z4.TabIndex = 90;
            this.z4.Scroll += new System.Windows.Forms.ScrollEventHandler(this.z4_Scroll);
            // 
            // z3
            // 
            this.z3.Location = new System.Drawing.Point(168, 201);
            this.z3.Maximum = 109;
            this.z3.Name = "z3";
            this.z3.Size = new System.Drawing.Size(110, 18);
            this.z3.TabIndex = 89;
            this.z3.Scroll += new System.Windows.Forms.ScrollEventHandler(this.z3_Scroll);
            // 
            // z2
            // 
            this.z2.Location = new System.Drawing.Point(168, 171);
            this.z2.Maximum = 109;
            this.z2.Name = "z2";
            this.z2.Size = new System.Drawing.Size(110, 18);
            this.z2.TabIndex = 88;
            this.z2.Scroll += new System.Windows.Forms.ScrollEventHandler(this.z2_Scroll);
            // 
            // z1
            // 
            this.z1.Location = new System.Drawing.Point(168, 142);
            this.z1.Maximum = 109;
            this.z1.Name = "z1";
            this.z1.Size = new System.Drawing.Size(110, 18);
            this.z1.TabIndex = 87;
            this.z1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.z1_Scroll);
            this.z1.ValueChanged += new System.EventHandler(this.z1SlidersValueChanged);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(195, 369);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(63, 13);
            this.label62.TabIndex = 10;
            this.label62.Text = "Throttle Idel";
            // 
            // s6t
            // 
            this.s6t.AutoSize = true;
            this.s6t.Location = new System.Drawing.Point(51, 293);
            this.s6t.Name = "s6t";
            this.s6t.Size = new System.Drawing.Size(37, 13);
            this.s6t.TabIndex = 86;
            this.s6t.Text = "          ";
            // 
            // s7t
            // 
            this.s7t.AutoSize = true;
            this.s7t.Location = new System.Drawing.Point(54, 320);
            this.s7t.Name = "s7t";
            this.s7t.Size = new System.Drawing.Size(37, 13);
            this.s7t.TabIndex = 85;
            this.s7t.Text = "          ";
            // 
            // st5
            // 
            this.st5.AutoSize = true;
            this.st5.Location = new System.Drawing.Point(54, 261);
            this.st5.Name = "st5";
            this.st5.Size = new System.Drawing.Size(28, 13);
            this.st5.TabIndex = 84;
            this.st5.Text = "       ";
            // 
            // s8t
            // 
            this.s8t.AutoSize = true;
            this.s8t.Location = new System.Drawing.Point(54, 352);
            this.s8t.Name = "s8t";
            this.s8t.Size = new System.Drawing.Size(40, 13);
            this.s8t.TabIndex = 83;
            this.s8t.Text = "           ";
            // 
            // s5t
            // 
            this.s5t.AutoSize = true;
            this.s5t.Location = new System.Drawing.Point(54, 261);
            this.s5t.Name = "s5t";
            this.s5t.Size = new System.Drawing.Size(0, 13);
            this.s5t.TabIndex = 82;
            // 
            // s7
            // 
            this.s7.AutoSize = true;
            this.s7.Location = new System.Drawing.Point(28, 320);
            this.s7.Name = "s7";
            this.s7.Size = new System.Drawing.Size(13, 13);
            this.s7.TabIndex = 81;
            this.s7.Text = "7";
            // 
            // s4
            // 
            this.s4.AutoSize = true;
            this.s4.Location = new System.Drawing.Point(28, 233);
            this.s4.Name = "s4";
            this.s4.Size = new System.Drawing.Size(13, 13);
            this.s4.TabIndex = 80;
            this.s4.Text = "4";
            // 
            // s3
            // 
            this.s3.AutoSize = true;
            this.s3.Location = new System.Drawing.Point(28, 201);
            this.s3.Name = "s3";
            this.s3.Size = new System.Drawing.Size(13, 13);
            this.s3.TabIndex = 79;
            this.s3.Text = "3";
            // 
            // s2
            // 
            this.s2.AutoSize = true;
            this.s2.Location = new System.Drawing.Point(28, 171);
            this.s2.Name = "s2";
            this.s2.Size = new System.Drawing.Size(13, 13);
            this.s2.TabIndex = 78;
            this.s2.Text = "2";
            // 
            // s1
            // 
            this.s1.AutoSize = true;
            this.s1.Location = new System.Drawing.Point(28, 142);
            this.s1.Name = "s1";
            this.s1.Size = new System.Drawing.Size(13, 13);
            this.s1.TabIndex = 77;
            this.s1.Text = "1";
            // 
            // p9
            // 
            this.p9.AutoSize = true;
            this.p9.Location = new System.Drawing.Point(453, 371);
            this.p9.Name = "p9";
            this.p9.Size = new System.Drawing.Size(29, 13);
            this.p9.TabIndex = 76;
            this.p9.Text = " 0ms";
            // 
            // p8
            // 
            this.p8.AutoSize = true;
            this.p8.Location = new System.Drawing.Point(449, 344);
            this.p8.Name = "p8";
            this.p8.Size = new System.Drawing.Size(47, 13);
            this.p8.TabIndex = 75;
            this.p8.Text = "0 +- 0ms";
            // 
            // p7
            // 
            this.p7.AutoSize = true;
            this.p7.Location = new System.Drawing.Point(449, 317);
            this.p7.Name = "p7";
            this.p7.Size = new System.Drawing.Size(47, 13);
            this.p7.TabIndex = 74;
            this.p7.Text = "0 +- 0ms";
            // 
            // p6
            // 
            this.p6.AutoSize = true;
            this.p6.Location = new System.Drawing.Point(449, 290);
            this.p6.Name = "p6";
            this.p6.Size = new System.Drawing.Size(47, 13);
            this.p6.TabIndex = 73;
            this.p6.Text = "0 +- 0ms";
            // 
            // p5
            // 
            this.p5.AutoSize = true;
            this.p5.Location = new System.Drawing.Point(450, 258);
            this.p5.Name = "p5";
            this.p5.Size = new System.Drawing.Size(47, 13);
            this.p5.TabIndex = 72;
            this.p5.Text = "0 +- 0ms";
            // 
            // p4
            // 
            this.p4.AutoSize = true;
            this.p4.Location = new System.Drawing.Point(449, 229);
            this.p4.Name = "p4";
            this.p4.Size = new System.Drawing.Size(47, 13);
            this.p4.TabIndex = 71;
            this.p4.Text = "0 +- 0ms";
            // 
            // p3
            // 
            this.p3.AutoSize = true;
            this.p3.Location = new System.Drawing.Point(449, 201);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(47, 13);
            this.p3.TabIndex = 70;
            this.p3.Text = "0 +- 0ms";
            // 
            // p2
            // 
            this.p2.AutoSize = true;
            this.p2.Location = new System.Drawing.Point(450, 172);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(47, 13);
            this.p2.TabIndex = 69;
            this.p2.Text = "0 +- 0ms";
            // 
            // p1
            // 
            this.p1.AutoSize = true;
            this.p1.Location = new System.Drawing.Point(450, 141);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(47, 13);
            this.p1.TabIndex = 68;
            this.p1.Text = "0 +- 0ms";
            // 
            // init8
            // 
            this.init8.FormattingEnabled = true;
            this.init8.Items.AddRange(new object[] {
            "Neutral",
            "Maximum",
            "Minimum"});
            this.init8.Location = new System.Drawing.Point(720, 344);
            this.init8.Name = "init8";
            this.init8.Size = new System.Drawing.Size(67, 21);
            this.init8.TabIndex = 46;
            this.init8.SelectedIndexChanged += new System.EventHandler(this.initChanged);
            // 
            // init7
            // 
            this.init7.FormattingEnabled = true;
            this.init7.Items.AddRange(new object[] {
            "Neutral",
            "Maximum",
            "Minimum"});
            this.init7.Location = new System.Drawing.Point(720, 317);
            this.init7.Name = "init7";
            this.init7.Size = new System.Drawing.Size(67, 21);
            this.init7.TabIndex = 45;
            this.init7.SelectedIndexChanged += new System.EventHandler(this.initChanged);
            // 
            // init6
            // 
            this.init6.FormattingEnabled = true;
            this.init6.Items.AddRange(new object[] {
            "Neutral",
            "Maximum",
            "Minimum"});
            this.init6.Location = new System.Drawing.Point(720, 290);
            this.init6.Name = "init6";
            this.init6.Size = new System.Drawing.Size(67, 21);
            this.init6.TabIndex = 44;
            this.init6.SelectedIndexChanged += new System.EventHandler(this.initChanged);
            // 
            // init5
            // 
            this.init5.FormattingEnabled = true;
            this.init5.Items.AddRange(new object[] {
            "Neutral",
            "Maximum",
            "Minimum"});
            this.init5.Location = new System.Drawing.Point(720, 261);
            this.init5.Name = "init5";
            this.init5.Size = new System.Drawing.Size(67, 21);
            this.init5.TabIndex = 43;
            this.init5.SelectedIndexChanged += new System.EventHandler(this.initChanged);
            // 
            // init4
            // 
            this.init4.FormattingEnabled = true;
            this.init4.Items.AddRange(new object[] {
            "Neutral",
            "Maximum",
            "Minimum"});
            this.init4.Location = new System.Drawing.Point(720, 234);
            this.init4.Name = "init4";
            this.init4.Size = new System.Drawing.Size(67, 21);
            this.init4.TabIndex = 42;
            this.init4.SelectedIndexChanged += new System.EventHandler(this.initChanged);
            // 
            // init3
            // 
            this.init3.FormattingEnabled = true;
            this.init3.Items.AddRange(new object[] {
            "Neutral",
            "Maximum",
            "Minimum"});
            this.init3.Location = new System.Drawing.Point(720, 207);
            this.init3.Name = "init3";
            this.init3.Size = new System.Drawing.Size(67, 21);
            this.init3.TabIndex = 41;
            this.init3.SelectedIndexChanged += new System.EventHandler(this.initChanged);
            // 
            // init2
            // 
            this.init2.FormattingEnabled = true;
            this.init2.Items.AddRange(new object[] {
            "Neutral",
            "Maximum",
            "Minimum"});
            this.init2.Location = new System.Drawing.Point(720, 172);
            this.init2.Name = "init2";
            this.init2.Size = new System.Drawing.Size(67, 21);
            this.init2.TabIndex = 40;
            this.init2.SelectedIndexChanged += new System.EventHandler(this.initChanged);
            // 
            // init1
            // 
            this.init1.FormattingEnabled = true;
            this.init1.Items.AddRange(new object[] {
            "Neutral",
            "Maximum",
            "Minimum"});
            this.init1.Location = new System.Drawing.Point(720, 139);
            this.init1.Name = "init1";
            this.init1.Size = new System.Drawing.Size(67, 21);
            this.init1.TabIndex = 39;
            this.init1.SelectedIndexChanged += new System.EventHandler(this.initChanged);
            // 
            // TIPercentage
            // 
            this.TIPercentage.AutoSize = true;
            this.TIPercentage.Location = new System.Drawing.Point(546, 371);
            this.TIPercentage.Name = "TIPercentage";
            this.TIPercentage.Size = new System.Drawing.Size(21, 13);
            this.TIPercentage.TabIndex = 38;
            this.TIPercentage.Text = "0%";
            this.TIPercentage.Click += new System.EventHandler(this.label80_Click);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(623, 316);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(78, 13);
            this.label82.TabIndex = 37;
            this.label82.Text = "Control Arm Up";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(617, 171);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(78, 13);
            this.label81.TabIndex = 36;
            this.label81.Text = "Control Arm Up";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(636, 200);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(42, 13);
            this.label79.TabIndex = 34;
            this.label79.Text = "TE Left";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(623, 233);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(72, 13);
            this.label78.TabIndex = 33;
            this.label78.Text = "Throttle Open";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(617, 142);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(78, 13);
            this.label77.TabIndex = 32;
            this.label77.Text = "Control Arm Up";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(38, 110);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(35, 13);
            this.label76.TabIndex = 31;
            this.label76.Text = "Servo";
            // 
            // swap8
            // 
            this.swap8.AutoSize = true;
            this.swap8.Location = new System.Drawing.Point(555, 347);
            this.swap8.Name = "swap8";
            this.swap8.Size = new System.Drawing.Size(15, 14);
            this.swap8.TabIndex = 30;
            this.swap8.UseVisualStyleBackColor = true;
            this.swap8.CheckedChanged += new System.EventHandler(this.swap8_CheckedChanged);
            // 
            // swap7
            // 
            this.swap7.AutoSize = true;
            this.swap7.Location = new System.Drawing.Point(555, 315);
            this.swap7.Name = "swap7";
            this.swap7.Size = new System.Drawing.Size(15, 14);
            this.swap7.TabIndex = 29;
            this.swap7.UseVisualStyleBackColor = true;
            this.swap7.CheckedChanged += new System.EventHandler(this.swap7_CheckedChanged);
            // 
            // swap6
            // 
            this.swap6.AutoSize = true;
            this.swap6.Location = new System.Drawing.Point(555, 289);
            this.swap6.Name = "swap6";
            this.swap6.Size = new System.Drawing.Size(15, 14);
            this.swap6.TabIndex = 28;
            this.swap6.UseVisualStyleBackColor = true;
            this.swap6.CheckedChanged += new System.EventHandler(this.swap6_CheckedChanged);
            // 
            // swap5
            // 
            this.swap5.AutoSize = true;
            this.swap5.Location = new System.Drawing.Point(555, 260);
            this.swap5.Name = "swap5";
            this.swap5.Size = new System.Drawing.Size(15, 14);
            this.swap5.TabIndex = 27;
            this.swap5.UseVisualStyleBackColor = true;
            this.swap5.CheckedChanged += new System.EventHandler(this.swap5_CheckedChanged);
            // 
            // swap4
            // 
            this.swap4.AutoSize = true;
            this.swap4.Location = new System.Drawing.Point(555, 233);
            this.swap4.Name = "swap4";
            this.swap4.Size = new System.Drawing.Size(15, 14);
            this.swap4.TabIndex = 26;
            this.swap4.UseVisualStyleBackColor = true;
            this.swap4.CheckedChanged += new System.EventHandler(this.swap4_CheckedChanged);
            // 
            // swap3
            // 
            this.swap3.AutoSize = true;
            this.swap3.Location = new System.Drawing.Point(555, 200);
            this.swap3.Name = "swap3";
            this.swap3.Size = new System.Drawing.Size(15, 14);
            this.swap3.TabIndex = 25;
            this.swap3.UseVisualStyleBackColor = true;
            this.swap3.CheckedChanged += new System.EventHandler(this.swap3_CheckedChanged);
            // 
            // swap2
            // 
            this.swap2.AutoSize = true;
            this.swap2.Location = new System.Drawing.Point(555, 171);
            this.swap2.Name = "swap2";
            this.swap2.Size = new System.Drawing.Size(15, 14);
            this.swap2.TabIndex = 24;
            this.swap2.UseVisualStyleBackColor = true;
            this.swap2.CheckedChanged += new System.EventHandler(this.swap2_CheckedChanged);
            // 
            // swap1
            // 
            this.swap1.AutoSize = true;
            this.swap1.Location = new System.Drawing.Point(555, 141);
            this.swap1.Name = "swap1";
            this.swap1.Size = new System.Drawing.Size(15, 14);
            this.swap1.TabIndex = 23;
            this.swap1.UseVisualStyleBackColor = true;
            this.swap1.CheckedChanged += new System.EventHandler(this.swap1_CheckedChanged);
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(730, 110);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(42, 13);
            this.label75.TabIndex = 22;
            this.label75.Text = "Intialize";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(636, 110);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(44, 13);
            this.label74.TabIndex = 21;
            this.label74.Text = "Positive";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(552, 110);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(34, 13);
            this.label73.TabIndex = 20;
            this.label73.Text = "Swap";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(455, 110);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(33, 13);
            this.label72.TabIndex = 19;
            this.label72.Text = "Pulse";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(326, 110);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(37, 13);
            this.label71.TabIndex = 18;
            this.label71.Text = "Travel";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(205, 110);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(29, 13);
            this.label70.TabIndex = 17;
            this.label70.Text = "Zero";
            // 
            // s8
            // 
            this.s8.AutoSize = true;
            this.s8.Location = new System.Drawing.Point(28, 348);
            this.s8.Name = "s8";
            this.s8.Size = new System.Drawing.Size(13, 13);
            this.s8.TabIndex = 16;
            this.s8.Text = "8";
            // 
            // s6
            // 
            this.s6.AutoSize = true;
            this.s6.Location = new System.Drawing.Point(28, 293);
            this.s6.Name = "s6";
            this.s6.Size = new System.Drawing.Size(13, 13);
            this.s6.TabIndex = 14;
            this.s6.Text = "6";
            this.s6.Click += new System.EventHandler(this.label67_Click);
            // 
            // s5
            // 
            this.s5.AutoSize = true;
            this.s5.Location = new System.Drawing.Point(28, 260);
            this.s5.Name = "s5";
            this.s5.Size = new System.Drawing.Size(13, 13);
            this.s5.TabIndex = 13;
            this.s5.Text = "5";
            // 
            // s4t
            // 
            this.s4t.AutoSize = true;
            this.s4t.Location = new System.Drawing.Point(54, 233);
            this.s4t.Name = "s4t";
            this.s4t.Size = new System.Drawing.Size(46, 13);
            this.s4t.TabIndex = 12;
            this.s4t.Text = " Throttle";
            // 
            // s3t
            // 
            this.s3t.AutoSize = true;
            this.s3t.Location = new System.Drawing.Point(51, 201);
            this.s3t.Name = "s3t";
            this.s3t.Size = new System.Drawing.Size(51, 13);
            this.s3t.TabIndex = 11;
            this.s3t.Text = "   Rudder";
            // 
            // s2t
            // 
            this.s2t.AutoSize = true;
            this.s2t.Location = new System.Drawing.Point(54, 172);
            this.s2t.Name = "s2t";
            this.s2t.Size = new System.Drawing.Size(43, 13);
            this.s2t.TabIndex = 10;
            this.s2t.Text = " Elvator";
            // 
            // s1t
            // 
            this.s1t.AutoSize = true;
            this.s1t.Location = new System.Drawing.Point(51, 142);
            this.s1t.Name = "s1t";
            this.s1t.Size = new System.Drawing.Size(60, 13);
            this.s1t.TabIndex = 9;
            this.s1t.Text = "Left Alieron";
            // 
            // StaffServoCICTimeoutT
            // 
            this.StaffServoCICTimeoutT.Location = new System.Drawing.Point(651, 67);
            this.StaffServoCICTimeoutT.Name = "StaffServoCICTimeoutT";
            this.StaffServoCICTimeoutT.Size = new System.Drawing.Size(115, 20);
            this.StaffServoCICTimeoutT.TabIndex = 8;
            this.StaffServoCICTimeoutT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // FlapSlewLimitT
            // 
            this.FlapSlewLimitT.Location = new System.Drawing.Point(651, 28);
            this.FlapSlewLimitT.Name = "FlapSlewLimitT";
            this.FlapSlewLimitT.Size = new System.Drawing.Size(115, 20);
            this.FlapSlewLimitT.TabIndex = 7;
            this.FlapSlewLimitT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(494, 70);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(121, 13);
            this.label61.TabIndex = 6;
            this.label61.Text = "Staff Servo CIC Timeout";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(525, 31);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(77, 13);
            this.label60.TabIndex = 5;
            this.label60.Text = "Flap Slew Limit";
            // 
            // servoRudderComboBox
            // 
            this.servoRudderComboBox.FormattingEnabled = true;
            this.servoRudderComboBox.Items.AddRange(new object[] {
            "Normal Rudders",
            "V- Tail",
            "Split Rudder",
            "No Rudder",
            "X- Tail",
            "Differential Thrust",
            "Twin Rudders"});
            this.servoRudderComboBox.Location = new System.Drawing.Point(306, 67);
            this.servoRudderComboBox.Name = "servoRudderComboBox";
            this.servoRudderComboBox.Size = new System.Drawing.Size(116, 21);
            this.servoRudderComboBox.TabIndex = 4;
            this.servoRudderComboBox.SelectedIndexChanged += new System.EventHandler(this.servoRudderComboBox_SelectedIndexChanged);
            // 
            // ServoTypeComboBox
            // 
            this.ServoTypeComboBox.FormattingEnabled = true;
            this.ServoTypeComboBox.Items.AddRange(new object[] {
            "No Flaps",
            "Separate Flaps",
            "Combined Flaperons",
            "Separate Flaperons",
            "Elevons ",
            "Split Flaperons",
            "Y-Tail",
            "Hydrodynamic Tabs"});
            this.ServoTypeComboBox.Location = new System.Drawing.Point(162, 67);
            this.ServoTypeComboBox.Name = "ServoTypeComboBox";
            this.ServoTypeComboBox.Size = new System.Drawing.Size(116, 21);
            this.ServoTypeComboBox.TabIndex = 3;
            this.ServoTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.ServoTypeComboBox_SelectedIndexChanged);
            // 
            // ServoNumComboBox
            // 
            this.ServoNumComboBox.FormattingEnabled = true;
            this.ServoNumComboBox.Items.AddRange(new object[] {
            "Servos 1 - 8"});
            this.ServoNumComboBox.Location = new System.Drawing.Point(19, 67);
            this.ServoNumComboBox.Name = "ServoNumComboBox";
            this.ServoNumComboBox.Size = new System.Drawing.Size(116, 21);
            this.ServoNumComboBox.TabIndex = 2;
            this.ServoNumComboBox.SelectedIndexChanged += new System.EventHandler(this.ServoNumComboBox_SelectedIndexChanged);
            // 
            // EchoServoAdjustment
            // 
            this.EchoServoAdjustment.FormattingEnabled = true;
            this.EchoServoAdjustment.Items.AddRange(new object[] {
            "Echo Disabled",
            "Echo Zeros",
            "Echo Max Travel",
            "Echo Throttle Idle",
            "Echo Mixed Servos"});
            this.EchoServoAdjustment.Location = new System.Drawing.Point(141, 21);
            this.EchoServoAdjustment.Name = "EchoServoAdjustment";
            this.EchoServoAdjustment.Size = new System.Drawing.Size(116, 21);
            this.EchoServoAdjustment.TabIndex = 1;
            this.EchoServoAdjustment.SelectedIndexChanged += new System.EventHandler(this.EchoServoAdjustment_SelectedIndexChanged);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(16, 24);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(118, 13);
            this.label59.TabIndex = 0;
            this.label59.Text = "Echo Servo Adjustment";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.customuarts);
            this.tabPage4.Controls.Add(this.groupBox19);
            this.tabPage4.Controls.Add(this.groupBox18);
            this.tabPage4.Controls.Add(this.groupBox17);
            this.tabPage4.Controls.Add(this.groupBox16);
            this.tabPage4.Controls.Add(this.groupBox15);
            this.tabPage4.Controls.Add(this.groupBox14);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(912, 747);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Comms";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // customuarts
            // 
            this.customuarts.Location = new System.Drawing.Point(405, 583);
            this.customuarts.Name = "customuarts";
            this.customuarts.Size = new System.Drawing.Size(207, 23);
            this.customuarts.TabIndex = 7;
            this.customuarts.Text = "Configure Custom UARTS";
            this.customuarts.UseVisualStyleBackColor = true;
            this.customuarts.Click += new System.EventHandler(this.customuarts_Click);
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.SLRCenterfailure);
            this.groupBox19.Controls.Add(this.SLRCsecondary);
            this.groupBox19.Controls.Add(this.MLRCenterfailure);
            this.groupBox19.Controls.Add(this.LRCuselrc);
            this.groupBox19.Controls.Add(this.LRCsecodarycomport);
            this.groupBox19.Controls.Add(this.LRConlyreadservos);
            this.groupBox19.Controls.Add(this.label136);
            this.groupBox19.Controls.Add(this.label134);
            this.groupBox19.Controls.Add(this.Lr);
            this.groupBox19.Controls.Add(this.label132);
            this.groupBox19.Controls.Add(this.label131);
            this.groupBox19.Controls.Add(this.label130);
            this.groupBox19.Controls.Add(this.label129);
            this.groupBox19.Location = new System.Drawing.Point(311, 383);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(382, 183);
            this.groupBox19.TabIndex = 5;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "LRC";
            // 
            // SLRCenterfailure
            // 
            this.SLRCenterfailure.AutoSize = true;
            this.SLRCenterfailure.Location = new System.Drawing.Point(340, 102);
            this.SLRCenterfailure.Name = "SLRCenterfailure";
            this.SLRCenterfailure.Size = new System.Drawing.Size(15, 14);
            this.SLRCenterfailure.TabIndex = 44;
            this.SLRCenterfailure.UseVisualStyleBackColor = true;
            this.SLRCenterfailure.CheckedChanged += new System.EventHandler(this.SLRCenterfailure_CheckedChanged);
            // 
            // SLRCsecondary
            // 
            this.SLRCsecondary.AutoSize = true;
            this.SLRCsecondary.Location = new System.Drawing.Point(340, 71);
            this.SLRCsecondary.Name = "SLRCsecondary";
            this.SLRCsecondary.Size = new System.Drawing.Size(15, 14);
            this.SLRCsecondary.TabIndex = 43;
            this.SLRCsecondary.UseVisualStyleBackColor = true;
            this.SLRCsecondary.CheckedChanged += new System.EventHandler(this.SLRCsecondary_CheckedChanged);
            // 
            // MLRCenterfailure
            // 
            this.MLRCenterfailure.AutoSize = true;
            this.MLRCenterfailure.Location = new System.Drawing.Point(141, 102);
            this.MLRCenterfailure.Name = "MLRCenterfailure";
            this.MLRCenterfailure.Size = new System.Drawing.Size(15, 14);
            this.MLRCenterfailure.TabIndex = 42;
            this.MLRCenterfailure.UseVisualStyleBackColor = true;
            this.MLRCenterfailure.CheckedChanged += new System.EventHandler(this.MLRCenterfailure_CheckedChanged);
            // 
            // LRCuselrc
            // 
            this.LRCuselrc.AutoSize = true;
            this.LRCuselrc.Location = new System.Drawing.Point(141, 69);
            this.LRCuselrc.Name = "LRCuselrc";
            this.LRCuselrc.Size = new System.Drawing.Size(15, 14);
            this.LRCuselrc.TabIndex = 41;
            this.LRCuselrc.UseVisualStyleBackColor = true;
            this.LRCuselrc.CheckedChanged += new System.EventHandler(this.LRCuselrc_CheckedChanged);
            // 
            // LRCsecodarycomport
            // 
            this.LRCsecodarycomport.FormattingEnabled = true;
            this.LRCsecodarycomport.Items.AddRange(new object[] {
            "Disable",
            "UART1",
            "UART2"});
            this.LRCsecodarycomport.Location = new System.Drawing.Point(197, 156);
            this.LRCsecodarycomport.Name = "LRCsecodarycomport";
            this.LRCsecodarycomport.Size = new System.Drawing.Size(100, 21);
            this.LRCsecodarycomport.TabIndex = 40;
            this.LRCsecodarycomport.SelectedIndexChanged += new System.EventHandler(this.LRCsecodarycomport_SelectedIndexChanged);
            // 
            // LRConlyreadservos
            // 
            this.LRConlyreadservos.AutoSize = true;
            this.LRConlyreadservos.Location = new System.Drawing.Point(28, 130);
            this.LRConlyreadservos.Name = "LRConlyreadservos";
            this.LRConlyreadservos.Size = new System.Drawing.Size(236, 17);
            this.LRConlyreadservos.TabIndex = 39;
            this.LRConlyreadservos.Text = "Only read servos from one COM port at once";
            this.LRConlyreadservos.UseVisualStyleBackColor = true;
            this.LRConlyreadservos.CheckedChanged += new System.EventHandler(this.LRConlyreadservos_CheckedChanged);
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(25, 156);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(131, 13);
            this.label136.TabIndex = 19;
            this.label136.Text = "Second COM port for LRC";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(194, 102);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(103, 13);
            this.label134.TabIndex = 17;
            this.label134.Text = "Enter Failure Pattern";
            // 
            // Lr
            // 
            this.Lr.AutoSize = true;
            this.Lr.Location = new System.Drawing.Point(194, 71);
            this.Lr.Name = "Lr";
            this.Lr.Size = new System.Drawing.Size(103, 13);
            this.Lr.TabIndex = 16;
            this.Lr.Text = "Use Secondary Link";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(15, 102);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(103, 13);
            this.label132.TabIndex = 15;
            this.label132.Text = "Enter Failure Pattern";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(15, 70);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(50, 13);
            this.label131.TabIndex = 14;
            this.label131.Text = "Use LRC";
            this.label131.Click += new System.EventHandler(this.label131_Click);
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(233, 40);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(81, 13);
            this.label130.TabIndex = 13;
            this.label130.Text = "Secondary Link";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(46, 40);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(53, 13);
            this.label129.TabIndex = 12;
            this.label129.Text = "Main Link";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label117);
            this.groupBox18.Controls.Add(this.label118);
            this.groupBox18.Controls.Add(this.label119);
            this.groupBox18.Controls.Add(this.label120);
            this.groupBox18.Controls.Add(this.label121);
            this.groupBox18.Controls.Add(this.label122);
            this.groupBox18.Controls.Add(this.label123);
            this.groupBox18.Controls.Add(this.label124);
            this.groupBox18.Controls.Add(this.label125);
            this.groupBox18.Controls.Add(this.label126);
            this.groupBox18.Controls.Add(this.label127);
            this.groupBox18.Controls.Add(this.label128);
            this.groupBox18.Controls.Add(this.UART2Rxchannel);
            this.groupBox18.Controls.Add(this.UART2disablebinding);
            this.groupBox18.Controls.Add(this.UART2Txprotocolfield);
            this.groupBox18.Controls.Add(this.UART2Txparity);
            this.groupBox18.Controls.Add(this.UART2Txtimeout);
            this.groupBox18.Controls.Add(this.UART2Txdatabits);
            this.groupBox18.Controls.Add(this.UART2Txbaud);
            this.groupBox18.Controls.Add(this.UART2Txchannel);
            this.groupBox18.Controls.Add(this.UART2Rxparity);
            this.groupBox18.Controls.Add(this.UART2Rxdatabits);
            this.groupBox18.Controls.Add(this.UART2Rxbaud);
            this.groupBox18.Controls.Add(this.UART2protocol);
            this.groupBox18.Location = new System.Drawing.Point(565, 21);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(248, 356);
            this.groupBox18.TabIndex = 4;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "UART2";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(6, 310);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(80, 13);
            this.label117.TabIndex = 75;
            this.label117.Text = "Disable Binding";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(6, 253);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(80, 13);
            this.label118.TabIndex = 74;
            this.label118.Text = "ModemTimeout";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(6, 278);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(71, 13);
            this.label119.TabIndex = 73;
            this.label119.Text = "Protocol Field";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(6, 227);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(48, 13);
            this.label120.TabIndex = 72;
            this.label120.Text = "Tx Parity";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(6, 201);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(90, 13);
            this.label121.TabIndex = 71;
            this.label121.Text = "Tx Num Data Bits";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(6, 176);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(47, 13);
            this.label122.TabIndex = 70;
            this.label122.Text = "Tx Baud";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(6, 147);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(61, 13);
            this.label123.TabIndex = 69;
            this.label123.Text = "Tx Channel";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(6, 123);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(49, 13);
            this.label124.TabIndex = 68;
            this.label124.Text = "Rx Parity";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(6, 98);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(91, 13);
            this.label125.TabIndex = 67;
            this.label125.Text = "Rx Num Data Bits";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(6, 75);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(48, 13);
            this.label126.TabIndex = 66;
            this.label126.Text = "Rx Baud";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(6, 48);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(62, 13);
            this.label127.TabIndex = 65;
            this.label127.Text = "Rx Channel";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(6, 22);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(46, 13);
            this.label128.TabIndex = 64;
            this.label128.Text = "Protocol";
            // 
            // UART2Rxchannel
            // 
            this.UART2Rxchannel.Location = new System.Drawing.Point(148, 45);
            this.UART2Rxchannel.Name = "UART2Rxchannel";
            this.UART2Rxchannel.Size = new System.Drawing.Size(34, 20);
            this.UART2Rxchannel.TabIndex = 63;
            this.UART2Rxchannel.TextChanged += new System.EventHandler(this.UART2Rxchannel_TextChanged);
            // 
            // UART2disablebinding
            // 
            this.UART2disablebinding.AutoSize = true;
            this.UART2disablebinding.Location = new System.Drawing.Point(148, 311);
            this.UART2disablebinding.Name = "UART2disablebinding";
            this.UART2disablebinding.Size = new System.Drawing.Size(15, 14);
            this.UART2disablebinding.TabIndex = 62;
            this.UART2disablebinding.UseVisualStyleBackColor = true;
            this.UART2disablebinding.CheckedChanged += new System.EventHandler(this.UART2disablebinding_CheckedChanged);
            // 
            // UART2Txprotocolfield
            // 
            this.UART2Txprotocolfield.FormattingEnabled = true;
            this.UART2Txprotocolfield.Items.AddRange(new object[] {
            "Scratch",
            "Protocol",
            "Custom"});
            this.UART2Txprotocolfield.Location = new System.Drawing.Point(148, 279);
            this.UART2Txprotocolfield.Name = "UART2Txprotocolfield";
            this.UART2Txprotocolfield.Size = new System.Drawing.Size(100, 21);
            this.UART2Txprotocolfield.TabIndex = 61;
            this.UART2Txprotocolfield.SelectedIndexChanged += new System.EventHandler(this.UART2Txprotocolfield_SelectedIndexChanged);
            // 
            // UART2Txparity
            // 
            this.UART2Txparity.Location = new System.Drawing.Point(148, 227);
            this.UART2Txparity.Name = "UART2Txparity";
            this.UART2Txparity.Size = new System.Drawing.Size(100, 20);
            this.UART2Txparity.TabIndex = 60;
            this.UART2Txparity.TextChanged += new System.EventHandler(this.UART2Txparity_TextChanged);
            // 
            // UART2Txtimeout
            // 
            this.UART2Txtimeout.Location = new System.Drawing.Point(148, 253);
            this.UART2Txtimeout.Name = "UART2Txtimeout";
            this.UART2Txtimeout.Size = new System.Drawing.Size(100, 20);
            this.UART2Txtimeout.TabIndex = 59;
            this.UART2Txtimeout.TextChanged += new System.EventHandler(this.UART2Txtimeout_TextChanged);
            // 
            // UART2Txdatabits
            // 
            this.UART2Txdatabits.Location = new System.Drawing.Point(148, 201);
            this.UART2Txdatabits.Name = "UART2Txdatabits";
            this.UART2Txdatabits.Size = new System.Drawing.Size(100, 20);
            this.UART2Txdatabits.TabIndex = 58;
            this.UART2Txdatabits.TextChanged += new System.EventHandler(this.UART2Txdatabits_TextChanged);
            // 
            // UART2Txbaud
            // 
            this.UART2Txbaud.Location = new System.Drawing.Point(148, 173);
            this.UART2Txbaud.Name = "UART2Txbaud";
            this.UART2Txbaud.Size = new System.Drawing.Size(100, 20);
            this.UART2Txbaud.TabIndex = 57;
            this.UART2Txbaud.TextChanged += new System.EventHandler(this.UART2Txbaud_TextChanged);
            // 
            // UART2Txchannel
            // 
            this.UART2Txchannel.Location = new System.Drawing.Point(148, 144);
            this.UART2Txchannel.Name = "UART2Txchannel";
            this.UART2Txchannel.Size = new System.Drawing.Size(100, 20);
            this.UART2Txchannel.TabIndex = 56;
            this.UART2Txchannel.TextChanged += new System.EventHandler(this.UART2Txchannel_TextChanged);
            // 
            // UART2Rxparity
            // 
            this.UART2Rxparity.Location = new System.Drawing.Point(148, 117);
            this.UART2Rxparity.Name = "UART2Rxparity";
            this.UART2Rxparity.Size = new System.Drawing.Size(100, 20);
            this.UART2Rxparity.TabIndex = 55;
            this.UART2Rxparity.TextChanged += new System.EventHandler(this.UART2Rxparity_TextChanged);
            // 
            // UART2Rxdatabits
            // 
            this.UART2Rxdatabits.Location = new System.Drawing.Point(148, 91);
            this.UART2Rxdatabits.Name = "UART2Rxdatabits";
            this.UART2Rxdatabits.Size = new System.Drawing.Size(100, 20);
            this.UART2Rxdatabits.TabIndex = 54;
            this.UART2Rxdatabits.TextChanged += new System.EventHandler(this.UART2Rxdatabits_TextChanged);
            // 
            // UART2Rxbaud
            // 
            this.UART2Rxbaud.Location = new System.Drawing.Point(148, 68);
            this.UART2Rxbaud.Name = "UART2Rxbaud";
            this.UART2Rxbaud.Size = new System.Drawing.Size(100, 20);
            this.UART2Rxbaud.TabIndex = 53;
            this.UART2Rxbaud.TextChanged += new System.EventHandler(this.UART2Rxbaud_TextChanged);
            // 
            // UART2protocol
            // 
            this.UART2protocol.FormattingEnabled = true;
            this.UART2protocol.Items.AddRange(new object[] {
            "GCS",
            "PTZ",
            "VIDEO",
            "STAMP",
            "MRA",
            "VD170",
            "COM Tunnel",
            "MDL Lasear Ace",
            "CM 160",
            "HMR 3300",
            "Transponder",
            "CSV",
            "ECU",
            "FCB Camera",
            "pegasus Actuator",
            "Loopback",
            "TASE Gimbal",
            "Tiger Eye Gimbal",
            "Mini SSC 11",
            "GLT 240128 LCD",
            "MHX 2400 Radio",
            "XRX PCA5",
            "External GPS",
            "Cross bow IM 4440",
            "Sagetch Transponder",
            "DST Gimble",
            "DSM Busfutaba Reciever",
            "I2I -3 (DRS)",
            "Next Vision Micro",
            "XSens TM Ti 1mu",
            "Hemisphere vs 101",
            "DP - 12X"});
            this.UART2protocol.Location = new System.Drawing.Point(148, 21);
            this.UART2protocol.Name = "UART2protocol";
            this.UART2protocol.Size = new System.Drawing.Size(100, 21);
            this.UART2protocol.TabIndex = 51;
            this.UART2protocol.SelectedIndexChanged += new System.EventHandler(this.UART2protocol_SelectedIndexChanged);
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.UART1Rxchannel);
            this.groupBox17.Controls.Add(this.UART1disablebinding);
            this.groupBox17.Controls.Add(this.UART1protocolfield);
            this.groupBox17.Controls.Add(this.UART1Txparity);
            this.groupBox17.Controls.Add(this.UART1Txtimeout);
            this.groupBox17.Controls.Add(this.UART1Txbatabits);
            this.groupBox17.Controls.Add(this.UART1Txbaud);
            this.groupBox17.Controls.Add(this.UART1Txchannel);
            this.groupBox17.Controls.Add(this.UART1Rxparity);
            this.groupBox17.Controls.Add(this.UART1Rxdatabits);
            this.groupBox17.Controls.Add(this.UART1Rxbaud);
            this.groupBox17.Controls.Add(this.UART1protocol);
            this.groupBox17.Controls.Add(this.label116);
            this.groupBox17.Controls.Add(this.label115);
            this.groupBox17.Controls.Add(this.label114);
            this.groupBox17.Controls.Add(this.label113);
            this.groupBox17.Controls.Add(this.label112);
            this.groupBox17.Controls.Add(this.label111);
            this.groupBox17.Controls.Add(this.label110);
            this.groupBox17.Controls.Add(this.label109);
            this.groupBox17.Controls.Add(this.label108);
            this.groupBox17.Controls.Add(this.label107);
            this.groupBox17.Controls.Add(this.label106);
            this.groupBox17.Controls.Add(this.label105);
            this.groupBox17.Location = new System.Drawing.Point(311, 21);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(241, 356);
            this.groupBox17.TabIndex = 3;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "UART1";
            // 
            // UART1Rxchannel
            // 
            this.UART1Rxchannel.Location = new System.Drawing.Point(141, 45);
            this.UART1Rxchannel.Name = "UART1Rxchannel";
            this.UART1Rxchannel.Size = new System.Drawing.Size(34, 20);
            this.UART1Rxchannel.TabIndex = 39;
            this.UART1Rxchannel.TextChanged += new System.EventHandler(this.UART1Rxchannel_TextChanged);
            // 
            // UART1disablebinding
            // 
            this.UART1disablebinding.AutoSize = true;
            this.UART1disablebinding.Location = new System.Drawing.Point(141, 309);
            this.UART1disablebinding.Name = "UART1disablebinding";
            this.UART1disablebinding.Size = new System.Drawing.Size(15, 14);
            this.UART1disablebinding.TabIndex = 38;
            this.UART1disablebinding.UseVisualStyleBackColor = true;
            this.UART1disablebinding.CheckedChanged += new System.EventHandler(this.UART1disablebinding_CheckedChanged);
            // 
            // UART1protocolfield
            // 
            this.UART1protocolfield.FormattingEnabled = true;
            this.UART1protocolfield.Items.AddRange(new object[] {
            "Scratch",
            "Protocol",
            "Custom"});
            this.UART1protocolfield.Location = new System.Drawing.Point(141, 276);
            this.UART1protocolfield.Name = "UART1protocolfield";
            this.UART1protocolfield.Size = new System.Drawing.Size(100, 21);
            this.UART1protocolfield.TabIndex = 37;
            this.UART1protocolfield.SelectedIndexChanged += new System.EventHandler(this.UART1protocolfield_SelectedIndexChanged);
            // 
            // UART1Txparity
            // 
            this.UART1Txparity.Location = new System.Drawing.Point(141, 224);
            this.UART1Txparity.Name = "UART1Txparity";
            this.UART1Txparity.Size = new System.Drawing.Size(100, 20);
            this.UART1Txparity.TabIndex = 36;
            this.UART1Txparity.TextChanged += new System.EventHandler(this.UART1Txparity_TextChanged);
            // 
            // UART1Txtimeout
            // 
            this.UART1Txtimeout.Location = new System.Drawing.Point(141, 250);
            this.UART1Txtimeout.Name = "UART1Txtimeout";
            this.UART1Txtimeout.Size = new System.Drawing.Size(100, 20);
            this.UART1Txtimeout.TabIndex = 35;
            this.UART1Txtimeout.TextChanged += new System.EventHandler(this.textBox18_TextChanged);
            // 
            // UART1Txbatabits
            // 
            this.UART1Txbatabits.Location = new System.Drawing.Point(141, 198);
            this.UART1Txbatabits.Name = "UART1Txbatabits";
            this.UART1Txbatabits.Size = new System.Drawing.Size(100, 20);
            this.UART1Txbatabits.TabIndex = 34;
            this.UART1Txbatabits.TextChanged += new System.EventHandler(this.UART1Txbatabits_TextChanged);
            // 
            // UART1Txbaud
            // 
            this.UART1Txbaud.Location = new System.Drawing.Point(141, 172);
            this.UART1Txbaud.Name = "UART1Txbaud";
            this.UART1Txbaud.Size = new System.Drawing.Size(100, 20);
            this.UART1Txbaud.TabIndex = 33;
            this.UART1Txbaud.TextChanged += new System.EventHandler(this.UART1Txbaud_TextChanged);
            // 
            // UART1Txchannel
            // 
            this.UART1Txchannel.Location = new System.Drawing.Point(141, 146);
            this.UART1Txchannel.Name = "UART1Txchannel";
            this.UART1Txchannel.Size = new System.Drawing.Size(100, 20);
            this.UART1Txchannel.TabIndex = 32;
            this.UART1Txchannel.TextChanged += new System.EventHandler(this.UART1Txchannel_TextChanged);
            // 
            // UART1Rxparity
            // 
            this.UART1Rxparity.Location = new System.Drawing.Point(141, 120);
            this.UART1Rxparity.Name = "UART1Rxparity";
            this.UART1Rxparity.Size = new System.Drawing.Size(100, 20);
            this.UART1Rxparity.TabIndex = 31;
            this.UART1Rxparity.TextChanged += new System.EventHandler(this.UART1Rxparity_TextChanged);
            // 
            // UART1Rxdatabits
            // 
            this.UART1Rxdatabits.Location = new System.Drawing.Point(141, 94);
            this.UART1Rxdatabits.Name = "UART1Rxdatabits";
            this.UART1Rxdatabits.Size = new System.Drawing.Size(100, 20);
            this.UART1Rxdatabits.TabIndex = 30;
            this.UART1Rxdatabits.TextChanged += new System.EventHandler(this.UART1Rxdatabits_TextChanged);
            // 
            // UART1Rxbaud
            // 
            this.UART1Rxbaud.Location = new System.Drawing.Point(141, 68);
            this.UART1Rxbaud.Name = "UART1Rxbaud";
            this.UART1Rxbaud.Size = new System.Drawing.Size(100, 20);
            this.UART1Rxbaud.TabIndex = 29;
            this.UART1Rxbaud.TextChanged += new System.EventHandler(this.textBox12_TextChanged);
            // 
            // UART1protocol
            // 
            this.UART1protocol.FormattingEnabled = true;
            this.UART1protocol.Items.AddRange(new object[] {
            "GCS",
            "PTZ",
            "VIDEO",
            "STAMP",
            "MRA",
            "VD170",
            "COM Tunnel",
            "MDL Lasear Ace",
            "CM 160",
            "HMR 3300",
            "Transponder",
            "CSV",
            "ECU",
            "FCB Camera",
            "pegasus Actuator",
            "Loopback",
            "TASE Gimbal",
            "Tiger Eye Gimbal",
            "Mini SSC 11",
            "GLT 240128 LCD",
            "MHX 2400 Radio",
            "XRX PCA5",
            "External GPS",
            "Cross bow IM 4440",
            "Sagetch Transponder",
            "DST Gimble",
            "DSM Busfutaba Reciever",
            "I2I -3 (DRS)",
            "Next Vision Micro",
            "XSens TM Ti 1mu",
            "Hemisphere vs 101",
            "DP - 12X"});
            this.UART1protocol.Location = new System.Drawing.Point(141, 18);
            this.UART1protocol.Name = "UART1protocol";
            this.UART1protocol.Size = new System.Drawing.Size(100, 21);
            this.UART1protocol.TabIndex = 26;
            this.UART1protocol.SelectedIndexChanged += new System.EventHandler(this.UART1protocol_SelectedIndexChanged);
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(6, 310);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(80, 13);
            this.label116.TabIndex = 11;
            this.label116.Text = "Disable Binding";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(6, 253);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(80, 13);
            this.label115.TabIndex = 10;
            this.label115.Text = "ModemTimeout";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(6, 278);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(71, 13);
            this.label114.TabIndex = 9;
            this.label114.Text = "Protocol Field";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(6, 227);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(48, 13);
            this.label113.TabIndex = 8;
            this.label113.Text = "Tx Parity";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(6, 201);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(90, 13);
            this.label112.TabIndex = 7;
            this.label112.Text = "Tx Num Data Bits";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(6, 176);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(47, 13);
            this.label111.TabIndex = 6;
            this.label111.Text = "Tx Baud";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(6, 147);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(61, 13);
            this.label110.TabIndex = 5;
            this.label110.Text = "Tx Channel";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(6, 123);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(49, 13);
            this.label109.TabIndex = 4;
            this.label109.Text = "Rx Parity";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(6, 98);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(91, 13);
            this.label108.TabIndex = 3;
            this.label108.Text = "Rx Num Data Bits";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(6, 75);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(48, 13);
            this.label107.TabIndex = 2;
            this.label107.Text = "Rx Baud";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(6, 48);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(62, 13);
            this.label106.TabIndex = 1;
            this.label106.Text = "Rx Channel";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 22);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(46, 13);
            this.label105.TabIndex = 0;
            this.label105.Text = "Protocol";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.EnableBindingfeature);
            this.groupBox16.Controls.Add(this.label139);
            this.groupBox16.Location = new System.Drawing.Point(15, 544);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(258, 62);
            this.groupBox16.TabIndex = 2;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Binding";
            // 
            // EnableBindingfeature
            // 
            this.EnableBindingfeature.AutoSize = true;
            this.EnableBindingfeature.Location = new System.Drawing.Point(185, 33);
            this.EnableBindingfeature.Name = "EnableBindingfeature";
            this.EnableBindingfeature.Size = new System.Drawing.Size(15, 14);
            this.EnableBindingfeature.TabIndex = 40;
            this.EnableBindingfeature.UseVisualStyleBackColor = true;
            this.EnableBindingfeature.CheckedChanged += new System.EventHandler(this.EnableBindingfeature_CheckedChanged);
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(10, 33);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(117, 13);
            this.label139.TabIndex = 14;
            this.label139.Text = "Enable Binding Feature";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.Vrsheadercommands);
            this.groupBox15.Controls.Add(this.Vrsfootercommands);
            this.groupBox15.Controls.Add(this.label138);
            this.groupBox15.Controls.Add(this.label137);
            this.groupBox15.Location = new System.Drawing.Point(15, 429);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(258, 104);
            this.groupBox15.TabIndex = 1;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "VRS Locking";
            // 
            // Vrsheadercommands
            // 
            this.Vrsheadercommands.Location = new System.Drawing.Point(157, 24);
            this.Vrsheadercommands.Name = "Vrsheadercommands";
            this.Vrsheadercommands.Size = new System.Drawing.Size(58, 20);
            this.Vrsheadercommands.TabIndex = 41;
            this.Vrsheadercommands.TextChanged += new System.EventHandler(this.Vrsheadercommands_TextChanged);
            // 
            // Vrsfootercommands
            // 
            this.Vrsfootercommands.AutoSize = true;
            this.Vrsfootercommands.Location = new System.Drawing.Point(174, 61);
            this.Vrsfootercommands.Name = "Vrsfootercommands";
            this.Vrsfootercommands.Size = new System.Drawing.Size(15, 14);
            this.Vrsfootercommands.TabIndex = 40;
            this.Vrsfootercommands.UseVisualStyleBackColor = true;
            this.Vrsfootercommands.CheckedChanged += new System.EventHandler(this.Vrsfootercommands_CheckedChanged);
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(10, 62);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(124, 13);
            this.label138.TabIndex = 15;
            this.label138.Text = "Lock Footers Commands";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(10, 30);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(124, 13);
            this.label137.TabIndex = 14;
            this.label137.Text = "Lock Header Commands";
            this.label137.Click += new System.EventHandler(this.label137_Click);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.Gcschecksum);
            this.groupBox14.Controls.Add(this.GcsParity);
            this.groupBox14.Controls.Add(this.Gcsflowcontrol);
            this.groupBox14.Controls.Add(this.Gcsbaud);
            this.groupBox14.Controls.Add(this.GcsStrenghtchannel);
            this.groupBox14.Controls.Add(this.Gcsenabledialing);
            this.groupBox14.Controls.Add(this.Gcsphonesecond);
            this.groupBox14.Controls.Add(this.Gcsphonefirst);
            this.groupBox14.Controls.Add(this.Gcsidentifier);
            this.groupBox14.Controls.Add(this.GcsStrenghtminimum);
            this.groupBox14.Controls.Add(this.GcsCommandrepeat);
            this.groupBox14.Controls.Add(this.GcsPadCharacter);
            this.groupBox14.Controls.Add(this.Gcsintialreport);
            this.groupBox14.Controls.Add(this.Gcstimeount);
            this.groupBox14.Controls.Add(this.lable34);
            this.groupBox14.Controls.Add(this.label103);
            this.groupBox14.Controls.Add(this.lable12);
            this.groupBox14.Controls.Add(this.lable33);
            this.groupBox14.Controls.Add(this.lable44);
            this.groupBox14.Controls.Add(this.lable66);
            this.groupBox14.Controls.Add(this.label80);
            this.groupBox14.Controls.Add(this.lable3);
            this.groupBox14.Controls.Add(this.label68);
            this.groupBox14.Controls.Add(this.lable);
            this.groupBox14.Controls.Add(this.label66);
            this.groupBox14.Controls.Add(this.label65);
            this.groupBox14.Controls.Add(this.label64);
            this.groupBox14.Controls.Add(this.label63);
            this.groupBox14.Location = new System.Drawing.Point(15, 21);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(270, 402);
            this.groupBox14.TabIndex = 0;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "GCS";
            // 
            // Gcschecksum
            // 
            this.Gcschecksum.AutoSize = true;
            this.Gcschecksum.Location = new System.Drawing.Point(157, 102);
            this.Gcschecksum.Name = "Gcschecksum";
            this.Gcschecksum.Size = new System.Drawing.Size(15, 14);
            this.Gcschecksum.TabIndex = 27;
            this.Gcschecksum.UseVisualStyleBackColor = true;
            this.Gcschecksum.CheckedChanged += new System.EventHandler(this.Gcschecksum_CheckedChanged);
            // 
            // GcsParity
            // 
            this.GcsParity.FormattingEnabled = true;
            this.GcsParity.Items.AddRange(new object[] {
            "None",
            "Even",
            "Odd"});
            this.GcsParity.Location = new System.Drawing.Point(157, 75);
            this.GcsParity.Name = "GcsParity";
            this.GcsParity.Size = new System.Drawing.Size(100, 21);
            this.GcsParity.TabIndex = 26;
            this.GcsParity.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // Gcsflowcontrol
            // 
            this.Gcsflowcontrol.FormattingEnabled = true;
            this.Gcsflowcontrol.Items.AddRange(new object[] {
            "None",
            "Xon/Xoff"});
            this.Gcsflowcontrol.Location = new System.Drawing.Point(157, 48);
            this.Gcsflowcontrol.Name = "Gcsflowcontrol";
            this.Gcsflowcontrol.Size = new System.Drawing.Size(100, 21);
            this.Gcsflowcontrol.TabIndex = 25;
            this.Gcsflowcontrol.SelectedIndexChanged += new System.EventHandler(this.Gcsflowcontrol_SelectedIndexChanged);
            // 
            // Gcsbaud
            // 
            this.Gcsbaud.Location = new System.Drawing.Point(157, 21);
            this.Gcsbaud.Name = "Gcsbaud";
            this.Gcsbaud.Size = new System.Drawing.Size(100, 20);
            this.Gcsbaud.TabIndex = 24;
            this.Gcsbaud.TextChanged += new System.EventHandler(this.Gcsbaud_TextChanged);
            // 
            // GcsStrenghtchannel
            // 
            this.GcsStrenghtchannel.Location = new System.Drawing.Point(157, 128);
            this.GcsStrenghtchannel.Name = "GcsStrenghtchannel";
            this.GcsStrenghtchannel.Size = new System.Drawing.Size(100, 20);
            this.GcsStrenghtchannel.TabIndex = 23;
            this.GcsStrenghtchannel.TextChanged += new System.EventHandler(this.GcsStrenghtchannel_TextChanged);
            // 
            // Gcsenabledialing
            // 
            this.Gcsenabledialing.Location = new System.Drawing.Point(157, 350);
            this.Gcsenabledialing.Name = "Gcsenabledialing";
            this.Gcsenabledialing.Size = new System.Drawing.Size(100, 20);
            this.Gcsenabledialing.TabIndex = 22;
            this.Gcsenabledialing.TextChanged += new System.EventHandler(this.Gcsenabledialing_TextChanged);
            // 
            // Gcsphonesecond
            // 
            this.Gcsphonesecond.Location = new System.Drawing.Point(157, 320);
            this.Gcsphonesecond.Name = "Gcsphonesecond";
            this.Gcsphonesecond.Size = new System.Drawing.Size(100, 20);
            this.Gcsphonesecond.TabIndex = 21;
            this.Gcsphonesecond.TextChanged += new System.EventHandler(this.Gcsphonesecond_TextChanged);
            // 
            // Gcsphonefirst
            // 
            this.Gcsphonefirst.Location = new System.Drawing.Point(157, 294);
            this.Gcsphonefirst.Name = "Gcsphonefirst";
            this.Gcsphonefirst.Size = new System.Drawing.Size(100, 20);
            this.Gcsphonefirst.TabIndex = 20;
            this.Gcsphonefirst.TextChanged += new System.EventHandler(this.Gcsphonefirst_TextChanged);
            // 
            // Gcsidentifier
            // 
            this.Gcsidentifier.Location = new System.Drawing.Point(157, 266);
            this.Gcsidentifier.Name = "Gcsidentifier";
            this.Gcsidentifier.Size = new System.Drawing.Size(100, 20);
            this.Gcsidentifier.TabIndex = 19;
            this.Gcsidentifier.TextChanged += new System.EventHandler(this.Gcsidentifier_TextChanged);
            // 
            // GcsStrenghtminimum
            // 
            this.GcsStrenghtminimum.Location = new System.Drawing.Point(157, 159);
            this.GcsStrenghtminimum.Name = "GcsStrenghtminimum";
            this.GcsStrenghtminimum.Size = new System.Drawing.Size(100, 20);
            this.GcsStrenghtminimum.TabIndex = 18;
            this.GcsStrenghtminimum.TextChanged += new System.EventHandler(this.GcsStrenghtminimum_TextChanged);
            // 
            // GcsCommandrepeat
            // 
            this.GcsCommandrepeat.Location = new System.Drawing.Point(157, 187);
            this.GcsCommandrepeat.Name = "GcsCommandrepeat";
            this.GcsCommandrepeat.Size = new System.Drawing.Size(100, 20);
            this.GcsCommandrepeat.TabIndex = 17;
            this.GcsCommandrepeat.TextChanged += new System.EventHandler(this.GcsCommandrepeat_TextChanged);
            // 
            // GcsPadCharacter
            // 
            this.GcsPadCharacter.Location = new System.Drawing.Point(157, 215);
            this.GcsPadCharacter.Name = "GcsPadCharacter";
            this.GcsPadCharacter.Size = new System.Drawing.Size(100, 20);
            this.GcsPadCharacter.TabIndex = 16;
            this.GcsPadCharacter.TextChanged += new System.EventHandler(this.GcsPadCharacter_TextChanged);
            // 
            // Gcsintialreport
            // 
            this.Gcsintialreport.Location = new System.Drawing.Point(157, 241);
            this.Gcsintialreport.Name = "Gcsintialreport";
            this.Gcsintialreport.Size = new System.Drawing.Size(100, 20);
            this.Gcsintialreport.TabIndex = 15;
            this.Gcsintialreport.TextChanged += new System.EventHandler(this.Gcsintialreport_TextChanged);
            // 
            // Gcstimeount
            // 
            this.Gcstimeount.Location = new System.Drawing.Point(157, 376);
            this.Gcstimeount.Name = "Gcstimeount";
            this.Gcstimeount.Size = new System.Drawing.Size(100, 20);
            this.Gcstimeount.TabIndex = 14;
            this.Gcstimeount.TextChanged += new System.EventHandler(this.Gcstimeount_TextChanged);
            // 
            // lable34
            // 
            this.lable34.AutoSize = true;
            this.lable34.Location = new System.Drawing.Point(10, 377);
            this.lable34.Name = "lable34";
            this.lable34.Size = new System.Drawing.Size(83, 13);
            this.lable34.TabIndex = 13;
            this.lable34.Text = "Modem Timeout";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(10, 354);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(75, 13);
            this.label103.TabIndex = 12;
            this.label103.Text = "Enable Dialing";
            // 
            // lable12
            // 
            this.lable12.AutoSize = true;
            this.lable12.Location = new System.Drawing.Point(10, 324);
            this.lable12.Name = "lable12";
            this.lable12.Size = new System.Drawing.Size(95, 13);
            this.lable12.TabIndex = 11;
            this.lable12.Text = "Phone No.Second";
            // 
            // lable33
            // 
            this.lable33.AutoSize = true;
            this.lable33.Location = new System.Drawing.Point(10, 294);
            this.lable33.Name = "lable33";
            this.lable33.Size = new System.Drawing.Size(77, 13);
            this.lable33.TabIndex = 10;
            this.lable33.Text = "Phone No.First";
            // 
            // lable44
            // 
            this.lable44.AutoSize = true;
            this.lable44.Location = new System.Drawing.Point(10, 266);
            this.lable44.Name = "lable44";
            this.lable44.Size = new System.Drawing.Size(91, 13);
            this.lable44.TabIndex = 9;
            this.lable44.Text = "Autopilot Identifier";
            // 
            // lable66
            // 
            this.lable66.AutoSize = true;
            this.lable66.Location = new System.Drawing.Point(10, 240);
            this.lable66.Name = "lable66";
            this.lable66.Size = new System.Drawing.Size(96, 13);
            this.lable66.TabIndex = 8;
            this.lable66.Text = "Force Initial Report";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(10, 214);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(75, 13);
            this.label80.TabIndex = 7;
            this.label80.Text = "Pad Character";
            // 
            // lable3
            // 
            this.lable3.AutoSize = true;
            this.lable3.Location = new System.Drawing.Point(10, 187);
            this.lable3.Name = "lable3";
            this.lable3.Size = new System.Drawing.Size(116, 13);
            this.lable3.TabIndex = 6;
            this.lable3.Text = "Text Command Repeat";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(10, 131);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(121, 13);
            this.label68.TabIndex = 5;
            this.label68.Text = "Signal Strenght Channel";
            // 
            // lable
            // 
            this.lable.AutoSize = true;
            this.lable.Location = new System.Drawing.Point(10, 159);
            this.lable.Name = "lable";
            this.lable.Size = new System.Drawing.Size(123, 13);
            this.lable.TabIndex = 4;
            this.lable.Text = "Signal Strenght Minimum";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(10, 102);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(111, 13);
            this.label66.TabIndex = 3;
            this.label66.Text = "32bit  CRC Checksum";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(10, 75);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(33, 13);
            this.label65.TabIndex = 2;
            this.label65.Text = "Parity";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(10, 48);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(65, 13);
            this.label64.TabIndex = 1;
            this.label64.Text = "Flow Control";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(10, 22);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(32, 13);
            this.label63.TabIndex = 0;
            this.label63.Text = "Baud";
            // 
            // GPS
            // 
            this.GPS.Controls.Add(this.Novatelgroup);
            this.GPS.Controls.Add(this.SBASgroup);
            this.GPS.Controls.Add(this.Ubloxgroup);
            this.GPS.Controls.Add(this.groupBox21);
            this.GPS.Controls.Add(this.groupBox20);
            this.GPS.Location = new System.Drawing.Point(4, 22);
            this.GPS.Name = "GPS";
            this.GPS.Padding = new System.Windows.Forms.Padding(3);
            this.GPS.Size = new System.Drawing.Size(925, 703);
            this.GPS.TabIndex = 4;
            this.GPS.Text = "GPS";
            this.GPS.UseVisualStyleBackColor = true;
            this.GPS.Click += new System.EventHandler(this.GPS_Click);
            // 
            // Novatelgroup
            // 
            this.Novatelgroup.Controls.Add(this.Novatelbasestation);
            this.Novatelgroup.Controls.Add(this.Novatelenablediableothe);
            this.Novatelgroup.Controls.Add(this.Novatelminimumspeed);
            this.Novatelgroup.Controls.Add(this.Novatelusel1flaot);
            this.Novatelgroup.Controls.Add(this.Novatelenablecorrection);
            this.Novatelgroup.Controls.Add(this.Novatelenablediasblenovatel);
            this.Novatelgroup.Location = new System.Drawing.Point(297, 393);
            this.Novatelgroup.Name = "Novatelgroup";
            this.Novatelgroup.Size = new System.Drawing.Size(481, 227);
            this.Novatelgroup.TabIndex = 2;
            this.Novatelgroup.TabStop = false;
            this.Novatelgroup.Text = "Novatel Align Settings";
            this.Novatelgroup.Visible = false;
            this.Novatelgroup.Enter += new System.EventHandler(this.Novatelgroup_Enter);
            // 
            // Novatelbasestation
            // 
            this.Novatelbasestation.AutoSize = true;
            this.Novatelbasestation.Location = new System.Drawing.Point(68, 158);
            this.Novatelbasestation.Name = "Novatelbasestation";
            this.Novatelbasestation.Size = new System.Drawing.Size(309, 17);
            this.Novatelbasestation.TabIndex = 13;
            this.Novatelbasestation.Text = "Base Station Speed and heading are transmitted to autopilot";
            this.Novatelbasestation.UseVisualStyleBackColor = true;
            this.Novatelbasestation.CheckedChanged += new System.EventHandler(this.Novatelbasestation_CheckedChanged);
            // 
            // Novatelenablediableothe
            // 
            this.Novatelenablediableothe.AutoSize = true;
            this.Novatelenablediableothe.Location = new System.Drawing.Point(68, 189);
            this.Novatelenablediableothe.Name = "Novatelenablediableothe";
            this.Novatelenablediableothe.Size = new System.Drawing.Size(342, 17);
            this.Novatelenablediableothe.TabIndex = 12;
            this.Novatelenablediableothe.Text = "Enable Novatel Align Heading (Disable other Novatel Align options)";
            this.Novatelenablediableothe.UseVisualStyleBackColor = true;
            this.Novatelenablediableothe.CheckedChanged += new System.EventHandler(this.Novatelenablediableothe_CheckedChanged);
            // 
            // Novatelminimumspeed
            // 
            this.Novatelminimumspeed.AutoSize = true;
            this.Novatelminimumspeed.Location = new System.Drawing.Point(68, 91);
            this.Novatelminimumspeed.Name = "Novatelminimumspeed";
            this.Novatelminimumspeed.Size = new System.Drawing.Size(337, 17);
            this.Novatelminimumspeed.TabIndex = 11;
            this.Novatelminimumspeed.Text = "Minimum Speed for treating base as moving is 9ft/s insteadof 3ft/s";
            this.Novatelminimumspeed.UseVisualStyleBackColor = true;
            this.Novatelminimumspeed.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // Novatelusel1flaot
            // 
            this.Novatelusel1flaot.AutoSize = true;
            this.Novatelusel1flaot.Location = new System.Drawing.Point(68, 124);
            this.Novatelusel1flaot.Name = "Novatelusel1flaot";
            this.Novatelusel1flaot.Size = new System.Drawing.Size(416, 17);
            this.Novatelusel1flaot.TabIndex = 10;
            this.Novatelusel1flaot.Text = "Use L1 float readings if the standard deviation is less than the defined  in fiel" +
    "d 1590";
            this.Novatelusel1flaot.UseVisualStyleBackColor = true;
            this.Novatelusel1flaot.CheckedChanged += new System.EventHandler(this.Novatelusel1flaot_CheckedChanged);
            // 
            // Novatelenablecorrection
            // 
            this.Novatelenablecorrection.AutoSize = true;
            this.Novatelenablecorrection.Location = new System.Drawing.Point(68, 60);
            this.Novatelenablecorrection.Name = "Novatelenablecorrection";
            this.Novatelenablecorrection.Size = new System.Drawing.Size(292, 17);
            this.Novatelenablecorrection.TabIndex = 9;
            this.Novatelenablecorrection.Text = "Enable Correction the Kalmanfilter with the align position ";
            this.Novatelenablecorrection.UseVisualStyleBackColor = true;
            this.Novatelenablecorrection.CheckedChanged += new System.EventHandler(this.Novatelenablecorrection_CheckedChanged);
            // 
            // Novatelenablediasblenovatel
            // 
            this.Novatelenablediasblenovatel.AutoSize = true;
            this.Novatelenablediasblenovatel.Location = new System.Drawing.Point(68, 30);
            this.Novatelenablediasblenovatel.Name = "Novatelenablediasblenovatel";
            this.Novatelenablediasblenovatel.Size = new System.Drawing.Size(278, 17);
            this.Novatelenablediasblenovatel.TabIndex = 8;
            this.Novatelenablediasblenovatel.Text = "Enable Novatel Align (Disable Novatel Align Heading)";
            this.Novatelenablediasblenovatel.UseVisualStyleBackColor = true;
            this.Novatelenablediasblenovatel.CheckedChanged += new System.EventHandler(this.Novatelenablediasblenovatel_CheckedChanged);
            // 
            // SBASgroup
            // 
            this.SBASgroup.Controls.Add(this.SBASsatellite);
            this.SBASgroup.Controls.Add(this.SBASnumberofchannel);
            this.SBASgroup.Controls.Add(this.SBASenterPRNs);
            this.SBASgroup.Controls.Add(this.label150);
            this.SBASgroup.Controls.Add(this.label149);
            this.SBASgroup.Controls.Add(this.label148);
            this.SBASgroup.Controls.Add(this.SBASlable);
            this.SBASgroup.Controls.Add(this.SBASusetestmode);
            this.SBASgroup.Controls.Add(this.SBASapplyintegrity);
            this.SBASgroup.Controls.Add(this.SBASenable);
            this.SBASgroup.Location = new System.Drawing.Point(297, 126);
            this.SBASgroup.Name = "SBASgroup";
            this.SBASgroup.Size = new System.Drawing.Size(481, 261);
            this.SBASgroup.TabIndex = 3;
            this.SBASgroup.TabStop = false;
            this.SBASgroup.Text = "SBAS Setting";
            // 
            // SBASsatellite
            // 
            this.SBASsatellite.FormattingEnabled = true;
            this.SBASsatellite.Items.AddRange(new object[] {
            "Autoscan",
            "WASS",
            "EGNOS",
            "Custom"});
            this.SBASsatellite.Location = new System.Drawing.Point(273, 146);
            this.SBASsatellite.Name = "SBASsatellite";
            this.SBASsatellite.Size = new System.Drawing.Size(103, 21);
            this.SBASsatellite.TabIndex = 12;
            this.SBASsatellite.SelectedIndexChanged += new System.EventHandler(this.SBASsatellite_SelectedIndexChanged);
            // 
            // SBASnumberofchannel
            // 
            this.SBASnumberofchannel.FormattingEnabled = true;
            this.SBASnumberofchannel.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "unknown"});
            this.SBASnumberofchannel.Location = new System.Drawing.Point(273, 115);
            this.SBASnumberofchannel.Name = "SBASnumberofchannel";
            this.SBASnumberofchannel.Size = new System.Drawing.Size(103, 21);
            this.SBASnumberofchannel.TabIndex = 13;
            this.SBASnumberofchannel.SelectedIndexChanged += new System.EventHandler(this.SBASnumberofchannel_SelectedIndexChanged);
            // 
            // SBASenterPRNs
            // 
            this.SBASenterPRNs.Location = new System.Drawing.Point(68, 207);
            this.SBASenterPRNs.Name = "SBASenterPRNs";
            this.SBASenterPRNs.Size = new System.Drawing.Size(350, 20);
            this.SBASenterPRNs.TabIndex = 7;
            this.SBASenterPRNs.TextChanged += new System.EventHandler(this.SBASenterPRNs_TextChanged);
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(89, 233);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(404, 13);
            this.label150.TabIndex = 6;
            this.label150.Text = "Note: Separate PRN\'s with Comms (.) only PRNs  from 120  to 151 are valid        " +
    "      ";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(65, 184);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(159, 13);
            this.label149.TabIndex = 5;
            this.label149.Text = "Enter PRNs of Desired Satellites";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(65, 115);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(141, 13);
            this.label148.TabIndex = 4;
            this.label148.Text = "Number of Channel to Track";
            // 
            // SBASlable
            // 
            this.SBASlable.AutoSize = true;
            this.SBASlable.Location = new System.Drawing.Point(65, 149);
            this.SBASlable.Name = "SBASlable";
            this.SBASlable.Size = new System.Drawing.Size(123, 13);
            this.SBASlable.TabIndex = 3;
            this.SBASlable.Text = "SBAS Satellites to Track";
            // 
            // SBASusetestmode
            // 
            this.SBASusetestmode.AutoSize = true;
            this.SBASusetestmode.Location = new System.Drawing.Point(68, 73);
            this.SBASusetestmode.Name = "SBASusetestmode";
            this.SBASusetestmode.Size = new System.Drawing.Size(139, 17);
            this.SBASusetestmode.TabIndex = 2;
            this.SBASusetestmode.Text = "Use Test Mode Satellite";
            this.SBASusetestmode.UseVisualStyleBackColor = true;
            this.SBASusetestmode.CheckedChanged += new System.EventHandler(this.SBASusetestmode_CheckedChanged);
            // 
            // SBASapplyintegrity
            // 
            this.SBASapplyintegrity.AutoSize = true;
            this.SBASapplyintegrity.Location = new System.Drawing.Point(68, 50);
            this.SBASapplyintegrity.Name = "SBASapplyintegrity";
            this.SBASapplyintegrity.Size = new System.Drawing.Size(147, 17);
            this.SBASapplyintegrity.TabIndex = 1;
            this.SBASapplyintegrity.Text = "Apply Integrity Information";
            this.SBASapplyintegrity.UseVisualStyleBackColor = true;
            this.SBASapplyintegrity.CheckedChanged += new System.EventHandler(this.SBASapplyintegrity_CheckedChanged);
            // 
            // SBASenable
            // 
            this.SBASenable.AutoSize = true;
            this.SBASenable.Location = new System.Drawing.Point(68, 27);
            this.SBASenable.Name = "SBASenable";
            this.SBASenable.Size = new System.Drawing.Size(175, 17);
            this.SBASenable.TabIndex = 0;
            this.SBASenable.Text = "Enable SBAS Satellite Tracking";
            this.SBASenable.UseVisualStyleBackColor = true;
            this.SBASenable.CheckedChanged += new System.EventHandler(this.SBASenable_CheckedChanged);
            // 
            // Ubloxgroup
            // 
            this.Ubloxgroup.Controls.Add(this.ubloxdynamicmodel);
            this.Ubloxgroup.Controls.Add(this.label146);
            this.Ubloxgroup.Location = new System.Drawing.Point(297, 15);
            this.Ubloxgroup.Name = "Ubloxgroup";
            this.Ubloxgroup.Size = new System.Drawing.Size(481, 105);
            this.Ubloxgroup.TabIndex = 2;
            this.Ubloxgroup.TabStop = false;
            this.Ubloxgroup.Text = "Ublox Setting";
            // 
            // ubloxdynamicmodel
            // 
            this.ubloxdynamicmodel.FormattingEnabled = true;
            this.ubloxdynamicmodel.Items.AddRange(new object[] {
            "0-Default",
            "1-Stationary",
            "2-Pedestrian",
            "3-Automotive",
            "4-Sea",
            "5-Airbone  1G",
            "6-Airbone  2G",
            "7-Airbone  4G"});
            this.ubloxdynamicmodel.Location = new System.Drawing.Point(177, 39);
            this.ubloxdynamicmodel.Name = "ubloxdynamicmodel";
            this.ubloxdynamicmodel.Size = new System.Drawing.Size(103, 21);
            this.ubloxdynamicmodel.TabIndex = 11;
            this.ubloxdynamicmodel.SelectedIndexChanged += new System.EventHandler(this.ubloxdynamicmodel_SelectedIndexChanged);
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(28, 39);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(80, 13);
            this.label146.TabIndex = 1;
            this.label146.Text = "Dynamic Model";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.GPS2stopbits);
            this.groupBox21.Controls.Add(this.GPS2bits);
            this.groupBox21.Controls.Add(this.GPS2baud);
            this.groupBox21.Controls.Add(this.GPS2flowcontrol);
            this.groupBox21.Controls.Add(this.GPS2parity);
            this.groupBox21.Controls.Add(this.label145);
            this.groupBox21.Controls.Add(this.label144);
            this.groupBox21.Controls.Add(this.label143);
            this.groupBox21.Controls.Add(this.label142);
            this.groupBox21.Controls.Add(this.label141);
            this.groupBox21.Location = new System.Drawing.Point(28, 359);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(263, 210);
            this.groupBox21.TabIndex = 1;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "GPS II";
            // 
            // GPS2stopbits
            // 
            this.GPS2stopbits.Location = new System.Drawing.Point(160, 168);
            this.GPS2stopbits.Name = "GPS2stopbits";
            this.GPS2stopbits.Size = new System.Drawing.Size(49, 20);
            this.GPS2stopbits.TabIndex = 17;
            this.GPS2stopbits.TextChanged += new System.EventHandler(this.GPS2stopbits_TextChanged);
            // 
            // GPS2bits
            // 
            this.GPS2bits.Location = new System.Drawing.Point(160, 131);
            this.GPS2bits.Name = "GPS2bits";
            this.GPS2bits.Size = new System.Drawing.Size(49, 20);
            this.GPS2bits.TabIndex = 16;
            this.GPS2bits.TextChanged += new System.EventHandler(this.GPS2bits_TextChanged);
            // 
            // GPS2baud
            // 
            this.GPS2baud.Location = new System.Drawing.Point(160, 95);
            this.GPS2baud.Name = "GPS2baud";
            this.GPS2baud.Size = new System.Drawing.Size(49, 20);
            this.GPS2baud.TabIndex = 15;
            this.GPS2baud.TextChanged += new System.EventHandler(this.GPS2baud_TextChanged);
            // 
            // GPS2flowcontrol
            // 
            this.GPS2flowcontrol.FormattingEnabled = true;
            this.GPS2flowcontrol.Items.AddRange(new object[] {
            "None",
            "Xon/Xoff"});
            this.GPS2flowcontrol.Location = new System.Drawing.Point(160, 60);
            this.GPS2flowcontrol.Name = "GPS2flowcontrol";
            this.GPS2flowcontrol.Size = new System.Drawing.Size(103, 21);
            this.GPS2flowcontrol.TabIndex = 12;
            this.GPS2flowcontrol.SelectedIndexChanged += new System.EventHandler(this.GPS2flowcontrol_SelectedIndexChanged);
            // 
            // GPS2parity
            // 
            this.GPS2parity.FormattingEnabled = true;
            this.GPS2parity.Items.AddRange(new object[] {
            "None",
            "Even",
            "Odd"});
            this.GPS2parity.Location = new System.Drawing.Point(160, 34);
            this.GPS2parity.Name = "GPS2parity";
            this.GPS2parity.Size = new System.Drawing.Size(103, 21);
            this.GPS2parity.TabIndex = 11;
            this.GPS2parity.SelectedIndexChanged += new System.EventHandler(this.GPS2parity_SelectedIndexChanged);
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(6, 99);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(32, 13);
            this.label145.TabIndex = 5;
            this.label145.Text = "Baud";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(6, 34);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(33, 13);
            this.label144.TabIndex = 4;
            this.label144.Text = "Parity";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(6, 64);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(65, 13);
            this.label143.TabIndex = 3;
            this.label143.Text = "Flow Control";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(6, 131);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(24, 13);
            this.label142.TabIndex = 2;
            this.label142.Text = "Bits";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(6, 171);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(49, 13);
            this.label141.TabIndex = 1;
            this.label141.Text = "Stop Bits";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.GPS1hemisphere);
            this.groupBox20.Controls.Add(this.GPS1kalmanfilter);
            this.groupBox20.Controls.Add(this.GPS1usegpsspeed);
            this.groupBox20.Controls.Add(this.GPS1usegpsaltitude);
            this.groupBox20.Controls.Add(this.GPS1fakegps);
            this.groupBox20.Controls.Add(this.GPS1fixrate);
            this.groupBox20.Controls.Add(this.GPS1bits);
            this.groupBox20.Controls.Add(this.GPS1baud);
            this.groupBox20.Controls.Add(this.GPS1parity);
            this.groupBox20.Controls.Add(this.GPS1type);
            this.groupBox20.Controls.Add(this.kalmanfilter);
            this.groupBox20.Controls.Add(this.hemisphere);
            this.groupBox20.Controls.Add(this.fakegps);
            this.groupBox20.Controls.Add(this.usegpsaltitude);
            this.groupBox20.Controls.Add(this.label102);
            this.groupBox20.Controls.Add(this.label101);
            this.groupBox20.Controls.Add(this.usegpsspeed);
            this.groupBox20.Controls.Add(this.label97);
            this.groupBox20.Controls.Add(this.label69);
            this.groupBox20.Controls.Add(this.label67);
            this.groupBox20.Location = new System.Drawing.Point(28, 15);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(263, 338);
            this.groupBox20.TabIndex = 0;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "GPS";
            // 
            // GPS1hemisphere
            // 
            this.GPS1hemisphere.AutoSize = true;
            this.GPS1hemisphere.Location = new System.Drawing.Point(177, 310);
            this.GPS1hemisphere.Name = "GPS1hemisphere";
            this.GPS1hemisphere.Size = new System.Drawing.Size(15, 14);
            this.GPS1hemisphere.TabIndex = 19;
            this.GPS1hemisphere.UseVisualStyleBackColor = true;
            this.GPS1hemisphere.CheckedChanged += new System.EventHandler(this.GPS1hemisphere_CheckedChanged);
            // 
            // GPS1kalmanfilter
            // 
            this.GPS1kalmanfilter.AutoSize = true;
            this.GPS1kalmanfilter.Location = new System.Drawing.Point(177, 278);
            this.GPS1kalmanfilter.Name = "GPS1kalmanfilter";
            this.GPS1kalmanfilter.Size = new System.Drawing.Size(15, 14);
            this.GPS1kalmanfilter.TabIndex = 18;
            this.GPS1kalmanfilter.UseVisualStyleBackColor = true;
            this.GPS1kalmanfilter.CheckedChanged += new System.EventHandler(this.GPS1kalmanfilter_CheckedChanged);
            // 
            // GPS1usegpsspeed
            // 
            this.GPS1usegpsspeed.AutoSize = true;
            this.GPS1usegpsspeed.Location = new System.Drawing.Point(177, 244);
            this.GPS1usegpsspeed.Name = "GPS1usegpsspeed";
            this.GPS1usegpsspeed.Size = new System.Drawing.Size(15, 14);
            this.GPS1usegpsspeed.TabIndex = 17;
            this.GPS1usegpsspeed.UseVisualStyleBackColor = true;
            this.GPS1usegpsspeed.CheckedChanged += new System.EventHandler(this.GPS1usegpsspeed_CheckedChanged);
            // 
            // GPS1usegpsaltitude
            // 
            this.GPS1usegpsaltitude.AutoSize = true;
            this.GPS1usegpsaltitude.Location = new System.Drawing.Point(177, 209);
            this.GPS1usegpsaltitude.Name = "GPS1usegpsaltitude";
            this.GPS1usegpsaltitude.Size = new System.Drawing.Size(15, 14);
            this.GPS1usegpsaltitude.TabIndex = 16;
            this.GPS1usegpsaltitude.UseVisualStyleBackColor = true;
            this.GPS1usegpsaltitude.CheckedChanged += new System.EventHandler(this.GPS1usegpsaltitude_CheckedChanged);
            // 
            // GPS1fakegps
            // 
            this.GPS1fakegps.AutoSize = true;
            this.GPS1fakegps.Location = new System.Drawing.Point(177, 173);
            this.GPS1fakegps.Name = "GPS1fakegps";
            this.GPS1fakegps.Size = new System.Drawing.Size(15, 14);
            this.GPS1fakegps.TabIndex = 15;
            this.GPS1fakegps.UseVisualStyleBackColor = true;
            this.GPS1fakegps.CheckedChanged += new System.EventHandler(this.GPS1fakegps_CheckedChanged);
            // 
            // GPS1fixrate
            // 
            this.GPS1fixrate.Location = new System.Drawing.Point(160, 144);
            this.GPS1fixrate.Name = "GPS1fixrate";
            this.GPS1fixrate.Size = new System.Drawing.Size(49, 20);
            this.GPS1fixrate.TabIndex = 14;
            this.GPS1fixrate.TextChanged += new System.EventHandler(this.GPS1fixrate_TextChanged);
            // 
            // GPS1bits
            // 
            this.GPS1bits.Location = new System.Drawing.Point(160, 108);
            this.GPS1bits.Name = "GPS1bits";
            this.GPS1bits.Size = new System.Drawing.Size(49, 20);
            this.GPS1bits.TabIndex = 13;
            this.GPS1bits.TextChanged += new System.EventHandler(this.GPS1bits_TextChanged);
            // 
            // GPS1baud
            // 
            this.GPS1baud.Location = new System.Drawing.Point(160, 80);
            this.GPS1baud.Name = "GPS1baud";
            this.GPS1baud.Size = new System.Drawing.Size(103, 20);
            this.GPS1baud.TabIndex = 12;
            this.GPS1baud.TextChanged += new System.EventHandler(this.GPS1baud_TextChanged);
            // 
            // GPS1parity
            // 
            this.GPS1parity.FormattingEnabled = true;
            this.GPS1parity.Items.AddRange(new object[] {
            "None",
            "Even",
            "Odd"});
            this.GPS1parity.Location = new System.Drawing.Point(160, 49);
            this.GPS1parity.Name = "GPS1parity";
            this.GPS1parity.Size = new System.Drawing.Size(103, 21);
            this.GPS1parity.TabIndex = 11;
            this.GPS1parity.SelectedIndexChanged += new System.EventHandler(this.GPS1parity_SelectedIndexChanged);
            // 
            // GPS1type
            // 
            this.GPS1type.FormattingEnabled = true;
            this.GPS1type.Items.AddRange(new object[] {
            "Trimble Tsip",
            "NMEA",
            "LassenLP DGPS",
            "Novatel"});
            this.GPS1type.Location = new System.Drawing.Point(160, 22);
            this.GPS1type.Name = "GPS1type";
            this.GPS1type.Size = new System.Drawing.Size(103, 21);
            this.GPS1type.TabIndex = 10;
            this.GPS1type.SelectedIndexChanged += new System.EventHandler(this.GPS1type_SelectedIndexChanged);
            // 
            // kalmanfilter
            // 
            this.kalmanfilter.AutoSize = true;
            this.kalmanfilter.Location = new System.Drawing.Point(6, 278);
            this.kalmanfilter.Name = "kalmanfilter";
            this.kalmanfilter.Size = new System.Drawing.Size(144, 13);
            this.kalmanfilter.TabIndex = 9;
            this.kalmanfilter.Text = "Set Altitute From Kalman filter";
            // 
            // hemisphere
            // 
            this.hemisphere.AutoSize = true;
            this.hemisphere.Location = new System.Drawing.Point(6, 311);
            this.hemisphere.Name = "hemisphere";
            this.hemisphere.Size = new System.Drawing.Size(171, 13);
            this.hemisphere.TabIndex = 8;
            this.hemisphere.Text = "Set Altitude From Hemisphere GPS";
            // 
            // fakegps
            // 
            this.fakegps.AutoSize = true;
            this.fakegps.Location = new System.Drawing.Point(6, 173);
            this.fakegps.Name = "fakegps";
            this.fakegps.Size = new System.Drawing.Size(56, 13);
            this.fakegps.TabIndex = 7;
            this.fakegps.Text = "Fake GPS";
            // 
            // usegpsaltitude
            // 
            this.usegpsaltitude.AutoSize = true;
            this.usegpsaltitude.Location = new System.Drawing.Point(6, 209);
            this.usegpsaltitude.Name = "usegpsaltitude";
            this.usegpsaltitude.Size = new System.Drawing.Size(89, 13);
            this.usegpsaltitude.TabIndex = 6;
            this.usegpsaltitude.Text = "Use GPS Altitude";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(6, 111);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(24, 13);
            this.label102.TabIndex = 5;
            this.label102.Text = "Bits";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(6, 52);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(33, 13);
            this.label101.TabIndex = 4;
            this.label101.Text = "Parity";
            this.label101.Click += new System.EventHandler(this.label101_Click);
            // 
            // usegpsspeed
            // 
            this.usegpsspeed.AutoSize = true;
            this.usegpsspeed.Location = new System.Drawing.Point(6, 244);
            this.usegpsspeed.Name = "usegpsspeed";
            this.usegpsspeed.Size = new System.Drawing.Size(85, 13);
            this.usegpsspeed.TabIndex = 3;
            this.usegpsspeed.Text = "Use GPS Speed";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(6, 144);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(46, 13);
            this.label97.TabIndex = 2;
            this.label97.Text = "Fix Rate";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(6, 80);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(32, 13);
            this.label69.TabIndex = 1;
            this.label69.Text = "Baud";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(6, 25);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(31, 13);
            this.label67.TabIndex = 0;
            this.label67.Text = "Type";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox25);
            this.tabPage5.Controls.Add(this.groupBox26);
            this.tabPage5.Controls.Add(this.Datalogform1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(912, 747);
            this.tabPage5.TabIndex = 5;
            this.tabPage5.Text = "Data Log";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.Datalogdonoterase);
            this.groupBox25.Controls.Add(this.Datalogenablecustom);
            this.groupBox25.Controls.Add(this.Dataloglogging);
            this.groupBox25.Location = new System.Drawing.Point(6, 6);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(663, 84);
            this.groupBox25.TabIndex = 1;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "     ";
            // 
            // Datalogdonoterase
            // 
            this.Datalogdonoterase.AutoSize = true;
            this.Datalogdonoterase.Location = new System.Drawing.Point(52, 42);
            this.Datalogdonoterase.Name = "Datalogdonoterase";
            this.Datalogdonoterase.Size = new System.Drawing.Size(279, 17);
            this.Datalogdonoterase.TabIndex = 30;
            this.Datalogdonoterase.Text = "Don\'t erase the data log Until it has been downloaded";
            this.Datalogdonoterase.UseVisualStyleBackColor = true;
            this.Datalogdonoterase.CheckedChanged += new System.EventHandler(this.Datalogdonoterase_CheckedChanged);
            // 
            // Datalogenablecustom
            // 
            this.Datalogenablecustom.AutoSize = true;
            this.Datalogenablecustom.Location = new System.Drawing.Point(52, 65);
            this.Datalogenablecustom.Name = "Datalogenablecustom";
            this.Datalogenablecustom.Size = new System.Drawing.Size(127, 17);
            this.Datalogenablecustom.TabIndex = 29;
            this.Datalogenablecustom.Text = "Enable Custom Fields";
            this.Datalogenablecustom.UseVisualStyleBackColor = true;
            this.Datalogenablecustom.CheckedChanged += new System.EventHandler(this.Datalogenablecustom_CheckedChanged_1);
            // 
            // Dataloglogging
            // 
            this.Dataloglogging.AutoSize = true;
            this.Dataloglogging.Location = new System.Drawing.Point(52, 19);
            this.Dataloglogging.Name = "Dataloglogging";
            this.Dataloglogging.Size = new System.Drawing.Size(95, 17);
            this.Dataloglogging.TabIndex = 28;
            this.Dataloglogging.Text = "30 Hz Logging";
            this.Dataloglogging.UseVisualStyleBackColor = true;
            this.Dataloglogging.CheckedChanged += new System.EventHandler(this.Dataloglogging_CheckedChanged_1);
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.button26);
            this.groupBox26.Controls.Add(this.Datalogcustomheadfltext8);
            this.groupBox26.Controls.Add(this.button27);
            this.groupBox26.Controls.Add(this.button28);
            this.groupBox26.Controls.Add(this.button29);
            this.groupBox26.Controls.Add(this.button30);
            this.groupBox26.Controls.Add(this.button31);
            this.groupBox26.Controls.Add(this.button32);
            this.groupBox26.Controls.Add(this.button33);
            this.groupBox26.Controls.Add(this.Datalogcustomheadfltext1);
            this.groupBox26.Controls.Add(this.Datalogcustomheadfltext2);
            this.groupBox26.Controls.Add(this.Datalogcustomheadfltext3);
            this.groupBox26.Controls.Add(this.Datalogcustomheadfltext4);
            this.groupBox26.Controls.Add(this.Datalogcustomheadfltext5);
            this.groupBox26.Controls.Add(this.Datalogcustomheadfltext6);
            this.groupBox26.Controls.Add(this.Datalogcustomheadfltext7);
            this.groupBox26.Controls.Add(this.Datalogcustomheadflable1);
            this.groupBox26.Controls.Add(this.Datalogcustomheadflable2);
            this.groupBox26.Controls.Add(this.Datalogcustomheadflable3);
            this.groupBox26.Controls.Add(this.Datalogcustomheadflable4);
            this.groupBox26.Controls.Add(this.Datalogcustomheadflable5);
            this.groupBox26.Controls.Add(this.Datalogcustomheadflable6);
            this.groupBox26.Controls.Add(this.Datalogcustomheadflable7);
            this.groupBox26.Controls.Add(this.Datalogcustomheadflable8);
            this.groupBox26.Location = new System.Drawing.Point(7, 425);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(855, 209);
            this.groupBox26.TabIndex = 0;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Custom Data Log Header Fields";
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(363, 188);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(26, 20);
            this.button26.TabIndex = 78;
            this.button26.Text = "?";
            this.button26.UseVisualStyleBackColor = true;
            // 
            // Datalogcustomheadfltext8
            // 
            this.Datalogcustomheadfltext8.Location = new System.Drawing.Point(266, 189);
            this.Datalogcustomheadfltext8.Name = "Datalogcustomheadfltext8";
            this.Datalogcustomheadfltext8.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomheadfltext8.TabIndex = 77;
            this.Datalogcustomheadfltext8.TextChanged += new System.EventHandler(this.Datalogcustomheadfltext8_TextChanged);
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(363, 14);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(26, 20);
            this.button27.TabIndex = 76;
            this.button27.Text = "?";
            this.button27.UseVisualStyleBackColor = true;
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(363, 38);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(26, 20);
            this.button28.TabIndex = 75;
            this.button28.Text = "?";
            this.button28.UseVisualStyleBackColor = true;
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(363, 61);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(26, 20);
            this.button29.TabIndex = 74;
            this.button29.Text = "?";
            this.button29.UseVisualStyleBackColor = true;
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(363, 85);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(26, 20);
            this.button30.TabIndex = 73;
            this.button30.Text = "?";
            this.button30.UseVisualStyleBackColor = true;
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(363, 110);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(26, 20);
            this.button31.TabIndex = 72;
            this.button31.Text = "?";
            this.button31.UseVisualStyleBackColor = true;
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(363, 137);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(26, 20);
            this.button32.TabIndex = 71;
            this.button32.Text = "?";
            this.button32.UseVisualStyleBackColor = true;
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(363, 162);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(26, 20);
            this.button33.TabIndex = 70;
            this.button33.Text = "?";
            this.button33.UseVisualStyleBackColor = true;
            // 
            // Datalogcustomheadfltext1
            // 
            this.Datalogcustomheadfltext1.Location = new System.Drawing.Point(266, 14);
            this.Datalogcustomheadfltext1.Name = "Datalogcustomheadfltext1";
            this.Datalogcustomheadfltext1.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomheadfltext1.TabIndex = 68;
            this.Datalogcustomheadfltext1.TextChanged += new System.EventHandler(this.Datalogcustomheadfltext1_TextChanged);
            // 
            // Datalogcustomheadfltext2
            // 
            this.Datalogcustomheadfltext2.Location = new System.Drawing.Point(266, 38);
            this.Datalogcustomheadfltext2.Name = "Datalogcustomheadfltext2";
            this.Datalogcustomheadfltext2.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomheadfltext2.TabIndex = 67;
            this.Datalogcustomheadfltext2.TextChanged += new System.EventHandler(this.Datalogcustomheadfltext2_TextChanged);
            // 
            // Datalogcustomheadfltext3
            // 
            this.Datalogcustomheadfltext3.Location = new System.Drawing.Point(266, 62);
            this.Datalogcustomheadfltext3.Name = "Datalogcustomheadfltext3";
            this.Datalogcustomheadfltext3.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomheadfltext3.TabIndex = 66;
            this.Datalogcustomheadfltext3.TextChanged += new System.EventHandler(this.Datalogcustomheadfltext3_TextChanged);
            // 
            // Datalogcustomheadfltext4
            // 
            this.Datalogcustomheadfltext4.Location = new System.Drawing.Point(266, 86);
            this.Datalogcustomheadfltext4.Name = "Datalogcustomheadfltext4";
            this.Datalogcustomheadfltext4.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomheadfltext4.TabIndex = 65;
            this.Datalogcustomheadfltext4.TextChanged += new System.EventHandler(this.Datalogcustomheadfltext4_TextChanged);
            // 
            // Datalogcustomheadfltext5
            // 
            this.Datalogcustomheadfltext5.Location = new System.Drawing.Point(266, 112);
            this.Datalogcustomheadfltext5.Name = "Datalogcustomheadfltext5";
            this.Datalogcustomheadfltext5.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomheadfltext5.TabIndex = 64;
            this.Datalogcustomheadfltext5.TextChanged += new System.EventHandler(this.Datalogcustomheadfltext5_TextChanged);
            // 
            // Datalogcustomheadfltext6
            // 
            this.Datalogcustomheadfltext6.Location = new System.Drawing.Point(266, 138);
            this.Datalogcustomheadfltext6.Name = "Datalogcustomheadfltext6";
            this.Datalogcustomheadfltext6.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomheadfltext6.TabIndex = 63;
            this.Datalogcustomheadfltext6.TextChanged += new System.EventHandler(this.Datalogcustomheadfltext6_TextChanged);
            // 
            // Datalogcustomheadfltext7
            // 
            this.Datalogcustomheadfltext7.Location = new System.Drawing.Point(266, 163);
            this.Datalogcustomheadfltext7.Name = "Datalogcustomheadfltext7";
            this.Datalogcustomheadfltext7.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomheadfltext7.TabIndex = 62;
            this.Datalogcustomheadfltext7.TextChanged += new System.EventHandler(this.Datalogcustomheadfltext7_TextChanged);
            // 
            // Datalogcustomheadflable1
            // 
            this.Datalogcustomheadflable1.AutoSize = true;
            this.Datalogcustomheadflable1.Location = new System.Drawing.Point(19, 17);
            this.Datalogcustomheadflable1.Name = "Datalogcustomheadflable1";
            this.Datalogcustomheadflable1.Size = new System.Drawing.Size(121, 13);
            this.Datalogcustomheadflable1.TabIndex = 19;
            this.Datalogcustomheadflable1.Text = "Custom Header Field #1";
            // 
            // Datalogcustomheadflable2
            // 
            this.Datalogcustomheadflable2.AutoSize = true;
            this.Datalogcustomheadflable2.Location = new System.Drawing.Point(19, 40);
            this.Datalogcustomheadflable2.Name = "Datalogcustomheadflable2";
            this.Datalogcustomheadflable2.Size = new System.Drawing.Size(121, 13);
            this.Datalogcustomheadflable2.TabIndex = 18;
            this.Datalogcustomheadflable2.Text = "Custom Header Field #2";
            // 
            // Datalogcustomheadflable3
            // 
            this.Datalogcustomheadflable3.AutoSize = true;
            this.Datalogcustomheadflable3.Location = new System.Drawing.Point(19, 62);
            this.Datalogcustomheadflable3.Name = "Datalogcustomheadflable3";
            this.Datalogcustomheadflable3.Size = new System.Drawing.Size(121, 13);
            this.Datalogcustomheadflable3.TabIndex = 17;
            this.Datalogcustomheadflable3.Text = "Custom Header Field #3";
            // 
            // Datalogcustomheadflable4
            // 
            this.Datalogcustomheadflable4.AutoSize = true;
            this.Datalogcustomheadflable4.Location = new System.Drawing.Point(19, 90);
            this.Datalogcustomheadflable4.Name = "Datalogcustomheadflable4";
            this.Datalogcustomheadflable4.Size = new System.Drawing.Size(121, 13);
            this.Datalogcustomheadflable4.TabIndex = 16;
            this.Datalogcustomheadflable4.Text = "Custom Header Field #4";
            // 
            // Datalogcustomheadflable5
            // 
            this.Datalogcustomheadflable5.AutoSize = true;
            this.Datalogcustomheadflable5.Location = new System.Drawing.Point(19, 116);
            this.Datalogcustomheadflable5.Name = "Datalogcustomheadflable5";
            this.Datalogcustomheadflable5.Size = new System.Drawing.Size(121, 13);
            this.Datalogcustomheadflable5.TabIndex = 15;
            this.Datalogcustomheadflable5.Text = "Custom Header Field #5";
            // 
            // Datalogcustomheadflable6
            // 
            this.Datalogcustomheadflable6.AutoSize = true;
            this.Datalogcustomheadflable6.Location = new System.Drawing.Point(19, 139);
            this.Datalogcustomheadflable6.Name = "Datalogcustomheadflable6";
            this.Datalogcustomheadflable6.Size = new System.Drawing.Size(121, 13);
            this.Datalogcustomheadflable6.TabIndex = 14;
            this.Datalogcustomheadflable6.Text = "Custom Header Field #6";
            // 
            // Datalogcustomheadflable7
            // 
            this.Datalogcustomheadflable7.AutoSize = true;
            this.Datalogcustomheadflable7.Location = new System.Drawing.Point(19, 164);
            this.Datalogcustomheadflable7.Name = "Datalogcustomheadflable7";
            this.Datalogcustomheadflable7.Size = new System.Drawing.Size(121, 13);
            this.Datalogcustomheadflable7.TabIndex = 13;
            this.Datalogcustomheadflable7.Text = "Custom Header Field #7";
            // 
            // Datalogcustomheadflable8
            // 
            this.Datalogcustomheadflable8.AutoSize = true;
            this.Datalogcustomheadflable8.Location = new System.Drawing.Point(19, 190);
            this.Datalogcustomheadflable8.Name = "Datalogcustomheadflable8";
            this.Datalogcustomheadflable8.Size = new System.Drawing.Size(121, 13);
            this.Datalogcustomheadflable8.TabIndex = 12;
            this.Datalogcustomheadflable8.Text = "Custom Header Field #8";
            // 
            // Datalogform1
            // 
            this.Datalogform1.Controls.Add(this.button14);
            this.Datalogform1.Controls.Add(this.button15);
            this.Datalogform1.Controls.Add(this.button16);
            this.Datalogform1.Controls.Add(this.button17);
            this.Datalogform1.Controls.Add(this.button18);
            this.Datalogform1.Controls.Add(this.button19);
            this.Datalogform1.Controls.Add(this.button20);
            this.Datalogform1.Controls.Add(this.button21);
            this.Datalogform1.Controls.Add(this.button22);
            this.Datalogform1.Controls.Add(this.button23);
            this.Datalogform1.Controls.Add(this.button24);
            this.Datalogform1.Controls.Add(this.button25);
            this.Datalogform1.Controls.Add(this.button13);
            this.Datalogform1.Controls.Add(this.button12);
            this.Datalogform1.Controls.Add(this.button11);
            this.Datalogform1.Controls.Add(this.button10);
            this.Datalogform1.Controls.Add(this.button9);
            this.Datalogform1.Controls.Add(this.button8);
            this.Datalogform1.Controls.Add(this.button7);
            this.Datalogform1.Controls.Add(this.button6);
            this.Datalogform1.Controls.Add(this.button5);
            this.Datalogform1.Controls.Add(this.button4);
            this.Datalogform1.Controls.Add(this.button3);
            this.Datalogform1.Controls.Add(this.button2);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext14);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext15);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext16);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext17);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext18);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext19);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext20);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext21);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext22);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext23);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext24);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext13);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext2);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext3);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext4);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext5);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext6);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext7);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext8);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext9);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext10);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext11);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext12);
            this.Datalogform1.Controls.Add(this.Datalogcustomftext1);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable18);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable17);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable19);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable20);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable21);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable22);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable23);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable24);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable15);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable14);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable13);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable16);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable4);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable5);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable6);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable7);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable8);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable9);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable10);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable11);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable12);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable2);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable1);
            this.Datalogform1.Controls.Add(this.Datalogcustomflable3);
            this.Datalogform1.Location = new System.Drawing.Point(6, 96);
            this.Datalogform1.Name = "Datalogform1";
            this.Datalogform1.Size = new System.Drawing.Size(663, 327);
            this.Datalogform1.TabIndex = 0;
            this.Datalogform1.TabStop = false;
            this.Datalogform1.Text = "        ";
            this.Datalogform1.Enter += new System.EventHandler(this.groupBox25_Enter);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(579, 43);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(26, 20);
            this.button14.TabIndex = 76;
            this.button14.Text = "?";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(579, 68);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(26, 20);
            this.button15.TabIndex = 75;
            this.button15.Text = "?";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(579, 92);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(26, 20);
            this.button16.TabIndex = 74;
            this.button16.Text = "?";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(579, 118);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(26, 20);
            this.button17.TabIndex = 73;
            this.button17.Text = "?";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(579, 146);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(26, 20);
            this.button18.TabIndex = 72;
            this.button18.Text = "?";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(579, 172);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(26, 20);
            this.button19.TabIndex = 71;
            this.button19.Text = "?";
            this.button19.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(579, 196);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(26, 20);
            this.button20.TabIndex = 70;
            this.button20.Text = "?";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(579, 222);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(26, 20);
            this.button21.TabIndex = 69;
            this.button21.Text = "?";
            this.button21.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(579, 248);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(26, 20);
            this.button22.TabIndex = 68;
            this.button22.Text = "?";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(579, 276);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(26, 20);
            this.button23.TabIndex = 67;
            this.button23.Text = "?";
            this.button23.UseVisualStyleBackColor = true;
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(579, 302);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(26, 20);
            this.button24.TabIndex = 66;
            this.button24.Text = "?";
            this.button24.UseVisualStyleBackColor = true;
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(579, 17);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(26, 20);
            this.button25.TabIndex = 65;
            this.button25.Text = "?";
            this.button25.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(254, 44);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(26, 20);
            this.button13.TabIndex = 64;
            this.button13.Text = "?";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(254, 69);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(26, 20);
            this.button12.TabIndex = 63;
            this.button12.Text = "?";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(254, 93);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(26, 20);
            this.button11.TabIndex = 62;
            this.button11.Text = "?";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(254, 119);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(26, 20);
            this.button10.TabIndex = 61;
            this.button10.Text = "?";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(254, 147);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(26, 20);
            this.button9.TabIndex = 60;
            this.button9.Text = "?";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(254, 173);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(26, 20);
            this.button8.TabIndex = 59;
            this.button8.Text = "?";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(254, 197);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(26, 20);
            this.button7.TabIndex = 58;
            this.button7.Text = "?";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(254, 223);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(26, 20);
            this.button6.TabIndex = 57;
            this.button6.Text = "?";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(254, 249);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(26, 20);
            this.button5.TabIndex = 56;
            this.button5.Text = "?";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(254, 277);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(26, 20);
            this.button4.TabIndex = 55;
            this.button4.Text = "?";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(254, 303);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(26, 20);
            this.button3.TabIndex = 54;
            this.button3.Text = "?";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(254, 18);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(26, 20);
            this.button2.TabIndex = 53;
            this.button2.Text = "?";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // Datalogcustomftext14
            // 
            this.Datalogcustomftext14.Location = new System.Drawing.Point(482, 44);
            this.Datalogcustomftext14.Name = "Datalogcustomftext14";
            this.Datalogcustomftext14.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext14.TabIndex = 52;
            this.Datalogcustomftext14.TextChanged += new System.EventHandler(this.Datalogcustomftext14_TextChanged);
            // 
            // Datalogcustomftext15
            // 
            this.Datalogcustomftext15.Location = new System.Drawing.Point(482, 70);
            this.Datalogcustomftext15.Name = "Datalogcustomftext15";
            this.Datalogcustomftext15.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext15.TabIndex = 51;
            this.Datalogcustomftext15.TextChanged += new System.EventHandler(this.Datalogcustomftext15_TextChanged);
            // 
            // Datalogcustomftext16
            // 
            this.Datalogcustomftext16.Location = new System.Drawing.Point(482, 94);
            this.Datalogcustomftext16.Name = "Datalogcustomftext16";
            this.Datalogcustomftext16.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext16.TabIndex = 50;
            this.Datalogcustomftext16.TextChanged += new System.EventHandler(this.Datalogcustomftext16_TextChanged);
            // 
            // Datalogcustomftext17
            // 
            this.Datalogcustomftext17.Location = new System.Drawing.Point(482, 120);
            this.Datalogcustomftext17.Name = "Datalogcustomftext17";
            this.Datalogcustomftext17.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext17.TabIndex = 49;
            this.Datalogcustomftext17.TextChanged += new System.EventHandler(this.Datalogcustomftext17_TextChanged);
            // 
            // Datalogcustomftext18
            // 
            this.Datalogcustomftext18.Location = new System.Drawing.Point(482, 146);
            this.Datalogcustomftext18.Name = "Datalogcustomftext18";
            this.Datalogcustomftext18.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext18.TabIndex = 48;
            this.Datalogcustomftext18.TextChanged += new System.EventHandler(this.Datalogcustomftext18_TextChanged);
            // 
            // Datalogcustomftext19
            // 
            this.Datalogcustomftext19.Location = new System.Drawing.Point(482, 172);
            this.Datalogcustomftext19.Name = "Datalogcustomftext19";
            this.Datalogcustomftext19.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext19.TabIndex = 47;
            this.Datalogcustomftext19.TextChanged += new System.EventHandler(this.Datalogcustomftext19_TextChanged);
            // 
            // Datalogcustomftext20
            // 
            this.Datalogcustomftext20.Location = new System.Drawing.Point(482, 198);
            this.Datalogcustomftext20.Name = "Datalogcustomftext20";
            this.Datalogcustomftext20.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext20.TabIndex = 46;
            this.Datalogcustomftext20.TextChanged += new System.EventHandler(this.Datalogcustomftext20_TextChanged);
            // 
            // Datalogcustomftext21
            // 
            this.Datalogcustomftext21.Location = new System.Drawing.Point(482, 224);
            this.Datalogcustomftext21.Name = "Datalogcustomftext21";
            this.Datalogcustomftext21.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext21.TabIndex = 45;
            this.Datalogcustomftext21.TextChanged += new System.EventHandler(this.Datalogcustomftext21_TextChanged);
            // 
            // Datalogcustomftext22
            // 
            this.Datalogcustomftext22.Location = new System.Drawing.Point(482, 250);
            this.Datalogcustomftext22.Name = "Datalogcustomftext22";
            this.Datalogcustomftext22.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext22.TabIndex = 44;
            this.Datalogcustomftext22.TextChanged += new System.EventHandler(this.Datalogcustomftext22_TextChanged);
            // 
            // Datalogcustomftext23
            // 
            this.Datalogcustomftext23.Location = new System.Drawing.Point(482, 277);
            this.Datalogcustomftext23.Name = "Datalogcustomftext23";
            this.Datalogcustomftext23.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext23.TabIndex = 43;
            this.Datalogcustomftext23.TextChanged += new System.EventHandler(this.Datalogcustomftext23_TextChanged);
            // 
            // Datalogcustomftext24
            // 
            this.Datalogcustomftext24.Location = new System.Drawing.Point(482, 303);
            this.Datalogcustomftext24.Name = "Datalogcustomftext24";
            this.Datalogcustomftext24.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext24.TabIndex = 42;
            this.Datalogcustomftext24.TextChanged += new System.EventHandler(this.Datalogcustomftext24_TextChanged);
            // 
            // Datalogcustomftext13
            // 
            this.Datalogcustomftext13.Location = new System.Drawing.Point(482, 18);
            this.Datalogcustomftext13.Name = "Datalogcustomftext13";
            this.Datalogcustomftext13.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext13.TabIndex = 41;
            this.Datalogcustomftext13.TextChanged += new System.EventHandler(this.Datalogcustomftext13_TextChanged);
            // 
            // Datalogcustomftext2
            // 
            this.Datalogcustomftext2.Location = new System.Drawing.Point(157, 44);
            this.Datalogcustomftext2.Name = "Datalogcustomftext2";
            this.Datalogcustomftext2.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext2.TabIndex = 40;
            this.Datalogcustomftext2.TextChanged += new System.EventHandler(this.Datalogcustomftext2_TextChanged);
            // 
            // Datalogcustomftext3
            // 
            this.Datalogcustomftext3.Location = new System.Drawing.Point(157, 70);
            this.Datalogcustomftext3.Name = "Datalogcustomftext3";
            this.Datalogcustomftext3.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext3.TabIndex = 39;
            this.Datalogcustomftext3.TextChanged += new System.EventHandler(this.Datalogcustomftext3_TextChanged);
            // 
            // Datalogcustomftext4
            // 
            this.Datalogcustomftext4.Location = new System.Drawing.Point(157, 94);
            this.Datalogcustomftext4.Name = "Datalogcustomftext4";
            this.Datalogcustomftext4.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext4.TabIndex = 38;
            this.Datalogcustomftext4.TextChanged += new System.EventHandler(this.Datalogcustomftext4_TextChanged);
            // 
            // Datalogcustomftext5
            // 
            this.Datalogcustomftext5.Location = new System.Drawing.Point(157, 120);
            this.Datalogcustomftext5.Name = "Datalogcustomftext5";
            this.Datalogcustomftext5.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext5.TabIndex = 37;
            this.Datalogcustomftext5.TextChanged += new System.EventHandler(this.Datalogcustomftext5_TextChanged);
            // 
            // Datalogcustomftext6
            // 
            this.Datalogcustomftext6.Location = new System.Drawing.Point(157, 146);
            this.Datalogcustomftext6.Name = "Datalogcustomftext6";
            this.Datalogcustomftext6.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext6.TabIndex = 36;
            this.Datalogcustomftext6.TextChanged += new System.EventHandler(this.Datalogcustomftext6_TextChanged);
            // 
            // Datalogcustomftext7
            // 
            this.Datalogcustomftext7.Location = new System.Drawing.Point(157, 172);
            this.Datalogcustomftext7.Name = "Datalogcustomftext7";
            this.Datalogcustomftext7.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext7.TabIndex = 35;
            this.Datalogcustomftext7.TextChanged += new System.EventHandler(this.Datalogcustomftext7_TextChanged);
            // 
            // Datalogcustomftext8
            // 
            this.Datalogcustomftext8.Location = new System.Drawing.Point(157, 198);
            this.Datalogcustomftext8.Name = "Datalogcustomftext8";
            this.Datalogcustomftext8.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext8.TabIndex = 34;
            this.Datalogcustomftext8.TextChanged += new System.EventHandler(this.Datalogcustomftext8_TextChanged);
            // 
            // Datalogcustomftext9
            // 
            this.Datalogcustomftext9.Location = new System.Drawing.Point(157, 224);
            this.Datalogcustomftext9.Name = "Datalogcustomftext9";
            this.Datalogcustomftext9.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext9.TabIndex = 33;
            this.Datalogcustomftext9.TextChanged += new System.EventHandler(this.Datalogcustomftext9_TextChanged);
            // 
            // Datalogcustomftext10
            // 
            this.Datalogcustomftext10.Location = new System.Drawing.Point(157, 250);
            this.Datalogcustomftext10.Name = "Datalogcustomftext10";
            this.Datalogcustomftext10.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext10.TabIndex = 32;
            this.Datalogcustomftext10.TextChanged += new System.EventHandler(this.Datalogcustomftext10_TextChanged);
            // 
            // Datalogcustomftext11
            // 
            this.Datalogcustomftext11.Location = new System.Drawing.Point(157, 277);
            this.Datalogcustomftext11.Name = "Datalogcustomftext11";
            this.Datalogcustomftext11.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext11.TabIndex = 31;
            this.Datalogcustomftext11.TextChanged += new System.EventHandler(this.Datalogcustomftext11_TextChanged);
            // 
            // Datalogcustomftext12
            // 
            this.Datalogcustomftext12.Location = new System.Drawing.Point(157, 303);
            this.Datalogcustomftext12.Name = "Datalogcustomftext12";
            this.Datalogcustomftext12.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext12.TabIndex = 30;
            this.Datalogcustomftext12.TextChanged += new System.EventHandler(this.Datalogcustomftext12_TextChanged);
            // 
            // Datalogcustomftext1
            // 
            this.Datalogcustomftext1.Location = new System.Drawing.Point(157, 18);
            this.Datalogcustomftext1.Name = "Datalogcustomftext1";
            this.Datalogcustomftext1.Size = new System.Drawing.Size(100, 20);
            this.Datalogcustomftext1.TabIndex = 29;
            this.Datalogcustomftext1.TextChanged += new System.EventHandler(this.Datalogcustomftext1_TextChanged);
            // 
            // Datalogcustomflable18
            // 
            this.Datalogcustomflable18.AutoSize = true;
            this.Datalogcustomflable18.Location = new System.Drawing.Point(367, 150);
            this.Datalogcustomflable18.Name = "Datalogcustomflable18";
            this.Datalogcustomflable18.Size = new System.Drawing.Size(44, 13);
            this.Datalogcustomflable18.TabIndex = 28;
            this.Datalogcustomflable18.Text = "Scratch";
            // 
            // Datalogcustomflable17
            // 
            this.Datalogcustomflable17.AutoSize = true;
            this.Datalogcustomflable17.Location = new System.Drawing.Point(367, 123);
            this.Datalogcustomflable17.Name = "Datalogcustomflable17";
            this.Datalogcustomflable17.Size = new System.Drawing.Size(44, 13);
            this.Datalogcustomflable17.TabIndex = 24;
            this.Datalogcustomflable17.Text = "Scratch";
            // 
            // Datalogcustomflable19
            // 
            this.Datalogcustomflable19.AutoSize = true;
            this.Datalogcustomflable19.Location = new System.Drawing.Point(367, 173);
            this.Datalogcustomflable19.Name = "Datalogcustomflable19";
            this.Datalogcustomflable19.Size = new System.Drawing.Size(42, 13);
            this.Datalogcustomflable19.TabIndex = 22;
            this.Datalogcustomflable19.Text = "origin E";
            // 
            // Datalogcustomflable20
            // 
            this.Datalogcustomflable20.AutoSize = true;
            this.Datalogcustomflable20.Location = new System.Drawing.Point(367, 197);
            this.Datalogcustomflable20.Name = "Datalogcustomflable20";
            this.Datalogcustomflable20.Size = new System.Drawing.Size(44, 13);
            this.Datalogcustomflable20.TabIndex = 21;
            this.Datalogcustomflable20.Text = "Scratch";
            // 
            // Datalogcustomflable21
            // 
            this.Datalogcustomflable21.AutoSize = true;
            this.Datalogcustomflable21.Location = new System.Drawing.Point(367, 225);
            this.Datalogcustomflable21.Name = "Datalogcustomflable21";
            this.Datalogcustomflable21.Size = new System.Drawing.Size(44, 13);
            this.Datalogcustomflable21.TabIndex = 20;
            this.Datalogcustomflable21.Text = "Scratch";
            // 
            // Datalogcustomflable22
            // 
            this.Datalogcustomflable22.AutoSize = true;
            this.Datalogcustomflable22.Location = new System.Drawing.Point(367, 253);
            this.Datalogcustomflable22.Name = "Datalogcustomflable22";
            this.Datalogcustomflable22.Size = new System.Drawing.Size(102, 13);
            this.Datalogcustomflable22.TabIndex = 19;
            this.Datalogcustomflable22.Text = "MP3X_badcrc coun";
            // 
            // Datalogcustomflable23
            // 
            this.Datalogcustomflable23.AutoSize = true;
            this.Datalogcustomflable23.Location = new System.Drawing.Point(367, 279);
            this.Datalogcustomflable23.Name = "Datalogcustomflable23";
            this.Datalogcustomflable23.Size = new System.Drawing.Size(102, 13);
            this.Datalogcustomflable23.TabIndex = 18;
            this.Datalogcustomflable23.Text = "MP3X_badcrc coun";
            // 
            // Datalogcustomflable24
            // 
            this.Datalogcustomflable24.AutoSize = true;
            this.Datalogcustomflable24.Location = new System.Drawing.Point(370, 303);
            this.Datalogcustomflable24.Name = "Datalogcustomflable24";
            this.Datalogcustomflable24.Size = new System.Drawing.Size(91, 13);
            this.Datalogcustomflable24.TabIndex = 17;
            this.Datalogcustomflable24.Text = "redundancystatus";
            // 
            // Datalogcustomflable15
            // 
            this.Datalogcustomflable15.AutoSize = true;
            this.Datalogcustomflable15.Location = new System.Drawing.Point(366, 73);
            this.Datalogcustomflable15.Name = "Datalogcustomflable15";
            this.Datalogcustomflable15.Size = new System.Drawing.Size(73, 13);
            this.Datalogcustomflable15.TabIndex = 16;
            this.Datalogcustomflable15.Text = "eulerPitch_KF";
            // 
            // Datalogcustomflable14
            // 
            this.Datalogcustomflable14.AutoSize = true;
            this.Datalogcustomflable14.Location = new System.Drawing.Point(367, 48);
            this.Datalogcustomflable14.Name = "Datalogcustomflable14";
            this.Datalogcustomflable14.Size = new System.Drawing.Size(67, 13);
            this.Datalogcustomflable14.TabIndex = 15;
            this.Datalogcustomflable14.Text = "eulerRoll_KF";
            // 
            // Datalogcustomflable13
            // 
            this.Datalogcustomflable13.AutoSize = true;
            this.Datalogcustomflable13.Location = new System.Drawing.Point(367, 25);
            this.Datalogcustomflable13.Name = "Datalogcustomflable13";
            this.Datalogcustomflable13.Size = new System.Drawing.Size(50, 13);
            this.Datalogcustomflable13.TabIndex = 14;
            this.Datalogcustomflable13.Text = "ServoVal";
            // 
            // Datalogcustomflable16
            // 
            this.Datalogcustomflable16.AutoSize = true;
            this.Datalogcustomflable16.Location = new System.Drawing.Point(367, 97);
            this.Datalogcustomflable16.Name = "Datalogcustomflable16";
            this.Datalogcustomflable16.Size = new System.Drawing.Size(70, 13);
            this.Datalogcustomflable16.TabIndex = 13;
            this.Datalogcustomflable16.Text = "eulerYaw_KF";
            // 
            // Datalogcustomflable4
            // 
            this.Datalogcustomflable4.AutoSize = true;
            this.Datalogcustomflable4.Location = new System.Drawing.Point(46, 97);
            this.Datalogcustomflable4.Name = "Datalogcustomflable4";
            this.Datalogcustomflable4.Size = new System.Drawing.Size(41, 13);
            this.Datalogcustomflable4.TabIndex = 12;
            this.Datalogcustomflable4.Text = "FServo";
            // 
            // Datalogcustomflable5
            // 
            this.Datalogcustomflable5.AutoSize = true;
            this.Datalogcustomflable5.Location = new System.Drawing.Point(46, 123);
            this.Datalogcustomflable5.Name = "Datalogcustomflable5";
            this.Datalogcustomflable5.Size = new System.Drawing.Size(41, 13);
            this.Datalogcustomflable5.TabIndex = 11;
            this.Datalogcustomflable5.Text = "FServo";
            this.Datalogcustomflable5.Click += new System.EventHandler(this.label162_Click);
            // 
            // Datalogcustomflable6
            // 
            this.Datalogcustomflable6.AutoSize = true;
            this.Datalogcustomflable6.Location = new System.Drawing.Point(46, 147);
            this.Datalogcustomflable6.Name = "Datalogcustomflable6";
            this.Datalogcustomflable6.Size = new System.Drawing.Size(45, 13);
            this.Datalogcustomflable6.TabIndex = 10;
            this.Datalogcustomflable6.Text = "TMPRT";
            // 
            // Datalogcustomflable7
            // 
            this.Datalogcustomflable7.AutoSize = true;
            this.Datalogcustomflable7.Location = new System.Drawing.Point(46, 171);
            this.Datalogcustomflable7.Name = "Datalogcustomflable7";
            this.Datalogcustomflable7.Size = new System.Drawing.Size(75, 13);
            this.Datalogcustomflable7.TabIndex = 9;
            this.Datalogcustomflable7.Text = "TMPRT_Pitch";
            // 
            // Datalogcustomflable8
            // 
            this.Datalogcustomflable8.AutoSize = true;
            this.Datalogcustomflable8.Location = new System.Drawing.Point(46, 199);
            this.Datalogcustomflable8.Name = "Datalogcustomflable8";
            this.Datalogcustomflable8.Size = new System.Drawing.Size(85, 13);
            this.Datalogcustomflable8.TabIndex = 8;
            this.Datalogcustomflable8.Text = "TMPRT_XACCL";
            // 
            // Datalogcustomflable9
            // 
            this.Datalogcustomflable9.AutoSize = true;
            this.Datalogcustomflable9.Location = new System.Drawing.Point(46, 227);
            this.Datalogcustomflable9.Name = "Datalogcustomflable9";
            this.Datalogcustomflable9.Size = new System.Drawing.Size(111, 13);
            this.Datalogcustomflable9.TabIndex = 7;
            this.Datalogcustomflable9.Text = "TMPRT_Temperature";
            // 
            // Datalogcustomflable10
            // 
            this.Datalogcustomflable10.AutoSize = true;
            this.Datalogcustomflable10.Location = new System.Drawing.Point(46, 253);
            this.Datalogcustomflable10.Name = "Datalogcustomflable10";
            this.Datalogcustomflable10.Size = new System.Drawing.Size(44, 13);
            this.Datalogcustomflable10.TabIndex = 6;
            this.Datalogcustomflable10.Text = "Scratch";
            // 
            // Datalogcustomflable11
            // 
            this.Datalogcustomflable11.AutoSize = true;
            this.Datalogcustomflable11.Location = new System.Drawing.Point(46, 282);
            this.Datalogcustomflable11.Name = "Datalogcustomflable11";
            this.Datalogcustomflable11.Size = new System.Drawing.Size(44, 13);
            this.Datalogcustomflable11.TabIndex = 5;
            this.Datalogcustomflable11.Text = "Scratch";
            // 
            // Datalogcustomflable12
            // 
            this.Datalogcustomflable12.AutoSize = true;
            this.Datalogcustomflable12.Location = new System.Drawing.Point(46, 308);
            this.Datalogcustomflable12.Name = "Datalogcustomflable12";
            this.Datalogcustomflable12.Size = new System.Drawing.Size(50, 13);
            this.Datalogcustomflable12.TabIndex = 4;
            this.Datalogcustomflable12.Text = "ServoVal";
            // 
            // Datalogcustomflable2
            // 
            this.Datalogcustomflable2.AutoSize = true;
            this.Datalogcustomflable2.Location = new System.Drawing.Point(46, 48);
            this.Datalogcustomflable2.Name = "Datalogcustomflable2";
            this.Datalogcustomflable2.Size = new System.Drawing.Size(52, 13);
            this.Datalogcustomflable2.TabIndex = 3;
            this.Datalogcustomflable2.Text = "FatalError";
            // 
            // Datalogcustomflable1
            // 
            this.Datalogcustomflable1.AutoSize = true;
            this.Datalogcustomflable1.Location = new System.Drawing.Point(46, 22);
            this.Datalogcustomflable1.Name = "Datalogcustomflable1";
            this.Datalogcustomflable1.Size = new System.Drawing.Size(33, 13);
            this.Datalogcustomflable1.TabIndex = 2;
            this.Datalogcustomflable1.Text = "tstate";
            // 
            // Datalogcustomflable3
            // 
            this.Datalogcustomflable3.AutoSize = true;
            this.Datalogcustomflable3.Location = new System.Drawing.Point(46, 71);
            this.Datalogcustomflable3.Name = "Datalogcustomflable3";
            this.Datalogcustomflable3.Size = new System.Drawing.Size(83, 13);
            this.Datalogcustomflable3.TabIndex = 0;
            this.Datalogcustomflable3.Text = "HeliTakeofferror";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.groupBox22);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(925, 703);
            this.tabPage6.TabIndex = 6;
            this.tabPage6.Text = "Sensors";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.agllowthrottleswitch);
            this.groupBox22.Controls.Add(this.agllowthrottlesetting);
            this.groupBox22.Controls.Add(this.aglswitchaltitude);
            this.groupBox22.Controls.Add(this.aglmode);
            this.groupBox22.Controls.Add(this.aglsensor);
            this.groupBox22.Controls.Add(this.label140);
            this.groupBox22.Controls.Add(this.label135);
            this.groupBox22.Controls.Add(this.label133);
            this.groupBox22.Controls.Add(this.label104);
            this.groupBox22.Controls.Add(this.label98);
            this.groupBox22.Location = new System.Drawing.Point(6, 6);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(294, 196);
            this.groupBox22.TabIndex = 0;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "AGL";
            // 
            // agllowthrottleswitch
            // 
            this.agllowthrottleswitch.Location = new System.Drawing.Point(210, 140);
            this.agllowthrottleswitch.Name = "agllowthrottleswitch";
            this.agllowthrottleswitch.Size = new System.Drawing.Size(78, 20);
            this.agllowthrottleswitch.TabIndex = 10;
            this.agllowthrottleswitch.TextChanged += new System.EventHandler(this.agllowthrottleswitch_TextChanged);
            // 
            // agllowthrottlesetting
            // 
            this.agllowthrottlesetting.Location = new System.Drawing.Point(210, 108);
            this.agllowthrottlesetting.Name = "agllowthrottlesetting";
            this.agllowthrottlesetting.Size = new System.Drawing.Size(78, 20);
            this.agllowthrottlesetting.TabIndex = 9;
            this.agllowthrottlesetting.TextChanged += new System.EventHandler(this.agllowthrottlesetting_TextChanged);
            // 
            // aglswitchaltitude
            // 
            this.aglswitchaltitude.Location = new System.Drawing.Point(210, 80);
            this.aglswitchaltitude.Name = "aglswitchaltitude";
            this.aglswitchaltitude.Size = new System.Drawing.Size(78, 20);
            this.aglswitchaltitude.TabIndex = 8;
            this.aglswitchaltitude.TextChanged += new System.EventHandler(this.aglswitchaltitude_TextChanged);
            // 
            // aglmode
            // 
            this.aglmode.FormattingEnabled = true;
            this.aglmode.Items.AddRange(new object[] {
            "0- Normal",
            "1- DEM",
            "2- DEM Pitch/Roll"});
            this.aglmode.Location = new System.Drawing.Point(167, 53);
            this.aglmode.Name = "aglmode";
            this.aglmode.Size = new System.Drawing.Size(121, 21);
            this.aglmode.TabIndex = 7;
            this.aglmode.SelectedIndexChanged += new System.EventHandler(this.aglmode_SelectedIndexChanged);
            // 
            // aglsensor
            // 
            this.aglsensor.FormattingEnabled = true;
            this.aglsensor.Items.AddRange(new object[] {
            "Disable",
            "Ultrasonic AGL",
            "Laser altimeter",
            "radar altimeter (not implemented)",
            "infrared altimeter "});
            this.aglsensor.Location = new System.Drawing.Point(167, 25);
            this.aglsensor.Name = "aglsensor";
            this.aglsensor.Size = new System.Drawing.Size(121, 21);
            this.aglsensor.TabIndex = 6;
            this.aglsensor.SelectedIndexChanged += new System.EventHandler(this.aglsensor_SelectedIndexChanged);
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(0, 147);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(133, 13);
            this.label140.TabIndex = 5;
            this.label140.Text = "Low Throttle Switch Alt (m)";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(0, 53);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(58, 13);
            this.label135.TabIndex = 4;
            this.label135.Text = "AGL Mode";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(0, 84);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(77, 13);
            this.label133.TabIndex = 3;
            this.label133.Text = "Switch Altitude";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(0, 115);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(165, 13);
            this.label104.TabIndex = 2;
            this.label104.Text = "Low Throttle  Setting (Fine Servo)";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(0, 25);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(64, 13);
            this.label98.TabIndex = 1;
            this.label98.Text = "AGL Sensor";
            // 
            // saveVrsFile
            // 
            this.saveVrsFile.Location = new System.Drawing.Point(835, 270);
            this.saveVrsFile.Name = "saveVrsFile";
            this.saveVrsFile.Size = new System.Drawing.Size(71, 30);
            this.saveVrsFile.TabIndex = 1;
            this.saveVrsFile.Text = "Check VRS";
            this.saveVrsFile.UseVisualStyleBackColor = true;
            this.saveVrsFile.Click += new System.EventHandler(this.saveVrsFile_Click);
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(835, 227);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(71, 30);
            this.Save.TabIndex = 2;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(835, 316);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(75, 38);
            this.button34.TabIndex = 4;
            this.button34.Text = "Transmite VRS";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.transmite_vrs);
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(835, 370);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(75, 38);
            this.button35.TabIndex = 5;
            this.button35.Text = "Acquire VRS";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.acquire_vrs);
            // 
            // open_vrsfile
            // 
            this.open_vrsfile.Location = new System.Drawing.Point(835, 127);
            this.open_vrsfile.Name = "open_vrsfile";
            this.open_vrsfile.Size = new System.Drawing.Size(71, 37);
            this.open_vrsfile.TabIndex = 6;
            this.open_vrsfile.Text = "Open vrsfile";
            this.open_vrsfile.UseVisualStyleBackColor = true;
            this.open_vrsfile.Click += new System.EventHandler(this.open_vrsfile_Click);
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(840, 28);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(57, 13);
            this.label147.TabIndex = 7;
            this.label147.Text = "File Name:";
            // 
            // vrsname
            // 
            this.vrsname.AutoSize = true;
            this.vrsname.Location = new System.Drawing.Point(840, 60);
            this.vrsname.Name = "vrsname";
            this.vrsname.Size = new System.Drawing.Size(0, 13);
            this.vrsname.TabIndex = 8;
            // 
            // MicroPilotSetup
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(936, 741);
            this.Controls.Add(this.vrsname);
            this.Controls.Add(this.label147);
            this.Controls.Add(this.open_vrsfile);
            this.Controls.Add(this.button35);
            this.Controls.Add(this.button34);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.saveVrsFile);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MicroPilotSetup";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "AutoPilotSetup";
            this.Load += new System.EventHandler(this.MicroPilotSetup_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.GPS.ResumeLayout(false);
            this.Novatelgroup.ResumeLayout(false);
            this.Novatelgroup.PerformLayout();
            this.SBASgroup.ResumeLayout(false);
            this.SBASgroup.PerformLayout();
            this.Ubloxgroup.ResumeLayout(false);
            this.Ubloxgroup.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.Datalogform1.ResumeLayout(false);
            this.Datalogform1.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox SlewLimitsT;
        private System.Windows.Forms.TextBox ResultMinT;
        private System.Windows.Forms.TextBox ResultMaxT;
        private System.Windows.Forms.TextBox ScheduledT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton GainEffects;
        private System.Windows.Forms.RadioButton RealUnites;
        private System.Windows.Forms.RadioButton AutoPilotUnites;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox EnableYawDamper;
        private System.Windows.Forms.CheckBox SmoothGainScheduling;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Range2T;
        private System.Windows.Forms.TextBox Range1T;
        private System.Windows.Forms.TextBox Range3T;
        private System.Windows.Forms.TextBox Range0T;
        private System.Windows.Forms.TextBox Sch3T;
        private System.Windows.Forms.TextBox Sch2T;
        private System.Windows.Forms.TextBox Sch1T;
        private System.Windows.Forms.TextBox Sch0T;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Propotional3T;
        private System.Windows.Forms.TextBox Propotional2T;
        private System.Windows.Forms.TextBox Propotional1T;
        private System.Windows.Forms.TextBox Propotional0T;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox feedForwardInputMinT;
        private System.Windows.Forms.TextBox feedForwardInputMaxT;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox feedForward3T;
        private System.Windows.Forms.TextBox feedForward2T;
        private System.Windows.Forms.TextBox feedForward1T;
        private System.Windows.Forms.TextBox feedForward0T;
        private System.Windows.Forms.TextBox DifferentialInputMinT;
        private System.Windows.Forms.TextBox DifferentialInputMaxT;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox Differential3T;
        private System.Windows.Forms.TextBox Differential2T;
        private System.Windows.Forms.TextBox Differential1T;
        private System.Windows.Forms.TextBox Differential0T;
        private System.Windows.Forms.TextBox ResAntiWindupMinT;
        private System.Windows.Forms.TextBox ResAntiWindupMaxT;
        private System.Windows.Forms.TextBox inteqralInputMinT;
        private System.Windows.Forms.TextBox inteqralInputMaxT;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox inteqral3T;
        private System.Windows.Forms.TextBox inteqral2T;
        private System.Windows.Forms.TextBox inteqral1T;
        private System.Windows.Forms.TextBox inteqral0T;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox inputMin;
        private System.Windows.Forms.TextBox inputMax;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox maxLunchPlatformSpeedT;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.CheckBox flyTosFromTosLineUp;
        private System.Windows.Forms.CheckBox fromTosToFlyTos;
        private System.Windows.Forms.CheckBox alwaysUseLineClimb;
        private System.Windows.Forms.TextBox offGroundAlgoT;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox lockedPichtT;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox takeoffPichT;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox enableDeadcheckBox;
        private System.Windows.Forms.ComboBox levelFlightModecomboBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox wpReduceTurnT;
        private System.Windows.Forms.TextBox wpDiaT;
        private System.Windows.Forms.TextBox cimbMarginT;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox minTargetSpeedT;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox minTargetAltT;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox onGroundSpeedT;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox stallSpeedT;
        private System.Windows.Forms.TextBox maxTargetSpeedT;
        private System.Windows.Forms.TextBox maxTargrtAltT;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.CheckBox invertFenceEWcheckBox;
        private System.Windows.Forms.CheckBox invertFenceNScheckBox;
        private System.Windows.Forms.TextBox southFenceT;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox westFenceT;
        private System.Windows.Forms.TextBox eastFenceT;
        private System.Windows.Forms.TextBox northFenceT;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox maxFlightAltT;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox maxFlightDistT;
        private System.Windows.Forms.CheckBox allowNnavigationAirSpeedcheckBox;
        private System.Windows.Forms.TextBox descentRateT;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox climbSpeedT;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox descentSpeedT;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox maxSpeedIncreasT;
        private System.Windows.Forms.TextBox cruiseSpeedT;
        private System.Windows.Forms.TextBox rotationSpeedT;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox descentTypeT;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox GyroControlServoT;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.TextBox SpoilerServoT;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox NoiseWheelServoT;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.TextBox ReleaseServoT;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox ParachuteServoT;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox DisengageLockSpeedT;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.CheckBox EnableSevoLock;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.CheckBox DisableMixingLimit;
        private System.Windows.Forms.CheckBox DisableRStaffServo;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label92;
        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private System.Windows.Forms.TextBox SpoilerRCInChannelT;
        private System.Windows.Forms.TextBox FlapsRCInChannelT;
        private System.Windows.Forms.TextBox SpoilerSlewLlimitT;
        private System.Windows.Forms.TextBox InputCalculationT;
        private System.Windows.Forms.ComboBox TO;
        private System.Windows.Forms.Label onGroundL;
        private System.Windows.Forms.Label slweLimitL;
        private System.Windows.Forms.Label fatalErrorMaxL;
        private System.Windows.Forms.Label approachL;
        private System.Windows.Forms.Label climbL;
        private System.Windows.Forms.Label descentL;
        private System.Windows.Forms.Label takeoffL;
        private System.Windows.Forms.Label cruiseL;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label lockRudderL;
        private System.Windows.Forms.Label lockAileronL;
        private System.Windows.Forms.Label lockElvatorL;
        private System.Windows.Forms.ComboBox SelectedPIDLloopCombo;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.CheckBox CalculateAlironCheckBox;
        private System.Windows.Forms.CheckBox CompatiblityCheckBox;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.HScrollBar z1;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label s6t;
        private System.Windows.Forms.Label s7t;
        private System.Windows.Forms.Label st5;
        private System.Windows.Forms.Label s8t;
        private System.Windows.Forms.Label s5t;
        private System.Windows.Forms.Label s7;
        private System.Windows.Forms.Label s4;
        private System.Windows.Forms.Label s3;
        private System.Windows.Forms.Label s2;
        private System.Windows.Forms.Label s1;
        private System.Windows.Forms.Label p9;
        private System.Windows.Forms.Label p8;
        private System.Windows.Forms.Label p7;
        private System.Windows.Forms.Label p6;
        private System.Windows.Forms.Label p5;
        private System.Windows.Forms.Label p4;
        private System.Windows.Forms.Label p3;
        private System.Windows.Forms.Label p2;
        private System.Windows.Forms.Label p1;
        private System.Windows.Forms.ComboBox init8;
        private System.Windows.Forms.ComboBox init7;
        private System.Windows.Forms.ComboBox init6;
        private System.Windows.Forms.ComboBox init5;
        private System.Windows.Forms.ComboBox init4;
        private System.Windows.Forms.ComboBox init3;
        private System.Windows.Forms.ComboBox init2;
        private System.Windows.Forms.ComboBox init1;
        private System.Windows.Forms.Label TIPercentage;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.CheckBox swap8;
        private System.Windows.Forms.CheckBox swap7;
        private System.Windows.Forms.CheckBox swap6;
        private System.Windows.Forms.CheckBox swap5;
        private System.Windows.Forms.CheckBox swap4;
        private System.Windows.Forms.CheckBox swap3;
        private System.Windows.Forms.CheckBox swap2;
        private System.Windows.Forms.CheckBox swap1;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label s8;
        private System.Windows.Forms.Label s6;
        private System.Windows.Forms.Label s5;
        private System.Windows.Forms.Label s4t;
        private System.Windows.Forms.Label s3t;
        private System.Windows.Forms.Label s2t;
        private System.Windows.Forms.Label s1t;
        private System.Windows.Forms.TextBox StaffServoCICTimeoutT;
        private System.Windows.Forms.TextBox FlapSlewLimitT;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.ComboBox servoRudderComboBox;
        private System.Windows.Forms.ComboBox ServoTypeComboBox;
        private System.Windows.Forms.ComboBox ServoNumComboBox;
        private System.Windows.Forms.ComboBox EchoServoAdjustment;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.HScrollBar z8;
        private System.Windows.Forms.HScrollBar z7;
        private System.Windows.Forms.HScrollBar z6;
        private System.Windows.Forms.HScrollBar z5;
        private System.Windows.Forms.HScrollBar z4;
        private System.Windows.Forms.HScrollBar z3;
        private System.Windows.Forms.HScrollBar z2;
        private System.Windows.Forms.Button saveVrsFile;
        private System.Windows.Forms.HScrollBar t1;
        private System.Windows.Forms.HScrollBar throttleIdel;
        private System.Windows.Forms.HScrollBar t8;
        private System.Windows.Forms.HScrollBar t7;
        private System.Windows.Forms.HScrollBar t6;
        private System.Windows.Forms.HScrollBar t5;
        private System.Windows.Forms.HScrollBar t4;
        private System.Windows.Forms.HScrollBar t3;
        private System.Windows.Forms.HScrollBar t2;
        private System.Windows.Forms.HScrollBar lockAileronSlider;
        private System.Windows.Forms.HScrollBar lockRudderSlider;
        private System.Windows.Forms.HScrollBar lockElavatorSlider;
        private System.Windows.Forms.HScrollBar fatalErrorSlider;
        private System.Windows.Forms.HScrollBar slewLimitSlider;
        private System.Windows.Forms.HScrollBar onGroundSlider;
        private System.Windows.Forms.HScrollBar takeoffSlider;
        private System.Windows.Forms.HScrollBar descentSlider;
        private System.Windows.Forms.HScrollBar climbSlider;
        private System.Windows.Forms.HScrollBar approachSlider;
        private System.Windows.Forms.HScrollBar cruiseSlider;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.ComboBox LRCsecodarycomport;
        private System.Windows.Forms.CheckBox LRConlyreadservos;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label Lr;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.CheckBox UART2disablebinding;
        private System.Windows.Forms.ComboBox UART2Txprotocolfield;
        private System.Windows.Forms.TextBox UART2Txparity;
        private System.Windows.Forms.TextBox UART2Txtimeout;
        private System.Windows.Forms.TextBox UART2Txdatabits;
        private System.Windows.Forms.TextBox UART2Txbaud;
        private System.Windows.Forms.TextBox UART2Txchannel;
        private System.Windows.Forms.TextBox UART2Rxparity;
        private System.Windows.Forms.TextBox UART2Rxdatabits;
        private System.Windows.Forms.TextBox UART2Rxbaud;
        private System.Windows.Forms.ComboBox UART2protocol;
        private System.Windows.Forms.CheckBox UART1disablebinding;
        private System.Windows.Forms.ComboBox UART1protocolfield;
        private System.Windows.Forms.TextBox UART1Txparity;
        private System.Windows.Forms.TextBox UART1Txtimeout;
        private System.Windows.Forms.TextBox UART1Txbatabits;
        private System.Windows.Forms.TextBox UART1Txbaud;
        private System.Windows.Forms.TextBox UART1Txchannel;
        private System.Windows.Forms.TextBox UART1Rxparity;
        private System.Windows.Forms.TextBox UART1Rxdatabits;
        private System.Windows.Forms.TextBox UART1Rxbaud;
        private System.Windows.Forms.ComboBox UART1protocol;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.CheckBox Gcschecksum;
        private System.Windows.Forms.ComboBox GcsParity;
        private System.Windows.Forms.ComboBox Gcsflowcontrol;
        private System.Windows.Forms.TextBox Gcsbaud;
        private System.Windows.Forms.TextBox GcsStrenghtchannel;
        private System.Windows.Forms.TextBox Gcsenabledialing;
        private System.Windows.Forms.TextBox Gcsphonesecond;
        private System.Windows.Forms.TextBox Gcsphonefirst;
        private System.Windows.Forms.TextBox Gcsidentifier;
        private System.Windows.Forms.TextBox GcsStrenghtminimum;
        private System.Windows.Forms.TextBox GcsCommandrepeat;
        private System.Windows.Forms.TextBox GcsPadCharacter;
        private System.Windows.Forms.TextBox Gcsintialreport;
        private System.Windows.Forms.TextBox Gcstimeount;
        private System.Windows.Forms.Label lable34;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label lable12;
        private System.Windows.Forms.Label lable33;
        private System.Windows.Forms.Label lable44;
        private System.Windows.Forms.Label lable66;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label lable3;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label lable;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.TextBox UART2Rxchannel;
        private System.Windows.Forms.TextBox UART1Rxchannel;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.CheckBox EnableBindingfeature;
        private System.Windows.Forms.CheckBox Vrsfootercommands;
        private System.Windows.Forms.CheckBox SLRCenterfailure;
        private System.Windows.Forms.CheckBox SLRCsecondary;
        private System.Windows.Forms.CheckBox MLRCenterfailure;
        private System.Windows.Forms.CheckBox LRCuselrc;
        private System.Windows.Forms.TabPage GPS;
        private System.Windows.Forms.GroupBox SBASgroup;
        private System.Windows.Forms.GroupBox Ubloxgroup;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.GroupBox Novatelgroup;
        private System.Windows.Forms.Label kalmanfilter;
        private System.Windows.Forms.Label hemisphere;
        private System.Windows.Forms.Label fakegps;
        private System.Windows.Forms.Label usegpsaltitude;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label usegpsspeed;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.CheckBox SBASusetestmode;
        private System.Windows.Forms.CheckBox SBASapplyintegrity;
        private System.Windows.Forms.CheckBox SBASenable;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.CheckBox Novatelbasestation;
        private System.Windows.Forms.CheckBox Novatelenablediableothe;
        private System.Windows.Forms.CheckBox Novatelminimumspeed;
        private System.Windows.Forms.CheckBox Novatelusel1flaot;
        private System.Windows.Forms.CheckBox Novatelenablecorrection;
        private System.Windows.Forms.CheckBox Novatelenablediasblenovatel;
        private System.Windows.Forms.TextBox SBASenterPRNs;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label SBASlable;
        private System.Windows.Forms.TextBox GPS2stopbits;
        private System.Windows.Forms.TextBox GPS2bits;
        private System.Windows.Forms.TextBox GPS2baud;
        private System.Windows.Forms.ComboBox GPS2flowcontrol;
        private System.Windows.Forms.ComboBox GPS2parity;
        private System.Windows.Forms.CheckBox GPS1hemisphere;
        private System.Windows.Forms.CheckBox GPS1kalmanfilter;
        private System.Windows.Forms.CheckBox GPS1usegpsspeed;
        private System.Windows.Forms.CheckBox GPS1usegpsaltitude;
        private System.Windows.Forms.CheckBox GPS1fakegps;
        private System.Windows.Forms.TextBox GPS1fixrate;
        private System.Windows.Forms.TextBox GPS1bits;
        private System.Windows.Forms.TextBox GPS1baud;
        private System.Windows.Forms.ComboBox GPS1parity;
        private System.Windows.Forms.ComboBox GPS1type;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.Label Datalogcustomheadflable1;
        private System.Windows.Forms.Label Datalogcustomheadflable2;
        private System.Windows.Forms.Label Datalogcustomheadflable3;
        private System.Windows.Forms.Label Datalogcustomheadflable4;
        private System.Windows.Forms.Label Datalogcustomheadflable5;
        private System.Windows.Forms.Label Datalogcustomheadflable6;
        private System.Windows.Forms.Label Datalogcustomheadflable7;
        private System.Windows.Forms.Label Datalogcustomheadflable8;
        private System.Windows.Forms.GroupBox Datalogform1;
        private System.Windows.Forms.Label Datalogcustomflable18;
        private System.Windows.Forms.Label Datalogcustomflable17;
        private System.Windows.Forms.Label Datalogcustomflable19;
        private System.Windows.Forms.Label Datalogcustomflable20;
        private System.Windows.Forms.Label Datalogcustomflable21;
        private System.Windows.Forms.Label Datalogcustomflable22;
        private System.Windows.Forms.Label Datalogcustomflable23;
        private System.Windows.Forms.Label Datalogcustomflable24;
        private System.Windows.Forms.Label Datalogcustomflable15;
        private System.Windows.Forms.Label Datalogcustomflable14;
        private System.Windows.Forms.Label Datalogcustomflable13;
        private System.Windows.Forms.Label Datalogcustomflable16;
        private System.Windows.Forms.Label Datalogcustomflable4;
        private System.Windows.Forms.Label Datalogcustomflable5;
        private System.Windows.Forms.Label Datalogcustomflable6;
        private System.Windows.Forms.Label Datalogcustomflable7;
        private System.Windows.Forms.Label Datalogcustomflable8;
        private System.Windows.Forms.Label Datalogcustomflable9;
        private System.Windows.Forms.Label Datalogcustomflable10;
        private System.Windows.Forms.Label Datalogcustomflable11;
        private System.Windows.Forms.Label Datalogcustomflable12;
        private System.Windows.Forms.Label Datalogcustomflable2;
        private System.Windows.Forms.Label Datalogcustomflable1;
        private System.Windows.Forms.Label Datalogcustomflable3;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox Datalogcustomftext14;
        private System.Windows.Forms.TextBox Datalogcustomftext15;
        private System.Windows.Forms.TextBox Datalogcustomftext16;
        private System.Windows.Forms.TextBox Datalogcustomftext17;
        private System.Windows.Forms.TextBox Datalogcustomftext18;
        private System.Windows.Forms.TextBox Datalogcustomftext19;
        private System.Windows.Forms.TextBox Datalogcustomftext20;
        private System.Windows.Forms.TextBox Datalogcustomftext21;
        private System.Windows.Forms.TextBox Datalogcustomftext22;
        private System.Windows.Forms.TextBox Datalogcustomftext23;
        private System.Windows.Forms.TextBox Datalogcustomftext24;
        private System.Windows.Forms.TextBox Datalogcustomftext13;
        private System.Windows.Forms.TextBox Datalogcustomftext2;
        private System.Windows.Forms.TextBox Datalogcustomftext3;
        private System.Windows.Forms.TextBox Datalogcustomftext4;
        private System.Windows.Forms.TextBox Datalogcustomftext5;
        private System.Windows.Forms.TextBox Datalogcustomftext6;
        private System.Windows.Forms.TextBox Datalogcustomftext7;
        private System.Windows.Forms.TextBox Datalogcustomftext8;
        private System.Windows.Forms.TextBox Datalogcustomftext9;
        private System.Windows.Forms.TextBox Datalogcustomftext10;
        private System.Windows.Forms.TextBox Datalogcustomftext11;
        private System.Windows.Forms.TextBox Datalogcustomftext12;
        private System.Windows.Forms.TextBox Datalogcustomftext1;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.TextBox Datalogcustomheadfltext1;
        private System.Windows.Forms.TextBox Datalogcustomheadfltext2;
        private System.Windows.Forms.TextBox Datalogcustomheadfltext3;
        private System.Windows.Forms.TextBox Datalogcustomheadfltext4;
        private System.Windows.Forms.TextBox Datalogcustomheadfltext5;
        private System.Windows.Forms.TextBox Datalogcustomheadfltext6;
        private System.Windows.Forms.TextBox Datalogcustomheadfltext7;
        private System.Windows.Forms.ComboBox SBASsatellite;
        private System.Windows.Forms.ComboBox SBASnumberofchannel;
        private System.Windows.Forms.ComboBox ubloxdynamicmodel;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.TextBox Datalogcustomheadfltext8;
        private System.Windows.Forms.Button customuarts;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.CheckBox Datalogdonoterase;
        private System.Windows.Forms.CheckBox Datalogenablecustom;
        private System.Windows.Forms.CheckBox Dataloglogging;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.TextBox agllowthrottleswitch;
        private System.Windows.Forms.TextBox agllowthrottlesetting;
        private System.Windows.Forms.TextBox aglswitchaltitude;
        private System.Windows.Forms.ComboBox aglmode;
        private System.Windows.Forms.ComboBox aglsensor;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button open_vrsfile;
        private System.Windows.Forms.TextBox Vrsheadercommands;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label vrsname;


    }
}