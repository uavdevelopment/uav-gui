﻿namespace UAV_GUI.Forms.Setup.MP
{
    partial class GroundSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GroundSetup));
            this.SIM = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.simulaterTypecomboBox = new System.Windows.Forms.ComboBox();
            this.RepSimPortT = new System.Windows.Forms.TextBox();
            this.RepSimHostT = new System.Windows.Forms.TextBox();
            this.RepSimT = new System.Windows.Forms.TextBox();
            this.duratuinT = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ThrottleT = new System.Windows.Forms.TextBox();
            this.AltT = new System.Windows.Forms.TextBox();
            this.speedT = new System.Windows.Forms.TextBox();
            this.LngT = new System.Windows.Forms.TextBox();
            this.latT = new System.Windows.Forms.TextBox();
            this.headingT = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Communications = new System.Windows.Forms.TabPage();
            this.comPortBitRateComboBox = new System.Windows.Forms.ComboBox();
            this.serverPortNumT = new System.Windows.Forms.TextBox();
            this.hostPortT = new System.Windows.Forms.TextBox();
            this.IPT = new System.Windows.Forms.TextBox();
            this.comPortNumT = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.enableTCPcheckBox = new System.Windows.Forms.CheckBox();
            this.useTCPradioButton = new System.Windows.Forms.RadioButton();
            this.useCOMradioButton = new System.Windows.Forms.RadioButton();
            this.General = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.enable1028radioButton = new System.Windows.Forms.RadioButton();
            this.enable2xRadioButton = new System.Windows.Forms.RadioButton();
            this.exp2T = new System.Windows.Forms.TextBox();
            this.exp1T = new System.Windows.Forms.TextBox();
            this.exp2checkBox = new System.Windows.Forms.CheckBox();
            this.exp1checkBox = new System.Windows.Forms.CheckBox();
            this.autoPilotVreT = new System.Windows.Forms.TextBox();
            this.installetSimT = new System.Windows.Forms.TextBox();
            this.installedHorizonT = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.enableFlightReplyCheckBox = new System.Windows.Forms.CheckBox();
            this.disableTakeoffCheckBox = new System.Windows.Forms.CheckBox();
            this.enableVRSeditorCheckBox = new System.Windows.Forms.CheckBox();
            this.enableFlightEditorCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.editCustmShape = new System.Windows.Forms.Button();
            this.useCustmShapCheckBox = new System.Windows.Forms.CheckBox();
            this.unitCcomboBox = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.mapProviderComboBox = new System.Windows.Forms.ComboBox();
            this.graphGroubBox = new System.Windows.Forms.GroupBox();
            this.AirSpeedC = new System.Windows.Forms.CheckBox();
            this.HeadingC = new System.Windows.Forms.CheckBox();
            this.AltitudeC = new System.Windows.Forms.CheckBox();
            this.Airspeed = new System.Windows.Forms.Button();
            this.AirspeedL = new System.Windows.Forms.Label();
            this.HeadingB = new System.Windows.Forms.Button();
            this.HeadingL = new System.Windows.Forms.Label();
            this.altitudepicker = new System.Windows.Forms.Button();
            this.altitudeL = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.Save = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SIM.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.Communications.SuspendLayout();
            this.General.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.graphGroubBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // SIM
            // 
            this.SIM.Controls.Add(this.groupBox2);
            this.SIM.Controls.Add(this.groupBox1);
            this.SIM.Location = new System.Drawing.Point(4, 22);
            this.SIM.Name = "SIM";
            this.SIM.Padding = new System.Windows.Forms.Padding(3);
            this.SIM.Size = new System.Drawing.Size(947, 387);
            this.SIM.TabIndex = 0;
            this.SIM.Text = "SIM";
            this.SIM.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.simulaterTypecomboBox);
            this.groupBox2.Controls.Add(this.RepSimPortT);
            this.groupBox2.Controls.Add(this.RepSimHostT);
            this.groupBox2.Controls.Add(this.RepSimT);
            this.groupBox2.Controls.Add(this.duratuinT);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(270, 24);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(242, 223);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Simulation Ooptions";
            this.groupBox2.Visible = false;
            // 
            // simulaterTypecomboBox
            // 
            this.simulaterTypecomboBox.FormattingEnabled = true;
            this.simulaterTypecomboBox.Items.AddRange(new object[] {
            "a",
            "b",
            "c"});
            this.simulaterTypecomboBox.Location = new System.Drawing.Point(113, 30);
            this.simulaterTypecomboBox.Name = "simulaterTypecomboBox";
            this.simulaterTypecomboBox.Size = new System.Drawing.Size(108, 21);
            this.simulaterTypecomboBox.TabIndex = 22;
            // 
            // RepSimPortT
            // 
            this.RepSimPortT.Location = new System.Drawing.Point(113, 176);
            this.RepSimPortT.Name = "RepSimPortT";
            this.RepSimPortT.Size = new System.Drawing.Size(108, 20);
            this.RepSimPortT.TabIndex = 21;
            // 
            // RepSimHostT
            // 
            this.RepSimHostT.Location = new System.Drawing.Point(113, 143);
            this.RepSimHostT.Name = "RepSimHostT";
            this.RepSimHostT.Size = new System.Drawing.Size(108, 20);
            this.RepSimHostT.TabIndex = 20;
            // 
            // RepSimT
            // 
            this.RepSimT.Location = new System.Drawing.Point(113, 105);
            this.RepSimT.Name = "RepSimT";
            this.RepSimT.Size = new System.Drawing.Size(108, 20);
            this.RepSimT.TabIndex = 19;
            // 
            // duratuinT
            // 
            this.duratuinT.Location = new System.Drawing.Point(113, 65);
            this.duratuinT.Name = "duratuinT";
            this.duratuinT.Size = new System.Drawing.Size(108, 20);
            this.duratuinT.TabIndex = 18;
            this.duratuinT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 176);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Replacment sim port";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Replacment sim host";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 108);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Replacment sim";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Duration";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Simulater Type";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ThrottleT);
            this.groupBox1.Controls.Add(this.AltT);
            this.groupBox1.Controls.Add(this.speedT);
            this.groupBox1.Controls.Add(this.LngT);
            this.groupBox1.Controls.Add(this.latT);
            this.groupBox1.Controls.Add(this.headingT);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(24, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(234, 229);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Intial Conditions";
            // 
            // ThrottleT
            // 
            this.ThrottleT.Location = new System.Drawing.Point(106, 203);
            this.ThrottleT.Name = "ThrottleT";
            this.ThrottleT.Size = new System.Drawing.Size(108, 20);
            this.ThrottleT.TabIndex = 11;
            this.ThrottleT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // AltT
            // 
            this.AltT.Location = new System.Drawing.Point(106, 170);
            this.AltT.Name = "AltT";
            this.AltT.Size = new System.Drawing.Size(108, 20);
            this.AltT.TabIndex = 10;
            this.AltT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // speedT
            // 
            this.speedT.Location = new System.Drawing.Point(106, 132);
            this.speedT.Name = "speedT";
            this.speedT.Size = new System.Drawing.Size(108, 20);
            this.speedT.TabIndex = 9;
            this.speedT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // LngT
            // 
            this.LngT.Location = new System.Drawing.Point(106, 92);
            this.LngT.Name = "LngT";
            this.LngT.Size = new System.Drawing.Size(108, 20);
            this.LngT.TabIndex = 8;
            // 
            // latT
            // 
            this.latT.Location = new System.Drawing.Point(106, 54);
            this.latT.Name = "latT";
            this.latT.Size = new System.Drawing.Size(108, 20);
            this.latT.TabIndex = 7;
            // 
            // headingT
            // 
            this.headingT.Location = new System.Drawing.Point(106, 15);
            this.headingT.Name = "headingT";
            this.headingT.Size = new System.Drawing.Size(108, 20);
            this.headingT.TabIndex = 6;
            this.headingT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 204);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Throttle (%)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Altitude AGL (m)\r\n";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Speed (m/s)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Longitude";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Latitude";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Heading (deg)";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.SIM);
            this.tabControl1.Controls.Add(this.Communications);
            this.tabControl1.Controls.Add(this.General);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(2, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(955, 413);
            this.tabControl1.TabIndex = 0;
            // 
            // Communications
            // 
            this.Communications.Controls.Add(this.comPortBitRateComboBox);
            this.Communications.Controls.Add(this.serverPortNumT);
            this.Communications.Controls.Add(this.hostPortT);
            this.Communications.Controls.Add(this.IPT);
            this.Communications.Controls.Add(this.comPortNumT);
            this.Communications.Controls.Add(this.label16);
            this.Communications.Controls.Add(this.label15);
            this.Communications.Controls.Add(this.label14);
            this.Communications.Controls.Add(this.label13);
            this.Communications.Controls.Add(this.label12);
            this.Communications.Controls.Add(this.enableTCPcheckBox);
            this.Communications.Controls.Add(this.useTCPradioButton);
            this.Communications.Controls.Add(this.useCOMradioButton);
            this.Communications.Location = new System.Drawing.Point(4, 22);
            this.Communications.Name = "Communications";
            this.Communications.Padding = new System.Windows.Forms.Padding(3);
            this.Communications.Size = new System.Drawing.Size(555, 283);
            this.Communications.TabIndex = 1;
            this.Communications.Text = "Communications";
            this.Communications.UseVisualStyleBackColor = true;
            this.Communications.Click += new System.EventHandler(this.Communications_Click);
            // 
            // comPortBitRateComboBox
            // 
            this.comPortBitRateComboBox.FormattingEnabled = true;
            this.comPortBitRateComboBox.Items.AddRange(new object[] {
            "110",
            "300",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.comPortBitRateComboBox.Location = new System.Drawing.Point(273, 89);
            this.comPortBitRateComboBox.Name = "comPortBitRateComboBox";
            this.comPortBitRateComboBox.Size = new System.Drawing.Size(104, 21);
            this.comPortBitRateComboBox.TabIndex = 12;
            // 
            // serverPortNumT
            // 
            this.serverPortNumT.Enabled = false;
            this.serverPortNumT.Location = new System.Drawing.Point(273, 228);
            this.serverPortNumT.Name = "serverPortNumT";
            this.serverPortNumT.Size = new System.Drawing.Size(104, 20);
            this.serverPortNumT.TabIndex = 11;
            this.serverPortNumT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // hostPortT
            // 
            this.hostPortT.Enabled = false;
            this.hostPortT.Location = new System.Drawing.Point(273, 177);
            this.hostPortT.Name = "hostPortT";
            this.hostPortT.Size = new System.Drawing.Size(104, 20);
            this.hostPortT.TabIndex = 10;
            this.hostPortT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // IPT
            // 
            this.IPT.Enabled = false;
            this.IPT.Location = new System.Drawing.Point(273, 142);
            this.IPT.Name = "IPT";
            this.IPT.Size = new System.Drawing.Size(104, 20);
            this.IPT.TabIndex = 9;
            // 
            // comPortNumT
            // 
            this.comPortNumT.Location = new System.Drawing.Point(273, 52);
            this.comPortNumT.Name = "comPortNumT";
            this.comPortNumT.Size = new System.Drawing.Size(104, 21);
            this.comPortNumT.TabIndex = 8;
            this.comPortNumT.SelectedIndexChanged += new System.EventHandler(this.comPortNumT_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(159, 235);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 13);
            this.label16.TabIndex = 7;
            this.label16.Text = "Server Port #";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(159, 177);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Host Port #";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(159, 145);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "Host Name or IP";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(159, 91);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "COM Port Bit Rate";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(159, 57);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "COM# Port";
            // 
            // enableTCPcheckBox
            // 
            this.enableTCPcheckBox.AutoSize = true;
            this.enableTCPcheckBox.Enabled = false;
            this.enableTCPcheckBox.Location = new System.Drawing.Point(123, 204);
            this.enableTCPcheckBox.Name = "enableTCPcheckBox";
            this.enableTCPcheckBox.Size = new System.Drawing.Size(132, 17);
            this.enableTCPcheckBox.TabIndex = 2;
            this.enableTCPcheckBox.Text = "Enable TCP/IP Server";
            this.enableTCPcheckBox.UseVisualStyleBackColor = true;
            // 
            // useTCPradioButton
            // 
            this.useTCPradioButton.AutoSize = true;
            this.useTCPradioButton.Enabled = false;
            this.useTCPradioButton.Location = new System.Drawing.Point(123, 116);
            this.useTCPradioButton.Name = "useTCPradioButton";
            this.useTCPradioButton.Size = new System.Drawing.Size(139, 17);
            this.useTCPradioButton.TabIndex = 1;
            this.useTCPradioButton.Text = "Use TCP/IP link to UAV";
            this.useTCPradioButton.UseVisualStyleBackColor = true;
            // 
            // useCOMradioButton
            // 
            this.useCOMradioButton.AutoSize = true;
            this.useCOMradioButton.Checked = true;
            this.useCOMradioButton.Location = new System.Drawing.Point(113, 26);
            this.useCOMradioButton.Name = "useCOMradioButton";
            this.useCOMradioButton.Size = new System.Drawing.Size(127, 17);
            this.useCOMradioButton.TabIndex = 0;
            this.useCOMradioButton.TabStop = true;
            this.useCOMradioButton.Text = "Use COM link to UAV";
            this.useCOMradioButton.UseVisualStyleBackColor = true;
            // 
            // General
            // 
            this.General.Controls.Add(this.groupBox5);
            this.General.Controls.Add(this.groupBox4);
            this.General.Controls.Add(this.groupBox3);
            this.General.Location = new System.Drawing.Point(4, 22);
            this.General.Name = "General";
            this.General.Padding = new System.Windows.Forms.Padding(3);
            this.General.Size = new System.Drawing.Size(555, 283);
            this.General.TabIndex = 2;
            this.General.Text = "General";
            this.General.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.enable1028radioButton);
            this.groupBox5.Controls.Add(this.enable2xRadioButton);
            this.groupBox5.Controls.Add(this.exp2T);
            this.groupBox5.Controls.Add(this.exp1T);
            this.groupBox5.Controls.Add(this.exp2checkBox);
            this.groupBox5.Controls.Add(this.exp1checkBox);
            this.groupBox5.Controls.Add(this.autoPilotVreT);
            this.groupBox5.Controls.Add(this.installetSimT);
            this.groupBox5.Controls.Add(this.installedHorizonT);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Location = new System.Drawing.Point(17, 148);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(469, 132);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Component Versions";
            this.groupBox5.Visible = false;
            // 
            // enable1028radioButton
            // 
            this.enable1028radioButton.AutoSize = true;
            this.enable1028radioButton.Location = new System.Drawing.Point(262, 109);
            this.enable1028radioButton.Name = "enable1028radioButton";
            this.enable1028radioButton.Size = new System.Drawing.Size(91, 17);
            this.enable1028radioButton.TabIndex = 11;
            this.enable1028radioButton.TabStop = true;
            this.enable1028radioButton.Text = "Enable 1028g";
            this.enable1028radioButton.UseVisualStyleBackColor = true;
            // 
            // enable2xRadioButton
            // 
            this.enable2xRadioButton.AutoSize = true;
            this.enable2xRadioButton.Location = new System.Drawing.Point(55, 109);
            this.enable2xRadioButton.Name = "enable2xRadioButton";
            this.enable2xRadioButton.Size = new System.Drawing.Size(90, 17);
            this.enable2xRadioButton.TabIndex = 10;
            this.enable2xRadioButton.TabStop = true;
            this.enable2xRadioButton.Text = "Enable 2x28g";
            this.enable2xRadioButton.UseVisualStyleBackColor = true;
            // 
            // exp2T
            // 
            this.exp2T.Location = new System.Drawing.Point(372, 73);
            this.exp2T.Name = "exp2T";
            this.exp2T.Size = new System.Drawing.Size(91, 20);
            this.exp2T.TabIndex = 9;
            // 
            // exp1T
            // 
            this.exp1T.Location = new System.Drawing.Point(372, 48);
            this.exp1T.Name = "exp1T";
            this.exp1T.Size = new System.Drawing.Size(91, 20);
            this.exp1T.TabIndex = 8;
            // 
            // exp2checkBox
            // 
            this.exp2checkBox.AutoSize = true;
            this.exp2checkBox.Location = new System.Drawing.Point(289, 75);
            this.exp2checkBox.Name = "exp2checkBox";
            this.exp2checkBox.Size = new System.Drawing.Size(71, 17);
            this.exp2checkBox.TabIndex = 7;
            this.exp2checkBox.Text = "Expected";
            this.exp2checkBox.UseVisualStyleBackColor = true;
            // 
            // exp1checkBox
            // 
            this.exp1checkBox.AutoSize = true;
            this.exp1checkBox.Location = new System.Drawing.Point(289, 50);
            this.exp1checkBox.Name = "exp1checkBox";
            this.exp1checkBox.Size = new System.Drawing.Size(71, 17);
            this.exp1checkBox.TabIndex = 6;
            this.exp1checkBox.Text = "Expected";
            this.exp1checkBox.UseVisualStyleBackColor = true;
            // 
            // autoPilotVreT
            // 
            this.autoPilotVreT.Location = new System.Drawing.Point(158, 77);
            this.autoPilotVreT.Name = "autoPilotVreT";
            this.autoPilotVreT.Size = new System.Drawing.Size(91, 20);
            this.autoPilotVreT.TabIndex = 5;
            // 
            // installetSimT
            // 
            this.installetSimT.Location = new System.Drawing.Point(158, 48);
            this.installetSimT.Name = "installetSimT";
            this.installetSimT.Size = new System.Drawing.Size(91, 20);
            this.installetSimT.TabIndex = 4;
            this.installetSimT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // installedHorizonT
            // 
            this.installedHorizonT.Location = new System.Drawing.Point(158, 19);
            this.installedHorizonT.Name = "installedHorizonT";
            this.installedHorizonT.Size = new System.Drawing.Size(91, 20);
            this.installedHorizonT.TabIndex = 3;
            this.installedHorizonT.TextChanged += new System.EventHandler(this.TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(27, 80);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(129, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "AutoPilot Fimware Version";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(27, 51);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(130, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Installed Simulator Version";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(27, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(123, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Installed Horizon Version";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.enableFlightReplyCheckBox);
            this.groupBox4.Controls.Add(this.disableTakeoffCheckBox);
            this.groupBox4.Controls.Add(this.enableVRSeditorCheckBox);
            this.groupBox4.Controls.Add(this.enableFlightEditorCheckBox);
            this.groupBox4.Location = new System.Drawing.Point(254, 11);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(232, 131);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Features Options";
            // 
            // enableFlightReplyCheckBox
            // 
            this.enableFlightReplyCheckBox.AutoSize = true;
            this.enableFlightReplyCheckBox.Location = new System.Drawing.Point(25, 108);
            this.enableFlightReplyCheckBox.Name = "enableFlightReplyCheckBox";
            this.enableFlightReplyCheckBox.Size = new System.Drawing.Size(191, 17);
            this.enableFlightReplyCheckBox.TabIndex = 3;
            this.enableFlightReplyCheckBox.Text = "Enable Flight Reply Features (beta)";
            this.enableFlightReplyCheckBox.UseVisualStyleBackColor = true;
            // 
            // disableTakeoffCheckBox
            // 
            this.disableTakeoffCheckBox.AutoSize = true;
            this.disableTakeoffCheckBox.Location = new System.Drawing.Point(25, 80);
            this.disableTakeoffCheckBox.Name = "disableTakeoffCheckBox";
            this.disableTakeoffCheckBox.Size = new System.Drawing.Size(135, 17);
            this.disableTakeoffCheckBox.TabIndex = 2;
            this.disableTakeoffCheckBox.Text = "Disable Takeoff Button";
            this.disableTakeoffCheckBox.UseVisualStyleBackColor = true;
            // 
            // enableVRSeditorCheckBox
            // 
            this.enableVRSeditorCheckBox.AutoSize = true;
            this.enableVRSeditorCheckBox.Location = new System.Drawing.Point(25, 47);
            this.enableVRSeditorCheckBox.Name = "enableVRSeditorCheckBox";
            this.enableVRSeditorCheckBox.Size = new System.Drawing.Size(139, 17);
            this.enableVRSeditorCheckBox.TabIndex = 1;
            this.enableVRSeditorCheckBox.Text = "Enable VRS Field Editor";
            this.enableVRSeditorCheckBox.UseVisualStyleBackColor = true;
            // 
            // enableFlightEditorCheckBox
            // 
            this.enableFlightEditorCheckBox.AutoSize = true;
            this.enableFlightEditorCheckBox.Location = new System.Drawing.Point(25, 18);
            this.enableFlightEditorCheckBox.Name = "enableFlightEditorCheckBox";
            this.enableFlightEditorCheckBox.Size = new System.Drawing.Size(165, 17);
            this.enableFlightEditorCheckBox.TabIndex = 0;
            this.enableFlightEditorCheckBox.Text = "Enable Flight Files Text Editor";
            this.enableFlightEditorCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.editCustmShape);
            this.groupBox3.Controls.Add(this.useCustmShapCheckBox);
            this.groupBox3.Controls.Add(this.unitCcomboBox);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Location = new System.Drawing.Point(17, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(206, 136);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "User Prefrences";
            // 
            // editCustmShape
            // 
            this.editCustmShape.Location = new System.Drawing.Point(43, 98);
            this.editCustmShape.Name = "editCustmShape";
            this.editCustmShape.Size = new System.Drawing.Size(123, 21);
            this.editCustmShape.TabIndex = 3;
            this.editCustmShape.Text = "Edit Custom Shape  ...";
            this.editCustmShape.UseVisualStyleBackColor = true;
            this.editCustmShape.Visible = false;
            // 
            // useCustmShapCheckBox
            // 
            this.useCustmShapCheckBox.AutoSize = true;
            this.useCustmShapCheckBox.Location = new System.Drawing.Point(19, 62);
            this.useCustmShapCheckBox.Name = "useCustmShapCheckBox";
            this.useCustmShapCheckBox.Size = new System.Drawing.Size(157, 17);
            this.useCustmShapCheckBox.TabIndex = 2;
            this.useCustmShapCheckBox.Text = "Use Custom Shape for UAV";
            this.useCustmShapCheckBox.UseVisualStyleBackColor = true;
            this.useCustmShapCheckBox.Visible = false;
            // 
            // unitCcomboBox
            // 
            this.unitCcomboBox.FormattingEnabled = true;
            this.unitCcomboBox.Items.AddRange(new object[] {
            "a",
            "b",
            "c"});
            this.unitCcomboBox.Location = new System.Drawing.Point(95, 21);
            this.unitCcomboBox.Name = "unitCcomboBox";
            this.unitCcomboBox.Size = new System.Drawing.Size(94, 21);
            this.unitCcomboBox.TabIndex = 1;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(16, 24);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Units";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.graphGroubBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(555, 283);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Visualization";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.mapProviderComboBox);
            this.groupBox6.Location = new System.Drawing.Point(6, 150);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(151, 74);
            this.groupBox6.TabIndex = 29;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Map Type";
            // 
            // mapProviderComboBox
            // 
            this.mapProviderComboBox.FormattingEnabled = true;
            this.mapProviderComboBox.Location = new System.Drawing.Point(18, 26);
            this.mapProviderComboBox.Name = "mapProviderComboBox";
            this.mapProviderComboBox.Size = new System.Drawing.Size(127, 21);
            this.mapProviderComboBox.TabIndex = 25;
            this.mapProviderComboBox.SelectedIndexChanged += new System.EventHandler(this.mapProviderComboBox_SelectedIndexChanged);
            // 
            // graphGroubBox
            // 
            this.graphGroubBox.Controls.Add(this.AirSpeedC);
            this.graphGroubBox.Controls.Add(this.HeadingC);
            this.graphGroubBox.Controls.Add(this.AltitudeC);
            this.graphGroubBox.Controls.Add(this.Airspeed);
            this.graphGroubBox.Controls.Add(this.AirspeedL);
            this.graphGroubBox.Controls.Add(this.HeadingB);
            this.graphGroubBox.Controls.Add(this.HeadingL);
            this.graphGroubBox.Controls.Add(this.altitudepicker);
            this.graphGroubBox.Controls.Add(this.altitudeL);
            this.graphGroubBox.Location = new System.Drawing.Point(6, 6);
            this.graphGroubBox.Name = "graphGroubBox";
            this.graphGroubBox.Size = new System.Drawing.Size(151, 126);
            this.graphGroubBox.TabIndex = 0;
            this.graphGroubBox.TabStop = false;
            this.graphGroubBox.Text = "Graphing Options";
            // 
            // AirSpeedC
            // 
            this.AirSpeedC.AutoSize = true;
            this.AirSpeedC.Location = new System.Drawing.Point(6, 101);
            this.AirSpeedC.Name = "AirSpeedC";
            this.AirSpeedC.Size = new System.Drawing.Size(15, 14);
            this.AirSpeedC.TabIndex = 7;
            this.AirSpeedC.UseVisualStyleBackColor = true;
            this.AirSpeedC.CheckedChanged += new System.EventHandler(this.AltitudeC_CheckedChanged);
            // 
            // HeadingC
            // 
            this.HeadingC.AutoSize = true;
            this.HeadingC.Location = new System.Drawing.Point(6, 62);
            this.HeadingC.Name = "HeadingC";
            this.HeadingC.Size = new System.Drawing.Size(15, 14);
            this.HeadingC.TabIndex = 6;
            this.HeadingC.UseVisualStyleBackColor = true;
            this.HeadingC.CheckedChanged += new System.EventHandler(this.AltitudeC_CheckedChanged);
            // 
            // AltitudeC
            // 
            this.AltitudeC.AutoSize = true;
            this.AltitudeC.Location = new System.Drawing.Point(6, 27);
            this.AltitudeC.Name = "AltitudeC";
            this.AltitudeC.Size = new System.Drawing.Size(15, 14);
            this.AltitudeC.TabIndex = 1;
            this.AltitudeC.UseVisualStyleBackColor = true;
            this.AltitudeC.CheckedChanged += new System.EventHandler(this.AltitudeC_CheckedChanged);
            // 
            // Airspeed
            // 
            this.Airspeed.Location = new System.Drawing.Point(100, 97);
            this.Airspeed.Name = "Airspeed";
            this.Airspeed.Size = new System.Drawing.Size(36, 23);
            this.Airspeed.TabIndex = 5;
            this.Airspeed.UseVisualStyleBackColor = true;
            this.Airspeed.Click += new System.EventHandler(this.altitudepicker_Click);
            // 
            // AirspeedL
            // 
            this.AirspeedL.AutoSize = true;
            this.AirspeedL.Location = new System.Drawing.Point(33, 102);
            this.AirspeedL.Name = "AirspeedL";
            this.AirspeedL.Size = new System.Drawing.Size(53, 13);
            this.AirspeedL.TabIndex = 4;
            this.AirspeedL.Text = "Air Speed";
            // 
            // HeadingB
            // 
            this.HeadingB.Location = new System.Drawing.Point(100, 57);
            this.HeadingB.Name = "HeadingB";
            this.HeadingB.Size = new System.Drawing.Size(36, 23);
            this.HeadingB.TabIndex = 3;
            this.HeadingB.UseVisualStyleBackColor = true;
            this.HeadingB.Click += new System.EventHandler(this.altitudepicker_Click);
            // 
            // HeadingL
            // 
            this.HeadingL.AutoSize = true;
            this.HeadingL.Location = new System.Drawing.Point(33, 62);
            this.HeadingL.Name = "HeadingL";
            this.HeadingL.Size = new System.Drawing.Size(47, 13);
            this.HeadingL.TabIndex = 2;
            this.HeadingL.Text = "Heading";
            // 
            // altitudepicker
            // 
            this.altitudepicker.Location = new System.Drawing.Point(100, 22);
            this.altitudepicker.Name = "altitudepicker";
            this.altitudepicker.Size = new System.Drawing.Size(36, 23);
            this.altitudepicker.TabIndex = 1;
            this.altitudepicker.UseVisualStyleBackColor = true;
            this.altitudepicker.Click += new System.EventHandler(this.altitudepicker_Click);
            // 
            // altitudeL
            // 
            this.altitudeL.AutoSize = true;
            this.altitudeL.Location = new System.Drawing.Point(33, 27);
            this.altitudeL.Name = "altitudeL";
            this.altitudeL.Size = new System.Drawing.Size(42, 13);
            this.altitudeL.TabIndex = 0;
            this.altitudeL.Text = "Altitude";
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(364, 420);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(70, 23);
            this.Save.TabIndex = 1;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(499, 420);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Restore Defaults";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // GroundSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 497);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GroundSetup";
            this.Text = "GroundSetup";
            this.Load += new System.EventHandler(this.GroundSetup_Load);
            this.SIM.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.Communications.ResumeLayout(false);
            this.Communications.PerformLayout();
            this.General.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.graphGroubBox.ResumeLayout(false);
            this.graphGroubBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage General;
        private System.Windows.Forms.TextBox installetSimT;
        private System.Windows.Forms.TabPage SIM;
        private System.Windows.Forms.TabPage Communications;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox simulaterTypecomboBox;
        private System.Windows.Forms.TextBox RepSimPortT;
        private System.Windows.Forms.TextBox RepSimHostT;
        private System.Windows.Forms.TextBox RepSimT;
        private System.Windows.Forms.TextBox duratuinT;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox ThrottleT;
        private System.Windows.Forms.TextBox AltT;
        private System.Windows.Forms.TextBox speedT;
        private System.Windows.Forms.TextBox LngT;
        private System.Windows.Forms.TextBox latT;
        private System.Windows.Forms.TextBox headingT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox graphGroubBox;
        private System.Windows.Forms.Label altitudeL;
        private System.Windows.Forms.Button Airspeed;
        private System.Windows.Forms.Label AirspeedL;
        private System.Windows.Forms.Button HeadingB;
        private System.Windows.Forms.Label HeadingL;
        private System.Windows.Forms.Button altitudepicker;
        private System.Windows.Forms.CheckBox AirSpeedC;
        private System.Windows.Forms.CheckBox HeadingC;
        private System.Windows.Forms.CheckBox AltitudeC;


        private System.Windows.Forms.ComboBox comPortBitRateComboBox;
        private System.Windows.Forms.TextBox serverPortNumT;
        private System.Windows.Forms.TextBox hostPortT;
        private System.Windows.Forms.TextBox IPT;
        private System.Windows.Forms.ComboBox comPortNumT;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox enableTCPcheckBox;
        private System.Windows.Forms.RadioButton useTCPradioButton;
        private System.Windows.Forms.RadioButton useCOMradioButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button editCustmShape;
        private System.Windows.Forms.CheckBox useCustmShapCheckBox;
        private System.Windows.Forms.ComboBox unitCcomboBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox enableFlightReplyCheckBox;
        private System.Windows.Forms.CheckBox disableTakeoffCheckBox;
        private System.Windows.Forms.CheckBox enableVRSeditorCheckBox;
        private System.Windows.Forms.CheckBox enableFlightEditorCheckBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton enable1028radioButton;
        private System.Windows.Forms.RadioButton enable2xRadioButton;
        private System.Windows.Forms.TextBox exp2T;
        private System.Windows.Forms.TextBox exp1T;
        private System.Windows.Forms.CheckBox exp2checkBox;
        private System.Windows.Forms.CheckBox exp1checkBox;
        private System.Windows.Forms.TextBox autoPilotVreT;
        private System.Windows.Forms.TextBox installedHorizonT;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox6;
        public System.Windows.Forms.ComboBox mapProviderComboBox;

    }
}