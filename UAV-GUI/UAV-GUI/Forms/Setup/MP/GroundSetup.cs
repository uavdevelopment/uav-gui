﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FileClasses.INI_Files;
using Error_Reporting;

namespace UAV_GUI.Forms.Setup.MP
{
    public partial class GroundSetup : Form
    {

        List<string> unChangedFiledsValues;

        public GroundSetup()
        {
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
            IniFileHandler.read();
            unChangedFiledsValues = new List<string>();
          
            this.FormClosed += new FormClosedEventHandler(f_FormClosed);
          //  comPortNumT.Items.Add("COM3");
         //   comPortNumT.SelectedIndex = 0;
            loadIniFieldsData(false);
            
        }

        void f_FormClosed(object sender, FormClosedEventArgs e)
        {
            bool saveFlage = false;
            for (int i = 0; i < IniFileHandler.iniFieldsVaues.Count; i++)
            {
              unChangedFiledsValues.Add( IniFileHandler.iniFieldsVaues[i]);
            }

            saveChanges();
            for (int i = 0; i <unChangedFiledsValues.Count; i++)
            {
                if(!IniFileHandler.iniFieldsVaues[i].Equals(unChangedFiledsValues[i]))
                {
                   saveFlage = true;
                    break;
                }
            }
            
           
            if(saveFlage)
            {
                if (MessageBox.Show("Do You want to Save Changes ? ", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                  //  saveChanges();
                    IniFileHandler.write();
                    IniFileHandler.read();
                    MessageBox.Show(" Changes Saved, Please Restart The Application In order that All Changes can take effect ");
                   // unChangedFiledsValues = IniFileHandler.iniFieldsVaues;
                }
            }
       //     System.IO.File.Delete(@"status\iniFields2.ini");
        }

        void loadIniFieldsData(bool defaults)
        {
            if(!defaults)
            IniFileHandler.read();
            try
            {
               
                //SIM
                headingT.Text = INIData.Heading.ToString();//IniFileHandler.iniFieldsVaues[0].ToString();
                latT.Text = INIData.latitude.ToString();//IniFileHandler.iniFieldsVaues[1].ToString();
                LngT.Text = INIData.longitude.ToString();//IniFileHandler.iniFieldsVaues[2].ToString();
                speedT.Text = INIData.speed.ToString();//IniFileHandler.iniFieldsVaues[3].ToString();
                AltT.Text = INIData.altitudeAGL.ToString();//IniFileHandler.iniFieldsVaues[4].ToString();
                ThrottleT.Text = INIData.throttle.ToString();//IniFileHandler.iniFieldsVaues[5].ToString();

                simulaterTypecomboBox.SelectedIndex = int.Parse(IniFileHandler.iniFieldsVaues[6]);
                duratuinT.Text = IniFileHandler.iniFieldsVaues[7].ToString();
                RepSimT.Text = IniFileHandler.iniFieldsVaues[8].ToString();
                RepSimHostT.Text = IniFileHandler.iniFieldsVaues[9].ToString();
                RepSimPortT.Text = IniFileHandler.iniFieldsVaues[10].ToString();
                //Commuinication

                useCOMradioButton.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[11]) == 1) ? true : false;
                var ports = System.IO.Ports.SerialPort.GetPortNames();
                comPortNumT.DataSource = ports;

                comPortNumT.SelectedItem = IniFileHandler.iniFieldsVaues[12].ToString();
                comPortBitRateComboBox.SelectedItem = IniFileHandler.iniFieldsVaues[13].ToString();
                useTCPradioButton.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[14]) == 1) ? true : false;
                IPT.Text = IniFileHandler.iniFieldsVaues[15].ToString();
                hostPortT.Text = IniFileHandler.iniFieldsVaues[16].ToString();
                enableTCPcheckBox.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[17]) == 1) ? true : false;
                serverPortNumT.Text = IniFileHandler.iniFieldsVaues[18].ToString();
                //General
                unitCcomboBox.SelectedIndex = int.Parse(IniFileHandler.iniFieldsVaues[19]);
                useCustmShapCheckBox.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[20]) == 1) ? true : false;
                enableFlightEditorCheckBox.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[21]) == 1) ? true : false;
                enableVRSeditorCheckBox.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[22]) == 1) ? true : false;
                disableTakeoffCheckBox.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[23]) == 1) ? true : false;
                enableFlightReplyCheckBox.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[24]) == 1) ? true : false;
                installedHorizonT.Text = IniFileHandler.iniFieldsVaues[25].ToString();
                installetSimT.Text = IniFileHandler.iniFieldsVaues[26].ToString();
                autoPilotVreT.Text = IniFileHandler.iniFieldsVaues[27].ToString();
                enable2xRadioButton.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[28]) == 1) ? true : false;
                enable1028radioButton.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[29]) == 1) ? true : false;
                exp1checkBox.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[30]) == 1) ? true : false;
                exp2checkBox.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[31]) == 1) ? true : false;
                exp1T.Text = IniFileHandler.iniFieldsVaues[32].ToString();
                exp2T.Text = IniFileHandler.iniFieldsVaues[33].ToString();

                //Visualization 

                /*ChecK Boxes 
                 Tariq Shatat.
                 */
                
                /*
                AltitudeC.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[34]) == 1) ? true : false;
                HeadingC.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[38]) == 1) ? true : false;
                AirSpeedC.Checked = (int.Parse(IniFileHandler.iniFieldsVaues[42]) == 1) ? true : false;
                */
                AltitudeC.Checked = INIData.AltEnabled;
                HeadingC.Checked = INIData.HeadingEnabled;
                AirSpeedC.Checked = INIData.ASpeedEnabled;

                Airspeed.BackColor = INIData.aSColor;
                HeadingB.BackColor = INIData.hColor;
                altitudepicker.BackColor = INIData.aLColor;

                mapProviderComboBox.DataSource = GMap.NET.MapProviders.GMapProviders.List.ToArray();
                mapProviderComboBox.SelectedIndex = INIData.selectedMap;

                /*Altitde of Value 
                 Tariq Shatat.
                 */
                
                /*Airspeed.BackColor = Color.FromArgb((byte)int.Parse(IniFileHandler.readSpecificField("AirSpeedR")),
                   (byte) int.Parse(IniFileHandler.readSpecificField("AirSpeedG")),
                   (byte) int.Parse(IniFileHandler.readSpecificField("AirSpeedB")));

                HeadingB.BackColor = Color.FromArgb((byte)int.Parse(IniFileHandler.readSpecificField("HeadingR")),
                   (byte)int.Parse(IniFileHandler.readSpecificField("HeadingG")),
                   (byte)int.Parse(IniFileHandler.readSpecificField("HeadingB")));

                altitudepicker.BackColor = Color.FromArgb((byte)int.Parse(IniFileHandler.readSpecificField("AltitudeR")),
                   (byte)int.Parse(IniFileHandler.readSpecificField("AltitudeG")),
                   (byte)int.Parse(IniFileHandler.readSpecificField("AltitudeB")));
                */

                
            }catch (Exception e){
                return; 
                
            
            }
        }

        void saveChanges()
        {
           // //SIM
          IniFileHandler.iniFieldsVaues[0] = (headingT.Text);
          IniFileHandler.iniFieldsVaues[1] = (latT.Text);
           IniFileHandler.iniFieldsVaues[2] = (LngT.Text);
           IniFileHandler.iniFieldsVaues[3] = (speedT.Text);
           IniFileHandler.iniFieldsVaues[4] = (AltT.Text);
           IniFileHandler.iniFieldsVaues[5] =(ThrottleT.Text);
            
             IniFileHandler.iniFieldsVaues[6] =  simulaterTypecomboBox.SelectedIndex .ToString();
            IniFileHandler.iniFieldsVaues[7] =(duratuinT.Text);
            IniFileHandler.iniFieldsVaues[8] = (RepSimT.Text);
            IniFileHandler.iniFieldsVaues[9] = (RepSimHostT.Text);
            IniFileHandler.iniFieldsVaues[10] = (RepSimPortT.Text);
            //Commuinication

          IniFileHandler.iniFieldsVaues[11] = ( (useCOMradioButton.Checked == true) ? 1 :0).ToString();
            if(comPortNumT.SelectedItem!=null)
          IniFileHandler.iniFieldsVaues[12] = (comPortNumT.SelectedItem.ToString());
   IniFileHandler.iniFieldsVaues[13]=  comPortBitRateComboBox.SelectedItem.ToString();
            IniFileHandler.iniFieldsVaues[14] = ((useTCPradioButton.Checked == true) ? 1 : 0).ToString();
            IniFileHandler.iniFieldsVaues[15] = (IPT.Text);
            IniFileHandler.iniFieldsVaues[16] = (hostPortT.Text);
            IniFileHandler.iniFieldsVaues[17] = ((enableTCPcheckBox.Checked == true) ? 1 : 0).ToString();
            IniFileHandler.iniFieldsVaues[18] = (serverPortNumT.Text);
            //General
 IniFileHandler.iniFieldsVaues[19] =  unitCcomboBox.SelectedIndex .ToString();
            IniFileHandler.iniFieldsVaues[20] = ((useCustmShapCheckBox.Checked == true) ? 1 : 0).ToString();
            IniFileHandler.iniFieldsVaues[21] = ((enableFlightEditorCheckBox.Checked== true) ? 1 : 0).ToString();
            IniFileHandler.iniFieldsVaues[22] = ((enableVRSeditorCheckBox.Checked == true) ? 1 : 0).ToString();
            IniFileHandler.iniFieldsVaues[23] = ((disableTakeoffCheckBox.Checked == true) ? 1 : 0).ToString();
       IniFileHandler.iniFieldsVaues[24] =     ((enableFlightReplyCheckBox.Checked == true) ? 1 : 0).ToString();
            IniFileHandler.iniFieldsVaues[25] = (installedHorizonT.Text);
            IniFileHandler.iniFieldsVaues[26] = (installetSimT.Text);
            IniFileHandler.iniFieldsVaues[27] = (autoPilotVreT.Text);
            IniFileHandler.iniFieldsVaues[28] = ((enable2xRadioButton.Checked == true) ? 1 : 0).ToString();
            IniFileHandler.iniFieldsVaues[29] = ((enable1028radioButton.Checked == true) ? 1 : 0).ToString();;
            IniFileHandler.iniFieldsVaues[30] = ((exp1checkBox.Checked == true) ? 1 : 0).ToString();;
            IniFileHandler.iniFieldsVaues[31] = ((exp2checkBox.Checked == true) ? 1 : 0).ToString();;
            IniFileHandler.iniFieldsVaues[32] = (exp1T.Text);
            IniFileHandler.iniFieldsVaues[33] = (exp2T.Text);
            INIData.selectedMap = ((ComboBox)mapProviderComboBox).SelectedIndex;
            IniFileHandler.iniFieldsVaues[46] = "" + INIData.selectedMap;

         
        }

        private void TextChanged(object sender, EventArgs e)
        {

            double Num;
            TextBox triggeredT = sender as TextBox;
           
                    if (!double.TryParse(triggeredT.Text, out Num))
                    {
                        // triggeredT.Clear();
                        throw new BusinessException(1501, "Input is Either non Integer or Empty");
                    }
                    if (triggeredT == ThrottleT)
                    {
                       if( double.Parse( triggeredT.Text) <0 || double.Parse( triggeredT.Text)>100)
                           throw new BusinessException(1502, "Throttle Value is Out of Range");
                       }
        }
        
        private void altitudepicker_Click(object sender, EventArgs e)
        {
            Button clickedButton = sender as Button;
            Color X = new Color();
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                X = colorDialog1.Color;
                clickedButton.BackColor = X;

                if (clickedButton == this.altitudepicker)
                {
                    IniFileHandler.iniFieldsVaues[35] = X.R.ToString();
                    IniFileHandler.iniFieldsVaues[36] = X.G.ToString();
                    IniFileHandler.iniFieldsVaues[37] = X.B.ToString();

                }
                else if (clickedButton == this.HeadingB)
                {
                    IniFileHandler.iniFieldsVaues[39] = X.R.ToString();
                    IniFileHandler.iniFieldsVaues[40] = X.G.ToString();
                    IniFileHandler.iniFieldsVaues[41] = X.B.ToString();

                }
                else
                {
                    IniFileHandler.iniFieldsVaues[43] = X.R.ToString();
                    IniFileHandler.iniFieldsVaues[44] = X.G.ToString();
                    IniFileHandler.iniFieldsVaues[45] = X.B.ToString();
                }
            }

        }

        private void AltitudeC_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox clickedButton = sender as CheckBox;
            int currentIndex = -1;
            if (clickedButton == AltitudeC)
            {
                currentIndex = 34;
            }
            else if (clickedButton == HeadingC)
            {
                currentIndex = 38;
            }
            else if (clickedButton == AirSpeedC)
            {
                currentIndex = 42;
            }

            IniFileHandler.iniFieldsVaues[currentIndex] = (clickedButton.Checked ? "1" : "0");
        }
        private void Save_Click(object sender, EventArgs e)
        {
            saveChanges();
            IniFileHandler.write();
            IniFileHandler.read();
            MessageBox.Show(" Changes Saved ,Please Restart The Application In order that All Changes can take effect ");

        }

        private void GroundSetup_Load(object sender, EventArgs e)
        {

        }

        private void Communications_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            IniFileHandler.readDefaults();
            loadIniFieldsData(true);
            MessageBox.Show("Defaults Restored");
        }

        private void comPortNumT_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void mapProviderComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
           
           //
        }

        private void save_sim()
        {
            IniFileHandler.writeSpecificFieldsim("UAV", "gps pos N", latT.Text.ToString());
            IniFileHandler.writeSpecificFieldsim("UAV", "gps pos E", LngT.Text.ToString());
            IniFileHandler.writeSpecificFieldsim("UAV", "initial altitude", AltT.Text.ToString());
            IniFileHandler.writeSpecificFieldsim("UAV", "initial hdg", headingT.Text.ToString());
            IniFileHandler.writeSpecificFieldsim("UAV", "initial speed", speedT.Text.ToString());
            IniFileHandler.writeSpecificFieldsim("UAV", "initial throttle", ThrottleT.Text.ToString());
            

        
        }
        private void save_uav()
        {
            IniFileHandler.writeSpecificFielduav("UAV", "gps pos N", latT.Text.ToString());
            IniFileHandler.writeSpecificFielduav("UAV", "gps pos E", LngT.Text.ToString());
            IniFileHandler.writeSpecificFielduav("UAV", "initial altitude", AltT.Text.ToString());
            IniFileHandler.writeSpecificFielduav("UAV", "initial hdg", headingT.Text.ToString());
            IniFileHandler.writeSpecificFielduav("UAV", "initial speed", speedT.Text.ToString());
            IniFileHandler.writeSpecificFielduav("UAV", "initial throttle", ThrottleT.Text.ToString());
        
        }

       
    }
}
