﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UAV_GUI.FileClasses;
using System.Drawing.Drawing2D;
using Error_Reporting;
using FileClasses.VRS_Files;
using UAV_GUI.Utilities;
using AutoPilotMemory;
using System.Text.RegularExpressions;
using UAV_GUI.FlightClasses;
using UAV_GUI.Forms.Main_Screen;
using System.Windows.Forms;
using GUtilities;
using System.IO;
using GLobal;

namespace UAV_GUI.Forms
{

       
    public partial class MicroPilotSetup : Form
    {
        // for access vrs transmitted and recieve window

        OpenFileDialog openFileDialog1= new OpenFileDialog();
        
        static TransmitVRSFile myVRSReceiveForm;
        static TransmitVRSFile myVRSTransmitForm;

       public static string filename = "vrsFile.vrs";
        public static string fileName = @"vrs\vrsFile.vrs";

        VrsFileHandler vrsHandler;
        int[,] PID_temp = new int[16, 31];
        bool swapChange = false;
        double OldMin = 0, zeroNewMax = 2.35, zeroNewMin = 0.65, OldMax = 100 , travelMax , travelMin = 0.05;
        double zeroCurrnt;
        bool manualChange = true;
        bool servo4 = false, firstIinit = true;
        bool cont = false;
        int x = 0;
        bool changesFlag = false;

        public MicroPilotSetup(ref VrsFileHandler vrsHandler, Flight myFlight)
        {          


            InitializeComponent();

            myVRSReceiveForm = new TransmitVRSFile(myFlight.flyUAV, true);
            myVRSTransmitForm = new UAV_GUI.Forms.Main_Screen.TransmitVRSFile(myFlight.flyUAV, false);
            //    System.IO.File.Copy(@"vrs\vrsFile.vrs", @"vrs\vrsFile2.vrs", true);

            //ApplicationLookAndFeel.UseTheme(this);
            this.BackgroundImage = Image.FromFile("imgs/formbackground.png");
            foreach(Control c in this.Controls)
            {
                c.BackgroundImage = Image.FromFile("imgs/formbackground.png");
                foreach(Control x in c.Controls)
                {
                    x.BackgroundImage = Image.FromFile("imgs/formbackground.png");
                }
            }
            this.FormClosed += new FormClosedEventHandler(f_FormClosed);


            this.vrsHandler = vrsHandler;
            try
            {

                fillAllDataFields();
                vrsname.Text = filename;
            }
            catch
            {
                throw new BusinessException(1503, "Corrupted Configurations ");
            }

            AutoPilotUnites.Checked = true;
            Sch1T.ReadOnly = true;
            Sch2T.ReadOnly = true;
            Sch3T.ReadOnly = true;
            ScheduledT.ReadOnly = true;
            zeroCurrnt = z1.Value;

            try
            {
                Update_VrsLines_list();
            }
            catch
            {
                throw new BusinessException(1504, "Corrupted Configurations ");
            }

            cruiseSlider.ValueChanged += new EventHandler(SlidersValueChanged1);
            approachSlider.ValueChanged += new EventHandler(SlidersValueChanged2);
            descentSlider.ValueChanged += new EventHandler(SlidersValueChanged3);
            climbSlider.ValueChanged += new EventHandler(SlidersValueChanged4);
            takeoffSlider.ValueChanged += new EventHandler(SlidersValueChanged5);
            onGroundSlider.ValueChanged += new EventHandler(SlidersValueChanged6);
            fatalErrorSlider.ValueChanged += new EventHandler(SlidersValueChanged7);
            slewLimitSlider.ValueChanged += new EventHandler(SlidersValueChanged8);
           lockElavatorSlider .ValueChanged += new EventHandler(SlidersValueChanged9);
            lockAileronSlider.ValueChanged += new EventHandler(SlidersValueChanged10);
            lockRudderSlider.ValueChanged += new EventHandler(SlidersValueChanged11);
            z1.ValueChanged += new EventHandler(z1SlidersValueChanged);
            z2.ValueChanged += new EventHandler(z2SlidersValueChanged);
            z3.ValueChanged += new EventHandler(z3SlidersValueChanged);
            z4.ValueChanged += new EventHandler(z4SlidersValueChanged);
            z5.ValueChanged += new EventHandler(z5SlidersValueChanged);
            z6.ValueChanged += new EventHandler(z6SlidersValueChanged);
            z7.ValueChanged += new EventHandler(z7SlidersValueChanged);
            z8.ValueChanged += new EventHandler(z8SlidersValueChanged);
            t1.ValueChanged += new EventHandler(t1SlidersValueChanged);
            t2.ValueChanged += new EventHandler(t2SlidersValueChanged);
            t3.ValueChanged += new EventHandler(t3SlidersValueChanged);
            t4.ValueChanged += new EventHandler(t4SlidersValueChanged);
            t5.ValueChanged += new EventHandler(t5SlidersValueChanged);
            t6.ValueChanged += new EventHandler(t6SlidersValueChanged);
            t7.ValueChanged += new EventHandler(t7SlidersValueChanged);
            t8.ValueChanged += new EventHandler(t8SlidersValueChanged);
            throttleIdel.ValueChanged += new EventHandler(throttleIdelSlidersValueChanged);

            SelectedPIDLloopCombo.SelectedIndexChanged += new EventHandler(SelectedPIDLloopComboChanged);
            CompatiblityCheckBox.Visible = false;
            
            CalculateAlironCheckBox.CheckedChanged += new EventHandler(CalculateAlironCheckBoxChanged);
            ServoNumComboBox.SelectedIndexChanged += new EventHandler(ServoNumComboBoxChanged);
            servoRudderComboBox.SelectedIndexChanged += new EventHandler(servoRudderComboBoxChanged);
            ServoTypeComboBox.SelectedIndexChanged += new EventHandler(ServoTypeComboBoxChanged);
            EnableSevoLock.CheckedChanged += new EventHandler(EnableSevoLockCheckBoxChanged);
            swap1.CheckedChanged += new EventHandler(swap1CheckBoxChanged);
            swap2.CheckedChanged += new EventHandler(swap2CheckBoxChanged);
            swap3.CheckedChanged += new EventHandler(swap3CheckBoxChanged);
            swap4.CheckedChanged += new EventHandler(swap4CheckBoxChanged);
            swap5.CheckedChanged += new EventHandler(swap5CheckBoxChanged);
            swap6.CheckedChanged += new EventHandler(swap6CheckBoxChanged);
            swap7.CheckedChanged += new EventHandler(swap7CheckBoxChanged);
            swap8.CheckedChanged += new EventHandler(swap8CheckBoxChanged);
            CompatiblityCheckBox.CheckedChanged += new EventHandler(compatiblityChanged);
            
            levelFlightModecomboBox.SelectedIndexChanged += new EventHandler(levelFlightModecomboBoxChanged);

           changesFlag = false;
        }

        //void fireEventChanges(object sender, EventArgs e)
        //{
        //    /* Check For Changes in All Controller 
        //     * Khaled M.Fathy
        //     */
        //    TabControl s = this.tabControl1;
        //    for (int i = 0; i < s.TabPages.Count; i++)
        //    {
        //        foreach (Control c in s.TabPages[i].Controls)
        //        {
        //            if (c is GroupBox)
        //            {
        //                foreach (Control cc in c.Controls)
        //                {
        //                    if (cc is CheckBox)
        //                    {
        //                        ((CheckBox)cc).CheckedChanged += c_ControlChanged;
        //                    }
        //                    else if (cc is TextBox)
        //                    {
        //                        ((TextBox)cc).TextChanged += c_ControlChanged;
        //                    }
        //                    else if (cc is HScrollBar)
        //                    {
        //                        ((HScrollBar)cc).ValueChanged += c_ControlChanged;
        //                    }
        //                    else if (cc is ComboBox)
        //                    {
        //                        ((ComboBox)cc).SelectedIndexChanged += c_ControlChanged;
        //                    }
        //                    else if (cc is RadioButton)
        //                    {
        //                        ((RadioButton)cc).CheckedChanged += c_ControlChanged;
        //                    }
        //                }
        //            }
        //            else if (c is CheckBox)
        //            {
        //                ((CheckBox)c).CheckedChanged += c_ControlChanged;
        //            }
        //            else if (c is TextBox)
        //            {
        //                ((TextBox)c).TextChanged += c_ControlChanged;
        //            }
        //            else if (c is HScrollBar)
        //            {
        //                ((HScrollBar)c).ValueChanged += c_ControlChanged;
        //            }
        //            else if (c is ComboBox)
        //            {
        //                ((ComboBox)c).SelectedIndexChanged += c_ControlChanged;
        //            }
        //            else if (c is RadioButton)
        //            {
        //                ((RadioButton)c).CheckedChanged += c_ControlChanged;
        //            }
        //        }
        //    }
        //}

        //private void c_ControlChanged(object sender,EventArgs e)
        //{
        //    changesFlag = true;
        //}

        double zeroMapping(double value)
        {
            return value * 2000;
        }
        double reversZeroMapping(double value)
        {
            return value / 2000;
        }
        double travelMapping(double value)
        {
            return value * 4000;
        }
        double reversTravelMapping(double value)
        {
            return value / 4000;
        }

        double zeroInterpolation(double OldValue)

        {
           double zeroNewMin2;
            if (servo4)
                zeroNewMin2 = 0.6;
            else
                zeroNewMin2 = zeroNewMin;
            return  Math.Round( (((OldValue - OldMin) * (zeroNewMax - zeroNewMin2)) / (OldMax - OldMin)) + zeroNewMin2  , 2);
        }
        double zeroReversInterpolation(double value)
        {

            double x = (((value - zeroNewMin) * (OldMax - OldMin)) / (zeroNewMax - zeroNewMin)) + OldMin;
          return Math.Round(x ,2);
        }
        double travelInterpolation(double OldValue )
        {
            return Math.Round((((OldValue - OldMin) * (travelMax - travelMin)) / (OldMax - OldMin)) + travelMin ,2);
        }
        double travelReversInterpolation(double value)
        {
            double x = (((value - travelMin) * (OldMax - OldMin)) / (travelMax - travelMin)) + OldMin;
            return Math.Round(x,2);
        }
      

        #region slider Event

        void throttleIdelSlidersValueChanged(object Obj, EventArgs args)
        {          
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_THROTTLE_IDLE)].Parameter_value = (int)Math.Round(map(throttleIdel.Value, 0, 100, 4000, 2000));                 
            TIPercentage.Text = throttleIdel.Value.ToString() + " %"; ;
            p9.Text = Math.Round(map(throttleIdel.Value, 0, 100, 2, 1)).ToString() + " ms";
        }

        void resetTravelRange(double zeroValue)
        {
            double tmp1 = 2.4 - zeroValue;
            if (servo4)
            {
                travelMax = tmp1;
                return;
            }
            double tmp2 = zeroValue -0.6;

            if (tmp1 <= tmp2)
                travelMax = tmp1;
            else
                travelMax = tmp2;
        }

        void z1SlidersValueChanged(object Obj, EventArgs args)
        {

            if (z1.Value == 0 || z1.Value == 100)
                t1.Enabled = false;
            else
                t1.Enabled = true;

                double NewValue = zeroInterpolation(z1.Value);
                GlobalData.LogConsole(z1.Value + "  "+ NewValue);
                resetTravelRange(NewValue);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_1)].Parameter_value = (int)zeroMapping(NewValue);
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_9)].Parameter_value = (int)zeroMapping(NewValue);
                else

                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_17)].Parameter_value = (int)zeroMapping(NewValue);

                p1.Text = NewValue.ToString() + " +- " + travelInterpolation(t1.Value).ToString();         
        }

        void z2SlidersValueChanged(object Obj, EventArgs args)
        {

            if (z2.Value == 0 || z2.Value == 100)
                t2.Enabled = false;
            else
                t2.Enabled = true;
                double NewValue = zeroInterpolation(z2.Value);
                resetTravelRange(NewValue);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_2)].Parameter_value = (int)zeroMapping(NewValue);
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_10)].Parameter_value = (int)zeroMapping(NewValue);
                else
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_18)].Parameter_value = (int)zeroMapping(NewValue);

                p2.Text = NewValue.ToString() + " +- " + travelInterpolation(t2.Value).ToString();
            
        }
        void z3SlidersValueChanged(object Obj, EventArgs args)
        {

            if (z3.Value == 0 || z3.Value == 100)
                t3.Enabled = false;
            else
                t3.Enabled = true;

                double NewValue = zeroInterpolation(z3.Value);
                resetTravelRange(NewValue);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_3)].Parameter_value = (int)zeroMapping(NewValue);
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_11)].Parameter_value = (int)zeroMapping(NewValue);
                else
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_19)].Parameter_value = (int)zeroMapping(NewValue);
                p3.Text = NewValue.ToString() + " +- " + travelInterpolation(t3.Value).ToString();
            
        }
        void z4SlidersValueChanged(object Obj, EventArgs args)
        {

                servo4 = true;
                if (z4.Value == 0 || z4.Value == 100)
                    t4.Enabled = false;
                else
                    t4.Enabled = true;

                double NewValue = zeroInterpolation(z4.Value);
                resetTravelRange(NewValue);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_4)].Parameter_value = (int)zeroMapping(NewValue);
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_12)].Parameter_value = (int)zeroMapping(NewValue);
                else
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_20)].Parameter_value = (int)zeroMapping(NewValue);


                p4.Text = NewValue.ToString() + " + " + travelInterpolation(t4.Value).ToString();
                servo4 = false;

        }
        void z5SlidersValueChanged(object Obj, EventArgs args)
        {

            if (z5.Value == 0 || z5.Value == 100)
                t5.Enabled = false;
            else
                t5.Enabled = true;
                double NewValue = zeroInterpolation(z5.Value);
                resetTravelRange(NewValue);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_5)].Parameter_value = (int)zeroMapping(NewValue);
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_13)].Parameter_value = (int)zeroMapping(NewValue);
                else
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_21)].Parameter_value = (int)zeroMapping(NewValue);
                p5.Text = NewValue.ToString() + " +- " + travelInterpolation(t5.Value).ToString();
            
        }
        void z6SlidersValueChanged(object Obj, EventArgs args)
        {

            if (z6.Value == 0 || z6.Value == 100)
                t6.Enabled = false;
            else
                t6.Enabled = true;
                double NewValue = zeroInterpolation(z6.Value);
                resetTravelRange(NewValue);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_6)].Parameter_value = (int)zeroMapping(NewValue);
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_14)].Parameter_value = (int)zeroMapping(NewValue);
                else
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_22)].Parameter_value = (int)zeroMapping(NewValue);
                p6.Text = NewValue.ToString() + " +- " + travelInterpolation(t6.Value).ToString();
            
        }
        void z7SlidersValueChanged(object Obj, EventArgs args)
        {

            if (z7.Value == 0 || z7.Value == 100)
                t7.Enabled = false;
            else
                t7.Enabled = true;

            
                double NewValue = zeroInterpolation(z7.Value);
                resetTravelRange(NewValue);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_7)].Parameter_value = (int)zeroMapping(NewValue);
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_15)].Parameter_value = (int)zeroMapping(NewValue);
                else
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_23)].Parameter_value = (int)zeroMapping(NewValue);
                p7.Text = NewValue.ToString() + " +- " + travelInterpolation(t7.Value).ToString();
            
        }
        void z8SlidersValueChanged(object Obj, EventArgs args)
        {

            if (z8.Value == 0 || z8.Value == 100)
                t8.Enabled = false;
            else
                t8.Enabled = true;

                double NewValue = zeroInterpolation(z8.Value);
                resetTravelRange(NewValue);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_8)].Parameter_value = (int)zeroMapping(NewValue);
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_16)].Parameter_value = (int)zeroMapping(NewValue);
                else
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_ZERO_24)].Parameter_value = (int)zeroMapping(NewValue);
                p8.Text = NewValue.ToString() + " +- " + travelInterpolation(t8.Value).ToString();
            
        }

        void t1SlidersValueChanged(object Obj, EventArgs args)
        {

        }

        void t2SlidersValueChanged(object Obj, EventArgs args)
        {
           
                resetTravelRange(zeroInterpolation(z2.Value));
                double NewValue = travelInterpolation(t2.Value);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_2)].Parameter_value = (int)(((swap2.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue)));
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_10)].Parameter_value = (int)(((swap2.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue)));
                else
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_18)].Parameter_value = (int)(((swap2.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue)));
                p2.Text = zeroInterpolation(z2.Value).ToString() + " +- " + NewValue.ToString();
            
        }
        void t3SlidersValueChanged(object Obj, EventArgs args)
        {
            
                resetTravelRange(zeroInterpolation(z3.Value));

                double NewValue = travelInterpolation(t3.Value);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_3)].Parameter_value = (int)((swap3.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_11)].Parameter_value = (int)((swap3.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));
                else
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_19)].Parameter_value = (int)((swap3.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));
                p3.Text = zeroInterpolation(z3.Value).ToString() + " +- " + NewValue.ToString();
            
        }
        void t4SlidersValueChanged(object Obj, EventArgs args)
        {

            servo4 = true;

                resetTravelRange(zeroInterpolation(z4.Value));
                double NewValue = travelInterpolation(t4.Value);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_4)].Parameter_value = (int)((swap4.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_12)].Parameter_value = (int)((swap4.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));
                else
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_20)].Parameter_value = (int)((swap4.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));

                p4.Text = zeroInterpolation(z4.Value).ToString() + " + " + NewValue.ToString();
                servo4 = false;

                p4.Text = zeroInterpolation(z4.Value).ToString() + " +- " + NewValue.ToString();
        }
        void t5SlidersValueChanged(object Obj, EventArgs args)
        {
            
                resetTravelRange(zeroInterpolation(z5.Value));
                double NewValue = travelInterpolation(t5.Value);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_5)].Parameter_value = (int)((swap5.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_13)].Parameter_value = (int)((swap5.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));
                else
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_21)].Parameter_value = (int)((swap5.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));
                p5.Text = zeroInterpolation(z5.Value).ToString() + " +- " + NewValue.ToString();
            
        }
        void t6SlidersValueChanged(object Obj, EventArgs args)
        {
            
                resetTravelRange(zeroInterpolation(z6.Value));

                double NewValue = travelInterpolation(t6.Value);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_6)].Parameter_value = (int)((swap6.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_14)].Parameter_value = (int)((swap6.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));
                else
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_22)].Parameter_value = (int)((swap6.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));
                p6.Text = zeroInterpolation(z6.Value).ToString() + " +- " + NewValue.ToString();
            
        }
        void t7SlidersValueChanged(object Obj, EventArgs args)
        {
            
                resetTravelRange(zeroInterpolation(z7.Value));

                double NewValue = travelInterpolation(t7.Value);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_7)].Parameter_value = (swap7.Checked == true) ? (int)-1 * (int)travelMapping(NewValue) : (int)travelMapping(NewValue);
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_15)].Parameter_value = (swap7.Checked == true) ? (int)-1 * (int)travelMapping(NewValue) : (int)travelMapping(NewValue);
                else
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_23)].Parameter_value = (swap7.Checked == true) ? (int)-1 * (int)travelMapping(NewValue) : (int)travelMapping(NewValue);
                p7.Text = zeroInterpolation(z7.Value).ToString() + " +- " + NewValue.ToString();
            
        }
        void t8SlidersValueChanged(object Obj, EventArgs args)
        {
            
                resetTravelRange(zeroInterpolation(z8.Value));

                double NewValue = travelInterpolation(t8.Value);
                if (ServoNumComboBox.SelectedIndex == 0)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_8)].Parameter_value = (int)((swap8.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));
                else if (ServoNumComboBox.SelectedIndex == 1)
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_16)].Parameter_value = (int)((swap8.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));
                else
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_24)].Parameter_value = (int)((swap8.Checked == true) ? (int)-1 * travelMapping(NewValue) : (int)travelMapping(NewValue));
                p8.Text = zeroInterpolation(z8.Value).ToString() + " +- " + NewValue.ToString();
            
        }

        void SlidersValueChanged1( object Obj , EventArgs args  )
        {
            cruiseL.Text = cruiseSlider.Value.ToString() + "%";
        }
        void SlidersValueChanged2(object Obj, EventArgs args)
        {
            approachL.Text = approachSlider.Value.ToString() + "%";
        }
        void SlidersValueChanged3(object Obj, EventArgs args)
        {
            descentL.Text = descentSlider.Value.ToString() + "%";
        }
        void SlidersValueChanged4(object Obj, EventArgs args)
        {
           climbL .Text = climbSlider.Value.ToString() + "%";
        }
        void SlidersValueChanged5(object Obj, EventArgs args)
        {
            takeoffL.Text = takeoffSlider.Value.ToString() + "%";
        }
        void SlidersValueChanged6(object Obj, EventArgs args)
        {
            onGroundL.Text = onGroundSlider.Value.ToString() + "%";
        }
        void SlidersValueChanged7(object Obj, EventArgs args)
        {
            fatalErrorMaxL.Text = fatalErrorSlider.Value.ToString() + "%";
        }
        void SlidersValueChanged8(object Obj, EventArgs args)
        {
            slweLimitL.Text = slewLimitSlider.Value.ToString() + "%";
        }
        void SlidersValueChanged9(object Obj, EventArgs args)
        {
            lockElvatorL.Text = lockElavatorSlider.Value.ToString() + "%";
        }
        void SlidersValueChanged10(object Obj, EventArgs args)
        {
            lockAileronL.Text = lockAileronSlider.Value.ToString() + "%";
        }
        void SlidersValueChanged11(object Obj, EventArgs args)
        {
            lockRudderL.Text = lockRudderSlider.Value.ToString() + "%";
        }

        #endregion

        void levelFlightModecomboBoxChanged(object Obj, EventArgs args)
        {
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_LEVEL_FLIGHT_MODE)].Parameter_value = levelFlightModecomboBox.SelectedIndex;            
        }

        //int  percentageMapping(double min, double max, double value)
        //{
        //    double newValue = ((value - min) / (max - min)) * 100 ;
        //    return (int)newValue;
        //}

        //int reversePercentageMapping(double min, double max, double value)
        //{
        //    double newValue = ((value / 100) * (max - min)) + min;
        //    return (int)newValue;
        //}
        double map(int x, int in_min, int in_max, int out_min, int out_max)
        {
            double va = (x - in_min) * (out_max - out_min) ;
            double val = (in_max - in_min);
            val = va / val+out_min;
            return val;
        }
        double map(double x, double in_min, double in_max, int out_min, int out_max)
        {
            double va = (x - in_min) * (out_max - out_min);
            double val = (in_max - in_min);
            val = va / val + out_min;
            return val;
        }

        public void fillAllDataFields()
        {
            manualChange = false;

            // AGL SENSOR
            aglsensor.SelectedIndex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_USE_AGL);
            aglmode.SelectedIndex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_AGL_MODE);
            aglswitchaltitude.Text = UnitConversion.feet_meters(  vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_AGL_SWITCH_ALTITUDE)).ToString();
            agllowthrottlesetting.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_AGL_LOW_THROTTLE_SETTING).ToString();
            agllowthrottleswitch.Text =((int) UnitConversion.feet_meters(  vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_AGL_LOW_THROTTLE_SWITCH_ALTITUDE))).ToString();


            // GPS 
            int gps1parityindex=0;

            GPS1type.SelectedIndex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GPS_TYPE);
          gps1parityindex  = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GPS_PARITY);

          if (gps1parityindex == 0)
              GPS1parity.SelectedIndex = 0;
          else if (gps1parityindex == 2)
              GPS1parity.SelectedIndex = 1;
          else if (gps1parityindex == 3)
              GPS1parity.SelectedIndex = 2;
            GPS1baud.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GPS_BAUDRATE).ToString();
            GPS1bits.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GPS_BITS).ToString();
            GPS1fixrate.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GPS_FIX_RATE).ToString();

            GPS1fakegps.Checked = BitField.isSet(0, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_FAKE_GPS)));


            GPS1usegpsaltitude.Checked = BitField.isSet(1, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_USE_GPS_DATA)));
            GPS1usegpsspeed.Checked = BitField.isSet(2, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_USE_GPS_DATA)));
            GPS1kalmanfilter.Checked = BitField.isSet(3, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_USE_GPS_DATA)));
            GPS1hemisphere.Checked = BitField.isSet(4, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_USE_GPS_DATA)));

            int gps2parityindex = 0;
            gps2parityindex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GPS_PORT_B_PARITY);

            if (gps2parityindex == 0)
                GPS2parity.SelectedIndex = 0;
            else if (gps2parityindex == 2)
                GPS2parity.SelectedIndex = 1;
            else if (gps2parityindex == 3)
                GPS2parity.SelectedIndex = 2;

            GPS2flowcontrol.SelectedIndex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GPS_PORT_B_FLOW);
            GPS2baud.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GPS_PORT_B_BAUD).ToString();
            GPS2bits.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GPS_PORT_B_BITS).ToString();
            GPS2stopbits.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GPS_PORT_B_STOP).ToString();
            ubloxdynamicmodel.SelectedIndex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GPS_DYNAMIC_MODEL);


            // To be checked for value that accept return checked
            SBASenable.Checked = BitField.isSet(0, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SBAS_ENABLE)));
            SBASapplyintegrity.Checked = BitField.isSet(1, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SBAS_ENABLE)));
            SBASusetestmode.Checked = BitField.isSet(2, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SBAS_ENABLE)));

            int satalliateindex = 0;
        //    SBASnumberofchannel.SelectedIndex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SBAS_ENABLE);
            satalliateindex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SBAS_ENABLE);

            if (BitField.isSet(8, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SBAS_ENABLE))))
            {  
             SBASsatellite.SelectedIndex = 1;
            }
            else if (BitField.isSet(9, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SBAS_ENABLE))))
            {  
             SBASsatellite.SelectedIndex = 2;
            }
            else if (BitField.isSet(9, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SBAS_ENABLE))) && BitField.isSet(8, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SBAS_ENABLE))))
            {  
             SBASsatellite.SelectedIndex = 3;
            }
            SBASenterPRNs.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SBAS_ENABLE).ToString();


            Novatelenablediasblenovatel.Checked = BitField.isSet(0,(int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)));
            Novatelenablecorrection.Checked = BitField.isSet(1, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)));
            Novatelminimumspeed.Checked = BitField.isSet(2, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)));
            Novatelusel1flaot.Checked = BitField.isSet(3, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)));
            Novatelbasestation.Checked = BitField.isSet(4, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)));
            Novatelenablediableothe.Checked = BitField.isSet(5, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)));

            //comms 
            int Parityindex = 0;
            Gcsbaud.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GCS_BAUDRATE).ToString();
            Gcsflowcontrol.SelectedIndex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GCS_XON_XOFF);
           Parityindex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GCS_PARITY);
           if (Parityindex == 0)
               GcsParity.SelectedIndex = 0;
           else if(Parityindex==2)
               GcsParity.SelectedIndex = 1;
           else if (Parityindex == 3)
               GcsParity.SelectedIndex = 2;
            Gcschecksum.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GCS_CRC_ENABLE) == 1) ? true : false;
            GcsStrenghtchannel.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_RSSI_CHANNEL).ToString();
            GcsStrenghtminimum.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_RSSI_MIN).ToString();
            GcsCommandrepeat.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_TEXT_COMMAND_REPEAT).ToString();
            GcsPadCharacter.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_COM_PAD_CHARACTERS).ToString();
            Gcsintialreport.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_FORCE_INITIAL_REPORT).ToString();
            Gcsidentifier.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_UAV_IDENTIFIER).ToString();
            Gcsphonefirst.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_PHONE_NUMBER_FIRST).ToString();
            Gcsphonesecond.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_PHONE_NUMBER_LAST).ToString();
            Gcsenabledialing.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DIALING_INSTRUCTIONS).ToString();
            Gcstimeount.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_ANSWER_MODEM_TIMEOUT).ToString();
            // COMMS  UART1 
            UART1protocol.SelectedIndex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_UART_1_PROTOCOL);
            UART1Rxchannel.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_ENABLE_RX_UART_1).ToString();
            UART1Rxbaud.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_RX_UART_1_BAUDRATE).ToString();
            UART1Rxdatabits.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_RX_UART_1_DATABIT).ToString();
            UART1Rxparity.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_RX_UART_1_PARITY).ToString();
            UART1Txchannel.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_ENABLE_TX_UART_1).ToString();
            UART1Txbaud.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_TX_UART_1_BAUDRATE).ToString();
            UART1Txbatabits.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_TX_UART_1_DATA_BIT).ToString();
            UART1Txparity.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_TX_UART_1_PARITY).ToString();
            UART1Txtimeout.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_ANSWER_MODEM_TIMEOUT_UART_1).ToString();
            UART1protocolfield.SelectedIndex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_UART_FIELD_LOCATION_1);
            UART1disablebinding.Checked = BitField.isSet(1, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_BIND_OPTIONS)));

            // COMMS  UART2 
            UART2protocol.SelectedIndex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_UART_2_PROTOCOL);
            UART2Rxchannel.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_ENABLE_RX_UART_2).ToString();
            UART2Rxbaud.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_RX_UART_2_BAUDRATE).ToString();
            UART2Rxdatabits.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_RX_UART_2_DATA_BIT).ToString();
            UART2Rxparity.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_RX_UART_2_PARITY).ToString();
            UART2Txchannel.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_ENABLE_TX_UART_2).ToString();
            UART2Txbaud.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_TX_UART_2_BAUDRATE).ToString();
            UART2Txdatabits.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_TX_UART_2_DATA_BIT).ToString();
            UART2Txparity.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_TX_UART_2_PARITY).ToString();
            UART2Txtimeout.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_ANSWER_MODEM_TIMEOUT_UART_2).ToString();
            UART2Txprotocolfield.SelectedIndex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_UART_FIELD_LOCATION_2);
            UART2disablebinding.Checked = BitField.isSet(2, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_BIND_OPTIONS)));

            EnableBindingfeature.Checked = BitField.isSet(0, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_BIND_OPTIONS)));

            LRCuselrc.Checked = BitField.isSet(0, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_USE_LRC_BOARD)));
            SLRCsecondary.Checked = BitField.isSet(1, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_USE_LRC_BOARD)));
            MLRCenterfailure.Checked = BitField.isSet(2, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_USE_LRC_BOARD)));
            SLRCenterfailure.Checked = BitField.isSet(3, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_USE_LRC_BOARD)));

            LRConlyreadservos.Checked = BitField.isSet(0, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_LRC_CONFIG_MODE)));
            LRCsecodarycomport.SelectedIndex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_LRC_CONFIG_OTHER_CHANNEL);
            Vrsheadercommands.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_VRS_LOCK_OPTIONS).ToString();
           // Vrsheadercommand.Checked = BitField.isSet(0, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_VRS_LOCK_OPTIONS)));
            Vrsfootercommands.Checked = BitField.isSet(10, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_VRS_LOCK_OPTIONS)));
            
            //DATALOG 
            Dataloglogging.Checked = BitField.isSet(0, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_TYPE)));
            Datalogenablecustom.Checked = BitField.isSet(1, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_TYPE)));
            Datalogdonoterase.Checked = BitField.isSet(0, (int)(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DONT_ERASE_DATALOG)));
            Datalogcustomftext1.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_1).ToString();
            Datalogcustomftext2.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_2).ToString();
            Datalogcustomftext3.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_3).ToString();
            Datalogcustomftext4.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_4).ToString();
            Datalogcustomftext5.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_5).ToString();
            Datalogcustomftext6.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_6).ToString();
            Datalogcustomftext7.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_7).ToString();
            Datalogcustomftext8.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_8).ToString();
            Datalogcustomftext9.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_9).ToString();
            Datalogcustomftext10.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_10).ToString();
            Datalogcustomftext11.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_11).ToString();
            Datalogcustomftext12.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_12).ToString();
            Datalogcustomftext13.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_13).ToString();
            Datalogcustomftext14.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_14).ToString();
            Datalogcustomftext15.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_15).ToString();
            Datalogcustomftext16.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_16).ToString();
            Datalogcustomftext17.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_17).ToString();
            Datalogcustomftext18.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_18).ToString();
            Datalogcustomftext19.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_19).ToString();
            Datalogcustomftext20.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_20).ToString();
            Datalogcustomftext21.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_21).ToString();
            Datalogcustomftext22.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_22).ToString();
            Datalogcustomftext23.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_23).ToString();
            Datalogcustomftext24.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_USER_FIELD_24).ToString();

            Datalogcustomheadfltext1.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_1).ToString();
            Datalogcustomheadfltext2.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_2).ToString();
            Datalogcustomheadfltext3.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_3).ToString();
            Datalogcustomheadfltext4.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_4).ToString();
            Datalogcustomheadfltext5.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_5).ToString();
            Datalogcustomheadfltext6.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_6).ToString();
            Datalogcustomheadfltext7.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_7).ToString();
            Datalogcustomheadfltext8.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_8).ToString();
            //   Flight : 
            climbSpeedT.Text = UnitConversion.fPerS_kmPerH(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_CLIMB_SPEED)).ToString();
            descentSpeedT.Text = UnitConversion.fPerS_kmPerH(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DESCENT_SPEED)).ToString();
            wpReduceTurnT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_WAYPOINT_REDUCE_TURN).ToString();
            descentRateT.Text =  UnitConversion.ftPerS_mPerMin(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DESCENT_RATE)).ToString();
            wpDiaT.Text =  UnitConversion.feet_meters( vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_WAYPOINT_DIAMETER)).ToString();
            takeoffPichT.Text = UnitConversion.rad_deg( vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_TAKEOFF_PITCH)).ToString();
            lockedPichtT.Text = UnitConversion.rad_deg(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_LOCKED_PITCH)).ToString();
            offGroundAlgoT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_OFF_GROUND_ALGORITHM).ToString();
            maxLunchPlatformSpeedT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_MAX_LAUNCH_PLATFORM_SPEED).ToString();
            northFenceT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_FENCE_NORTH).ToString();
            eastFenceT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_FENCE_EAST).ToString();
            westFenceT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_FENCE_WEST).ToString();
            southFenceT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_FENCE_SOUTH).ToString();
            maxTargrtAltT.Text = UnitConversion.feet_meters( vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_MAX_TARGET_ALTITUDE)).ToString();
            maxTargetSpeedT.Text =UnitConversion.fPerS_kmPerH( vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_MAX_TARGET_SPEED)).ToString();
            minTargetAltT.Text = UnitConversion.feet_meters(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_MIN_TARGET_ALTITUDE)).ToString();
            minTargetSpeedT.Text = UnitConversion.fPerS_kmPerH( vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_MIN_TARGET_SPEED)).ToString();
            stallSpeedT.Text =UnitConversion.fPerS_kmPerH(  vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_STALL_SPEED)).ToString();
            onGroundSpeedT.Text =UnitConversion.fPerS_kmPerH(  vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_ON_GROUND_SPEED)).ToString();
            maxFlightAltT.Text =UnitConversion.feet_meters( vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_MAXIMUM_FLIGHT_ALTITUDE)).ToString();
            cruiseSpeedT.Text =UnitConversion.fPerS_kmPerH( vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_CRUISE_SPEED)).ToString();
            maxSpeedIncreasT.Text = UnitConversion.fPerS_kmPerH( vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_MAX_SPEED_INCREASE)).ToString();
            descentTypeT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DESCENT_TYPE).ToString();
            enableDeadcheckBox.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_USE_DEAD_RECKONING) == 1) ? true : false;
            flyTosFromTosLineUp.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_MAKE_FLYTO_FROMTOS) == 1) ? true : false;
            fromTosToFlyTos.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DISABLE_FROMTO) == 1) ? true : false;
            alwaysUseLineClimb.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_ALWAYS_USE_LINE_CLIMB) == 1) ? true : false;
            invertFenceNScheckBox.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_INVERT_FENCE_NORTHSOUTH) == 1) ? true : false;
            invertFenceEWcheckBox.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_INVERT_FENCE_EASTWEST) == 1) ? true : false;
            rotationSpeedT.Text = UnitConversion.fPerS_kmPerH(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_ROTATION_SPEED)).ToString();
            allowNnavigationAirSpeedcheckBox.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_ALLOW_NEGATIVE_AIRSPEED) == 1) ? true : false;
           maxFlightDistT.Text = UnitConversion.feet_meters( vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_MAX_DIST_TO_WPT)).ToString();
          //**
            levelFlightModecomboBox.SelectedIndex = (int) vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_LEVEL_FLIGHT_MODE);
            cimbMarginT.Text = UnitConversion.feet_meters(  vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_CLIMB_MARGIN)).ToString();

            double value = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_CRUISE_THROTTLE);
            double max = 32767;
            Console.WriteLine("1 " + value);
            
            cruiseSlider.Value = (int)Math.Round( map(value , 0 , 32767,0,100));
            cruiseL.Text = (cruiseSlider.Value).ToString() + "%";

             value = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_APPROACH_THROTTLE);
             approachSlider.Value = (int)Math.Round( map((int) value , 0 ,32767 ,0,100));
             approachL.Text = approachSlider.Value.ToString() + "%";

             value = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_CLIMB_THROTTLE);
             climbSlider.Value = (int)Math.Round( map((int) value , 0 , 32767,0,100));
             climbL.Text = climbSlider.Value.ToString() + "%";

             value = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DESCENT_THROTTLE);
             descentSlider.Value = (int)Math.Round( map((int) value , 0 ,32767 ,0,100));
             descentL.Text = descentSlider.Value.ToString() + "%";

             value = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_TAKEOFF_THROTTLE);
             takeoffSlider.Value = (int) Math.Round(map((int) value , 0 ,32767 ,0,100));
             takeoffL.Text = takeoffSlider.Value.ToString() + "%";

             value = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.onGround);
             onGroundSlider.Value = (int)Math.Round(map((int)value, 0, 32767, 0, 100));
             onGroundL.Text = onGroundSlider.Value.ToString() + "%";

             value = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_TH_SLEW_LIMIT);
             slewLimitSlider.Value = (int)Math.Round( map((int) value , 0 ,32767 ,0,100));
             slweLimitL.Text = slewLimitSlider.Value.ToString() + "%";

             value = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_FATAL_ERROR_MAX_THROTTLE);
             fatalErrorSlider.Value = (int)Math.Round(map((int)value, 0, 32767, 0, 100));
             fatalErrorMaxL.Text = fatalErrorSlider.Value.ToString() + "%";
        
      
            
            //****************************************\\
            // PID : 

         EnableYawDamper.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_ENABLE_YAW_DAMPER) == 1) ? true : false;
         SmoothGainScheduling.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_ENABLE_SMOOTH_GAIN_SCHEDULING) == 1) ? true : false;
         CompatiblityCheckBox.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_COMPATIBILITY_MODE) <1000) ? true : false;

         // feedback loops
            SelectedPIDLloopCombo.SelectedIndex = 0;
         setPIDToolBoxes(true);
            
            // Display ?? 

            
            // Calc 

         ScheduledT.Text = "Fast - 30 Hz";
         SlewLimitsT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.pidOutputSlewLimit).ToString();
         ResultMaxT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.pidMaxResult).ToString();
         ResultMinT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.pidMinResult).ToString();
    
     
            
       
       //****************************************\\
            
            //Sevoes :

         FlapSlewLimitT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_FLAP_SLEW_LIMIT).ToString();
         StaffServoCICTimeoutT.Text =( vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_STUFF_SERVOS_CIC_TIMEOUT)*0.2).ToString();
          ParachuteServoT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_CHUTE).ToString();
         ReleaseServoT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RELEASE).ToString();
         NoiseWheelServoT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_NOSEWHEEL_SERVO).ToString();
         SpoilerServoT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SPOILER_SERVO).ToString();
         GyroControlServoT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_EXTERNAL_GYRO_OUTPUT_SERVO).ToString();
         EnableSevoLock.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_LOCK) == 1) ? true : false;
         DisengageLockSpeedT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_LOCK_SPEED).ToString();
            FlapsRCInChannelT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_FLAPS_RC_INPUT_CHANNEL).ToString();
         SpoilerRCInChannelT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SPOILER_RC_INPUT_CHANNEL).ToString();
         SpoilerSlewLlimitT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SPOILER_SLEW_LIMIT).ToString();
         DisableMixingLimit.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DISABLE_MIXING_LIMITS) == 1) ? true : false;
         DisableRStaffServo.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_DISABLE_RSTUFFSERVOS) == 1) ? true : false;
        TO.SelectedIndex= (int) vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_THROTTLE_OVERRIDE);
        InputCalculationT.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_INPUT_CALCULATION).ToString();
       

               
            //// na2es el travel 
            // value = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_ELEVATOR);
            // ElevatorZeroSlider.Value = this.percentageMapping(-max, max, (double)value);
            // elevatorL.Text = ElevatorZeroSlider.Value.ToString() + "%";

            
            // value = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_RUDDER);
            // rudderZeroSlider.Value = this.percentageMapping(-max, max, (double)value);
            // rudderL.Text = rudderZeroSlider.Value.ToString() + "%";


            // value = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_THROTTLE);
            // throttleZeroSlider.Value = this.percentageMapping(-max, max, (double)value);
            // throttleL.Text = throttleZeroSlider.Value.ToString() + "%";




             ServoNumComboBox.SelectedIndex = 0;
             ServoTypeComboBox.SelectedIndex = (int) vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.servoType);
             servoRudderComboBox.SelectedIndex = (int)vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.servoRudder);
             EnableSevoLock.Checked = (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_LOCK) == 1) ? true : false;
             enableServoLocking(EnableSevoLock.Checked);

             value = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_LOCK_EL);
             lockElavatorSlider.Value = (int)map((int)value, -32767, 32767, -100, 100);
             lockElvatorL.Text = lockElavatorSlider.Value.ToString() + "%";

             value = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_LOCK_AI);
             lockAileronSlider.Value = (int) map((int)value, -32767, 32767, -100, 100);
             lockAileronL.Text = lockAileronSlider.Value.ToString() + "%";

             value = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_LOCK_RDR);
             lockRudderSlider.Value = (int)map((int)value, -32767, 32767, -100, 100);
             lockRudderL.Text = lockRudderSlider.Value.ToString() + "%";
            
            // Zeros & travel

             changeZeroTravel(1);
             
             int TI_fileValue    = (int) (vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_THROTTLE_IDLE));
             throttleIdel.Value = (int)Math.Round( map(TI_fileValue, 4000, 2000, 0, 100));
            TIPercentage.Text = throttleIdel.Value.ToString() + " %";
             p9.Text = Math.Round(map (throttleIdel.Value, 0 , 100 , 2 ,1)).ToString() + " ms";

             setInitComboboxes();

             manualChange = true;
             firstIinit = false;
        }

      void  initSetting(int max , int min , int index)
        {
            if (BitField.isSet(index, max))
                init1.SelectedIndex = 1;
            else if (BitField.isSet(index, min))
               init1.SelectedIndex = 2;
            else
                init1.SelectedIndex = 0;

            if (BitField.isSet(index+1, max))
                init2.SelectedIndex = 1;
            else if (BitField.isSet(index+1, min))
               init2.SelectedIndex = 2;
            else
                init2.SelectedIndex = 0;

            if (BitField.isSet(index+2, max))
                init3.SelectedIndex = 1;
            else if (BitField.isSet(index+2, min))
               init3.SelectedIndex = 2;
            else
                init3.SelectedIndex = 0;

            if (BitField.isSet(index+3, max))
                init4.SelectedIndex = 1;
            else if (BitField.isSet(index+3, min))
               init4.SelectedIndex = 2;
            else
                init4.SelectedIndex = 0;
            if (BitField.isSet(index+4, max))
                init5.SelectedIndex = 1;
            else if (BitField.isSet(index+4, min))
               init5.SelectedIndex = 2;
            else
                init5.SelectedIndex = 0;
            if (BitField.isSet(index+5, max))
                init6.SelectedIndex = 1;
            else if (BitField.isSet(index+5, min))
               init6.SelectedIndex = 2;
            else
                init6.SelectedIndex = 0;
            if (BitField.isSet(index+6, max))
                init7.SelectedIndex = 1;
            else if (BitField.isSet(index+6, min))
               init7.SelectedIndex = 2;
            else
                init7.SelectedIndex = 0;
            if (BitField.isSet(index+7, max))
                init8.SelectedIndex = 1;
            else if (BitField.isSet(index+7, min))
               init8.SelectedIndex = 2;
            else
                init8.SelectedIndex = 0;
        }

      void   setInitComboboxes()
    {
      
          int max =(int) vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_INITIALIZE_SERVO_MAXIMUM);
          int min = (int) vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_INITIALIZE_SERVO_MINIMUM);
        if (ServoNumComboBox.SelectedIndex == 0)
        {
            initSetting(max, min, 0);
        }
        else if (ServoNumComboBox.SelectedIndex == 1)
        {
            initSetting(max, min, 8);
        }
          else
        {
            initSetting(max, min, 16);   
        }
    }

    

            void setOriginalPIDDataFields()
        {
            PIDFieldsID.pidNextRecord_1 = 3000;
            PIDFieldsID.pidGnSchaRecord_1 = 3001;
       PIDFieldsID. pidGnSchbRecord_1 = 3002;
       PIDFieldsID. pidGnSchcRecord_1 = 3003;
      PIDFieldsID.  pidGnSchdRecord_1 = 3004;
       PIDFieldsID. pidMultIaRecord_1 = 3005;
       PIDFieldsID. pidMultPbRecord_1 = 3009;
       PIDFieldsID. pidMultPcRecord_1 = 3013;
       PIDFieldsID. pidMultPdRecord_1 = 3017;
       PIDFieldsID. pidMaxPRecord_1 = 3041;
       PIDFieldsID. pidMinPRecord_1 = 3045;
       PIDFieldsID. pidMultIaInteqralRecord_1 = 3006;
       PIDFieldsID. pidMultIbInteqralRecord_1 = 3010;
      PIDFieldsID.  pidMultIcInteqralRecord_1 = 3014;
       PIDFieldsID. pidMultIdInteqralRecord_1 = 3018;
       PIDFieldsID. pidMaxIInteqralRecord_1 = 3042;
       PIDFieldsID. pidMinIInteqralRecord_1 = 3046;
      PIDFieldsID.  pidWindupMaxInteqralRecord_1 = 3049;
      PIDFieldsID.  pidWindupMinInteqralRecord_1 = 3050;
      PIDFieldsID.  pidMultDaRecord_1 = 3007;
      PIDFieldsID.  pidMultDbRecord_1 = 3011;
      PIDFieldsID.  pidMultDcRecord_1 = 3015;
      PIDFieldsID.  pidMultDdRecord_1 = 3019;
       PIDFieldsID. pidMaxDRecord_1 = 3043;
      PIDFieldsID.  pidMinDRecord_1 = 3047;
      PIDFieldsID.  pidMultDDaRecord_1 = 3008;
      PIDFieldsID.  pidMultDDbRecord_1 = 3012;
      PIDFieldsID.  pidMultDDcRecord_1 = 3016;
      PIDFieldsID.  pidMultDDdRecord_1 = 3020;
       PIDFieldsID. pidMaxDDRecord_1 = 3044;
       PIDFieldsID. pidMinDDRecord_1 = 3048;
        }

            void changePID_ID(int value)
 {
     PIDFieldsID.pidNextRecord_1 += value;
     PIDFieldsID.pidGnSchaRecord_1 += value;
     PIDFieldsID.pidGnSchbRecord_1 += value;
     PIDFieldsID.pidGnSchcRecord_1 += value;
     PIDFieldsID.pidGnSchdRecord_1 += value;
     PIDFieldsID.pidMultIaRecord_1 += value;
     PIDFieldsID.pidMultPbRecord_1 += value;
     PIDFieldsID.pidMultPcRecord_1 += value;
     PIDFieldsID.pidMultPdRecord_1 += value;
     PIDFieldsID.pidMaxPRecord_1 += value;
     PIDFieldsID.pidMinPRecord_1 += value;
     PIDFieldsID.pidMultIaInteqralRecord_1 += value;
     PIDFieldsID.pidMultIbInteqralRecord_1 += value;
     PIDFieldsID.pidMultIcInteqralRecord_1 += value;
     PIDFieldsID.pidMultIdInteqralRecord_1 += value;
     PIDFieldsID.pidMaxIInteqralRecord_1 += value;
     PIDFieldsID.pidMinIInteqralRecord_1 += value;
     PIDFieldsID.pidWindupMaxInteqralRecord_1 += value;
     PIDFieldsID.pidWindupMinInteqralRecord_1 += value;
     PIDFieldsID.pidMultDaRecord_1 += value;
     PIDFieldsID.pidMultDbRecord_1 += value;
     PIDFieldsID.pidMultDcRecord_1 += value;
     PIDFieldsID.pidMultDdRecord_1 += value;
     PIDFieldsID.pidMaxDRecord_1 += value;
     PIDFieldsID.pidMinDRecord_1 += value;
     PIDFieldsID.pidMultDDaRecord_1 += value;
     PIDFieldsID.pidMultDDbRecord_1 += value;
     PIDFieldsID.pidMultDDcRecord_1 += value;
     PIDFieldsID.pidMultDDdRecord_1 += value;
     PIDFieldsID.pidMaxDDRecord_1 += value;
     PIDFieldsID.pidMinDDRecord_1 += value;
 }

            void setPIDToolBoxes(bool firstTime)
        {
            Sch0T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidNextRecord_1).ToString();

            Range0T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidGnSchaRecord_1).ToString();
            
                Range1T.Text  = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidGnSchbRecord_1).ToString();
               

                Range2T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidGnSchcRecord_1).ToString();
                

                 Range3T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidGnSchdRecord_1).ToString();
             
                if (firstTime)
                 {
                     Range0T.Text = UnitConversion.fPerS_kmPerH(double.Parse(Range0T.Text)).ToString();
                     Range1T.Text = UnitConversion.fPerS_kmPerH(double.Parse(Range1T.Text)).ToString();
                     Range2T.Text = UnitConversion.fPerS_kmPerH(double.Parse(Range2T.Text)).ToString();
                     Range3T.Text = UnitConversion.fPerS_kmPerH(double.Parse(Range3T.Text)).ToString();
                 }
                Sch1T.Text = Range0T.Text;
                Sch2T.Text = Range1T.Text;
                Sch3T.Text = Range2T.Text;

                Propotional0T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultIaRecord_1).ToString();
                Propotional1T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultPbRecord_1).ToString();
                Propotional2T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultPcRecord_1).ToString();
                Propotional3T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultPdRecord_1).ToString();

                inputMax.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMaxPRecord_1).ToString();
                inputMin.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMinPRecord_1).ToString();

                inteqral0T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultIaInteqralRecord_1).ToString();
                inteqral1T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultIbInteqralRecord_1).ToString();
                inteqral2T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultIcInteqralRecord_1).ToString();
                inteqral3T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultIdInteqralRecord_1).ToString();
                inteqralInputMaxT.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMaxIInteqralRecord_1).ToString();
                inteqralInputMinT.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMinIInteqralRecord_1).ToString();

                ResAntiWindupMaxT.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidWindupMaxInteqralRecord_1).ToString();
                ResAntiWindupMinT.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidWindupMinInteqralRecord_1).ToString();

                Differential0T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultDaRecord_1).ToString();
                Differential1T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultDbRecord_1).ToString();
                Differential2T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultDcRecord_1).ToString();
                Differential3T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultDdRecord_1).ToString();
                DifferentialInputMaxT.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMaxDRecord_1).ToString();
                DifferentialInputMinT.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMinDRecord_1).ToString();

                feedForward0T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultDDaRecord_1).ToString();
                feedForward1T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultDDbRecord_1).ToString();
                feedForward2T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultDDcRecord_1).ToString();
                feedForward3T.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMultDDdRecord_1).ToString();
                feedForwardInputMaxT.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMaxDDRecord_1).ToString();
                feedForwardInputMinT.Text = vrsHandler.searchByFieldID_getParameter_value(PIDFieldsID.pidMinDDRecord_1).ToString();

        }

       
      void  UpdateVrsArrayPID()
        {
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidNextRecord_1)].Parameter_value = (int)double.Parse(Sch0T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidGnSchaRecord_1)].Parameter_value = (int)double.Parse(Range0T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidGnSchbRecord_1)].Parameter_value = (int)double.Parse(Range1T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidGnSchcRecord_1)].Parameter_value = (int)double.Parse(Range2T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidGnSchdRecord_1)].Parameter_value = (int)double.Parse(Range3T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultIaRecord_1)].Parameter_value = (int)double.Parse(Propotional0T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultPbRecord_1)].Parameter_value = (int)double.Parse(Propotional1T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultPcRecord_1)].Parameter_value = (int)double.Parse(Propotional2T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultPdRecord_1)].Parameter_value = (int)double.Parse(Propotional3T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMaxPRecord_1)].Parameter_value = (int)double.Parse(inputMax.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMinPRecord_1)].Parameter_value = (int)double.Parse(inputMin.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultIaInteqralRecord_1)].Parameter_value = (int)double.Parse(inteqral0T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultIbInteqralRecord_1)].Parameter_value = (int)double.Parse(inteqral1T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultIcInteqralRecord_1)].Parameter_value = (int)double.Parse(inteqral2T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultIdInteqralRecord_1)].Parameter_value = (int)double.Parse(inteqral3T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMaxIInteqralRecord_1)].Parameter_value = (int)double.Parse(inteqralInputMaxT.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMinIInteqralRecord_1)].Parameter_value = (int)double.Parse(inteqralInputMinT.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidWindupMaxInteqralRecord_1)].Parameter_value = (int)double.Parse(ResAntiWindupMaxT.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidWindupMinInteqralRecord_1)].Parameter_value = (int)double.Parse(ResAntiWindupMinT.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultDaRecord_1)].Parameter_value = (int)double.Parse(Differential0T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultDbRecord_1)].Parameter_value = (int)double.Parse(Differential1T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultDcRecord_1)].Parameter_value = (int)double.Parse(Differential2T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultDdRecord_1)].Parameter_value = (int)double.Parse(Differential3T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMaxDRecord_1)].Parameter_value = (int)double.Parse(DifferentialInputMaxT.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMinDRecord_1)].Parameter_value = (int)double.Parse(DifferentialInputMinT.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultDDaRecord_1)].Parameter_value = (int)double.Parse(feedForward0T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultDDbRecord_1)].Parameter_value = (int)double.Parse(feedForward1T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultDDcRecord_1)].Parameter_value = (int)double.Parse(feedForward2T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMultDDdRecord_1)].Parameter_value = (int)double.Parse(feedForward3T.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMaxDDRecord_1)].Parameter_value = (int)double.Parse(feedForwardInputMaxT.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidMinDDRecord_1)].Parameter_value = (int)double.Parse(feedForwardInputMinT.Text);
           
        }

         void updateScheduled()
         {
             int  index = SelectedPIDLloopCombo.SelectedIndex;
             if(index == 4 || (index <=12 && index >=6))
             {
                 ScheduledT.Text = "Slow - 5 Hz";
             }
             else
                 ScheduledT.Text = "Fast - 30 Hz";
             // need to see hwo horizon put pramter value is it 30 - 5 or what .
           //  vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.Scheduled)].Parameter_value = double.Parse(ScheduledT.Text);
           
         }


        void SelectedPIDLloopComboChanged( object sender, 	System.EventArgs e)
        {
            UpdateVrsArrayPID();
            updateToolBoxes(false);
            updateScheduled();
           
        }

      void  servoRudderComboBoxChanged(object sender, System.EventArgs e)
        {
            
          changeServoAssignment(ServoTypeComboBox.SelectedIndex , servoRudderComboBox.SelectedIndex);

            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.servoRudder)].Parameter_value = servoRudderComboBox.SelectedIndex;
         
        }

      void ServoTypeComboBoxChanged(object sender, System.EventArgs e)
      {
          changeServoAssignment(ServoTypeComboBox.SelectedIndex, servoRudderComboBox.SelectedIndex);

          vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.servoType)].Parameter_value = ServoTypeComboBox.SelectedIndex;
      }

      void changeServoAssignment(int servoTypeIndex, int servoRudderIndex)
        {
            bool s1 = false, s2 = false, s3 = false, s4 = false, s5=false ,s6=false, s7=false, s8=false;
            #region s1
            if (servoTypeIndex == 0 || servoTypeIndex == 1)
                s1t.Text = "ailerons";
            else if (servoTypeIndex == 2 || servoTypeIndex == 3 || servoTypeIndex == 4 || servoTypeIndex == 5)
                s1t.Text = "left aileron ";
            else if (servoTypeIndex == 6 && servoRudderIndex == 0)
                s1t.Text = "left fin";
            else if ((servoTypeIndex == 7 && servoRudderIndex == 0) || (servoTypeIndex == 7 && servoRudderIndex == 1)
                || (servoTypeIndex == 7 && servoRudderIndex == 2) || (servoTypeIndex == 7 && servoRudderIndex == 3)
                || (servoTypeIndex == 7 && servoRudderIndex == 4) || (servoTypeIndex == 7 && servoRudderIndex == 5)
                || (servoTypeIndex == 7 && servoRudderIndex == 6))
                s1t.Text = "aileron";
            else
                s1 = true;
 #endregion
            #region s2
             if ((servoTypeIndex == 0 && servoRudderIndex == 1) || (servoTypeIndex == 1 && servoRudderIndex == 1)
                || (servoTypeIndex == 2 && servoRudderIndex == 1) || (servoTypeIndex == 3 && servoRudderIndex == 1)
                 || (servoTypeIndex == 4 && servoRudderIndex == 1)  || (servoTypeIndex == 5 && servoRudderIndex == 1)
                || (servoTypeIndex == 7 && servoRudderIndex == 1))
                 s2t.Text = "left V tail";
            else if ((servoTypeIndex == 0 && servoRudderIndex == 4) || (servoTypeIndex == 1 && servoRudderIndex == 4)
                 || (servoTypeIndex == 7 && servoRudderIndex == 4))
                s2t.Text = "upper left X tail";
             else if (servoTypeIndex == 6 && servoRudderIndex == 0)
                 s2t.Text = "right fin";
            else if (servoTypeIndex == 0 || servoTypeIndex == 1 || servoTypeIndex == 2
                || servoTypeIndex == 3 || servoTypeIndex == 5 || (servoTypeIndex == 7 && servoRudderIndex == 0)
                 || (servoTypeIndex == 7 && servoRudderIndex == 2) || (servoTypeIndex == 7 && servoRudderIndex == 3)
                 || (servoTypeIndex == 7 && servoRudderIndex == 5) || (servoTypeIndex == 7 && servoRudderIndex == 6))
                s2t.Text = "elevator";
             else if (servoTypeIndex == 4)
                s2t.Text = "right elevon";
            else
                s2 = true;
            #endregion
            #region s3
             if ((servoTypeIndex == 0 && servoRudderIndex == 1) || (servoTypeIndex == 1 && servoRudderIndex == 1)
                 || (servoTypeIndex == 2 && servoRudderIndex == 1) || (servoTypeIndex == 3 && servoRudderIndex == 1)
                 || (servoTypeIndex == 4 && servoRudderIndex == 1) || (servoTypeIndex == 5 && servoRudderIndex == 1)
                 || (servoTypeIndex == 7 && servoRudderIndex == 1))
                 s3t.Text = "right V tail";
             else if ((servoTypeIndex == 0 && servoRudderIndex == 4) || (servoTypeIndex == 7 && servoRudderIndex == 4))
                 s3t.Text = "upper right X tail";
             else if ((servoTypeIndex == 0 && servoRudderIndex == 2) || (servoTypeIndex == 1 && servoRudderIndex == 2)
                  || (servoTypeIndex == 2 && servoRudderIndex == 2) || (servoTypeIndex == 3 && servoRudderIndex == 2)
                  || (servoTypeIndex == 4 && servoRudderIndex == 2) || (servoTypeIndex == 5 && servoRudderIndex == 2)
                 || (servoTypeIndex == 7 && servoRudderIndex == 2))
                 s3t.Text = "left split rudder";
             else if (servoTypeIndex == 6 && servoRudderIndex == 0)
                 s3t.Text = "bottom fin";
             else if ((servoTypeIndex != 6 && servoRudderIndex == 5) || (servoTypeIndex == 7 && servoRudderIndex == 5)) 
                 s3t.Text = "right throttle";
             else if (servoRudderIndex == 6 )
                 s3t.Text = "left twin rudder";
             else if (servoRudderIndex == 3)
                 s3t.Text = "   ";
             else if (servoTypeIndex == 0 || servoTypeIndex == 1 || servoTypeIndex == 2 || servoTypeIndex == 3
                 || servoTypeIndex == 4 || servoTypeIndex == 5 ||
                 (servoTypeIndex == 7 && servoRudderIndex == 0))
                 s3t.Text = "rudder";
             else
                 s3 = true;
             #endregion
            #region s4
             if ((servoTypeIndex != 6 && servoRudderIndex == 5) || (servoTypeIndex == 7 && servoRudderIndex == 5)) 
                 s4t.Text = "left throttle";
          else
            s4t.Text = "throttle";
        //else 
             //s4= true;
             #endregion
            #region s5
            if (servoTypeIndex == 4 && servoRudderIndex == 1)
                st5.Text = "right elevon";
            else if (servoTypeIndex == 1)
                st5.Text = "flaps";
            else if (servoTypeIndex == 3)
                st5.Text = "left flaps";
         
            else
                s5 = true;
            #endregion
            #region s6 
            if ((servoTypeIndex == 0 && servoRudderIndex == 4) || (servoTypeIndex == 1 && servoRudderIndex == 4)
                || (servoTypeIndex == 7 && servoRudderIndex == 4))
                s6t.Text = "lower right X tail";
            else if (servoTypeIndex == 2 || servoTypeIndex == 3 || servoTypeIndex == 5)
                s6t.Text = "right aileron";
          else
                s6 = true;
            #endregion
            #region s7
            if ((servoTypeIndex == 0 && servoRudderIndex == 4) || (servoTypeIndex == 1 && servoRudderIndex == 4)
            || (servoTypeIndex == 7 && servoRudderIndex == 4))
                s7t.Text = "lower left X tail";
            else if (servoTypeIndex == 3)
                s7t.Text = "right flap";
          else
                s7 = true;
            #endregion
            #region s8 
            if (servoRudderIndex == 2 || (servoTypeIndex == 7 && servoRudderIndex == 2))
                s8t.Text = "right split rudder";
            else if (servoRudderIndex == 6)
                s8t.Text = "right twin rudder";
          else
                s8 = true;
            #endregion
            #region empty servos
            if (s1) s1t.Text = "   ";
            if (s2) s2t.Text = "   ";
            if (s3) s3t.Text = "   ";
            if (s4) s4t.Text = "   ";
            if (s5) st5.Text = "   ";
            if (s6) s6t.Text = "   ";
            if (s7) s7t.Text = "   ";
            if (s8) s8t.Text = "   ";
            #endregion
            #region undefined servos
            if (((servoTypeIndex == 2 || servoTypeIndex == 3 || servoTypeIndex == 4 || servoTypeIndex == 5 || servoTypeIndex == 6)
                && servoRudderIndex == 4) || (servoTypeIndex ==6 && servoRudderIndex !=0) )
            {
                s1t.Text = "???";
                s2t.Text = "???";
                s3t.Text = "???";
                s4t.Text = "???";
                st5.Text = "???";
                s6t.Text = "???";
                s7t.Text = "???";
                s8t.Text = "???";
            }
            #endregion
        }

      void changeZeroTravel(int index)
      {
           double travelVvalue ;
         

          if (index == 1)
          {
              z1.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_1)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_1); 
              if (travelVvalue < 0)
                  swap1.Checked = true;
              else
                  swap1.Checked = false;
              double q = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_1);

              resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_1)));
              t1.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z2.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_2)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_2);
              if (travelVvalue < 0)
                  swap2.Checked = true;
              else
                  swap2.Checked = false;
              resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_2)));
              t2.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z3.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_3)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_3);
              if (travelVvalue < 0)
                  swap3.Checked = true;
              else
                  swap3.Checked = false;
              servo4 = true;
              resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_3)));
              t3.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z4.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_4)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_4);
              if (travelVvalue < 0)
                  swap4.Checked = true;
              else
                  swap4.Checked = false;
          //    t4.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              servo4 = false;
              z5.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_5)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_5);
              if (travelVvalue < 0)
                  swap5.Checked = true;
              else
                  swap5.Checked = false;
              resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_5)));
              t5.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z6.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_6)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_6);
              if (travelVvalue < 0)
                  swap6.Checked = true;
              else
                  swap6.Checked = false;
              resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_6)));
              t6.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z7.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_7)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_7);
              if (travelVvalue < 0)
                  swap7.Checked = true;
              else
                  swap7.Checked = false;
              resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_7)));
              t7.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z8.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_8)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_8);
              if (travelVvalue < 0)
                  swap8.Checked = true;
              else
                  swap8.Checked = false;
              resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_8)));
              t8.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));   
          }
          else if (index == 2)
          {
              

              z1.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_9)));
               travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_9);
               if (travelVvalue < 0)
                   swap1.Checked = true;
               else
                   swap1.Checked = false;
               resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_9)));
              t1.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));
              
              z2.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_10)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_10);
              if (travelVvalue < 0)
                  swap2.Checked = true;
              else
                  swap2.Checked = false;
              resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_10)));
              t2.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z3.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_11)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_11);
              if (travelVvalue < 0)
                  swap3.Checked = true;
              else
                  swap3.Checked = false;
              resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_11)));
              t3.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));
              servo4 = true ;
              z4.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_12)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_12);
              if (travelVvalue < 0)
                  swap4.Checked = true;
              else
                  swap4.Checked = false;
             // t4.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));
              servo4 = false;
              z5.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_13)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_13);
              if (travelVvalue < 0)
                  swap5.Checked = true;
              else
                  swap5.Checked = false;
              resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_13)));
              t5.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z6.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_14)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_14);
              if (travelVvalue < 0)
                  swap6.Checked = true;
              else
                  swap6.Checked = false;
              resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_14)));
              t6.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z7.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_15)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_15);
              if (travelVvalue < 0)
                  swap7.Checked = true;
              else
                  swap7.Checked = false;
             resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_15)));
              t7.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z8.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_16)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_16);
              if (travelVvalue < 0)
                  swap8.Checked = true;
              else
                  swap8.Checked = false;
              resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_16)));
              t8.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));
          }
          else
          {
              z1.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_17)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_17);
              if (travelVvalue < 0)
                  swap1.Checked = true;
              else
                  swap1.Checked = false;
              resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_17)));
              t1.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z2.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_18)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_18);
              if (travelVvalue < 0)
                  swap2.Checked = true;
              else
                  swap2.Checked = false;
               resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_18)));
              t2.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z3.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_19)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_19);
              if (travelVvalue < 0)
                  swap3.Checked = true;
              else
                  swap3.Checked = false;
               resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_19)));
              t3.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));
              servo4 = true;
              z4.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_20)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_20);
              if (travelVvalue < 0)
                  swap4.Checked = true;
              else
                  swap4.Checked = false;
              //t4.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));
              servo4 = false;
              z5.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_21)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_21);
              if (travelVvalue < 0)
                  swap5.Checked = true;
              else
                  swap5.Checked = false;
               resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_21)));
              t5.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z6.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_22)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_22);
              if (travelVvalue < 0)
                  swap6.Checked = true;
              else
                  swap6.Checked = false;
           resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_22)));
              t6.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z7.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_23)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_23);
              if (travelVvalue < 0)
                  swap7.Checked = true;
              else
                  swap7.Checked = false;
               resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_23)));
              t7.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

              z8.Value = (int)zeroReversInterpolation(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_24)));
              travelVvalue = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_RANGE_24);
              if (travelVvalue < 0)
                  swap8.Checked = true;
              else
                  swap8.Checked = false;
              resetTravelRange(reversZeroMapping(vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_SERVO_ZERO_24)));
              t8.Value = (int)travelReversInterpolation(reversTravelMapping(Math.Abs(travelVvalue)));

          }

          GlobalData.LogConsole(z1.Value + "    " + zeroInterpolation(z1.Value));
            
          p1.Text = zeroInterpolation(z1.Value).ToString() + " +- " + travelInterpolation(t1.Value).ToString();
          p2.Text = zeroInterpolation(z2.Value).ToString() + " +- " + travelInterpolation(t2.Value).ToString();
          p3.Text = zeroInterpolation(z3.Value).ToString() + " +- " + travelInterpolation(t3.Value).ToString();
          p4.Text = zeroInterpolation(z4.Value).ToString() + " + " + travelInterpolation(t4.Value).ToString();
          p5.Text = zeroInterpolation(z5.Value).ToString() + " +- " + travelInterpolation(t5.Value).ToString();
          p6.Text = zeroInterpolation(z6.Value).ToString() + " +- " + travelInterpolation(t6.Value).ToString();
          p7.Text = zeroInterpolation(z7.Value).ToString() + " +- " + travelInterpolation(t7.Value).ToString();
          p8.Text = zeroInterpolation(z8.Value).ToString() + " +- " + travelInterpolation(t8.Value).ToString();

          swapChange = false;
      }

        void ServoNumComboBoxChanged(object sender, System.EventArgs e)
        {
            swapChange = true;
            int index = ServoNumComboBox.SelectedIndex;
            if (index == 0)
            {
                setServoNum(1);
                changeZeroTravel(1);
            }
            else if (index == 1)
            {
                setServoNum(9);
                changeZeroTravel(2);
            }
            else
            {
                setServoNum(17);
                changeZeroTravel(3);
            }

            setInitComboboxes();
        }

      void  setServoNum( int index)
        {

            s1.Text = index.ToString();
            index++;
            s2.Text = index.ToString();
            index++;
            s3.Text = index.ToString();
            index++;
            s4.Text = index.ToString();
            index++;
            s5.Text = index.ToString();
            index++;
            s6.Text = index.ToString();
            index++;
            s7.Text = index.ToString();
            index++;
            s8.Text = index.ToString();
        }

        void CalculateAlironCheckBoxChanged(object sender, System.EventArgs e)
        {
            if (SelectedPIDLloopCombo.SelectedIndex == 0)
            {
                if (CalculateAlironCheckBox.Checked)
                {
                    SelectedPIDLloopCombo.Text = "Aliron From Roll Rate";
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_EULER_RATE_CONTROL_MODE)].Parameter_value += 1;   
                }
                else
                {
                    SelectedPIDLloopCombo.Text = "Aliron From Desired Roll ";
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_EULER_RATE_CONTROL_MODE)].Parameter_value -= 1;
                }             
            }

          
           else if (SelectedPIDLloopCombo.SelectedIndex == 1)
            {
                if (CalculateAlironCheckBox.Checked)
                {
                    SelectedPIDLloopCombo.Text = "Elevator From Pitch Rate";
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_EULER_RATE_CONTROL_MODE)].Parameter_value += 2;   
                }
                else
                {
                    SelectedPIDLloopCombo.Text = "Elevator From Desired Pitch";
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_EULER_RATE_CONTROL_MODE)].Parameter_value = 2;   
                }
            }

            else if (SelectedPIDLloopCombo.SelectedIndex == 13)
            {
                if (CalculateAlironCheckBox.Checked)
                {
                    SelectedPIDLloopCombo.Text = "Roll Rate From Error";
                    // vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_EULER_RATE_CONTROL_MODE)].Parameter_value += 1;
                }
                else
                {
                    SelectedPIDLloopCombo.Text = "Roll Rate From Error (DISABLED)";
                 //   vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_EULER_RATE_CONTROL_MODE)].Parameter_value += 1;
                }
            }

            else if (SelectedPIDLloopCombo.SelectedIndex == 14)
            {
                if (CalculateAlironCheckBox.Checked)
                {
                    SelectedPIDLloopCombo.Text = "Pitch Rate From Error";
                    // vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_EULER_RATE_CONTROL_MODE)].Parameter_value += 1;   
                }
                else
                {
                    SelectedPIDLloopCombo.Text = "Pitch Rate From Error (DISABLED)";
                    //vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_EULER_RATE_CONTROL_MODE)].Parameter_value += 1;   
                }
            }

            else if (SelectedPIDLloopCombo.SelectedIndex == 15)
            {
                if (CalculateAlironCheckBox.Checked)
                {
                    SelectedPIDLloopCombo.Text = "Yaw Rate From Error";
                    //   vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_EULER_RATE_CONTROL_MODE)].Parameter_value += 1;
                }
                else
                {
                    SelectedPIDLloopCombo.Text = "Yaw Rate From Error (DISABLED)";
                 //   vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_EULER_RATE_CONTROL_MODE)].Parameter_value += 1;
                }
            }
        }

        void swap1CheckBoxChanged(object sender, System.EventArgs e)
        {
            if (!swapChange)
            {
                if (ServoNumComboBox.SelectedIndex == 0)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_1)].Parameter_value *= -1;
                }
                else if (ServoNumComboBox.SelectedIndex == 1)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_9)].Parameter_value *= -1;
                }
                else
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_17)].Parameter_value *= -1;
                }
            }
        }

        void swap2CheckBoxChanged(object sender, System.EventArgs e)
        {
            if (!swapChange)
            {
                if (ServoNumComboBox.SelectedIndex == 0)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_2)].Parameter_value *= -1;
                }
                else if (ServoNumComboBox.SelectedIndex == 1)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_10)].Parameter_value *= -1;
                }
                else
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_18)].Parameter_value *= -1;
                }
            }
        }

        void swap3CheckBoxChanged(object sender, System.EventArgs e)
        {
            if (!swapChange)
            {
                if (ServoNumComboBox.SelectedIndex == 0)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_3)].Parameter_value *= -1;
                }
                else if (ServoNumComboBox.SelectedIndex == 1)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_11)].Parameter_value *= -1;
                }
                else
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_19)].Parameter_value *= -1;
                }
            }
        }

        void swap4CheckBoxChanged(object sender, System.EventArgs e)
        {
            if (!swapChange)
            {
                if (ServoNumComboBox.SelectedIndex == 0)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_4)].Parameter_value *= -1;
                }
                else if (ServoNumComboBox.SelectedIndex == 1)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_12)].Parameter_value *= -1;
                }
                else
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_20)].Parameter_value *= -1;
                }
            }
        }

        void swap5CheckBoxChanged(object sender, System.EventArgs e)
        {
            if (!swapChange)
            {
                if (ServoNumComboBox.SelectedIndex == 0)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_5)].Parameter_value *= -1;
                }
                else if (ServoNumComboBox.SelectedIndex == 1)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_13)].Parameter_value *= -1;
                }
                else
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_21)].Parameter_value *= -1;
                }
            }
        }

        void swap6CheckBoxChanged(object sender, System.EventArgs e)
        {
            if (!swapChange)
            {
                if (ServoNumComboBox.SelectedIndex == 0)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_6)].Parameter_value *= -1;
                }
                else if (ServoNumComboBox.SelectedIndex == 1)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_14)].Parameter_value *= -1;
                }
                else
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_22)].Parameter_value *= -1;
                }
            }
        }

        void swap7CheckBoxChanged(object sender, System.EventArgs e)
        {
            if (!swapChange)
            {
                if (ServoNumComboBox.SelectedIndex == 0)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_7)].Parameter_value *= -1;
                }
                else if (ServoNumComboBox.SelectedIndex == 1)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_15)].Parameter_value *= -1;
                }
                else
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_23)].Parameter_value *= -1;
                }
            }
        }

        void compatiblityChanged(object sender, System.EventArgs e)
        {
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_COMPATIBILITY_MODE)].Parameter_value = (CompatiblityCheckBox.Checked == true) ? 100 : 1000;
        }

        void swap8CheckBoxChanged(object sender, System.EventArgs e)
        {
            if (!swapChange)
            {
                if (ServoNumComboBox.SelectedIndex == 0)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_8)].Parameter_value *= -1;
                }
                else if (ServoNumComboBox.SelectedIndex == 1)
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_16)].Parameter_value *= -1;
                }
                else
                {
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RANGE_24)].Parameter_value *= -1;
                }
            }
        }

       void EnableSevoLockCheckBoxChanged(object sender, System.EventArgs e)
        {
            enableServoLocking(EnableSevoLock.Checked);
        }

        void  enableServoLocking(bool check)
    {
        if (check)
        {
            DisengageLockSpeedT.Enabled = true;
            lockElavatorSlider.Enabled = true;
            lockAileronSlider.Enabled = true;
            lockRudderSlider.Enabled = true;
            
        }
        else
        {  
                DisengageLockSpeedT.Enabled = false;
                lockElavatorSlider.Enabled = false;
                lockAileronSlider.Enabled = false;
                lockRudderSlider.Enabled = false;
         }
    }

        void updateToolBoxes(bool save)
    {
           

          string  selected = SelectedPIDLloopCombo.SelectedItem.ToString();

          if (selected.Equals("Aliron From Desired Roll ") || selected.Equals("Aliron From Roll Rate"))
            {
               
                setOriginalPIDDataFields();
                changePID_ID(0);
               if(!save)
                setPIDToolBoxes(false);
               label14.Text = "AirSpeed (km/h)";
                CalculateAlironCheckBox.Text = "Calculate Aliron From Roll Rate and Roll Rate From Roll Error";
                CalculateAlironCheckBox.Visible = true;
                CompatiblityCheckBox.Visible = false;
            }
            else if (selected.Equals("Elevator From Desired Pitch") || selected.Equals("Elevator From Pitch Rate"))
            {
                setOriginalPIDDataFields();
                changePID_ID(100);
                if (!save)
                setPIDToolBoxes(false);
                label14.Text = "AirSpeed (km/h)";
                CalculateAlironCheckBox.Text = "Calculate Aliron From Pitch Rate and Pitch Rate From Pitch Error";
                CalculateAlironCheckBox.Visible = true;
                CompatiblityCheckBox.Visible = false;
            }
            else if (selected.Equals("Rudder From Y Acceleremoter "))
            {
                setOriginalPIDDataFields();
                changePID_ID(200);
                if (!save)
                setPIDToolBoxes(false);
                label14.Text = "AirSpeed (km/h)";
                CalculateAlironCheckBox.Visible = false; 
                CompatiblityCheckBox.Visible = false;
            }
            else if (selected.Equals("Rudder from heading"))
            {
                setOriginalPIDDataFields();
                changePID_ID(300);
                if (!save)
                setPIDToolBoxes(false);
                label14.Text = "GroundSpeed (km/h)";
                CalculateAlironCheckBox.Text = "Calculate Rudder From Yaw Rate and Yaw Rate From Yaw Error";
                CalculateAlironCheckBox.Visible = true;
                CompatiblityCheckBox.Visible = false;
            }
            else if (selected.Equals("Throttle from speed "))
            {
                setOriginalPIDDataFields();
                changePID_ID(400);
                if (!save)
                setPIDToolBoxes(false);
                label14.Text = "AirSpeed (km/h)";
                CalculateAlironCheckBox.Visible = false;
                CompatiblityCheckBox.Visible = false;
            }
            else if (selected.Equals("Throttle from Z velocity"))
            {
                setOriginalPIDDataFields();
                changePID_ID(500);
                if (!save)
                setPIDToolBoxes(false);
                label14.Text = "AirSpeed (km/h)";
                CalculateAlironCheckBox.Visible = false;
                CompatiblityCheckBox.Visible = false;
            }
            else if (selected.Equals("Pitch from altitude "))
            {
                setOriginalPIDDataFields();
                changePID_ID(600);
                if (!save)
                setPIDToolBoxes(false);
                label14.Text = "AirSpeed (km/h)";
                CalculateAlironCheckBox.Visible = false;
                CompatiblityCheckBox.Visible = false;
            }
            else if (selected.Equals("Pitch from AGL Altitude"))
            {
                setOriginalPIDDataFields();
                changePID_ID(700);
                if (!save)
                setPIDToolBoxes(false);
                CalculateAlironCheckBox.Visible = false;
                CompatiblityCheckBox.Visible = false;
            }
            else if (selected.Equals("Pitch from airspeed "))
            {
                setOriginalPIDDataFields();
                changePID_ID(800);
                if (!save)
                setPIDToolBoxes(false);
                label14.Text = "AirSpeed (km/h)";
                CalculateAlironCheckBox.Visible = false;
                CompatiblityCheckBox.Visible = false;
            }
            else if (selected.Equals("Roll from heading "))
            {
                setOriginalPIDDataFields();
                changePID_ID(900);
                if (!save)
                setPIDToolBoxes(false);
                label14.Text = "GroundSpeed (km/h)";
                CalculateAlironCheckBox.Visible = false;
                CompatiblityCheckBox.Visible = true;
            }
           
            else if (selected.Equals("Heading from Cross Track error "))
            {
                setOriginalPIDDataFields();
                changePID_ID(1000);
                if (!save)
                setPIDToolBoxes(false);
                label14.Text = "GroundSpeed (km/h)";
                CompatiblityCheckBox.Visible = false;
                CalculateAlironCheckBox.Visible = false;
            }
            else if (selected.Equals("Pitch from Descent Rate"))
            {
                setOriginalPIDDataFields();
                changePID_ID(1400);
                if (!save)
                setPIDToolBoxes(false);
                label14.Text = "Range";
                CalculateAlironCheckBox.Visible = false;
                CompatiblityCheckBox.Visible = false;
            }
            else if (selected.Equals("Roll from Radius "))
            {
                setOriginalPIDDataFields();
                changePID_ID(1500);
                if (!save)
                setPIDToolBoxes(false);
                label14.Text = "GroundSpeed (km/h)";
                CompatiblityCheckBox.Visible = false;
                CalculateAlironCheckBox.Visible = false;
              
            }
          else if (selected.Equals("Roll Rate From Error (DISABLED)") || selected.Equals("Roll Rate From Error"))
            {
                setOriginalPIDDataFields();
                changePID_ID(1600);
                if (!save)
                setPIDToolBoxes(false);
                label14.Text = "AirSpeed (km/h)";
                CompatiblityCheckBox.Visible = false;
                CalculateAlironCheckBox.Visible = true;
                CalculateAlironCheckBox.Text = "Calculate Aliron From Roll Rate and Roll Rate From Roll Error";
            }
          else if (selected.Equals("Pitch Rate From Error (DISABLED)") || selected.Equals("Pitch Rate From Error"))
            {
                setOriginalPIDDataFields();
                changePID_ID(1700);
                if (!save)
                setPIDToolBoxes(false);
                label14.Text = "AirSpeed (km/h)";
                CompatiblityCheckBox.Visible = false;
                CalculateAlironCheckBox.Visible = true;
                CalculateAlironCheckBox.Text = "Calculate Elvator From Pitch Rate and Pitch Rate From Pitch Error";
        
          }
          else if (selected.Equals("Yaw Rate From Error (DISABLED)") || selected.Equals("Yaw Rate From Error"))
            {
                setOriginalPIDDataFields();
                changePID_ID(1800);
                if (!save)
                setPIDToolBoxes(false);
                label14.Text = "AirSpeed (km/h)";
                CompatiblityCheckBox.Visible = false;
                CalculateAlironCheckBox.Visible = true;
                CalculateAlironCheckBox.Text = "Calculate Rudder From Yaw Rate and Yaw Rate From Yaw Error";
            }
    }

        void f_FormClosed(object sender, FormClosedEventArgs e)
        {
       //     Update_VrsLines_list();
       //     vrsHandler.fileUIMapping2();
       //     String txt1 = System.IO.File.ReadAllText(@"vrs\vrsFile2.vrs");
       //     txt1 = Regex.Replace(txt1, @"^\s*$\n", string.Empty, RegexOptions.Multiline)
       //.TrimEnd();
           
       //     String txt2 = System.IO.File.ReadAllText(@"vrs\vrsFile.vrs");
       //     txt2 = Regex.Replace(txt2, @"^\s*$\n", string.Empty, RegexOptions.Multiline)
       // .TrimEnd();
            //if (!txt1.Equals(txt2))
            //{
               if(changesFlag)
                if (MessageBox.Show("Do You want to Save Changes ? ", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                      //Update_VrsLines_list();
                    vrsHandler.fileUIMapping();
                    MessageBox.Show(" Changes Saved ");
                }

            //}
          //  System.IO.File.Delete(@"vrs\vrsFile2.vrs");
        }

     

       
        private void Update_VrsLines_list()
        {

         //   try
         //   {
            // AGL 
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_AGL)].Parameter_value = aglsensor.SelectedIndex;
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_AGL_MODE)].Parameter_value = aglmode.SelectedIndex;
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_AGL_SWITCH_ALTITUDE)].Parameter_value = (int)UnitConversion.meters_feet(double.Parse(aglswitchaltitude.Text)); 
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_AGL_LOW_THROTTLE_SETTING)].Parameter_value = int.Parse(agllowthrottlesetting.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_AGL_LOW_THROTTLE_SWITCH_ALTITUDE)].Parameter_value = (int)UnitConversion.meters_feet(double.Parse(agllowthrottleswitch.Text)); 

            // GPS 
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_TYPE)].Parameter_value = GPS1type.SelectedIndex;
          //  vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_PARITY)].Parameter_value = GPS1parity.SelectedIndex;
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_BAUDRATE)].Parameter_value = int.Parse(GPS1baud.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_BITS)].Parameter_value = int.Parse(GPS1bits.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_FIX_RATE)].Parameter_value = int.Parse(GPS1fixrate.Text);
           // vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_PORT_B_PARITY)].Parameter_value = GPS2parity.SelectedIndex;
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_PORT_B_FLOW)].Parameter_value = GPS2flowcontrol.SelectedIndex;
            
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_PORT_B_BAUD)].Parameter_value = int.Parse(GPS2baud.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_PORT_B_BITS)].Parameter_value = int.Parse(GPS2bits.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_PORT_B_STOP)].Parameter_value = int.Parse(GPS2stopbits.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_DYNAMIC_MODEL)].Parameter_value = ubloxdynamicmodel.SelectedIndex;
            // it is check for certain bit field in SBAS TAKE INTO CONSIDERATION
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SBAS_ENABLE)].Parameter_value = SBASnumberofchannel.SelectedIndex;

            #region GPS Checkboxes

            if (GPS1fakegps.Checked == true)
            {
                BitField.set(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_FAKE_GPS)].Parameter_value);
            }
            else
            {
                BitField.clear(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_FAKE_GPS)].Parameter_value);
            }

            if (GPS1usegpsaltitude.Checked == true)
            {
                BitField.set(1, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_GPS_DATA)].Parameter_value);
            }
            else
            {
                BitField.clear(1, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_GPS_DATA)].Parameter_value);
            }


            if (GPS1usegpsspeed.Checked == true)
            {
                BitField.set(2, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_GPS_DATA)].Parameter_value);
            }
            else
            {
                BitField.clear(2, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_GPS_DATA)].Parameter_value);
            }

            if (GPS1kalmanfilter.Checked == true)
            {
                BitField.set(3, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_GPS_DATA)].Parameter_value);
            }
            else
            {
                BitField.clear(3, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_GPS_DATA)].Parameter_value);
            }

            if (GPS1hemisphere.Checked == true)
            {
                BitField.set(4, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_GPS_DATA)].Parameter_value);
            }
            else
            {
                BitField.clear(4, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_GPS_DATA)].Parameter_value);
            }
            if (SBASenable.Checked == true)
            {
                BitField.set(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SBAS_ENABLE)].Parameter_value);
            }
            else
            {
                BitField.clear(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SBAS_ENABLE)].Parameter_value);
            }
            if (SBASapplyintegrity.Checked == true)
            {
                BitField.set(1, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SBAS_ENABLE)].Parameter_value);
            }
            else
            {
                BitField.clear(1, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SBAS_ENABLE)].Parameter_value);
            }
            if (SBASusetestmode.Checked == true)
            {
                BitField.set(2, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SBAS_ENABLE)].Parameter_value);
            }
            else
            {
                BitField.clear(2, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SBAS_ENABLE)].Parameter_value);
            }

            #endregion

            #region Novatel checkboxes

            if (Novatelenablediasblenovatel.Checked == true)
            {
                BitField.set(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)].Parameter_value);
            }
            else
            {
                BitField.clear(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)].Parameter_value);
            }
            if (Novatelenablecorrection.Checked == true)
            {
                BitField.set(1, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)].Parameter_value);
            }
            else
            {
                BitField.clear(1, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)].Parameter_value);
            }
            if (Novatelminimumspeed.Checked == true)
            {
                BitField.set(2, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)].Parameter_value);
            }
            else
            {
                BitField.clear(2, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)].Parameter_value);
            }
            if (Novatelusel1flaot.Checked == true)
            {
                BitField.set(3, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)].Parameter_value);
            }
            else
            {
                BitField.clear(3, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)].Parameter_value);
            }
            if (Novatelbasestation.Checked == true)
            {
                BitField.set(4, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)].Parameter_value);
            }
            else
            {
                BitField.clear(4, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)].Parameter_value);
            }
            if (Novatelenablediableothe.Checked == true)
            {
                BitField.set(5, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)].Parameter_value);
            }
            else
            {
                BitField.clear(5, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_NOVATEL_ALIGN_OPTIONS)].Parameter_value);
            }

            #endregion 
            //DATALOG
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_1)].Parameter_value = int.Parse(Datalogcustomftext1.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_2)].Parameter_value = int.Parse(Datalogcustomftext2.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_3)].Parameter_value = int.Parse(Datalogcustomftext3.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_4)].Parameter_value = int.Parse(Datalogcustomftext4.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_5)].Parameter_value = int.Parse(Datalogcustomftext5.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_6)].Parameter_value = int.Parse(Datalogcustomftext6.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_7)].Parameter_value = int.Parse(Datalogcustomftext7.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_8)].Parameter_value = int.Parse(Datalogcustomftext8.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_9)].Parameter_value = int.Parse(Datalogcustomftext9.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_10)].Parameter_value = int.Parse(Datalogcustomftext10.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_11)].Parameter_value = int.Parse(Datalogcustomftext11.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_12)].Parameter_value = int.Parse(Datalogcustomftext12.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_13)].Parameter_value = int.Parse(Datalogcustomftext13.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_14)].Parameter_value = int.Parse(Datalogcustomftext14.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_15)].Parameter_value = int.Parse(Datalogcustomftext15.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_16)].Parameter_value = int.Parse(Datalogcustomftext16.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_17)].Parameter_value = int.Parse(Datalogcustomftext17.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_18)].Parameter_value = int.Parse(Datalogcustomftext18.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_19)].Parameter_value = int.Parse(Datalogcustomftext19.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_20)].Parameter_value = int.Parse(Datalogcustomftext20.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_21)].Parameter_value = int.Parse(Datalogcustomftext21.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_22)].Parameter_value = int.Parse(Datalogcustomftext22.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_23)].Parameter_value = int.Parse(Datalogcustomftext23.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_USER_FIELD_24)].Parameter_value = int.Parse(Datalogcustomftext24.Text);

            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_1)].Parameter_value = int.Parse(Datalogcustomheadfltext1.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_2)].Parameter_value = int.Parse(Datalogcustomheadfltext2.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_3)].Parameter_value = int.Parse(Datalogcustomheadfltext3.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_4)].Parameter_value = int.Parse(Datalogcustomheadfltext4.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_5)].Parameter_value = int.Parse(Datalogcustomheadfltext5.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_6)].Parameter_value = int.Parse(Datalogcustomheadfltext6.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_7)].Parameter_value = int.Parse(Datalogcustomheadfltext7.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_HEADER_USER_FIELD_8)].Parameter_value = int.Parse(Datalogcustomheadfltext8.Text);

            // checkboxes
            #region  checkboxes for datalog
            if ( Dataloglogging.Checked == true )
           {
               BitField.set(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_TYPE)].Parameter_value);
           }
           else
           {
               BitField.clear(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_TYPE)].Parameter_value);
           }

           if (Datalogdonoterase.Checked == true)
           {
               BitField.set(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DONT_ERASE_DATALOG)].Parameter_value);
           }
           else
           {
               BitField.clear(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DONT_ERASE_DATALOG)].Parameter_value);
           }

           if (Datalogenablecustom.Checked == true)
           {
               BitField.set(1, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_TYPE)].Parameter_value);
           }
           else
           {
               BitField.clear(1, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DATALOG_TYPE)].Parameter_value);
           }
            #endregion
           

           vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GCS_BAUDRATE)].Parameter_value = int.Parse(Gcsbaud.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GCS_XON_XOFF)].Parameter_value = Gcsflowcontrol.SelectedIndex;
            
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_RSSI_CHANNEL)].Parameter_value = int.Parse(GcsStrenghtchannel.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_RSSI_MIN)].Parameter_value = int.Parse(GcsStrenghtminimum.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_TEXT_COMMAND_REPEAT)].Parameter_value = int.Parse(GcsCommandrepeat.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_COM_PAD_CHARACTERS)].Parameter_value = int.Parse(GcsPadCharacter.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_FORCE_INITIAL_REPORT)].Parameter_value = int.Parse(Gcsintialreport.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_UAV_IDENTIFIER)].Parameter_value = int.Parse(Gcsidentifier.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_PHONE_NUMBER_FIRST)].Parameter_value = int.Parse(Gcsphonefirst.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_PHONE_NUMBER_LAST)].Parameter_value = int.Parse(Gcsphonesecond.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DIALING_INSTRUCTIONS)].Parameter_value = int.Parse(Gcsenabledialing.Text);
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_ANSWER_MODEM_TIMEOUT)].Parameter_value = int.Parse(Gcstimeount.Text);

            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_VRS_LOCK_OPTIONS)].Parameter_value = int.Parse(Vrsheadercommands.Text);
            // checkboxes
            #region comm checkboxees
        

            if (Vrsfootercommands.Checked == true)
            {
                BitField.set(10, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_VRS_LOCK_OPTIONS)].Parameter_value);
            }
            else
            {
                BitField.clear(10, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_VRS_LOCK_OPTIONS)].Parameter_value);
            }

            if (UART1disablebinding.Checked == true)
            {
                BitField.set(1, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_BIND_OPTIONS)].Parameter_value);
            }
            else
            {
                BitField.clear(1, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_BIND_OPTIONS)].Parameter_value);
            }
            if (UART2disablebinding.Checked == true)
            {
                BitField.set(2, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_BIND_OPTIONS)].Parameter_value);
            }
            else
            {
                BitField.clear(2, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_BIND_OPTIONS)].Parameter_value);
            }

            if (EnableBindingfeature.Checked == true)
            {
                BitField.set(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_BIND_OPTIONS)].Parameter_value);
            }
            else
            {
                BitField.clear(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_BIND_OPTIONS)].Parameter_value);
            }

            if (LRCuselrc.Checked == true)
            {
                BitField.set(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_LRC_BOARD)].Parameter_value);
            }
            else
            {
                BitField.clear(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_LRC_BOARD)].Parameter_value);
            }
            if (SLRCsecondary.Checked == true)
            {
                BitField.set(1, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_LRC_BOARD)].Parameter_value);
            }
            else
            {
                BitField.clear(1, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_LRC_BOARD)].Parameter_value);
            }

            if (MLRCenterfailure.Checked == true)
            {
                BitField.set(2, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_LRC_BOARD)].Parameter_value);
            }
            else
            {
                BitField.clear(2, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_LRC_BOARD)].Parameter_value);
            }
            if (SLRCenterfailure.Checked == true)
            {
                BitField.set(3, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_LRC_BOARD)].Parameter_value);
            }
            else
            {
                BitField.clear(3, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_LRC_BOARD)].Parameter_value);
            }


            if (LRConlyreadservos.Checked == true)
            {
                BitField.set(0, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_LRC_CONFIG_MODE)].Parameter_value);
            }
            else
            {
                BitField.clear(3, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_LRC_CONFIG_MODE)].Parameter_value);
            }

            #endregion 



            // Flight :
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_CLIMB_SPEED)].Parameter_value = (int)UnitConversion.kmPerH_fPerS(double.Parse(climbSpeedT.Text));
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_CRUISE_SPEED)].Parameter_value = (int)UnitConversion.kmPerH_fPerS(double.Parse(cruiseSpeedT.Text));
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DESCENT_RATE)].Parameter_value = (int)UnitConversion.mPerMin_ftPerS(double.Parse(descentRateT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DESCENT_SPEED)].Parameter_value = (int)UnitConversion.kmPerH_fPerS(double.Parse(descentSpeedT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DESCENT_TYPE)].Parameter_value = int.Parse(descentTypeT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_FENCE_EAST)].Parameter_value = int.Parse(eastFenceT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_FENCE_NORTH)].Parameter_value = int.Parse(northFenceT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_FENCE_SOUTH)].Parameter_value = int.Parse(southFenceT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_FENCE_WEST)].Parameter_value = int.Parse(westFenceT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_LOCKED_PITCH)].Parameter_value = (int)UnitConversion.deg_rad(double.Parse(lockedPichtT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_TAKEOFF_PITCH)].Parameter_value = (int)UnitConversion.deg_rad(double.Parse(takeoffPichT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_WAYPOINT_DIAMETER)].Parameter_value = (int)UnitConversion.meters_feet(double.Parse(wpDiaT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_WAYPOINT_REDUCE_TURN)].Parameter_value = (int)(double.Parse(wpReduceTurnT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_OFF_GROUND_ALGORITHM)].Parameter_value = (int)(double.Parse(offGroundAlgoT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_ROTATION_SPEED)].Parameter_value = (int)(UnitConversion.kmPerH_fPerS(double.Parse(rotationSpeedT.Text)));


                //vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_MAXIMUM_FLIGHT_ALTITUDE)].Parameter_value = int.Parse(climbSpeedT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_MAX_LAUNCH_PLATFORM_SPEED)].Parameter_value =(int)UnitConversion.kmPerH_fPerS(double.Parse(maxLunchPlatformSpeedT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_MAX_SPEED_INCREASE)].Parameter_value =(int)UnitConversion.kmPerH_fPerS(double.Parse(maxSpeedIncreasT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_MAX_TARGET_ALTITUDE)].Parameter_value = (int)UnitConversion.meters_feet(double.Parse(maxTargrtAltT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_MAX_TARGET_SPEED)].Parameter_value =(int)UnitConversion.kmPerH_fPerS(double.Parse(maxTargetSpeedT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_MIN_TARGET_ALTITUDE)].Parameter_value = (int)UnitConversion.meters_feet(double.Parse(minTargetAltT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_MIN_TARGET_SPEED)].Parameter_value =(int)UnitConversion.kmPerH_fPerS(double.Parse(minTargetSpeedT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_STALL_SPEED)].Parameter_value =(int)UnitConversion.kmPerH_fPerS(double.Parse(stallSpeedT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_ON_GROUND_SPEED)].Parameter_value =(int)UnitConversion.kmPerH_fPerS(double.Parse(onGroundSpeedT.Text));
               // vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_MAXIMUM_FLIGHT_ALTITUDE)].Parameter_value = int.Parse(maxFlightAltT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_CRUISE_SPEED)].Parameter_value =(int)UnitConversion.kmPerH_fPerS(double.Parse(cruiseSpeedT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_MAX_SPEED_INCREASE)].Parameter_value =(int)UnitConversion.kmPerH_fPerS(double.Parse(maxSpeedIncreasT.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DESCENT_TYPE)].Parameter_value = int.Parse(descentTypeT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_CLIMB_MARGIN)].Parameter_value = (int)UnitConversion.meters_feet(double.Parse(cimbMarginT.Text));
                //vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_DEAD_RECKONING)].Parameter_value = (enableDeadcheckBox.Checked == true) ? 1 : 0;
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_USE_DEAD_RECKONING)].Parameter_value = (enableDeadcheckBox.Checked == true) ? 1 : 0;
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_ALWAYS_USE_LINE_CLIMB)].Parameter_value = (alwaysUseLineClimb.Checked == true) ? 1 : 0;
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_INVERT_FENCE_NORTHSOUTH)].Parameter_value = (invertFenceNScheckBox.Checked == true) ? 1 : 0;
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_INVERT_FENCE_EASTWEST)].Parameter_value = (invertFenceEWcheckBox.Checked == true) ? 1 : 0;
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_MAKE_FLYTO_FROMTOS)].Parameter_value = (flyTosFromTosLineUp.Checked == true) ? 1 : 0;
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DISABLE_FROMTO)].Parameter_value = (fromTosToFlyTos.Checked == true) ? 1 : 0;

                double max = 32767;
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_CRUISE_THROTTLE)].Parameter_value = (int)Math.Round(map(cruiseSlider.Value, 0, 100, 0, 32767));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_APPROACH_THROTTLE)].Parameter_value = (int)Math.Round(map(approachSlider.Value, 0, 100, 0, 32767));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_CLIMB_THROTTLE)].Parameter_value = (int)Math.Round(map(climbSlider.Value, 0, 100, 0, 32767));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DESCENT_THROTTLE)].Parameter_value = (int)Math.Round(map(descentSlider.Value, 0, 100, 0, 32767));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_TAKEOFF_THROTTLE)].Parameter_value = (int)Math.Round(map(takeoffSlider.Value, 0, 100, 0, 32767));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.onGround)].Parameter_value = (int)Math.Round(map(onGroundSlider.Value, 0, 100, 0, 32767));

                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_TH_SLEW_LIMIT)].Parameter_value = (int)Math.Round(map(slewLimitSlider.Value, 0, 100, 0, 32767));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_FATAL_ERROR_MAX_THROTTLE)].Parameter_value = (int)Math.Round(map(fatalErrorSlider.Value, 0, 100, 0, 32767));          
                
                //****************************************\\
                // PID : 

                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_ENABLE_YAW_DAMPER)].Parameter_value = (EnableYawDamper.Checked == true) ? 1 : 0;
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_ENABLE_SMOOTH_GAIN_SCHEDULING)].Parameter_value = (SmoothGainScheduling.Checked == true) ? 1 : 0;

                updateToolBoxes(true);
                UpdateVrsArrayPID();

                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidGnSchaRecord_1)].Parameter_value = (int)UnitConversion.kmPerH_fPerS(double.Parse(Range0T.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidGnSchbRecord_1)].Parameter_value = (int)UnitConversion.kmPerH_fPerS(double.Parse(Range1T.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidGnSchcRecord_1)].Parameter_value = (int)UnitConversion.kmPerH_fPerS(double.Parse(Range2T.Text));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex(PIDFieldsID.pidGnSchdRecord_1)].Parameter_value = (int)UnitConversion.kmPerH_fPerS(double.Parse(Range3T.Text)); 

                setOriginalPIDDataFields();

                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.pidOutputSlewLimit)].Parameter_value = (int)double.Parse(SlewLimitsT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.pidMaxResult)].Parameter_value = (int)double.Parse(ResultMaxT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.pidMinResult)].Parameter_value = (int)double.Parse(ResultMinT.Text);

    
            
            //****************************************\\

                //Sevoes :


                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_FLAP_SLEW_LIMIT)].Parameter_value = int.Parse(FlapSlewLimitT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_STUFF_SERVOS_CIC_TIMEOUT)].Parameter_value = int.Parse(StaffServoCICTimeoutT.Text)*5;
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_RELEASE)].Parameter_value = int.Parse(ReleaseServoT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_FLAPS_RC_INPUT_CHANNEL)].Parameter_value = int.Parse(FlapsRCInChannelT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_FLAP_SLEW_LIMIT)].Parameter_value = int.Parse(FlapSlewLimitT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SPOILER_RC_INPUT_CHANNEL)].Parameter_value = int.Parse(SpoilerRCInChannelT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SPOILER_SLEW_LIMIT)].Parameter_value = int.Parse(SpoilerSlewLlimitT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_INPUT_CALCULATION)].Parameter_value = int.Parse(InputCalculationT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SERVO_CHUTE)].Parameter_value = int.Parse(ParachuteServoT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_NOSEWHEEL_SERVO)].Parameter_value = int.Parse(NoiseWheelServoT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SPOILER_SERVO)].Parameter_value = int.Parse(SpoilerServoT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_EXTERNAL_GYRO_OUTPUT_SERVO)].Parameter_value = int.Parse(GyroControlServoT.Text);
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_LOCK_SPEED)].Parameter_value = int.Parse(DisengageLockSpeedT.Text);


                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DISABLE_MIXING_LIMITS)].Parameter_value = (DisableMixingLimit.Checked == true) ? 1 : 0;
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_DISABLE_RSTUFFSERVOS)].Parameter_value = (DisableRStaffServo.Checked == true) ? 1 : 0;
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_LOCK)].Parameter_value = (EnableSevoLock.Checked == true) ? 1 : 0;


                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_LOCK_EL)].Parameter_value = (int)Math.Round(map(lockElavatorSlider.Value, -100, 100, -32767, 32767));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_LOCK_AI)].Parameter_value = (int)Math.Round(map(lockAileronSlider.Value, -100, 100, -32767, 32767));
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_LOCK_RDR)].Parameter_value = (int)Math.Round(map(lockRudderSlider.Value, -100, 100, -32767, 32767));

                
         //   }
         //catch (Exception IndexOutOfRangeException)
            
         //   {
                  
         //   }
     
     
     
     }


     void   updateInit(ref int max , ref int min , int index )
        {
              if (init1.SelectedIndex == 1)
                  BitField.set(index, ref max);

                else if (init1.SelectedIndex == 2)
                  BitField.set(index, ref min);
             
                else
                {
                    BitField.clear(index, ref max);
                    BitField.clear(index, ref min);
                }

                if (init2.SelectedIndex == 1)
                    BitField.set(index+1, ref max);

                else if (init2.SelectedIndex == 2)
                    BitField.set(index+1, ref min);

                else
                {
                    BitField.clear(index+1, ref max);
                    BitField.clear(index+1, ref min);
                }

                if (init3.SelectedIndex == 1)
                    BitField.set(index+2, ref max);

                else if (init3.SelectedIndex == 2)
                    BitField.set(index+2, ref min);

                else
                {
                    BitField.clear(index+2, ref max);
                    BitField.clear(index+2, ref min);
                }

                if (init4.SelectedIndex == 1)
                    BitField.set(index+3, ref max);

                else if (init4.SelectedIndex == 2)
                    BitField.set(index+3, ref min);

                else
                {
                    BitField.clear(index+3, ref max);
                    BitField.clear(index+3, ref min);
                }

                if (init5.SelectedIndex == 1)
                    BitField.set(index+4, ref max);

                else if (init5.SelectedIndex == 2)
                    BitField.set(index+4, ref min);

                else
                {
                    BitField.clear(index+4, ref max);
                    BitField.clear(index+4, ref min);
                }

                if (init6.SelectedIndex == 1)
                    BitField.set(index+5, ref max);

                else if (init6.SelectedIndex == 2)
                    BitField.set(index+5, ref min);

                else
                {
                    BitField.clear(index+5, ref max);
                    BitField.clear(index+5, ref min);
                }

                if (init7.SelectedIndex == 1)
                    BitField.set(index+6, ref max);

                else if (init7.SelectedIndex == 2)
                    BitField.set(index+6, ref min);

                else
                {
                    BitField.clear(index+6, ref max);
                    BitField.clear(index+6, ref min);
                }

                if (init8.SelectedIndex == 1)
                    BitField.set(index+7, ref max);

                else if (init8.SelectedIndex == 2)
                    BitField.set(index+7, ref min);

                else
                {
                    BitField.clear(index+7, ref max);
                    BitField.clear(index+7, ref min);
                }
        }

        void initChanged(object sender, System.EventArgs e)
        {
            changesFlag = true;
            if (!firstIinit)
            {
                int max = (int)vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_INITIALIZE_SERVO_MAXIMUM)].Parameter_value;
                int min = (int)vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_INITIALIZE_SERVO_MINIMUM)].Parameter_value;


                if (ServoNumComboBox.SelectedIndex == 0)
                {
                    updateInit(ref max, ref min, 0);

                }

                else if (ServoNumComboBox.SelectedIndex == 1)
                {
                    updateInit(ref max, ref min, 8);

                }
                else
                {
                    updateInit(ref max, ref min, 16);

                }

                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_INITIALIZE_SERVO_MAXIMUM)].Parameter_value = max;
                vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_INITIALIZE_SERVO_MINIMUM)].Parameter_value = min;
            }
        }

     void checkVrsParameters()
        {
      
         //  GPS Tab
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (GPS1baud.Enabled)
            {
                if ((int.Parse(GPS1baud.Text) <= 0))
                    throw new BusinessException(1505, "GPS Baud is Out of Range ");
            }
            if (GPS1bits.Enabled)
            {
                if ((int.Parse(GPS1bits.Text) <= 0))
                    throw new BusinessException(1506, "GPS Bits is Out of Range ");
            }
            if (GPS1fixrate.Enabled)
            {
                if ((int.Parse(GPS1fixrate.Text) <= 0))
                    throw new BusinessException(1507, "GPS fix rate is Out of Range ");
            }
         if ((int.Parse(GPS2baud.Text) <= 0))
             throw new BusinessException(1508, "GPS2 baud  is Out of Range ");

          if ((int.Parse(GPS2bits.Text) <= 0))
             throw new BusinessException(1509, "GPS2 bits  is Out of Range ");

          if ((int.Parse(GPS2stopbits.Text) <= 0))
              throw new BusinessException(1510, "GPS2 stop bits  is Out of Range ");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
         // Comms Tab
          if ((int.Parse(GcsStrenghtchannel.Text) <= 0))
              throw new BusinessException(1511, "Gcs Strenght channel is Out of Range ");

          if ((int.Parse(GcsStrenghtminimum.Text) <= 0))
              throw new BusinessException(1512, "Gcs Strenght minimum is Out of Range ");

          if ((int.Parse(GcsCommandrepeat.Text) <= 0))
              throw new BusinessException(1513, "Gcs Command repeat is Out of Range ");

          if ((int.Parse(GcsPadCharacter.Text) <= 0))
              throw new BusinessException(1514, "Gcs Pad Character is Out of Range ");

          if ((int.Parse(Gcsintialreport.Text) <= 0))
              throw new BusinessException(1515, "Gcs intial report is Out of Range ");

          if ((int.Parse(Gcsidentifier.Text) <= 0))
              throw new BusinessException(1516, "Gcs identifier is Out of Range ");

          if ((int.Parse(Gcsphonefirst.Text) <= 0))
              throw new BusinessException(1517, "Gcs phone first is Out of Range ");

          if ((int.Parse(Gcsphonesecond.Text) <= 0))
              throw new BusinessException(1518, "Gcs phone last is Out of Range ");

          if ((int.Parse(Gcsenabledialing.Text) <= 0))
              throw new BusinessException(1519, "Gcs enable dialing is Out of Range ");

          if ((int.Parse(Gcstimeount.Text) <= 0))
              throw new BusinessException(1520, "Gcs time out is Out of Range ");



          if ((int.Parse(UART1Rxchannel.Text) <= 0))
              throw new BusinessException(1521, "UART1 Rx channel is Out of Range ");

          if ((int.Parse(UART1Rxbaud.Text) <= 0))
              throw new BusinessException(1522, "UART1 Rx baud is Out of Range ");

          if ((int.Parse(UART1Rxdatabits.Text) <= 0))
              throw new BusinessException(1523, "UART1 Rx data bits is Out of Range ");

          if ((int.Parse(UART1Rxparity.Text) <= 0))
              throw new BusinessException(1524, "UART1 Rx parity is Out of Range ");

          if ((int.Parse(UART1Txchannel.Text) <= 0))
              throw new BusinessException(1525, "UART1 Tx channel is Out of Range ");

          if ((int.Parse(UART1Txbaud.Text) <= 0))
              throw new BusinessException(1526, "UART1 Tx baud is Out of Range ");

          if ((int.Parse(UART1Rxdatabits.Text) <= 0))
              throw new BusinessException(1527, "UART1 Rx data bits is Out of Range ");

          if ((int.Parse(UART1Rxparity.Text) <= 0))
              throw new BusinessException(1528, "UART1 Rx parity is Out of Range ");

          if ((int.Parse(UART1Txtimeout.Text) <= 0))
              throw new BusinessException(1529, "UART1 time out is Out of Range ");



          if ((int.Parse(UART2Rxchannel.Text) <= 0))
              throw new BusinessException(1530, "UART2 Rx channel is Out of Range ");

          if ((int.Parse(UART2Rxbaud.Text) <= 0))
              throw new BusinessException(1531, "UART2 Rx baud is Out of Range ");

          if ((int.Parse(UART2Rxdatabits.Text) <= 0))
              throw new BusinessException(1532, "UART2 Rx data bits is Out of Range ");

          if ((int.Parse(UART2Rxparity.Text) <= 0))
              throw new BusinessException(1533, "UART2 Rx parity is Out of Range ");

          if ((int.Parse(UART2Txchannel.Text) <= 0))
              throw new BusinessException(1534, "UART2 Tx channel is Out of Range ");

          if ((int.Parse(UART2Txbaud.Text) <= 0))
              throw new BusinessException(1535, "UART2 Tx baud is Out of Range ");

          if ((int.Parse(UART2Rxdatabits.Text) <= 0))
              throw new BusinessException(1536, "UART2 Rx data bits is Out of Range ");

          if ((int.Parse(UART2Rxparity.Text) <= 0))
              throw new BusinessException(1537, "UART2 Rx parity is Out of Range ");

          if ((int.Parse(UART2Txtimeout.Text) <=0))
              throw new BusinessException(1538, "UART2 Tx time out is Out of Range ");

         //datalog checked


          if ((int.Parse(Datalogcustomftext1.Text) <= 29999))
              throw new BusinessException(1539, "Custom field #1 is Out of Range ");

          if ((int.Parse(Datalogcustomftext2.Text) <= 29999))
              throw new BusinessException(1540, "Custom field #2 is Out of Range ");

          if ((int.Parse(Datalogcustomftext3.Text) <= 29999))
              throw new BusinessException(1541, "Custom field #3 is Out of Range ");

          if ((int.Parse(Datalogcustomftext4.Text) <= 29999))
              throw new BusinessException(1542, "Custom field #4 is Out of Range ");

          if ((int.Parse(Datalogcustomftext5.Text) <= 29999))
              throw new BusinessException(1543, "Custom field #5 is Out of Range ");

          if ((int.Parse(Datalogcustomftext6.Text) <= 29999))
              throw new BusinessException(1544, "Custom field #6 is Out of Range ");

          if ((int.Parse(Datalogcustomftext7.Text) <= 29999))
              throw new BusinessException(1545, "Custom field #7 is Out of Range ");

          if ((int.Parse(Datalogcustomftext8.Text) <= 29999))
              throw new BusinessException(1546, "Custom field #8 is Out of Range ");

          if ((int.Parse(Datalogcustomftext9.Text) <= 29999))
              throw new BusinessException(1547, "Custom field #9 is Out of Range ");

          if ((int.Parse(Datalogcustomftext10.Text) <= 29999))
              throw new BusinessException(1548, "Custom field #10 is Out of Range ");

          if ((int.Parse(Datalogcustomftext11.Text) <= 29999))
              throw new BusinessException(1549, "Custom field #11 is Out of Range ");

          if ((int.Parse(Datalogcustomftext12.Text) <= 29999))
              throw new BusinessException(1550, "Custom field #12 is Out of Range ");

          if ((int.Parse(Datalogcustomftext13.Text) <= 29999))
              throw new BusinessException(1551, "Custom field #13 is Out of Range ");

          if ((int.Parse(Datalogcustomftext14.Text) <= 29999))
              throw new BusinessException(1552, "Custom field #14 is Out of Range ");

          if ((int.Parse(Datalogcustomftext15.Text) <= 29999))
              throw new BusinessException(1553, "Custom field #15 is Out of Range ");

          if ((int.Parse(Datalogcustomftext16.Text) <= 29999))
              throw new BusinessException(1554, "Custom field #16 is Out of Range ");

          if ((int.Parse(Datalogcustomftext17.Text) <= 29999))
              throw new BusinessException(1555, "Custom field #17 is Out of Range ");

          if ((int.Parse(Datalogcustomftext18.Text) <= 29999))
              throw new BusinessException(1556, "Custom field #18 is Out of Range ");

          if ((int.Parse(Datalogcustomftext19.Text) <= 29999))
              throw new BusinessException(1557, "Custom field #19 is Out of Range ");

          if ((int.Parse(Datalogcustomftext20.Text) <= 29999))
              throw new BusinessException(1558, "Custom field #20 is Out of Range ");

          if ((int.Parse(Datalogcustomftext21.Text) <= 29999))
              throw new BusinessException(1559, "Custom field #21 is Out of Range ");

          if ((int.Parse(Datalogcustomftext1.Text) <= 29999))
              throw new BusinessException(1560, "Custom field #22 is Out of Range ");

          if ((int.Parse(Datalogcustomftext1.Text) <= 29999))
              throw new BusinessException(1561, "Custom field #23 is Out of Range ");

          if ((int.Parse(Datalogcustomftext24.Text) <= 29999))
              throw new BusinessException(1562, "Custom field #24 is Out of Range ");

          if ((int.Parse(Datalogcustomheadfltext1.Text) <= 29999))
              throw new BusinessException(1563, "Custom header field #1 is Out of Range ");

          if ((int.Parse(Datalogcustomheadfltext1.Text) <= 29999))
              throw new BusinessException(1564, "Custom header field #2 is Out of Range ");
          if ((int.Parse(Datalogcustomheadfltext2.Text) <= 29999))
              throw new BusinessException(1565, "Custom header field #3 is Out of Range ");
          if ((int.Parse(Datalogcustomheadfltext3.Text) <= 29999))
              throw new BusinessException(1566, "Custom header field #4 is Out of Range ");
          if ((int.Parse(Datalogcustomheadfltext4.Text) <= 29999))
              throw new BusinessException(1567, "Custom header field #5 is Out of Range ");
          if ((int.Parse(Datalogcustomheadfltext5.Text) <= 29999))
              throw new BusinessException(1568, "Custom header field #6 is Out of Range ");
          if ((int.Parse(Datalogcustomheadfltext6.Text) <= 29999))
              throw new BusinessException(1569, "Custom header field #7 is Out of Range ");
          if ((int.Parse(Datalogcustomheadfltext7.Text) <= 29999))
              throw new BusinessException(1570, "Custom header field #8 is Out of Range ");
          if ((int.Parse(Datalogcustomheadfltext8.Text) <= 29999))
              throw new BusinessException(1571, "Custom header field #9 is Out of Range ");

         //DataLog Tab



         //Flghit Tab 

          if((double.Parse(cimbMarginT.Text) <= 30.48))
                throw new BusinessException(1572, "Cimb Margin is Out of Range ");

          if ((double.Parse(wpDiaT.Text) <0))
              throw new BusinessException(1573, "Wp Dia is Out of Range "); 
         
          if ((double.Parse(wpReduceTurnT.Text) <0))
              throw new BusinessException(1574, "Wp Reduce Turn is  Out of Range");

          if ((double.Parse(takeoffPichT.Text) < 0) || (double.Parse(takeoffPichT.Text) >360) )
              throw new BusinessException(1575, "Take off Pich is  Out of Range");

          if ((double.Parse(lockedPichtT.Text) < 0) || (double.Parse(lockedPichtT.Text) > 360))
              throw new BusinessException(1576, "Locked Picht is  Out of Range"); 
         
          if ((double.Parse(maxLunchPlatformSpeedT.Text) < 0) )
              throw new BusinessException(1577, "Max Lunch Platform Speed is  Out of Range");


          if (double.Parse(northFenceT.Text) < 0)
              throw new BusinessException(1578, "North Fence is  Out of Range");

          if (double.Parse(eastFenceT.Text) < 0)
              throw new BusinessException(1579, "East Fence is  Out of Range");

          //if (double.Parse(westFenceT.Text) < 0)
          //    throw new BusinessException(1507, "West Fence is  Out of Range");

          //if (double.Parse(southFenceT.Text) < 0)
          //    throw new BusinessException(1508, "South Fence is  Out of Range"); 

         
          if ((double.Parse(maxTargrtAltT.Text) < 0) )
              throw new BusinessException(1580, "Max Targrt Alt is  Out of Range");

         if ((double.Parse(maxTargetSpeedT.Text) < 0) )
              throw new BusinessException(1581, "Max Target Speed is  Out of Range");
         
          if (double.Parse(stallSpeedT.Text) < 0)
              throw new BusinessException(1582, "Stall Speed is  Out of Range");

          if (double.Parse(maxFlightDistT.Text) < 0)
              throw new BusinessException(1583, "Max Flight Dist is  Out of Range");

          if ((double.Parse(minTargetAltT.Text) < 0))
              throw new BusinessException(1584, "Min Targrt Alt is  Out of Range");

          if ((double.Parse(minTargetSpeedT.Text) < 0))
              throw new BusinessException(1585, "Min Target Speed is  Out of Range");

          if ((double.Parse(onGroundSpeedT.Text) < 0))
              throw new BusinessException(1586, "On Ground Speed is  Out of Range");

          if ((double.Parse(maxFlightAltT.Text) < 0))
              throw new BusinessException(1587, "Max Flight Alt is  Out of Range");

          if ((double.Parse(rotationSpeedT.Text) < 0))
              throw new BusinessException(1588," Rotation Speed is  Out of Range");
         
          //if ((double.Parse(rotationSpeedT.Text) < 0))
          //    throw new BusinessException(1515," Rotation Speed is  Out of Range");

              if ((double.Parse(cruiseSpeedT.Text) < 0))
              throw new BusinessException(1589,"Cruise Speed is  Out of Range");           

                  if ((double.Parse(maxSpeedIncreasT.Text) < 0))
              throw new BusinessException(1590," Max Speed Increas is  Out of Range");
         
             if ((double.Parse(climbSpeedT.Text) < 0))
              throw new BusinessException(1591," Climb Speed is  Out of Range");
            
           if ((double.Parse(descentRateT.Text) < 0))
              throw new BusinessException(1592," Descent Rate is  Out of Range");        
                 
                  if ((double.Parse(descentSpeedT.Text) < 0))
              throw new BusinessException(1593," Descent SpeedTis  Out of Range");

                  if ((double.Parse(descentTypeT.Text) != 0) || (double.Parse(descentTypeT.Text) != 1))
                      throw new BusinessException(1594, " Descent Type is not Valid");
         

         // PID Tab 


                  if (double.Parse(ResultMaxT.Text) != 32767)
                      throw new BusinessException(1595, " Result Max is not Valid");

                  if (double.Parse(ResultMinT.Text) != -32767)
                      throw new BusinessException(1596, " Result Min is not Valid");

                  if (double.Parse(ResAntiWindupMaxT.Text) != 16383)
                      throw new BusinessException(1597, " Result Anti-Windup Max is not Valid");

                  if (double.Parse(ResAntiWindupMinT.Text) != -16383)
                      throw new BusinessException(1598, " Result Anti-Windup Min is not Valid");
      

           if (double.Parse(Sch0T.Text) <0)
               throw new BusinessException(1599, " Sch is Out of Range");

         if( SelectedPIDLloopCombo.SelectedIndex == 0 || SelectedPIDLloopCombo.SelectedIndex == 1 ||
             SelectedPIDLloopCombo.SelectedIndex == 2 || SelectedPIDLloopCombo.SelectedIndex == 4 ||
             SelectedPIDLloopCombo.SelectedIndex == 5 || SelectedPIDLloopCombo.SelectedIndex == 6 ||
             SelectedPIDLloopCombo.SelectedIndex == 8 || SelectedPIDLloopCombo.SelectedIndex == 13 ||
              SelectedPIDLloopCombo.SelectedIndex == 14 ||  SelectedPIDLloopCombo.SelectedIndex == 15)
         {
           if (double.Parse(Range0T.Text) < 0)    
               throw new BusinessException(1600, " Air Speed is Out of Range");
           

           if (double.Parse(Range1T.Text) < 0)
               throw new BusinessException(1601, " Air Speed is Out of Range");


           if (double.Parse(Range2T.Text) < 0)
               throw new BusinessException(1602, " Air Speed is Out of Range");


           if (double.Parse(Range3T.Text) < 0)
               throw new BusinessException(1603, " Air Speed is Out of Range");      
         
     }

         else if (SelectedPIDLloopCombo.SelectedIndex == 3 || SelectedPIDLloopCombo.SelectedIndex == 9 ||
            SelectedPIDLloopCombo.SelectedIndex == 10 || SelectedPIDLloopCombo.SelectedIndex == 12)
         {
             if (double.Parse(Range0T.Text) < 0)
                 throw new BusinessException(1604, " Ground Speed is Out of Range");


             if (double.Parse(Range1T.Text) < 0)
                 throw new BusinessException(1605, " Ground Speed is Out of Range");


             if (double.Parse(Range2T.Text) < 0)
                 throw new BusinessException(1606, " Ground Speed is Out of Range");


             if (double.Parse(Range3T.Text) < 0)
                 throw new BusinessException(1607, " Ground Speed is Out of Range");

         }

         else
         {
             //Range
         }
         

        
         // servoes
         if ((double.Parse(FlapSlewLimitT.Text) < -32767) || (double.Parse(FlapSlewLimitT.Text) > 32767))
             throw new BusinessException(1608, " Flap Slew Limit is Out of Range");

         if (double.Parse(StaffServoCICTimeoutT.Text) < 0)
             throw new BusinessException(1609, "Staff Servo CIC Time Out is Out of Range");

         if (int.Parse(ParachuteServoT.Text) < 0 || int.Parse(ParachuteServoT.Text) >32 )
             throw new BusinessException(1610, "Parachute Servo is not valid");

         if (int.Parse(ReleaseServoT.Text) < 0 || int.Parse(ReleaseServoT.Text) >32 )
             throw new BusinessException(1611, "Release Servo is not valid");

           if (int.Parse(NoiseWheelServoT.Text) < 0 || int.Parse(NoiseWheelServoT.Text) >32 )
             throw new BusinessException(1612, "Noise Wheel Servo is not valid");

           if (int.Parse(NoiseWheelServoT.Text) < 0 || int.Parse(NoiseWheelServoT.Text) >32 )
             throw new BusinessException(1613, "Noise Wheel Servo is not valid");

          if (int.Parse(SpoilerServoT.Text) < 0 || int.Parse(SpoilerServoT.Text) >32 )
             throw new BusinessException(1614, "Spoiler Servo is not valid");

          if (int.Parse(GyroControlServoT.Text) < 0 || int.Parse(GyroControlServoT.Text) > 32)
              throw new BusinessException(1615, "Gyro Control Servo is not valid");

          if (double.Parse(DisengageLockSpeedT.Text) < 0 )
              throw new BusinessException(1616, "Disengage Lock Speed is Out of Range");

          if (int.Parse(InputCalculationT.Text) < 0 || int.Parse(InputCalculationT.Text) >2)
              throw new BusinessException(1617, "Input Calculation is not Valid");

           // FlapsRCInChannelT    
            //    SpoilerRCInChannelT


          if ((double.Parse(SpoilerSlewLlimitT.Text) < -32767) || (double.Parse(SpoilerSlewLlimitT.Text) > 32767))
              throw new BusinessException(1618, "Spoiler Slewl Limit is Out of Range");

         // Here we put the limts of our value
        }

    
     
    
       
     
        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click_1(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void label67_Click(object sender, EventArgs e)
        {

        }

        private void label80_Click(object sender, EventArgs e)
        {

        }

        private void label87_Click(object sender, EventArgs e)
        {

        }

        private void label93_Click(object sender, EventArgs e)
        {

        }

        private void textBox59_TextChanged(object sender, EventArgs e)
        {

        }

        private void MicroPilotSetup_Load(object sender, EventArgs e)
        {

        }

        private void textBox60_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox39_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox58_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox40_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox44_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox43_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox42_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox45_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox48_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox47_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox46_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox49_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox53_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox52_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox54_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox55_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox51_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox56_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox62_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox61_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox64_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void colorSlider1_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void colorSlider8_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void groupBox6_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox10_Enter(object sender, EventArgs e)
        {

        }

        private void levelFlightModecomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox7_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void saveVrsFile_Click(object sender, EventArgs e)
        {
            checkVrsParameters();
        }

        private void ScheduledT_TextChanged(object sender, EventArgs e)
        {
           // double Num;
           // TextBox triggeredT = sender as TextBox;
           //if(! double.TryParse( triggeredT.Text , out Num)){
           //    triggeredT.Clear();
           //    throw new BusinessException(1, "Input is either non integr or empty");
           //}
        }

        private void Scheduledkk_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
            double Num;
            TextBox triggeredT = sender as TextBox;
            if (manualChange)
            {
                if (triggeredT == Range0T)
                {
                    Sch1T.Text = triggeredT.Text; 
                }
               else if (triggeredT == Range1T)
                {
                    Sch2T.Text = triggeredT.Text;
                }
               else if (triggeredT == Range2T)
                {
                    Sch3T.Text = triggeredT.Text;
                }

                if (triggeredT.Text[0] == '-')//&& triggeredT.Text.Length != 1)
                {
                    //  triggeredT.Text = triggeredT.Text.Substring(1);

                    cont = true;
                }
                else
                    cont = false;
                if(!cont)
                if (!double.TryParse(triggeredT.Text, out Num))
                {
                    // triggeredT.Clear();
                    throw new BusinessException(1619, "Input is Either non Integer or Empty");
                }
            }
        }

        private void ServoNumComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void TO_SelectedIndexChanged(object sender, EventArgs e)
        {
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_THROTTLE_OVERRIDE)].Parameter_value = TO.SelectedIndex;
            changesFlag = true;
        }

        private void throttleIdel_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void lockElavatorSlider_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void groupBox9_Enter(object sender, EventArgs e)
        {

        }

        private void Save_Click(object sender, EventArgs e)
        {
            
            Update_VrsLines_list();
            vrsHandler.fileUIMapping();
            MessageBox.Show(" Changes Saved ");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            vrsHandler.restoreDefaults();
            fillAllDataFields();
            vrsname.Text = "Default file";
            MessageBox.Show("Defaults Restored");
        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void label137_Click(object sender, EventArgs e)
        {

        }

        private void label131_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
            switch (GcsParity.SelectedIndex)
            { 
                case 0:
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GCS_PARITY)].Parameter_value = 0;
            break;
                case 1:
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GCS_PARITY)].Parameter_value = 2;
            break;
                case 2:
            vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GCS_PARITY)].Parameter_value = 3;
            break;
                default:
            break;

        }
        }

        private void textBox18_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void label101_Click(object sender, EventArgs e)
        {

        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GPS_Click(object sender, EventArgs e)
        {

        }

        private void groupBox25_Enter(object sender, EventArgs e)
        {

        }

        private void label162_Click(object sender, EventArgs e)
        {

        }

        private void Dataloglogging_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void GPS1type_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
            switch (GPS1type.SelectedIndex)
            { 
                case 0:
                    Ubloxgroup.Visible=false;
                    SBASgroup.Visible=false;
                    Novatelgroup.Visible = false;
                    GPS1baud.Enabled = false;
                    GPS1parity.Enabled = false;
                    GPS1bits.Enabled = false;
                    GPS1fixrate.Enabled = false;
                    break;
                case 1:
                      Ubloxgroup.Visible=true;
                    SBASgroup.Visible=true;
                    Novatelgroup.Visible = false;
                    GPS1baud.Enabled = true;
                    GPS1parity.Enabled = true;
                    GPS1bits.Enabled = true;
                    GPS1fixrate.Enabled = false;

                    break;
                case 2:
                     Ubloxgroup.Visible=false;
                    SBASgroup.Visible=false;
                    Novatelgroup.Visible = false;
                    GPS1baud.Enabled = false;
                    GPS1parity.Enabled = false;
                    GPS1bits.Enabled = false;
                    GPS1fixrate.Enabled = false;
                    break;
                case 3:
                    Ubloxgroup.Visible=false;
                    SBASgroup.Visible=false;
                    Novatelgroup.Visible = true;
                    GPS1baud.Enabled = true;
                    GPS1parity.Enabled = true;
                    GPS1bits.Enabled = false;
                    GPS1fixrate.Enabled = true;
                    break;
                default :
                    break;
  
            
            }
             
        }

        private void customuarts_Click(object sender, EventArgs e)
        {
            MessageBox.Show("this button for configure UART but not implemented ");
        }

      

        private void Datalogenablecustom_CheckedChanged_1(object sender, EventArgs e)
        {
            changesFlag = true;
            if (Datalogenablecustom.Checked)
            {
                Datalogform1.Visible = true;
            }

            else
                Datalogform1.Visible = false;
        }

        private void Novatelgroup_Enter(object sender, EventArgs e)
        {

        }

        private void transmite_vrs(object sender, EventArgs e)
        {
            myVRSTransmitForm.ShowDialog();
        }

        private void acquire_vrs(object sender, EventArgs e)
        {
            myVRSReceiveForm.ShowDialog();
        }

        private void flyTosFromTosLineUp_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void fromTosToFlyTos_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void alwaysUseLineClimb_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void cruiseSlider_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void approachSlider_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void climbSlider_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void descentSlider_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void takeoffSlider_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void onGroundSlider_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void slewLimitSlider_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void fatalErrorSlider_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void invertFenceNScheckBox_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void invertFenceEWcheckBox_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void allowNnavigationAirSpeedcheckBox_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void SmoothGainScheduling_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void EnableYawDamper_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void AutoPilotUnites_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void RealUnites_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GainEffects_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void SelectedPIDLloopCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Sch1T_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Sch2T_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void CalculateAlironCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void CompatiblityCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void EchoServoAdjustment_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void servoRudderComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void t1_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void z2_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void z3_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void z4_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void z5_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void z6_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void z7_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void z8_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void swap1_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void t2_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void swap2_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void t3_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void swap3_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void swap8_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void swap7_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void swap6_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void swap5_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void swap4_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void t4_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void t5_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void t6_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void t7_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void t8_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void throttleIdel_Scroll_1(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void EnableSevoLock_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void lockElavatorSlider_Scroll_1(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void lockAileronSlider_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void lockRudderSlider_Scroll(object sender, ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void Vrsheadercommands_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Gcstimeount_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Gcsenabledialing_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Gcsphonesecond_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Gcsphonefirst_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Gcsidentifier_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Gcsintialreport_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GcsPadCharacter_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GcsCommandrepeat_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GcsStrenghtminimum_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GcsStrenghtchannel_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Gcschecksum_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Gcsflowcontrol_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Gcsbaud_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART1protocol_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART1Rxchannel_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART1Rxdatabits_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART1Rxparity_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART1Txchannel_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART1Txbaud_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART1Txbatabits_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART1Txparity_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART1protocolfield_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART1disablebinding_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART2protocol_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART2Rxchannel_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART2Rxbaud_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART2Rxdatabits_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART2Rxparity_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART2Txchannel_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART2Txbaud_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART2Txdatabits_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART2Txparity_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART2Txtimeout_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART2Txprotocolfield_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void UART2disablebinding_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Vrsfootercommands_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void EnableBindingfeature_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void LRCuselrc_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void SLRCsecondary_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void MLRCenterfailure_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void SLRCenterfailure_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void LRConlyreadservos_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void LRCsecodarycomport_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void aglsensor_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void aglmode_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void aglswitchaltitude_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void agllowthrottlesetting_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void agllowthrottleswitch_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GPS1baud_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GPS1bits_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GPS1fixrate_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GPS1fakegps_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GPS1usegpsaltitude_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GPS1usegpsspeed_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GPS1kalmanfilter_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GPS1hemisphere_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void ubloxdynamicmodel_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void SBASenable_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void SBASapplyintegrity_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void SBASusetestmode_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void SBASnumberofchannel_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void SBASenterPRNs_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GPS2flowcontrol_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GPS2baud_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GPS2bits_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void GPS2stopbits_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Novatelenablediasblenovatel_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Novatelenablecorrection_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Novatelusel1flaot_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Novatelbasestation_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Novatelenablediableothe_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Dataloglogging_CheckedChanged_1(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogdonoterase_CheckedChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext1_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext2_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext3_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext4_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext5_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext6_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext7_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext8_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext9_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext10_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext11_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext12_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext13_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext14_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext15_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext16_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext17_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext18_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext19_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext20_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext21_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext22_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext23_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomftext24_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomheadfltext1_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomheadfltext2_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomheadfltext3_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomheadfltext4_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomheadfltext5_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomheadfltext6_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomheadfltext7_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void Datalogcustomheadfltext8_TextChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void open_vrsfile_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do You want to Save Changes " + " in "+ filename +" ?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Update_VrsLines_list();
                this.vrsHandler.fileUIMapping();
                MessageBox.Show(" Changes Saved ");
            }
 

            DialogResult result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                fileName = openFileDialog1.FileName;
                // name=openFileDialog1.
                filename = Path.GetFileName(fileName);
                vrsname.Text = filename;
            }
            else
            {
                return;
            }

            VrsFileHandler vrsHandler = new VrsFileHandler(fileName);

            this.vrsHandler = vrsHandler;
            try
            {
                fillAllDataFields();
            }
            catch
            {
                throw new BusinessException(1503, "Corrupted Configurations ");
            }
        }

        private void z1_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
        {
            changesFlag = true;
        }

        private void ServoTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
        }

        private void SBASsatellite_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;

            switch (SBASsatellite.SelectedIndex)
            {
                
              
                case 1:
                    BitField.set(8, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SBAS_ENABLE)].Parameter_value);
                    break;
                case 2:
                    BitField.set(9, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SBAS_ENABLE)].Parameter_value);
                    break;
                case 3:
                    BitField.set(8, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SBAS_ENABLE)].Parameter_value);
                    BitField.set(9, ref vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_SBAS_ENABLE)].Parameter_value);
                    break;
                default:
                    SBASsatellite.SelectedIndex = 0;
                    break;

            }

        }

        private void GPS1parity_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
            switch (GPS1parity.SelectedIndex)
            {
                case 0:
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_PARITY)].Parameter_value = 0;
                    break;
                case 1:
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_PARITY)].Parameter_value = 2;
                    break;
                case 2:
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_PARITY)].Parameter_value = 3;
                    break;
                default:
                    break;

            }
        }

        private void GPS2parity_SelectedIndexChanged(object sender, EventArgs e)
        {
            changesFlag = true;
            switch (GPS2parity.SelectedIndex)
            {
                case 0:
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_PORT_B_PARITY)].Parameter_value = 0;
                    break;
                case 1:
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_PORT_B_PARITY)].Parameter_value = 2;
                    break;
                case 2:
                    vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_PORT_B_PARITY)].Parameter_value = 3;
                    break;
                default:
                    break;

            }


        }

       

    }
}
