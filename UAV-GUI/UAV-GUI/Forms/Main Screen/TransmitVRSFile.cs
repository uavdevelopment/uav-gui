﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MP_Lib;
using System.Threading;
using Error_Reporting;

namespace UAV_GUI.Forms.Main_Screen
{
    public partial class TransmitVRSFile : Form
    {
        public UAV myUav;
        public String fileNme;
        public TransmitVRSFile(UAV myUAV_,bool Receive)
        {
            myUav=myUAV_;
            InitializeComponent();
            if (Receive)
            {
                loadCurrent.Visible = false;
                savToFlash.Visible = false;
                saveCancelButton.Visible = false;
                simulatorL.Visible = false;
 
            }
            else {

                receiveButton.Visible = false;           
            }
            ApplicationLookAndFeel.UseTheme(this);
           
        }
        private Thread transmotThread;
        private void TransmitVRSFile_Load(object sender, EventArgs e)
        {
        }
        private void saveCancelButton_Click(object sender, EventArgs e)
        {
            
            if (saveCancelButton.Text == "Save")
            {
                fileNme = @"vrs\vrsFile.vrs";
                if (!loadCurrent.Checked)
                {
                    DialogResult result = openFileDialog1.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        fileNme = openFileDialog1.FileName;
                    }
                    else
                    {
                        return;
                    }
                }
                transmotThread = new Thread(new ThreadStart(transmitVRSFile));
                transmotThread.Start();
                saveCancelButton.Text = "Cancel";
                progressBar1.Style = ProgressBarStyle.Marquee;

            }
            else {

                saveCancelButton.Text = "Save";
                transmotThread.Abort();
                transmotThread = null;
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 0;
            }
        }
        private void transmitVRSFile ()
        {
          
            int retValu=0;
               
           retValu = myUav.transmitVRSFileAndReportProgress(fileNme,savToFlash.Checked,simulatorL.Checked);
           progressBar1.Style = ProgressBarStyle.Blocks;
           progressBar1.Value = 0;
           saveCancelButton.Text = "Save";
           if (retValu == 0)
           {
               MessageBox.Show("Tranmitting The VRS File is done");
           }
           else {
               
            MessageBox.Show("Failed to transmit the fields <reply code is " + retValu + ">.", "Error (1101)",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
           }
        }
        private void ReceiveVRSFile()
        {

            int retValu = 0;

            retValu = myUav.GetVRsFromAutoPilot(fileNme);
            progressBar1.Style = ProgressBarStyle.Blocks;
            progressBar1.Value = 0;
            receiveButton.Text = "Receive";
            if (retValu == 0)
            {
                MessageBox.Show("File is acquired from Autopilot properly");
            }
            else
            {

                MessageBox.Show("Failed to receive the fields <reply code is " + retValu + ">.", "Error (1102)",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void receiveButton_Click(object sender, EventArgs e)
        {
            if (receiveButton.Text == "Receive")
            {
               
                DialogResult result = saveFileDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    fileNme = saveFileDialog1.FileName;
                }
                else
                {
                    return;
                }
                
                transmotThread = new Thread(new ThreadStart(ReceiveVRSFile));
                transmotThread.Start();
                receiveButton.Text = "Cancel";
                progressBar1.Style = ProgressBarStyle.Marquee;

            }
            else
            {

                receiveButton.Text = "Receive";
                transmotThread.Abort();
                transmotThread = null;
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 0;
            }
        }
        
    }
}
