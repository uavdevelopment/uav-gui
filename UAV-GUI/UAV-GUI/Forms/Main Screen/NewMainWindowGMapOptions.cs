﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Threading;
using UAV_GUI.FileClasses;
using MasterClasses;
using Error_Reporting;
using MP_Lib;
using JoystickPlugin;
using FileClasses.VRS_Files;
using FileClasses.INI_Files;
using UAV_GUI.FileClasses.FlyFiles;
using UAV_GUI.FlightClasses;
using UAV_GUI.Forms.Setup.MP;
using UAV_GUI.Forms;
using UAV_GUI.Forms.Main_Screen;
using GMap.NET.MapProviders;
using UAV_GUI.Utilities;
using FlightMonitor;
using System.Diagnostics;
using UAV_GUI.Forms.Set_Variable;
using GLobal;


namespace UAV_GUI
{
    public partial class NewMainWindow
    {
        private void map1_MouseDoubleClick_(object sender, MouseEventArgs e)
        {
            MessageBox.Show("Mouse Clicked");

            PointLatLng p = gmap.FromLocalToLatLng(e.X, e.Y);
            if (MainMap_orlong == -1 || MainMap_orlat == -1)
            {
                if (MessageBox.Show("Can't Add WP Befor Setting the Origin Location \n Do You Want to Set it Here ? ", "Origin Setting", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MainMap_orlong = p.Lng;
                    MainMap_orlat = p.Lat;
                    gmap.Position = p;
                    MessageBox.Show("Orign Set Successfully ");

                }

            }
            else
            {
                WayPoint n = MapOptions.addWP(p, ref route, ref routeList,
                                        ref markersOverlay, ref markersList, ref gmap);
                CommandArg = new List<double>();
                CommandArg.Add(p.Lat);
                CommandArg.Add(p.Lng);
                //commandsList.Add(new Command(CommandType.basic , CommandName.flyTo, CommandArg) );
                Command comd = new Command(MasterClasses.CommandType.basic, MasterClasses.CommandName.flyTo, CommandArg);
                n.commandsList.Add(comd);
                wayPointList.Add(n);
            }

        }
        void MainMap_MouseMove1(object sender, MouseEventArgs e)
        {
            gmap.SendToBack();
        }

        void MainMap_MouseMove(object sender, MouseEventArgs e)
        {
            gmap.SendToBack();
            if (e.Button == MouseButtons.Left && isMouseDown && Control.ModifierKeys != Keys.Alt)
            {
                PointLatLng point = gmap.FromLocalToLatLng(e.X, e.Y);
                if (selectedMarker != null && selectedMarkerFlag) ;

                //MapOptions.Drag(ref  route, ref  selectedMarker, point, ref  gmap);
            }
            if (panFlag)
            {
                this.gmap.Position = gmap.FromLocalToLatLng(e.X, e.Y);
            }

        }

        private bool panFlag = false;

        void MainMap_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                panFlag = false;
            }
            isMouseDown = true;
        }
        private void panel1_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            trackBar1.Value = (int)gmap.Zoom;
        }

        void MainMap_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
            if (e.Button == MouseButtons.Right)
            {
                panFlag = false;
            }
        }

        private void TrackBar1_ValueChanged(object sender, System.EventArgs e)
        {
            MapOptions.mapZooming(ref sender, ref gmap);
        }



        void on_marker_click(GMapMarker sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && Control.ModifierKeys == Keys.Alt)
            {
                ScrewTurn.DropDownPanel WayPointPanel = null;
                MapOptions.MarkerSelection(ref  selectedMarkerFlag, ref  selectedMarker,
                                 ref  sender, ref  selectedMarkerDeleted,
                                ref  WayPointPanel, ref  markersOverlay, ref  markersList);
            }
        }

        private void on_marker_enter(GMapMarker sender)
        {
            //selectedMarker = (GMapMarker)sender;         
        }

        private void on_marker_leave(GMapMarker sender)
        {
            // selectedMarker = null;
        }


        private void updateMap()
        {

            while (true)
            {

            }
        }



        void nearNextWP_Indecation()
        {
            // if (myFlight.flyData.DTNWP <= 0.1) // for example
            // {    
            // need to set currntMarker first 
            // Steps  

            #region 1- return the currnt WP to the orignal color
            PointLatLng position = currntMarker.Position;
            markersOverlay.Markers.Remove(currntMarker);
            markersList.Remove(currntMarker);
            GMarkerGoogle nMarker = new GMarkerGoogle(position, GMarkerGoogleType.blue_small);
            markersOverlay.Markers.Add(nMarker);
            markersList.Add(nMarker);
            currntMarker = nMarker;
            //route.Points.Add(position);
            //routeList.Add(route);
            //gmap.UpdateRouteLocalPosition(route);
            //route.Stroke.Color = Color.Green;
            #endregion

            #region 2- change the currnt WP to the next WP
            // markersList.Remove(currntMarker);
            //markersOverlay.Markers.Remove(currntMarker);
            currntMarker = wayPointList[Count].marker;
            #endregion

            #region 3- change color of the currnt WP
            position = currntMarker.Position;
            nMarker = new GMarkerGoogle(position, GMarkerGoogleType.pink);
            markersOverlay.Markers.Add(nMarker);
            markersList.Add(nMarker);
            currntMarker = nMarker;
            //route.Points.Add(position);
            //routeList.Add(route);
            //gmap.UpdateRouteLocalPosition(route);
            //route.Stroke.Color = Color.Pink;
            #endregion
            //}
        }

        private void reloadMap_Click(object sender, EventArgs e)
        {
            /*ScrewTurn.DropDownPanel WayPointPanel = null;
            MapOptions.deleteMarker(ref  selectedMarker, ref wayPointList
                                , ref  route, ref routeList
                                , ref  markersOverlay, ref markersList,
                                 ref  gmap, ref  selectedMarkerDeleted
                                 , ref  selectedMarkerFlag, ref  WayPointPanel);*/

            initmMap();
            if (!this.flyFileLabel.Text.Equals("No File Loaded"))
                loadFlyFile(fileNme);
            if (updateMapThread != null)
            {
                updateMapStop.Set();
                Thread.Sleep(10);
                updateMapThread.Abort();
                updateMapThread = null;
                Thread.Sleep(10);
                //updateMap
                updateMapThread = new Thread(new ThreadStart(updateMap));
                updateMapStop.Reset();
                updateMapThread.Start();
            }
        }

        private void clearMap_Click(object sender, EventArgs e)
        {

            MapOptions.clearMap(ref gmap);

        }
    }
}
