﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using System.IO.Ports;
using  System.Runtime.InteropServices;
using System.Threading;
using UAV_GUI.FileClasses;
using UAV_GUI.MasterClasses;
using UAV_GUI.Error_Reporting;
using UAV_GUI.CommunicationClass;
using JoystickPlugin;
using UAV_GUI.FileClasses.VRS_Files;
using UAV_GUI.FileClasses.FlyFiles;
using UAV_GUI.FlightClasses;
using UAV_GUI.Forms.Setup.MP;
using UAV_GUI.Forms.ServoTest;
using UAV_GUI.Forms.Main_Screen;
using GMap.NET.MapProviders;
using UAV_GUI.Utilities;
using UAV_GUI.FlightMonitor;
using System.Diagnostics;
using UAV_GUI.Forms.Set_Variable;
namespace UAV_GUI
{
    public partial class OrganizedMainWindow : Form
    {

        Form2 n;
        bool start = true;
        GMapOverlay markersOverlay;
        List<PointLatLng> points;
        GMapRoute route;
        GMapOverlay routesOverlay;
        static string filename = @"vrs\vrsFile.vrs";
        List<WayPoint> wayPointList;
        List<Command> commandsList;
        List<double> CommandArg = new List<double>();
        List<GMapMarker> markersList = new List<GMapMarker>();
        List<GMapRoute> routeList = new List<GMapRoute>();
        GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(39.4666666597666, -76.1681666929581), GMarkerGoogleType.arrow);
        GMarkerGoogle currntMarker;  // NEED TO SET IT
        int Count = 0;
        private static Flight myFlight;

        public Thread gaugeFillingThread;
        public Thread updateMapThread;
        public Thread seekingGPSLock;
        public Thread VisualGraphing;
        public Thread checkForLinkThread;
        GMapMarker selectedMarker = null;
        bool selectedMarkerFlag = false;
        bool selectedMarkerDeleted = false;
        bool isMouseDown = false;

        public double MainMap_orlong = -1;
        public double MainMap_orlat = -1;
        public double MainMap_oralt = -1;
        String fileNme = null;
        public double val = 0;
        private GraphOptions myGraph;
        bool rcChangedToOFF = false;
        
        /*Forms Invoked Upon Click from Any Button and/Or ToolStrip*/
        static FailurePatternSimulation myFailurePatternsToolstrip ;
        static TransmitFlyFile myFlyFileTransmisionForm ;
        static TransmitVRSFile myVRSReceiveForm;
        static TransmitVRSFile myVRSTransmitForm;
        public OrganizedMainWindow()
        {
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Error_Reporting.ErrorsReporting.Application_ThreadException);
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
            IniFileHandler.read();
            Init();
            initializebinding();
            convertCheckBoxesToButton();
           // Console.WriteLine(AutoPilotUnits.uncompileGPSPositionFromAutoPilotUnits( INIData.latitude));
            //Console.WriteLine(AutoPilotUnits.compileGPSPositionFromAutoPilotUnits(-664870010)) ;
            
            
        }

        void convertCheckBoxesToButton()
        {
            hoverC.Appearance = Appearance.Button;
            orbitRight.Appearance = Appearance.Button;
            orbitLeftC.Appearance = Appearance.Button;
            takePictureC.Appearance = Appearance.Button;
            DescC.Appearance = Appearance.Button;
            circleRightC.Appearance = Appearance.Button;
            circleLeftC.Appearance = Appearance.Button;
            FlyHomeC.Appearance = Appearance.Button;
            stopC.Appearance = Appearance.Button;
            landHomeC.Appearance = Appearance.Button;
            landHereC.Appearance = Appearance.Button;
            figure8C.Appearance = Appearance.Button;

            ArtifcialHorizonTab.Size = new Size(500, 140);
            for (int i = 0; i < flightToolStripMenuItem.DropDown.Items.Count; i++)
                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[i]).Checked = true;

            hoverC.BackColor = Color.SteelBlue;
            orbitRight.BackColor = Color.SteelBlue;
            orbitLeftC.BackColor = Color.SteelBlue;
            takePictureC.BackColor = Color.SteelBlue;
            DescC.BackColor = Color.SteelBlue;
            circleRightC.BackColor = Color.SteelBlue;
            circleLeftC.BackColor = Color.SteelBlue;
            FlyHomeC.BackColor = Color.SteelBlue;
            stopC.BackColor = Color.SteelBlue;
            landHomeC.BackColor = Color.SteelBlue;
            landHereC.BackColor = Color.SteelBlue;
            figure8C.BackColor = Color.SteelBlue;
        }

        public void initmMap()
        {
            /**/
            this.gmap = new GMap.NET.WindowsForms.GMapControl();

            this.gmap.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.gmap.Bearing = 0F;
            this.gmap.CanDragMap = false;
            this.gmap.EmptyTileColor = System.Drawing.Color.Navy;
            this.gmap.GrayScaleMode = false;
            this.gmap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gmap.LevelsKeepInMemmory = 5;
            this.gmap.Location = new System.Drawing.Point(12, 27);
            this.gmap.MarkersEnabled = true;
            this.gmap.MaxZoom = 18;
            this.gmap.MinZoom = 2;
            this.gmap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.ViewCenter;
            this.gmap.Name = "gmap";
            this.gmap.NegativeMode = false;
            this.gmap.PolygonsEnabled = true;
            this.gmap.RetryLoadTile = 0;
            this.gmap.RoutesEnabled = true;
            this.gmap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Fractional;
            this.gmap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gmap.ShowTileGridLines = false;
            this.gmap.Size = new System.Drawing.Size(767, 465);
            this.gmap.TabIndex = 16;
            this.gmap.Zoom = 10D;
            gmap.Tag = 2;

            ///
            this.gmap.OnMarkerClick += new MarkerClick(this.on_marker_click);
           this.gmap.OnMarkerEnter += new MarkerEnter(this.on_marker_enter);
             this.gmap.OnMarkerLeave += new MarkerLeave(this.on_marker_leave);
            this.gmap.MouseDown += new MouseEventHandler(MainMap_MouseDown);
            this.gmap.MouseUp += new MouseEventHandler(MainMap_MouseUp);
            trackBar1.ValueChanged += new System.EventHandler(TrackBar1_ValueChanged);
            this.gmap.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseWheel);
            ///
            this.gmap.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.map1_MouseDoubleClick_);
            if (!start)
            {

                foreach (Control item in this.Controls.OfType<Control>())
                {
                    if (item.Name.Equals("gmap"))
                        this.Controls.Remove(item);
                }
            }
            this.Controls.Add(this.gmap);

            /**/
            markersOverlay = new GMapOverlay("markers");
            gmap.Overlays.Add(markersOverlay);
            points = new List<PointLatLng>();
            route = new GMapRoute(points, "route");
            routesOverlay = new GMapOverlay("route");
            routesOverlay.Routes.Add(route);

            gmap.Overlays.Add(routesOverlay);
            gmap.MapProvider = GMapProviders.List[INIData.selectedMap]; 
                //GMap.NET.MapProviders.GoogleTerrainMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;
            gmap.Position = new PointLatLng(39.4666666597666, -76.1681666929581);
            gmap.Zoom = 14;
            wayPointList = new List<WayPoint>();
            commandsList = new List<Command>();
            start = false;
            gmap.UpdateRouteLocalPosition(route);
            trackBar1.Minimum = 14;
            trackBar1.Maximum = 30;
           // marker = new GMarkerGoogle(new PointLatLng(39.4666666597666, -76.1681666929581), GMarkerGoogleType.arrow);

        }

        public void Init()
        {
            ///

            initmMap();
            myFlight = new Flight();

            initializeFormsAssociated();
        }

        /// <summary>
        /// Initiailization of Menu tool Stip upon click or tool strip click
        /// </summary>

        private void initializeFormsAssociated() {
            myFailurePatternsToolstrip = new FailurePatternSimulation(myFlight.flyUAV);
            myFlyFileTransmisionForm = new TransmitFlyFile(myFlight.flyUAV);
            myVRSReceiveForm = new TransmitVRSFile(myFlight.flyUAV, true);
            myVRSTransmitForm = new UAV_GUI.Forms.Main_Screen.TransmitVRSFile(myFlight.flyUAV, false);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            gmap.MouseMove += new MouseEventHandler(MainMap_MouseMove);
           // Console.WriteLine(Math.Sin (30*3.14 / 180));

            //IniFileHandler.writeSpecificFieldsim("UAV", "model", "2028");
            //262581499 273434625 -2283
            /*
             * Reading Configuration Data. 
             * Data will be accessed through INIData Static class . 
             */

        }


        private void map1_MouseDoubleClick_(object sender, MouseEventArgs e)
        {

            PointLatLng p = gmap.FromLocalToLatLng(e.X, e.Y);
            if (MainMap_orlong == -1 || MainMap_orlat == -1)
            {
                if (MessageBox.Show("Can't Add WP Befor Setting the Origin Location \n Do You Want to Set it Here ? ", "Origin Setting", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MainMap_orlong = p.Lng;
                    MainMap_orlat = p.Lat;
                    gmap.Position = p;
                    MessageBox.Show("Orign Set Successfully ");

                }

            }
            else
            {
                WayPoint n = MapOptions.addWP(p, ref route, ref routeList,
                                        ref markersOverlay, ref markersList, ref gmap);
                CommandArg = new List<double>();
                CommandArg.Add(p.Lat);
                CommandArg.Add(p.Lng);
                //commandsList.Add(new Command(CommandType.basic , CommandName.flyTo, CommandArg) );
                Command comd = new Command(MasterClasses.CommandType.basic, MasterClasses.CommandName.flyTo, CommandArg);
                n.commandsList.Add(comd);
                wayPointList.Add(n);
            }

        }

        void MainMap_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && isMouseDown && Control.ModifierKeys != Keys.Alt)
            {
                PointLatLng point = gmap.FromLocalToLatLng(e.X, e.Y);
                if (selectedMarker != null && selectedMarkerFlag)

                    MapOptions.Drag(ref  route, ref  selectedMarker, point, ref  gmap);
            }
            if (e.Button == MouseButtons.Right && panFlag) {
                this.gmap.Position = gmap.FromLocalToLatLng(e.X, e.Y);
            }

        }

        private bool panFlag =false;
        void MainMap_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                panFlag = true;
            }
                isMouseDown = true;
        }
        private void panel1_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (updateMapStop.WaitOne(0) ) 
            this.gmap.Position = gmap.FromLocalToLatLng(e.X, e.Y);
        }

        void MainMap_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
            if (e.Button == MouseButtons.Right)
            {
                panFlag = true;
            }
        }

        private void TrackBar1_ValueChanged(object sender, System.EventArgs e)
        {
            MapOptions.mapZooming(ref sender, ref gmap);
        }



        void on_marker_click(GMapMarker sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && Control.ModifierKeys == Keys.Alt)
            {
                ScrewTurn.DropDownPanel WayPointPanel = null;
                MapOptions.MarkerSelection(ref  selectedMarkerFlag, ref  selectedMarker,
                                 ref  sender, ref  selectedMarkerDeleted,
                                ref  WayPointPanel, ref  markersOverlay, ref  markersList);
            }
        }

        private void on_marker_enter(GMapMarker sender)
        {
            //selectedMarker = (GMapMarker)sender;         
        }

        private void on_marker_leave(GMapMarker sender)
        {
            // selectedMarker = null;
        }

        private void seekGPSLock()
        {

            while (false)
            {
                try
                {
                    while (threadGPSSeekStop.WaitOne(0))
                    {

                    }
                    myFlight.flyData.GpsLock = myFlight.flyUAV.isGPSOK() == 1 ? true : false;
                    if (myFlight.flyData.GpsLock)
                    {
                       
                        this.gpsLock.Text = "Locked";
                        this.gpsLock.BackColor = Color.Green;
                        //return;
                    }
                    else
                    {
                        this.gpsLock.Text = "Not Locked";
                        this.gpsLock.BackColor = Color.Red;
                    }
                }
                catch (Exception e)
                {
                    GlobalData.LogConsole(e.Message + "at " + this.Name + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());
                    continue;
                }
            }

        }
        private void bindGauges()
        {

            while (true)
            {
                try
                {
                    while (gaugeFillingStop.WaitOne(0))
                    {

                    }

                    /*Display in MTN Data from Scratch fields */

                    NavLatdis.Text = "" + MTNState.latitude;
                    LongNavDis.Text = "" + MTNState.longitude;
                    AltNavDis.Text = "" + MTNState.Altitude;
                    GPSNAVStatus.Text = MTNState.Gps_State == 1 ? "GPS Signal Available" : "No GPS Signal Available";
                    packetsCountDisplay.Text = "" + MTNState.rightpackets + " / " + MTNState.Totalpackets + " / " +MTNState.RateTotalpackets ;
                    modeNavLabel.Text = MTNState.NaV_State == Navigation_Status.Alignment ? "Alignment " : "Navigation";
                    label3.Text = ""+myFlight.flyData.Latitude;
                    label4.Text = ""+myFlight.flyData.Longitude;
                    label5.Text = ""+myFlight.flyData.Altitude;

                    label15.Text = "" + myFlight.flyUAV.getJoy(1562); //MTNState.VelocityE;
                    label16.Text = "" + myFlight.flyUAV.getJoy(1563); //MTNState.VelocityN;
                    label17.Text = "" + myFlight.flyUAV.getJoy(1564);//MTNState.yaw;

                    label18.Text = "" + MTNState.pitch;
                    label19.Text = "" + MTNState.roll;
                    label20.Text = "" + MTNState.yaw;

                    label6.Text = "" + myFlight.flyData.Pitch;
                    label7.Text = "" + myFlight.flyData.Roll; ;
                    label11.Text = "" + myFlight.flyData.Yaw; ;
                    label24.Text = "" + myFlight.flyUAV.getJoy(1224);
                    /*Check If there are a fatal Errors Or Not */
                   
                    int fatalValue = myFlight.flyUAV.checkForFatalError();
                    if ((fatalValue < 2 || fatalValue > 110))
                    {
                        fatalStrip.BackColor = Color.Green;
                        fatalStrip.Text = "None";
                    }
                    else if (fatalValue == 72)
                    {
                        fatalStrip.BackColor = Color.Red;
                        fatalStrip.Text = "Multiple Fatal Errors ";
                    }
                    else { 
                        fatalStrip.BackColor = Color.Red;
                        fatalStrip.Text = fatalErrorsLookUPTable.fatalErrors.ElementAt(fatalValue - 2).Description;
                    } 
                    int status = myFlight.flyData.pStdTlmData.status;
                    int rcState = myFlight.flyUAV.getJoy(1156);
                    
                    Color failureColor = Color.Red;
                    failureStrip.Text = myFlight.flyUAV.getCurrentFailureAsString(ref failureColor);
                    failureStrip.BackColor = failureColor ; 
                    
                    if ((status >> 7 & 1) == 1)
                    {
                        readySL.Text = "Ready";
                        if (!GlobalData.autoPilotReady)
                        {
                            Location orLoca = myFlight.flyUAV.getOrigin() ;
                            GlobalData.orValEDeg = orLoca.Longitude;
                            GlobalData.orValNDeg = orLoca.Latitude;
                            GlobalData.orValNRad = (GlobalData.orValNDeg * Math.PI) / 180.0;
                            GlobalData.orValERad = (GlobalData.orValEDeg * Math.PI) / 180.0;
                            gmap.Position = new PointLatLng(orLoca.Latitude, orLoca.Longitude);
                            GlobalData.autoPilotReady = true;
                        }
                    }
                    else
                    {
                        readySL.Text = "Not Ready ";
                        GlobalData.autoPilotReady = false;
                    }
                    if (!((status >> 5 & 1) == 1) && !((status >> 4 & 1) == 1))
                    { 
                        radioButton3.Checked = true;
                        GlobalData.currentMode = FlightMode.UAV;
                    }
                    else if (!((status >> 5 & 1) == 1) && ((status >> 4 & 1) == 1))
                    {
                        radioButton2.Checked = true;
                        GlobalData.currentMode = FlightMode.RPV;
                    }
                    else if (((status >> 1 & 1) == 1))
                    {
                        
                        picRadButton.Checked = true;
                        GlobalData.currentMode = FlightMode.PIC;
                    }
                    if (GlobalData.modeSimulatorOrAuto == 1)
                    {
                       
                        myFlight.flyUAV.setJoy(1502, (int)0x40);
                     }
                    if (true) 
                    {
                        if (rcState == 0 )
                        {
                            rcStausL.Text = "RC- OFF";
                        }
                        else if (rcState == 1)
                        {
                            float ch5 =  myFlight.flyUAV.getJoy(1419) / 2000f;
                            if (ch5<1)
                            {
                                rcStausL.Text = "RC-OFF";
                            }
                            else if (ch5 > 1 && ch5 <1.5)
                            {
                                rcStausL.Text = "RC- ON";
                            }
                            else if (ch5 > 1.5 && ch5 < 2)
                            {
                                rcStausL.Text = "RC- PIC";
                            }
                            else
                            {
                                rcStausL.Text = "RC- OFF";
                            }
                        }
                     
                        else
                        {
                            rcStausL.Text = "RC- OFF";
                        }
                    }
                    else
                    {// not executed

                        if ((rcState & 0x01) == 0x01)
                        {
                            rcStausL.Text = "LRC-ON";
                        }
                        else if ((rcState & 0x02) == 0x02)
                        {
                            rcStausL.Text = "RCP-PIC";
                        }
                        else if ((rcState & 0x08) == 0x08)
                        {
                            rcStausL.Text = "LRC-PIC";
                        }
                        else if ((rcState & 0x40) == 0x40)
                        {
                            rcStausL.Text = "RC- ON";
                        }
                        else
                        {
                            rcStausL.Text = "" + rcState;
                        }
                    }
                  
                        
                    
                    /*Unbind Labelas*/
                    DTNWPTL.Text = "" + myFlight.flyData.DTNWP;
                    AltitudeLL.Text = "" + myFlight.flyData.Altitude;
                    velocityL.Text = "" + myFlight.flyData.AngularVelocity;
                    altitudeL.Text = "" + myFlight.flyData.Altitude;
                    HeadingL.Text = "" + myFlight.flyData.Heading;
                    headingStL.Text = "" + myFlight.flyData.Heading;
                    DistTONXPL.Text = "" + myFlight.flyData.DTNWP;
                    yawL.Text = "" + myFlight.flyData.Yaw;
                    airSpeedL.Text = "" + myFlight.flyData.AirSpeed;
                    attitudeIndicatorInstrumentControl2.SetAttitudeIndicatorParameters(myFlight.flyData.Pitch, myFlight.flyData.Roll);
                    airSpeedIndicatorInstrumentControl2.SetAirSpeedIndicatorParameters((int)myFlight.flyData.AirSpeed);
                    headingIndicatorInstrumentControl2.SetHeadingIndicatorParameters((int)myFlight.flyData.Heading);
                    altimeterInstrumentControl2.SetAlimeterParameters((int)myFlight.flyData.Altitude);

                    /*Progress Bars Values*/
                    thrL.Text = "Throttle (" + myFlight.flyData.ThrottleValue + " %)";
                    throttleProgressBar.Value = (int)myFlight.flyData.ThrottleValue;

                    /*Main Battery Voltage */
                    mbatL.Text = "Main Battery (" + myFlight.flyData.MBatValue + " %)";
                    mBatProgreesbar.Value = (int)myFlight.flyData.MBatValue;

                    /*Servo Battery Voltage */
                    sBatL.Text = "Servo Battery (" + myFlight.flyData.SBatValue + " %)";
                    sBatProgressBar.Value = (int)myFlight.flyData.SBatValue;


                }
                catch (Exception e)
                {
                    GlobalData.LogConsole(e.Message + "at " + this.Name + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());
                    continue;
                }
            }
        }

        private PointLatLng getSuitableMapPostion(PointLatLng mapPosition,PointLatLng markerPosition) {
            //MessageBox.Show("" + gmap.Location.X + " " + gmap.Location.Y + " " + gmap.Width + " " + gmap.Height + " ");
            //PointLatLng Border1 = gmap.FromLocalToLatLng((int)gmap.Location.X + (gmap.Width) - 4, (int)gmap.Location.Y + (gmap.Height ) - 3);
          //  MessageBox.Show(""+ gmap.PointToScreen(Point.Empty).Y);
            GPoint b1 = new GPoint((int)gmap.PointToScreen(Point.Empty).X - (gmap.Width), (int)gmap.PointToScreen(Point.Empty).Y + (gmap.Height));
            PointLatLng Border2 = gmap.FromLocalToLatLng((int)gmap.PointToScreen(Point.Empty).X - (gmap.Width), (int)gmap.PointToScreen(Point.Empty).Y + (gmap.Height));
            //PointLatLng Border3 = gmap.FromLocalToLatLng((int)gmap.Location.X - (gmap.Width) - 4, (int)gmap.Location.Y - (gmap.Height) - 3);
            PointLatLng Border4 = gmap.FromLocalToLatLng((int)gmap.PointToScreen(Point.Empty).X + (gmap.Width), (int)gmap.PointToScreen(Point.Empty).Y - (gmap.Height));
            GPoint b2 = new GPoint((int)gmap.PointToScreen(Point.Empty).X + (gmap.Width), (int)gmap.PointToScreen(Point.Empty).Y - (gmap.Height));
            GPoint my =gmap.FromLatLngToLocal(marker.Position);
            MessageBox.Show( ""+  my.X + " "+ my.Y + " " +b1.X + " " + b1.Y+" "+ b2.X+" "+b2.Y+ " " );
            GMarkerGoogle marker1 = new GMarkerGoogle(Border2, GMarkerGoogleType.orange);
            GMarkerGoogle marker2 = new GMarkerGoogle(Border4, GMarkerGoogleType.pink);
            
            GPoint newCenter = gmap.FromLatLngToLocal(mapPosition) ;
            /* MessageBox.Show( Border2.Lat + " " + Border2.Lng + " "
                  + Border4.Lat + " " + Border4.Lng + " ");*/
             markersOverlay.Markers.Add(marker1);
             markersOverlay.Markers.Add(marker2);
               
                //double BorderLat1 = gmap.FromLocalToLatLng ((int)gmap.PositionPixel.X+(Width/2),(int)gmap.PositionPixel.Y+(Height/2)).Lat ; 
            //double BorderLong =

            if (my.X < b1.X) {
                newCenter.X = b1.X;
            }
            else if (my.Y<b2.Y)
            {
                newCenter.Y = b2.Y;     
            }
            else if (my.X > b2.X)
            {
                newCenter.X = b2.X;
            }
            else if (my.Y > b1.Y)
            {
                newCenter.Y = b1.Y;
            }
            MessageBox.Show("new Center is " + newCenter.X + " " + newCenter.Y);
            return gmap.FromLocalToLatLng((int)newCenter.X,(int)newCenter.Y);
        }
        private void updateMap()
        {
            long count = 0;
            int pathC = -1;
            long i = 0;


            GMapOverlay pathOverlay;
            pathOverlay = new GMapOverlay("path");
            List<PointLatLng> pointsPath = new List<PointLatLng>();

            List<GMapRoute> path = new List<GMapRoute>();

            marker.ToolTipMode = MarkerTooltipMode.Always;
            gmap.Overlays.Add(pathOverlay);
            while (true)
            {
                try
                {
                    while (updateMapStop.WaitOne(0))
                    {

                    }
                    try
                    {
                        
                        if (!GlobalData.autoPilotReady) {
                            marker.Position = new PointLatLng(INIData.latitude, INIData.longitude);
                            continue; 
                        }
                        marker.Position = new PointLatLng(myFlight.flyData.Latitude, myFlight.flyData.Longitude);
                        //gmap.Position = getSuitableMapPostion(gmap.Position, marker.Position);
                        gmap.Position = marker.Position; 
                        marker.ToolTipText = ("(" + marker.Position.Lat + " , " + marker.Position.Lng + ")");


                        if (i % 500 == 0 && marker.Position.Lng != 0 && marker.Position.Lat != 0)
                        {
                            GMapRoute currentR = new GMapRoute(pointsPath, "path" + pathC);
                            currentR.Stroke.Color = Color.Black;
                            currentR.Stroke.Width = 1;
                            path.Add(currentR);
                            pathC++;
                            pathOverlay.Routes.Add(path[pathC]);


                            i = 1;
                        }
                        if (count % 5 == 0 && Math.Abs(marker.Position.Lng - 0) > 2 && Math.Abs(marker.Position.Lat - 0) > 2)
                        {
                            path[pathC].Points.Add(marker.Position);
                            gmap.UpdateRouteLocalPosition(path[pathC]);
                            count = 0;
                            i++;
                        }

                        count++;
                    }
                    catch (InvalidOperationException e)
                    {
                        GlobalData.LogConsole(e.Message + "at " + this.Name + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());
                        continue;
                    }
                }
                catch (Exception e)
                {
                    GlobalData.LogConsole(e.Message + "at " + this.Name + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());
                    continue;
                }

            }
        }

        private void bindLabels()
        {

            altitudeL.DataBindings.Add(new Binding("Text", myFlight.flyData, "Altitude"));
            yawL.DataBindings.Add(new Binding("Text", myFlight.flyData, "yaw"));
            HeadingL.DataBindings.Add(new Binding("Text", myFlight.flyData, "Heading"));
            DistTONXPL.DataBindings.Add(new Binding("Text", myFlight.flyData, "DTNWP"));
            airSpeedL.DataBindings.Add(new Binding("Text", myFlight.flyData, "AirSpeed"));

            /*Testing*/
           /* verticalSpeedL.DataBindings.Add(new Binding("Text", myFlight.flyData, "Altitude"));
            groundSpeedL.DataBindings.Add(new Binding("Text", myFlight.flyData, "Altitude"));*/

        }

        private void initializebinding()
        {
            // bindGauges();
            bindLabels();
        }


        void nearNextWP_Indecation()
        {
            // if (myFlight.flyData.DTNWP <= 0.1) // for example
            // {    
            // need to set currntMarker first 
            // Steps  

            #region 1- return the currnt WP to the orignal color
            PointLatLng position = currntMarker.Position;
            markersOverlay.Markers.Remove(currntMarker);
            markersList.Remove(currntMarker);
            GMarkerGoogle nMarker = new GMarkerGoogle(position, GMarkerGoogleType.blue_small);
            markersOverlay.Markers.Add(nMarker);
            markersList.Add(nMarker);
            currntMarker = nMarker;
            //route.Points.Add(position);
            //routeList.Add(route);
            //gmap.UpdateRouteLocalPosition(route);
            //route.Stroke.Color = Color.Green;
            #endregion

            #region 2- change the currnt WP to the next WP
            // markersList.Remove(currntMarker);
            //markersOverlay.Markers.Remove(currntMarker);
            currntMarker = wayPointList[Count].marker;
            #endregion

            #region 3- change color of the currnt WP
            position = currntMarker.Position;
            nMarker = new GMarkerGoogle(position, GMarkerGoogleType.pink);
            markersOverlay.Markers.Add(nMarker);
            markersList.Add(nMarker);
            currntMarker = nMarker;
            //route.Points.Add(position);
            //routeList.Add(route);
            //gmap.UpdateRouteLocalPosition(route);
            //route.Stroke.Color = Color.Pink;
            #endregion
            //}
        }



        private void toolStripButton10_Click(object sender, EventArgs e)
        {
            /*AGLGauge_W AGL = new AGLGauge_W();
            AGL.Show();*/
        }

        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            V_OSD_W window = new V_OSD_W();
            window.Show();
        }


        private void flightPlanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            n = new Form2();
            n.Text = " Untitled.fly - Flight Plan";
            n.FormClosed += new FormClosedEventHandler(f_FormClosed);
            n.Show();

        }

        void f_FormClosed(object sender, FormClosedEventArgs e)
        {

            n.checkForSaving();
        }
        private void flightControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FlightControl_W myControl = new FlightControl_W();
            myControl.ShowDialog();
        }


        private void cameraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /* CameraControl_W camControl = new CameraControl_W();
             camControl.ShowDialog();*/
        }

        private void artificialHorizonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[0]).Checked)
            {

                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[0]).Checked = false;

                ArtifcialHorizonTab.TabPages.Remove(AHdropDownPanel);
            }

            else
            {
                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[0]).Checked = true;
                ArtifcialHorizonTab.TabPages.Add(AHdropDownPanel);


            }

            //  Artifical_Horizon AH = new Artifical_Horizon();
            //  AH.Show();

        }
        [STAThread()]
        private void button14_Click(object sender, EventArgs e)
        {

            //Console.WriteLine("un Compiloe " + AutoPilotUnits.uncompileGPSPositionFromAutoPilotUnits(INIData.latitude));
            //Console.WriteLine(INIData.latitude + "Compiloe " +AutoPilotUnits.compileGPSPositionFromAutoPilotUnits( AutoPilotUnits.uncompileGPSPositionFromAutoPilotUnits(INIData.latitude)));
            if (MainMap_orlong == -1 || MainMap_orlat == -1)
            {
                MessageBox.Show("Can't Add WP Befor Setting the Origin Location \n  Please Click on the Map to Specify an Origin ", "Origin Setting");

            }
            else
            {
                /*  Clear Map Elements   */




                // this.openFileDialog1.Filter = "*.fly| *.wpt";
                DialogResult result = openFileDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    fileNme = openFileDialog1.FileName;
                }
                else
                {
                    return;
                }
                loadFlyFile(fileNme);
                this.flyFileLabel.Text = fileNme;
                this.gmap.Position = new PointLatLng(INIData.latitude, INIData.longitude);
                connectButton.Enabled = true;


            }
        }
        private void loadFlyFile(String fileNme)
        {
            MapOptions.clearMap(ref  markersList, ref  markersOverlay,
                             ref  route, ref  routeList,
                             ref  gmap, ref  selectedMarker,
                            ref  selectedMarkerFlag, ref wayPointList, ref routesOverlay);
            FlyFileParser f = new FlyFileParser();
            f.orlat = INIData.latitude;
            f.orlong = INIData.longitude;
            markersOverlay.Markers.Add(marker);
            wayPointList.AddRange(f.parseFlyFile(fileNme));
            bool fixed_ = false;
            for (int u = 0; u < wayPointList.Count; u++)
            {


                for (int m = 0; m < wayPointList[u].commandsList.Count; m++)
                {
                    if (wayPointList[u].commandsList[m].name == CommandName.fixed_)
                        fixed_ = true;
                }
                if (fixed_)
                    break;

                //GlobalData.LogConsole("1.txt" + u + "\n");
                PointLatLng p = new PointLatLng(wayPointList[u].lat, wayPointList[u].longt);
                GMarkerGoogle nMarker = new GMarkerGoogle(p, GMarkerGoogleType.blue_small);
                markersOverlay.Markers.Add(nMarker);
                markersList.Add(nMarker);
                route.Points.Add(p);
                routeList.Add(route);
                gmap.UpdateRouteLocalPosition(route);
                route.Stroke.Color = Color.Turquoise;
                route.Stroke.Width = 1;
                wayPointList[u].marker = nMarker;


            }
        }



        private void generalSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Setup setupForm = new Setup();
            setupForm.StartPosition = FormStartPosition.CenterParent;
            setupForm.ShowDialog();
        }

        private void microPilotSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // for first time load default vrs file 
            VrsFileHandler vrsHandler = new VrsFileHandler(UAV_GUI.Forms.MicroPilotSetup.fileName);
            UAV_GUI.Forms.MicroPilotSetup MicroPilotSetupForm = new UAV_GUI.Forms.MicroPilotSetup(ref vrsHandler, myFlight);
            MicroPilotSetupForm.StartPosition = FormStartPosition.CenterParent;
            MicroPilotSetupForm.ShowDialog();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            /*ScrewTurn.DropDownPanel WayPointPanel = null;
            MapOptions.deleteMarker(ref  selectedMarker, ref wayPointList
                                , ref  route, ref routeList
                                , ref  markersOverlay, ref markersList,
                                 ref  gmap, ref  selectedMarkerDeleted
                                 , ref  selectedMarkerFlag, ref  WayPointPanel);*/

            initmMap();
            if (!this.flyFileLabel.Text.Equals("No File Loaded")) 
                 loadFlyFile(fileNme);
            if (updateMapThread != null)
            {
                updateMapStop.Set();
                Thread.Sleep(10);
                updateMapThread.Abort();
                updateMapThread = null;
                Thread.Sleep(10);
                //updateMap
                updateMapThread = new Thread(new ThreadStart(updateMap));
                updateMapStop.Reset();
                updateMapThread.Start();
            }
        }

        private void button14_Click_1(object sender, EventArgs e)
        {
            MapOptions.clearMap(ref  markersList, ref  markersOverlay,
                              ref  route, ref  routeList,
                              ref  gmap, ref  selectedMarker,
                             ref  selectedMarkerFlag, ref wayPointList);
        }


        private void button15_Click(object sender, EventArgs e)
        {
            try
            {
                object result = Jacksonsoft.WaitWindow.Show(this.WorkerMethod);
                Thread.Sleep(10000);
                stopWaiting = true;
                //   throw new BusinessException(4,"The Fist Best Practice Error Message");

            }
            catch (ExternalException y)
            {
                MessageBox.Show("Tariq");

            }
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if (connectButton.Text.Equals("       Connect"))
            {
                bool ret = false;

                //ret = myFlight.flyUAV.connect(fileNme, GlobalData.modeSimulatorOrAuto);
                loadFlyFile(fileNme);
                myFlight = new Flight();
                ret = (bool)Jacksonsoft.WaitWindow.Show(this.WorkerMethod, null, "5");
                if (ret)
                {

                    startAllThreads();
                    this.arm_takeOff.Visible = true;
                    this.arm_takeOff.Enabled = true;
                    connectButton.Text = "       Disconnect";
                    this.isSimulator.Enabled = false;


                }
                else
                {
                    MessageBox.Show("Plz check configuration and com port", "COM Port error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            else
            {
                if (myFlight.flyUAV.disConnect())
                {



                    connectButton.Text = "       Connect";
                    this.isSimulator.Enabled = false;

                    stopAllThreads();
                    isSimulator.Enabled = true;
                    this.arm_takeOff.Visible = false;
                    this.arm_takeOff.Text = "Arm";

                }

            }
        }

        private void isSimulatorChech_Changed(object sender, EventArgs e)
        {
            CheckBox sender_ = sender as CheckBox;
            if (sender_.Checked)
            {
                GlobalData.modeSimulatorOrAuto = 1;
                GlobalData.LogConsole("checked");
            }
            else
            {
                GlobalData.modeSimulatorOrAuto = 0;
                GlobalData.LogConsole("Not checked");
            }
            GlobalData.LogConsole(GlobalData.modeSimulatorOrAuto + "");

        }

        private void arm_takeOff_Click(object sender, EventArgs e)
        {


            bool ret = false;
            Button sen = (sender as Button);
            if (sen.Text.Equals("Arm"))
            {
                
                
                if (!GlobalData.autoPilotReady && GlobalData.modeSimulatorOrAuto==0)
                {
                    throw new BusinessException(1010, "We cannot fly while autopilot is not ready");

                }

                int valueA = myFlight.flyUAV.checkForFatalError();
                if (valueA >= 2 && valueA <= 110)
                {
                //    throw new BusinessException(1011, "We cannot fly while there were a fatal Error");
                }

                if (!myFlight.flyUAV.verifyBetweenAirCruiseSpeed())
                {
                    throw new BusinessException(1012, "We cannot fly while the Wind Speed exceeds 50 % of the Curise Speed");
                }
                suspendAllThreads();
                ret = (bool)Jacksonsoft.WaitWindow.Show(this.WorkerMethod, null, "6");//Arm_initialize in background

                if (ret)
                {

                    
                    sen.Text = "Take Off";
                }
                resumeAllThreads();
            }
            else if (sen.Text.Equals("Take Off"))
            {
                
                suspendAllThreads();
                ret = (bool)Jacksonsoft.WaitWindow.Show(this.WorkerMethod, null, "7");

                if (ret)
                {

                    sen.Text = "Terminate";
                }

                resumeAllThreads();
            }
            else if (sen.Text.Equals("Terminate"))
            {
                /*Need to add terminte fly API*/
                stopAllThreads();
                sen.Text = "Arm";
                sen.Visible = false;
                connectButton.Text = "       Connect";
                connectButton.Enabled = true;
                isSimulator.Enabled = true;
                isSimulator.Checked = false;


            }




        }

        private void gpsLock_Click(object sender, EventArgs e)
        {
            try
            {
                if (!myFlight.flyUAV.fakeTheGPSLock())
                {
                    throw new BusinessException(1013, "Can not fake the GPS");
                }

                threadGPSSeekStop.Set();
                //readBiasSetupStartThread.Abort();
            }
            catch (Exception exception)
            {
                GlobalData.LogConsole(exception.Message);
                throw new BusinessException(1014, "Can not fake the GPS");
                
            }
            if (seekingGPSLock != null)
            {
                seekingGPSLock.Abort();
                seekingGPSLock = null;
            }
            this.gpsLock.Text = "GPS Faked";
            this.gpsLock.BackColor = Color.Yellow;

        }


        private void flightMonitorToolStripMenuItem_Click(object sender, EventArgs e)
        {



            GlobalData.myLogger.Show();
        }



        private void joystickControllerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 ss = new Form1();
            ss.StartPosition = FormStartPosition.CenterParent;
            ss.Show();
        }

        private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabControl tabContralObj = sender as TabControl;



            if (tabContralObj.SelectedIndex == ArtifcialHorizonTab.TabPages.IndexOf(AutoPilotContralTab))
            {
                tabContralObj.Size = new Size(500, 140);
            }
            else if (tabContralObj.SelectedIndex == ArtifcialHorizonTab.TabPages.IndexOf(AHdropDownPanel))
            {
                tabContralObj.Size = new Size(527, 679);
            }
            else
            {
                tabContralObj.Size = new Size(500, 200);
            }
        }




        private void groundSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {

            UAV_GUI.Forms.Setup.MP.GroundSetup GroundSetupForm = new UAV_GUI.Forms.Setup.MP.GroundSetup();
            GroundSetupForm.StartPosition = FormStartPosition.CenterParent;
            GroundSetupForm.ShowDialog();

        }

        private void loadFlyFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MainMap_orlong == -1 || MainMap_orlat == -1)
            {
                MessageBox.Show("Can't Add WP Befor Setting the Origin Location \n  Please Click on the Map to Specify an Origin ", "Origin Setting");

            }
            else
            {
                /*  Clear Map Elements   */




                // this.openFileDialog1.Filter = "*.fly| *.wpt";
                DialogResult result = openFileDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    fileNme = openFileDialog1.FileName;
                }
                else
                {
                    return;
                }
                loadFlyFile(fileNme);
                connectButton.Enabled = true;


            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }




        private void statusPanelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[1]).Checked)
            {

                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[1]).Checked = false;

                NavBoardStatus.TabPages.Remove(StatusTabPage);
            }

            else
            {
                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[1]).Checked = true;
                NavBoardStatus.TabPages.Add(StatusTabPage);

            }
        }


        private void autopilotControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[2]).Checked)
            {

                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[2]).Checked = false;

                ArtifcialHorizonTab.TabPages.Remove(AutoPilotContralTab);
            }

            else
            {
                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[2]).Checked = true;
                ArtifcialHorizonTab.TabPages.Add(AutoPilotContralTab);

            }
        }

        private void patternsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[3]).Checked)
            {

                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[3]).Checked = false;

                ArtifcialHorizonTab.TabPages.Remove(PatternDropDownPanel);
            }

            else
            {
                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[3]).Checked = true;
                ArtifcialHorizonTab.TabPages.Add(PatternDropDownPanel);

            }
        }

        private void graphDisplayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[4]).Checked)
            {

                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[4]).Checked = false;

                NavBoardStatus.TabPages.Remove(graphTabPage);
            }

            else
            {
                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[4]).Checked = true;
                NavBoardStatus.TabPages.Add(graphTabPage);

            }
        }

        private void flightControlToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[5]).Checked)
            {

                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[5]).Checked = false;

                NavBoardStatus.TabPages.Remove(graphTabPage);
            }

            else
            {
                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[5]).Checked = true;
                NavBoardStatus.TabPages.Add(graphTabPage);

            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UAV_GUI.Forms.Main_Screen.AboutWindow aboutWindow = new UAV_GUI.Forms.Main_Screen.AboutWindow();
            aboutWindow.StartPosition = FormStartPosition.CenterParent;
            aboutWindow.ShowDialog();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            Form1 ss = new Form1();
            ss.StartPosition = FormStartPosition.CenterParent;
            ss.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            UAV_GUI.Forms.Setup.MP.GroundSetup GroundSetupForm = new UAV_GUI.Forms.Setup.MP.GroundSetup();
            GroundSetupForm.StartPosition = FormStartPosition.CenterParent;
            GroundSetupForm.ShowDialog();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            try
            {
                GlobalData.myLogger.Show();
            }
            catch (Exception ec)
            {
                throw ec;
                //GlobalData.LogConsole(ec.Message+ "at "+ this.Name + " "+ new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber() );
                GlobalData.myLogger = new Logger();
                GlobalData.myLogger.Show();
            }
        }



        private void updateGraph()
        {
            int numGraphs = 0;
            if (INIData.AltEnabled) numGraphs++;
            if (INIData.HeadingEnabled) numGraphs++;
            if (INIData.ASpeedEnabled) numGraphs++;
            if (numGraphs == 0) return;
            List<ZedGraph.PointPairList> myList = new List<ZedGraph.PointPairList>();

            String[] tags = { "Altitude", "Heading", "Air Speed" };
            myGraph = new GraphOptions(graphDisplay, myList, numGraphs, tags.ToList());
            myGraph.initialize();
            while (true)
            {
                try
                {
                    while (visualGraphingStop.WaitOne(0))
                    {

                    }
                    GlobalData.time++;
                    if (INIData.AltEnabled)
                    {
                        ZedGraph.PointPair nP = new ZedGraph.PointPair(GlobalData.time, myFlight.flyUAV.getAltitude());
                        myGraph.addPointToCurve(0, nP, INIData.aLColor, "Altitude");
                    }
                    if (INIData.HeadingEnabled)
                    {
                        ZedGraph.PointPair nP = new ZedGraph.PointPair(GlobalData.time, myFlight.flyUAV.getHeading());
                        myGraph.addPointToCurve(0, nP, INIData.hColor, "Heading");
                    }
                    if (INIData.ASpeedEnabled)
                    {
                        ZedGraph.PointPair nP = new ZedGraph.PointPair(GlobalData.time, myFlight.flyUAV.getAirSpeed());
                        myGraph.addPointToCurve(0, nP, INIData.aSColor, "Air Speed");
                    }
                    Thread.Sleep(250);

                }
                catch (Exception e)
                {
                    GlobalData.LogConsole(e.Message + "at " + this.Name + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());
                    continue;
                }
            }

        }

        private void checkForLink()
        {
            while (true)
            {
                // this line of code check link connection 
                if (myFlight.flyUAV.checkLink())
                {
                    GlobalData.apConnected = true;
                    linkSPanel.Text = "Connected";
                    linkSPanel.BackColor = Color.Green;
                }
                else
                {
                    GlobalData.apConnected = false;
                    linkSPanel.Text = "Disconnected";
                    linkSPanel.BackColor = Color.Red;
                }
                Thread.Sleep(1000);
            }
        }

        #region Thread Manipuation
        /// <summary>
        /// Manipulated all availabe function to handle threads .
        /// </summary>

        private void startAllThreads()
        {

            Jacksonsoft.WaitWindow.Show(this.WorkerMethod, null, "1");
        }
        private void startAllThreadsBackGround()
        {
            //Gauges Binding
            gaugeFillingThread = new Thread(new ThreadStart(bindGauges));
            gaugeFillingStop.Reset();
            gaugeFillingThread.Start();

            //updateMap
           updateMapThread = new Thread(new ThreadStart(updateMap));
            updateMapStop.Reset();
            updateMapThread.Start();

            //GPS Lock 
            seekingGPSLock = new Thread(new ThreadStart(seekGPSLock));
            threadGPSSeekStop.Reset();
            seekingGPSLock.Start();

            SeekingTimer.Enabled = true;

            //Visual Monitoring 
            VisualGraphing = new Thread(new ThreadStart(updateGraph));
            visualGraphingStop.Reset();
            VisualGraphing.Start();

            //Link Check 

            checkForLinkThread = new Thread(new ThreadStart(checkForLink));
            checkForLinkStop.Reset();
            checkForLinkThread.Start();
            //Flight Data . 
            myFlight.Start();
        }
        public void suspendAllThreads()
        {
            Jacksonsoft.WaitWindow.Show(this.WorkerMethod, null, "3");
        }
        public void suspendAllThreadsBackGround()
        {
            gaugeFillingStop.Set();
            updateMapStop.Set();
            threadGPSSeekStop.Set();
            visualGraphingStop.Set();
            checkForLinkStop.Set();
            myFlight.Suspend();
            Thread.Sleep(10);
            SeekingTimer.Enabled = false;
        }
        public void resumeAllThreads()
        {
            Jacksonsoft.WaitWindow.Show(this.WorkerMethod, null, "4");
        }
        public void resumeAllThreadsBackGround()
        {
            gaugeFillingStop.Reset();
            updateMapStop.Reset();
            threadGPSSeekStop.Reset();
            visualGraphingStop.Reset();
            checkForLinkStop.Reset();
            myFlight.Resume();
            SeekingTimer.Enabled = true;
        }
        public void stopAllThreads()
        {
            Jacksonsoft.WaitWindow.Show(this.WorkerMethod, null, "2");
        }
        public void stopAllThreadsBackGround()
        {
            SeekingTimer.Enabled = false;
            //Gauges Binding
            if (gaugeFillingThread != null)
            {
                gaugeFillingStop.Set();
                Thread.Sleep(10);
                gaugeFillingThread.Abort();
                gaugeFillingThread = null;
            }

            //updateMap
            if (updateMapThread != null)
            {
                updateMapStop.Set();
                Thread.Sleep(10);
                updateMapThread.Abort();
                updateMapThread = null;
            }

            //GPS Lock 
            if (seekingGPSLock != null)
            {
                threadGPSSeekStop.Set();
                Thread.Sleep(10);
                seekingGPSLock.Abort();
                seekingGPSLock = null;
            }

            //Visual Monitoring 
            if (VisualGraphing != null)
            {
                visualGraphingStop.Set();
                Thread.Sleep(10);
                VisualGraphing.Abort();
                VisualGraphing = null;
            }

            //check for link 
            if (checkForLinkThread != null)
            {
                checkForLinkStop.Set();
                Thread.Sleep(10);
                checkForLinkThread.Abort();
                checkForLinkThread = null;
            }
            //Flight Data .
            myFlight.Stop();
        }

        #endregion

        /*To be reserved whatever
         Tariq Shatat
         * Add the ability to pause threads
         */
        private void WorkerMethod(object sender, Jacksonsoft.WaitWindowEventArgs e)
        {
            if (e.Arguments.Count > 0)
            {
                if (e.Arguments[0].Equals("1"))
                {
                    startAllThreadsBackGround();
                }
                else if (e.Arguments[0].Equals("2"))
                {
                    stopAllThreadsBackGround();
                }
                else if (e.Arguments[0].Equals("3"))
                {
                    suspendAllThreadsBackGround();
                }
                else if (e.Arguments[0].Equals("4"))
                {
                    resumeAllThreadsBackGround();
                }
                else if (e.Arguments[0].Equals("5"))
                {

                    e.Result = myFlight.flyUAV.connect(fileNme, GlobalData.modeSimulatorOrAuto);
                    gmap.Position = new PointLatLng(INIData.latitude, INIData.longitude);
                }
                else if (e.Arguments[0].Equals("6"))
                {

                    e.Result = myFlight.flyUAV.Arm_Initialize(GlobalData.modeSimulatorOrAuto);
                }
                else if (e.Arguments[0].Equals("7"))
                {
                    e.Result = myFlight.flyUAV.takeOff(GlobalData.modeSimulatorOrAuto); Thread.Sleep(250);
                    Location ee = myFlight.flyUAV.getOrigin();
                    gmap.Position = new PointLatLng(ee.Latitude, ee.Longitude);
                }
               

            }
        }

        protected ManualResetEvent threadGPSSeekStop = new ManualResetEvent(false);
        protected ManualResetEvent gaugeFillingStop = new ManualResetEvent(false);
        protected ManualResetEvent updateMapStop = new ManualResetEvent(false);
        protected ManualResetEvent visualGraphingStop = new ManualResetEvent(false);
        protected ManualResetEvent checkForLinkStop = new ManualResetEvent(false);
        private bool stopWaiting = false;

        private void patternCheckBox1_CheckedChanged_1(object sender, EventArgs e)
        {



            CheckBox ckBox = sender as CheckBox;

            if (ckBox.Checked)
            {
                ckBox.BackColor = Color.DarkBlue;
            }
            else
            {
                ckBox.BackColor = Color.SteelBlue;
            }

            if (!ckBox.Checked)
            {
                INIData.latitude = INIData.spare_latitude;
                INIData.longitude = INIData.spare_longitude;
                myFlight.flyUAV.startPattern(-1);
                return;

            }
            bool returnV = myFlight.flyUAV.setFlightMode(FlightMode.UAV);
            if (!returnV)
            {
                throw new BusinessException(1015, "Cannot Switch The Current Mode ");

            }
            foreach (Control s in ArtifcialHorizonTab.TabPages[2].Controls)
            {
                if (s.Name.Equals("tableLayoutPanel3"))
                {
                    foreach (Control ss in s.Controls)
                    {
                        if (!ss.Equals(ckBox) && ckBox.Checked)
                        {
                            CheckBox sss = ss as CheckBox;
                            sss.Checked = false;
                        }
                    }
                }

            }
            INIData.spare_latitude = INIData.latitude;
            INIData.spare_longitude = INIData.longitude;
            INIData.latitude = myFlight.flyData.Latitude;
            INIData.longitude = myFlight.flyData.Longitude;
            if (ckBox.Equals(this.hoverC))
            {
                myFlight.flyUAV.startPattern(4);
            }
            else if (ckBox.Equals(this.orbitRight))
            {
                myFlight.flyUAV.startPattern(0);
            }

            else if (ckBox.Equals(this.orbitLeftC))
            {
                myFlight.flyUAV.startPattern(1);
            }
            else if (ckBox.Equals(this.takePictureC))
            {
                myFlight.flyUAV.startPattern(3);
            }
            else if (ckBox.Equals(this.DescC))
            {
                myFlight.flyUAV.startPattern(5);
            }
            else if (ckBox.Equals(this.circleRightC))
            {
                myFlight.flyUAV.startPattern(6);
            }
            else if (ckBox.Equals(this.circleLeftC))
            {
                myFlight.flyUAV.startPattern(7);
            }
            else if (ckBox.Equals(this.FlyHomeC))
            {
                myFlight.flyUAV.startPattern(8);
            }
            else if (ckBox.Equals(this.stopC))
            {
                myFlight.flyUAV.startPattern(9);
            }
            else if (ckBox.Equals(this.landHomeC))
            {
                myFlight.flyUAV.startPattern(10);
            }
            else if (ckBox.Equals(this.landHereC))
            {
                myFlight.flyUAV.startPattern(11);
            }
            else if (ckBox.Equals(this.figure8C))
            {
                myFlight.flyUAV.startPattern(2);
            }
        }


        private void radioButton1_Click(object sender, EventArgs e)
        {
            bool returnV = false;
            if (GlobalData.modeSimulatorOrAuto == 0)
            {
                if (GlobalData.currentMode == FlightMode.RPV)
                {//UAV Mode. 
                    returnV = myFlight.flyUAV.setFlightMode(FlightMode.PIC);

                }
                else if (GlobalData.currentMode == FlightMode.UAV)
                {//RPV Mode. 
                    returnV = myFlight.flyUAV.setFlightMode(FlightMode.RPV);
                }
                else if (GlobalData.currentMode == FlightMode.PIC)
                {//RPV Mode. 
                    returnV = myFlight.flyUAV.setFlightMode(FlightMode.UAV);
                }
                else
                {
                    throw new BusinessException(1002, "Please Make Sure that you chosen a proper mode ");

                }
            }
            else {
                if (GlobalData.currentMode == FlightMode.RPV)
                {//UAV Mode. 
                    returnV = myFlight.flyUAV.setFlightMode(FlightMode.UAV);

                }
                else if (GlobalData.currentMode == FlightMode.UAV)
                {//RPV Mode. 
                    returnV = myFlight.flyUAV.setFlightMode(FlightMode.RPV);
                }
               
                else
                {
                    throw new BusinessException(1016, "Please Make Sure that you chosen a proper mode ");

                }
            }
            if (!returnV)
            {
                throw new BusinessException(1017, "Cannot Switch The Current Mode ");

            }
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            stopAllThreads();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            myFlyFileTransmisionForm.ShowDialog();

        }

        private void transmitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myVRSTransmitForm.ShowDialog();
        }

        private void acquireToolStripMenuItem_Click(object sender, EventArgs e)
        {
             myVRSReceiveForm.ShowDialog();
        }

        private void flyFileUtilityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myFlyFileTransmisionForm.ShowDialog();
        }

        private void vrsAcquire_Click(object sender, EventArgs e)
        {
            myVRSReceiveForm.ShowDialog();
        }

        private void transmitVRS_Click(object sender, EventArgs e)
        {
            myVRSTransmitForm.ShowDialog();
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            myFlyFileTransmisionForm.ShowDialog();
        }

        
        /*Not Used In The current version May Be Used Later. */
        /* private void rcStausL_Click(object sender, EventArgs e)
        {
            int ret = myFlight.flyUAV.setJoy(1502,(int) 0x0);
            if (ret != 0) {
                throw new BusinessException(1520, "Cann't Change the RC Status");
            }
            rcChangedToOFF = true;
             ret = myFlight.flyUAV.startFailurePattern(5);
           myFlight.flyUAV.setJoy(1262, 5);
            if (ret != 0)
            {
                throw new BusinessException(1520, "Error Simulating the RC lost failure patern");
            }
        }*/

       

        private void FlightPattern_Click(object sender, EventArgs e)
        {
            myFailurePatternsToolstrip.StartPosition = FormStartPosition.CenterScreen;
            myFailurePatternsToolstrip.Show();
        }

        
        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {

        }

        private void fatalStrip_Click(object sender, EventArgs e)
        {
            GlobalData.myLogger.Show();
        }

       

        private void TestServo(object sender, EventArgs e)
        {
            ServoTest servotest = new ServoTest(myFlight.flyUAV);
            servotest.ShowDialog();
          
        }

        private void fileflyLabel_Click(object sender, EventArgs e)
        {

        }

        private void flyFileLabel_Click(object sender, EventArgs e)
        {

        }

        private void toolsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void vRSFileUtilityToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void setVariableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VrsFileHandler vrsHandler = new VrsFileHandler(UAV_GUI.Forms.MicroPilotSetup.fileName);
            setvariable set_variable = new setvariable(myFlight.flyUAV);
            set_variable.ShowDialog();

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void automaticInitializationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UAV_GUI.Forms.AutoConnectWizard myConnectionWiz = new Forms.AutoConnectWizard();
            myConnectionWiz.ShowDialog();
        }

        private void rCSensorMonitorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UAV_GUI.RC_Sensors_Monitor mon_rc = new RC_Sensors_Monitor(myFlight.flyUAV);
            mon_rc.Show();
        }

        private void updatemapTimer_Tick(object sender, EventArgs e)
        {

        }

        private void updateGaugeTimer_Tick(object sender, EventArgs e)
        {

        }

        private void SeekingTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                
                myFlight.flyData.GpsLock = myFlight.flyUAV.isGPSOK() == 1 ? true : false;
                if (myFlight.flyData.GpsLock)
                {

                    this.gpsLock.Text = "Locked";
                    this.gpsLock.BackColor = Color.Green;
                    //return;
                }
                else
                {
                    this.gpsLock.Text = "Not Locked";
                    this.gpsLock.BackColor = Color.Red;
                }
            }
            catch (Exception k)
            {
                GlobalData.LogConsole(k.Message + "at " + this.Name + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());
                
            }
        }

        private void changeLabelsTimer_Tick(object sender, EventArgs e)
        {

        }

        private void visualGraphingTimer_Tick(object sender, EventArgs e)
        {

        }

        private void navBoardTimer_Tick(object sender, EventArgs e)
        {

        }

        
        
       

        

       



    }
}
