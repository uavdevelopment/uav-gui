﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MP_Lib;
namespace UAV_GUI.Forms.Main_Screen
{
    public partial class AGLForm : Form
    {
        public UAV myUAV;

        public AGLForm()
        {
            InitializeComponent();
        }

        public AGLForm(UAV myUav_)
        {
            myUAV = myUav_;
            InitializeComponent();
            timer1.Start();
            ApplicationLookAndFeel.UseTheme(this);
        }

        private void AGL_Load(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int aglv= myUAV.getAglHeight();
            valueL.Text = "" + aglv;
            aquaGauge1.Value = aglv;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
