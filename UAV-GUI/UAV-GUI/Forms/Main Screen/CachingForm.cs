﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET;
using System.IO;
using System.Text.RegularExpressions;
using UAV_GUI.FileClasses;
using UAV_GUI.Forms;
using MasterClasses;
using GMap.NET.MapProviders;
using Error_Reporting;
using UAV_GUI.FileClasses.FlyFiles;
using UAV_GUI.FlightClasses;
using FileClasses.INI_Files;

namespace UAV_GUI.Forms.Main_Screen
{
    public partial class CachingForm : Form
    {
        public CachingForm()
        {
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
        }

        private void CachingForm_Load(object sender, EventArgs e)
        {
            mapProviderComboBox.DataSource = GMapProviders.List.ToArray();
            gmap.MapProvider = GMapProviders.List[INIData.selectedMap];
            mapProviderComboBox.SelectedItem = gmap.MapProvider;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;
            gmap.Position = new PointLatLng(39.4666666597666, -76.1681666929581);
            gmap.Zoom = 14;
            mapProviderComboBox.SelectedItem = gmap.MapProvider;
        }

        private void mapProviderComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            gmap.MapProvider = (GMapProvider)mapProviderComboBox.SelectedItem;
        }

        private void Import_Click(object sender, EventArgs e)
        {
            gmap.ShowImportDialog();
        }

        private void Export_Click(object sender, EventArgs e)
        {
            gmap.ShowExportDialog();
        }

        private void prefetch_Click(object sender, EventArgs e)
        {
            RectLatLng area = gmap.SelectedArea;
            if (!area.IsEmpty)
            {
                for (int i = (int)gmap.Zoom; i <= gmap.MaxZoom; i++)
                {
                    DialogResult res = MessageBox.Show("Ready ripp at Zoom = " + i + " ?", "GMap.NET", MessageBoxButtons.YesNoCancel);

                    if (res == DialogResult.Yes)
                    {
                        using (TilePrefetcher obj = new TilePrefetcher())
                        {
                            //obj.Overlay = objects; // set overlay if you want to see cache progress on the map

                            obj.Shuffle = gmap.Manager.Mode != AccessMode.CacheOnly;
                            obj.Owner = this;
                            obj.ShowCompleteMessage = true;
                            obj.Start(area, i, gmap.MapProvider, gmap.Manager.Mode == AccessMode.CacheOnly ? 0 : 100, gmap.Manager.Mode == AccessMode.CacheOnly ? 0 : 1);
                        }
                    }
                    else if (res == DialogResult.No)
                    {
                        continue;
                    }
                    else if (res == DialogResult.Cancel)
                    {
                        break;
                    }
                }
            }
            else
            {
                MessageBox.Show("Select map area holding ALT", "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void clear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You sure?", "Clear GMap.NET cache?", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                try
                {
                    gmap.Manager.PrimaryCache.DeleteOlderThan(DateTime.Now, null);
                    MessageBox.Show("Done. Cache is clear.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void cacheLocation_Click(object sender, EventArgs e)
        {
            try
            {
                string argument = "/select, \"" + gmap.CacheLocation + "TileDBv5\"";
                System.Diagnostics.Process.Start("explorer.exe", argument);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to open: " + ex.Message, "GMap.NET", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
