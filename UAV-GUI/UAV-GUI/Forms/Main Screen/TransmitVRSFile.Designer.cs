﻿namespace UAV_GUI.Forms.Main_Screen
{
    partial class TransmitVRSFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.saveCancelButton = new System.Windows.Forms.Button();
            this.loadCurrent = new System.Windows.Forms.CheckBox();
            this.savToFlash = new System.Windows.Forms.CheckBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.receiveButton = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.simulatorL = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(3, 33);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(222, 23);
            this.progressBar1.TabIndex = 0;
            // 
            // saveCancelButton
            // 
            this.saveCancelButton.Location = new System.Drawing.Point(313, 74);
            this.saveCancelButton.Name = "saveCancelButton";
            this.saveCancelButton.Size = new System.Drawing.Size(75, 23);
            this.saveCancelButton.TabIndex = 1;
            this.saveCancelButton.Text = "Save";
            this.saveCancelButton.UseVisualStyleBackColor = true;
            this.saveCancelButton.Click += new System.EventHandler(this.saveCancelButton_Click);
            // 
            // loadCurrent
            // 
            this.loadCurrent.AutoSize = true;
            this.loadCurrent.Location = new System.Drawing.Point(277, 8);
            this.loadCurrent.Name = "loadCurrent";
            this.loadCurrent.Size = new System.Drawing.Size(152, 17);
            this.loadCurrent.TabIndex = 2;
            this.loadCurrent.Text = "Load Current Configuration";
            this.loadCurrent.UseVisualStyleBackColor = true;
            // 
            // savToFlash
            // 
            this.savToFlash.AutoSize = true;
            this.savToFlash.Location = new System.Drawing.Point(277, 33);
            this.savToFlash.Name = "savToFlash";
            this.savToFlash.Size = new System.Drawing.Size(95, 17);
            this.savToFlash.TabIndex = 3;
            this.savToFlash.Text = "Save To Flash";
            this.savToFlash.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.simulatorL);
            this.panel1.Controls.Add(this.receiveButton);
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.savToFlash);
            this.panel1.Controls.Add(this.saveCancelButton);
            this.panel1.Controls.Add(this.loadCurrent);
            this.panel1.Location = new System.Drawing.Point(4, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(431, 100);
            this.panel1.TabIndex = 4;
            // 
            // receiveButton
            // 
            this.receiveButton.Location = new System.Drawing.Point(313, 31);
            this.receiveButton.Name = "receiveButton";
            this.receiveButton.Size = new System.Drawing.Size(75, 23);
            this.receiveButton.TabIndex = 1;
            this.receiveButton.Text = "Receive";
            this.receiveButton.UseVisualStyleBackColor = true;
            this.receiveButton.Click += new System.EventHandler(this.receiveButton_Click);
            // 
            // simulatorL
            // 
            this.simulatorL.AutoSize = true;
            this.simulatorL.Location = new System.Drawing.Point(277, 56);
            this.simulatorL.Name = "simulatorL";
            this.simulatorL.Size = new System.Drawing.Size(93, 17);
            this.simulatorL.TabIndex = 4;
            this.simulatorL.Text = "Simulator Only";
            this.simulatorL.UseVisualStyleBackColor = true;
            // 
            // TransmitVRSFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 124);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "TransmitVRSFile";
            this.Text = "VRS File Utility";
            this.Load += new System.EventHandler(this.TransmitVRSFile_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button saveCancelButton;
        private System.Windows.Forms.CheckBox loadCurrent;
        private System.Windows.Forms.CheckBox savToFlash;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button receiveButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.CheckBox simulatorL;
    }
}