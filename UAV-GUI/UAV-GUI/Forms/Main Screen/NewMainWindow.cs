﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using System.IO.Ports;
using  System.Runtime.InteropServices;
using System.Threading;
using UAV_GUI.FileClasses;
using MasterClasses;
using Error_Reporting;
using MP_Lib;
using JoystickPlugin;
using FileClasses.VRS_Files;
using FileClasses.INI_Files;
using UAV_GUI.FileClasses.FlyFiles;
using UAV_GUI.FlightClasses;
using UAV_GUI.Forms.Setup.MP;
using UAV_GUI.Forms;
using UAV_GUI.Forms.Main_Screen;
using GMap.NET.MapProviders;
using UAV_GUI.Utilities;
using FlightMonitor;
using System.Diagnostics;
using UAV_GUI.Forms.Set_Variable;
using GLobal;
using UAV_GUI.Forms.Setup;

namespace UAV_GUI
{
    public partial class NewMainWindow : Form
    {

        public NewMainWindow()
        {
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Error_Reporting.ErrorsReporting.Application_ThreadException);
            /*Dummy Delay for Spplash screen*/
            Thread.Sleep(1250);
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
            IniFileHandler.read();
            Init();
            initializebinding();
            convertCheckBoxesToButton();
         }

        

        public void Init()
        {
            ///
            initContextMenu();
            initmMap();
            initSplitPanel();
            myFlight = new Flight();
            initializeFormsAssociated();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            gmap.MouseClick += new MouseEventHandler(MainMap_MouseMove);
            gmap.MouseMove += new MouseEventHandler(MainMap_MouseMove1);
           
        }

        void f_FormClosed(object sender, FormClosedEventArgs e)
        {
            n.checkForSaving();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 ss = new Form1();
            ss.StartPosition = FormStartPosition.CenterParent;
            ss.Show();
        }

        private void GNDSetup_Click(object sender, EventArgs e)
        {
            UAV_GUI.Forms.Setup.MP.GroundSetup GroundSetupForm = new UAV_GUI.Forms.Setup.MP.GroundSetup();
            GroundSetupForm.StartPosition = FormStartPosition.CenterParent;
            GroundSetupForm.ShowDialog();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            try
            {
                GlobalData.myLogger.Show();
            }
            catch (Exception ec)
            {
                GlobalData.myLogger = new Logger();
                GlobalData.myLogger.Show();
                throw ec;
            }
        }
       

        protected ManualResetEvent updateMapStop = new ManualResetEvent(false);
       
        
        private void patternCheckBox1_CheckedChanged_1(object sender, EventArgs e)
        {
            CheckBox ckBox = sender as CheckBox;
            reflectUIBasedOnCurrentPattern(ckBox);
            /*
           
            */
            
            //INIData.latitude = myFlight.flyData.Latitude;
            //INIData.longitude = myFlight.flyData.Longitude;
            activatePattern(ckBox.Text);
        }

        private void changeActivePatternCheckBox(RadioButton ActiveChechBox, bool native = true)
        {
            if (native)
                changeActivePatternMenuStrip(patterns.MenuItems.Cast<MenuItem>().FirstOrDefault(i => i.Text.Equals(ActiveChechBox.Text))
                    ,false);
            if (ActiveChechBox.Equals(GlobalData.ActivePatternCheckBox)) {
                GlobalData.ActivePatternCheckBox = null;
                return;
            }
            if (ActiveChechBox.Checked)
            {
                if (GlobalData.ActivePatternCheckBox == null)
                {
                    GlobalData.ActivePatternCheckBox = ActiveChechBox;
                    return;
                }
                GlobalData.ActivePatternCheckBox.Checked = false;
                GlobalData.ActivePatternCheckBox = ActiveChechBox;
                return;
            }
        }
        private void changeActivePatternMenuStrip(MenuItem ActiveMenuItem, bool native =true)
        {

            if (native)
                changeActivePatternCheckBox((RadioButton)tableLayoutPanel3.Controls.Cast<Control>().FirstOrDefault(i => i.Text.Equals(ActiveMenuItem.Text))
                                                ,false);
            if (ActiveMenuItem.Checked) ActiveMenuItem.Checked = false;
            else ActiveMenuItem.Checked = true;
            if (ActiveMenuItem.Equals(GlobalData.ActivePatternMenuItem))
            {
                GlobalData.ActivePatternMenuItem = null;
                return;
            }
            if (ActiveMenuItem.Checked)
            {
                if (GlobalData.ActivePatternMenuItem == null)
                {
                    GlobalData.ActivePatternMenuItem = ActiveMenuItem;
                    return;
                }
                GlobalData.ActivePatternMenuItem.Checked = false;
                GlobalData.ActivePatternMenuItem = ActiveMenuItem;
                return;
            }
        }
        private void reflectUIBasedOnCurrentPattern(Object Pattern_Name)
        {
            bool startOrExit = false;
            if (Pattern_Name is RadioButton)
            {

                RadioButton ckBox = Pattern_Name as RadioButton;
                changeActivePatternCheckBox(ckBox);
                if (ckBox.Checked)
                {
                    ckBox.BackColor = Color.DarkBlue;
                }
                else
                {
                    ckBox.BackColor = Color.FromArgb(255, 154, 181, 152);
                }
                if (ckBox.Checked)
                {
                    startOrExit = true; 
                }
            }
            else {
                MenuItem ckBox = Pattern_Name as MenuItem;
                if (ckBox.Checked)
                {
                    startOrExit = true;
                }
            }
            if (!startOrExit)
            {
                GlobalData.orValNDeg = INIData.spare_latitude;
                GlobalData.orValEDeg = INIData.spare_longitude;
                GlobalData.orValNRad = (GlobalData.orValNDeg * Math.PI) / 180.0;
                GlobalData.orValERad = (GlobalData.orValEDeg * Math.PI) / 180.0;
                myFlight.flyUAV.startPattern(-1);
                return;
            }
            bool returnV = myFlight.flyUAV.setFlightMode(FlightMode.UAV);
            if (!returnV)
            {
                throw new BusinessException(1015, "Cannot Switch The Current Mode ");
            }

            INIData.spare_latitude = GlobalData.orValNDeg;
            INIData.spare_longitude = GlobalData.orValEDeg;
            GlobalData.orValNDeg = myFlight.flyData.Latitude;
            GlobalData.orValEDeg = myFlight.flyData.Longitude;
            GlobalData.orValNRad = (GlobalData.orValNDeg * Math.PI) / 180.0;
            GlobalData.orValERad = (GlobalData.orValEDeg * Math.PI) / 180.0;
        }
        private bool activatePattern(String Pattern_Name) 
        {
            if (Pattern_Name.Equals(PatternsTextEnum.Hover))
            {
               return myFlight.flyUAV.startPattern(4);
            }
            else if (Pattern_Name.Equals(PatternsTextEnum.Orbit_Right))
            {
                return myFlight.flyUAV.startPattern(0);
            }

            else if (Pattern_Name.Equals(PatternsTextEnum.Orbit_left))
            {
                return myFlight.flyUAV.startPattern(1);
            }
            else if (Pattern_Name.Equals(PatternsTextEnum.Take_Picture))
            {
                return myFlight.flyUAV.startPattern(3);
            }
            else if (Pattern_Name.Equals(PatternsTextEnum.Descent_Here))
            {
                return myFlight.flyUAV.startPattern(5);
            }
            else if (Pattern_Name.Equals(PatternsTextEnum.Circle_Right))
            {
                return myFlight.flyUAV.startPattern(6);
            }
            else if (Pattern_Name.Equals(PatternsTextEnum.Circle_left))
            {
                return myFlight.flyUAV.startPattern(7);
            }
            else if (Pattern_Name.Equals(PatternsTextEnum.Fly_Home))
            {
                return myFlight.flyUAV.startPattern(8);
            }
            else if (Pattern_Name.Equals(PatternsTextEnum.Stop_Engine))
            {
                return myFlight.flyUAV.startPattern(9);
            }
            else if (Pattern_Name.Equals(PatternsTextEnum.Land_at_Home))
            {
                return myFlight.flyUAV.startPattern(10);
            }
            else if (Pattern_Name.Equals(PatternsTextEnum. Land_here))
            {
                return myFlight.flyUAV.startPattern(11);
            }
            else if (Pattern_Name.Equals(PatternsTextEnum.Figure8))
            {
                return myFlight.flyUAV.startPattern(2);
            }
            return false; 
        } 


        private void radioButton1_Click(object sender, EventArgs e)
        {
            bool returnV = false;
            if (GlobalData.modeSimulatorOrAuto == 0)
            {
                if (GlobalData.currentMode == FlightMode.RPV)
                {//UAV Mode. 
                    returnV = myFlight.flyUAV.setFlightMode(FlightMode.PIC);

                }
                else if (GlobalData.currentMode == FlightMode.UAV)
                {//RPV Mode. 
                    returnV = myFlight.flyUAV.setFlightMode(FlightMode.RPV);
                }
                else if (GlobalData.currentMode == FlightMode.PIC)
                {//RPV Mode. 
                    returnV = myFlight.flyUAV.setFlightMode(FlightMode.UAV);
                }
                else
                {
                    throw new BusinessException(1002, "Please Make Sure that you chosen a proper mode ");

                }
            }
            else {
                if (GlobalData.currentMode == FlightMode.RPV)
                {//UAV Mode. 
                    returnV = myFlight.flyUAV.setFlightMode(FlightMode.UAV);

                }
                else if (GlobalData.currentMode == FlightMode.UAV)
                {//RPV Mode. 
                    returnV = myFlight.flyUAV.setFlightMode(FlightMode.RPV);
                }
               
                else
                {
                    throw new BusinessException(1016, "Please Make Sure that you chosen a proper mode ");

                }
            }
            if (!returnV)
            {
                throw new BusinessException(1017, "Cannot Switch The Current Mode ");

            }
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            stopAllThreads();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            StaticForms.myFlyFileTransmisionForm.ShowDialog();

        }

        private void transmitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticForms.myVRSTransmitForm.ShowDialog();
        }

        private void acquireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticForms.myVRSReceiveForm.ShowDialog();
        }

        private void flyFileUtilityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticForms.myFlyFileTransmisionForm.ShowDialog();
        }

        private void vrsAcquire_Click(object sender, EventArgs e)
        {
            StaticForms.myVRSReceiveForm.ShowDialog();
        }

        private void transmitVRS_Click(object sender, EventArgs e)
        {
            StaticForms.myVRSTransmitForm.ShowDialog();
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            StaticForms.myFlyFileTransmisionForm.ShowDialog();
        }

        
        /*Not Used In The current version May Be Used Later. */
        /* private void rcStausL_Click(object sender, EventArgs e)
        {
            int ret = myFlight.flyUAV.setJoy(1502,(int) 0x0);
            if (ret != 0) {
                throw new BusinessException(1520, "Cann't Change the RC Status");
            }
            rcChangedToOFF = true;
             ret = myFlight.flyUAV.startFailurePattern(5);
           myFlight.flyUAV.setJoy(1262, 5);
            if (ret != 0)
            {
                throw new BusinessException(1520, "Error Simulating the RC lost failure patern");
            }
        }*/

       

        private void FlightPattern_Click(object sender, EventArgs e)
        {
            StaticForms.myFailurePatternsToolstrip.StartPosition = FormStartPosition.CenterScreen;
            StaticForms.myFailurePatternsToolstrip.Show();
        }

        
        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {

        }

        private void fatalStrip_Click(object sender, EventArgs e)
        {
            GlobalData.myLogger.Show();
        }

       

        private void TestServo(object sender, EventArgs e)
        {
            ServoTest servotest = new ServoTest(myFlight.flyUAV);
            servotest.ShowDialog();
          
        }

      
        private void setVariableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VrsFileHandler vrsHandler = new VrsFileHandler(UAV_GUI.Forms.MicroPilotSetup.fileName);
            setvariable set_variable = new setvariable(myFlight.flyUAV);
            set_variable.ShowDialog();

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void automaticInitializationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UAV_GUI.Forms.AutoConnectWizard myConnectionWiz = new Forms.AutoConnectWizard();
            myConnectionWiz.ShowDialog();
        }

        private void rCSensorMonitorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UAV_GUI.RC_Sensors_Monitor mon_rc = new RC_Sensors_Monitor(myFlight.flyUAV);
            mon_rc.Show();
        }
        
        
        /*Deprciated
        private void updateMapTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                
                try
                {
                    if (!GlobalData.autoPilotReady)
                    {
                        marker.Position = new PointLatLng(INIData.latitude, INIData.longitude);
                        return;
                    }
                    marker.Position = new PointLatLng(myFlight.flyData.Latitude, myFlight.flyData.Longitude);
                    //gmap.Position = getSuitableMapPostion(gmap.Position, marker.Position);
                    gmap.Position = marker.Position;
                    marker.ToolTipText = ("(" + marker.Position.Lat + " , " + marker.Position.Lng + ")");


                    if (i % 500 == 0 && marker.Position.Lng != 0 && marker.Position.Lat != 0)
                    {
                        GMapRoute currentR = new GMapRoute(pointsPath, "path" + pathC);
                        currentR.Stroke.Color = Color.Black;
                        currentR.Stroke.Width = 1;
                        path.Add(currentR);
                        pathC++;
                        pathOverlay.Routes.Add(path[pathC]);


                        i = 1;
                    }
                    if (count % 5 == 0 && Math.Abs(marker.Position.Lng - 0) > 2 && Math.Abs(marker.Position.Lat - 0) > 2)
                    {
                        path[pathC].Points.Add(marker.Position);
                        gmap.UpdateRouteLocalPosition(path[pathC]);
                        count = 0;
                        i++;
                    }

                    count++;
                }
                catch (InvalidOperationException s)
                {
                    GlobalData.LogConsole(s.Message + "at " + this.Name + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());
                    return;
                }
            }
            catch (Exception s)
            {
                GlobalData.LogConsole(s.Message + "at " + this.Name + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());
                return;
            }

        }*/


        private void mapType_Click(object sender, EventArgs e)
        {

        }

        
        private void aGLMonitorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticForms.myAglForm = new AGLForm(myFlight.flyUAV);
            StaticForms.myAglForm.StartPosition = FormStartPosition.CenterScreen;
            StaticForms.myAglForm.Show();
        }

        private void mIBSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*myMibForm = new MIB_Setup();
            myMibForm.StartPosition = FormStartPosition.CenterScreen;
            myMibForm.ShowDialog();*/
        }

        private void cachingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CachingForm form = new CachingForm();
            form.Text = "Caching Properties";
            //form.FormClosed += new FormClosedEventHandler(f_FormClosed);
            form.Show();
        }

        private void airSpeedIndicator_Click(object sender, System.EventArgs e)
        {
            widget widget1 = new widget();
            widget1.WindowState = FormWindowState.Normal;
            widget1.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            widget1.Size = new Size(170, 190);
            widget1.Text = "Air Speed Indicator";
            widget1.Controls.Add(airSpeedIndicatorInstrumentControl2);
            airSpeedIndicatorInstrumentControl2.Location = new Point(0, 0);
            widget1.Show();
            widget1.FormClosing += widget_FormClosing;
        }


        private void altimeterInstrument_Click(Object sender,System.EventArgs e)
        {
            widget widget2 = new widget();
            widget2.WindowState = FormWindowState.Normal;
            widget2.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            widget2.Size = new Size(170, 190);
            widget2.Text = "Altimeter Instrument";
            widget2.Controls.Add(altimeterInstrumentControl2);
            altimeterInstrumentControl2.Location = new Point(0, 0);
            widget2.Show();
        }


        private void attitudeIndicatorInstrument_Click(Object sender,System.EventArgs e)
        {
            widget widget3 = new widget();
            widget3.WindowState = FormWindowState.Normal;
            widget3.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            widget3.Size = new Size(170, 190);
            widget3.Text = "Attitude Indicator";
            widget3.Controls.Add(attitudeIndicatorInstrumentControl2);
            attitudeIndicatorInstrumentControl2.Location = new Point(0, 0);
            widget3.Show();
        }

        private void turnCoordinatorInstrument_Click(Object sender,System.EventArgs e)
        {
            widget widget4 = new widget();
            widget4.WindowState = FormWindowState.Normal;
            widget4.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            widget4.Size = new Size(170, 190);
            widget4.Text = "Turn Coordinator Instrument";
            widget4.Controls.Add(turnCoordinatorInstrumentControl1);
            turnCoordinatorInstrumentControl1.Location = new Point(0, 0);
            widget4.Show();
        }

        private void verticalSpeedIndicator_Click(Object sender,System.EventArgs e)
        {
            widget widget5 = new widget();
            widget5.WindowState = FormWindowState.Normal;
            widget5.Size = new Size(170, 190);
            widget5.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            widget5.Text = "Vertical Speed Indicator";
            widget5.Controls.Add(verticalSpeedIndicatorInstrumentControl1);
            verticalSpeedIndicatorInstrumentControl1.Location = new Point(0, 0);
            widget5.Show();
        }

        private void headingIndicator_Click(Object sender,System.EventArgs e)
        {
            widget widget6 = new widget();
            widget6.WindowState = FormWindowState.Normal;
            widget6.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            widget6.Size = new Size(170, 190);
            widget6.Text = "Heading Indicator";
            widget6.Controls.Add(headingIndicatorInstrumentControl2);
            headingIndicatorInstrumentControl2.Location = new Point(0, 0);
            widget6.Show();
        }

        private void widget_FormClosing(Object sender, FormClosingEventArgs e)
        {
            //In case windows is trying to shut down, don't hold the process up
            if (e.CloseReason == CloseReason.WindowsShutDown) return;

        }

        private void hover_Click(Object sender, System.EventArgs e)
        {
            MenuItem ckBox = sender as MenuItem;
            reflectUIBasedOnCurrentPattern(ckBox);
           // changeActivePatternMenuStrip(ckBox);
            return;
            

            if (!ckBox.Checked)
            {
                INIData.latitude = INIData.spare_latitude;
                INIData.longitude = INIData.spare_longitude;
                myFlight.flyUAV.startPattern(-1);
                return;

            }
            bool returnV = myFlight.flyUAV.setFlightMode(FlightMode.UAV);

            for (int i = 0; i < patterns.MenuItems.Count; i++)
            {

                if (!patterns.MenuItems[i].Text.Equals(ckBox.Text) && patterns.MenuItems[i].Checked)
                {
                    patterns.MenuItems[i].Checked = false;
                }
            }

            if (!returnV)
            {
                throw new BusinessException(1015, "Cannot Switch The Current Mode ");

            }
            
            INIData.spare_latitude = INIData.latitude;
            INIData.spare_longitude = INIData.longitude;
            INIData.latitude = myFlight.flyData.Latitude;
            INIData.longitude = myFlight.flyData.Longitude;
            activatePattern(ckBox.Text);
        }

        private void FlightPlan_CheckedChanged(object sender, EventArgs e)
        {
            if (FlightPlan.Checked)
            {
                //Uncheck Other Side Menu
               

                n = new Form2();
                n.TopLevel = false;
                this.splitContainer1.Panel2.Controls.Clear();
                this.splitContainer1.Panel2.Controls.Add(n);
                n.Text = "Untitled.fly - Flight Plan";
                n.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                n.Dock = DockStyle.Fill;
                n.Show();
            }
        }


        private void Panel_CheckedChanged(object sender, EventArgs e)
        {
         
            CheckBox currentPanel = sender as CheckBox;

            if (!currentPanel.Checked)
            {
                return;
            }
            
            Console.WriteLine(currentPanel.Text); 
            if (!currentPanel.Equals(Tools))
                this.Tools.Checked = false;
            if (!currentPanel.Equals(Settings))
                this.Settings.Checked = false;
            if (!currentPanel.Equals(FlightPlan))
                this.FlightPlan.Checked = false;
            if (!currentPanel.Equals(FlightData))
                this.FlightData.Checked = false;
            if (!currentPanel.Equals(Help))
                this.Help.Checked = false;
           
        
        }
        private void Settings_CheckedChanged(object sender, EventArgs e)
        {
            
            
          if (Settings.Checked)
            {
                //Clear Main Panel
                this.splitContainer1.Panel2.Controls.Clear();
                VrsFileHandler vrsHandler = new VrsFileHandler(UAV_GUI.Forms.MicroPilotSetup.fileName);
                StaticForms.SettingsForm = new SettingsMainForm(myFlight, vrsHandler);
                StaticForms.SettingsForm.TopLevel = false;
                this.splitContainer1.Panel2.Controls.Clear();
                this.splitContainer1.Panel2.Controls.Add(StaticForms.SettingsForm);

                StaticForms.SettingsForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                StaticForms.SettingsForm.Dock = DockStyle.Fill;
                StaticForms.SettingsForm.Show();


            }else
            {
                Console.WriteLine("changed1");
                StaticForms.SettingsForm.Close();
            }
        }

        private void FlightData_CheckedChanged(object sender, EventArgs e)
        {
            if (FlightData.Checked)
            {
                this.splitContainer1.Panel2.Controls.Clear();
                this.splitContainer1.Panel2.Controls.Add(gmap);
                this.splitContainer1.Panel2.Controls.Add(trackBar1);
                this.splitContainer1.Panel2.Controls.Add(NavBoardStatus);
                this.splitContainer1.Panel2.Controls.Add(tabControl1);

            }
        }


        private void Tools_CheckedChanged(object sender, EventArgs e)
        {
            if (Tools.Checked)
            {
                //Clear Main Panel
                this.splitContainer1.Panel2.Controls.Clear();
                StaticForms.toolsForm = new ToolsWindow(myFlight);
                StaticForms.toolsForm.TopLevel = false;
                this.splitContainer1.Panel2.Controls.Clear();
                this.splitContainer1.Panel2.Controls.Add(StaticForms.toolsForm);
                StaticForms.toolsForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                StaticForms.toolsForm.Dock = DockStyle.Fill;
                StaticForms.toolsForm.Show();


            }
            else
            {
                Console.WriteLine("changed");
                StaticForms.toolsForm.Close();
            }
            
        }

        private void Monitor_click(object sender, EventArgs e)
        {
            GlobalData.myLogger.Show();
        }

        private void FailurePatterns_Click(object sender, EventArgs e)
        {
            StaticForms.myFailurePatternsToolstrip.StartPosition = FormStartPosition.CenterScreen;
            StaticForms.myFailurePatternsToolstrip.Show();
        }


        private void StatusTabPage_Click(object sender, EventArgs e)
        {

        }

        private void Help_CheckedChanged(object sender, EventArgs e)
        {
            if (Help.Checked)
            {
                

                UAV_GUI.Forms.Main_Screen.AboutWindow n = new UAV_GUI.Forms.Main_Screen.AboutWindow();
                n.TopLevel = false;
                this.splitContainer1.Panel2.Controls.Clear();
                this.splitContainer1.Panel2.Controls.Add(n);
                n.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                n.Dock = DockStyle.Fill;
                n.Show();
            }
            
        }

       

    }
}
