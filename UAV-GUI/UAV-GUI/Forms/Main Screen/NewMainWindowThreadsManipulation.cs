﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Threading;
using UAV_GUI.FileClasses;
using MasterClasses;
using Error_Reporting;
using MP_Lib;
using JoystickPlugin;
using FileClasses.VRS_Files;
using FileClasses.INI_Files;
using UAV_GUI.FileClasses.FlyFiles;
using UAV_GUI.FlightClasses;
using UAV_GUI.Forms.Setup.MP;
using UAV_GUI.Forms;
using UAV_GUI.Forms.Main_Screen;
using GMap.NET.MapProviders;
using UAV_GUI.Utilities;
using FlightMonitor;
using System.Diagnostics;
using UAV_GUI.Forms.Set_Variable;
using GLobal;


namespace UAV_GUI
{
    public partial class NewMainWindow
    {
        #region Thread Manipuation
        /// <summary>
        /// Manipulated all availabe function to handle threads .
        /// </summary>

        private void startAllThreads()
        {
            initializeGraphs();
            //initRouteVariables();
            seekingTimer.Enabled = true;
            apStatusTimer.Enabled = true;
            apStatus2Timer.Enabled = true;
            bindGaugesTimer.Enabled = true;
            graphTimer.Enabled = true;
            labelsTimer.Enabled = true;
            updateMapTimer.Enabled = true;
            simStatusTimer.Enabled = true;
            Jacksonsoft.WaitWindow.Show(this.WorkerMethod, null, "1");
        }
        private void startAllThreadsBackGround()
        {

            //updateMap
            updateMapThread = new Thread(new ThreadStart(updateMap));
            updateMapStop.Reset();
            updateMapThread.Start();

            myFlight.Start();

        }
        public void suspendAllThreads()
        {
            seekingTimer.Enabled = false;
            apStatusTimer.Enabled = false;
            apStatus2Timer.Enabled = false;
            bindGaugesTimer.Enabled = false;
            graphTimer.Enabled = false;
            labelsTimer.Enabled = false;
            updateMapTimer.Enabled = false;
            simStatusTimer.Enabled = false;
            Jacksonsoft.WaitWindow.Show(this.WorkerMethod, null, "3");
        }

        public void suspendAllThreadsBackGround()
        {
            updateMapStop.Set();

            myFlight.Suspend();
            //seekingTimer.Enabled = false;
            Thread.Sleep(10);

        }
        public void resumeAllThreads()
        {
            seekingTimer.Enabled = true;
            apStatusTimer.Enabled = true;
            apStatus2Timer.Enabled = true;
            bindGaugesTimer.Enabled = true;
            graphTimer.Enabled = true;
            labelsTimer.Enabled = true;
            updateMapTimer.Enabled = true;
            simStatusTimer.Enabled = true;
            Jacksonsoft.WaitWindow.Show(this.WorkerMethod, null, "4");
        }
        public void resumeAllThreadsBackGround()
        {
            updateMapStop.Reset();
            myFlight.Resume();

        }
        public void stopAllThreads()
        {
            seekingTimer.Enabled = false;
            apStatusTimer.Enabled = false;
            apStatus2Timer.Enabled = false;
            bindGaugesTimer.Enabled = false;
            graphTimer.Enabled = false;
            labelsTimer.Enabled = false;
            updateMapTimer.Enabled = false;
            simStatusTimer.Enabled = false;
            Jacksonsoft.WaitWindow.Show(this.WorkerMethod, null, "2");
        }
        public void stopAllThreadsBackGround()
        {
            //updateMap
            if (updateMapThread != null)
            {
                updateMapStop.Set();
                Thread.Sleep(10);
                updateMapThread.Abort();
                updateMapThread = null;
            }

            //Flight Data .
            myFlight.Stop();
        }

        #endregion

        /*To be reserved whatever
         Tariq Shatat
         * Add the ability to pause threads
         */
        

    }
}
