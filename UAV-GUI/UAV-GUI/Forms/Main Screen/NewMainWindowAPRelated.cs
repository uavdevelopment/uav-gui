﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Threading;
using UAV_GUI.FileClasses;
using MasterClasses;
using Error_Reporting;
using MP_Lib;
using JoystickPlugin;
using FileClasses.VRS_Files;
using FileClasses.INI_Files;
using UAV_GUI.FileClasses.FlyFiles;
using UAV_GUI.FlightClasses;
using UAV_GUI.Forms.Setup.MP;
using UAV_GUI.Forms;
using UAV_GUI.Forms.Main_Screen;
using GMap.NET.MapProviders;
using UAV_GUI.Utilities;
using FlightMonitor;
using System.Diagnostics;
using UAV_GUI.Forms.Set_Variable;
using GLobal;
using System.IO;

namespace UAV_GUI
{
    public partial class NewMainWindow
    {
        [STAThread()]
        private void LoadFlyFileB_Click(object sender, EventArgs e)
        {

            //Console.WriteLine("un Compiloe " + AutoPilotUnits.uncompileGPSPositionFromAutoPilotUnits(INIData.latitude));
            //Console.WriteLine(INIData.latitude + "Compiloe " +AutoPilotUnits.compileGPSPositionFromAutoPilotUnits( AutoPilotUnits.uncompileGPSPositionFromAutoPilotUnits(INIData.latitude)));
            if (MainMap_orlong == -1 || MainMap_orlat == -1)
            {
                MessageBox.Show("Can't Add WP Befor Setting the Origin Location \n  Please Click on the Map to Specify an Origin ", "Origin Setting");

            }
            else
            {
                /*  Clear Map Elements   */
                // this.openFileDialog1.Filter = "*.fly| *.wpt";
                DialogResult result = openFileDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    fileNme = openFileDialog1.FileName;
                }
                else
                {
                    return;
                }
                loadFlyFile(fileNme);
                this.flyFileLabel.Text = fileNme;
                this.gmap.Position = new PointLatLng(INIData.latitude, INIData.longitude);
                connectButton.Enabled = true;


            }
        }

        private void loadFlyFile(String fileNme)
        {
            MapOptions.clearMap(ref gmap);
            wayPointList.Clear();
            markersList.Clear();
            points.Clear();
            route.Points.Clear();

            FlyFileParser f = new FlyFileParser();
            f.orlat = INIData.latitude;
            f.orlong = INIData.longitude;



            wayPointList.AddRange(f.parseFlyFile(fileNme));
            bool fixed_ = false;
            Console.WriteLine("Count is --------->" + wayPointList.Count);
            for (int u = 0; u < wayPointList.Count; u++)
            {
                for (int m = 0; m < wayPointList[u].commandsList.Count; m++)
                {
                    if (wayPointList[u].commandsList[m].name == CommandName.fixed_)
                        fixed_ = true;
                }
                if (fixed_)
                    break;

                PointLatLng p = new PointLatLng(wayPointList[u].lat, wayPointList[u].longt);
                GMarkerGoogle nMarker = new GMarkerGoogle(p, GMarkerGoogleType.blue_small);

                markersOverlay.Markers.Add(nMarker);
                markersList.Add(nMarker);
                route.Points.Add(p);
                routeList.Add(route);

                route.Stroke.Color = Color.Red;
                route.Stroke.Width = 1.5f;
                wpRouteOverlay.Routes.Add(route);

                moVingmarker = new GMapPathMarker(new PointLatLng(39.466, -76.16), mya, 0, 1500, 4);
                pathOverlay.Routes.Add(moVingmarker.route);
                markersOverlay.Markers.Add(moVingmarker);

                gmap.UpdateRouteLocalPosition(route);
                wayPointList[u].marker = nMarker;
            }
        }




        private void connectButton_Click(object sender, EventArgs e)
        {
            if (connectButton.Text.Equals("Connect"))
            {
                bool ret = false;

                //ret = myFlight.flyUAV.connect(fileNme, GlobalData.modeSimulatorOrAuto);
                loadFlyFile(fileNme);
                myFlight = new Flight();
                ret = (bool)Jacksonsoft.WaitWindow.Show(this.WorkerMethod, null, "5");
                if (ret)
                {

                    startAllThreads();
                    this.arm_takeOff.Visible = true;
                    this.arm_takeOff.Enabled = true;
                    connectButton.Text = "Disconnect";
                    this.isSimulator.Enabled = false;


                }
                else
                {
                    MessageBox.Show("Plz check configuration and com port", "COM Port error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            else
            {
                if (myFlight.flyUAV.disConnect())
                {



                    connectButton.Text = "Connect";
                    this.isSimulator.Enabled = false;

                    stopAllThreads();
                    isSimulator.Enabled = true;
                    this.arm_takeOff.Visible = false;
                    this.arm_takeOff.Text = "Arm";

                }

            }
        }

        private void isSimulatorChech_Changed(object sender, EventArgs e)
        {
            CheckBox sender_ = sender as CheckBox;
            if (sender_.Checked)
            {
                GlobalData.modeSimulatorOrAuto = 1;
                GlobalData.LogConsole("checked");
            }
            else
            {
                GlobalData.modeSimulatorOrAuto = 0;
                GlobalData.LogConsole("Not checked");
            }
            GlobalData.LogConsole(GlobalData.modeSimulatorOrAuto + "");

        }

        private void arm_takeOff_Click(object sender, EventArgs e)
        {


            bool ret = false;
            Button sen = (sender as Button);
            if (sen.Text.Equals("Arm"))
            {


                if (!GlobalData.autoPilotReady && GlobalData.modeSimulatorOrAuto == 0)
                {
                    throw new BusinessException(1010, "We cannot fly while autopilot is not ready");

                }

                int valueA = myFlight.flyUAV.checkForFatalError();
                if (valueA >= 2 && valueA <= 110)
                {
                    //    throw new BusinessException(1011, "We cannot fly while there were a fatal Error");
                }

                if (!myFlight.flyUAV.verifyBetweenAirCruiseSpeed())
                {
                    throw new BusinessException(1012, "We cannot fly while the Wind Speed exceeds 50 % of the Curise Speed");
                }
                suspendAllThreads();
                ret = (bool)Jacksonsoft.WaitWindow.Show(this.WorkerMethod, null, "6");//Arm_initialize in background

                if (ret)
                {


                    sen.Text = "Take Off";
                }
                resumeAllThreads();
            }
            else if (sen.Text.Equals("Take Off"))
            {

                suspendAllThreads();
                ret = (bool)Jacksonsoft.WaitWindow.Show(this.WorkerMethod, null, "7");

                if (ret)
                {

                    sen.Text = "Terminate";
                }

                resumeAllThreads();
            }
            else if (sen.Text.Equals("Terminate"))
            {
                List<string> lines = new List<string>();
                foreach (PointLatLng phoneBookCore in moVingmarker.ActualPath)
                {
                    lines.Add(phoneBookCore.Lat+" "+phoneBookCore.Lng);  // Adds the Group Name
                    
                }
                System.IO.File.WriteAllLines(@"D:\txt.txt", lines);
                /*Need to add terminte fly API*/
                stopAllThreads();
                sen.Text = "Arm";
                sen.Visible = false;
                connectButton.Text = "Connect";
                connectButton.Enabled = true;
                isSimulator.Enabled = true;
                isSimulator.Checked = false;


            }




        }

        private void gpsLock_Click(object sender, EventArgs e)
        {
            try
            {
                if (!myFlight.flyUAV.fakeTheGPSLock())
                {
                    throw new BusinessException(1013, "Can not fake the GPS");
                }


                //readBiasSetupStartThread.Abort();
            }
            catch (Exception exception)
            {
                GlobalData.LogConsole(exception.Message);
                throw new BusinessException(1014, "Can not fake the GPS");

            }
            if (seekingTimer.Enabled)
            {
                seekingTimer.Enabled = false;
            }
            this.gpsLock.Text = "GPS Faked";
            this.gpsLock.BackColor = Color.Yellow;

        }

        private void WorkerMethod(object sender, Jacksonsoft.WaitWindowEventArgs e)
        {
            if (e.Arguments.Count > 0)
            {
                if (e.Arguments[0].Equals("1"))
                {
                    startAllThreadsBackGround();
                }
                else if (e.Arguments[0].Equals("2"))
                {
                    stopAllThreadsBackGround();
                }
                else if (e.Arguments[0].Equals("3"))
                {
                    suspendAllThreadsBackGround();
                }
                else if (e.Arguments[0].Equals("4"))
                {
                    resumeAllThreadsBackGround();
                }
                else if (e.Arguments[0].Equals("5"))
                {

                    e.Result = myFlight.flyUAV.connect(fileNme, GlobalData.modeSimulatorOrAuto);
                    MessageBox.Show(myFlight.flyUAV.getErrorString(5)+"Hhhhhhhhhh");
                    gmap.Position = new PointLatLng(INIData.latitude, INIData.longitude);
                }
                else if (e.Arguments[0].Equals("6"))
                {

                    e.Result = myFlight.flyUAV.Arm_Initialize(GlobalData.modeSimulatorOrAuto);
                }
                else if (e.Arguments[0].Equals("7"))
                {
                    e.Result = myFlight.flyUAV.takeOff(GlobalData.modeSimulatorOrAuto); Thread.Sleep(250);
                    Location ee = myFlight.flyUAV.getOrigin();
                    gmap.Position = new PointLatLng(ee.Latitude, ee.Longitude);
                }


            }
        }

    }
}
