﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Threading;
using UAV_GUI.FileClasses;
using MasterClasses;
using Error_Reporting;
using MP_Lib;
using JoystickPlugin;
using FileClasses.VRS_Files;
using FileClasses.INI_Files;
using UAV_GUI.FileClasses.FlyFiles;
using UAV_GUI.FlightClasses;
using UAV_GUI.Forms.Setup.MP;
using UAV_GUI.Forms;
using UAV_GUI.Forms.Main_Screen;
using GMap.NET.MapProviders;
using UAV_GUI.Utilities;
using FlightMonitor;
using System.Diagnostics;
using UAV_GUI.Forms.Set_Variable;
using GLobal;


namespace UAV_GUI
{
    public partial class NewMainWindow
    {
        int numGraphs = 0;
        List<ZedGraph.PointPairList> myList;
        String[] tags = new String[3];

        private void initializeGraphs()
        {
            numGraphs = 0;
            if (INIData.AltEnabled) numGraphs++;
            if (INIData.HeadingEnabled) numGraphs++;
            if (INIData.ASpeedEnabled) numGraphs++;
            if (numGraphs == 0) return;
            myList = new List<ZedGraph.PointPairList>();

            tags[0] = "Altitude";
            tags[1] = "Heading";
            tags[2] = "Air Speed";
            myGraph = new GraphOptions(graphDisplay, myList, numGraphs, tags.ToList());
            myGraph.initialize();
        }

        private void bindLabels()
        {

            altitudeL.DataBindings.Add(new Binding("Text", myFlight.flyData, "Altitude"));
            yawL.DataBindings.Add(new Binding("Text", myFlight.flyData, "yaw"));
            HeadingL.DataBindings.Add(new Binding("Text", myFlight.flyData, "Heading"));
            DistTONXPL.DataBindings.Add(new Binding("Text", myFlight.flyData, "DTNWP"));
            airSpeedL.DataBindings.Add(new Binding("Text", myFlight.flyData, "AirSpeed"));
            aglHeighL.DataBindings.Add(new Binding("Text", myFlight.flyData, "AglHeight"));
            /*Testing*/
            /* verticalSpeedL.DataBindings.Add(new Binding("Text", myFlight.flyData, "Altitude"));
             groundSpeedL.DataBindings.Add(new Binding("Text", myFlight.flyData, "Altitude"));*/

        }

        private void initializebinding()
        {
            // bindGauges();
            bindLabels();
        }

        void initContextMenu()
        {
            /*Menu Item Initializer*/

            MenuItem loadFlyFile = new MenuItem("Load Fly File");
            loadFlyFile.Click += new System.EventHandler(this.LoadFlyFileB_Click);

            MenuItem connect = new MenuItem("Connect");
            connect.Click += new System.EventHandler(this.connectButton_Click);

            MenuItem artificialGCS = new MenuItem("Artificial GCS");
            MenuItem airSpeedIndicator = new MenuItem("Air Speed Indicator");
            MenuItem altimeterInstrument = new MenuItem("Altimeter Instrument");
            MenuItem attitudeIndicatorInstrument = new MenuItem("Attitude Indicator Instrument");
            MenuItem turnCoordinatorInstrument = new MenuItem("Turn Coordinator Instrument");
            MenuItem verticalSpeedIndicator = new MenuItem("Vertical Speed Indicator");
            MenuItem headingIndicator = new MenuItem("Heading Indicator");
            airSpeedIndicator.Click += new System.EventHandler(this.airSpeedIndicator_Click);
            altimeterInstrument.Click += new System.EventHandler(this.altimeterInstrument_Click);
            attitudeIndicatorInstrument.Click += new System.EventHandler(this.attitudeIndicatorInstrument_Click);
            turnCoordinatorInstrument.Click += new System.EventHandler(this.turnCoordinatorInstrument_Click);
            verticalSpeedIndicator.Click += new System.EventHandler(this.verticalSpeedIndicator_Click);
            headingIndicator.Click += new System.EventHandler(this.headingIndicator_Click);
            artificialGCS.MenuItems.Add(airSpeedIndicator);
            artificialGCS.MenuItems.Add(altimeterInstrument);
            artificialGCS.MenuItems.Add(attitudeIndicatorInstrument);
            artificialGCS.MenuItems.Add(turnCoordinatorInstrument);
            artificialGCS.MenuItems.Add(verticalSpeedIndicator);
            artificialGCS.MenuItems.Add(headingIndicator);
            patterns = new MenuItem("Patterns");
            MenuItem hover = new MenuItem(PatternsTextEnum.Hover);
            hover.Click += new System.EventHandler(patternCheckBox1_CheckedChanged_1);
            MenuItem orbit = new MenuItem(PatternsTextEnum.Orbit_Right);
            orbit.Click += new System.EventHandler(hover_Click);
            MenuItem orbitLeft = new MenuItem(PatternsTextEnum.Orbit_left);
            orbitLeft.Click += new System.EventHandler(hover_Click);
            MenuItem takePicture = new MenuItem(PatternsTextEnum.Take_Picture);
            takePicture.Click += new System.EventHandler(hover_Click);
            MenuItem descent = new MenuItem(PatternsTextEnum.Descent_Here);
            descent.Click += new System.EventHandler(hover_Click);
            MenuItem circle = new MenuItem(PatternsTextEnum.Circle_Right);
            circle.Click += new System.EventHandler(hover_Click);
            MenuItem circleLeft = new MenuItem(PatternsTextEnum.Circle_left);
            circleLeft.Click += new System.EventHandler(hover_Click);
            MenuItem flyHome = new MenuItem(PatternsTextEnum.Fly_Home);
            flyHome.Click += new System.EventHandler(hover_Click);
            MenuItem stop = new MenuItem(PatternsTextEnum.Stop_Engine);
            stop.Click += new System.EventHandler(hover_Click);
            MenuItem landAt = new MenuItem(PatternsTextEnum.Land_at_Home);
            landAt.Click += new System.EventHandler(hover_Click);
            MenuItem landHere = new MenuItem(PatternsTextEnum.Land_at_Home);
            landHere.Click += new System.EventHandler(hover_Click);
            MenuItem figure8 = new MenuItem(PatternsTextEnum.Figure8);
            figure8.Click += new System.EventHandler(hover_Click);
            patterns.MenuItems.Add(hover);
            patterns.MenuItems.Add(orbit);
            patterns.MenuItems.Add(orbitLeft);
            patterns.MenuItems.Add(takePicture);
            patterns.MenuItems.Add(descent);
            patterns.MenuItems.Add(circle);
            patterns.MenuItems.Add(circleLeft);
            patterns.MenuItems.Add(flyHome);
            patterns.MenuItems.Add(stop);
            patterns.MenuItems.Add(landAt);
            patterns.MenuItems.Add(landHere);
            patterns.MenuItems.Add(figure8);
            MenuItem mapType = new MenuItem("Map Type");
            GMapProvider[] providers = GMapProviders.List.ToArray();

            for (int i = 0; i < providers.Length; i++)
            {
                mapType.MenuItems.Add(providers[i].Name);
                mapType.MenuItems[i].Click += new System.EventHandler(mapType_Click);
            }
            MenuItem clearMap = new MenuItem("Clear Map");
            clearMap.Click += new System.EventHandler(this.clearMap_Click);
            MenuItem reload = new MenuItem("Reload");
            reload.Click += new System.EventHandler(this.reloadMap_Click);
            cm.MenuItems.Add(loadFlyFile);
            cm.MenuItems.Add(connect);
            cm.MenuItems.Add("-");
            cm.MenuItems.Add(artificialGCS);
            cm.MenuItems.Add(patterns);
            cm.MenuItems.Add("-");
            cm.MenuItems.Add(mapType);
            cm.MenuItems.Add(clearMap);
            cm.MenuItems.Add(reload);

        }
        void convertCheckBoxesToButton()
        {
            hoverC.Appearance = Appearance.Button;
            orbitRight.Appearance = Appearance.Button;
            orbitLeftC.Appearance = Appearance.Button;
            takePictureC.Appearance = Appearance.Button;
            DescC.Appearance = Appearance.Button;
            circleRightC.Appearance = Appearance.Button;
            circleLeftC.Appearance = Appearance.Button;
            FlyHomeC.Appearance = Appearance.Button;
            stopC.Appearance = Appearance.Button;
            landHomeC.Appearance = Appearance.Button;
            landHereC.Appearance = Appearance.Button;
            figure8C.Appearance = Appearance.Button;

            for (int i = 0; i < flightToolStripMenuItem.DropDown.Items.Count; i++)
                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[i]).Checked = true;

            hoverC.BackColor = Color.SteelBlue;
            orbitRight.BackColor = Color.SteelBlue;
            orbitLeftC.BackColor = Color.SteelBlue;
            takePictureC.BackColor = Color.SteelBlue;
            DescC.BackColor = Color.SteelBlue;
            circleRightC.BackColor = Color.SteelBlue;
            circleLeftC.BackColor = Color.SteelBlue;
            FlyHomeC.BackColor = Color.SteelBlue;
            stopC.BackColor = Color.SteelBlue;
            landHomeC.BackColor = Color.SteelBlue;
            landHereC.BackColor = Color.SteelBlue;
            figure8C.BackColor = Color.SteelBlue;
        }

        public void initmMap()
        {
            /**/
            this.gmap = new GMap.NET.WindowsForms.GMapControl();

            this.gmap.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.gmap.Bearing = 0F;
            this.gmap.CanDragMap = false;
            this.gmap.EmptyTileColor = System.Drawing.Color.Navy;
            this.gmap.GrayScaleMode = false;
            this.gmap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gmap.LevelsKeepInMemmory = 5;
            this.gmap.Location = new System.Drawing.Point(0, 15);
            this.gmap.MarkersEnabled = true;
            this.gmap.MaxZoom = 20;
            this.gmap.MinZoom = 0;
            this.gmap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionWithoutCenter;
            this.gmap.Name = "gmap";
            this.gmap.NegativeMode = false;
            this.gmap.PolygonsEnabled = true;
            this.gmap.RetryLoadTile = 0;
            this.gmap.RoutesEnabled = true;
            this.gmap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Fractional;
            this.gmap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gmap.ShowTileGridLines = false;
            this.gmap.Size = new System.Drawing.Size(1215, 480);
            this.gmap.TabIndex = 16;
            this.gmap.Zoom = 10D;
            gmap.Tag = 2;
            this.gmap.DragButton = MouseButtons.Right;
            this.gmap.CanDragMap = true;
            this.gmap.BorderStyle = BorderStyle.Fixed3D;
            this.gmap.DisableFocusOnMouseEnter = true;
            this.gmap.ContextMenu = cm;

            ///
            //this.gmap.OnMarkerClick += new MarkerClick(this.on_marker_click);
            //this.gmap.OnMarkerEnter += new MarkerEnter(this.on_marker_enter);
            ///  this.gmap.OnMarkerLeave += new MarkerLeave(this.on_marker_leave);
            // this.gmap.MouseDown += new MouseEventHandler(MainMap_MouseDown);
            // this.gmap.MouseUp += new MouseEventHandler(MainMap_MouseUp);
            trackBar1.ValueChanged += new System.EventHandler(TrackBar1_ValueChanged);
            this.gmap.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseWheel);
            ///
            // this.gmap.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.map1_MouseDoubleClick_);
            if (!start)
            {

                foreach (Control item in this.Controls.OfType<Control>())
                {
                    if (item.Name.Equals("gmap"))
                        this.Controls.Remove(item);
                }
            }
            this.Controls.Add(this.gmap);

            /*Overlays Defintions*/

            moVingmarker = new GMapPathMarker(new PointLatLng(39.466, -76.16), mya, 0, 1500, 4);

            markersOverlay = new GMapOverlay("markers");
            gmap.Overlays.Add(markersOverlay);

            points = new List<PointLatLng>();
            route = new GMapRoute(points, "route");
            route.Stroke = new Pen(Color.Red, 1);
            wpRouteOverlay = new GMapOverlay("route");
            wpRouteOverlay.Routes.Add(route);
            gmap.Overlays.Add(wpRouteOverlay);


            pathOverlay = new GMapOverlay("path");
            pathOverlay.Routes.Add(moVingmarker.route);
            gmap.Overlays.Add(pathOverlay);
            gmap.UpdateRouteLocalPosition(moVingmarker.route);


            gmap.MapProvider = GMapProviders.List[INIData.selectedMap];
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;
            gmap.Position = new PointLatLng(39.4666666597666, -76.1681666929581);
            gmap.Zoom = 14;
            wayPointList = new List<WayPoint>();
            commandsList = new List<Command>();
            start = false;
            //gmap.UpdateRouteLocalPosition(route)
        }

        /// <summary>
        /// Initiailization of Menu tool Stip upon click or tool strip click
        /// </summary>

        private void initializeFormsAssociated()
        {
            StaticForms.myFailurePatternsToolstrip = new FailurePatternSimulation(myFlight.flyUAV);
            StaticForms.myFlyFileTransmisionForm = new TransmitFlyFile(myFlight.flyUAV);
            StaticForms.myVRSReceiveForm = new TransmitVRSFile(myFlight.flyUAV, true);
            StaticForms.myVRSTransmitForm = new UAV_GUI.Forms.Main_Screen.TransmitVRSFile(myFlight.flyUAV, false);


        }

        private void initSplitPanel() {

            FlightData.Image = Image.FromFile("imgs/flightdata2.png");
            FlightData.BackColor = Color.FromArgb(255, 2, 28, 30);
            FlightPlan.Image = Image.FromFile("imgs/flightplan2.png");
            //Simulation.Image = Image.FromFile("simulation2.png");
            Settings.Image = Image.FromFile("imgs/settings2.png");
            Tools.Image = Image.FromFile("imgs/tools2.png");
            Help.Image = Image.FromFile("imgs/help2.png");
            this.splitContainer1.Panel1.Controls.Add(FlightData);
            this.splitContainer1.Panel1.Controls.Add(FlightPlan);
            //this.splitContainer1.Panel1.Controls.Add(Simulation);
            this.splitContainer1.Panel1.Controls.Add(Settings);
            this.splitContainer1.Panel1.Controls.Add(Tools);
            this.splitContainer1.Panel1.Controls.Add(Help);

            this.splitContainer1.Panel2.Controls.Add(gmap);
            this.splitContainer1.Panel2.Controls.Add(trackBar1);
            this.splitContainer1.Panel2.Controls.Add(NavBoardStatus);
            this.splitContainer1.Panel2.Controls.Add(tabControl1);
        }
    }
}
