﻿namespace UAV_GUI
{
    partial class NewMainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewMainWindow));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadFlyFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.artificialHorizonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusPanelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autopilotControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.patternsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graphDisplayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flightControlToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rCSensorMonitorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automaticInitializationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flightPlanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generalSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.microPilotSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groundSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mIBSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flightMonitorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.joyStickControllerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.flyFileUtilityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vRSFileUtilityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transmitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acquireToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testServosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.failurePatternsSimulationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setVariableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aGLMonitorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cachingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.navigationDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serialPortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cameraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flightMonitorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.joystickControllerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.DTNWPTL = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.AltitudeLL = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.headingStL = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel7 = new System.Windows.Forms.ToolStripStatusLabel();
            this.velocityL = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.gpsLock = new System.Windows.Forms.ToolStripStatusLabel();
            this.readySL = new System.Windows.Forms.ToolStripStatusLabel();
            this.linkSPanel = new System.Windows.Forms.ToolStripStatusLabel();
            this.rcStausL = new System.Windows.Forms.ToolStripStatusLabel();
            this.failureStrip = new System.Windows.Forms.ToolStripStatusLabel();
            this.fatalStrip = new System.Windows.Forms.ToolStripStatusLabel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.flyFileLabel = new System.Windows.Forms.Label();
            this.arm_takeOff = new System.Windows.Forms.Button();
            this.connectButton = new System.Windows.Forms.Button();
            this.isSimulator = new System.Windows.Forms.CheckBox();
            this.loadFileButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.figure8C = new System.Windows.Forms.RadioButton();
            this.hoverC = new System.Windows.Forms.RadioButton();
            this.landHereC = new System.Windows.Forms.RadioButton();
            this.landHomeC = new System.Windows.Forms.RadioButton();
            this.stopC = new System.Windows.Forms.RadioButton();
            this.FlyHomeC = new System.Windows.Forms.RadioButton();
            this.circleLeftC = new System.Windows.Forms.RadioButton();
            this.circleRightC = new System.Windows.Forms.RadioButton();
            this.takePictureC = new System.Windows.Forms.RadioButton();
            this.orbitLeftC = new System.Windows.Forms.RadioButton();
            this.orbitRight = new System.Windows.Forms.RadioButton();
            this.DescC = new System.Windows.Forms.RadioButton();
            this.seekingTimer = new System.Windows.Forms.Timer(this.components);
            this.apStatusTimer = new System.Windows.Forms.Timer(this.components);
            this.apStatus2Timer = new System.Windows.Forms.Timer(this.components);
            this.bindGaugesTimer = new System.Windows.Forms.Timer(this.components);
            this.graphTimer = new System.Windows.Forms.Timer(this.components);
            this.labelsTimer = new System.Windows.Forms.Timer(this.components);
            this.updateMapTimer = new System.Windows.Forms.Timer(this.components);
            this.FlightData = new System.Windows.Forms.RadioButton();
            this.FlightPlan = new System.Windows.Forms.RadioButton();
            this.Settings = new System.Windows.Forms.RadioButton();
            this.Tools = new System.Windows.Forms.RadioButton();
            this.Help = new System.Windows.Forms.RadioButton();
            this.NavBoardStatus = new System.Windows.Forms.TabControl();
            this.StatusTabPage = new System.Windows.Forms.TabPage();
            this.mbatL = new System.Windows.Forms.Label();
            this.sBatL = new System.Windows.Forms.Label();
            this.thrL = new System.Windows.Forms.Label();
            this.mBatProgreesbar = new System.Windows.Forms.ProgressBar();
            this.sBatProgressBar = new System.Windows.Forms.ProgressBar();
            this.throttleProgressBar = new System.Windows.Forms.ProgressBar();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.bankAngleL = new System.Windows.Forms.Label();
            this.airSpeedL = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.turnRateL = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.DistTONXPL = new System.Windows.Forms.Label();
            this.aglHeighL = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.altitudeL = new System.Windows.Forms.Label();
            this.verticalSpeedL = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.HeadingL = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.yawL = new System.Windows.Forms.Label();
            this.gpsTimeL = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.graphTabPage = new System.Windows.Forms.TabPage();
            this.graphDisplay = new ZedGraph.ZedGraphControl();
            this.flightContral = new System.Windows.Forms.TabPage();
            this.button8 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.vrsAcquire = new System.Windows.Forms.Button();
            this.transmitVRS = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.picRadButton = new System.Windows.Forms.RadioButton();
            this.button4 = new System.Windows.Forms.Button();
            this.radioButton1 = new System.Windows.Forms.Button();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton19 = new System.Windows.Forms.RadioButton();
            this.radioButton24 = new System.Windows.Forms.RadioButton();
            this.navigationBoard = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.AltNavLabel = new System.Windows.Forms.Label();
            this.AltNavDis = new System.Windows.Forms.Label();
            this.LongNavLabel = new System.Windows.Forms.Label();
            this.LongNavDis = new System.Windows.Forms.Label();
            this.packetsCountLabel = new System.Windows.Forms.Label();
            this.packetsCountDisplay = new System.Windows.Forms.Label();
            this.StNavLabel = new System.Windows.Forms.Label();
            this.modeNavLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.GPSNAVStatus = new System.Windows.Forms.Label();
            this.navLatLabel = new System.Windows.Forms.Label();
            this.NavLatdis = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.AHdropDownPanel = new System.Windows.Forms.TabPage();
            this.PatternDropDownPanel = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.simStatusTimer = new System.Windows.Forms.Timer(this.components);
            this.headingIndicatorInstrumentControl2 = new UAV_GUI.HeadingIndicatorInstrumentControl();
            this.turnCoordinatorInstrumentControl1 = new UAV_GUI.TurnCoordinatorInstrumentControl();
            this.verticalSpeedIndicatorInstrumentControl1 = new UAV_GUI.VerticalSpeedIndicatorInstrumentControl();
            this.airSpeedIndicatorInstrumentControl2 = new UAV_GUI.AirSpeedIndicatorInstrumentControl();
            this.attitudeIndicatorInstrumentControl2 = new UAV_GUI.AttitudeIndicatorInstrumentControl();
            this.altimeterInstrumentControl2 = new UAV_GUI.AltimeterInstrumentControl();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.NavBoardStatus.SuspendLayout();
            this.StatusTabPage.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.graphTabPage.SuspendLayout();
            this.flightContral.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.navigationBoard.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.AHdropDownPanel.SuspendLayout();
            this.PatternDropDownPanel.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Menu;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.flightToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(9, 10);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(184, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadFlyFileToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadFlyFileToolStripMenuItem
            // 
            this.loadFlyFileToolStripMenuItem.Name = "loadFlyFileToolStripMenuItem";
            this.loadFlyFileToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.loadFlyFileToolStripMenuItem.Text = "Load Fly File";
            this.loadFlyFileToolStripMenuItem.Click += new System.EventHandler(this.loadFlyFileToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // flightToolStripMenuItem
            // 
            this.flightToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.artificialHorizonToolStripMenuItem,
            this.statusPanelToolStripMenuItem,
            this.autopilotControlToolStripMenuItem,
            this.patternsToolStripMenuItem,
            this.graphDisplayToolStripMenuItem,
            this.flightControlToolStripMenuItem1,
            this.rCSensorMonitorToolStripMenuItem});
            this.flightToolStripMenuItem.Name = "flightToolStripMenuItem";
            this.flightToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.flightToolStripMenuItem.Text = "View ";
            // 
            // artificialHorizonToolStripMenuItem
            // 
            this.artificialHorizonToolStripMenuItem.Name = "artificialHorizonToolStripMenuItem";
            this.artificialHorizonToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.artificialHorizonToolStripMenuItem.Text = "Artificial GCS ";
            // 
            // statusPanelToolStripMenuItem
            // 
            this.statusPanelToolStripMenuItem.Name = "statusPanelToolStripMenuItem";
            this.statusPanelToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.statusPanelToolStripMenuItem.Text = "Status Panel ";
            this.statusPanelToolStripMenuItem.Click += new System.EventHandler(this.statusPanelToolStripMenuItem_Click);
            // 
            // autopilotControlToolStripMenuItem
            // 
            this.autopilotControlToolStripMenuItem.Name = "autopilotControlToolStripMenuItem";
            this.autopilotControlToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.autopilotControlToolStripMenuItem.Text = "Autopilot Control";
            this.autopilotControlToolStripMenuItem.Click += new System.EventHandler(this.autopilotControlToolStripMenuItem_Click);
            // 
            // patternsToolStripMenuItem
            // 
            this.patternsToolStripMenuItem.Name = "patternsToolStripMenuItem";
            this.patternsToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.patternsToolStripMenuItem.Text = "Patterns";
            this.patternsToolStripMenuItem.Click += new System.EventHandler(this.patternsToolStripMenuItem_Click);
            // 
            // graphDisplayToolStripMenuItem
            // 
            this.graphDisplayToolStripMenuItem.Name = "graphDisplayToolStripMenuItem";
            this.graphDisplayToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.graphDisplayToolStripMenuItem.Text = "Graph Display";
            this.graphDisplayToolStripMenuItem.Click += new System.EventHandler(this.graphDisplayToolStripMenuItem_Click);
            // 
            // flightControlToolStripMenuItem1
            // 
            this.flightControlToolStripMenuItem1.Name = "flightControlToolStripMenuItem1";
            this.flightControlToolStripMenuItem1.Size = new System.Drawing.Size(173, 22);
            this.flightControlToolStripMenuItem1.Text = "Flight Control";
            this.flightControlToolStripMenuItem1.Click += new System.EventHandler(this.flightControlToolStripMenuItem1_Click_1);
            // 
            // rCSensorMonitorToolStripMenuItem
            // 
            this.rCSensorMonitorToolStripMenuItem.Name = "rCSensorMonitorToolStripMenuItem";
            this.rCSensorMonitorToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.rCSensorMonitorToolStripMenuItem.Text = "RC Sensor Monitor";
            this.rCSensorMonitorToolStripMenuItem.Click += new System.EventHandler(this.rCSensorMonitorToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.automaticInitializationToolStripMenuItem,
            this.flightPlanToolStripMenuItem,
            this.setupToolStripMenuItem,
            this.flightMonitorToolStripMenuItem1,
            this.joyStickControllerToolStripMenuItem1,
            this.flyFileUtilityToolStripMenuItem,
            this.vRSFileUtilityToolStripMenuItem,
            this.testServosToolStripMenuItem,
            this.failurePatternsSimulationToolStripMenuItem,
            this.setVariableToolStripMenuItem,
            this.aGLMonitorToolStripMenuItem,
            this.cachingToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // automaticInitializationToolStripMenuItem
            // 
            this.automaticInitializationToolStripMenuItem.Name = "automaticInitializationToolStripMenuItem";
            this.automaticInitializationToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.automaticInitializationToolStripMenuItem.Text = "Automatic Initialization";
            this.automaticInitializationToolStripMenuItem.Click += new System.EventHandler(this.automaticInitializationToolStripMenuItem_Click);
            // 
            // flightPlanToolStripMenuItem
            // 
            this.flightPlanToolStripMenuItem.Name = "flightPlanToolStripMenuItem";
            this.flightPlanToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.flightPlanToolStripMenuItem.Text = "Flight Plan";
            this.flightPlanToolStripMenuItem.Click += new System.EventHandler(this.flightPlanToolStripMenuItem_Click);
            // 
            // setupToolStripMenuItem
            // 
            this.setupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generalSetupToolStripMenuItem,
            this.microPilotSetupToolStripMenuItem,
            this.groundSetupToolStripMenuItem,
            this.mIBSetupToolStripMenuItem});
            this.setupToolStripMenuItem.Name = "setupToolStripMenuItem";
            this.setupToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.setupToolStripMenuItem.Text = "Setup ";
            // 
            // generalSetupToolStripMenuItem
            // 
            this.generalSetupToolStripMenuItem.Name = "generalSetupToolStripMenuItem";
            this.generalSetupToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.generalSetupToolStripMenuItem.Text = "General Setup";
            this.generalSetupToolStripMenuItem.Click += new System.EventHandler(this.generalSetupToolStripMenuItem_Click);
            // 
            // microPilotSetupToolStripMenuItem
            // 
            this.microPilotSetupToolStripMenuItem.Name = "microPilotSetupToolStripMenuItem";
            this.microPilotSetupToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.microPilotSetupToolStripMenuItem.Text = "AutoPilot Setup";
            this.microPilotSetupToolStripMenuItem.Click += new System.EventHandler(this.microPilotSetupToolStripMenuItem_Click);
            // 
            // groundSetupToolStripMenuItem
            // 
            this.groundSetupToolStripMenuItem.Name = "groundSetupToolStripMenuItem";
            this.groundSetupToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.groundSetupToolStripMenuItem.Text = "Ground Setup";
            this.groundSetupToolStripMenuItem.Click += new System.EventHandler(this.groundSetupToolStripMenuItem_Click);
            // 
            // mIBSetupToolStripMenuItem
            // 
            this.mIBSetupToolStripMenuItem.Name = "mIBSetupToolStripMenuItem";
            this.mIBSetupToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.mIBSetupToolStripMenuItem.Text = "MIB Setup";
            this.mIBSetupToolStripMenuItem.Click += new System.EventHandler(this.mIBSetupToolStripMenuItem_Click);
            // 
            // flightMonitorToolStripMenuItem1
            // 
            this.flightMonitorToolStripMenuItem1.Name = "flightMonitorToolStripMenuItem1";
            this.flightMonitorToolStripMenuItem1.Size = new System.Drawing.Size(215, 22);
            this.flightMonitorToolStripMenuItem1.Text = "Flight Monitor";
            this.flightMonitorToolStripMenuItem1.Click += new System.EventHandler(this.flightMonitorToolStripMenuItem_Click);
            // 
            // joyStickControllerToolStripMenuItem1
            // 
            this.joyStickControllerToolStripMenuItem1.Name = "joyStickControllerToolStripMenuItem1";
            this.joyStickControllerToolStripMenuItem1.Size = new System.Drawing.Size(215, 22);
            this.joyStickControllerToolStripMenuItem1.Text = "JoyStick Controller";
            this.joyStickControllerToolStripMenuItem1.Click += new System.EventHandler(this.joystickControllerToolStripMenuItem_Click);
            // 
            // flyFileUtilityToolStripMenuItem
            // 
            this.flyFileUtilityToolStripMenuItem.Name = "flyFileUtilityToolStripMenuItem";
            this.flyFileUtilityToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.flyFileUtilityToolStripMenuItem.Text = "Fly File Utility";
            this.flyFileUtilityToolStripMenuItem.Click += new System.EventHandler(this.flyFileUtilityToolStripMenuItem_Click);
            // 
            // vRSFileUtilityToolStripMenuItem
            // 
            this.vRSFileUtilityToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.transmitToolStripMenuItem,
            this.acquireToolStripMenuItem});
            this.vRSFileUtilityToolStripMenuItem.Name = "vRSFileUtilityToolStripMenuItem";
            this.vRSFileUtilityToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.vRSFileUtilityToolStripMenuItem.Text = "VRS File Utility";
            // 
            // transmitToolStripMenuItem
            // 
            this.transmitToolStripMenuItem.Name = "transmitToolStripMenuItem";
            this.transmitToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.transmitToolStripMenuItem.Text = "Transmit";
            this.transmitToolStripMenuItem.Click += new System.EventHandler(this.transmitToolStripMenuItem_Click);
            // 
            // acquireToolStripMenuItem
            // 
            this.acquireToolStripMenuItem.Name = "acquireToolStripMenuItem";
            this.acquireToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.acquireToolStripMenuItem.Text = "Acquire";
            this.acquireToolStripMenuItem.Click += new System.EventHandler(this.acquireToolStripMenuItem_Click);
            // 
            // testServosToolStripMenuItem
            // 
            this.testServosToolStripMenuItem.Name = "testServosToolStripMenuItem";
            this.testServosToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.testServosToolStripMenuItem.Text = "Test Servos";
            this.testServosToolStripMenuItem.Click += new System.EventHandler(this.TestServo);
            // 
            // failurePatternsSimulationToolStripMenuItem
            // 
            this.failurePatternsSimulationToolStripMenuItem.Name = "failurePatternsSimulationToolStripMenuItem";
            this.failurePatternsSimulationToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.failurePatternsSimulationToolStripMenuItem.Text = "Failure Patterns Simulation";
            this.failurePatternsSimulationToolStripMenuItem.Click += new System.EventHandler(this.FlightPattern_Click);
            // 
            // setVariableToolStripMenuItem
            // 
            this.setVariableToolStripMenuItem.Name = "setVariableToolStripMenuItem";
            this.setVariableToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.setVariableToolStripMenuItem.Text = "Change Parameter";
            this.setVariableToolStripMenuItem.Click += new System.EventHandler(this.setVariableToolStripMenuItem_Click);
            // 
            // aGLMonitorToolStripMenuItem
            // 
            this.aGLMonitorToolStripMenuItem.Name = "aGLMonitorToolStripMenuItem";
            this.aGLMonitorToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.aGLMonitorToolStripMenuItem.Text = "AGL Monitor";
            this.aGLMonitorToolStripMenuItem.Click += new System.EventHandler(this.aGLMonitorToolStripMenuItem_Click);
            // 
            // cachingToolStripMenuItem
            // 
            this.cachingToolStripMenuItem.Name = "cachingToolStripMenuItem";
            this.cachingToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.cachingToolStripMenuItem.Text = "Caching";
            this.cachingToolStripMenuItem.Click += new System.EventHandler(this.cachingToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.aboutToolStripMenuItem.Text = "About ...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // navigationDataToolStripMenuItem
            // 
            this.navigationDataToolStripMenuItem.Name = "navigationDataToolStripMenuItem";
            this.navigationDataToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // serialPortToolStripMenuItem
            // 
            this.serialPortToolStripMenuItem.Name = "serialPortToolStripMenuItem";
            this.serialPortToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // cameraToolStripMenuItem
            // 
            this.cameraToolStripMenuItem.Name = "cameraToolStripMenuItem";
            this.cameraToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // flightMonitorToolStripMenuItem
            // 
            this.flightMonitorToolStripMenuItem.Name = "flightMonitorToolStripMenuItem";
            this.flightMonitorToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // joystickControllerToolStripMenuItem
            // 
            this.joystickControllerToolStripMenuItem.Name = "joystickControllerToolStripMenuItem";
            this.joystickControllerToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // trackBar1
            // 
            this.trackBar1.AutoSize = false;
            this.trackBar1.Location = new System.Drawing.Point(1212, 13);
            this.trackBar1.Maximum = 20;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBar1.Size = new System.Drawing.Size(25, 483);
            this.trackBar1.TabIndex = 20;
            this.trackBar1.Value = 14;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(153, 17);
            this.toolStripStatusLabel1.Text = "Distance to Next Way point ";
            // 
            // DTNWPTL
            // 
            this.DTNWPTL.Name = "DTNWPTL";
            this.DTNWPTL.Size = new System.Drawing.Size(28, 17);
            this.DTNWPTL.Text = "0.00";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(49, 17);
            this.toolStripStatusLabel3.Text = "Altitude";
            // 
            // AltitudeLL
            // 
            this.AltitudeLL.Name = "AltitudeLL";
            this.AltitudeLL.Size = new System.Drawing.Size(28, 17);
            this.AltitudeLL.Text = "0.00";
            // 
            // toolStripStatusLabel5
            // 
            this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
            this.toolStripStatusLabel5.Size = new System.Drawing.Size(52, 17);
            this.toolStripStatusLabel5.Text = "Heading";
            // 
            // headingStL
            // 
            this.headingStL.Name = "headingStL";
            this.headingStL.Size = new System.Drawing.Size(28, 17);
            this.headingStL.Text = "0.00";
            // 
            // toolStripStatusLabel7
            // 
            this.toolStripStatusLabel7.Name = "toolStripStatusLabel7";
            this.toolStripStatusLabel7.Size = new System.Drawing.Size(49, 17);
            this.toolStripStatusLabel7.Text = "Velocity";
            // 
            // velocityL
            // 
            this.velocityL.Name = "velocityL";
            this.velocityL.Size = new System.Drawing.Size(28, 17);
            this.velocityL.Text = "0.00";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.DTNWPTL,
            this.toolStripStatusLabel3,
            this.AltitudeLL,
            this.toolStripStatusLabel5,
            this.headingStL,
            this.toolStripStatusLabel7,
            this.velocityL,
            this.gpsLock,
            this.readySL,
            this.linkSPanel,
            this.rcStausL,
            this.failureStrip,
            this.fatalStrip});
            this.statusStrip1.Location = new System.Drawing.Point(0, 711);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1354, 22);
            this.statusStrip1.TabIndex = 17;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // gpsLock
            // 
            this.gpsLock.BackColor = System.Drawing.Color.Red;
            this.gpsLock.Name = "gpsLock";
            this.gpsLock.Size = new System.Drawing.Size(48, 17);
            this.gpsLock.Text = "NoLock";
            this.gpsLock.Click += new System.EventHandler(this.gpsLock_Click);
            // 
            // readySL
            // 
            this.readySL.Name = "readySL";
            this.readySL.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.readySL.Size = new System.Drawing.Size(39, 17);
            this.readySL.Text = "Ready";
            // 
            // linkSPanel
            // 
            this.linkSPanel.BackColor = System.Drawing.Color.Red;
            this.linkSPanel.Name = "linkSPanel";
            this.linkSPanel.Size = new System.Drawing.Size(79, 17);
            this.linkSPanel.Text = "Disconnected";
            // 
            // rcStausL
            // 
            this.rcStausL.Name = "rcStausL";
            this.rcStausL.Size = new System.Drawing.Size(27, 17);
            this.rcStausL.Text = "----";
            // 
            // failureStrip
            // 
            this.failureStrip.BackColor = System.Drawing.Color.Green;
            this.failureStrip.Name = "failureStrip";
            this.failureStrip.Size = new System.Drawing.Size(36, 17);
            this.failureStrip.Text = "None";
            this.failureStrip.Click += new System.EventHandler(this.toolStripStatusLabel2_Click);
            // 
            // fatalStrip
            // 
            this.fatalStrip.BackColor = System.Drawing.Color.Green;
            this.fatalStrip.Name = "fatalStrip";
            this.fatalStrip.Size = new System.Drawing.Size(87, 17);
            this.fatalStrip.Text = "No Fatal Errors ";
            this.fatalStrip.Click += new System.EventHandler(this.fatalStrip_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // flyFileLabel
            // 
            this.flyFileLabel.AutoSize = true;
            this.flyFileLabel.Location = new System.Drawing.Point(20, 104);
            this.flyFileLabel.Name = "flyFileLabel";
            this.flyFileLabel.Size = new System.Drawing.Size(79, 13);
            this.flyFileLabel.TabIndex = 11;
            this.flyFileLabel.Text = "No File Loaded";
            // 
            // arm_takeOff
            // 
            this.arm_takeOff.Enabled = false;
            this.arm_takeOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.arm_takeOff.Location = new System.Drawing.Point(299, 46);
            this.arm_takeOff.Name = "arm_takeOff";
            this.arm_takeOff.Size = new System.Drawing.Size(106, 48);
            this.arm_takeOff.TabIndex = 10;
            this.arm_takeOff.Text = "Arm";
            this.arm_takeOff.UseVisualStyleBackColor = true;
            this.arm_takeOff.Visible = false;
            this.arm_takeOff.Click += new System.EventHandler(this.arm_takeOff_Click);
            // 
            // connectButton
            // 
            this.connectButton.Enabled = false;
            this.connectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectButton.Image = ((System.Drawing.Image)(resources.GetObject("connectButton.Image")));
            this.connectButton.Location = new System.Drawing.Point(146, 45);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(130, 49);
            this.connectButton.TabIndex = 9;
            this.connectButton.Text = "Connect";
            this.connectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // isSimulator
            // 
            this.isSimulator.AutoSize = true;
            this.isSimulator.Location = new System.Drawing.Point(24, 11);
            this.isSimulator.Name = "isSimulator";
            this.isSimulator.Size = new System.Drawing.Size(69, 17);
            this.isSimulator.TabIndex = 8;
            this.isSimulator.Text = "Simulator";
            this.isSimulator.UseVisualStyleBackColor = true;
            this.isSimulator.CheckedChanged += new System.EventHandler(this.isSimulatorChech_Changed);
            // 
            // loadFileButton
            // 
            this.loadFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadFileButton.Image = ((System.Drawing.Image)(resources.GetObject("loadFileButton.Image")));
            this.loadFileButton.Location = new System.Drawing.Point(23, 45);
            this.loadFileButton.Name = "loadFileButton";
            this.loadFileButton.Size = new System.Drawing.Size(108, 49);
            this.loadFileButton.TabIndex = 7;
            this.loadFileButton.Text = "LoadFlyFile";
            this.loadFileButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.loadFileButton.UseVisualStyleBackColor = true;
            this.loadFileButton.Click += new System.EventHandler(this.LoadFlyFileB_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.figure8C, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.hoverC, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.landHereC, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.landHomeC, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.stopC, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.FlyHomeC, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.circleLeftC, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.circleRightC, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.takePictureC, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.orbitLeftC, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.orbitRight, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.DescC, 0, 1);
            this.tableLayoutPanel3.Enabled = false;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(777, 142);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // figure8C
            // 
            this.figure8C.AutoSize = true;
            this.figure8C.Enabled = false;
            this.figure8C.Location = new System.Drawing.Point(585, 97);
            this.figure8C.Name = "figure8C";
            this.figure8C.Size = new System.Drawing.Size(80, 20);
            this.figure8C.TabIndex = 14;
            this.figure8C.Text = "Figure8";
            this.figure8C.UseVisualStyleBackColor = true;
            this.figure8C.CheckedChanged += new System.EventHandler(this.patternCheckBox1_CheckedChanged_1);
            // 
            // hoverC
            // 
            this.hoverC.AutoSize = true;
            this.hoverC.Enabled = false;
            this.hoverC.Location = new System.Drawing.Point(3, 3);
            this.hoverC.Name = "hoverC";
            this.hoverC.Size = new System.Drawing.Size(106, 20);
            this.hoverC.TabIndex = 3;
            this.hoverC.Text = "Hover here";
            this.hoverC.UseVisualStyleBackColor = true;
            this.hoverC.CheckedChanged += new System.EventHandler(this.patternCheckBox1_CheckedChanged_1);
            // 
            // landHereC
            // 
            this.landHereC.AutoSize = true;
            this.landHereC.Enabled = false;
            this.landHereC.Location = new System.Drawing.Point(391, 97);
            this.landHereC.Name = "landHereC";
            this.landHereC.Size = new System.Drawing.Size(97, 20);
            this.landHereC.TabIndex = 13;
            this.landHereC.Text = "Land here";
            this.landHereC.UseVisualStyleBackColor = true;
            this.landHereC.CheckedChanged += new System.EventHandler(this.patternCheckBox1_CheckedChanged_1);
            // 
            // landHomeC
            // 
            this.landHomeC.AutoSize = true;
            this.landHomeC.Enabled = false;
            this.landHomeC.Location = new System.Drawing.Point(197, 97);
            this.landHomeC.Name = "landHomeC";
            this.landHomeC.Size = new System.Drawing.Size(121, 20);
            this.landHomeC.TabIndex = 12;
            this.landHomeC.Text = "Land at Home";
            this.landHomeC.UseVisualStyleBackColor = true;
            this.landHomeC.CheckedChanged += new System.EventHandler(this.patternCheckBox1_CheckedChanged_1);
            // 
            // stopC
            // 
            this.stopC.AutoSize = true;
            this.stopC.Enabled = false;
            this.stopC.Location = new System.Drawing.Point(3, 97);
            this.stopC.Name = "stopC";
            this.stopC.Size = new System.Drawing.Size(111, 20);
            this.stopC.TabIndex = 11;
            this.stopC.Text = "Stop Engine";
            this.stopC.UseVisualStyleBackColor = true;
            this.stopC.CheckedChanged += new System.EventHandler(this.patternCheckBox1_CheckedChanged_1);
            // 
            // FlyHomeC
            // 
            this.FlyHomeC.AutoSize = true;
            this.FlyHomeC.Enabled = false;
            this.FlyHomeC.Location = new System.Drawing.Point(585, 50);
            this.FlyHomeC.Margin = new System.Windows.Forms.Padding(3, 3, 1, 3);
            this.FlyHomeC.Name = "FlyHomeC";
            this.FlyHomeC.Size = new System.Drawing.Size(90, 20);
            this.FlyHomeC.TabIndex = 10;
            this.FlyHomeC.Text = "Fly Home";
            this.FlyHomeC.UseVisualStyleBackColor = true;
            this.FlyHomeC.CheckedChanged += new System.EventHandler(this.patternCheckBox1_CheckedChanged_1);
            // 
            // circleLeftC
            // 
            this.circleLeftC.AutoSize = true;
            this.circleLeftC.Enabled = false;
            this.circleLeftC.Location = new System.Drawing.Point(391, 50);
            this.circleLeftC.Name = "circleLeftC";
            this.circleLeftC.Size = new System.Drawing.Size(94, 20);
            this.circleLeftC.TabIndex = 9;
            this.circleLeftC.Text = "Circle left";
            this.circleLeftC.UseVisualStyleBackColor = true;
            this.circleLeftC.CheckedChanged += new System.EventHandler(this.patternCheckBox1_CheckedChanged_1);
            // 
            // circleRightC
            // 
            this.circleRightC.AutoSize = true;
            this.circleRightC.Enabled = false;
            this.circleRightC.Location = new System.Drawing.Point(197, 50);
            this.circleRightC.Name = "circleRightC";
            this.circleRightC.Size = new System.Drawing.Size(108, 20);
            this.circleRightC.TabIndex = 8;
            this.circleRightC.Text = "Circle Right";
            this.circleRightC.UseVisualStyleBackColor = true;
            this.circleRightC.CheckedChanged += new System.EventHandler(this.patternCheckBox1_CheckedChanged_1);
            // 
            // takePictureC
            // 
            this.takePictureC.AutoSize = true;
            this.takePictureC.Enabled = false;
            this.takePictureC.Location = new System.Drawing.Point(585, 3);
            this.takePictureC.Name = "takePictureC";
            this.takePictureC.Size = new System.Drawing.Size(112, 20);
            this.takePictureC.TabIndex = 6;
            this.takePictureC.Text = "Take Picture";
            this.takePictureC.UseVisualStyleBackColor = true;
            this.takePictureC.CheckedChanged += new System.EventHandler(this.patternCheckBox1_CheckedChanged_1);
            // 
            // orbitLeftC
            // 
            this.orbitLeftC.AutoSize = true;
            this.orbitLeftC.Enabled = false;
            this.orbitLeftC.Location = new System.Drawing.Point(391, 3);
            this.orbitLeftC.Name = "orbitLeftC";
            this.orbitLeftC.Size = new System.Drawing.Size(88, 20);
            this.orbitLeftC.TabIndex = 5;
            this.orbitLeftC.Text = "Orbit left";
            this.orbitLeftC.UseVisualStyleBackColor = true;
            this.orbitLeftC.CheckedChanged += new System.EventHandler(this.patternCheckBox1_CheckedChanged_1);
            // 
            // orbitRight
            // 
            this.orbitRight.AutoSize = true;
            this.orbitRight.Enabled = false;
            this.orbitRight.Location = new System.Drawing.Point(197, 3);
            this.orbitRight.Name = "orbitRight";
            this.orbitRight.Size = new System.Drawing.Size(102, 20);
            this.orbitRight.TabIndex = 4;
            this.orbitRight.Text = "Orbit Right";
            this.orbitRight.UseVisualStyleBackColor = true;
            this.orbitRight.CheckedChanged += new System.EventHandler(this.patternCheckBox1_CheckedChanged_1);
            // 
            // DescC
            // 
            this.DescC.AutoSize = true;
            this.DescC.Enabled = false;
            this.DescC.Location = new System.Drawing.Point(3, 50);
            this.DescC.Name = "DescC";
            this.DescC.Size = new System.Drawing.Size(122, 20);
            this.DescC.TabIndex = 7;
            this.DescC.Text = "Descent Here";
            this.DescC.UseVisualStyleBackColor = true;
            this.DescC.CheckedChanged += new System.EventHandler(this.patternCheckBox1_CheckedChanged_1);
            // 
            // seekingTimer
            // 
            this.seekingTimer.Tick += new System.EventHandler(this.seekingTimer_Tick);
            // 
            // apStatusTimer
            // 
            this.apStatusTimer.Tick += new System.EventHandler(this.apStatusTimer_Tick);
            // 
            // apStatus2Timer
            // 
            this.apStatus2Timer.Tick += new System.EventHandler(this.apStatus2Timer_Tick);
            // 
            // bindGaugesTimer
            // 
            this.bindGaugesTimer.Tick += new System.EventHandler(this.bindGaugesTimer_Tick);
            // 
            // graphTimer
            // 
            this.graphTimer.Tick += new System.EventHandler(this.graphTimer_Tick);
            // 
            // labelsTimer
            // 
            this.labelsTimer.Tick += new System.EventHandler(this.labelsTimer_Tick);
            // 
            // updateMapTimer
            // 
            this.updateMapTimer.Tick += new System.EventHandler(this.updateMapTimer_Tick1);
            // 
            // FlightData
            // 
            this.FlightData.Appearance = System.Windows.Forms.Appearance.Button;
            this.FlightData.BackColor = System.Drawing.SystemColors.Control;
            this.FlightData.FlatAppearance.BorderSize = 0;
            this.FlightData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FlightData.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.FlightData.Location = new System.Drawing.Point(5, 21);
            this.FlightData.Name = "FlightData";
            this.FlightData.Size = new System.Drawing.Size(90, 104);
            this.FlightData.TabIndex = 30;
            this.FlightData.Text = "Flight Data";
            this.FlightData.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.FlightData.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.FlightData.UseVisualStyleBackColor = false;
            this.FlightData.CheckedChanged += new System.EventHandler(this.FlightData_CheckedChanged);
            // 
            // FlightPlan
            // 
            this.FlightPlan.Appearance = System.Windows.Forms.Appearance.Button;
            this.FlightPlan.FlatAppearance.BorderSize = 0;
            this.FlightPlan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FlightPlan.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.FlightPlan.Location = new System.Drawing.Point(5, 160);
            this.FlightPlan.Name = "FlightPlan";
            this.FlightPlan.Size = new System.Drawing.Size(90, 104);
            this.FlightPlan.TabIndex = 31;
            this.FlightPlan.Text = "Flight Plan";
            this.FlightPlan.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.FlightPlan.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.FlightPlan.UseVisualStyleBackColor = true;
            this.FlightPlan.CheckedChanged += new System.EventHandler(this.FlightPlan_CheckedChanged);
            // 
            // Settings
            // 
            this.Settings.Appearance = System.Windows.Forms.Appearance.Button;
            this.Settings.FlatAppearance.BorderSize = 0;
            this.Settings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Settings.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Settings.Location = new System.Drawing.Point(5, 300);
            this.Settings.Name = "Settings";
            this.Settings.Size = new System.Drawing.Size(90, 104);
            this.Settings.TabIndex = 33;
            this.Settings.Text = "Settings";
            this.Settings.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Settings.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.Settings.UseVisualStyleBackColor = true;
            this.Settings.CheckedChanged += new System.EventHandler(this.Settings_CheckedChanged);
            // 
            // Tools
            // 
            this.Tools.Appearance = System.Windows.Forms.Appearance.Button;
            this.Tools.FlatAppearance.BorderSize = 0;
            this.Tools.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Tools.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Tools.Location = new System.Drawing.Point(5, 431);
            this.Tools.Name = "Tools";
            this.Tools.Size = new System.Drawing.Size(90, 104);
            this.Tools.TabIndex = 34;
            this.Tools.Text = "Tools";
            this.Tools.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Tools.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.Tools.UseVisualStyleBackColor = true;
            this.Tools.CheckedChanged += new System.EventHandler(this.Tools_CheckedChanged);
            // 
            // Help
            // 
            this.Help.Appearance = System.Windows.Forms.Appearance.Button;
            this.Help.FlatAppearance.BorderSize = 0;
            this.Help.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Help.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Help.Location = new System.Drawing.Point(5, 563);
            this.Help.Name = "Help";
            this.Help.Size = new System.Drawing.Size(90, 120);
            this.Help.TabIndex = 35;
            this.Help.Text = "Help";
            this.Help.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Help.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.Help.UseVisualStyleBackColor = true;
            this.Help.CheckedChanged += new System.EventHandler(this.Help_CheckedChanged);
            // 
            // NavBoardStatus
            // 
            this.NavBoardStatus.Controls.Add(this.StatusTabPage);
            this.NavBoardStatus.Controls.Add(this.graphTabPage);
            this.NavBoardStatus.Controls.Add(this.flightContral);
            this.NavBoardStatus.Controls.Add(this.navigationBoard);
            this.NavBoardStatus.Controls.Add(this.AHdropDownPanel);
            this.NavBoardStatus.Controls.Add(this.PatternDropDownPanel);
            this.NavBoardStatus.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.NavBoardStatus.Location = new System.Drawing.Point(0, 496);
            this.NavBoardStatus.Name = "NavBoardStatus";
            this.NavBoardStatus.SelectedIndex = 0;
            this.NavBoardStatus.Size = new System.Drawing.Size(792, 177);
            this.NavBoardStatus.TabIndex = 3;
            // 
            // StatusTabPage
            // 
            this.StatusTabPage.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.StatusTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.StatusTabPage.Controls.Add(this.mbatL);
            this.StatusTabPage.Controls.Add(this.sBatL);
            this.StatusTabPage.Controls.Add(this.thrL);
            this.StatusTabPage.Controls.Add(this.mBatProgreesbar);
            this.StatusTabPage.Controls.Add(this.sBatProgressBar);
            this.StatusTabPage.Controls.Add(this.throttleProgressBar);
            this.StatusTabPage.Controls.Add(this.tableLayoutPanel1);
            this.StatusTabPage.Location = new System.Drawing.Point(4, 25);
            this.StatusTabPage.Name = "StatusTabPage";
            this.StatusTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.StatusTabPage.Size = new System.Drawing.Size(784, 148);
            this.StatusTabPage.TabIndex = 0;
            this.StatusTabPage.Text = "Status";
            this.StatusTabPage.Click += new System.EventHandler(this.StatusTabPage_Click);
            // 
            // mbatL
            // 
            this.mbatL.AutoSize = true;
            this.mbatL.BackColor = System.Drawing.SystemColors.Control;
            this.mbatL.Location = new System.Drawing.Point(586, 3);
            this.mbatL.Name = "mbatL";
            this.mbatL.Size = new System.Drawing.Size(144, 16);
            this.mbatL.TabIndex = 34;
            this.mbatL.Text = "Main Battery (0.0 %)";
            // 
            // sBatL
            // 
            this.sBatL.AutoSize = true;
            this.sBatL.BackColor = System.Drawing.SystemColors.Control;
            this.sBatL.Location = new System.Drawing.Point(577, 53);
            this.sBatL.Name = "sBatL";
            this.sBatL.Size = new System.Drawing.Size(152, 16);
            this.sBatL.TabIndex = 33;
            this.sBatL.Text = "Servo Battery (0.0 %)";
            // 
            // thrL
            // 
            this.thrL.AutoSize = true;
            this.thrL.BackColor = System.Drawing.SystemColors.Control;
            this.thrL.Location = new System.Drawing.Point(603, 102);
            this.thrL.Name = "thrL";
            this.thrL.Size = new System.Drawing.Size(111, 16);
            this.thrL.TabIndex = 32;
            this.thrL.Text = "Throttle (0.0 %)";
            // 
            // mBatProgreesbar
            // 
            this.mBatProgreesbar.Location = new System.Drawing.Point(565, 21);
            this.mBatProgreesbar.Name = "mBatProgreesbar";
            this.mBatProgreesbar.Size = new System.Drawing.Size(175, 25);
            this.mBatProgreesbar.TabIndex = 31;
            // 
            // sBatProgressBar
            // 
            this.sBatProgressBar.Location = new System.Drawing.Point(565, 69);
            this.sBatProgressBar.Name = "sBatProgressBar";
            this.sBatProgressBar.Size = new System.Drawing.Size(175, 25);
            this.sBatProgressBar.TabIndex = 30;
            // 
            // throttleProgressBar
            // 
            this.throttleProgressBar.Location = new System.Drawing.Point(565, 118);
            this.throttleProgressBar.Name = "throttleProgressBar";
            this.throttleProgressBar.Size = new System.Drawing.Size(175, 25);
            this.throttleProgressBar.TabIndex = 29;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.bankAngleL, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.airSpeedL, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.label29, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.turnRateL, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label28, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label32, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.DistTONXPL, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.aglHeighL, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.altitudeL, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.verticalSpeedL, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.HeadingL, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.yawL, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.gpsTimeL, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label24, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.label8, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 3);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(19, 20);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(499, 106);
            this.tableLayoutPanel1.TabIndex = 13;
            // 
            // bankAngleL
            // 
            this.bankAngleL.AutoSize = true;
            this.bankAngleL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bankAngleL.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bankAngleL.Location = new System.Drawing.Point(400, 113);
            this.bankAngleL.Name = "bankAngleL";
            this.bankAngleL.Size = new System.Drawing.Size(27, 13);
            this.bankAngleL.TabIndex = 26;
            this.bankAngleL.Text = "N/A";
            this.bankAngleL.Visible = false;
            // 
            // airSpeedL
            // 
            this.airSpeedL.AutoSize = true;
            this.airSpeedL.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.airSpeedL.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.airSpeedL.Location = new System.Drawing.Point(400, 55);
            this.airSpeedL.Name = "airSpeedL";
            this.airSpeedL.Size = new System.Drawing.Size(36, 16);
            this.airSpeedL.TabIndex = 26;
            this.airSpeedL.Text = "0.00";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(276, 113);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(89, 13);
            this.label29.TabIndex = 24;
            this.label29.Text = "Bank Angle (deg)";
            this.label29.Visible = false;
            // 
            // turnRateL
            // 
            this.turnRateL.AutoSize = true;
            this.turnRateL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.turnRateL.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.turnRateL.Location = new System.Drawing.Point(177, 113);
            this.turnRateL.Name = "turnRateL";
            this.turnRateL.Size = new System.Drawing.Size(27, 13);
            this.turnRateL.TabIndex = 25;
            this.turnRateL.Text = "N/A";
            this.turnRateL.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Altitude(m)      ";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(276, 55);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(110, 16);
            this.label28.TabIndex = 25;
            this.label28.Text = "Air Speed(m/s)";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(3, 113);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(55, 13);
            this.label32.TabIndex = 23;
            this.label32.Text = "Turn Rate";
            this.label32.Visible = false;
            // 
            // DistTONXPL
            // 
            this.DistTONXPL.AutoSize = true;
            this.DistTONXPL.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.DistTONXPL.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.DistTONXPL.Location = new System.Drawing.Point(400, 27);
            this.DistTONXPL.Name = "DistTONXPL";
            this.DistTONXPL.Size = new System.Drawing.Size(36, 16);
            this.DistTONXPL.TabIndex = 10;
            this.DistTONXPL.Text = "0.00";
            // 
            // aglHeighL
            // 
            this.aglHeighL.AutoSize = true;
            this.aglHeighL.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.aglHeighL.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.aglHeighL.Location = new System.Drawing.Point(177, 55);
            this.aglHeighL.Name = "aglHeighL";
            this.aglHeighL.Size = new System.Drawing.Size(36, 16);
            this.aglHeighL.TabIndex = 12;
            this.aglHeighL.Text = "0.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(276, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Dist to Wp (m)";
            // 
            // altitudeL
            // 
            this.altitudeL.AutoSize = true;
            this.altitudeL.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.altitudeL.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.altitudeL.Location = new System.Drawing.Point(177, 0);
            this.altitudeL.Name = "altitudeL";
            this.altitudeL.Size = new System.Drawing.Size(36, 16);
            this.altitudeL.TabIndex = 6;
            this.altitudeL.Text = "0.00";
            // 
            // verticalSpeedL
            // 
            this.verticalSpeedL.AutoSize = true;
            this.verticalSpeedL.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.verticalSpeedL.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.verticalSpeedL.Location = new System.Drawing.Point(177, 84);
            this.verticalSpeedL.Name = "verticalSpeedL";
            this.verticalSpeedL.Size = new System.Drawing.Size(31, 16);
            this.verticalSpeedL.TabIndex = 9;
            this.verticalSpeedL.Text = "N/A";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(3, 27);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(107, 16);
            this.label14.TabIndex = 5;
            this.label14.Text = "Heading (deg)";
            // 
            // HeadingL
            // 
            this.HeadingL.AutoSize = true;
            this.HeadingL.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.HeadingL.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.HeadingL.Location = new System.Drawing.Point(177, 27);
            this.HeadingL.Name = "HeadingL";
            this.HeadingL.Size = new System.Drawing.Size(36, 16);
            this.HeadingL.TabIndex = 8;
            this.HeadingL.Text = "0.00";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(3, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 16);
            this.label13.TabIndex = 4;
            this.label13.Text = "AGL Height";
            // 
            // yawL
            // 
            this.yawL.AutoSize = true;
            this.yawL.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.yawL.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.yawL.Location = new System.Drawing.Point(400, 0);
            this.yawL.Name = "yawL";
            this.yawL.Size = new System.Drawing.Size(36, 16);
            this.yawL.TabIndex = 11;
            this.yawL.Text = "0.00";
            // 
            // gpsTimeL
            // 
            this.gpsTimeL.AutoSize = true;
            this.gpsTimeL.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.gpsTimeL.Location = new System.Drawing.Point(276, 84);
            this.gpsTimeL.Name = "gpsTimeL";
            this.gpsTimeL.Size = new System.Drawing.Size(74, 16);
            this.gpsTimeL.TabIndex = 27;
            this.gpsTimeL.Text = "GPS Time";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label24.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label24.Location = new System.Drawing.Point(400, 84);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(31, 16);
            this.label24.TabIndex = 28;
            this.label24.Text = "N/A";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(276, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "Yaw(deg)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(3, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(143, 16);
            this.label9.TabIndex = 3;
            this.label9.Text = "Vertical Speed(m/s)";
            // 
            // graphTabPage
            // 
            this.graphTabPage.BackColor = System.Drawing.Color.LightGray;
            this.graphTabPage.Controls.Add(this.graphDisplay);
            this.graphTabPage.Location = new System.Drawing.Point(4, 25);
            this.graphTabPage.Name = "graphTabPage";
            this.graphTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.graphTabPage.Size = new System.Drawing.Size(784, 148);
            this.graphTabPage.TabIndex = 1;
            this.graphTabPage.Text = "Graph Display";
            // 
            // graphDisplay
            // 
            this.graphDisplay.Location = new System.Drawing.Point(1, 1);
            this.graphDisplay.Name = "graphDisplay";
            this.graphDisplay.ScrollGrace = 0D;
            this.graphDisplay.ScrollMaxX = 0D;
            this.graphDisplay.ScrollMaxY = 0D;
            this.graphDisplay.ScrollMaxY2 = 0D;
            this.graphDisplay.ScrollMinX = 0D;
            this.graphDisplay.ScrollMinY = 0D;
            this.graphDisplay.ScrollMinY2 = 0D;
            this.graphDisplay.Size = new System.Drawing.Size(783, 147);
            this.graphDisplay.TabIndex = 0;
            // 
            // flightContral
            // 
            this.flightContral.Controls.Add(this.button8);
            this.flightContral.Controls.Add(this.button1);
            this.flightContral.Controls.Add(this.vrsAcquire);
            this.flightContral.Controls.Add(this.transmitVRS);
            this.flightContral.Controls.Add(this.button6);
            this.flightContral.Controls.Add(this.button5);
            this.flightContral.Controls.Add(this.button3);
            this.flightContral.Controls.Add(this.button2);
            this.flightContral.Controls.Add(this.groupBox3);
            this.flightContral.Controls.Add(this.groupBox1);
            this.flightContral.Location = new System.Drawing.Point(4, 25);
            this.flightContral.Name = "flightContral";
            this.flightContral.Padding = new System.Windows.Forms.Padding(3);
            this.flightContral.Size = new System.Drawing.Size(784, 148);
            this.flightContral.TabIndex = 2;
            this.flightContral.Text = "Flight Control ";
            this.flightContral.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(1023, 82);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(144, 37);
            this.button8.TabIndex = 32;
            this.button8.Text = "TestServo";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(582, 18);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(189, 31);
            this.button1.TabIndex = 31;
            this.button1.Text = "Flight Pattern Simulation";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.FailurePatterns_Click);
            // 
            // vrsAcquire
            // 
            this.vrsAcquire.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vrsAcquire.Location = new System.Drawing.Point(1023, 37);
            this.vrsAcquire.Name = "vrsAcquire";
            this.vrsAcquire.Size = new System.Drawing.Size(144, 39);
            this.vrsAcquire.TabIndex = 30;
            this.vrsAcquire.Text = "Get VRS File";
            this.vrsAcquire.UseVisualStyleBackColor = true;
            // 
            // transmitVRS
            // 
            this.transmitVRS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transmitVRS.Location = new System.Drawing.Point(596, 95);
            this.transmitVRS.Name = "transmitVRS";
            this.transmitVRS.Size = new System.Drawing.Size(144, 33);
            this.transmitVRS.TabIndex = 29;
            this.transmitVRS.Text = "Transmit VRS File";
            this.transmitVRS.UseVisualStyleBackColor = true;
            this.transmitVRS.Click += new System.EventHandler(this.transmitToolStripMenuItem_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(596, 54);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(144, 33);
            this.button6.TabIndex = 28;
            this.button6.Text = "Fly File Utility";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.flyFileUtilityToolStripMenuItem_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(375, 56);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(140, 33);
            this.button5.TabIndex = 27;
            this.button5.Text = "Ground Setup";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.GNDSetup_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(375, 93);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(140, 33);
            this.button3.TabIndex = 26;
            this.button3.Text = "Flight Monitor";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Monitor_click);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(375, 18);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(140, 31);
            this.button2.TabIndex = 25;
            this.button2.Text = "Joystick";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.picRadButton);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.radioButton1);
            this.groupBox3.Controls.Add(this.radioButton2);
            this.groupBox3.Controls.Add(this.radioButton3);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(6, 10);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(271, 121);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Flight Mode";
            // 
            // picRadButton
            // 
            this.picRadButton.AutoSize = true;
            this.picRadButton.Location = new System.Drawing.Point(22, 83);
            this.picRadButton.Name = "picRadButton";
            this.picRadButton.Size = new System.Drawing.Size(45, 17);
            this.picRadButton.TabIndex = 4;
            this.picRadButton.Text = "PIC";
            this.picRadButton.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(380, 14);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Set Mode";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(135, 47);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(104, 31);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.Text = "Switch Mode";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.Click += new System.EventHandler(this.radioButton1_Click);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(22, 54);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(50, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "RPV";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(22, 24);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(50, 17);
            this.radioButton3.TabIndex = 0;
            this.radioButton3.Text = "UAV";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton19);
            this.groupBox1.Controls.Add(this.radioButton24);
            this.groupBox1.Location = new System.Drawing.Point(387, -84);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(151, 72);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mode";
            // 
            // radioButton19
            // 
            this.radioButton19.AutoSize = true;
            this.radioButton19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton19.Location = new System.Drawing.Point(6, 42);
            this.radioButton19.Name = "radioButton19";
            this.radioButton19.Size = new System.Drawing.Size(66, 17);
            this.radioButton19.TabIndex = 5;
            this.radioButton19.TabStop = true;
            this.radioButton19.Text = "Manual";
            this.radioButton19.UseVisualStyleBackColor = true;
            // 
            // radioButton24
            // 
            this.radioButton24.AutoSize = true;
            this.radioButton24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton24.Location = new System.Drawing.Point(6, 19);
            this.radioButton24.Name = "radioButton24";
            this.radioButton24.Size = new System.Drawing.Size(81, 17);
            this.radioButton24.TabIndex = 0;
            this.radioButton24.TabStop = true;
            this.radioButton24.Text = "Automatic";
            this.radioButton24.UseVisualStyleBackColor = true;
            // 
            // navigationBoard
            // 
            this.navigationBoard.Controls.Add(this.tableLayoutPanel2);
            this.navigationBoard.Location = new System.Drawing.Point(4, 25);
            this.navigationBoard.Name = "navigationBoard";
            this.navigationBoard.Padding = new System.Windows.Forms.Padding(3);
            this.navigationBoard.Size = new System.Drawing.Size(784, 148);
            this.navigationBoard.TabIndex = 3;
            this.navigationBoard.Text = "  Navigation Board";
            this.navigationBoard.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Controls.Add(this.AltNavLabel, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.AltNavDis, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.LongNavLabel, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.LongNavDis, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.packetsCountLabel, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.packetsCountDisplay, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.StNavLabel, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.modeNavLabel, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.label10, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.GPSNAVStatus, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.navLatLabel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.NavLatdis, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label4, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.label5, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.label12, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.label7, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.label6, 4, 2);
            this.tableLayoutPanel2.Controls.Add(this.label11, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.label20, 4, 4);
            this.tableLayoutPanel2.Controls.Add(this.label15, 4, 3);
            this.tableLayoutPanel2.Controls.Add(this.label19, 3, 4);
            this.tableLayoutPanel2.Controls.Add(this.label16, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label17, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.label18, 2, 4);
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(771, 155);
            this.tableLayoutPanel2.TabIndex = 14;
            // 
            // AltNavLabel
            // 
            this.AltNavLabel.AutoSize = true;
            this.AltNavLabel.Location = new System.Drawing.Point(3, 27);
            this.AltNavLabel.Name = "AltNavLabel";
            this.AltNavLabel.Size = new System.Drawing.Size(62, 16);
            this.AltNavLabel.TabIndex = 35;
            this.AltNavLabel.Text = "Altitude";
            // 
            // AltNavDis
            // 
            this.AltNavDis.AutoSize = true;
            this.AltNavDis.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.AltNavDis.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.AltNavDis.Location = new System.Drawing.Point(157, 27);
            this.AltNavDis.Name = "AltNavDis";
            this.AltNavDis.Size = new System.Drawing.Size(36, 16);
            this.AltNavDis.TabIndex = 36;
            this.AltNavDis.Text = "0.00";
            // 
            // LongNavLabel
            // 
            this.LongNavLabel.AutoSize = true;
            this.LongNavLabel.Location = new System.Drawing.Point(3, 55);
            this.LongNavLabel.Name = "LongNavLabel";
            this.LongNavLabel.Size = new System.Drawing.Size(79, 16);
            this.LongNavLabel.TabIndex = 33;
            this.LongNavLabel.Text = "Longitude";
            // 
            // LongNavDis
            // 
            this.LongNavDis.AutoSize = true;
            this.LongNavDis.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.LongNavDis.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.LongNavDis.Location = new System.Drawing.Point(157, 55);
            this.LongNavDis.Name = "LongNavDis";
            this.LongNavDis.Size = new System.Drawing.Size(36, 16);
            this.LongNavDis.TabIndex = 34;
            this.LongNavDis.Text = "0.00";
            // 
            // packetsCountLabel
            // 
            this.packetsCountLabel.AutoSize = true;
            this.packetsCountLabel.Location = new System.Drawing.Point(311, 0);
            this.packetsCountLabel.Name = "packetsCountLabel";
            this.packetsCountLabel.Size = new System.Drawing.Size(109, 16);
            this.packetsCountLabel.TabIndex = 31;
            this.packetsCountLabel.Text = "Packets Count";
            // 
            // packetsCountDisplay
            // 
            this.packetsCountDisplay.AutoSize = true;
            this.packetsCountDisplay.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.packetsCountDisplay.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.packetsCountDisplay.Location = new System.Drawing.Point(465, 0);
            this.packetsCountDisplay.Name = "packetsCountDisplay";
            this.packetsCountDisplay.Size = new System.Drawing.Size(36, 16);
            this.packetsCountDisplay.TabIndex = 32;
            this.packetsCountDisplay.Text = "0.00";
            // 
            // StNavLabel
            // 
            this.StNavLabel.AutoSize = true;
            this.StNavLabel.Location = new System.Drawing.Point(311, 55);
            this.StNavLabel.Name = "StNavLabel";
            this.StNavLabel.Size = new System.Drawing.Size(127, 16);
            this.StNavLabel.TabIndex = 23;
            this.StNavLabel.Text = "Navigation Mode";
            // 
            // modeNavLabel
            // 
            this.modeNavLabel.AutoSize = true;
            this.modeNavLabel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.modeNavLabel.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.modeNavLabel.Location = new System.Drawing.Point(465, 55);
            this.modeNavLabel.Name = "modeNavLabel";
            this.modeNavLabel.Size = new System.Drawing.Size(36, 16);
            this.modeNavLabel.TabIndex = 24;
            this.modeNavLabel.Text = "0.00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(311, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 16);
            this.label10.TabIndex = 11;
            this.label10.Text = "GPS Status";
            // 
            // GPSNAVStatus
            // 
            this.GPSNAVStatus.AutoSize = true;
            this.GPSNAVStatus.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.GPSNAVStatus.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.GPSNAVStatus.Location = new System.Drawing.Point(465, 27);
            this.GPSNAVStatus.Name = "GPSNAVStatus";
            this.GPSNAVStatus.Size = new System.Drawing.Size(36, 16);
            this.GPSNAVStatus.TabIndex = 12;
            this.GPSNAVStatus.Text = "0.00";
            // 
            // navLatLabel
            // 
            this.navLatLabel.AutoSize = true;
            this.navLatLabel.Location = new System.Drawing.Point(3, 0);
            this.navLatLabel.Name = "navLatLabel";
            this.navLatLabel.Size = new System.Drawing.Size(65, 16);
            this.navLatLabel.TabIndex = 29;
            this.navLatLabel.Text = "Latitude";
            // 
            // NavLatdis
            // 
            this.NavLatdis.AutoSize = true;
            this.NavLatdis.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.NavLatdis.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.NavLatdis.Location = new System.Drawing.Point(157, 0);
            this.NavLatdis.Name = "NavLatdis";
            this.NavLatdis.Size = new System.Drawing.Size(36, 16);
            this.NavLatdis.TabIndex = 30;
            this.NavLatdis.Text = "0.00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 16);
            this.label3.TabIndex = 37;
            this.label3.Text = "0.00";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(157, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 16);
            this.label4.TabIndex = 38;
            this.label4.Text = "0.00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(311, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 16);
            this.label5.TabIndex = 39;
            this.label5.Text = "0.00";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(619, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 16);
            this.label12.TabIndex = 43;
            this.label12.Text = "0.00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(619, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 16);
            this.label7.TabIndex = 41;
            this.label7.Text = "0.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(619, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 16);
            this.label6.TabIndex = 40;
            this.label6.Text = "0.00";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(465, 84);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 16);
            this.label11.TabIndex = 42;
            this.label11.Text = "0.00";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(619, 113);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(36, 16);
            this.label20.TabIndex = 49;
            this.label20.Text = "0.00";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(619, 84);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 16);
            this.label15.TabIndex = 44;
            this.label15.Text = "0.00";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(465, 113);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 16);
            this.label19.TabIndex = 48;
            this.label19.Text = "0.00";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 113);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 16);
            this.label16.TabIndex = 45;
            this.label16.Text = "0.00";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(157, 113);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 16);
            this.label17.TabIndex = 46;
            this.label17.Text = "0.00";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(311, 113);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(36, 16);
            this.label18.TabIndex = 47;
            this.label18.Text = "0.00";
            // 
            // AHdropDownPanel
            // 
            this.AHdropDownPanel.Controls.Add(this.headingIndicatorInstrumentControl2);
            this.AHdropDownPanel.Controls.Add(this.turnCoordinatorInstrumentControl1);
            this.AHdropDownPanel.Controls.Add(this.verticalSpeedIndicatorInstrumentControl1);
            this.AHdropDownPanel.Controls.Add(this.airSpeedIndicatorInstrumentControl2);
            this.AHdropDownPanel.Controls.Add(this.attitudeIndicatorInstrumentControl2);
            this.AHdropDownPanel.Controls.Add(this.altimeterInstrumentControl2);
            this.AHdropDownPanel.Location = new System.Drawing.Point(4, 25);
            this.AHdropDownPanel.Name = "AHdropDownPanel";
            this.AHdropDownPanel.Size = new System.Drawing.Size(784, 148);
            this.AHdropDownPanel.TabIndex = 4;
            this.AHdropDownPanel.Text = "Artifical GCS";
            this.AHdropDownPanel.UseVisualStyleBackColor = true;
            // 
            // PatternDropDownPanel
            // 
            this.PatternDropDownPanel.Controls.Add(this.tableLayoutPanel3);
            this.PatternDropDownPanel.Location = new System.Drawing.Point(4, 25);
            this.PatternDropDownPanel.Name = "PatternDropDownPanel";
            this.PatternDropDownPanel.Size = new System.Drawing.Size(784, 148);
            this.PatternDropDownPanel.TabIndex = 5;
            this.PatternDropDownPanel.Text = "Patterns";
            this.PatternDropDownPanel.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(798, 496);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(419, 177);
            this.tabControl1.TabIndex = 37;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.arm_takeOff);
            this.tabPage1.Controls.Add(this.loadFileButton);
            this.tabPage1.Controls.Add(this.isSimulator);
            this.tabPage1.Controls.Add(this.flyFileLabel);
            this.tabPage1.Controls.Add(this.connectButton);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(411, 151);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Auto Pilot Control";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(0, -1);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.trackBar1);
            this.splitContainer1.Size = new System.Drawing.Size(1350, 670);
            this.splitContainer1.SplitterDistance = 98;
            this.splitContainer1.TabIndex = 38;
            // 
            // simStatusTimer
            // 
            this.simStatusTimer.Interval = 1000;
            this.simStatusTimer.Tick += new System.EventHandler(this.simStatusTimer_Tick);
            // 
            // headingIndicatorInstrumentControl2
            // 
            this.headingIndicatorInstrumentControl2.Location = new System.Drawing.Point(784, 3);
            this.headingIndicatorInstrumentControl2.Name = "headingIndicatorInstrumentControl2";
            this.headingIndicatorInstrumentControl2.Size = new System.Drawing.Size(155, 155);
            this.headingIndicatorInstrumentControl2.TabIndex = 21;
            this.headingIndicatorInstrumentControl2.Text = "headingIndicatorInstrumentControl1";
            // 
            // turnCoordinatorInstrumentControl1
            // 
            this.turnCoordinatorInstrumentControl1.Location = new System.Drawing.Point(627, 3);
            this.turnCoordinatorInstrumentControl1.Name = "turnCoordinatorInstrumentControl1";
            this.turnCoordinatorInstrumentControl1.Size = new System.Drawing.Size(155, 155);
            this.turnCoordinatorInstrumentControl1.TabIndex = 22;
            this.turnCoordinatorInstrumentControl1.Text = "turnCoordinatorInstrumentControl1";
            // 
            // verticalSpeedIndicatorInstrumentControl1
            // 
            this.verticalSpeedIndicatorInstrumentControl1.Location = new System.Drawing.Point(315, 3);
            this.verticalSpeedIndicatorInstrumentControl1.Name = "verticalSpeedIndicatorInstrumentControl1";
            this.verticalSpeedIndicatorInstrumentControl1.Size = new System.Drawing.Size(155, 155);
            this.verticalSpeedIndicatorInstrumentControl1.TabIndex = 23;
            this.verticalSpeedIndicatorInstrumentControl1.Text = "verticalSpeedIndicatorInstrumentControl1";
            // 
            // airSpeedIndicatorInstrumentControl2
            // 
            this.airSpeedIndicatorInstrumentControl2.Location = new System.Drawing.Point(3, 3);
            this.airSpeedIndicatorInstrumentControl2.Name = "airSpeedIndicatorInstrumentControl2";
            this.airSpeedIndicatorInstrumentControl2.Size = new System.Drawing.Size(155, 155);
            this.airSpeedIndicatorInstrumentControl2.TabIndex = 18;
            this.airSpeedIndicatorInstrumentControl2.Text = "airSpeedIndicatorInstrumentControl1";
            // 
            // attitudeIndicatorInstrumentControl2
            // 
            this.attitudeIndicatorInstrumentControl2.Location = new System.Drawing.Point(471, 3);
            this.attitudeIndicatorInstrumentControl2.Name = "attitudeIndicatorInstrumentControl2";
            this.attitudeIndicatorInstrumentControl2.Size = new System.Drawing.Size(155, 155);
            this.attitudeIndicatorInstrumentControl2.TabIndex = 20;
            this.attitudeIndicatorInstrumentControl2.Text = "attitudeIndicatorInstrumentControl1";
            // 
            // altimeterInstrumentControl2
            // 
            this.altimeterInstrumentControl2.Location = new System.Drawing.Point(159, 3);
            this.altimeterInstrumentControl2.Name = "altimeterInstrumentControl2";
            this.altimeterInstrumentControl2.Size = new System.Drawing.Size(155, 155);
            this.altimeterInstrumentControl2.TabIndex = 19;
            this.altimeterInstrumentControl2.Text = "altimeterInstrumentControl1";
            // 
            // NewMainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1354, 733);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.NavBoardStatus);
            this.Controls.Add(this.Help);
            this.Controls.Add(this.Tools);
            this.Controls.Add(this.Settings);
            this.Controls.Add(this.FlightPlan);
            this.Controls.Add(this.FlightData);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "NewMainWindow";
            this.Text = "Ground Station";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.NavBoardStatus.ResumeLayout(false);
            this.StatusTabPage.ResumeLayout(false);
            this.StatusTabPage.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.graphTabPage.ResumeLayout(false);
            this.flightContral.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.navigationBoard.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.AHdropDownPanel.ResumeLayout(false);
            this.PatternDropDownPanel.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flightToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private GMap.NET.WindowsForms.GMapControl gmap;
        private System.Windows.Forms.ToolStripMenuItem statusPanelToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem navigationDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serialPortToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cameraToolStripMenuItem;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel DTNWPTL;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel AltitudeLL;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel5;
        private System.Windows.Forms.ToolStripStatusLabel headingStL;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel7;
        private System.Windows.Forms.ToolStripStatusLabel velocityL;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripStatusLabel gpsLock;
        private System.Windows.Forms.ToolStripStatusLabel readySL;
        private System.Windows.Forms.ToolStripMenuItem flightMonitorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem joystickControllerToolStripMenuItem;
        private System.Windows.Forms.Button arm_takeOff;
        private System.Windows.Forms.CheckBox isSimulator;

        private System.Windows.Forms.Button loadFileButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private VerticalSpeedIndicatorInstrumentControl verticalSpeedIndicatorInstrumentControl1;
        private TurnCoordinatorInstrumentControl turnCoordinatorInstrumentControl1;
        private HeadingIndicatorInstrumentControl headingIndicatorInstrumentControl2;
        private AttitudeIndicatorInstrumentControl attitudeIndicatorInstrumentControl2;
        private AltimeterInstrumentControl altimeterInstrumentControl2;
        private AirSpeedIndicatorInstrumentControl airSpeedIndicatorInstrumentControl2;

        private System.Windows.Forms.RadioButton figure8C;
        private System.Windows.Forms.RadioButton landHereC;
        private System.Windows.Forms.RadioButton landHomeC;
        private System.Windows.Forms.RadioButton stopC;
        private System.Windows.Forms.RadioButton FlyHomeC;
        private System.Windows.Forms.RadioButton circleLeftC;
        private System.Windows.Forms.RadioButton circleRightC;
        private System.Windows.Forms.RadioButton DescC;
        private System.Windows.Forms.RadioButton takePictureC;
        private System.Windows.Forms.RadioButton orbitLeftC;
        private System.Windows.Forms.RadioButton orbitRight;
        private System.Windows.Forms.RadioButton hoverC;


        private System.Windows.Forms.ToolStripMenuItem loadFlyFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flightPlanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generalSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem microPilotSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groundSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flightMonitorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem joyStickControllerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem autopilotControlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem patternsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graphDisplayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flightControlToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel linkSPanel;
        private System.Windows.Forms.ToolStripMenuItem flyFileUtilityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vRSFileUtilityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transmitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acquireToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel rcStausL;
        private System.Windows.Forms.ToolStripStatusLabel failureStrip;
        private System.Windows.Forms.ToolStripStatusLabel fatalStrip;
        private System.Windows.Forms.ToolStripMenuItem testServosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem failurePatternsSimulationToolStripMenuItem;
        private System.Windows.Forms.Label flyFileLabel;
        private System.Windows.Forms.ToolStripMenuItem setVariableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem automaticInitializationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem artificialHorizonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rCSensorMonitorToolStripMenuItem;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Timer seekingTimer;
        private System.Windows.Forms.Timer apStatusTimer;
        private System.Windows.Forms.Timer apStatus2Timer;
        private System.Windows.Forms.Timer bindGaugesTimer;
        private System.Windows.Forms.Timer graphTimer;
        private System.Windows.Forms.Timer labelsTimer;
        private System.Windows.Forms.Timer updateMapTimer;
        private System.Windows.Forms.ToolStripMenuItem aGLMonitorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mIBSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cachingToolStripMenuItem;
        private System.Windows.Forms.RadioButton FlightData;
        private System.Windows.Forms.RadioButton FlightPlan;
        private System.Windows.Forms.RadioButton Settings;
        private System.Windows.Forms.RadioButton Tools;
        private System.Windows.Forms.RadioButton Help;
        private System.Windows.Forms.TabControl NavBoardStatus;
        private System.Windows.Forms.TabPage StatusTabPage;
        private System.Windows.Forms.Label mbatL;
        private System.Windows.Forms.Label sBatL;
        private System.Windows.Forms.Label thrL;
        private System.Windows.Forms.ProgressBar mBatProgreesbar;
        private System.Windows.Forms.ProgressBar sBatProgressBar;
        private System.Windows.Forms.ProgressBar throttleProgressBar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label bankAngleL;
        private System.Windows.Forms.Label airSpeedL;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label turnRateL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label DistTONXPL;
        private System.Windows.Forms.Label aglHeighL;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label altitudeL;
        private System.Windows.Forms.Label verticalSpeedL;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label HeadingL;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label yawL;
        private System.Windows.Forms.Label gpsTimeL;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage graphTabPage;
        private ZedGraph.ZedGraphControl graphDisplay;
        private System.Windows.Forms.TabPage flightContral;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton picRadButton;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton19;
        private System.Windows.Forms.RadioButton radioButton24;
        private System.Windows.Forms.TabPage navigationBoard;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label AltNavLabel;
        private System.Windows.Forms.Label AltNavDis;
        private System.Windows.Forms.Label LongNavLabel;
        private System.Windows.Forms.Label LongNavDis;
        private System.Windows.Forms.Label packetsCountLabel;
        private System.Windows.Forms.Label packetsCountDisplay;
        private System.Windows.Forms.Label StNavLabel;
        private System.Windows.Forms.Label modeNavLabel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label GPSNAVStatus;
        private System.Windows.Forms.Label navLatLabel;
        private System.Windows.Forms.Label NavLatdis;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label20;

        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button vrsAcquire;
        private System.Windows.Forms.Button transmitVRS;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabPage AHdropDownPanel;
        private System.Windows.Forms.TabPage PatternDropDownPanel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Timer simStatusTimer;

    }
}

