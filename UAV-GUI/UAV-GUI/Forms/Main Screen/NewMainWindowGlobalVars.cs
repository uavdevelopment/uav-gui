﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Threading;
using UAV_GUI.FileClasses;
using MasterClasses;
using Error_Reporting;
using MP_Lib;
using JoystickPlugin;
using FileClasses.VRS_Files;
using FileClasses.INI_Files;
using UAV_GUI.FileClasses.FlyFiles;
using UAV_GUI.FlightClasses;
using UAV_GUI.Forms.Setup.MP;
using UAV_GUI.Forms;
using UAV_GUI.Forms.Main_Screen;
using GMap.NET.MapProviders;
using UAV_GUI.Utilities;
using FlightMonitor;
using System.Diagnostics;
using UAV_GUI.Forms.Set_Variable;
using GLobal;
using UAV_GUI.Forms.Setup;

namespace UAV_GUI
{
    public partial class NewMainWindow
    {
        /*Overlays defintions***************************/
        Image mya = Image.FromFile("imgs/a.png");
        GMapOverlay markersOverlay;
        GMapOverlay pathOverlay;
        GMapOverlay wpRouteOverlay;
        List<PointLatLng> points; // related to wpRoute overlay 
        GMapRoute route; // related to route overlay
        /*End of Overlays Declarations******************/

        /*Parsing Fly File Related Variables***************************/
        List<WayPoint> wayPointList;
        List<Command> commandsList;
        List<double> CommandArg = new List<double>();
        List<GMapMarker> markersList = new List<GMapMarker>();
        List<GMapRoute> routeList = new List<GMapRoute>();
        String fileNme = null;
        /*Parsing Fly File******************************************************************/

        /*Markers Defintions*****************************************************/
        GMarkerGoogle currntMarker;  // NEED TO SET IT
        GMapPathMarker moVingmarker;
        /*End of Markers Defintions*****************************************************/

        /*Global Variables *******************************/
        bool start = true; // Detect the first launching of the application 
        int Count = 0;
        private static Flight myFlight;
        public Thread updateMapThread;
        GMapMarker selectedMarker = null;
        bool selectedMarkerFlag = false;
        bool selectedMarkerDeleted = false;
        bool isMouseDown = false;
        public double MainMap_orlong = 0;
        public double MainMap_orlat = 0;
        public double MainMap_oralt = 0;
        public double val = 0;
        private GraphOptions myGraph;
        

        /*End Global Variables *******************************/

        /*Forms Invoked Upon Click from Any Button and/Or ToolStrip*/

        Form2 n;
        

        /*End Of Forms Section ***********************************/

        /* Right Click Menu*/

        ContextMenu cm = new ContextMenu();
        MenuItem patterns;

        /* End Right Click Menu*/

    }
}
