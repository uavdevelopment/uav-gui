﻿namespace UAV_GUI
{
    partial class Artifical_Horizon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Artifical_Horizon));
            this.verticalSpeedIndicatorInstrumentControl2 = new UAV_GUI.VerticalSpeedIndicatorInstrumentControl();
            this.turnCoordinatorInstrumentControl2 = new UAV_GUI.TurnCoordinatorInstrumentControl();
            this.headingIndicatorInstrumentControl2 = new UAV_GUI.HeadingIndicatorInstrumentControl();
            this.attitudeIndicatorInstrumentControl2 = new UAV_GUI.AttitudeIndicatorInstrumentControl();
            this.airSpeedIndicatorInstrumentControl1 = new UAV_GUI.AirSpeedIndicatorInstrumentControl();
            this.altimeterInstrumentControl1 = new UAV_GUI.AltimeterInstrumentControl();
            this.SuspendLayout();
            // 
            // verticalSpeedIndicatorInstrumentControl2
            // 
            this.verticalSpeedIndicatorInstrumentControl2.Location = new System.Drawing.Point(12, 235);
            this.verticalSpeedIndicatorInstrumentControl2.Name = "verticalSpeedIndicatorInstrumentControl2";
            this.verticalSpeedIndicatorInstrumentControl2.Size = new System.Drawing.Size(174, 172);
            this.verticalSpeedIndicatorInstrumentControl2.TabIndex = 21;
            this.verticalSpeedIndicatorInstrumentControl2.Text = "verticalSpeedIndicatorInstrumentControl2";
            // 
            // turnCoordinatorInstrumentControl2
            // 
            this.turnCoordinatorInstrumentControl2.Location = new System.Drawing.Point(364, 21);
            this.turnCoordinatorInstrumentControl2.Name = "turnCoordinatorInstrumentControl2";
            this.turnCoordinatorInstrumentControl2.Size = new System.Drawing.Size(198, 196);
            this.turnCoordinatorInstrumentControl2.TabIndex = 20;
            this.turnCoordinatorInstrumentControl2.Text = "turnCoordinatorInstrumentControl2";
            // 
            // headingIndicatorInstrumentControl2
            // 
            this.headingIndicatorInstrumentControl2.Location = new System.Drawing.Point(218, 52);
            this.headingIndicatorInstrumentControl2.Name = "headingIndicatorInstrumentControl2";
            this.headingIndicatorInstrumentControl2.Size = new System.Drawing.Size(124, 128);
            this.headingIndicatorInstrumentControl2.TabIndex = 19;
            this.headingIndicatorInstrumentControl2.Text = "headingIndicatorInstrumentControl2";
            // 
            // attitudeIndicatorInstrumentControl2
            // 
            this.attitudeIndicatorInstrumentControl2.Location = new System.Drawing.Point(12, 12);
            this.attitudeIndicatorInstrumentControl2.Name = "attitudeIndicatorInstrumentControl2";
            this.attitudeIndicatorInstrumentControl2.Size = new System.Drawing.Size(191, 195);
            this.attitudeIndicatorInstrumentControl2.TabIndex = 18;
            this.attitudeIndicatorInstrumentControl2.Text = "attitudeIndicatorInstrumentControl2";
            // 
            // airSpeedIndicatorInstrumentControl1
            // 
            this.airSpeedIndicatorInstrumentControl1.Location = new System.Drawing.Point(200, 235);
            this.airSpeedIndicatorInstrumentControl1.Name = "airSpeedIndicatorInstrumentControl1";
            this.airSpeedIndicatorInstrumentControl1.Size = new System.Drawing.Size(170, 172);
            this.airSpeedIndicatorInstrumentControl1.TabIndex = 22;
            this.airSpeedIndicatorInstrumentControl1.Text = "airSpeedIndicatorInstrumentControl1";
            // 
            // altimeterInstrumentControl1
            // 
            this.altimeterInstrumentControl1.Location = new System.Drawing.Point(398, 235);
            this.altimeterInstrumentControl1.Name = "altimeterInstrumentControl1";
            this.altimeterInstrumentControl1.Size = new System.Drawing.Size(169, 172);
            this.altimeterInstrumentControl1.TabIndex = 23;
            this.altimeterInstrumentControl1.Text = "altimeterInstrumentControl1";
            // 
            // Artifical_Horizon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(591, 413);
            this.Controls.Add(this.altimeterInstrumentControl1);
            this.Controls.Add(this.airSpeedIndicatorInstrumentControl1);
            this.Controls.Add(this.verticalSpeedIndicatorInstrumentControl2);
            this.Controls.Add(this.turnCoordinatorInstrumentControl2);
            this.Controls.Add(this.headingIndicatorInstrumentControl2);
            this.Controls.Add(this.attitudeIndicatorInstrumentControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Artifical_Horizon";
            this.ShowIcon = false;
            this.Text = "Artificail GCS";
            this.Load += new System.EventHandler(this.Form5_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private VerticalSpeedIndicatorInstrumentControl verticalSpeedIndicatorInstrumentControl2;
        private TurnCoordinatorInstrumentControl turnCoordinatorInstrumentControl2;
        private HeadingIndicatorInstrumentControl headingIndicatorInstrumentControl2;
        private AttitudeIndicatorInstrumentControl attitudeIndicatorInstrumentControl2;
        private AirSpeedIndicatorInstrumentControl airSpeedIndicatorInstrumentControl1;
        private AltimeterInstrumentControl altimeterInstrumentControl1;



    }
}