﻿namespace UAV_GUI.Forms.Main_Screen
{
    partial class CachingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.gmap = new GMap.NET.WindowsForms.GMapControl();
            this.mapProviderComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Import = new System.Windows.Forms.Button();
            this.Export = new System.Windows.Forms.Button();
            this.prefetch = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.cacheLocation = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.groupBox4.SuspendLayout();

            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.trackBar1);
            this.groupBox1.Controls.Add(this.gmap);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(248, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(610, 465);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Map";
            // 
            // trackBar1
            // 
            this.trackBar1.AutoSize = false;
            this.trackBar1.LargeChange = 1;
            this.trackBar1.Location = new System.Drawing.Point(679, 26);
            this.trackBar1.Maximum = 50;
            this.trackBar1.Minimum = 2;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBar1.Size = new System.Drawing.Size(45, 482);
            this.trackBar1.TabIndex = 1;
            this.trackBar1.Value = 14;
            // 
            // gmap
            // 
            this.gmap.Bearing = 0F;
            this.gmap.CanDragMap = true;
            this.gmap.EmptyTileColor = System.Drawing.Color.Navy;
            this.gmap.GrayScaleMode = false;
            this.gmap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gmap.LevelsKeepInMemmory = 5;
            this.gmap.Location = new System.Drawing.Point(6, 19);
            this.gmap.MarkersEnabled = true;
            this.gmap.MaxZoom = 50;
            this.gmap.MinZoom = 2;
            this.gmap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionWithoutCenter;
            this.gmap.Name = "gmap";
            this.gmap.NegativeMode = true;
            this.gmap.PolygonsEnabled = true;
            this.gmap.RetryLoadTile = 0;
            this.gmap.RoutesEnabled = true;
            this.gmap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gmap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gmap.ShowTileGridLines = false;
            this.gmap.Size = new System.Drawing.Size(598, 438);
            this.gmap.TabIndex = 0;
            this.gmap.Zoom = 2D;
            // 
            // mapProviderComboBox
            // 
            this.mapProviderComboBox.FormattingEnabled = true;
            this.mapProviderComboBox.Location = new System.Drawing.Point(18, 26);
            this.mapProviderComboBox.Name = "mapProviderComboBox";
            this.mapProviderComboBox.Size = new System.Drawing.Size(127, 21);
            this.mapProviderComboBox.TabIndex = 25;
            this.mapProviderComboBox.SelectedIndexChanged += new System.EventHandler(this.mapProviderComboBox_SelectedIndexChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.mapProviderComboBox);
            this.groupBox4.Location = new System.Drawing.Point(12, 63);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(190, 74);
            this.groupBox4.TabIndex = 28;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Map Type";
            // 
            // Import
            // 

            this.Import.Location = new System.Drawing.Point(30, 182);
            this.Import.Name = "Import";
            this.Import.Size = new System.Drawing.Size(140, 23);
            this.Import.TabIndex = 29;
            this.Import.Text = "Import";
          this.Import.UseVisualStyleBackColor = true;
            this.Import.Click += new System.EventHandler(this.Import_Click);
            // 
            // Export
            // 

            this.Export.Location = new System.Drawing.Point(30, 211);
            this.Export.Name = "Export";
            this.Export.Size = new System.Drawing.Size(140, 23);
            this.Export.TabIndex = 30;
            this.Export.Text = "Export";
            this.Export.UseVisualStyleBackColor = true;
            this.Export.Click += new System.EventHandler(this.Export_Click);
            // 
            // prefetch
            // 

            this.prefetch.Location = new System.Drawing.Point(30, 240);
            this.prefetch.Name = "prefetch";
            this.prefetch.Size = new System.Drawing.Size(140, 23);
            this.prefetch.TabIndex = 31;
            this.prefetch.Text = "Prefetch Selected Area";
           this.prefetch.UseVisualStyleBackColor = true;
            this.prefetch.Click += new System.EventHandler(this.prefetch_Click);
            // 
            // clear
            // 

            this.clear.Location = new System.Drawing.Point(30, 269);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(140, 23);
            this.clear.TabIndex = 32;
            this.clear.Text = "Clear Tiles in Cache";
           this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // cacheLocation
            // 

            this.cacheLocation.Location = new System.Drawing.Point(30, 298);
            this.cacheLocation.Name = "cacheLocation";
            this.cacheLocation.Size = new System.Drawing.Size(140, 23);
            this.cacheLocation.TabIndex = 33;
            this.cacheLocation.Text = "Open Cache Location";
            this.cacheLocation.UseVisualStyleBackColor = true;
            this.cacheLocation.Click += new System.EventHandler(this.cacheLocation_Click);
            // 
            // groupBox2
            // 

            this.groupBox2.Location = new System.Drawing.Point(12, 152);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(190, 181);
           this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cache Options";
            // 
            // CachingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 591);

            this.Controls.Add(this.cacheLocation);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.prefetch);
            this.Controls.Add(this.Export);
            this.Controls.Add(this.Import);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "CachingForm";
            this.Text = "CachingForm";
            this.Load += new System.EventHandler(this.CachingForm_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.groupBox4.ResumeLayout(false);
           this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TrackBar trackBar1;
        private GMap.NET.WindowsForms.GMapControl gmap;
        public System.Windows.Forms.ComboBox mapProviderComboBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button Import;
        private System.Windows.Forms.Button Export;
        private System.Windows.Forms.Button prefetch;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button cacheLocation;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}