﻿namespace UAV_GUI.Forms.Main_Screen
{
    partial class AGLForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.valueL = new System.Windows.Forms.Label();
            this.aquaGauge1 = new AquaControls.AquaGauge();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Current Value";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // valueL
            // 
            this.valueL.AutoSize = true;
            this.valueL.Location = new System.Drawing.Point(137, 22);
            this.valueL.Name = "valueL";
            this.valueL.Size = new System.Drawing.Size(25, 13);
            this.valueL.TabIndex = 1;
            this.valueL.Text = "000";
            // 
            // aquaGauge1
            // 
            this.aquaGauge1.BackColor = System.Drawing.Color.Transparent;
            this.aquaGauge1.DialColor = System.Drawing.Color.Lavender;
            this.aquaGauge1.DialText = null;
            this.aquaGauge1.Glossiness = 11.36364F;
            this.aquaGauge1.Location = new System.Drawing.Point(12, 38);
            this.aquaGauge1.MaxValue = 3600F;
            this.aquaGauge1.MinValue = 0F;
            this.aquaGauge1.Name = "aquaGauge1";
            this.aquaGauge1.RecommendedValue = 0F;
            this.aquaGauge1.Size = new System.Drawing.Size(150, 150);
            this.aquaGauge1.TabIndex = 2;
            this.aquaGauge1.ThresholdPercent = 0F;
            this.aquaGauge1.Value = 0F;
            // 
            // AGLForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(184, 189);
            this.Controls.Add(this.aquaGauge1);
            this.Controls.Add(this.valueL);
            this.Controls.Add(this.label1);
            this.Name = "AGLForm";
            this.Text = "AGL Monitor";
            this.Load += new System.EventHandler(this.AGL_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label valueL;
        private AquaControls.AquaGauge aquaGauge1;

    }
}