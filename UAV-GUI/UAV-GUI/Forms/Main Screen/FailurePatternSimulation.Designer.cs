﻿namespace UAV_GUI.Forms.Main_Screen
{
    partial class FailurePatternSimulation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.controlC = new System.Windows.Forms.CheckBox();
            this.GPsC = new System.Windows.Forms.CheckBox();
            this.engineC = new System.Windows.Forms.CheckBox();
            this.rcC = new System.Windows.Forms.CheckBox();
            this.BatteryC = new System.Windows.Forms.CheckBox();
            this.GcsC = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // controlC
            // 
            this.controlC.Appearance = System.Windows.Forms.Appearance.Button;
            this.controlC.AutoSize = true;
            this.controlC.Location = new System.Drawing.Point(12, 12);
            this.controlC.Name = "controlC";
            this.controlC.Size = new System.Drawing.Size(84, 23);
            this.controlC.TabIndex = 0;
            this.controlC.Text = "Control Failure";
            this.controlC.UseVisualStyleBackColor = true;
            this.controlC.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // GPsC
            // 
            this.GPsC.Appearance = System.Windows.Forms.Appearance.Button;
            this.GPsC.AutoSize = true;
            this.GPsC.Location = new System.Drawing.Point(124, 12);
            this.GPsC.Name = "GPsC";
            this.GPsC.Size = new System.Drawing.Size(64, 23);
            this.GPsC.TabIndex = 1;
            this.GPsC.Text = "GPS Loss";
            this.GPsC.UseVisualStyleBackColor = true;
            this.GPsC.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // engineC
            // 
            this.engineC.Appearance = System.Windows.Forms.Appearance.Button;
            this.engineC.AutoSize = true;
            this.engineC.Location = new System.Drawing.Point(12, 60);
            this.engineC.Name = "engineC";
            this.engineC.Size = new System.Drawing.Size(84, 23);
            this.engineC.TabIndex = 2;
            this.engineC.Text = "Engine Failure";
            this.engineC.UseVisualStyleBackColor = true;
            this.engineC.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // rcC
            // 
            this.rcC.Appearance = System.Windows.Forms.Appearance.Button;
            this.rcC.AutoSize = true;
            this.rcC.Location = new System.Drawing.Point(124, 60);
            this.rcC.Name = "rcC";
            this.rcC.Size = new System.Drawing.Size(57, 23);
            this.rcC.TabIndex = 3;
            this.rcC.Text = "RC Loss";
            this.rcC.UseVisualStyleBackColor = true;
            this.rcC.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // BatteryC
            // 
            this.BatteryC.Appearance = System.Windows.Forms.Appearance.Button;
            this.BatteryC.AutoSize = true;
            this.BatteryC.Location = new System.Drawing.Point(12, 112);
            this.BatteryC.Name = "BatteryC";
            this.BatteryC.Size = new System.Drawing.Size(84, 23);
            this.BatteryC.TabIndex = 4;
            this.BatteryC.Text = "Battery Failure";
            this.BatteryC.UseVisualStyleBackColor = true;
            this.BatteryC.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // GcsC
            // 
            this.GcsC.Appearance = System.Windows.Forms.Appearance.Button;
            this.GcsC.AutoSize = true;
            this.GcsC.Location = new System.Drawing.Point(124, 112);
            this.GcsC.Name = "GcsC";
            this.GcsC.Size = new System.Drawing.Size(71, 23);
            this.GcsC.TabIndex = 5;
            this.GcsC.Text = "GCS Comm";
            this.GcsC.UseVisualStyleBackColor = true;
            this.GcsC.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // FailurePatternSimulation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(210, 156);
            this.Controls.Add(this.GcsC);
            this.Controls.Add(this.BatteryC);
            this.Controls.Add(this.rcC);
            this.Controls.Add(this.engineC);
            this.Controls.Add(this.GPsC);
            this.Controls.Add(this.controlC);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FailurePatternSimulation";
            this.Text = "Failure Patterns Simulation";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.CheckBox controlC;
        private System.Windows.Forms.CheckBox GPsC;
        private System.Windows.Forms.CheckBox engineC;
        private System.Windows.Forms.CheckBox rcC;
        private System.Windows.Forms.CheckBox BatteryC;
        private System.Windows.Forms.CheckBox GcsC;
    }
}