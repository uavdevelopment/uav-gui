﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MP_Lib;
using System.Threading;
using Error_Reporting;

namespace UAV_GUI.Forms.Main_Screen
{
    public partial class TransmitFlyFile : Form
    {
        public UAV myUav;
        public String fileNme;
        public TransmitFlyFile(UAV myUAV_)
        {
            myUav=myUAV_;
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
        }
        private Thread transmotThread;
        private void TransmitVRSFile_Load(object sender, EventArgs e)
        {
        }
        private void saveCancelButton_Click(object sender, EventArgs e)
        {
            
            if (saveCancelButton.Text == "Save")
            {
                
                    DialogResult result = openFileDialog1.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        fileNme = openFileDialog1.FileName;
                    }
                    else
                    {
                        return;
                    }
                
                transmotThread = new Thread(new ThreadStart(transmitflyFile));
                transmotThread.Start();
                saveCancelButton.Text = "Cancel";
                progressBar1.Style = ProgressBarStyle.Marquee;

            }
            else {

                saveCancelButton.Text = "Save";
                transmotThread.Abort();
                transmotThread = null;
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 0;
            }
        }
        private void transmitflyFile ()
        {
          
            int retValu=0;
               
           retValu = myUav.transmitFLyFileOnGround(fileNme);

           //MessageBox.Show("Transmission done ");
           //retValu = myUav.verifySimulatorAndAutoPilot(true);
            
           progressBar1.Style = ProgressBarStyle.Blocks;
           progressBar1.Value = 0;
           progressBar1.Show();
           saveCancelButton.Text = "Save";
           if (retValu == 0)
           {
               MessageBox.Show("Tranmitting The Fly File is done");
           }
           else {
               
            MessageBox.Show("Failed to transmit the fields <reply code is " + retValu + ">.", "Error (1101)",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
           }
        }
        
      
        private void VerifyflyFile()
        {

            int retValu = 0;

            retValu = myUav.verifySimulatorAndAutoPilot(true);
            progressBar1.Style = ProgressBarStyle.Blocks;
            progressBar1.Value = 0;
            receiveButton.Text = "Verify";
            if (retValu == 0)
            {
                MessageBox.Show("Fly File is matched on both simulator and AutoPilot");
            }
            else
            {

                MessageBox.Show("Failed to verify the Simulator and the AutoPilot <reply code is " + retValu + ">.", "Error (1102)",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
       
        private void receiveButton_Click(object sender, EventArgs e)
        {
            if (receiveButton.Text == "Verify")
            {
               
                transmotThread = new Thread(new ThreadStart(VerifyflyFile));
                transmotThread.Start();
                receiveButton.Text = "Cancel";
                progressBar1.Style = ProgressBarStyle.Marquee;

            }
            else
            {

                receiveButton.Text = "Verify";
                transmotThread.Abort();
                transmotThread = null;
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 0;
            }
        }
        
    }
}
