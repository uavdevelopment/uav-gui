﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MP_Lib;
using System.Threading;
using Error_Reporting;

namespace UAV_GUI.Forms.Main_Screen
{
    public partial class FailurePatternSimulation : Form
    {
        public UAV myUav;
        public String fileNme;
        public FailurePatternSimulation(UAV myUAV_)
        {
            myUav=myUAV_;
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
        }

        private void unCheckAllBoxes()
        {
            foreach (Control ss in this.Controls)
            {
                      CheckBox sss = ss as CheckBox;
                }
        }

        private bool validateOnlyPush(CheckBox SelectedCheckBox)
        {
            foreach (Control ss in this.Controls)
            {
                if (!ss.Equals(SelectedCheckBox))
                {
                    CheckBox sss = ss as CheckBox;
                    if (sss.Checked) return false;
                }
            }
            return true;
        }

        private bool checkIfWeCanEndTheCurrentPattern() {

            int x = myUav.getCurrentFailurePattern();
            if (x != 0)
            {

                MessageBox.Show("Cannot End simulation Of This Failure Pattern");
                return false;
            }
            if (x == 0 || x == 255)
            {
                MessageBox.Show("There is no running Failure Patterns");
                return false;
            }
            if (x != 1 && x != 4) //recoverable
            {
                myUav.endFailurePattern((short)x);
                return true;
            }
            else
            {
                MessageBox.Show("Unrecoverable Failure .. Restart Simulation");
                return false;
            }
        }
        private void Form_FormClosing(object sender, FormClosingEventArgs e)
        {

            this.Hide();
            e.Cancel = true; // this cancels the close event.

        }

        private void startPatternOfCheckBox(CheckBox SelectedCheckBox)
        {
            if (SelectedCheckBox.Equals(rcC))
            {
                myUav.startFailurePattern(5);
            }
            else if (SelectedCheckBox.Equals(GcsC))
            {
                myUav.startFailurePattern(6);
            }
            else if (SelectedCheckBox.Equals(GPsC))
            {
                myUav.startFailurePattern(2);
            }
            else if (SelectedCheckBox.Equals(controlC))
            {
                myUav.startFailurePattern(0);
            }
            else if (SelectedCheckBox.Equals(engineC))
            {
                myUav.startFailurePattern(3);
            }
            else if (SelectedCheckBox.Equals(BatteryC))
            {
                myUav.startFailurePattern(4);
            }
        }
        
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox SelectedCheckBox = sender as CheckBox;

            
            if (SelectedCheckBox.Checked)
            {
                if (validateOnlyPush(SelectedCheckBox))
                {
                    startPatternOfCheckBox(SelectedCheckBox);
                }
                else {
                    if (checkIfWeCanEndTheCurrentPattern())
                    {
                        startPatternOfCheckBox(SelectedCheckBox);
                        foreach (Control ss in this.Controls)
                        {
                            if (!ss.Equals(SelectedCheckBox))
                            {
                                CheckBox sss = ss as CheckBox;
                                sss.Checked = false;

                            }
                        }
                    }
                    else {
                        unCheckAllBoxes();
                    
                    }
                
                }

                
            }
            else
            {

                checkIfWeCanEndTheCurrentPattern();
                
            }
        }
        
    }
}
