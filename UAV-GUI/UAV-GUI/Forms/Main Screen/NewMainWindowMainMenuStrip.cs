﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Threading;
using UAV_GUI.FileClasses;
using MasterClasses;
using Error_Reporting;
using MP_Lib;
using JoystickPlugin;
using FileClasses.VRS_Files;
using FileClasses.INI_Files;
using UAV_GUI.FileClasses.FlyFiles;
using UAV_GUI.FlightClasses;
using UAV_GUI.Forms.Setup.MP;
using UAV_GUI.Forms;
using UAV_GUI.Forms.Main_Screen;
using GMap.NET.MapProviders;
using UAV_GUI.Utilities;
using FlightMonitor;
using System.Diagnostics;
using UAV_GUI.Forms.Set_Variable;
using GLobal;


namespace UAV_GUI
{
    /*Depreciated from V 2.0*/
    public partial class NewMainWindow
    {
        private void flightMonitorToolStripMenuItem_Click(object sender, EventArgs e)
        {

            GlobalData.myLogger.Show();
        }



        private void joystickControllerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 ss = new Form1();
            ss.StartPosition = FormStartPosition.CenterParent;
            ss.Show();
        }

        private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabControl tabContralObj = sender as TabControl;
        }




        private void groundSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {

            UAV_GUI.Forms.Setup.MP.GroundSetup GroundSetupForm = new UAV_GUI.Forms.Setup.MP.GroundSetup();
            GroundSetupForm.StartPosition = FormStartPosition.CenterParent;
            GroundSetupForm.ShowDialog();

        }

        private void loadFlyFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MainMap_orlong == -1 || MainMap_orlat == -1)
            {
                MessageBox.Show("Can't Add WP Befor Setting the Origin Location \n  Please Click on the Map to Specify an Origin ", "Origin Setting");

            }
            else
            {
                /*  Clear Map Elements   */




                // this.openFileDialog1.Filter = "*.fly| *.wpt";
                DialogResult result = openFileDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    fileNme = openFileDialog1.FileName;
                }
                else
                {
                    return;
                }
                loadFlyFile(fileNme);
                connectButton.Enabled = true;


            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }




        private void statusPanelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[1]).Checked)
            {

                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[1]).Checked = false;

                NavBoardStatus.TabPages.Remove(StatusTabPage);
            }

            else
            {
                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[1]).Checked = true;
                NavBoardStatus.TabPages.Add(StatusTabPage);

            }
        }


        private void autopilotControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[2]).Checked)
            {

                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[2]).Checked = false;

            }

            else
            {
                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[2]).Checked = true;

            }
        }

        private void patternsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[3]).Checked)
            {

                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[3]).Checked = false;

            }

            else
            {
                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[3]).Checked = true;

            }
        }

        private void graphDisplayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[4]).Checked)
            {

                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[4]).Checked = false;

                NavBoardStatus.TabPages.Remove(graphTabPage);
            }

            else
            {
                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[4]).Checked = true;
                NavBoardStatus.TabPages.Add(graphTabPage);

            }
        }

        private void flightControlToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[5]).Checked)
            {

                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[5]).Checked = false;

                NavBoardStatus.TabPages.Remove(graphTabPage);
            }

            else
            {
                ((ToolStripMenuItem)flightToolStripMenuItem.DropDown.Items[5]).Checked = true;
                NavBoardStatus.TabPages.Add(graphTabPage);

            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UAV_GUI.Forms.Main_Screen.AboutWindow aboutWindow = new UAV_GUI.Forms.Main_Screen.AboutWindow();
            aboutWindow.StartPosition = FormStartPosition.CenterParent;
            aboutWindow.ShowDialog();
        }

        private void flightPlanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            n = new Form2();
            n.Text = "Untitled.fly - Flight Plan";
            n.FormClosed += new FormClosedEventHandler(f_FormClosed);
            n.Show();
        }

        private void flightControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FlightControl_W myControl = new FlightControl_W();
            myControl.ShowDialog();
        }


        private void cameraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /* CameraControl_W camControl = new CameraControl_W();
             camControl.ShowDialog();*/
        }
        private void generalSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Setup setupForm = new Setup();
            setupForm.StartPosition = FormStartPosition.CenterParent;
            setupForm.ShowDialog();
        }


        private void microPilotSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // for first time load default vrs file 
            VrsFileHandler vrsHandler = new VrsFileHandler(UAV_GUI.Forms.MicroPilotSetup.fileName);
            UAV_GUI.Forms.MicroPilotSetup MicroPilotSetupForm = new UAV_GUI.Forms.MicroPilotSetup(ref vrsHandler, myFlight);
            MicroPilotSetupForm.StartPosition = FormStartPosition.CenterParent;
            MicroPilotSetupForm.ShowDialog();
        }


    }
}
