﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Threading;
using UAV_GUI.FileClasses;
using MasterClasses;
using Error_Reporting;
using MP_Lib;
using JoystickPlugin;
using FileClasses.VRS_Files;
using FileClasses.INI_Files;
using UAV_GUI.FileClasses.FlyFiles;
using UAV_GUI.FlightClasses;
using UAV_GUI.Forms.Setup.MP;
using UAV_GUI.Forms;
using UAV_GUI.Forms.Main_Screen;
using GMap.NET.MapProviders;
using UAV_GUI.Utilities;
using FlightMonitor;
using System.Diagnostics;
using UAV_GUI.Forms.Set_Variable;
using GLobal;


namespace UAV_GUI
{
    public partial class NewMainWindow
    {

        private void updateMapTimer_Tick1(object sender, EventArgs e)
        {

            try
            {
                if (!GlobalData.autoPilotReady)
                {
                    moVingmarker.Position = new PointLatLng(INIData.latitude, INIData.longitude);
                    return;
                }
                
                moVingmarker.LogPosition = new PointLatLng(myFlight.flyData.Latitude, myFlight.flyData.Longitude);
                moVingmarker.Heading = myFlight.flyData.Heading;
                gmap.Position = moVingmarker.Position;
                moVingmarker.ToolTipText = ("( Lat: " + moVingmarker.Position.Lat + "\r\n Long: " + moVingmarker.Position.Lng + " \r\n Hdg: " + moVingmarker.Heading + ")");
                gmap.UpdateRouteLocalPosition(moVingmarker.route);

            }
            catch (InvalidOperationException s)
            {
                GlobalData.LogConsole(s.Message + "at " + this.Name + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());
                return;
            }

            catch (Exception s)
            {
                GlobalData.LogConsole(s.Message + "at " + this.Name + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());
                return;
            }

        }

        private void seekingTimer_Tick(object sender, EventArgs e)
        {

            try
            {
                /*Checking The GPS Lock */
                myFlight.flyData.GpsLock = myFlight.flyUAV.isGPSOK() == 1 ? true : false;
                if (myFlight.flyData.GpsLock)
                {

                    this.gpsLock.Text = "Locked";
                    this.gpsLock.BackColor = Color.Green;
                    //return;
                }
                else
                {
                    this.gpsLock.Text = "Not Locked";
                    this.gpsLock.BackColor = Color.Red;
                }
            }
            catch (Exception g)
            {
                GlobalData.LogConsole(g.Message + "at " + this.Name + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());

            }

        }

        private void apStatusTimer_Tick(object sender, EventArgs e)
        {
            /*Cheking The communication Link */
            if (myFlight.flyUAV.checkLink())
            {
                GlobalData.apConnected = true;
                linkSPanel.Text = "Connected";
                linkSPanel.BackColor = Color.Green;
            }
            else
            {
                GlobalData.apConnected = false;
                linkSPanel.Text = "Disconnected";
                linkSPanel.BackColor = Color.Red;
            }
            int status = myFlight.flyData.pStdTlmData.status;

            /*Check For Readiness*/
            if ((status >> 7 & 1) == 1)
            {
                readySL.Text = "Ready";
                if (!GlobalData.autoPilotReady)
                {
                    Location orLoca = myFlight.flyUAV.getOrigin();
                    GlobalData.orValEDeg = orLoca.Longitude;
                    GlobalData.orValNDeg = orLoca.Latitude;
                    GlobalData.orValNRad = (GlobalData.orValNDeg * Math.PI) / 180.0;
                    GlobalData.orValERad = (GlobalData.orValEDeg * Math.PI) / 180.0;
                    gmap.Position = new PointLatLng(orLoca.Latitude, orLoca.Longitude);
                    GlobalData.autoPilotReady = true;
                }
            }
            else
            {
                readySL.Text = "Not Ready ";
                GlobalData.autoPilotReady = false;
            }

            /*Check For Mode*/
            if (!((status >> 5 & 1) == 1) && !((status >> 4 & 1) == 1))
            {
                radioButton3.Checked = true;
                GlobalData.currentMode = FlightMode.UAV;
            }
            else if (!((status >> 5 & 1) == 1) && ((status >> 4 & 1) == 1))
            {
                radioButton2.Checked = true;
                GlobalData.currentMode = FlightMode.RPV;
            }
            else if (((status >> 1 & 1) == 1))
            {

                picRadButton.Checked = true;
                GlobalData.currentMode = FlightMode.PIC;
            }

        }

        private void apStatus2Timer_Tick(object sender, EventArgs e)
        {
            /*Check If there are a fatal Errors Or Not */

            int fatalValue = myFlight.flyUAV.checkForFatalError();
            if ((fatalValue < 2 || fatalValue > 110))
            {
                fatalStrip.BackColor = Color.Green;
                fatalStrip.Text = "None";
            }
            else if (fatalValue == 72)
            {
                fatalStrip.BackColor = Color.Red;
                fatalStrip.Text = "Multiple Fatal Errors ";
            }
            else
            {
                fatalStrip.BackColor = Color.Red;
                fatalStrip.Text = fatalErrorsLookUPTable.fatalErrors.ElementAt(fatalValue - 2).Description;
            }

            /*Check For RC Status */
            int rcState = myFlight.flyUAV.getJoy(1156);

            /*Current Failure pattern */
            Color failureColor = Color.Red;
            failureStrip.Text = myFlight.flyUAV.getCurrentFailureAsString(ref failureColor);
            failureStrip.BackColor = failureColor;

            if (GlobalData.modeSimulatorOrAuto == 1)
            {

                myFlight.flyUAV.setJoy(1502, (int)0x40);
            }

            if (rcState == 0)
            {
                rcStausL.Text = "RC- OFF";
            }
            else if (rcState == 1)
            {
                float ch5 = myFlight.flyUAV.getJoy(1419) / 2000f;
                if (ch5 < 1)
                {
                    rcStausL.Text = "RC-OFF";
                }
                else if (ch5 > 1 && ch5 < 1.5)
                {
                    rcStausL.Text = "RC- ON";
                }
                else if (ch5 > 1.5 && ch5 < 2)
                {
                    rcStausL.Text = "RC- PIC";
                }
                else
                {
                    rcStausL.Text = "RC- OFF";
                }
            }

            else
            {
                rcStausL.Text = "RC- OFF";
            }



        }

        private void bindGaugesTimer_Tick(object sender, EventArgs e)
        {
            /*Progress Bars Values*/
            thrL.Text = "Throttle (" + myFlight.flyData.ThrottleValue + " %)";
            throttleProgressBar.Value = (int)myFlight.flyData.ThrottleValue;

            /*Main Battery Voltage */
            mbatL.Text = "Main Battery (" + myFlight.flyData.MBatValue + " %)";
            mBatProgreesbar.Value = (int)myFlight.flyData.MBatValue;

            /*Servo Battery Voltage */
            sBatL.Text = "Servo Battery (" + myFlight.flyData.SBatValue + " %)";
            sBatProgressBar.Value = (int)myFlight.flyData.SBatValue;

            /*AGL alue*/
            aglHeighL.Text = "" + myFlight.flyData.AglHeight;

            /*Gauges Values*/
            attitudeIndicatorInstrumentControl2.SetAttitudeIndicatorParameters(myFlight.flyData.Pitch, myFlight.flyData.Roll);
            airSpeedIndicatorInstrumentControl2.SetAirSpeedIndicatorParameters((int)myFlight.flyData.AirSpeed);
            headingIndicatorInstrumentControl2.SetHeadingIndicatorParameters((int)myFlight.flyData.Heading);
            altimeterInstrumentControl2.SetAlimeterParameters((int)myFlight.flyData.Altitude);

        }

        private void graphTimer_Tick(object sender, EventArgs e)
        {
            try
            {

                GlobalData.time++;
                if (INIData.AltEnabled)
                {
                    ZedGraph.PointPair nP = new ZedGraph.PointPair(GlobalData.time, myFlight.flyUAV.getAltitude());
                    myGraph.addPointToCurve(0, nP, INIData.aLColor, "Altitude");
                }
                if (INIData.HeadingEnabled)
                {
                    ZedGraph.PointPair nP = new ZedGraph.PointPair(GlobalData.time, myFlight.flyUAV.getHeading());
                    myGraph.addPointToCurve(0, nP, INIData.hColor, "Heading");
                }
                if (INIData.ASpeedEnabled)
                {
                    ZedGraph.PointPair nP = new ZedGraph.PointPair(GlobalData.time, myFlight.flyUAV.getAirSpeed());
                    myGraph.addPointToCurve(0, nP, INIData.aSColor, "Air Speed");
                }


            }
            catch (Exception h)
            {
                GlobalData.LogConsole(h.Message + "at " + this.Name + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());

            }
        }

        private void labelsTimer_Tick(object sender, EventArgs e)
        {
            try
            {


                /*Display in MTN Data from Scratch fields */

                NavLatdis.Text = "" + MTNState.latitude;
                LongNavDis.Text = "" + MTNState.longitude;
                AltNavDis.Text = "" + MTNState.Altitude;
                GPSNAVStatus.Text = MTNState.Gps_State == 1 ? "GPS Signal Available" : "No GPS Signal Available";
                packetsCountDisplay.Text = "" + MTNState.rightpackets + " / " + MTNState.Totalpackets + " / " + MTNState.RateTotalpackets;
                modeNavLabel.Text = MTNState.NaV_State == Navigation_Status.Alignment ? "Alignment " : "Navigation";
                label3.Text = "" + myFlight.flyData.Latitude;
                label4.Text = "" + myFlight.flyData.Longitude;
                label5.Text = "" + myFlight.flyData.Altitude;

                label15.Text = "" + myFlight.flyUAV.getJoy(1562); //MTNState.VelocityE;
                label16.Text = "" + myFlight.flyUAV.getJoy(1563); //MTNState.VelocityN;
                label17.Text = "" + myFlight.flyUAV.getJoy(1564);//MTNState.yaw;

                label18.Text = "" + MTNState.pitch;
                label19.Text = "" + MTNState.roll;
                label20.Text = "" + MTNState.yaw;

                label6.Text = "" + myFlight.flyData.Pitch;
                label7.Text = "" + myFlight.flyData.Roll; ;
                label11.Text = "" + myFlight.flyData.Yaw; ;
                label24.Text = "" + myFlight.flyUAV.getJoy(1224);



                /*Unbind Labelas*/
                DTNWPTL.Text = "" + myFlight.flyData.DTNWP;
                AltitudeLL.Text = "" + myFlight.flyData.Altitude;
                velocityL.Text = "" + myFlight.flyData.AngularVelocity;
                altitudeL.Text = "" + myFlight.flyData.Altitude;
                HeadingL.Text = "" + myFlight.flyData.Heading;
                headingStL.Text = "" + myFlight.flyData.Heading;
                DistTONXPL.Text = "" + myFlight.flyData.DTNWP;
                yawL.Text = "" + myFlight.flyData.Yaw;
                airSpeedL.Text = "" + myFlight.flyData.AirSpeed;

                
            }
            catch (Exception g)
            {
                GlobalData.LogConsole(g.Message + "at " + this.Name + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());

            }
        }

        private void simStatusTimer_Tick(object sender, EventArgs e)
        {
            if (GlobalData.modeSimulatorOrAuto == 1)
            {
                String ErMSG = "";
                Int32 SimulationStatus = myFlight.flyUAV.checkSimStatus(ref ErMSG);
                if (SimulationStatus == UAV.MP_MORE || SimulationStatus == UAV.MP_OK)
                {
                    // Do Nothing up To Now
                }
                else if (SimulationStatus == UAV.MP_SIM_OVER)
                {

                    //simStatusTimer.Stop();
                    //simStatusTimer.Enabled = false;
                    stopAllThreads();
                    MessageBox.Show("Simulation has Completed", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {

                    //simStatusTimer.Stop();
                    //simStatusTimer.Enabled = false;
                    stopAllThreads();
                    MessageBox.Show(ErMSG, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
        }


    }
}
