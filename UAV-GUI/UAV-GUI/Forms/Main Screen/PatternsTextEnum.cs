﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAV_GUI
{
    public static class PatternsTextEnum
    {
        public readonly static String Hover = "Hover here";
        public readonly static String Figure8= "Figure8";
        public readonly static String Land_here ="Land here";
        public readonly static String Land_at_Home ="Land at Home";
        public readonly static String Stop_Engine = "Stop Engine";
        public readonly static String Fly_Home ="Fly Home";
        public readonly static String Circle_left ="Circle left";
        public readonly static String Circle_Right="Circle Right";
        public readonly static String Take_Picture = "Take Picture";
        public readonly static String Orbit_left ="Orbit left";
        public readonly static String Orbit_Right = "Orbit Right";
        public readonly static String Descent_Here="Descent Here";
    }
}
