﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using  System.Runtime.InteropServices;
namespace UAV_GUI.Forms.Set_Variable
{

  [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)] 
    public struct MPVARINFO
        {
          public Int32 cbSize;		/*!< @brief Always init to sizeof(MPVARINFO) before passing to any functions */
           [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
          public String nameStr;  /*!< @brief Name of the field				*/
           [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]      
          public String unitsStr; /*!< @brief Unit type of the field			*/
           [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]      
          public String hintStr;  /*!< @brief Hint string for the field		*/
          public Int32 entryId;       /*!< @brief Field's array position number	*/
        };
  public enum MPFieldsUnits
  {
      Heading
      , Orientation,
      FineServo,
      CoarseServo,
      Velocity,
      Acceleration,
      RelativePostion,
      Altitude,
      Distance,
      Absolute_position,
      Time,
      Raw

  }
   static class  MPField
    {
        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
       private static extern Int32  mpReadVarNameEx(int PlaneId, ref MPVARINFO pVarInfo, int varId) ;

       
       [DllImport("multi-uav.dll", CallingConvention = CallingConvention.StdCall)]
       [return: MarshalAs(UnmanagedType.I4)]
       private static extern Int32 mpCreate(int PlaneId);


       [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
       [return: MarshalAs(UnmanagedType.I4)]
       private static extern Int32 mpInitFly(int PlaneId, string filename);

       ///Need to Add A check if UAV#1 is already Created. 
        
       public static int getVarInfo (int fieldId,ref MPVARINFO mpField){
           int retVal = 0;
           try
            {
                if(System.IO.File.Exists("uav1.dll")){
                int mperr =MPField.mpCreate(1);
                retVal=mperr = MPField.mpInitFly(1, null);
                }
                //MPVARINFO mpVar = new MPVARINFO () ;
                mpField.cbSize = System.Runtime.InteropServices.Marshal.SizeOf(typeof(MPVARINFO)); ;
               // UserTelemetryFile ss = new UserTelemetryFile("");
               retVal= MPField.mpReadVarNameEx(1, ref mpField, fieldId);
                int x = 0;
                return retVal;
            }
            catch
           {
               return retVal;
                  
            }
            
           
       
       }

     public static  MPFieldsUnits getUnitsFromString(String unitstr)
       {
           if (unitstr.Contains("feet per second"))
           {
               return MPFieldsUnits.Velocity;
           }
           else if (unitstr.Contains("Heading in degrees * 100 (0 to 36000)"))
           {
               return MPFieldsUnits.Heading;
           }
           else if (unitstr.Contains("Radians*1024"))
           {
               return MPFieldsUnits.Orientation;
           }
           else if (unitstr.Contains("-32,767 to 32,767"))
           {
               return MPFieldsUnits.FineServo;
           }
           else if (unitstr.Contains("2000 to 4000 covers the full servo range"))
           {
               return MPFieldsUnits.CoarseServo;
           }
           else if (unitstr.Contains("G*100"))
           {
               return MPFieldsUnits.Acceleration;
           }
           else if (unitstr.Contains("Radians*500,000,000"))
           {
               return MPFieldsUnits.Absolute_position;
           }
           else if (unitstr.Contains("negative feet*8"))
           {
               return MPFieldsUnits.Altitude;
           }
           else if (unitstr.Contains("feet*8"))
           {
               return MPFieldsUnits.Distance;
           }
           return MPFieldsUnits.Raw;
       }
    }
}
