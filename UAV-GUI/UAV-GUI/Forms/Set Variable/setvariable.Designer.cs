﻿namespace UAV_GUI.Forms.Set_Variable
{
    partial class setvariable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.variablename = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.setvalue = new System.Windows.Forms.Button();
            this.requiredvalue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.flashsaving = new System.Windows.Forms.Button();
            this.description = new System.Windows.Forms.Label();
            this.currentvalue = new System.Windows.Forms.Label();
            this.displaycurrentvalue = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // variablename
            // 
            this.variablename.AutoCompleteCustomSource.AddRange(new string[] {
            "GPS1baud",
            "GPS1bits",
            "GPS1fixrate"});
            this.variablename.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.variablename.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.variablename.Location = new System.Drawing.Point(154, 21);
            this.variablename.Name = "variablename";
            this.variablename.Size = new System.Drawing.Size(100, 20);
            this.variablename.TabIndex = 0;
            this.variablename.TextChanged += new System.EventHandler(this.variablename_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Field ID :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Description";
            // 
            // setvalue
            // 
            this.setvalue.Location = new System.Drawing.Point(386, 12);
            this.setvalue.Name = "setvalue";
            this.setvalue.Size = new System.Drawing.Size(106, 38);
            this.setvalue.TabIndex = 3;
            this.setvalue.Text = "Set Field Value";
            this.setvalue.UseVisualStyleBackColor = true;
            this.setvalue.Click += new System.EventHandler(this.setvalue_Click);
            // 
            // requiredvalue
            // 
            this.requiredvalue.Location = new System.Drawing.Point(153, 64);
            this.requiredvalue.Name = "requiredvalue";
            this.requiredvalue.Size = new System.Drawing.Size(98, 20);
            this.requiredvalue.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Current Value";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(39, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Required Value";
            // 
            // flashsaving
            // 
            this.flashsaving.Location = new System.Drawing.Point(386, 133);
            this.flashsaving.Name = "flashsaving";
            this.flashsaving.Size = new System.Drawing.Size(106, 38);
            this.flashsaving.TabIndex = 8;
            this.flashsaving.Text = "Save To Flash";
            this.flashsaving.UseVisualStyleBackColor = true;
            this.flashsaving.Click += new System.EventHandler(this.flashsaving_Click);
            // 
            // description
            // 
            this.description.AutoSize = true;
            this.description.Location = new System.Drawing.Point(150, 158);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(0, 13);
            this.description.TabIndex = 9;
            // 
            // currentvalue
            // 
            this.currentvalue.AutoSize = true;
            this.currentvalue.Location = new System.Drawing.Point(156, 117);
            this.currentvalue.Name = "currentvalue";
            this.currentvalue.Size = new System.Drawing.Size(0, 13);
            this.currentvalue.TabIndex = 10;
            // 
            // displaycurrentvalue
            // 
            this.displaycurrentvalue.Location = new System.Drawing.Point(386, 70);
            this.displaycurrentvalue.Name = "displaycurrentvalue";
            this.displaycurrentvalue.Size = new System.Drawing.Size(106, 39);
            this.displaycurrentvalue.TabIndex = 11;
            this.displaycurrentvalue.Text = "Display Field Value";
            this.displaycurrentvalue.UseVisualStyleBackColor = true;
            this.displaycurrentvalue.Click += new System.EventHandler(this.displaycurrentvalue_Click);
            // 
            // setvariable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 235);
            this.Controls.Add(this.displaycurrentvalue);
            this.Controls.Add(this.currentvalue);
            this.Controls.Add(this.description);
            this.Controls.Add(this.flashsaving);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.requiredvalue);
            this.Controls.Add(this.setvalue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.variablename);
            this.Name = "setvariable";
            this.Text = "Change Parameter Utility";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.setvariable_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox variablename;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button setvalue;
        private System.Windows.Forms.TextBox requiredvalue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button flashsaving;
        private System.Windows.Forms.Label description;
        private System.Windows.Forms.Label currentvalue;
        private System.Windows.Forms.Button displaycurrentvalue;
    }
}