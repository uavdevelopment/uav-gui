﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UAV_GUI.FlightClasses;
using MP_Lib;
using FileClasses.VRS_Files;
using AutoPilotMemory;
using System.Threading;
using Error_Reporting;
using GLobal;
namespace UAV_GUI.Forms.Set_Variable
{
    public partial class setvariable : Form
    {

        string fieldName;
        int fieldvalue;
        int fieldId;
        MPFieldsUnits fieldUnits;
        String fieldDes;
        public UAV myUav;


        public setvariable(UAV myUav_)
        {
            
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
            myUav = myUav_;
            if (!GlobalData.apConnected) {
               bool retValue =  myUav_.connect("", 0);
               if (!retValue) 
                    throw new BusinessException(1025, "Unable to establish connection , Check an autopilot Connected" );
            }
            
        }

        private void variablename_TextChanged(object sender, EventArgs e)
        {

            int field_id;

            if (int.TryParse(variablename.Text, out field_id))
            {
                if (field_id == 1) return;
                fieldId = field_id;
                int retvalue = -1;
                retvalue = myUav.read_value(ref fieldvalue, fieldId);
                if (retvalue == 0)
                {

                    MPVARINFO mpVar = new MPVARINFO();

                    // The condition of field id =1 here 
                    if (MPField.getVarInfo(fieldId, ref mpVar) != 0 || fieldId != 1)
                    {
                        this.fieldName = "Field Name " + fieldId;
                        currentvalue.Text = fieldvalue.ToString();
                        this.fieldUnits = MPField.getUnitsFromString("");
                        this.fieldDes = mpVar.nameStr;
                        description.Text = "Field Name :" + mpVar.nameStr + Environment.NewLine + "Field Units :" + mpVar.unitsStr;

                    }
                    else
                    {

                        this.fieldName = "Field Name " + fieldId;
                        //  currentvalue.Text = "Field Name " + fieldId;
                        this.fieldName = mpVar.nameStr;
                        currentvalue.Text = fieldvalue.ToString();
                        //this.fieldUnits = getUnitsFromString(mpVar.unitsStr);
                        // this.fieldDes = mpVar.nameStr;

                        description.Text = "Field Name :" + mpVar.nameStr + Environment.NewLine + "Field ID :" + field_id;
                    }
                }
                else
                {
                    throw new BusinessException(1019, "unable to read value " + "Error Number " + retvalue);
                }
            }
            else {
                currentvalue.Text = "";
                description.Text = "";
            
            }
                
           
            
            
            // currentvalue.Text = vrsHandler.searchByFieldID_getParameter_value((int)fieldsID.MPFID_GPS_BAUDRATE).ToString();
        }

        private void setvalue_Click(object sender, EventArgs e)
        {
          //  vrsHandler.vrsFile.VrsLines[vrsHandler.searchByFieldID_getListIndex((int)fieldsID.MPFID_GPS_BAUDRATE)].Parameter_value = int.Parse(requiredvalue.Text);


                 int value;
                 int retvalue1=-1;
        

                 if (!string.IsNullOrWhiteSpace(requiredvalue.Text))
                       {
                 if (int.TryParse(requiredvalue.Text, out value))
                 {
                     currentvalue.Text = requiredvalue.Text;
                     retvalue1 = myUav.set_value_variable(value, fieldId);
                     if (retvalue1 != 0)
                     {

                         throw new BusinessException(1021, "Unable to wrire value" + " Error Number " + retvalue1);
                     }
                     else { 
                     
                          MessageBox.Show("Parameter Changed temporatily , Save flash option make it permanently", "Successfuly Tuned",MessageBoxButtons.OK, MessageBoxIcon.Information);
                     }
                   
                 }
                        }

                 else
                 
                {
                    throw new BusinessException(1020, "Make sure you entered correct values");
                }

            //or use this 

            
            

        }

        private void flashsaving_Click(object sender, EventArgs e)
        {
            int retvalue = -1;
            DialogResult myResult = MessageBox.Show("All current params will be saved permanenetly ", "Attention required", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

            if (myResult == DialogResult.Cancel)
            {
                MessageBox.Show("No parameters will be saved , power cycle will retsore defaults ", "Aborted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            retvalue = myUav.save_flash();
            if (retvalue != 0)
            {
                throw new BusinessException(1018, "Please Try Again" + "Error Number " + retvalue);
            }
            else 
            {
                MessageBox.Show("All Parameters have been saved to permanenetly ", "Successfuly Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                  
            }

        }

        private void displaycurrentvalue_Click(object sender, EventArgs e)
        {
            variablename_TextChanged(this.variablename, null);
        }

        private void setvariable_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!GlobalData.apConnected) {
                do
                {

                    myUav.disConnect();

                } while (false);
            
            }
        }


    }
}
