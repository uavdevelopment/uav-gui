﻿namespace JoystickPlugin
{
    partial class axisConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.buttonLable = new System.Windows.Forms.TextBox();
            this.cancle = new System.Windows.Forms.Button();
            this.ok = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.minValue = new System.Windows.Forms.TextBox();
            this.maxValue = new System.Windows.Forms.TextBox();
            this.filedId = new System.Windows.Forms.TextBox();
            this.PRV_Control = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.PIC_Control = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "axis Label";
            // 
            // buttonLable
            // 
            this.buttonLable.Location = new System.Drawing.Point(98, 141);
            this.buttonLable.Name = "buttonLable";
            this.buttonLable.Size = new System.Drawing.Size(100, 20);
            this.buttonLable.TabIndex = 23;
            // 
            // cancle
            // 
            this.cancle.Location = new System.Drawing.Point(123, 167);
            this.cancle.Name = "cancle";
            this.cancle.Size = new System.Drawing.Size(75, 23);
            this.cancle.TabIndex = 25;
            this.cancle.Text = "cancle";
            this.cancle.UseVisualStyleBackColor = true;
            this.cancle.Click += new System.EventHandler(this.cancle_Click);
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(42, 167);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(75, 23);
            this.ok.TabIndex = 24;
            this.ok.Text = "ok";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "max value";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "min value";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "field ID";
            // 
            // minValue
            // 
            this.minValue.Location = new System.Drawing.Point(98, 89);
            this.minValue.Name = "minValue";
            this.minValue.Size = new System.Drawing.Size(100, 20);
            this.minValue.TabIndex = 21;
            // 
            // maxValue
            // 
            this.maxValue.Location = new System.Drawing.Point(98, 115);
            this.maxValue.Name = "maxValue";
            this.maxValue.Size = new System.Drawing.Size(100, 20);
            this.maxValue.TabIndex = 22;
            // 
            // filedId
            // 
            this.filedId.Location = new System.Drawing.Point(98, 63);
            this.filedId.Name = "filedId";
            this.filedId.Size = new System.Drawing.Size(100, 20);
            this.filedId.TabIndex = 20;
            // 
            // PRV_Control
            // 
            this.PRV_Control.FormattingEnabled = true;
            this.PRV_Control.Location = new System.Drawing.Point(98, 36);
            this.PRV_Control.Name = "PRV_Control";
            this.PRV_Control.Size = new System.Drawing.Size(100, 21);
            this.PRV_Control.TabIndex = 41;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "PRV Control";
            // 
            // PIC_Control
            // 
            this.PIC_Control.FormattingEnabled = true;
            this.PIC_Control.Location = new System.Drawing.Point(98, 9);
            this.PIC_Control.Name = "PIC_Control";
            this.PIC_Control.Size = new System.Drawing.Size(100, 21);
            this.PIC_Control.TabIndex = 39;
            this.PIC_Control.SelectedIndexChanged += new System.EventHandler(this.PIC_Control_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 38;
            this.label5.Text = "PIC Control";
            // 
            // axisConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(206, 201);
            this.Controls.Add(this.PRV_Control);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.PIC_Control);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonLable);
            this.Controls.Add(this.cancle);
            this.Controls.Add(this.ok);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.minValue);
            this.Controls.Add(this.maxValue);
            this.Controls.Add(this.filedId);
            this.Name = "axisConfig";
            this.Text = "axisConfig";
            this.Load += new System.EventHandler(this.axisConfig_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox buttonLable;
        private System.Windows.Forms.Button cancle;
        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox minValue;
        private System.Windows.Forms.TextBox maxValue;
        private System.Windows.Forms.TextBox filedId;
        private System.Windows.Forms.ComboBox PRV_Control;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox PIC_Control;
        private System.Windows.Forms.Label label5;
    }
}