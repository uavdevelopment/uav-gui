﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.DirectX.DirectInput;

namespace JoystickPlugin
{
    class joystick
    {
        public static Device joy;


        public static DeviceList populate_Joysticks()
        {
            return Manager.GetDevices(DeviceType.Joystick, EnumDevicesFlags.AttachedOnly);
        }

        public static void select_Joystick(string name, IntPtr hwnd)
        {

            foreach (DeviceInstance di in Manager.GetDevices(DeviceType.Joystick, EnumDevicesFlags.AttachedOnly))
            {

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"debug.txt", true))
                {
                    file.WriteLine(name + ", " + di.InstanceName);
                }


                if ((di.InstanceName + di.InstanceGuid) == name)
                {
                    joy = new Device(di.InstanceGuid);
                    joy.Properties.AxisModeAbsolute = true;
                    joy.Acquire();
                }
            }
        }

    }
}
