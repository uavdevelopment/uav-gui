﻿namespace JoystickPlugin
{
    partial class buttonConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.buttonLable = new System.Windows.Forms.TextBox();
            this.cancle = new System.Windows.Forms.Button();
            this.ok = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.firstValue = new System.Windows.Forms.TextBox();
            this.secondValue = new System.Windows.Forms.TextBox();
            this.filedId = new System.Windows.Forms.TextBox();
            this.buttonAction = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonFunction = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Button Label";
            // 
            // buttonLable
            // 
            this.buttonLable.Location = new System.Drawing.Point(97, 145);
            this.buttonLable.Name = "buttonLable";
            this.buttonLable.Size = new System.Drawing.Size(100, 20);
            this.buttonLable.TabIndex = 14;
            // 
            // cancle
            // 
            this.cancle.Location = new System.Drawing.Point(116, 171);
            this.cancle.Name = "cancle";
            this.cancle.Size = new System.Drawing.Size(75, 23);
            this.cancle.TabIndex = 18;
            this.cancle.Text = "cancle";
            this.cancle.UseVisualStyleBackColor = true;
            this.cancle.Click += new System.EventHandler(this.cancle_Click);
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(35, 171);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(75, 23);
            this.ok.TabIndex = 16;
            this.ok.Text = "ok";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "2nd value";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "1st value";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "field ID";
            // 
            // firstValue
            // 
            this.firstValue.Location = new System.Drawing.Point(98, 89);
            this.firstValue.Name = "firstValue";
            this.firstValue.Size = new System.Drawing.Size(100, 20);
            this.firstValue.TabIndex = 11;
            // 
            // secondValue
            // 
            this.secondValue.Location = new System.Drawing.Point(98, 115);
            this.secondValue.Name = "secondValue";
            this.secondValue.Size = new System.Drawing.Size(100, 20);
            this.secondValue.TabIndex = 13;
            // 
            // filedId
            // 
            this.filedId.Location = new System.Drawing.Point(98, 63);
            this.filedId.Name = "filedId";
            this.filedId.Size = new System.Drawing.Size(100, 20);
            this.filedId.TabIndex = 10;
            // 
            // buttonAction
            // 
            this.buttonAction.FormattingEnabled = true;
            this.buttonAction.Location = new System.Drawing.Point(98, 36);
            this.buttonAction.Name = "buttonAction";
            this.buttonAction.Size = new System.Drawing.Size(100, 21);
            this.buttonAction.TabIndex = 37;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "Button Action";
            // 
            // buttonFunction
            // 
            this.buttonFunction.FormattingEnabled = true;
            this.buttonFunction.Location = new System.Drawing.Point(98, 9);
            this.buttonFunction.Name = "buttonFunction";
            this.buttonFunction.Size = new System.Drawing.Size(100, 21);
            this.buttonFunction.TabIndex = 35;
            this.buttonFunction.SelectedValueChanged += new System.EventHandler(this.buttonFunction_SelectedValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "Button Function";
            // 
            // buttonConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(210, 202);
            this.Controls.Add(this.buttonAction);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buttonFunction);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonLable);
            this.Controls.Add(this.cancle);
            this.Controls.Add(this.ok);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.firstValue);
            this.Controls.Add(this.secondValue);
            this.Controls.Add(this.filedId);
            this.Name = "buttonConfig";
            this.Text = "buttonConfig";
            this.Load += new System.EventHandler(this.buttonConfig_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox buttonLable;
        private System.Windows.Forms.Button cancle;
        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox firstValue;
        private System.Windows.Forms.TextBox secondValue;
        private System.Windows.Forms.TextBox filedId;
        private System.Windows.Forms.ComboBox buttonAction;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox buttonFunction;
        private System.Windows.Forms.Label label5;
    }
}