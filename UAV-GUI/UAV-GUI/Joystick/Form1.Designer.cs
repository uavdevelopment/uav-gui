﻿namespace JoystickPlugin
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CVZ = new System.Windows.Forms.TextBox();
            this.LVZ = new System.Windows.Forms.Label();
            this.CVY = new System.Windows.Forms.TextBox();
            this.VZ = new System.Windows.Forms.TrackBar();
            this.CVX = new System.Windows.Forms.TextBox();
            this.VY = new System.Windows.Forms.TrackBar();
            this.VX = new System.Windows.Forms.TrackBar();
            this.label4 = new System.Windows.Forms.Label();
            this.LVY = new System.Windows.Forms.Label();
            this.LVX = new System.Windows.Forms.Label();
            this.CZ = new System.Windows.Forms.TextBox();
            this.CY = new System.Windows.Forms.TextBox();
            this.CX = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Z = new System.Windows.Forms.TrackBar();
            this.Y = new System.Windows.Forms.TrackBar();
            this.LZ = new System.Windows.Forms.Label();
            this.LY = new System.Windows.Forms.Label();
            this.LX = new System.Windows.Forms.Label();
            this.X = new System.Windows.Forms.TrackBar();
            this.CRz = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.CRy = new System.Windows.Forms.TextBox();
            this.CRx = new System.Windows.Forms.TextBox();
            this.LRz = new System.Windows.Forms.Label();
            this.LRy = new System.Windows.Forms.Label();
            this.LRx = new System.Windows.Forms.Label();
            this.Rz = new System.Windows.Forms.TrackBar();
            this.Rx = new System.Windows.Forms.TrackBar();
            this.Ry = new System.Windows.Forms.TrackBar();
            this.VRz = new System.Windows.Forms.TrackBar();
            this.fieldList = new System.Windows.Forms.ListBox();
            this.connection_monitor = new System.Windows.Forms.TabPage();
            this.Button128 = new System.Windows.Forms.RadioButton();
            this.Button127 = new System.Windows.Forms.RadioButton();
            this.Button126 = new System.Windows.Forms.RadioButton();
            this.Button125 = new System.Windows.Forms.RadioButton();
            this.Button124 = new System.Windows.Forms.RadioButton();
            this.Button123 = new System.Windows.Forms.RadioButton();
            this.Button122 = new System.Windows.Forms.RadioButton();
            this.Button121 = new System.Windows.Forms.RadioButton();
            this.Button120 = new System.Windows.Forms.RadioButton();
            this.Button119 = new System.Windows.Forms.RadioButton();
            this.Button118 = new System.Windows.Forms.RadioButton();
            this.Button117 = new System.Windows.Forms.RadioButton();
            this.Button116 = new System.Windows.Forms.RadioButton();
            this.Button115 = new System.Windows.Forms.RadioButton();
            this.Button114 = new System.Windows.Forms.RadioButton();
            this.Button113 = new System.Windows.Forms.RadioButton();
            this.Button112 = new System.Windows.Forms.RadioButton();
            this.Button111 = new System.Windows.Forms.RadioButton();
            this.Button110 = new System.Windows.Forms.RadioButton();
            this.Button109 = new System.Windows.Forms.RadioButton();
            this.Buttons = new System.Windows.Forms.TabPage();
            this.Button108 = new System.Windows.Forms.RadioButton();
            this.Button107 = new System.Windows.Forms.RadioButton();
            this.Button106 = new System.Windows.Forms.RadioButton();
            this.Button105 = new System.Windows.Forms.RadioButton();
            this.Button104 = new System.Windows.Forms.RadioButton();
            this.Button103 = new System.Windows.Forms.RadioButton();
            this.Button102 = new System.Windows.Forms.RadioButton();
            this.Button101 = new System.Windows.Forms.RadioButton();
            this.Button100 = new System.Windows.Forms.RadioButton();
            this.Button99 = new System.Windows.Forms.RadioButton();
            this.Button98 = new System.Windows.Forms.RadioButton();
            this.Button97 = new System.Windows.Forms.RadioButton();
            this.Button96 = new System.Windows.Forms.RadioButton();
            this.Button95 = new System.Windows.Forms.RadioButton();
            this.Button94 = new System.Windows.Forms.RadioButton();
            this.Button93 = new System.Windows.Forms.RadioButton();
            this.Button92 = new System.Windows.Forms.RadioButton();
            this.Button91 = new System.Windows.Forms.RadioButton();
            this.Button90 = new System.Windows.Forms.RadioButton();
            this.Button89 = new System.Windows.Forms.RadioButton();
            this.Button88 = new System.Windows.Forms.RadioButton();
            this.Button87 = new System.Windows.Forms.RadioButton();
            this.Button86 = new System.Windows.Forms.RadioButton();
            this.Button85 = new System.Windows.Forms.RadioButton();
            this.Button84 = new System.Windows.Forms.RadioButton();
            this.Button83 = new System.Windows.Forms.RadioButton();
            this.Button82 = new System.Windows.Forms.RadioButton();
            this.Button81 = new System.Windows.Forms.RadioButton();
            this.Button48 = new System.Windows.Forms.RadioButton();
            this.Button47 = new System.Windows.Forms.RadioButton();
            this.Button46 = new System.Windows.Forms.RadioButton();
            this.Button45 = new System.Windows.Forms.RadioButton();
            this.Button44 = new System.Windows.Forms.RadioButton();
            this.Button43 = new System.Windows.Forms.RadioButton();
            this.Button42 = new System.Windows.Forms.RadioButton();
            this.Button41 = new System.Windows.Forms.RadioButton();
            this.Button40 = new System.Windows.Forms.RadioButton();
            this.Button39 = new System.Windows.Forms.RadioButton();
            this.Button38 = new System.Windows.Forms.RadioButton();
            this.Button37 = new System.Windows.Forms.RadioButton();
            this.Button36 = new System.Windows.Forms.RadioButton();
            this.Button80 = new System.Windows.Forms.RadioButton();
            this.Button79 = new System.Windows.Forms.RadioButton();
            this.Button78 = new System.Windows.Forms.RadioButton();
            this.Button77 = new System.Windows.Forms.RadioButton();
            this.Button76 = new System.Windows.Forms.RadioButton();
            this.Button75 = new System.Windows.Forms.RadioButton();
            this.Button74 = new System.Windows.Forms.RadioButton();
            this.Button73 = new System.Windows.Forms.RadioButton();
            this.Button72 = new System.Windows.Forms.RadioButton();
            this.Button71 = new System.Windows.Forms.RadioButton();
            this.Button70 = new System.Windows.Forms.RadioButton();
            this.Button69 = new System.Windows.Forms.RadioButton();
            this.Button68 = new System.Windows.Forms.RadioButton();
            this.Button67 = new System.Windows.Forms.RadioButton();
            this.Button66 = new System.Windows.Forms.RadioButton();
            this.Button65 = new System.Windows.Forms.RadioButton();
            this.Button64 = new System.Windows.Forms.RadioButton();
            this.Button63 = new System.Windows.Forms.RadioButton();
            this.Button62 = new System.Windows.Forms.RadioButton();
            this.Button61 = new System.Windows.Forms.RadioButton();
            this.Button60 = new System.Windows.Forms.RadioButton();
            this.Button59 = new System.Windows.Forms.RadioButton();
            this.Button58 = new System.Windows.Forms.RadioButton();
            this.Button57 = new System.Windows.Forms.RadioButton();
            this.Button56 = new System.Windows.Forms.RadioButton();
            this.Button55 = new System.Windows.Forms.RadioButton();
            this.Button54 = new System.Windows.Forms.RadioButton();
            this.Button53 = new System.Windows.Forms.RadioButton();
            this.Button52 = new System.Windows.Forms.RadioButton();
            this.Button51 = new System.Windows.Forms.RadioButton();
            this.Button50 = new System.Windows.Forms.RadioButton();
            this.Button49 = new System.Windows.Forms.RadioButton();
            this.Button35 = new System.Windows.Forms.RadioButton();
            this.Button34 = new System.Windows.Forms.RadioButton();
            this.Button33 = new System.Windows.Forms.RadioButton();
            this.Button32 = new System.Windows.Forms.RadioButton();
            this.Button31 = new System.Windows.Forms.RadioButton();
            this.Button30 = new System.Windows.Forms.RadioButton();
            this.Button29 = new System.Windows.Forms.RadioButton();
            this.Button28 = new System.Windows.Forms.RadioButton();
            this.Button27 = new System.Windows.Forms.RadioButton();
            this.Button26 = new System.Windows.Forms.RadioButton();
            this.Button25 = new System.Windows.Forms.RadioButton();
            this.Button24 = new System.Windows.Forms.RadioButton();
            this.Button23 = new System.Windows.Forms.RadioButton();
            this.Button22 = new System.Windows.Forms.RadioButton();
            this.Button21 = new System.Windows.Forms.RadioButton();
            this.Button20 = new System.Windows.Forms.RadioButton();
            this.Button19 = new System.Windows.Forms.RadioButton();
            this.Button18 = new System.Windows.Forms.RadioButton();
            this.Button17 = new System.Windows.Forms.RadioButton();
            this.Button16 = new System.Windows.Forms.RadioButton();
            this.Button15 = new System.Windows.Forms.RadioButton();
            this.Button14 = new System.Windows.Forms.RadioButton();
            this.Button13 = new System.Windows.Forms.RadioButton();
            this.Button12 = new System.Windows.Forms.RadioButton();
            this.Button11 = new System.Windows.Forms.RadioButton();
            this.Button10 = new System.Windows.Forms.RadioButton();
            this.Button9 = new System.Windows.Forms.RadioButton();
            this.Button8 = new System.Windows.Forms.RadioButton();
            this.Button7 = new System.Windows.Forms.RadioButton();
            this.Button6 = new System.Windows.Forms.RadioButton();
            this.Button5 = new System.Windows.Forms.RadioButton();
            this.Button4 = new System.Windows.Forms.RadioButton();
            this.Button3 = new System.Windows.Forms.RadioButton();
            this.Button2 = new System.Windows.Forms.RadioButton();
            this.Button1 = new System.Windows.Forms.RadioButton();
            this.FRz = new System.Windows.Forms.TrackBar();
            this.joystickList = new System.Windows.Forms.ComboBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.Axis = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.CFRz = new System.Windows.Forms.TextBox();
            this.LFRz = new System.Windows.Forms.Label();
            this.CFRy = new System.Windows.Forms.TextBox();
            this.LFRy = new System.Windows.Forms.Label();
            this.CFRx = new System.Windows.Forms.TextBox();
            this.LFRx = new System.Windows.Forms.Label();
            this.FRx = new System.Windows.Forms.TrackBar();
            this.FRy = new System.Windows.Forms.TrackBar();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.CFZ = new System.Windows.Forms.TextBox();
            this.LFZ = new System.Windows.Forms.Label();
            this.CFY = new System.Windows.Forms.TextBox();
            this.FZ = new System.Windows.Forms.TrackBar();
            this.CFX = new System.Windows.Forms.TextBox();
            this.FY = new System.Windows.Forms.TrackBar();
            this.FX = new System.Windows.Forms.TrackBar();
            this.label10 = new System.Windows.Forms.Label();
            this.LFY = new System.Windows.Forms.Label();
            this.LFX = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.CARz = new System.Windows.Forms.TextBox();
            this.LARz = new System.Windows.Forms.Label();
            this.CARy = new System.Windows.Forms.TextBox();
            this.LARy = new System.Windows.Forms.Label();
            this.CARx = new System.Windows.Forms.TextBox();
            this.LARx = new System.Windows.Forms.Label();
            this.ARx = new System.Windows.Forms.TrackBar();
            this.ARz = new System.Windows.Forms.TrackBar();
            this.ARy = new System.Windows.Forms.TrackBar();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CAZ = new System.Windows.Forms.TextBox();
            this.LAZ = new System.Windows.Forms.Label();
            this.CAY = new System.Windows.Forms.TextBox();
            this.AZ = new System.Windows.Forms.TrackBar();
            this.CAX = new System.Windows.Forms.TextBox();
            this.AY = new System.Windows.Forms.TrackBar();
            this.AX = new System.Windows.Forms.TrackBar();
            this.label7 = new System.Windows.Forms.Label();
            this.LAY = new System.Windows.Forms.Label();
            this.LAX = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.CVRz = new System.Windows.Forms.TextBox();
            this.LVRz = new System.Windows.Forms.Label();
            this.CVRy = new System.Windows.Forms.TextBox();
            this.LVRy = new System.Windows.Forms.Label();
            this.CVRx = new System.Windows.Forms.TextBox();
            this.LVRx = new System.Windows.Forms.Label();
            this.VRx = new System.Windows.Forms.TrackBar();
            this.VRy = new System.Windows.Forms.TrackBar();
            this.selectJoystick = new System.Windows.Forms.Label();
            this.connect = new System.Windows.Forms.Button();
            this.Disconnect = new System.Windows.Forms.Button();
            this.FlagBox = new System.Windows.Forms.CheckBox();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VX)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Z)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.X)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Rz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VRz)).BeginInit();
            this.connection_monitor.SuspendLayout();
            this.Buttons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRz)).BeginInit();
            this.tabControl.SuspendLayout();
            this.Axis.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRy)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FX)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ARx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ARz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ARy)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AX)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VRx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VRy)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CVZ);
            this.groupBox2.Controls.Add(this.LVZ);
            this.groupBox2.Controls.Add(this.CVY);
            this.groupBox2.Controls.Add(this.VZ);
            this.groupBox2.Controls.Add(this.CVX);
            this.groupBox2.Controls.Add(this.VY);
            this.groupBox2.Controls.Add(this.VX);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.LVY);
            this.groupBox2.Controls.Add(this.LVX);
            this.groupBox2.Location = new System.Drawing.Point(193, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(180, 181);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "velocity";
            // 
            // CVZ
            // 
            this.CVZ.Location = new System.Drawing.Point(134, 120);
            this.CVZ.Name = "CVZ";
            this.CVZ.Size = new System.Drawing.Size(38, 20);
            this.CVZ.TabIndex = 33;
            // 
            // LVZ
            // 
            this.LVZ.AutoSize = true;
            this.LVZ.Location = new System.Drawing.Point(8, 121);
            this.LVZ.Name = "LVZ";
            this.LVZ.Size = new System.Drawing.Size(13, 13);
            this.LVZ.TabIndex = 9;
            this.LVZ.Text = "Z";
            this.LVZ.Click += new System.EventHandler(this.LFX_Click);
            // 
            // CVY
            // 
            this.CVY.Location = new System.Drawing.Point(134, 70);
            this.CVY.Name = "CVY";
            this.CVY.Size = new System.Drawing.Size(38, 20);
            this.CVY.TabIndex = 32;
            // 
            // VZ
            // 
            this.VZ.Location = new System.Drawing.Point(27, 119);
            this.VZ.Maximum = 65535;
            this.VZ.Name = "VZ";
            this.VZ.Size = new System.Drawing.Size(104, 45);
            this.VZ.TabIndex = 11;
            this.VZ.Value = 32768;
            // 
            // CVX
            // 
            this.CVX.Location = new System.Drawing.Point(135, 16);
            this.CVX.Name = "CVX";
            this.CVX.Size = new System.Drawing.Size(38, 20);
            this.CVX.TabIndex = 31;
            // 
            // VY
            // 
            this.VY.Location = new System.Drawing.Point(27, 68);
            this.VY.Maximum = 65535;
            this.VY.Name = "VY";
            this.VY.Size = new System.Drawing.Size(104, 45);
            this.VY.TabIndex = 10;
            this.VY.Value = 32768;
            // 
            // VX
            // 
            this.VX.Location = new System.Drawing.Point(27, 17);
            this.VX.Maximum = 65535;
            this.VX.Name = "VX";
            this.VX.Size = new System.Drawing.Size(104, 45);
            this.VX.TabIndex = 8;
            this.VX.Value = 32768;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 123);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Z";
            // 
            // LVY
            // 
            this.LVY.AutoSize = true;
            this.LVY.Location = new System.Drawing.Point(6, 71);
            this.LVY.Name = "LVY";
            this.LVY.Size = new System.Drawing.Size(13, 13);
            this.LVY.TabIndex = 4;
            this.LVY.Text = "Y";
            this.LVY.Click += new System.EventHandler(this.LFX_Click);
            // 
            // LVX
            // 
            this.LVX.AutoSize = true;
            this.LVX.Location = new System.Drawing.Point(6, 19);
            this.LVX.Name = "LVX";
            this.LVX.Size = new System.Drawing.Size(13, 13);
            this.LVX.TabIndex = 3;
            this.LVX.Text = "X";
            this.LVX.Click += new System.EventHandler(this.LFX_Click);
            // 
            // CZ
            // 
            this.CZ.Location = new System.Drawing.Point(135, 121);
            this.CZ.Name = "CZ";
            this.CZ.Size = new System.Drawing.Size(38, 20);
            this.CZ.TabIndex = 10;
            // 
            // CY
            // 
            this.CY.Location = new System.Drawing.Point(135, 71);
            this.CY.Name = "CY";
            this.CY.Size = new System.Drawing.Size(38, 20);
            this.CY.TabIndex = 9;
            // 
            // CX
            // 
            this.CX.Location = new System.Drawing.Point(136, 17);
            this.CX.Name = "CX";
            this.CX.Size = new System.Drawing.Size(38, 20);
            this.CX.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CZ);
            this.groupBox1.Controls.Add(this.CY);
            this.groupBox1.Controls.Add(this.CX);
            this.groupBox1.Controls.Add(this.Z);
            this.groupBox1.Controls.Add(this.Y);
            this.groupBox1.Controls.Add(this.LZ);
            this.groupBox1.Controls.Add(this.LY);
            this.groupBox1.Controls.Add(this.LX);
            this.groupBox1.Controls.Add(this.X);
            this.groupBox1.Location = new System.Drawing.Point(7, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(180, 181);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "position";
            // 
            // Z
            // 
            this.Z.Location = new System.Drawing.Point(25, 121);
            this.Z.Maximum = 65535;
            this.Z.Name = "Z";
            this.Z.Size = new System.Drawing.Size(104, 45);
            this.Z.TabIndex = 7;
            this.Z.Value = 32768;
            // 
            // Y
            // 
            this.Y.Location = new System.Drawing.Point(25, 70);
            this.Y.Maximum = 65535;
            this.Y.Name = "Y";
            this.Y.Size = new System.Drawing.Size(104, 45);
            this.Y.TabIndex = 6;
            this.Y.Value = 32768;
            // 
            // LZ
            // 
            this.LZ.AutoSize = true;
            this.LZ.Location = new System.Drawing.Point(6, 123);
            this.LZ.Name = "LZ";
            this.LZ.Size = new System.Drawing.Size(13, 13);
            this.LZ.TabIndex = 5;
            this.LZ.Text = "Z";
            this.LZ.Click += new System.EventHandler(this.LFX_Click);
            // 
            // LY
            // 
            this.LY.AutoSize = true;
            this.LY.Location = new System.Drawing.Point(6, 71);
            this.LY.Name = "LY";
            this.LY.Size = new System.Drawing.Size(13, 13);
            this.LY.TabIndex = 4;
            this.LY.Text = "Y";
            this.LY.Click += new System.EventHandler(this.LFX_Click);
            // 
            // LX
            // 
            this.LX.AutoSize = true;
            this.LX.Location = new System.Drawing.Point(6, 19);
            this.LX.Name = "LX";
            this.LX.Size = new System.Drawing.Size(13, 13);
            this.LX.TabIndex = 3;
            this.LX.Text = "X";
            this.LX.Click += new System.EventHandler(this.LFX_Click);
            // 
            // X
            // 
            this.X.Location = new System.Drawing.Point(25, 19);
            this.X.Maximum = 65535;
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(104, 45);
            this.X.TabIndex = 0;
            this.X.Value = 32768;
            // 
            // CRz
            // 
            this.CRz.Location = new System.Drawing.Point(135, 123);
            this.CRz.Name = "CRz";
            this.CRz.Size = new System.Drawing.Size(38, 20);
            this.CRz.TabIndex = 27;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.CRz);
            this.groupBox8.Controls.Add(this.CRy);
            this.groupBox8.Controls.Add(this.CRx);
            this.groupBox8.Controls.Add(this.LRz);
            this.groupBox8.Controls.Add(this.LRy);
            this.groupBox8.Controls.Add(this.LRx);
            this.groupBox8.Controls.Add(this.Rz);
            this.groupBox8.Controls.Add(this.Rx);
            this.groupBox8.Controls.Add(this.Ry);
            this.groupBox8.Location = new System.Drawing.Point(7, 194);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(180, 181);
            this.groupBox8.TabIndex = 7;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "rotation";
            // 
            // CRy
            // 
            this.CRy.Location = new System.Drawing.Point(135, 73);
            this.CRy.Name = "CRy";
            this.CRy.Size = new System.Drawing.Size(38, 20);
            this.CRy.TabIndex = 26;
            // 
            // CRx
            // 
            this.CRx.Location = new System.Drawing.Point(136, 19);
            this.CRx.Name = "CRx";
            this.CRx.Size = new System.Drawing.Size(38, 20);
            this.CRx.TabIndex = 25;
            // 
            // LRz
            // 
            this.LRz.AutoSize = true;
            this.LRz.Location = new System.Drawing.Point(6, 123);
            this.LRz.Name = "LRz";
            this.LRz.Size = new System.Drawing.Size(13, 13);
            this.LRz.TabIndex = 5;
            this.LRz.Text = "Z";
            this.LRz.Click += new System.EventHandler(this.LFX_Click);
            // 
            // LRy
            // 
            this.LRy.AutoSize = true;
            this.LRy.Location = new System.Drawing.Point(6, 71);
            this.LRy.Name = "LRy";
            this.LRy.Size = new System.Drawing.Size(13, 13);
            this.LRy.TabIndex = 4;
            this.LRy.Text = "Y";
            this.LRy.Click += new System.EventHandler(this.LFX_Click);
            // 
            // LRx
            // 
            this.LRx.AutoSize = true;
            this.LRx.Location = new System.Drawing.Point(6, 19);
            this.LRx.Name = "LRx";
            this.LRx.Size = new System.Drawing.Size(13, 13);
            this.LRx.TabIndex = 3;
            this.LRx.Text = "X";
            this.LRx.Click += new System.EventHandler(this.LFX_Click);
            // 
            // Rz
            // 
            this.Rz.Location = new System.Drawing.Point(25, 121);
            this.Rz.Maximum = 65535;
            this.Rz.Name = "Rz";
            this.Rz.Size = new System.Drawing.Size(104, 45);
            this.Rz.TabIndex = 24;
            this.Rz.Value = 32768;
            // 
            // Rx
            // 
            this.Rx.Location = new System.Drawing.Point(25, 19);
            this.Rx.Maximum = 65535;
            this.Rx.Name = "Rx";
            this.Rx.Size = new System.Drawing.Size(104, 45);
            this.Rx.TabIndex = 17;
            this.Rx.Value = 32768;
            // 
            // Ry
            // 
            this.Ry.Location = new System.Drawing.Point(25, 72);
            this.Ry.Maximum = 65535;
            this.Ry.Name = "Ry";
            this.Ry.Size = new System.Drawing.Size(104, 45);
            this.Ry.TabIndex = 21;
            this.Ry.Value = 32768;
            // 
            // VRz
            // 
            this.VRz.Location = new System.Drawing.Point(23, 125);
            this.VRz.Maximum = 65535;
            this.VRz.Name = "VRz";
            this.VRz.Size = new System.Drawing.Size(104, 45);
            this.VRz.TabIndex = 14;
            this.VRz.Value = 32768;
            // 
            // fieldList
            // 
            this.fieldList.FormattingEnabled = true;
            this.fieldList.Location = new System.Drawing.Point(4, 4);
            this.fieldList.Name = "fieldList";
            this.fieldList.Size = new System.Drawing.Size(262, 290);
            this.fieldList.TabIndex = 0;
            // 
            // connection_monitor
            // 
            this.connection_monitor.Controls.Add(this.fieldList);
            this.connection_monitor.Location = new System.Drawing.Point(4, 22);
            this.connection_monitor.Name = "connection_monitor";
            this.connection_monitor.Size = new System.Drawing.Size(758, 387);
            this.connection_monitor.TabIndex = 2;
            this.connection_monitor.Text = "connection monitor";
            this.connection_monitor.UseVisualStyleBackColor = true;
            // 
            // Button128
            // 
            this.Button128.AutoSize = true;
            this.Button128.Location = new System.Drawing.Point(511, 351);
            this.Button128.Name = "Button128";
            this.Button128.Size = new System.Drawing.Size(75, 17);
            this.Button128.TabIndex = 127;
            this.Button128.Text = "Button128";
            this.Button128.UseVisualStyleBackColor = true;
            this.Button128.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button127
            // 
            this.Button127.AutoSize = true;
            this.Button127.Location = new System.Drawing.Point(511, 328);
            this.Button127.Name = "Button127";
            this.Button127.Size = new System.Drawing.Size(75, 17);
            this.Button127.TabIndex = 126;
            this.Button127.Text = "Button127";
            this.Button127.UseVisualStyleBackColor = true;
            this.Button127.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button126
            // 
            this.Button126.AutoSize = true;
            this.Button126.Location = new System.Drawing.Point(511, 305);
            this.Button126.Name = "Button126";
            this.Button126.Size = new System.Drawing.Size(75, 17);
            this.Button126.TabIndex = 125;
            this.Button126.Text = "Button126";
            this.Button126.UseVisualStyleBackColor = true;
            this.Button126.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button125
            // 
            this.Button125.AutoSize = true;
            this.Button125.Location = new System.Drawing.Point(511, 282);
            this.Button125.Name = "Button125";
            this.Button125.Size = new System.Drawing.Size(75, 17);
            this.Button125.TabIndex = 124;
            this.Button125.Text = "Button125";
            this.Button125.UseVisualStyleBackColor = true;
            this.Button125.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button124
            // 
            this.Button124.AutoSize = true;
            this.Button124.Location = new System.Drawing.Point(511, 259);
            this.Button124.Name = "Button124";
            this.Button124.Size = new System.Drawing.Size(75, 17);
            this.Button124.TabIndex = 123;
            this.Button124.Text = "Button124";
            this.Button124.UseVisualStyleBackColor = true;
            this.Button124.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button123
            // 
            this.Button123.AutoSize = true;
            this.Button123.Location = new System.Drawing.Point(511, 236);
            this.Button123.Name = "Button123";
            this.Button123.Size = new System.Drawing.Size(75, 17);
            this.Button123.TabIndex = 122;
            this.Button123.Text = "Button123";
            this.Button123.UseVisualStyleBackColor = true;
            this.Button123.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button122
            // 
            this.Button122.AutoSize = true;
            this.Button122.Location = new System.Drawing.Point(511, 213);
            this.Button122.Name = "Button122";
            this.Button122.Size = new System.Drawing.Size(75, 17);
            this.Button122.TabIndex = 121;
            this.Button122.Text = "Button122";
            this.Button122.UseVisualStyleBackColor = true;
            this.Button122.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button121
            // 
            this.Button121.AutoSize = true;
            this.Button121.Location = new System.Drawing.Point(511, 190);
            this.Button121.Name = "Button121";
            this.Button121.Size = new System.Drawing.Size(75, 17);
            this.Button121.TabIndex = 120;
            this.Button121.Text = "Button121";
            this.Button121.UseVisualStyleBackColor = true;
            this.Button121.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button120
            // 
            this.Button120.AutoSize = true;
            this.Button120.Location = new System.Drawing.Point(510, 167);
            this.Button120.Name = "Button120";
            this.Button120.Size = new System.Drawing.Size(75, 17);
            this.Button120.TabIndex = 119;
            this.Button120.Text = "Button120";
            this.Button120.UseVisualStyleBackColor = true;
            this.Button120.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button119
            // 
            this.Button119.AutoSize = true;
            this.Button119.Location = new System.Drawing.Point(510, 144);
            this.Button119.Name = "Button119";
            this.Button119.Size = new System.Drawing.Size(75, 17);
            this.Button119.TabIndex = 118;
            this.Button119.Text = "Button119";
            this.Button119.UseVisualStyleBackColor = true;
            this.Button119.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button118
            // 
            this.Button118.AutoSize = true;
            this.Button118.Location = new System.Drawing.Point(510, 121);
            this.Button118.Name = "Button118";
            this.Button118.Size = new System.Drawing.Size(75, 17);
            this.Button118.TabIndex = 117;
            this.Button118.Text = "Button118";
            this.Button118.UseVisualStyleBackColor = true;
            this.Button118.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button117
            // 
            this.Button117.AutoSize = true;
            this.Button117.Location = new System.Drawing.Point(510, 98);
            this.Button117.Name = "Button117";
            this.Button117.Size = new System.Drawing.Size(75, 17);
            this.Button117.TabIndex = 116;
            this.Button117.Text = "Button117";
            this.Button117.UseVisualStyleBackColor = true;
            this.Button117.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button116
            // 
            this.Button116.AutoSize = true;
            this.Button116.Location = new System.Drawing.Point(511, 75);
            this.Button116.Name = "Button116";
            this.Button116.Size = new System.Drawing.Size(75, 17);
            this.Button116.TabIndex = 115;
            this.Button116.Text = "Button116";
            this.Button116.UseVisualStyleBackColor = true;
            this.Button116.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button115
            // 
            this.Button115.AutoSize = true;
            this.Button115.Location = new System.Drawing.Point(511, 52);
            this.Button115.Name = "Button115";
            this.Button115.Size = new System.Drawing.Size(75, 17);
            this.Button115.TabIndex = 114;
            this.Button115.Text = "Button115";
            this.Button115.UseVisualStyleBackColor = true;
            this.Button115.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button114
            // 
            this.Button114.AutoSize = true;
            this.Button114.Location = new System.Drawing.Point(511, 29);
            this.Button114.Name = "Button114";
            this.Button114.Size = new System.Drawing.Size(75, 17);
            this.Button114.TabIndex = 113;
            this.Button114.Text = "Button114";
            this.Button114.UseVisualStyleBackColor = true;
            this.Button114.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button113
            // 
            this.Button113.AutoSize = true;
            this.Button113.Location = new System.Drawing.Point(511, 6);
            this.Button113.Name = "Button113";
            this.Button113.Size = new System.Drawing.Size(75, 17);
            this.Button113.TabIndex = 112;
            this.Button113.Text = "Button113";
            this.Button113.UseVisualStyleBackColor = true;
            this.Button113.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button112
            // 
            this.Button112.AutoSize = true;
            this.Button112.Location = new System.Drawing.Point(436, 351);
            this.Button112.Name = "Button112";
            this.Button112.Size = new System.Drawing.Size(75, 17);
            this.Button112.TabIndex = 111;
            this.Button112.Text = "Button112";
            this.Button112.UseVisualStyleBackColor = true;
            this.Button112.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button111
            // 
            this.Button111.AutoSize = true;
            this.Button111.Location = new System.Drawing.Point(436, 328);
            this.Button111.Name = "Button111";
            this.Button111.Size = new System.Drawing.Size(75, 17);
            this.Button111.TabIndex = 110;
            this.Button111.Text = "Button111";
            this.Button111.UseVisualStyleBackColor = true;
            this.Button111.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button110
            // 
            this.Button110.AutoSize = true;
            this.Button110.Location = new System.Drawing.Point(436, 305);
            this.Button110.Name = "Button110";
            this.Button110.Size = new System.Drawing.Size(75, 17);
            this.Button110.TabIndex = 109;
            this.Button110.Text = "Button110";
            this.Button110.UseVisualStyleBackColor = true;
            this.Button110.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button109
            // 
            this.Button109.AutoSize = true;
            this.Button109.Location = new System.Drawing.Point(436, 282);
            this.Button109.Name = "Button109";
            this.Button109.Size = new System.Drawing.Size(75, 17);
            this.Button109.TabIndex = 108;
            this.Button109.Text = "Button109";
            this.Button109.UseVisualStyleBackColor = true;
            this.Button109.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Buttons
            // 
            this.Buttons.Controls.Add(this.Button128);
            this.Buttons.Controls.Add(this.Button127);
            this.Buttons.Controls.Add(this.Button126);
            this.Buttons.Controls.Add(this.Button125);
            this.Buttons.Controls.Add(this.Button124);
            this.Buttons.Controls.Add(this.Button123);
            this.Buttons.Controls.Add(this.Button122);
            this.Buttons.Controls.Add(this.Button121);
            this.Buttons.Controls.Add(this.Button120);
            this.Buttons.Controls.Add(this.Button119);
            this.Buttons.Controls.Add(this.Button118);
            this.Buttons.Controls.Add(this.Button117);
            this.Buttons.Controls.Add(this.Button116);
            this.Buttons.Controls.Add(this.Button115);
            this.Buttons.Controls.Add(this.Button114);
            this.Buttons.Controls.Add(this.Button113);
            this.Buttons.Controls.Add(this.Button112);
            this.Buttons.Controls.Add(this.Button111);
            this.Buttons.Controls.Add(this.Button110);
            this.Buttons.Controls.Add(this.Button109);
            this.Buttons.Controls.Add(this.Button108);
            this.Buttons.Controls.Add(this.Button107);
            this.Buttons.Controls.Add(this.Button106);
            this.Buttons.Controls.Add(this.Button105);
            this.Buttons.Controls.Add(this.Button104);
            this.Buttons.Controls.Add(this.Button103);
            this.Buttons.Controls.Add(this.Button102);
            this.Buttons.Controls.Add(this.Button101);
            this.Buttons.Controls.Add(this.Button100);
            this.Buttons.Controls.Add(this.Button99);
            this.Buttons.Controls.Add(this.Button98);
            this.Buttons.Controls.Add(this.Button97);
            this.Buttons.Controls.Add(this.Button96);
            this.Buttons.Controls.Add(this.Button95);
            this.Buttons.Controls.Add(this.Button94);
            this.Buttons.Controls.Add(this.Button93);
            this.Buttons.Controls.Add(this.Button92);
            this.Buttons.Controls.Add(this.Button91);
            this.Buttons.Controls.Add(this.Button90);
            this.Buttons.Controls.Add(this.Button89);
            this.Buttons.Controls.Add(this.Button88);
            this.Buttons.Controls.Add(this.Button87);
            this.Buttons.Controls.Add(this.Button86);
            this.Buttons.Controls.Add(this.Button85);
            this.Buttons.Controls.Add(this.Button84);
            this.Buttons.Controls.Add(this.Button83);
            this.Buttons.Controls.Add(this.Button82);
            this.Buttons.Controls.Add(this.Button81);
            this.Buttons.Controls.Add(this.Button48);
            this.Buttons.Controls.Add(this.Button47);
            this.Buttons.Controls.Add(this.Button46);
            this.Buttons.Controls.Add(this.Button45);
            this.Buttons.Controls.Add(this.Button44);
            this.Buttons.Controls.Add(this.Button43);
            this.Buttons.Controls.Add(this.Button42);
            this.Buttons.Controls.Add(this.Button41);
            this.Buttons.Controls.Add(this.Button40);
            this.Buttons.Controls.Add(this.Button39);
            this.Buttons.Controls.Add(this.Button38);
            this.Buttons.Controls.Add(this.Button37);
            this.Buttons.Controls.Add(this.Button36);
            this.Buttons.Controls.Add(this.Button80);
            this.Buttons.Controls.Add(this.Button79);
            this.Buttons.Controls.Add(this.Button78);
            this.Buttons.Controls.Add(this.Button77);
            this.Buttons.Controls.Add(this.Button76);
            this.Buttons.Controls.Add(this.Button75);
            this.Buttons.Controls.Add(this.Button74);
            this.Buttons.Controls.Add(this.Button73);
            this.Buttons.Controls.Add(this.Button72);
            this.Buttons.Controls.Add(this.Button71);
            this.Buttons.Controls.Add(this.Button70);
            this.Buttons.Controls.Add(this.Button69);
            this.Buttons.Controls.Add(this.Button68);
            this.Buttons.Controls.Add(this.Button67);
            this.Buttons.Controls.Add(this.Button66);
            this.Buttons.Controls.Add(this.Button65);
            this.Buttons.Controls.Add(this.Button64);
            this.Buttons.Controls.Add(this.Button63);
            this.Buttons.Controls.Add(this.Button62);
            this.Buttons.Controls.Add(this.Button61);
            this.Buttons.Controls.Add(this.Button60);
            this.Buttons.Controls.Add(this.Button59);
            this.Buttons.Controls.Add(this.Button58);
            this.Buttons.Controls.Add(this.Button57);
            this.Buttons.Controls.Add(this.Button56);
            this.Buttons.Controls.Add(this.Button55);
            this.Buttons.Controls.Add(this.Button54);
            this.Buttons.Controls.Add(this.Button53);
            this.Buttons.Controls.Add(this.Button52);
            this.Buttons.Controls.Add(this.Button51);
            this.Buttons.Controls.Add(this.Button50);
            this.Buttons.Controls.Add(this.Button49);
            this.Buttons.Controls.Add(this.Button35);
            this.Buttons.Controls.Add(this.Button34);
            this.Buttons.Controls.Add(this.Button33);
            this.Buttons.Controls.Add(this.Button32);
            this.Buttons.Controls.Add(this.Button31);
            this.Buttons.Controls.Add(this.Button30);
            this.Buttons.Controls.Add(this.Button29);
            this.Buttons.Controls.Add(this.Button28);
            this.Buttons.Controls.Add(this.Button27);
            this.Buttons.Controls.Add(this.Button26);
            this.Buttons.Controls.Add(this.Button25);
            this.Buttons.Controls.Add(this.Button24);
            this.Buttons.Controls.Add(this.Button23);
            this.Buttons.Controls.Add(this.Button22);
            this.Buttons.Controls.Add(this.Button21);
            this.Buttons.Controls.Add(this.Button20);
            this.Buttons.Controls.Add(this.Button19);
            this.Buttons.Controls.Add(this.Button18);
            this.Buttons.Controls.Add(this.Button17);
            this.Buttons.Controls.Add(this.Button16);
            this.Buttons.Controls.Add(this.Button15);
            this.Buttons.Controls.Add(this.Button14);
            this.Buttons.Controls.Add(this.Button13);
            this.Buttons.Controls.Add(this.Button12);
            this.Buttons.Controls.Add(this.Button11);
            this.Buttons.Controls.Add(this.Button10);
            this.Buttons.Controls.Add(this.Button9);
            this.Buttons.Controls.Add(this.Button8);
            this.Buttons.Controls.Add(this.Button7);
            this.Buttons.Controls.Add(this.Button6);
            this.Buttons.Controls.Add(this.Button5);
            this.Buttons.Controls.Add(this.Button4);
            this.Buttons.Controls.Add(this.Button3);
            this.Buttons.Controls.Add(this.Button2);
            this.Buttons.Controls.Add(this.Button1);
            this.Buttons.Location = new System.Drawing.Point(4, 22);
            this.Buttons.Name = "Buttons";
            this.Buttons.Padding = new System.Windows.Forms.Padding(3);
            this.Buttons.Size = new System.Drawing.Size(758, 387);
            this.Buttons.TabIndex = 1;
            this.Buttons.Text = "Buttons";
            this.Buttons.UseVisualStyleBackColor = true;
            // 
            // Button108
            // 
            this.Button108.AutoSize = true;
            this.Button108.Location = new System.Drawing.Point(436, 259);
            this.Button108.Name = "Button108";
            this.Button108.Size = new System.Drawing.Size(75, 17);
            this.Button108.TabIndex = 107;
            this.Button108.Text = "Button108";
            this.Button108.UseVisualStyleBackColor = true;
            this.Button108.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button107
            // 
            this.Button107.AutoSize = true;
            this.Button107.Location = new System.Drawing.Point(436, 236);
            this.Button107.Name = "Button107";
            this.Button107.Size = new System.Drawing.Size(75, 17);
            this.Button107.TabIndex = 106;
            this.Button107.Text = "Button107";
            this.Button107.UseVisualStyleBackColor = true;
            this.Button107.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button106
            // 
            this.Button106.AutoSize = true;
            this.Button106.Location = new System.Drawing.Point(436, 213);
            this.Button106.Name = "Button106";
            this.Button106.Size = new System.Drawing.Size(75, 17);
            this.Button106.TabIndex = 105;
            this.Button106.Text = "Button106";
            this.Button106.UseVisualStyleBackColor = true;
            this.Button106.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button105
            // 
            this.Button105.AutoSize = true;
            this.Button105.Location = new System.Drawing.Point(436, 190);
            this.Button105.Name = "Button105";
            this.Button105.Size = new System.Drawing.Size(75, 17);
            this.Button105.TabIndex = 104;
            this.Button105.Text = "Button105";
            this.Button105.UseVisualStyleBackColor = true;
            this.Button105.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button104
            // 
            this.Button104.AutoSize = true;
            this.Button104.Location = new System.Drawing.Point(435, 167);
            this.Button104.Name = "Button104";
            this.Button104.Size = new System.Drawing.Size(75, 17);
            this.Button104.TabIndex = 103;
            this.Button104.Text = "Button104";
            this.Button104.UseVisualStyleBackColor = true;
            this.Button104.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button103
            // 
            this.Button103.AutoSize = true;
            this.Button103.Location = new System.Drawing.Point(435, 144);
            this.Button103.Name = "Button103";
            this.Button103.Size = new System.Drawing.Size(75, 17);
            this.Button103.TabIndex = 102;
            this.Button103.Text = "Button103";
            this.Button103.UseVisualStyleBackColor = true;
            this.Button103.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button102
            // 
            this.Button102.AutoSize = true;
            this.Button102.Location = new System.Drawing.Point(435, 121);
            this.Button102.Name = "Button102";
            this.Button102.Size = new System.Drawing.Size(75, 17);
            this.Button102.TabIndex = 101;
            this.Button102.Text = "Button102";
            this.Button102.UseVisualStyleBackColor = true;
            this.Button102.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button101
            // 
            this.Button101.AutoSize = true;
            this.Button101.Location = new System.Drawing.Point(435, 98);
            this.Button101.Name = "Button101";
            this.Button101.Size = new System.Drawing.Size(75, 17);
            this.Button101.TabIndex = 100;
            this.Button101.Text = "Button101";
            this.Button101.UseVisualStyleBackColor = true;
            this.Button101.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button100
            // 
            this.Button100.AutoSize = true;
            this.Button100.Location = new System.Drawing.Point(436, 75);
            this.Button100.Name = "Button100";
            this.Button100.Size = new System.Drawing.Size(75, 17);
            this.Button100.TabIndex = 99;
            this.Button100.Text = "Button100";
            this.Button100.UseVisualStyleBackColor = true;
            this.Button100.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button99
            // 
            this.Button99.AutoSize = true;
            this.Button99.Location = new System.Drawing.Point(436, 52);
            this.Button99.Name = "Button99";
            this.Button99.Size = new System.Drawing.Size(69, 17);
            this.Button99.TabIndex = 98;
            this.Button99.Text = "Button99";
            this.Button99.UseVisualStyleBackColor = true;
            this.Button99.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button98
            // 
            this.Button98.AutoSize = true;
            this.Button98.Location = new System.Drawing.Point(436, 29);
            this.Button98.Name = "Button98";
            this.Button98.Size = new System.Drawing.Size(69, 17);
            this.Button98.TabIndex = 97;
            this.Button98.Text = "Button98";
            this.Button98.UseVisualStyleBackColor = true;
            this.Button98.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button97
            // 
            this.Button97.AutoSize = true;
            this.Button97.Location = new System.Drawing.Point(436, 6);
            this.Button97.Name = "Button97";
            this.Button97.Size = new System.Drawing.Size(69, 17);
            this.Button97.TabIndex = 96;
            this.Button97.Text = "Button97";
            this.Button97.UseVisualStyleBackColor = true;
            this.Button97.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button96
            // 
            this.Button96.AutoSize = true;
            this.Button96.Location = new System.Drawing.Point(367, 349);
            this.Button96.Name = "Button96";
            this.Button96.Size = new System.Drawing.Size(69, 17);
            this.Button96.TabIndex = 95;
            this.Button96.Text = "Button96";
            this.Button96.UseVisualStyleBackColor = true;
            this.Button96.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button95
            // 
            this.Button95.AutoSize = true;
            this.Button95.Location = new System.Drawing.Point(367, 326);
            this.Button95.Name = "Button95";
            this.Button95.Size = new System.Drawing.Size(69, 17);
            this.Button95.TabIndex = 94;
            this.Button95.Text = "Button95";
            this.Button95.UseVisualStyleBackColor = true;
            this.Button95.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button94
            // 
            this.Button94.AutoSize = true;
            this.Button94.Location = new System.Drawing.Point(367, 303);
            this.Button94.Name = "Button94";
            this.Button94.Size = new System.Drawing.Size(69, 17);
            this.Button94.TabIndex = 93;
            this.Button94.Text = "Button94";
            this.Button94.UseVisualStyleBackColor = true;
            this.Button94.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button93
            // 
            this.Button93.AutoSize = true;
            this.Button93.Location = new System.Drawing.Point(367, 280);
            this.Button93.Name = "Button93";
            this.Button93.Size = new System.Drawing.Size(69, 17);
            this.Button93.TabIndex = 92;
            this.Button93.Text = "Button93";
            this.Button93.UseVisualStyleBackColor = true;
            this.Button93.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button92
            // 
            this.Button92.AutoSize = true;
            this.Button92.Location = new System.Drawing.Point(367, 257);
            this.Button92.Name = "Button92";
            this.Button92.Size = new System.Drawing.Size(69, 17);
            this.Button92.TabIndex = 91;
            this.Button92.Text = "Button92";
            this.Button92.UseVisualStyleBackColor = true;
            this.Button92.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button91
            // 
            this.Button91.AutoSize = true;
            this.Button91.Location = new System.Drawing.Point(367, 234);
            this.Button91.Name = "Button91";
            this.Button91.Size = new System.Drawing.Size(69, 17);
            this.Button91.TabIndex = 90;
            this.Button91.Text = "Button91";
            this.Button91.UseVisualStyleBackColor = true;
            this.Button91.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button90
            // 
            this.Button90.AutoSize = true;
            this.Button90.Location = new System.Drawing.Point(367, 211);
            this.Button90.Name = "Button90";
            this.Button90.Size = new System.Drawing.Size(69, 17);
            this.Button90.TabIndex = 89;
            this.Button90.Text = "Button90";
            this.Button90.UseVisualStyleBackColor = true;
            this.Button90.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button89
            // 
            this.Button89.AutoSize = true;
            this.Button89.Location = new System.Drawing.Point(367, 188);
            this.Button89.Name = "Button89";
            this.Button89.Size = new System.Drawing.Size(69, 17);
            this.Button89.TabIndex = 88;
            this.Button89.Text = "Button89";
            this.Button89.UseVisualStyleBackColor = true;
            this.Button89.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button88
            // 
            this.Button88.AutoSize = true;
            this.Button88.Location = new System.Drawing.Point(366, 165);
            this.Button88.Name = "Button88";
            this.Button88.Size = new System.Drawing.Size(69, 17);
            this.Button88.TabIndex = 87;
            this.Button88.Text = "Button88";
            this.Button88.UseVisualStyleBackColor = true;
            this.Button88.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button87
            // 
            this.Button87.AutoSize = true;
            this.Button87.Location = new System.Drawing.Point(366, 142);
            this.Button87.Name = "Button87";
            this.Button87.Size = new System.Drawing.Size(69, 17);
            this.Button87.TabIndex = 86;
            this.Button87.Text = "Button87";
            this.Button87.UseVisualStyleBackColor = true;
            this.Button87.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button86
            // 
            this.Button86.AutoSize = true;
            this.Button86.Location = new System.Drawing.Point(366, 119);
            this.Button86.Name = "Button86";
            this.Button86.Size = new System.Drawing.Size(69, 17);
            this.Button86.TabIndex = 85;
            this.Button86.Text = "Button86";
            this.Button86.UseVisualStyleBackColor = true;
            this.Button86.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button85
            // 
            this.Button85.AutoSize = true;
            this.Button85.Location = new System.Drawing.Point(366, 96);
            this.Button85.Name = "Button85";
            this.Button85.Size = new System.Drawing.Size(69, 17);
            this.Button85.TabIndex = 84;
            this.Button85.Text = "Button85";
            this.Button85.UseVisualStyleBackColor = true;
            this.Button85.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button84
            // 
            this.Button84.AutoSize = true;
            this.Button84.Location = new System.Drawing.Point(367, 73);
            this.Button84.Name = "Button84";
            this.Button84.Size = new System.Drawing.Size(69, 17);
            this.Button84.TabIndex = 83;
            this.Button84.Text = "Button84";
            this.Button84.UseVisualStyleBackColor = true;
            this.Button84.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button83
            // 
            this.Button83.AutoSize = true;
            this.Button83.Location = new System.Drawing.Point(367, 50);
            this.Button83.Name = "Button83";
            this.Button83.Size = new System.Drawing.Size(69, 17);
            this.Button83.TabIndex = 82;
            this.Button83.Text = "Button83";
            this.Button83.UseVisualStyleBackColor = true;
            this.Button83.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button82
            // 
            this.Button82.AutoSize = true;
            this.Button82.Location = new System.Drawing.Point(367, 27);
            this.Button82.Name = "Button82";
            this.Button82.Size = new System.Drawing.Size(69, 17);
            this.Button82.TabIndex = 81;
            this.Button82.Text = "Button82";
            this.Button82.UseVisualStyleBackColor = true;
            this.Button82.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button81
            // 
            this.Button81.AutoSize = true;
            this.Button81.Location = new System.Drawing.Point(367, 6);
            this.Button81.Name = "Button81";
            this.Button81.Size = new System.Drawing.Size(69, 17);
            this.Button81.TabIndex = 80;
            this.Button81.Text = "Button81";
            this.Button81.UseVisualStyleBackColor = true;
            this.Button81.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button48
            // 
            this.Button48.AutoSize = true;
            this.Button48.Location = new System.Drawing.Point(151, 351);
            this.Button48.Name = "Button48";
            this.Button48.Size = new System.Drawing.Size(69, 17);
            this.Button48.TabIndex = 79;
            this.Button48.Text = "Button48";
            this.Button48.UseVisualStyleBackColor = true;
            this.Button48.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button47
            // 
            this.Button47.AutoSize = true;
            this.Button47.Location = new System.Drawing.Point(151, 328);
            this.Button47.Name = "Button47";
            this.Button47.Size = new System.Drawing.Size(69, 17);
            this.Button47.TabIndex = 78;
            this.Button47.Text = "Button47";
            this.Button47.UseVisualStyleBackColor = true;
            this.Button47.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button46
            // 
            this.Button46.AutoSize = true;
            this.Button46.Location = new System.Drawing.Point(151, 305);
            this.Button46.Name = "Button46";
            this.Button46.Size = new System.Drawing.Size(69, 17);
            this.Button46.TabIndex = 77;
            this.Button46.Text = "Button46";
            this.Button46.UseVisualStyleBackColor = true;
            this.Button46.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button45
            // 
            this.Button45.AutoSize = true;
            this.Button45.Location = new System.Drawing.Point(151, 282);
            this.Button45.Name = "Button45";
            this.Button45.Size = new System.Drawing.Size(69, 17);
            this.Button45.TabIndex = 76;
            this.Button45.Text = "Button45";
            this.Button45.UseVisualStyleBackColor = true;
            this.Button45.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button44
            // 
            this.Button44.AutoSize = true;
            this.Button44.Location = new System.Drawing.Point(151, 259);
            this.Button44.Name = "Button44";
            this.Button44.Size = new System.Drawing.Size(69, 17);
            this.Button44.TabIndex = 75;
            this.Button44.Text = "Button44";
            this.Button44.UseVisualStyleBackColor = true;
            this.Button44.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button43
            // 
            this.Button43.AutoSize = true;
            this.Button43.Location = new System.Drawing.Point(151, 236);
            this.Button43.Name = "Button43";
            this.Button43.Size = new System.Drawing.Size(69, 17);
            this.Button43.TabIndex = 74;
            this.Button43.Text = "Button43";
            this.Button43.UseVisualStyleBackColor = true;
            this.Button43.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button42
            // 
            this.Button42.AutoSize = true;
            this.Button42.Location = new System.Drawing.Point(151, 213);
            this.Button42.Name = "Button42";
            this.Button42.Size = new System.Drawing.Size(69, 17);
            this.Button42.TabIndex = 73;
            this.Button42.Text = "Button42";
            this.Button42.UseVisualStyleBackColor = true;
            this.Button42.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button41
            // 
            this.Button41.AutoSize = true;
            this.Button41.Location = new System.Drawing.Point(151, 190);
            this.Button41.Name = "Button41";
            this.Button41.Size = new System.Drawing.Size(69, 17);
            this.Button41.TabIndex = 72;
            this.Button41.Text = "Button41";
            this.Button41.UseVisualStyleBackColor = true;
            this.Button41.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button40
            // 
            this.Button40.AutoSize = true;
            this.Button40.Location = new System.Drawing.Point(150, 167);
            this.Button40.Name = "Button40";
            this.Button40.Size = new System.Drawing.Size(69, 17);
            this.Button40.TabIndex = 71;
            this.Button40.Text = "Button40";
            this.Button40.UseVisualStyleBackColor = true;
            this.Button40.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button39
            // 
            this.Button39.AutoSize = true;
            this.Button39.Location = new System.Drawing.Point(150, 144);
            this.Button39.Name = "Button39";
            this.Button39.Size = new System.Drawing.Size(69, 17);
            this.Button39.TabIndex = 70;
            this.Button39.Text = "Button39";
            this.Button39.UseVisualStyleBackColor = true;
            this.Button39.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button38
            // 
            this.Button38.AutoSize = true;
            this.Button38.Location = new System.Drawing.Point(150, 121);
            this.Button38.Name = "Button38";
            this.Button38.Size = new System.Drawing.Size(69, 17);
            this.Button38.TabIndex = 69;
            this.Button38.Text = "Button38";
            this.Button38.UseVisualStyleBackColor = true;
            this.Button38.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button37
            // 
            this.Button37.AutoSize = true;
            this.Button37.Location = new System.Drawing.Point(150, 98);
            this.Button37.Name = "Button37";
            this.Button37.Size = new System.Drawing.Size(69, 17);
            this.Button37.TabIndex = 68;
            this.Button37.Text = "Button37";
            this.Button37.UseVisualStyleBackColor = true;
            this.Button37.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button36
            // 
            this.Button36.AutoSize = true;
            this.Button36.Location = new System.Drawing.Point(151, 75);
            this.Button36.Name = "Button36";
            this.Button36.Size = new System.Drawing.Size(69, 17);
            this.Button36.TabIndex = 67;
            this.Button36.Text = "Button36";
            this.Button36.UseVisualStyleBackColor = true;
            this.Button36.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button80
            // 
            this.Button80.AutoSize = true;
            this.Button80.Location = new System.Drawing.Point(295, 351);
            this.Button80.Name = "Button80";
            this.Button80.Size = new System.Drawing.Size(69, 17);
            this.Button80.TabIndex = 66;
            this.Button80.Text = "Button80";
            this.Button80.UseVisualStyleBackColor = true;
            this.Button80.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button79
            // 
            this.Button79.AutoSize = true;
            this.Button79.Location = new System.Drawing.Point(295, 328);
            this.Button79.Name = "Button79";
            this.Button79.Size = new System.Drawing.Size(69, 17);
            this.Button79.TabIndex = 65;
            this.Button79.Text = "Button79";
            this.Button79.UseVisualStyleBackColor = true;
            this.Button79.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button78
            // 
            this.Button78.AutoSize = true;
            this.Button78.Location = new System.Drawing.Point(295, 305);
            this.Button78.Name = "Button78";
            this.Button78.Size = new System.Drawing.Size(69, 17);
            this.Button78.TabIndex = 64;
            this.Button78.Text = "Button78";
            this.Button78.UseVisualStyleBackColor = true;
            this.Button78.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button77
            // 
            this.Button77.AutoSize = true;
            this.Button77.Location = new System.Drawing.Point(295, 282);
            this.Button77.Name = "Button77";
            this.Button77.Size = new System.Drawing.Size(69, 17);
            this.Button77.TabIndex = 63;
            this.Button77.Text = "Button77";
            this.Button77.UseVisualStyleBackColor = true;
            this.Button77.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button76
            // 
            this.Button76.AutoSize = true;
            this.Button76.Location = new System.Drawing.Point(295, 259);
            this.Button76.Name = "Button76";
            this.Button76.Size = new System.Drawing.Size(69, 17);
            this.Button76.TabIndex = 62;
            this.Button76.Text = "Button76";
            this.Button76.UseVisualStyleBackColor = true;
            this.Button76.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button75
            // 
            this.Button75.AutoSize = true;
            this.Button75.Location = new System.Drawing.Point(295, 236);
            this.Button75.Name = "Button75";
            this.Button75.Size = new System.Drawing.Size(69, 17);
            this.Button75.TabIndex = 61;
            this.Button75.Text = "Button75";
            this.Button75.UseVisualStyleBackColor = true;
            this.Button75.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button74
            // 
            this.Button74.AutoSize = true;
            this.Button74.Location = new System.Drawing.Point(295, 213);
            this.Button74.Name = "Button74";
            this.Button74.Size = new System.Drawing.Size(69, 17);
            this.Button74.TabIndex = 60;
            this.Button74.Text = "Button74";
            this.Button74.UseVisualStyleBackColor = true;
            this.Button74.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button73
            // 
            this.Button73.AutoSize = true;
            this.Button73.Location = new System.Drawing.Point(295, 190);
            this.Button73.Name = "Button73";
            this.Button73.Size = new System.Drawing.Size(69, 17);
            this.Button73.TabIndex = 59;
            this.Button73.Text = "Button73";
            this.Button73.UseVisualStyleBackColor = true;
            this.Button73.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button72
            // 
            this.Button72.AutoSize = true;
            this.Button72.Location = new System.Drawing.Point(294, 167);
            this.Button72.Name = "Button72";
            this.Button72.Size = new System.Drawing.Size(69, 17);
            this.Button72.TabIndex = 58;
            this.Button72.Text = "Button72";
            this.Button72.UseVisualStyleBackColor = true;
            this.Button72.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button71
            // 
            this.Button71.AutoSize = true;
            this.Button71.Location = new System.Drawing.Point(294, 144);
            this.Button71.Name = "Button71";
            this.Button71.Size = new System.Drawing.Size(69, 17);
            this.Button71.TabIndex = 57;
            this.Button71.Text = "Button71";
            this.Button71.UseVisualStyleBackColor = true;
            this.Button71.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button70
            // 
            this.Button70.AutoSize = true;
            this.Button70.Location = new System.Drawing.Point(294, 121);
            this.Button70.Name = "Button70";
            this.Button70.Size = new System.Drawing.Size(69, 17);
            this.Button70.TabIndex = 56;
            this.Button70.Text = "Button70";
            this.Button70.UseVisualStyleBackColor = true;
            this.Button70.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button69
            // 
            this.Button69.AutoSize = true;
            this.Button69.Location = new System.Drawing.Point(294, 98);
            this.Button69.Name = "Button69";
            this.Button69.Size = new System.Drawing.Size(69, 17);
            this.Button69.TabIndex = 55;
            this.Button69.Text = "Button69";
            this.Button69.UseVisualStyleBackColor = true;
            this.Button69.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button68
            // 
            this.Button68.AutoSize = true;
            this.Button68.Location = new System.Drawing.Point(295, 75);
            this.Button68.Name = "Button68";
            this.Button68.Size = new System.Drawing.Size(69, 17);
            this.Button68.TabIndex = 54;
            this.Button68.Text = "Button68";
            this.Button68.UseVisualStyleBackColor = true;
            this.Button68.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button67
            // 
            this.Button67.AutoSize = true;
            this.Button67.Location = new System.Drawing.Point(295, 52);
            this.Button67.Name = "Button67";
            this.Button67.Size = new System.Drawing.Size(69, 17);
            this.Button67.TabIndex = 53;
            this.Button67.Text = "Button67";
            this.Button67.UseVisualStyleBackColor = true;
            this.Button67.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button66
            // 
            this.Button66.AutoSize = true;
            this.Button66.Location = new System.Drawing.Point(295, 29);
            this.Button66.Name = "Button66";
            this.Button66.Size = new System.Drawing.Size(69, 17);
            this.Button66.TabIndex = 52;
            this.Button66.Text = "Button66";
            this.Button66.UseVisualStyleBackColor = true;
            this.Button66.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button65
            // 
            this.Button65.AutoSize = true;
            this.Button65.Location = new System.Drawing.Point(295, 6);
            this.Button65.Name = "Button65";
            this.Button65.Size = new System.Drawing.Size(69, 17);
            this.Button65.TabIndex = 51;
            this.Button65.Text = "Button65";
            this.Button65.UseVisualStyleBackColor = true;
            this.Button65.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button64
            // 
            this.Button64.AutoSize = true;
            this.Button64.Location = new System.Drawing.Point(226, 349);
            this.Button64.Name = "Button64";
            this.Button64.Size = new System.Drawing.Size(69, 17);
            this.Button64.TabIndex = 50;
            this.Button64.Text = "Button64";
            this.Button64.UseVisualStyleBackColor = true;
            this.Button64.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button63
            // 
            this.Button63.AutoSize = true;
            this.Button63.Location = new System.Drawing.Point(226, 326);
            this.Button63.Name = "Button63";
            this.Button63.Size = new System.Drawing.Size(69, 17);
            this.Button63.TabIndex = 49;
            this.Button63.Text = "Button63";
            this.Button63.UseVisualStyleBackColor = true;
            this.Button63.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button62
            // 
            this.Button62.AutoSize = true;
            this.Button62.Location = new System.Drawing.Point(226, 303);
            this.Button62.Name = "Button62";
            this.Button62.Size = new System.Drawing.Size(69, 17);
            this.Button62.TabIndex = 48;
            this.Button62.Text = "Button62";
            this.Button62.UseVisualStyleBackColor = true;
            this.Button62.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button61
            // 
            this.Button61.AutoSize = true;
            this.Button61.Location = new System.Drawing.Point(226, 280);
            this.Button61.Name = "Button61";
            this.Button61.Size = new System.Drawing.Size(69, 17);
            this.Button61.TabIndex = 47;
            this.Button61.Text = "Button61";
            this.Button61.UseVisualStyleBackColor = true;
            this.Button61.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button60
            // 
            this.Button60.AutoSize = true;
            this.Button60.Location = new System.Drawing.Point(226, 257);
            this.Button60.Name = "Button60";
            this.Button60.Size = new System.Drawing.Size(69, 17);
            this.Button60.TabIndex = 46;
            this.Button60.Text = "Button60";
            this.Button60.UseVisualStyleBackColor = true;
            this.Button60.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button59
            // 
            this.Button59.AutoSize = true;
            this.Button59.Location = new System.Drawing.Point(226, 234);
            this.Button59.Name = "Button59";
            this.Button59.Size = new System.Drawing.Size(69, 17);
            this.Button59.TabIndex = 45;
            this.Button59.Text = "Button59";
            this.Button59.UseVisualStyleBackColor = true;
            this.Button59.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button58
            // 
            this.Button58.AutoSize = true;
            this.Button58.Location = new System.Drawing.Point(226, 211);
            this.Button58.Name = "Button58";
            this.Button58.Size = new System.Drawing.Size(69, 17);
            this.Button58.TabIndex = 44;
            this.Button58.Text = "Button58";
            this.Button58.UseVisualStyleBackColor = true;
            this.Button58.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button57
            // 
            this.Button57.AutoSize = true;
            this.Button57.Location = new System.Drawing.Point(226, 188);
            this.Button57.Name = "Button57";
            this.Button57.Size = new System.Drawing.Size(69, 17);
            this.Button57.TabIndex = 43;
            this.Button57.Text = "Button57";
            this.Button57.UseVisualStyleBackColor = true;
            this.Button57.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button56
            // 
            this.Button56.AutoSize = true;
            this.Button56.Location = new System.Drawing.Point(225, 165);
            this.Button56.Name = "Button56";
            this.Button56.Size = new System.Drawing.Size(69, 17);
            this.Button56.TabIndex = 42;
            this.Button56.Text = "Button56";
            this.Button56.UseVisualStyleBackColor = true;
            this.Button56.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button55
            // 
            this.Button55.AutoSize = true;
            this.Button55.Location = new System.Drawing.Point(225, 142);
            this.Button55.Name = "Button55";
            this.Button55.Size = new System.Drawing.Size(69, 17);
            this.Button55.TabIndex = 41;
            this.Button55.Text = "Button55";
            this.Button55.UseVisualStyleBackColor = true;
            this.Button55.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button54
            // 
            this.Button54.AutoSize = true;
            this.Button54.Location = new System.Drawing.Point(225, 119);
            this.Button54.Name = "Button54";
            this.Button54.Size = new System.Drawing.Size(69, 17);
            this.Button54.TabIndex = 40;
            this.Button54.Text = "Button54";
            this.Button54.UseVisualStyleBackColor = true;
            this.Button54.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button53
            // 
            this.Button53.AutoSize = true;
            this.Button53.Location = new System.Drawing.Point(225, 96);
            this.Button53.Name = "Button53";
            this.Button53.Size = new System.Drawing.Size(69, 17);
            this.Button53.TabIndex = 39;
            this.Button53.Text = "Button53";
            this.Button53.UseVisualStyleBackColor = true;
            this.Button53.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button52
            // 
            this.Button52.AutoSize = true;
            this.Button52.Location = new System.Drawing.Point(226, 73);
            this.Button52.Name = "Button52";
            this.Button52.Size = new System.Drawing.Size(69, 17);
            this.Button52.TabIndex = 38;
            this.Button52.Text = "Button52";
            this.Button52.UseVisualStyleBackColor = true;
            this.Button52.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button51
            // 
            this.Button51.AutoSize = true;
            this.Button51.Location = new System.Drawing.Point(226, 50);
            this.Button51.Name = "Button51";
            this.Button51.Size = new System.Drawing.Size(69, 17);
            this.Button51.TabIndex = 37;
            this.Button51.Text = "Button51";
            this.Button51.UseVisualStyleBackColor = true;
            this.Button51.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button50
            // 
            this.Button50.AutoSize = true;
            this.Button50.Location = new System.Drawing.Point(226, 27);
            this.Button50.Name = "Button50";
            this.Button50.Size = new System.Drawing.Size(69, 17);
            this.Button50.TabIndex = 36;
            this.Button50.Text = "Button50";
            this.Button50.UseVisualStyleBackColor = true;
            this.Button50.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button49
            // 
            this.Button49.AutoSize = true;
            this.Button49.Location = new System.Drawing.Point(226, 6);
            this.Button49.Name = "Button49";
            this.Button49.Size = new System.Drawing.Size(69, 17);
            this.Button49.TabIndex = 35;
            this.Button49.Text = "Button49";
            this.Button49.UseVisualStyleBackColor = true;
            this.Button49.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button35
            // 
            this.Button35.AutoSize = true;
            this.Button35.Location = new System.Drawing.Point(151, 50);
            this.Button35.Name = "Button35";
            this.Button35.Size = new System.Drawing.Size(69, 17);
            this.Button35.TabIndex = 34;
            this.Button35.Text = "Button35";
            this.Button35.UseVisualStyleBackColor = true;
            this.Button35.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button34
            // 
            this.Button34.AutoSize = true;
            this.Button34.Location = new System.Drawing.Point(151, 29);
            this.Button34.Name = "Button34";
            this.Button34.Size = new System.Drawing.Size(69, 17);
            this.Button34.TabIndex = 33;
            this.Button34.Text = "Button34";
            this.Button34.UseVisualStyleBackColor = true;
            this.Button34.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button33
            // 
            this.Button33.AutoSize = true;
            this.Button33.Location = new System.Drawing.Point(151, 6);
            this.Button33.Name = "Button33";
            this.Button33.Size = new System.Drawing.Size(69, 17);
            this.Button33.TabIndex = 32;
            this.Button33.Text = "Button33";
            this.Button33.UseVisualStyleBackColor = true;
            this.Button33.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button32
            // 
            this.Button32.AutoSize = true;
            this.Button32.Location = new System.Drawing.Point(76, 351);
            this.Button32.Name = "Button32";
            this.Button32.Size = new System.Drawing.Size(69, 17);
            this.Button32.TabIndex = 31;
            this.Button32.Text = "Button32";
            this.Button32.UseVisualStyleBackColor = true;
            this.Button32.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button31
            // 
            this.Button31.AutoSize = true;
            this.Button31.Location = new System.Drawing.Point(76, 328);
            this.Button31.Name = "Button31";
            this.Button31.Size = new System.Drawing.Size(69, 17);
            this.Button31.TabIndex = 30;
            this.Button31.Text = "Button31";
            this.Button31.UseVisualStyleBackColor = true;
            this.Button31.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button30
            // 
            this.Button30.AutoSize = true;
            this.Button30.Location = new System.Drawing.Point(76, 305);
            this.Button30.Name = "Button30";
            this.Button30.Size = new System.Drawing.Size(69, 17);
            this.Button30.TabIndex = 29;
            this.Button30.Text = "Button30";
            this.Button30.UseVisualStyleBackColor = true;
            this.Button30.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button29
            // 
            this.Button29.AutoSize = true;
            this.Button29.Location = new System.Drawing.Point(76, 282);
            this.Button29.Name = "Button29";
            this.Button29.Size = new System.Drawing.Size(69, 17);
            this.Button29.TabIndex = 28;
            this.Button29.Text = "Button29";
            this.Button29.UseVisualStyleBackColor = true;
            this.Button29.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button28
            // 
            this.Button28.AutoSize = true;
            this.Button28.Location = new System.Drawing.Point(76, 259);
            this.Button28.Name = "Button28";
            this.Button28.Size = new System.Drawing.Size(69, 17);
            this.Button28.TabIndex = 27;
            this.Button28.Text = "Button28";
            this.Button28.UseVisualStyleBackColor = true;
            this.Button28.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button27
            // 
            this.Button27.AutoSize = true;
            this.Button27.Location = new System.Drawing.Point(76, 236);
            this.Button27.Name = "Button27";
            this.Button27.Size = new System.Drawing.Size(69, 17);
            this.Button27.TabIndex = 26;
            this.Button27.Text = "Button27";
            this.Button27.UseVisualStyleBackColor = true;
            this.Button27.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button26
            // 
            this.Button26.AutoSize = true;
            this.Button26.Location = new System.Drawing.Point(76, 213);
            this.Button26.Name = "Button26";
            this.Button26.Size = new System.Drawing.Size(69, 17);
            this.Button26.TabIndex = 25;
            this.Button26.Text = "Button26";
            this.Button26.UseVisualStyleBackColor = true;
            this.Button26.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button25
            // 
            this.Button25.AutoSize = true;
            this.Button25.Location = new System.Drawing.Point(76, 190);
            this.Button25.Name = "Button25";
            this.Button25.Size = new System.Drawing.Size(69, 17);
            this.Button25.TabIndex = 24;
            this.Button25.Text = "Button25";
            this.Button25.UseVisualStyleBackColor = true;
            this.Button25.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button24
            // 
            this.Button24.AutoSize = true;
            this.Button24.Location = new System.Drawing.Point(75, 167);
            this.Button24.Name = "Button24";
            this.Button24.Size = new System.Drawing.Size(69, 17);
            this.Button24.TabIndex = 23;
            this.Button24.Text = "Button24";
            this.Button24.UseVisualStyleBackColor = true;
            this.Button24.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button23
            // 
            this.Button23.AutoSize = true;
            this.Button23.Location = new System.Drawing.Point(75, 144);
            this.Button23.Name = "Button23";
            this.Button23.Size = new System.Drawing.Size(69, 17);
            this.Button23.TabIndex = 22;
            this.Button23.Text = "Button23";
            this.Button23.UseVisualStyleBackColor = true;
            this.Button23.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button22
            // 
            this.Button22.AutoSize = true;
            this.Button22.Location = new System.Drawing.Point(75, 121);
            this.Button22.Name = "Button22";
            this.Button22.Size = new System.Drawing.Size(69, 17);
            this.Button22.TabIndex = 21;
            this.Button22.Text = "Button22";
            this.Button22.UseVisualStyleBackColor = true;
            this.Button22.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button21
            // 
            this.Button21.AutoSize = true;
            this.Button21.Location = new System.Drawing.Point(75, 98);
            this.Button21.Name = "Button21";
            this.Button21.Size = new System.Drawing.Size(69, 17);
            this.Button21.TabIndex = 20;
            this.Button21.Text = "Button21";
            this.Button21.UseVisualStyleBackColor = true;
            this.Button21.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button20
            // 
            this.Button20.AutoSize = true;
            this.Button20.Location = new System.Drawing.Point(76, 75);
            this.Button20.Name = "Button20";
            this.Button20.Size = new System.Drawing.Size(69, 17);
            this.Button20.TabIndex = 19;
            this.Button20.Text = "Button20";
            this.Button20.UseVisualStyleBackColor = true;
            this.Button20.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button19
            // 
            this.Button19.AutoSize = true;
            this.Button19.Location = new System.Drawing.Point(76, 52);
            this.Button19.Name = "Button19";
            this.Button19.Size = new System.Drawing.Size(69, 17);
            this.Button19.TabIndex = 18;
            this.Button19.Text = "Button19";
            this.Button19.UseVisualStyleBackColor = true;
            this.Button19.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button18
            // 
            this.Button18.AutoSize = true;
            this.Button18.Location = new System.Drawing.Point(76, 29);
            this.Button18.Name = "Button18";
            this.Button18.Size = new System.Drawing.Size(69, 17);
            this.Button18.TabIndex = 17;
            this.Button18.Text = "Button18";
            this.Button18.UseVisualStyleBackColor = true;
            this.Button18.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button17
            // 
            this.Button17.AutoSize = true;
            this.Button17.Location = new System.Drawing.Point(76, 6);
            this.Button17.Name = "Button17";
            this.Button17.Size = new System.Drawing.Size(69, 17);
            this.Button17.TabIndex = 16;
            this.Button17.Text = "Button17";
            this.Button17.UseVisualStyleBackColor = true;
            this.Button17.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button16
            // 
            this.Button16.AutoSize = true;
            this.Button16.Location = new System.Drawing.Point(7, 349);
            this.Button16.Name = "Button16";
            this.Button16.Size = new System.Drawing.Size(69, 17);
            this.Button16.TabIndex = 15;
            this.Button16.Text = "Button16";
            this.Button16.UseVisualStyleBackColor = true;
            this.Button16.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button15
            // 
            this.Button15.AutoSize = true;
            this.Button15.Location = new System.Drawing.Point(7, 326);
            this.Button15.Name = "Button15";
            this.Button15.Size = new System.Drawing.Size(69, 17);
            this.Button15.TabIndex = 14;
            this.Button15.Text = "Button15";
            this.Button15.UseVisualStyleBackColor = true;
            this.Button15.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button14
            // 
            this.Button14.AutoSize = true;
            this.Button14.Location = new System.Drawing.Point(7, 303);
            this.Button14.Name = "Button14";
            this.Button14.Size = new System.Drawing.Size(69, 17);
            this.Button14.TabIndex = 13;
            this.Button14.Text = "Button14";
            this.Button14.UseVisualStyleBackColor = true;
            this.Button14.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button13
            // 
            this.Button13.AutoSize = true;
            this.Button13.Location = new System.Drawing.Point(7, 280);
            this.Button13.Name = "Button13";
            this.Button13.Size = new System.Drawing.Size(69, 17);
            this.Button13.TabIndex = 12;
            this.Button13.Text = "Button13";
            this.Button13.UseVisualStyleBackColor = true;
            this.Button13.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button12
            // 
            this.Button12.AutoSize = true;
            this.Button12.Location = new System.Drawing.Point(7, 257);
            this.Button12.Name = "Button12";
            this.Button12.Size = new System.Drawing.Size(69, 17);
            this.Button12.TabIndex = 11;
            this.Button12.Text = "Button12";
            this.Button12.UseVisualStyleBackColor = true;
            this.Button12.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button11
            // 
            this.Button11.AutoSize = true;
            this.Button11.Location = new System.Drawing.Point(7, 234);
            this.Button11.Name = "Button11";
            this.Button11.Size = new System.Drawing.Size(69, 17);
            this.Button11.TabIndex = 10;
            this.Button11.Text = "Button11";
            this.Button11.UseVisualStyleBackColor = true;
            this.Button11.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button10
            // 
            this.Button10.AutoSize = true;
            this.Button10.Location = new System.Drawing.Point(7, 211);
            this.Button10.Name = "Button10";
            this.Button10.Size = new System.Drawing.Size(69, 17);
            this.Button10.TabIndex = 9;
            this.Button10.Text = "Button10";
            this.Button10.UseVisualStyleBackColor = true;
            this.Button10.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button9
            // 
            this.Button9.AutoSize = true;
            this.Button9.Location = new System.Drawing.Point(7, 188);
            this.Button9.Name = "Button9";
            this.Button9.Size = new System.Drawing.Size(63, 17);
            this.Button9.TabIndex = 8;
            this.Button9.Text = "Button9";
            this.Button9.UseVisualStyleBackColor = true;
            this.Button9.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button8
            // 
            this.Button8.AutoSize = true;
            this.Button8.Location = new System.Drawing.Point(6, 165);
            this.Button8.Name = "Button8";
            this.Button8.Size = new System.Drawing.Size(63, 17);
            this.Button8.TabIndex = 7;
            this.Button8.Text = "Button8";
            this.Button8.UseVisualStyleBackColor = true;
            this.Button8.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button7
            // 
            this.Button7.AutoSize = true;
            this.Button7.Location = new System.Drawing.Point(6, 142);
            this.Button7.Name = "Button7";
            this.Button7.Size = new System.Drawing.Size(63, 17);
            this.Button7.TabIndex = 6;
            this.Button7.Text = "Button7";
            this.Button7.UseVisualStyleBackColor = true;
            this.Button7.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button6
            // 
            this.Button6.AutoSize = true;
            this.Button6.Location = new System.Drawing.Point(6, 119);
            this.Button6.Name = "Button6";
            this.Button6.Size = new System.Drawing.Size(63, 17);
            this.Button6.TabIndex = 5;
            this.Button6.Text = "Button6";
            this.Button6.UseVisualStyleBackColor = true;
            this.Button6.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button5
            // 
            this.Button5.AutoSize = true;
            this.Button5.Location = new System.Drawing.Point(6, 96);
            this.Button5.Name = "Button5";
            this.Button5.Size = new System.Drawing.Size(63, 17);
            this.Button5.TabIndex = 4;
            this.Button5.Text = "Button5";
            this.Button5.UseVisualStyleBackColor = true;
            this.Button5.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button4
            // 
            this.Button4.AutoSize = true;
            this.Button4.Location = new System.Drawing.Point(7, 73);
            this.Button4.Name = "Button4";
            this.Button4.Size = new System.Drawing.Size(63, 17);
            this.Button4.TabIndex = 3;
            this.Button4.Text = "Button4";
            this.Button4.UseVisualStyleBackColor = true;
            this.Button4.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button3
            // 
            this.Button3.AutoSize = true;
            this.Button3.Location = new System.Drawing.Point(7, 50);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(63, 17);
            this.Button3.TabIndex = 2;
            this.Button3.Text = "Button3";
            this.Button3.UseVisualStyleBackColor = true;
            this.Button3.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button2
            // 
            this.Button2.AutoSize = true;
            this.Button2.Location = new System.Drawing.Point(7, 27);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(63, 17);
            this.Button2.TabIndex = 1;
            this.Button2.Text = "Button2";
            this.Button2.UseVisualStyleBackColor = true;
            this.Button2.Click += new System.EventHandler(this.Button128_Click);
            // 
            // Button1
            // 
            this.Button1.AutoSize = true;
            this.Button1.Checked = true;
            this.Button1.Location = new System.Drawing.Point(7, 6);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(63, 17);
            this.Button1.TabIndex = 0;
            this.Button1.TabStop = true;
            this.Button1.Text = "Button1";
            this.Button1.UseVisualStyleBackColor = true;
            this.Button1.Click += new System.EventHandler(this.Button128_Click);
            // 
            // FRz
            // 
            this.FRz.Location = new System.Drawing.Point(25, 123);
            this.FRz.Maximum = 65535;
            this.FRz.Name = "FRz";
            this.FRz.Size = new System.Drawing.Size(104, 45);
            this.FRz.TabIndex = 22;
            this.FRz.Value = 32768;
            // 
            // joystickList
            // 
            this.joystickList.FormattingEnabled = true;
            this.joystickList.Location = new System.Drawing.Point(112, 9);
            this.joystickList.Name = "joystickList";
            this.joystickList.Size = new System.Drawing.Size(331, 21);
            this.joystickList.TabIndex = 4;
            this.joystickList.SelectedValueChanged += new System.EventHandler(this.joystickList_SelectedValueChanged);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.Axis);
            this.tabControl.Controls.Add(this.Buttons);
            this.tabControl.Controls.Add(this.connection_monitor);
            this.tabControl.Enabled = false;
            this.tabControl.Location = new System.Drawing.Point(15, 40);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(766, 413);
            this.tabControl.TabIndex = 5;
            // 
            // Axis
            // 
            this.Axis.Controls.Add(this.groupBox5);
            this.Axis.Controls.Add(this.groupBox4);
            this.Axis.Controls.Add(this.groupBox6);
            this.Axis.Controls.Add(this.groupBox3);
            this.Axis.Controls.Add(this.groupBox7);
            this.Axis.Controls.Add(this.groupBox2);
            this.Axis.Controls.Add(this.groupBox8);
            this.Axis.Controls.Add(this.groupBox1);
            this.Axis.Location = new System.Drawing.Point(4, 22);
            this.Axis.Name = "Axis";
            this.Axis.Padding = new System.Windows.Forms.Padding(3);
            this.Axis.Size = new System.Drawing.Size(758, 387);
            this.Axis.TabIndex = 0;
            this.Axis.Text = "Axis";
            this.Axis.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.CFRz);
            this.groupBox5.Controls.Add(this.LFRz);
            this.groupBox5.Controls.Add(this.CFRy);
            this.groupBox5.Controls.Add(this.LFRy);
            this.groupBox5.Controls.Add(this.CFRx);
            this.groupBox5.Controls.Add(this.FRz);
            this.groupBox5.Controls.Add(this.LFRx);
            this.groupBox5.Controls.Add(this.FRx);
            this.groupBox5.Controls.Add(this.FRy);
            this.groupBox5.Location = new System.Drawing.Point(565, 194);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(180, 181);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "torque";
            // 
            // CFRz
            // 
            this.CFRz.Location = new System.Drawing.Point(134, 125);
            this.CFRz.Name = "CFRz";
            this.CFRz.Size = new System.Drawing.Size(38, 20);
            this.CFRz.TabIndex = 45;
            // 
            // LFRz
            // 
            this.LFRz.AutoSize = true;
            this.LFRz.Location = new System.Drawing.Point(6, 123);
            this.LFRz.Name = "LFRz";
            this.LFRz.Size = new System.Drawing.Size(13, 13);
            this.LFRz.TabIndex = 5;
            this.LFRz.Text = "Z";
            this.LFRz.Click += new System.EventHandler(this.LFX_Click);
            // 
            // CFRy
            // 
            this.CFRy.Location = new System.Drawing.Point(134, 75);
            this.CFRy.Name = "CFRy";
            this.CFRy.Size = new System.Drawing.Size(38, 20);
            this.CFRy.TabIndex = 44;
            // 
            // LFRy
            // 
            this.LFRy.AutoSize = true;
            this.LFRy.Location = new System.Drawing.Point(6, 71);
            this.LFRy.Name = "LFRy";
            this.LFRy.Size = new System.Drawing.Size(13, 13);
            this.LFRy.TabIndex = 4;
            this.LFRy.Text = "Y";
            this.LFRy.Click += new System.EventHandler(this.LFX_Click);
            // 
            // CFRx
            // 
            this.CFRx.Location = new System.Drawing.Point(135, 21);
            this.CFRx.Name = "CFRx";
            this.CFRx.Size = new System.Drawing.Size(38, 20);
            this.CFRx.TabIndex = 43;
            // 
            // LFRx
            // 
            this.LFRx.AutoSize = true;
            this.LFRx.Location = new System.Drawing.Point(6, 19);
            this.LFRx.Name = "LFRx";
            this.LFRx.Size = new System.Drawing.Size(13, 13);
            this.LFRx.TabIndex = 3;
            this.LFRx.Text = "X";
            this.LFRx.Click += new System.EventHandler(this.LFX_Click);
            // 
            // FRx
            // 
            this.FRx.Location = new System.Drawing.Point(25, 21);
            this.FRx.Maximum = 65535;
            this.FRx.Name = "FRx";
            this.FRx.Size = new System.Drawing.Size(104, 45);
            this.FRx.TabIndex = 15;
            this.FRx.Value = 32768;
            // 
            // FRy
            // 
            this.FRy.Location = new System.Drawing.Point(25, 72);
            this.FRy.Maximum = 65535;
            this.FRy.Name = "FRy";
            this.FRy.Size = new System.Drawing.Size(104, 45);
            this.FRy.TabIndex = 19;
            this.FRy.Value = 32768;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.CFZ);
            this.groupBox4.Controls.Add(this.LFZ);
            this.groupBox4.Controls.Add(this.CFY);
            this.groupBox4.Controls.Add(this.FZ);
            this.groupBox4.Controls.Add(this.CFX);
            this.groupBox4.Controls.Add(this.FY);
            this.groupBox4.Controls.Add(this.FX);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.LFY);
            this.groupBox4.Controls.Add(this.LFX);
            this.groupBox4.Location = new System.Drawing.Point(565, 7);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(180, 181);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "force";
            // 
            // CFZ
            // 
            this.CFZ.Location = new System.Drawing.Point(135, 120);
            this.CFZ.Name = "CFZ";
            this.CFZ.Size = new System.Drawing.Size(38, 20);
            this.CFZ.TabIndex = 39;
            // 
            // LFZ
            // 
            this.LFZ.AutoSize = true;
            this.LFZ.Location = new System.Drawing.Point(8, 121);
            this.LFZ.Name = "LFZ";
            this.LFZ.Size = new System.Drawing.Size(13, 13);
            this.LFZ.TabIndex = 9;
            this.LFZ.Text = "Z";
            this.LFZ.Click += new System.EventHandler(this.LFX_Click);
            // 
            // CFY
            // 
            this.CFY.Location = new System.Drawing.Point(135, 70);
            this.CFY.Name = "CFY";
            this.CFY.Size = new System.Drawing.Size(38, 20);
            this.CFY.TabIndex = 38;
            // 
            // FZ
            // 
            this.FZ.Location = new System.Drawing.Point(27, 119);
            this.FZ.Maximum = 65535;
            this.FZ.Name = "FZ";
            this.FZ.Size = new System.Drawing.Size(104, 45);
            this.FZ.TabIndex = 11;
            this.FZ.Value = 32768;
            // 
            // CFX
            // 
            this.CFX.Location = new System.Drawing.Point(136, 16);
            this.CFX.Name = "CFX";
            this.CFX.Size = new System.Drawing.Size(38, 20);
            this.CFX.TabIndex = 37;
            // 
            // FY
            // 
            this.FY.Location = new System.Drawing.Point(27, 68);
            this.FY.Maximum = 65535;
            this.FY.Name = "FY";
            this.FY.Size = new System.Drawing.Size(104, 45);
            this.FY.TabIndex = 10;
            this.FY.Value = 32768;
            // 
            // FX
            // 
            this.FX.Location = new System.Drawing.Point(27, 17);
            this.FX.Maximum = 65535;
            this.FX.Name = "FX";
            this.FX.Size = new System.Drawing.Size(104, 45);
            this.FX.TabIndex = 8;
            this.FX.Value = 32768;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 123);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Z";
            // 
            // LFY
            // 
            this.LFY.AutoSize = true;
            this.LFY.Location = new System.Drawing.Point(6, 71);
            this.LFY.Name = "LFY";
            this.LFY.Size = new System.Drawing.Size(13, 13);
            this.LFY.TabIndex = 4;
            this.LFY.Text = "Y";
            this.LFY.Click += new System.EventHandler(this.LFX_Click);
            // 
            // LFX
            // 
            this.LFX.AutoSize = true;
            this.LFX.Location = new System.Drawing.Point(6, 19);
            this.LFX.Name = "LFX";
            this.LFX.Size = new System.Drawing.Size(13, 13);
            this.LFX.TabIndex = 3;
            this.LFX.Text = "X";
            this.LFX.Click += new System.EventHandler(this.LFX_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.CARz);
            this.groupBox6.Controls.Add(this.LARz);
            this.groupBox6.Controls.Add(this.CARy);
            this.groupBox6.Controls.Add(this.LARy);
            this.groupBox6.Controls.Add(this.CARx);
            this.groupBox6.Controls.Add(this.LARx);
            this.groupBox6.Controls.Add(this.ARx);
            this.groupBox6.Controls.Add(this.ARz);
            this.groupBox6.Controls.Add(this.ARy);
            this.groupBox6.Location = new System.Drawing.Point(379, 194);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(180, 181);
            this.groupBox6.TabIndex = 9;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "angular acceleration";
            // 
            // CARz
            // 
            this.CARz.Location = new System.Drawing.Point(134, 123);
            this.CARz.Name = "CARz";
            this.CARz.Size = new System.Drawing.Size(38, 20);
            this.CARz.TabIndex = 42;
            // 
            // LARz
            // 
            this.LARz.AutoSize = true;
            this.LARz.Location = new System.Drawing.Point(6, 123);
            this.LARz.Name = "LARz";
            this.LARz.Size = new System.Drawing.Size(13, 13);
            this.LARz.TabIndex = 5;
            this.LARz.Text = "Z";
            this.LARz.Click += new System.EventHandler(this.LFX_Click);
            // 
            // CARy
            // 
            this.CARy.Location = new System.Drawing.Point(134, 73);
            this.CARy.Name = "CARy";
            this.CARy.Size = new System.Drawing.Size(38, 20);
            this.CARy.TabIndex = 41;
            // 
            // LARy
            // 
            this.LARy.AutoSize = true;
            this.LARy.Location = new System.Drawing.Point(6, 71);
            this.LARy.Name = "LARy";
            this.LARy.Size = new System.Drawing.Size(13, 13);
            this.LARy.TabIndex = 4;
            this.LARy.Text = "Y";
            this.LARy.Click += new System.EventHandler(this.LFX_Click);
            // 
            // CARx
            // 
            this.CARx.Location = new System.Drawing.Point(135, 19);
            this.CARx.Name = "CARx";
            this.CARx.Size = new System.Drawing.Size(38, 20);
            this.CARx.TabIndex = 40;
            // 
            // LARx
            // 
            this.LARx.AutoSize = true;
            this.LARx.Location = new System.Drawing.Point(6, 19);
            this.LARx.Name = "LARx";
            this.LARx.Size = new System.Drawing.Size(13, 13);
            this.LARx.TabIndex = 3;
            this.LARx.Text = "X";
            this.LARx.Click += new System.EventHandler(this.LFX_Click);
            // 
            // ARx
            // 
            this.ARx.Location = new System.Drawing.Point(25, 21);
            this.ARx.Maximum = 65535;
            this.ARx.Name = "ARx";
            this.ARx.Size = new System.Drawing.Size(104, 45);
            this.ARx.TabIndex = 12;
            this.ARx.Value = 32768;
            // 
            // ARz
            // 
            this.ARz.Location = new System.Drawing.Point(27, 121);
            this.ARz.Maximum = 65535;
            this.ARz.Name = "ARz";
            this.ARz.Size = new System.Drawing.Size(104, 45);
            this.ARz.TabIndex = 23;
            this.ARz.Value = 32768;
            // 
            // ARy
            // 
            this.ARy.Location = new System.Drawing.Point(27, 70);
            this.ARy.Maximum = 65535;
            this.ARy.Name = "ARy";
            this.ARy.Size = new System.Drawing.Size(104, 45);
            this.ARy.TabIndex = 20;
            this.ARy.Value = 32768;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CAZ);
            this.groupBox3.Controls.Add(this.LAZ);
            this.groupBox3.Controls.Add(this.CAY);
            this.groupBox3.Controls.Add(this.AZ);
            this.groupBox3.Controls.Add(this.CAX);
            this.groupBox3.Controls.Add(this.AY);
            this.groupBox3.Controls.Add(this.AX);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.LAY);
            this.groupBox3.Controls.Add(this.LAX);
            this.groupBox3.Location = new System.Drawing.Point(379, 7);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(180, 181);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "acceleration";
            // 
            // CAZ
            // 
            this.CAZ.Location = new System.Drawing.Point(136, 121);
            this.CAZ.Name = "CAZ";
            this.CAZ.Size = new System.Drawing.Size(38, 20);
            this.CAZ.TabIndex = 36;
            // 
            // LAZ
            // 
            this.LAZ.AutoSize = true;
            this.LAZ.Location = new System.Drawing.Point(8, 121);
            this.LAZ.Name = "LAZ";
            this.LAZ.Size = new System.Drawing.Size(13, 13);
            this.LAZ.TabIndex = 9;
            this.LAZ.Text = "Z";
            this.LAZ.Click += new System.EventHandler(this.LFX_Click);
            // 
            // CAY
            // 
            this.CAY.Location = new System.Drawing.Point(136, 71);
            this.CAY.Name = "CAY";
            this.CAY.Size = new System.Drawing.Size(38, 20);
            this.CAY.TabIndex = 35;
            // 
            // AZ
            // 
            this.AZ.Location = new System.Drawing.Point(27, 119);
            this.AZ.Maximum = 65535;
            this.AZ.Name = "AZ";
            this.AZ.Size = new System.Drawing.Size(104, 45);
            this.AZ.TabIndex = 11;
            this.AZ.Value = 32768;
            // 
            // CAX
            // 
            this.CAX.Location = new System.Drawing.Point(137, 17);
            this.CAX.Name = "CAX";
            this.CAX.Size = new System.Drawing.Size(38, 20);
            this.CAX.TabIndex = 34;
            // 
            // AY
            // 
            this.AY.Location = new System.Drawing.Point(27, 68);
            this.AY.Maximum = 65535;
            this.AY.Name = "AY";
            this.AY.Size = new System.Drawing.Size(104, 45);
            this.AY.TabIndex = 10;
            this.AY.Value = 32768;
            // 
            // AX
            // 
            this.AX.Location = new System.Drawing.Point(27, 17);
            this.AX.Maximum = 65535;
            this.AX.Name = "AX";
            this.AX.Size = new System.Drawing.Size(104, 45);
            this.AX.TabIndex = 8;
            this.AX.Value = 32768;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 123);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Z";
            // 
            // LAY
            // 
            this.LAY.AutoSize = true;
            this.LAY.Location = new System.Drawing.Point(6, 71);
            this.LAY.Name = "LAY";
            this.LAY.Size = new System.Drawing.Size(13, 13);
            this.LAY.TabIndex = 4;
            this.LAY.Text = "Y";
            this.LAY.Click += new System.EventHandler(this.LFX_Click);
            // 
            // LAX
            // 
            this.LAX.AutoSize = true;
            this.LAX.Location = new System.Drawing.Point(6, 19);
            this.LAX.Name = "LAX";
            this.LAX.Size = new System.Drawing.Size(13, 13);
            this.LAX.TabIndex = 3;
            this.LAX.Text = "X";
            this.LAX.Click += new System.EventHandler(this.LFX_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.CVRz);
            this.groupBox7.Controls.Add(this.LVRz);
            this.groupBox7.Controls.Add(this.CVRy);
            this.groupBox7.Controls.Add(this.LVRy);
            this.groupBox7.Controls.Add(this.CVRx);
            this.groupBox7.Controls.Add(this.LVRx);
            this.groupBox7.Controls.Add(this.VRx);
            this.groupBox7.Controls.Add(this.VRy);
            this.groupBox7.Controls.Add(this.VRz);
            this.groupBox7.Location = new System.Drawing.Point(193, 194);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(180, 181);
            this.groupBox7.TabIndex = 10;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "angular velocity";
            // 
            // CVRz
            // 
            this.CVRz.Location = new System.Drawing.Point(134, 125);
            this.CVRz.Name = "CVRz";
            this.CVRz.Size = new System.Drawing.Size(38, 20);
            this.CVRz.TabIndex = 30;
            // 
            // LVRz
            // 
            this.LVRz.AutoSize = true;
            this.LVRz.Location = new System.Drawing.Point(6, 123);
            this.LVRz.Name = "LVRz";
            this.LVRz.Size = new System.Drawing.Size(13, 13);
            this.LVRz.TabIndex = 5;
            this.LVRz.Text = "Z";
            this.LVRz.Click += new System.EventHandler(this.LFX_Click);
            // 
            // CVRy
            // 
            this.CVRy.Location = new System.Drawing.Point(134, 75);
            this.CVRy.Name = "CVRy";
            this.CVRy.Size = new System.Drawing.Size(38, 20);
            this.CVRy.TabIndex = 29;
            // 
            // LVRy
            // 
            this.LVRy.AutoSize = true;
            this.LVRy.Location = new System.Drawing.Point(6, 71);
            this.LVRy.Name = "LVRy";
            this.LVRy.Size = new System.Drawing.Size(13, 13);
            this.LVRy.TabIndex = 4;
            this.LVRy.Text = "Y";
            this.LVRy.Click += new System.EventHandler(this.LFX_Click);
            // 
            // CVRx
            // 
            this.CVRx.Location = new System.Drawing.Point(135, 21);
            this.CVRx.Name = "CVRx";
            this.CVRx.Size = new System.Drawing.Size(38, 20);
            this.CVRx.TabIndex = 28;
            // 
            // LVRx
            // 
            this.LVRx.AutoSize = true;
            this.LVRx.Location = new System.Drawing.Point(6, 19);
            this.LVRx.Name = "LVRx";
            this.LVRx.Size = new System.Drawing.Size(13, 13);
            this.LVRx.TabIndex = 3;
            this.LVRx.Text = "X";
            this.LVRx.Click += new System.EventHandler(this.LFX_Click);
            // 
            // VRx
            // 
            this.VRx.Location = new System.Drawing.Point(25, 21);
            this.VRx.Maximum = 65535;
            this.VRx.Name = "VRx";
            this.VRx.Size = new System.Drawing.Size(104, 45);
            this.VRx.TabIndex = 16;
            this.VRx.Value = 32768;
            // 
            // VRy
            // 
            this.VRy.Location = new System.Drawing.Point(23, 74);
            this.VRy.Maximum = 65535;
            this.VRy.Name = "VRy";
            this.VRy.Size = new System.Drawing.Size(104, 45);
            this.VRy.TabIndex = 13;
            this.VRy.Value = 32768;
            // 
            // selectJoystick
            // 
            this.selectJoystick.AutoSize = true;
            this.selectJoystick.Font = new System.Drawing.Font("Book Antiqua", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectJoystick.Location = new System.Drawing.Point(12, 9);
            this.selectJoystick.Name = "selectJoystick";
            this.selectJoystick.Size = new System.Drawing.Size(94, 18);
            this.selectJoystick.TabIndex = 3;
            this.selectJoystick.Text = "Select Joystick";
            // 
            // connect
            // 
            this.connect.Location = new System.Drawing.Point(23, 456);
            this.connect.Name = "connect";
            this.connect.Size = new System.Drawing.Size(75, 23);
            this.connect.TabIndex = 6;
            this.connect.Text = "connect";
            this.connect.UseVisualStyleBackColor = true;
            this.connect.Click += new System.EventHandler(this.connect_Click);
            // 
            // Disconnect
            // 
            this.Disconnect.Enabled = false;
            this.Disconnect.Location = new System.Drawing.Point(104, 456);
            this.Disconnect.Name = "Disconnect";
            this.Disconnect.Size = new System.Drawing.Size(75, 23);
            this.Disconnect.TabIndex = 7;
            this.Disconnect.Text = "Disconnect";
            this.Disconnect.UseVisualStyleBackColor = true;
            this.Disconnect.Click += new System.EventHandler(this.Disconnect_Click);
            // 
            // FlagBox
            // 
            this.FlagBox.AutoSize = true;
            this.FlagBox.Location = new System.Drawing.Point(699, 13);
            this.FlagBox.Name = "FlagBox";
            this.FlagBox.Size = new System.Drawing.Size(77, 17);
            this.FlagBox.TabIndex = 8;
            this.FlagBox.Text = "checkBox1";
            this.FlagBox.UseVisualStyleBackColor = true;
            this.FlagBox.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 491);
            this.Controls.Add(this.FlagBox);
            this.Controls.Add(this.Disconnect);
            this.Controls.Add(this.connect);
            this.Controls.Add(this.joystickList);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.selectJoystick);
            this.Name = "Form1";
            this.Text = "Joystick";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VX)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Z)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.X)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Rz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VRz)).EndInit();
            this.connection_monitor.ResumeLayout(false);
            this.Buttons.ResumeLayout(false);
            this.Buttons.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRz)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.Axis.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FRy)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FX)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ARx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ARz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ARy)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AX)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VRx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VRy)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox CVZ;
        private System.Windows.Forms.Label LVZ;
        private System.Windows.Forms.TextBox CVY;
        private System.Windows.Forms.TrackBar VZ;
        private System.Windows.Forms.TextBox CVX;
        private System.Windows.Forms.TrackBar VY;
        private System.Windows.Forms.TrackBar VX;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LVY;
        private System.Windows.Forms.Label LVX;
        private System.Windows.Forms.TextBox CZ;
        private System.Windows.Forms.TextBox CY;
        private System.Windows.Forms.TextBox CX;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TrackBar Z;
        private System.Windows.Forms.TrackBar Y;
        private System.Windows.Forms.Label LZ;
        private System.Windows.Forms.Label LY;
        private System.Windows.Forms.Label LX;
        private System.Windows.Forms.TrackBar X;
        private System.Windows.Forms.TextBox CRz;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox CRy;
        private System.Windows.Forms.TextBox CRx;
        private System.Windows.Forms.Label LRz;
        private System.Windows.Forms.Label LRy;
        private System.Windows.Forms.Label LRx;
        private System.Windows.Forms.TrackBar Rz;
        private System.Windows.Forms.TrackBar Rx;
        private System.Windows.Forms.TrackBar Ry;
        private System.Windows.Forms.TrackBar VRz;
        private System.Windows.Forms.ListBox fieldList;
        private System.Windows.Forms.TabPage connection_monitor;
        private System.Windows.Forms.RadioButton Button128;
        private System.Windows.Forms.RadioButton Button127;
        private System.Windows.Forms.RadioButton Button126;
        private System.Windows.Forms.RadioButton Button125;
        private System.Windows.Forms.RadioButton Button124;
        private System.Windows.Forms.RadioButton Button123;
        private System.Windows.Forms.RadioButton Button122;
        private System.Windows.Forms.RadioButton Button121;
        private System.Windows.Forms.RadioButton Button120;
        private System.Windows.Forms.RadioButton Button119;
        private System.Windows.Forms.RadioButton Button118;
        private System.Windows.Forms.RadioButton Button117;
        private System.Windows.Forms.RadioButton Button116;
        private System.Windows.Forms.RadioButton Button115;
        private System.Windows.Forms.RadioButton Button114;
        private System.Windows.Forms.RadioButton Button113;
        private System.Windows.Forms.RadioButton Button112;
        private System.Windows.Forms.RadioButton Button111;
        private System.Windows.Forms.RadioButton Button110;
        private System.Windows.Forms.RadioButton Button109;
        private System.Windows.Forms.TabPage Buttons;
        private System.Windows.Forms.RadioButton Button108;
        private System.Windows.Forms.RadioButton Button107;
        private System.Windows.Forms.RadioButton Button106;
        private System.Windows.Forms.RadioButton Button105;
        private System.Windows.Forms.RadioButton Button104;
        private System.Windows.Forms.RadioButton Button103;
        private System.Windows.Forms.RadioButton Button102;
        private System.Windows.Forms.RadioButton Button101;
        private System.Windows.Forms.RadioButton Button100;
        private System.Windows.Forms.RadioButton Button99;
        private System.Windows.Forms.RadioButton Button98;
        private System.Windows.Forms.RadioButton Button97;
        private System.Windows.Forms.RadioButton Button96;
        private System.Windows.Forms.RadioButton Button95;
        private System.Windows.Forms.RadioButton Button94;
        private System.Windows.Forms.RadioButton Button93;
        private System.Windows.Forms.RadioButton Button92;
        private System.Windows.Forms.RadioButton Button91;
        private System.Windows.Forms.RadioButton Button90;
        private System.Windows.Forms.RadioButton Button89;
        private System.Windows.Forms.RadioButton Button88;
        private System.Windows.Forms.RadioButton Button87;
        private System.Windows.Forms.RadioButton Button86;
        private System.Windows.Forms.RadioButton Button85;
        private System.Windows.Forms.RadioButton Button84;
        private System.Windows.Forms.RadioButton Button83;
        private System.Windows.Forms.RadioButton Button82;
        private System.Windows.Forms.RadioButton Button81;
        private System.Windows.Forms.RadioButton Button48;
        private System.Windows.Forms.RadioButton Button47;
        private System.Windows.Forms.RadioButton Button46;
        private System.Windows.Forms.RadioButton Button45;
        private System.Windows.Forms.RadioButton Button44;
        private System.Windows.Forms.RadioButton Button43;
        private System.Windows.Forms.RadioButton Button42;
        private System.Windows.Forms.RadioButton Button41;
        private System.Windows.Forms.RadioButton Button40;
        private System.Windows.Forms.RadioButton Button39;
        private System.Windows.Forms.RadioButton Button38;
        private System.Windows.Forms.RadioButton Button37;
        private System.Windows.Forms.RadioButton Button36;
        private System.Windows.Forms.RadioButton Button80;
        private System.Windows.Forms.RadioButton Button79;
        private System.Windows.Forms.RadioButton Button78;
        private System.Windows.Forms.RadioButton Button77;
        private System.Windows.Forms.RadioButton Button76;
        private System.Windows.Forms.RadioButton Button75;
        private System.Windows.Forms.RadioButton Button74;
        private System.Windows.Forms.RadioButton Button73;
        private System.Windows.Forms.RadioButton Button72;
        private System.Windows.Forms.RadioButton Button71;
        private System.Windows.Forms.RadioButton Button70;
        private System.Windows.Forms.RadioButton Button69;
        private System.Windows.Forms.RadioButton Button68;
        private System.Windows.Forms.RadioButton Button67;
        private System.Windows.Forms.RadioButton Button66;
        private System.Windows.Forms.RadioButton Button65;
        private System.Windows.Forms.RadioButton Button64;
        private System.Windows.Forms.RadioButton Button63;
        private System.Windows.Forms.RadioButton Button62;
        private System.Windows.Forms.RadioButton Button61;
        private System.Windows.Forms.RadioButton Button60;
        private System.Windows.Forms.RadioButton Button59;
        private System.Windows.Forms.RadioButton Button58;
        private System.Windows.Forms.RadioButton Button57;
        private System.Windows.Forms.RadioButton Button56;
        private System.Windows.Forms.RadioButton Button55;
        private System.Windows.Forms.RadioButton Button54;
        private System.Windows.Forms.RadioButton Button53;
        private System.Windows.Forms.RadioButton Button52;
        private System.Windows.Forms.RadioButton Button51;
        private System.Windows.Forms.RadioButton Button50;
        private System.Windows.Forms.RadioButton Button49;
        private System.Windows.Forms.RadioButton Button35;
        private System.Windows.Forms.RadioButton Button34;
        private System.Windows.Forms.RadioButton Button33;
        private System.Windows.Forms.RadioButton Button32;
        private System.Windows.Forms.RadioButton Button31;
        private System.Windows.Forms.RadioButton Button30;
        private System.Windows.Forms.RadioButton Button29;
        private System.Windows.Forms.RadioButton Button28;
        private System.Windows.Forms.RadioButton Button27;
        private System.Windows.Forms.RadioButton Button26;
        private System.Windows.Forms.RadioButton Button25;
        private System.Windows.Forms.RadioButton Button24;
        private System.Windows.Forms.RadioButton Button23;
        private System.Windows.Forms.RadioButton Button22;
        private System.Windows.Forms.RadioButton Button21;
        private System.Windows.Forms.RadioButton Button20;
        private System.Windows.Forms.RadioButton Button19;
        private System.Windows.Forms.RadioButton Button18;
        private System.Windows.Forms.RadioButton Button17;
        private System.Windows.Forms.RadioButton Button16;
        private System.Windows.Forms.RadioButton Button15;
        private System.Windows.Forms.RadioButton Button14;
        private System.Windows.Forms.RadioButton Button13;
        private System.Windows.Forms.RadioButton Button12;
        private System.Windows.Forms.RadioButton Button11;
        private System.Windows.Forms.RadioButton Button10;
        private System.Windows.Forms.RadioButton Button9;
        private System.Windows.Forms.RadioButton Button8;
        private System.Windows.Forms.RadioButton Button7;
        private System.Windows.Forms.RadioButton Button6;
        private System.Windows.Forms.RadioButton Button5;
        private System.Windows.Forms.RadioButton Button4;
        private System.Windows.Forms.RadioButton Button3;
        private System.Windows.Forms.RadioButton Button2;
        private System.Windows.Forms.RadioButton Button1;
        private System.Windows.Forms.TrackBar FRz;
        private System.Windows.Forms.ComboBox joystickList;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage Axis;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox CFRz;
        private System.Windows.Forms.Label LFRz;
        private System.Windows.Forms.TextBox CFRy;
        private System.Windows.Forms.Label LFRy;
        private System.Windows.Forms.TextBox CFRx;
        private System.Windows.Forms.Label LFRx;
        private System.Windows.Forms.TrackBar FRx;
        private System.Windows.Forms.TrackBar FRy;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox CFZ;
        private System.Windows.Forms.Label LFZ;
        private System.Windows.Forms.TextBox CFY;
        private System.Windows.Forms.TrackBar FZ;
        private System.Windows.Forms.TextBox CFX;
        private System.Windows.Forms.TrackBar FY;
        private System.Windows.Forms.TrackBar FX;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label LFY;
        private System.Windows.Forms.Label LFX;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox CARz;
        private System.Windows.Forms.Label LARz;
        private System.Windows.Forms.TextBox CARy;
        private System.Windows.Forms.Label LARy;
        private System.Windows.Forms.TextBox CARx;
        private System.Windows.Forms.Label LARx;
        private System.Windows.Forms.TrackBar ARx;
        private System.Windows.Forms.TrackBar ARz;
        private System.Windows.Forms.TrackBar ARy;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox CAZ;
        private System.Windows.Forms.Label LAZ;
        private System.Windows.Forms.TextBox CAY;
        private System.Windows.Forms.TrackBar AZ;
        private System.Windows.Forms.TextBox CAX;
        private System.Windows.Forms.TrackBar AY;
        private System.Windows.Forms.TrackBar AX;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label LAY;
        private System.Windows.Forms.Label LAX;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox CVRz;
        private System.Windows.Forms.Label LVRz;
        private System.Windows.Forms.TextBox CVRy;
        private System.Windows.Forms.Label LVRy;
        private System.Windows.Forms.TextBox CVRx;
        private System.Windows.Forms.Label LVRx;
        private System.Windows.Forms.TrackBar VRx;
        private System.Windows.Forms.TrackBar VRy;
        private System.Windows.Forms.Label selectJoystick;
        private System.Windows.Forms.Button connect;
        private System.Windows.Forms.Button Disconnect;
        private System.Windows.Forms.CheckBox FlagBox;

    }
}

