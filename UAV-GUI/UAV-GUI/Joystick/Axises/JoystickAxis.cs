﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JoystickPlugin.Enumeration;

namespace JoystickPlugin
{
    class JoystickAxis
    {
        PIC_Control pic_control;
        PRV_Control prv_control;
        int axisMinimum;
        int axisMaximum;
        int fieldId;// if pic_control & prv_control are none
        int axisValue;
        string label;

        public JoystickAxis() {
            pic_control = PIC_Control.None;
            prv_control = PRV_Control.None;
            axisMinimum = 0;
            axisMaximum = 0;
        }
      
        public PIC_Control Pic_control
        {
            get { return pic_control; }
            set { pic_control = value; }
        }
        

        public PRV_Control Prv_control
        {
            get { return prv_control; }
            set { prv_control = value; }
        }

        public int AxisMinimum
        {
            get { return axisMinimum; }
            set { axisMinimum = value; }
        }

        public int AxisMaximum
        {
            get { return axisMaximum; }
            set { axisMaximum = value; }
        }

        public int FieldId
        {
            get { return fieldId; }
            set { fieldId = value; }
        }

        public int AxisValue
        {
            get { return axisValue; }
            set {
                axisValue = (int)map(value, 0, 65535, axisMinimum, axisMaximum);
            }
        }

        public string Label
        {
            get { return label; }
            set { label = value; }
        }



        public void axisAction() { 
             if (this.Pic_control == PIC_Control.Elevator)
             {
                ManualControlData.elevator = AxisValue;
             }
             if (this.Pic_control == PIC_Control.Aileron)
             {
                 ManualControlData.aileron = AxisValue;
             }
             if (this.Pic_control == PIC_Control.Rudder)
             {
                 ManualControlData.rudder = AxisValue;
             }
             if (this.Pic_control == PIC_Control.Throttle)
             {
                 ManualControlData.throttle = AxisValue;
             }
             if (this.Prv_control == PRV_Control.AirSpeed)
             {
                 ManualControlData.airSpeed = AxisValue;
             }
             if (this.Prv_control == PRV_Control.Altitude)
             {
                 ManualControlData.altitude = AxisValue;
             }
             if (this.Prv_control == PRV_Control.Heading)
             {
                 ManualControlData.heading = AxisValue;
             }
             if (this.FieldId != 0) {
                 if (ManualControlData.fieldList.ContainsKey(this.FieldId))
                 {
                     ManualControlData.fieldList[this.FieldId] = AxisValue;
                 }
                 else {
                     ManualControlData.fieldList.Add(this.FieldId, AxisValue);
                 }
             }
        }

        private static decimal map(decimal x, decimal in_min, decimal in_max, decimal out_min, decimal out_max)
        {
            return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
        }
    }
}
