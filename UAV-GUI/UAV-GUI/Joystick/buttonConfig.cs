﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using JoystickPlugin.Enumeration;

namespace JoystickPlugin
{
    public partial class buttonConfig : Form
    {
        int index;
        string joystickname;
        public CheckBox Flag;

        public string Joystickname
        {
            get { return joystickname; }
            set { joystickname = value; }
        }

        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        public buttonConfig()
        {
            InitializeComponent();
        }

        private void buttonConfig_Load(object sender, EventArgs e)
        {
            inti();
            this.buttonFunction.SelectedIndex = buttonFunction.Items.IndexOf(GUIData.buttons[index - 1].ButtonFunction);
            this.buttonAction.SelectedIndex = buttonAction.Items.IndexOf(GUIData.buttons[index - 1].ButtonAction);
            this.filedId.Text = "" + GUIData.buttons[index - 1].FieldId;
            this.firstValue.Text = "" + GUIData.buttons[index - 1].PressedValue;
            this.secondValue.Text = "" + GUIData.buttons[index - 1].UnPressedValue;
            this.buttonLable.Text = GUIData.buttons[index - 1].Label;
        }

        private void ok_Click(object sender, EventArgs e)
        {
            GUIData.buttons[index - 1].ButtonFunction = (ButtonFunction)buttonFunction.SelectedItem;
            GUIData.buttons[index - 1].ButtonAction = (int)buttonAction.SelectedItem;
            GUIData.buttons[index - 1].FieldId = Int32.Parse(filedId.Text);
            GUIData.buttons[index - 1].PressedValue = Int32.Parse(firstValue.Text);
            GUIData.buttons[index - 1].UnPressedValue = Int32.Parse(secondValue.Text);
            GUIData.buttons[index - 1].Label = this.buttonLable.Text;
            GUIData.writeJoystick(joystickname);
            JoystickRunner.stop();
            GUIData.readJoystick(joystickname);
            JoystickRunner.start();
            Flag.Checked = false;
            this.Close();
        }

        private void cancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void inti()
        {
            this.buttonFunction.Items.Add(ButtonFunction._ignore);
            this.buttonFunction.Items.Add(ButtonFunction.Pattern);
            this.buttonFunction.Items.Add(ButtonFunction.Thread);
            this.buttonFunction.Items.Add(ButtonFunction.MP_Mode);
            this.buttonFunction.Items.Add(ButtonFunction.FieldId);

            buttonFunction.SelectedIndex = buttonFunction.Items.IndexOf(ButtonFunction._ignore);
        }

        private void buttonFunction_SelectedValueChanged(object sender, EventArgs e)
        {
            ButtonFunction selectedButtonFunction = (ButtonFunction)buttonFunction.SelectedItem;
            if (selectedButtonFunction == ButtonFunction._ignore)
            {
                buttonAction.Enabled = false;
                filedId.Enabled = false;
                firstValue.Enabled = false;
                secondValue.Enabled = false;
                buttonLable.Enabled = false;
            }
            else if (selectedButtonFunction == ButtonFunction.FieldId)
            {
                buttonAction.Enabled = false;
                filedId.Enabled = true;
                firstValue.Enabled = true;
                secondValue.Enabled = true;
                buttonLable.Enabled = true;
            }
            else if (selectedButtonFunction == ButtonFunction.MP_Mode)
            {
                buttonAction.Enabled = true;
                filedId.Enabled = false;
                firstValue.Enabled = false;
                secondValue.Enabled = false;
                buttonLable.Enabled = true;

                this.buttonAction.Items.Clear();
                this.buttonAction.Items.Add(MP_Mode.UAV_PIC);
                this.buttonAction.Items.Add(MP_Mode.UAV_RPV);

                buttonAction.SelectedIndex = buttonAction.Items.IndexOf(MP_Mode.UAV_PIC);
            }
            else if (selectedButtonFunction == ButtonFunction.Pattern)
            {
                buttonAction.Enabled = true;
                filedId.Enabled = false;
                firstValue.Enabled = false;
                secondValue.Enabled = false;
                buttonLable.Enabled = true;
                this.buttonAction.Items.Clear();
                this.buttonAction.Items.AddRange(Enumerable.Range(0, 16).Cast<object>().ToArray());

                buttonAction.SelectedIndex = 0;
            }
            else if (selectedButtonFunction == ButtonFunction.Thread)
            {
                buttonAction.Enabled = true;
                filedId.Enabled = false;
                firstValue.Enabled = false;
                secondValue.Enabled = false;
                buttonLable.Enabled = true;
                this.buttonAction.Items.Clear();

                this.buttonAction.Items.AddRange(Enumerable.Range(1, 4).Cast<object>().ToArray());

                buttonAction.SelectedIndex = 0;
            }
        }


    }
}
