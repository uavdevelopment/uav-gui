﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JoystickPlugin.Enumeration;

namespace JoystickPlugin
{
    class ManualControlData
    {
        // UAV MODE
        public static MP_Mode? mode = null;
        
        // PIC MODE 
        public static int? elevator = null;
        public static int? throttle = null;
        public static int? aileron = null;
        public static int? rudder = null;

        // RPV MODE 
        public static int? heading = null;
        public static int? altitude = null;
        public static int? airSpeed = null;
        
        // Field ID List
        public static Dictionary<int, int> fieldList = new Dictionary<int, int>();

        // Pattern Stack
        public static int? currentPattern = null;

        // Thread Stack
        public static int? currentThread = null;
        
    }
}
