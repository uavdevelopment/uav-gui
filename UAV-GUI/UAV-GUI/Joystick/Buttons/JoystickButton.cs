﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JoystickPlugin.Enumeration;

namespace JoystickPlugin
{
    public abstract class JoystickButton
    {
        ButtonFunction buttonFunction;
        int button_Action;
        int buttonType;
        int fieldId;
        int fieldValue;
        int pressedValue;
        int unPressedValue;
        string label;
        bool pressed;
        int currentValue;

        public bool Pressed
        {
            get { return pressed; }
            set
            {
                pressed = value;
                if (value == true)
                {
                    currentValue = pressedValue;
                }
                else
                {
                    currentValue = unPressedValue;
                }
            }
        }

        public int CurrentValue
        {
            get { return currentValue; }
            set { currentValue = value; }
        }
   
        public ButtonFunction ButtonFunction
        {
            get { return buttonFunction; }
            set { buttonFunction = value; }
        }

        public int ButtonAction
        {
            get { return button_Action; }
            set { button_Action = value; }
        }

        public int ButtonType
        {
            get { return buttonType; }
            set { buttonType = value; }
        }

        public int FieldId
        {
            get { return fieldId; }
            set { fieldId = value; }
        }
        
        public int FieldValue
        {
            get { return fieldValue; }
            set { fieldValue = value; }
        }
        
        public int PressedValue
        {
            get { return pressedValue; }
            set { pressedValue = value; }
        }

        public int UnPressedValue
        {
            get { return unPressedValue; }
            set { unPressedValue = value; }
        }
        
        public string Label
        {
            get { return label; }
            set { label = value; }
        }

        public abstract void buttonAtion();
        public abstract void unbuttonAtion();

    }
}
