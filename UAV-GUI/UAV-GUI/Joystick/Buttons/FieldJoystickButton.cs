﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JoystickPlugin.Exceptions;

namespace JoystickPlugin
{
    public class FieldJoystickButton : JoystickButton
    {
        public FieldJoystickButton(int field_id) {
            try
            {
                ManualControlData.fieldList.Add(this.FieldId, this.UnPressedValue);
            }
            catch {
                throw new FieldIdException("this field is already tacken by other button or axis");
            }
            
        }

        public override void buttonAtion()
        {
            //TODO : ser Field current value to Field List 
            ManualControlData.fieldList[FieldId] = this.PressedValue;
            //throw new NotImplementedException();
        }

        public override void unbuttonAtion()
        {
            //TODO : ser Field current value to Field List 
            ManualControlData.fieldList[FieldId] = this.UnPressedValue;
            //throw new NotImplementedException();
        }
    }
}
