﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoystickPlugin.Exceptions 
{
    class FieldIdException : Exception
    {
        public FieldIdException()
        {
        }

        public FieldIdException(string message)
            : base(message)
        {

        }

        public FieldIdException(string message, Exception inner)
            : base(message, inner)
        {

        }

    }
}
