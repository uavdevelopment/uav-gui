﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.DirectX.DirectInput;
using System.Windows.Forms;
using JoystickPlugin.Enumeration;

namespace JoystickPlugin
{
    class JoystickConnectionRunner
    {
        //0 for false, 1 for true. 
        private static int usingResource = 0;

        static Thread joystickViewThread = null;
        static ManualResetEvent stopThread = new ManualResetEvent(false);

        public static void start()
        {
            joystickViewThread = new Thread(new ThreadStart(ThreadProcSafe));
            stopThread.Reset();
            joystickViewThread.Start();
        }

        public static void stop()
        {
            if (joystickViewThread != null)
            {
                joystickViewThread.Abort();
                joystickViewThread = null;
            }
        }

        public static void suspend()
        {
            stopThread.Set();
        }

        public static void resume()
        {
            stopThread.Reset();
        }

        private static void ThreadProcSafe()
        {
            while (true)
            {
                while (stopThread.WaitOne(0))
                {

                }

                if (joystick.joy != null)
                {
                    try
                    {
                        JoystickState state = joystick.joy.CurrentJoystickState;

                        GUIData.Axis["ARx"].AxisValue = state.ARx;
                        GUIData.Axis["ARx"].axisAction();

                        GUIData.Axis["ARy"].AxisValue = state.ARy;
                        GUIData.Axis["ARy"].axisAction();

                        GUIData.Axis["ARz"].AxisValue = state.ARz;
                        GUIData.Axis["ARz"].axisAction(); ;

                        GUIData.Axis["AX"].AxisValue = state.AX;
                        GUIData.Axis["AX"].axisAction();

                        GUIData.Axis["AY"].AxisValue = state.AY;
                        GUIData.Axis["AY"].axisAction();

                        GUIData.Axis["AZ"].AxisValue = state.AZ;
                        GUIData.Axis["AZ"].axisAction();

                        GUIData.Axis["FRx"].AxisValue = state.FRx;
                        GUIData.Axis["FRx"].axisAction();

                        GUIData.Axis["FRy"].AxisValue = state.FRy;
                        GUIData.Axis["FRy"].axisAction();

                        GUIData.Axis["FRz"].AxisValue = state.FRz;
                        GUIData.Axis["FRz"].axisAction();

                        GUIData.Axis["FX"].AxisValue = state.FX;
                        GUIData.Axis["FX"].axisAction();

                        GUIData.Axis["FY"].AxisValue = state.FY;
                        GUIData.Axis["FY"].axisAction();

                        GUIData.Axis["FZ"].AxisValue = state.FZ;
                        GUIData.Axis["FZ"].axisAction();

                        GUIData.Axis["Rx"].AxisValue = state.Rx;
                        GUIData.Axis["Rx"].axisAction();

                        GUIData.Axis["Ry"].AxisValue = state.Ry;
                        GUIData.Axis["Ry"].axisAction();

                        GUIData.Axis["Rz"].AxisValue = state.Rz;
                        GUIData.Axis["Rz"].axisAction();

                        GUIData.Axis["VRx"].AxisValue = state.VRx;
                        GUIData.Axis["VRx"].axisAction();

                        GUIData.Axis["VRy"].AxisValue = state.VRy;
                        GUIData.Axis["VRy"].axisAction();

                        GUIData.Axis["VRz"].AxisValue = state.VRz;
                        GUIData.Axis["VRz"].axisAction();

                        GUIData.Axis["VX"].AxisValue = state.VX;
                        GUIData.Axis["VX"].axisAction();

                        GUIData.Axis["VY"].AxisValue = state.VY;
                        GUIData.Axis["VY"].axisAction();

                        GUIData.Axis["VZ"].AxisValue = state.VZ;
                        GUIData.Axis["VZ"].axisAction();

                        GUIData.Axis["X"].AxisValue = state.X;
                        GUIData.Axis["X"].axisAction();

                        GUIData.Axis["Y"].AxisValue = state.Y;
                        GUIData.Axis["Y"].axisAction();

                        GUIData.Axis["Z"].AxisValue = state.Z;
                        GUIData.Axis["Z"].axisAction();

                        //Capture Buttons.
                        byte[] buttons = state.GetButtons();
                        for (int i = 0; i < buttons.Length; i++)
                       {
                            if (buttons[i] != 0)
                            {
                                Console.Write("\r press");
                                GUIData.buttons[i].Pressed = true;
                                    GUIData.buttons[i].buttonAtion();
                            }
                            else
                            {
                                GUIData.buttons[i].Pressed = false;
                                GUIData.buttons[i].unbuttonAtion();
                            }

                        }
                    }
                    catch
                    {
                        //MessageBox.Show("ss");
                        //Application.Exit();
                        //stop();

                    }
                }
            }
        }
    
    }
}
