﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using JoystickPlugin.Enumeration;
using JoystickPlugin.Buttons;

namespace JoystickPlugin
{
    static class GUIData
    {
        public static Dictionary<string, JoystickAxis> Axis = new Dictionary<string, JoystickAxis>();
        public static Dictionary<int, JoystickButton> buttons = new Dictionary<int, JoystickButton>();

        public static void writeJoystick(string name)
        {
            using (XmlWriter writer = XmlWriter.Create(name+".xml"))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Joystick");

                foreach (KeyValuePair<int, JoystickButton> entry in GUIData.buttons.ToList())
                {
                    writer.WriteStartElement("Button");
                    
                    writer.WriteElementString("Id", entry.Key.ToString());
                    writer.WriteElementString("ButtonFunction", Convert.ToInt32(entry.Value.ButtonFunction)+"");
                    writer.WriteElementString("ButtonAction", entry.Value.ButtonAction.ToString());
                    writer.WriteElementString("ButtonType", entry.Value.ButtonType.ToString());
                    writer.WriteElementString("FieldId", entry.Value.FieldId.ToString());
                    writer.WriteElementString("PressedValue", entry.Value.PressedValue.ToString());
                    writer.WriteElementString("UnPressedValue", entry.Value.UnPressedValue.ToString());
                    writer.WriteElementString("Label", entry.Value.Label);
                    
                    writer.WriteEndElement();
                }

                foreach (KeyValuePair<string, JoystickAxis> entry in GUIData.Axis.ToList())
                {
                    writer.WriteStartElement("Axis");

                    writer.WriteElementString("Id", entry.Key);
                    writer.WriteElementString("PIC_Control", Convert.ToInt32(entry.Value.Pic_control)+"");
                    writer.WriteElementString("PRV_Control", Convert.ToInt32(entry.Value.Prv_control)+"");
                    writer.WriteElementString("FieldId", entry.Value.FieldId.ToString());
                    writer.WriteElementString("AxisMinimum", entry.Value.AxisMinimum.ToString());
                    writer.WriteElementString("AxisMaximum", entry.Value.AxisMaximum.ToString());
                    writer.WriteElementString("Label", entry.Value.Label);

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
          
        }

        public static void clean() {
            GUIData.buttons.Clear();
            GUIData.Axis.Clear();
        }

        public static void readJoystick(string name)
        {
            clean();
            XmlDocument doc = new XmlDocument();
            doc.Load(name + ".xml");
            
            XmlNodeList buttonList = doc.GetElementsByTagName("Button");

            foreach (XmlNode node in buttonList)
            {
                string ButtonFunctionstring = node.SelectSingleNode("ButtonFunction").InnerText;
                JoystickButton temp;

                switch ((ButtonFunction)Convert.ToInt32(ButtonFunctionstring))
                {
                    case ButtonFunction._ignore:
                        {
                        temp = new IgnoreJoystickButton();
                        temp.ButtonFunction = ButtonFunction._ignore;
                        break;
                        }
                    case ButtonFunction.FieldId:
                        {
                            temp = new FieldJoystickButton((int)ButtonFunction.FieldId);
                            temp.ButtonFunction = ButtonFunction.FieldId;
                            break;
                        }
                    case ButtonFunction.MP_Mode:
                        {
                            temp = new MP_ModeJoystickButton();
                            temp.ButtonFunction = ButtonFunction.MP_Mode;
                            break;
                        }
                    case ButtonFunction.Pattern:
                        {
                            temp = new PatternJoystickButton();
                            temp.ButtonFunction = ButtonFunction.Pattern;
                            break;
                        }
                    case ButtonFunction.Thread:
                        {
                            temp = new ThreadJoystickButton();
                            temp.ButtonFunction = ButtonFunction.Thread;
                            break;
                        }

                    default:{
                        temp = new IgnoreJoystickButton();
                        temp.ButtonFunction = ButtonFunction._ignore;
                        break;
                    }
                }

                temp.ButtonAction = Convert.ToInt32(node.SelectSingleNode("ButtonAction").InnerText);
                temp.ButtonType = Convert.ToInt32(node.SelectSingleNode("ButtonType").InnerText);
                temp.FieldId = Convert.ToInt32(node.SelectSingleNode("FieldId").InnerText);
                temp.PressedValue = Convert.ToInt32(node.SelectSingleNode("PressedValue").InnerText);
                temp.UnPressedValue = Convert.ToInt32(node.SelectSingleNode("UnPressedValue").InnerText);
                temp.Label = node.SelectSingleNode("Label").InnerText;
                
                int id = Convert.ToInt32(node.SelectSingleNode("Id").InnerText);
                GUIData.buttons.Add(id, temp);
            }


            XmlNodeList axisList = doc.GetElementsByTagName("Axis");

            foreach (XmlNode node in axisList)
            {
                JoystickAxis temp = new JoystickAxis();

                temp.Pic_control = (PIC_Control)Convert.ToInt32(node["PIC_Control"].InnerText);
                temp.Prv_control = (PRV_Control)Convert.ToInt32(node["PRV_Control"].InnerText);
                temp.FieldId = Convert.ToInt32(node.SelectSingleNode("FieldId").InnerText);
                temp.AxisMinimum = Convert.ToInt32(node.SelectSingleNode("AxisMinimum").InnerText);
                temp.AxisMaximum = Convert.ToInt32(node.SelectSingleNode("AxisMaximum").InnerText);
                temp.Label = node.SelectSingleNode("Label").InnerText;

                string id = node.SelectSingleNode("Id").InnerText;

                GUIData.Axis.Add(id, temp);
            }
        }


        public static void EmptyJoystick(string name)
        {
            using (XmlWriter writer = XmlWriter.Create(name + ".xml"))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Joystick");

                for (int i = 0; i < 128; i++)
                {
                    writer.WriteStartElement("Button");

                    writer.WriteElementString("Id", i.ToString());
                    writer.WriteElementString("ButtonFunction", Convert.ToInt32(ButtonFunction._ignore)+"");
                    writer.WriteElementString("ButtonAction", "0");
                    writer.WriteElementString("ButtonType", "0");
                    writer.WriteElementString("FieldId", "0");
                    writer.WriteElementString("PressedValue", "0");
                    writer.WriteElementString("UnPressedValue", "0");
                    writer.WriteElementString("Label", "button"+(i+1));

                    writer.WriteEndElement();
                }

                
                writeAxis(writer, "X");
                writeAxis(writer, "Y");
                writeAxis(writer, "Z");

                writeAxis(writer, "VX");
                writeAxis(writer, "VY");
                writeAxis(writer, "VZ");

                writeAxis(writer, "VRx");
                writeAxis(writer, "VRy");
                writeAxis(writer, "VRz");

                writeAxis(writer, "Rx");
                writeAxis(writer, "Ry");
                writeAxis(writer, "Rz");

                writeAxis(writer, "FX");
                writeAxis(writer, "FY");
                writeAxis(writer, "FZ");

                writeAxis(writer, "FRx");
                writeAxis(writer, "FRy");
                writeAxis(writer, "FRz");

                writeAxis(writer, "AX");
                writeAxis(writer, "AY");
                writeAxis(writer, "AZ");

                writeAxis(writer, "ARx");
                writeAxis(writer, "ARy");
                writeAxis(writer, "ARz");

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

        }

        private static void writeAxis(XmlWriter writer, string name)
        {
            writer.WriteStartElement("Axis");
            writer.WriteElementString("Id", name);

            writer.WriteElementString("PIC_Control", Convert.ToInt32(PIC_Control.None)+"");
            writer.WriteElementString("PRV_Control", Convert.ToInt32(PRV_Control.None) + "");
            writer.WriteElementString("FieldId", "0");
            writer.WriteElementString("AxisMinimum", "0");
            writer.WriteElementString("AxisMaximum", "65535");
            writer.WriteElementString("Label", name);
            writer.WriteEndElement();
        }

        public static void checkFieldId() { 
            
        }

    }
}
