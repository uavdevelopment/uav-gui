﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoystickPlugin.Enumeration
{
    public enum ButtonFunction
    {
        _ignore,
        Pattern,
        Thread,
        MP_Mode,
        FieldId
    }
}
