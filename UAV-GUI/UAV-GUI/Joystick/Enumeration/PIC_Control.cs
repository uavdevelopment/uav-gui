﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoystickPlugin.Enumeration
{
    public enum PIC_Control
    {
        None,
        Aileron,
        Elevator,
        Throttle,
        Rudder
    }
}
