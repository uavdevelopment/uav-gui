﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoystickPlugin.Enumeration
{
    public enum MP_Mode
    {
        UAV_PIC,
        UAV_RPV,
        UAV_UAV
    }
}
