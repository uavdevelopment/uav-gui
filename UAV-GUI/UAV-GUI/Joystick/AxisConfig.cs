﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;


namespace JoystickPlugin
{
    public partial class axisConfig : Form
    {
        string axisName;
        string joystickname;
        public CheckBox Flag;

        public string Joystickname
        {
            get { return joystickname; }
            set { joystickname = value; }
        }

        public string AxisName
        {
            get { return axisName; }
            set { axisName = value; }
        }

        public axisConfig()
        {
            InitializeComponent();
        }

        private void axisConfig_Load(object sender, EventArgs e)
        {
            inti();
            this.PIC_Control.SelectedIndex = PIC_Control.Items.IndexOf(GUIData.Axis[axisName].Pic_control);
            this.PRV_Control.SelectedIndex = PRV_Control.Items.IndexOf(GUIData.Axis[axisName].Prv_control);
            this.filedId.Text = "" + GUIData.Axis[axisName].FieldId;
            this.minValue.Text = "" + GUIData.Axis[axisName].AxisMinimum;
            this.maxValue.Text = "" + GUIData.Axis[axisName].AxisMaximum;
            this.buttonLable.Text = GUIData.Axis[axisName].Label;
        }

        private void ok_Click(object sender, EventArgs e)
        {
            GUIData.Axis[axisName].Pic_control = (Enumeration.PIC_Control)this.PIC_Control.SelectedItem;
            GUIData.Axis[axisName].Prv_control = (Enumeration.PRV_Control)this.PRV_Control.SelectedItem;
            GUIData.Axis[axisName].FieldId = Int32.Parse(this.filedId.Text);
            GUIData.Axis[axisName].AxisMinimum = Int32.Parse(this.minValue.Text);
            GUIData.Axis[axisName].AxisMaximum = Int32.Parse(this.maxValue.Text);
            GUIData.Axis[axisName].Label = this.buttonLable.Text;
            GUIData.writeJoystick(joystickname);
            JoystickRunner.stop();
            GUIData.readJoystick(joystickname);
            JoystickRunner.start();
            Flag.Checked = false;
            this.Close();
        }

        private void cancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

        private void PIC_Control_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void inti()
        {
            this.PIC_Control.Items.Add(Enumeration.PIC_Control.None);
            this.PIC_Control.Items.Add(Enumeration.PIC_Control.Aileron);
            this.PIC_Control.Items.Add(Enumeration.PIC_Control.Elevator);
            this.PIC_Control.Items.Add(Enumeration.PIC_Control.Throttle);
            this.PIC_Control.Items.Add(Enumeration.PIC_Control.Rudder);

            PIC_Control.SelectedIndex = PIC_Control.Items.IndexOf(Enumeration.PIC_Control.None);

            this.PRV_Control.Items.Add(Enumeration.PRV_Control.None);
            this.PRV_Control.Items.Add(Enumeration.PRV_Control.Heading);
            this.PRV_Control.Items.Add(Enumeration.PRV_Control.Altitude);
            this.PRV_Control.Items.Add(Enumeration.PRV_Control.AirSpeed);


            PRV_Control.SelectedIndex = PRV_Control.Items.IndexOf(Enumeration.PRV_Control.None);
        }
    }
}
