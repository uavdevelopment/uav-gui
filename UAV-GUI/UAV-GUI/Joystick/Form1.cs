﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Microsoft.DirectX.DirectInput;
using System.IO;
using UAV_GUI;
using GLobal;

namespace JoystickPlugin
{
    public partial class Form1 : Form
    {
        Thread updateGui = null;
        Thread connectMonitor = null;

        string joystickname;

        public Form1()
        {
            InitializeComponent();
            ApplicationLookAndFeel.UseTheme(this);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DeviceList joystickarray = joystick.populate_Joysticks();

            if (joystickarray.Count == 0)
            {
                MessageBox.Show("there is no joystick connected please connect one and reopen app");
                this.Close();
            }

            foreach (DeviceInstance di in joystickarray)
            {
                joystickList.Items.Add(di.InstanceName + di.InstanceGuid);
                if (!File.Exists(di.InstanceName + di.InstanceGuid + ".xml")) {
                    GUIData.EmptyJoystick(di.InstanceName + di.InstanceGuid);
                }                
            }

            JoystickRunner.start();
            updateGui_Start();
        }


        private void updateGuiValues()
        {
            while (true)
            {
                if (joystick.joy != null && !FlagBox.Checked)
                {
                    set_Slider(X, CX, LX, "X");
                    set_Slider(Y, CY, LY, "Y");
                    set_Slider(Z, CZ, LZ, "Z");

                    set_Slider(Rx, CRx, LRx, "Rx");
                    set_Slider(Ry, CRy, LRy, "Ry");
                    set_Slider(Rz, CRz, LRz, "Rz");

                    set_Slider(AX, CAX, LAX, "AX");
                    set_Slider(AY, CAY, LAY, "AY");
                    set_Slider(AZ, CAZ, LAZ, "AZ");

                    set_Slider(ARx, CARx, LARx, "ARx");
                    set_Slider(ARy, CARy, LARy, "ARy");
                    set_Slider(ARz, CARz, LARz, "ARz");

                    set_Slider(FX, CFX, LFX, "FX");
                    set_Slider(FY, CFY, LFY, "FY");
                    set_Slider(FZ, CFZ, LFZ, "FZ");

                    set_Slider(FRx, CFRx, LFRx, "FRx");
                    set_Slider(FRy, CFRy, LFRy, "FRy");
                    set_Slider(FRz, CFRz, LFRz, "FRz");

                    set_Slider(VX, CVX, LVX, "VX");
                    set_Slider(VY, CVY, LVY, "VY");
                    set_Slider(VZ, CVZ, LVZ, "VZ");

                    set_Slider(VRx, CVRx, LVRx, "VRx");
                    set_Slider(VRy, CVRy, LVRy, "VRy");
                    set_Slider(VRz, CVRz, LVRz, "VRz");

                    int i = 0;

                    Button1.Checked = GUIData.buttons[i++].Pressed;
                    Button2.Checked = GUIData.buttons[i++].Pressed;
                    Button3.Checked = GUIData.buttons[i++].Pressed;
                    Button4.Checked = GUIData.buttons[i++].Pressed;
                    Button5.Checked = GUIData.buttons[i++].Pressed;
                    Button6.Checked = GUIData.buttons[i++].Pressed;
                    Button7.Checked = GUIData.buttons[i++].Pressed;
                    Button8.Checked = GUIData.buttons[i++].Pressed;
                    Button9.Checked = GUIData.buttons[i++].Pressed;
                    Button10.Checked = GUIData.buttons[i++].Pressed;

                    Button11.Checked = GUIData.buttons[i++].Pressed;
                    Button12.Checked = GUIData.buttons[i++].Pressed;
                    Button13.Checked = GUIData.buttons[i++].Pressed;
                    Button14.Checked = GUIData.buttons[i++].Pressed;
                    Button15.Checked = GUIData.buttons[i++].Pressed;
                    Button16.Checked = GUIData.buttons[i++].Pressed;
                    Button17.Checked = GUIData.buttons[i++].Pressed;
                    Button18.Checked = GUIData.buttons[i++].Pressed;
                    Button19.Checked = GUIData.buttons[i++].Pressed;
                    Button20.Checked = GUIData.buttons[i++].Pressed;

                    Button21.Checked = GUIData.buttons[i++].Pressed;
                    Button22.Checked = GUIData.buttons[i++].Pressed;
                    Button23.Checked = GUIData.buttons[i++].Pressed;
                    Button24.Checked = GUIData.buttons[i++].Pressed;
                    Button25.Checked = GUIData.buttons[i++].Pressed;
                    Button26.Checked = GUIData.buttons[i++].Pressed;
                    Button27.Checked = GUIData.buttons[i++].Pressed;
                    Button28.Checked = GUIData.buttons[i++].Pressed;
                    Button29.Checked = GUIData.buttons[i++].Pressed;
                    Button30.Checked = GUIData.buttons[i++].Pressed;


                    Button31.Checked = GUIData.buttons[i++].Pressed;
                    Button32.Checked = GUIData.buttons[i++].Pressed;
                    Button33.Checked = GUIData.buttons[i++].Pressed;
                    Button34.Checked = GUIData.buttons[i++].Pressed;
                    Button35.Checked = GUIData.buttons[i++].Pressed;
                    Button36.Checked = GUIData.buttons[i++].Pressed;
                    Button37.Checked = GUIData.buttons[i++].Pressed;
                    Button38.Checked = GUIData.buttons[i++].Pressed;
                    Button39.Checked = GUIData.buttons[i++].Pressed;
                    Button40.Checked = GUIData.buttons[i++].Pressed;

                    Button41.Checked = GUIData.buttons[i++].Pressed;
                    Button42.Checked = GUIData.buttons[i++].Pressed;
                    Button43.Checked = GUIData.buttons[i++].Pressed;
                    Button44.Checked = GUIData.buttons[i++].Pressed;
                    Button45.Checked = GUIData.buttons[i++].Pressed;
                    Button46.Checked = GUIData.buttons[i++].Pressed;
                    Button47.Checked = GUIData.buttons[i++].Pressed;
                    Button48.Checked = GUIData.buttons[i++].Pressed;
                    Button49.Checked = GUIData.buttons[i++].Pressed;
                    Button50.Checked = GUIData.buttons[i++].Pressed;

                    Button51.Checked = GUIData.buttons[i++].Pressed;
                    Button52.Checked = GUIData.buttons[i++].Pressed;
                    Button53.Checked = GUIData.buttons[i++].Pressed;
                    Button54.Checked = GUIData.buttons[i++].Pressed;
                    Button55.Checked = GUIData.buttons[i++].Pressed;
                    Button56.Checked = GUIData.buttons[i++].Pressed;
                    Button57.Checked = GUIData.buttons[i++].Pressed;
                    Button58.Checked = GUIData.buttons[i++].Pressed;
                    Button59.Checked = GUIData.buttons[i++].Pressed;
                    Button60.Checked = GUIData.buttons[i++].Pressed;

                    Button61.Checked = GUIData.buttons[i++].Pressed;
                    Button62.Checked = GUIData.buttons[i++].Pressed;
                    Button63.Checked = GUIData.buttons[i++].Pressed;
                    Button64.Checked = GUIData.buttons[i++].Pressed;
                    Button65.Checked = GUIData.buttons[i++].Pressed;
                    Button66.Checked = GUIData.buttons[i++].Pressed;
                    Button67.Checked = GUIData.buttons[i++].Pressed;
                    Button68.Checked = GUIData.buttons[i++].Pressed;
                    Button69.Checked = GUIData.buttons[i++].Pressed;
                    Button70.Checked = GUIData.buttons[i++].Pressed;

                    Button71.Checked = GUIData.buttons[i++].Pressed;
                    Button72.Checked = GUIData.buttons[i++].Pressed;
                    Button73.Checked = GUIData.buttons[i++].Pressed;
                    Button74.Checked = GUIData.buttons[i++].Pressed;
                    Button75.Checked = GUIData.buttons[i++].Pressed;
                    Button76.Checked = GUIData.buttons[i++].Pressed;
                    Button77.Checked = GUIData.buttons[i++].Pressed;
                    Button78.Checked = GUIData.buttons[i++].Pressed;
                    Button79.Checked = GUIData.buttons[i++].Pressed;
                    Button80.Checked = GUIData.buttons[i++].Pressed;

                    Button81.Checked = GUIData.buttons[i++].Pressed;
                    Button82.Checked = GUIData.buttons[i++].Pressed;
                    Button83.Checked = GUIData.buttons[i++].Pressed;
                    Button84.Checked = GUIData.buttons[i++].Pressed;
                    Button85.Checked = GUIData.buttons[i++].Pressed;
                    Button86.Checked = GUIData.buttons[i++].Pressed;
                    Button87.Checked = GUIData.buttons[i++].Pressed;
                    Button88.Checked = GUIData.buttons[i++].Pressed;
                    Button89.Checked = GUIData.buttons[i++].Pressed;
                    Button90.Checked = GUIData.buttons[i++].Pressed;

                    Button91.Checked = GUIData.buttons[i++].Pressed;
                    Button92.Checked = GUIData.buttons[i++].Pressed;
                    Button93.Checked = GUIData.buttons[i++].Pressed;
                    Button94.Checked = GUIData.buttons[i++].Pressed;
                    Button95.Checked = GUIData.buttons[i++].Pressed;
                    Button96.Checked = GUIData.buttons[i++].Pressed;
                    Button97.Checked = GUIData.buttons[i++].Pressed;
                    Button98.Checked = GUIData.buttons[i++].Pressed;
                    Button99.Checked = GUIData.buttons[i++].Pressed;
                    Button100.Checked = GUIData.buttons[i++].Pressed;

                    Button101.Checked = GUIData.buttons[i++].Pressed;
                    Button102.Checked = GUIData.buttons[i++].Pressed;
                    Button103.Checked = GUIData.buttons[i++].Pressed;
                    Button104.Checked = GUIData.buttons[i++].Pressed;
                    Button105.Checked = GUIData.buttons[i++].Pressed;
                    Button106.Checked = GUIData.buttons[i++].Pressed;
                    Button107.Checked = GUIData.buttons[i++].Pressed;
                    Button108.Checked = GUIData.buttons[i++].Pressed;
                    Button109.Checked = GUIData.buttons[i++].Pressed;
                    Button110.Checked = GUIData.buttons[i++].Pressed;

                    Button111.Checked = GUIData.buttons[i++].Pressed;
                    Button112.Checked = GUIData.buttons[i++].Pressed;
                    Button113.Checked = GUIData.buttons[i++].Pressed;
                    Button114.Checked = GUIData.buttons[i++].Pressed;
                    Button115.Checked = GUIData.buttons[i++].Pressed;
                    Button116.Checked = GUIData.buttons[i++].Pressed;
                    Button117.Checked = GUIData.buttons[i++].Pressed;
                    Button118.Checked = GUIData.buttons[i++].Pressed;
                    Button119.Checked = GUIData.buttons[i++].Pressed;
                    Button120.Checked = GUIData.buttons[i++].Pressed;

                    Button121.Checked = GUIData.buttons[i++].Pressed;
                    Button122.Checked = GUIData.buttons[i++].Pressed;
                    Button123.Checked = GUIData.buttons[i++].Pressed;
                    Button124.Checked = GUIData.buttons[i++].Pressed;
                    Button125.Checked = GUIData.buttons[i++].Pressed;
                    Button126.Checked = GUIData.buttons[i++].Pressed;
                    Button127.Checked = GUIData.buttons[i++].Pressed;
                    Button128.Checked = GUIData.buttons[i++].Pressed;

                }
            }
        }


        private void connectionMonitor()
        {
            while (true)
            {
                if (joystick.joy != null)
                {
                    Thread.Sleep(100);

                    fieldList.Items.Clear();

                    if (ManualControlData.mode != null)
                    {
                        fieldList.Items.Add("CurrentMode = " + ManualControlData.mode.ToString());
                    }

                    if (ManualControlData.elevator != null)
                    {
                        fieldList.Items.Add("Elevator =  " + ManualControlData.elevator);
                    }

                    if (ManualControlData.throttle != null)
                    {
                        fieldList.Items.Add("Throttle = " + ManualControlData.throttle);
                    }

                    if (ManualControlData.aileron != null)
                    {
                        fieldList.Items.Add("Aileron: " + ManualControlData.aileron);
                    }

                    if (ManualControlData.rudder != null)
                    {
                        fieldList.Items.Add("Rudder: " + ManualControlData.rudder);
                    }

                    if (ManualControlData.heading != null)
                    {
                        fieldList.Items.Add("Heading: " + ManualControlData.heading);
                    }

                    if (ManualControlData.altitude != null)
                    {
                        fieldList.Items.Add("Altitude: " + ManualControlData.altitude);
                    }

                    if (ManualControlData.airSpeed != null)
                    {
                        fieldList.Items.Add("AirSpeed: " + ManualControlData.airSpeed);
                    }

                    foreach (KeyValuePair<int, int> entry in ManualControlData.fieldList.ToList())
                    {
                        fieldList.Items.Add("Field Id: " + entry.Key + "   Value: " + entry.Value);
                    }

                    fieldList.Items.Add("Current Patten:" + ManualControlData.currentPattern);

                    fieldList.Items.Add("Current Thread:" + ManualControlData.currentThread);
                    
                }
            }
        }

        private void set_Slider(TrackBar track_bar, TextBox text_box, Label label, string key)
        {
            try
            {
                track_bar.Minimum = GUIData.Axis[key].AxisMinimum;
                track_bar.Maximum = GUIData.Axis[key].AxisMaximum;
                track_bar.Value = GUIData.Axis[key].AxisValue;
                text_box.Text = GUIData.Axis[key].AxisValue + "";
                label.Text = GUIData.Axis[key].Label;
            }
            catch {
                GlobalData.LogConsole("2shta y3ny");
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            JoystickConnectionRunner.stop();
            JoystickRunner.stop();
            connectMonitor_Stop();
            updateGui_Stop();
        }

        private void joystickList_SelectedValueChanged(object sender, EventArgs e)
        {
            tabControl.Enabled = true;
            JoystickRunner.stop();
            GUIData.clean();
            GUIData.readJoystick(joystickList.Text);
            joystick.select_Joystick(joystickList.Text, IntPtr.Zero);
            joystickname = joystickList.Text;
            JoystickRunner.start();
        }

        private void LFX_Click(object sender, EventArgs e)
        {
            Label temp = sender as Label;
            axisConfig axisConfigformconfig = new axisConfig();
            axisConfigformconfig.Flag = FlagBox;
            FlagBox.Checked = true;
            axisConfigformconfig.AxisName = temp.Name.Substring(1);
            axisConfigformconfig.Joystickname = this.joystickname;
            axisConfigformconfig.Show();
        }

        private void Button128_Click(object sender, EventArgs e)
        {
            RadioButton temp = sender as RadioButton;
            int index = 0;
            int.TryParse(temp.Name.Substring(6), out index);
            buttonConfig buttonConfigform = new buttonConfig();
            buttonConfigform.Flag = FlagBox;
            FlagBox.Checked = true;
            buttonConfigform.Index = index;
            buttonConfigform.Joystickname = this.joystickname;
            buttonConfigform.Show();
        }

        private void connect_Click(object sender, EventArgs e)
        {
            foreach (Control ctl in tabControl.Controls)
            {
                if (ctl.TabIndex < 2)
                {
                    ctl.Enabled = false;
                }
            }
            joystickList.Enabled = false;
            this.connect.Enabled = false;
            this.Disconnect.Enabled = true;
            JoystickConnectionRunner.start();
            updateGui_Stop();
            connectMonitor_Start();
            GlobalData.JoyStickMode = true;
        }

        private void Disconnect_Click(object sender, EventArgs e)
        {
            GlobalData.JoyStickMode = false;
            joystickList.Enabled = true;
            this.connect.Enabled = true;
            this.Disconnect.Enabled = false;
            JoystickConnectionRunner.stop();
            connectMonitor_Stop();
            fieldList.Items.Clear();
            updateGui_Start();
            foreach (Control ctl in tabControl.Controls)
            {
                if (ctl.TabIndex < 2){
                    ctl.Enabled = true;
                }
            }
        }

        private void updateGui_Stop(){
            if (updateGui != null)
            {
                updateGui.Abort();
                updateGui = null;
            }
        }

        private void updateGui_Start()
        {
            updateGui = new Thread(new ThreadStart(updateGuiValues));
            updateGui.Start();
        }

        private void connectMonitor_Start(){
            connectMonitor = new Thread(new ThreadStart(connectionMonitor));
            connectMonitor.Start();
        }

        private void connectMonitor_Stop(){
            if (connectMonitor != null)
            {
                connectMonitor.Abort();
                connectMonitor = null;
            }
        }
     
    }
}
