﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.DirectX.DirectInput;
using System.Windows.Forms;
using JoystickPlugin.Enumeration;

namespace JoystickPlugin
{
    class JoystickRunner
    {
        static Thread joystickViewThread = null;
        static ManualResetEvent stopThread = new ManualResetEvent(false);

        public static void start()
        {
            joystickViewThread = new Thread(new ThreadStart(ThreadProcSafe));
            stopThread.Reset();
            joystickViewThread.Start();
        }

        public static void stop()
        {
            if (joystickViewThread != null)
            {
                joystickViewThread.Abort();
                joystickViewThread = null;
            }
        }

        public static void suspend()
        {
            stopThread.Set();
        }

        public static void resume()
        {
            stopThread.Reset();
        }
    
        private static void ThreadProcSafe()
        {
            while (true)
            {
                while (stopThread.WaitOne(0))
                {

                }
                
                if (joystick.joy != null)
                {
                    try
                    {
                        JoystickState state = joystick.joy.CurrentJoystickState;
                        
                        GUIData.Axis["ARx"].AxisValue = state.ARx;                      
                        GUIData.Axis["ARy"].AxisValue = state.ARy;
                        GUIData.Axis["ARz"].AxisValue = state.ARz;
                        GUIData.Axis["AX"].AxisValue = state.AX;
                        GUIData.Axis["AY"].AxisValue = state.AY;
                        GUIData.Axis["AZ"].AxisValue = state.AZ;
                        GUIData.Axis["FRx"].AxisValue = state.FRx;                                             
                        GUIData.Axis["FRy"].AxisValue = state.FRy;                                                                                          
                        GUIData.Axis["FRz"].AxisValue = state.FRz;                                                
                        GUIData.Axis["FX"].AxisValue = state.FX;                                                
                        GUIData.Axis["FY"].AxisValue = state.FY;
                        GUIData.Axis["FZ"].AxisValue = state.FZ;                                               
                        GUIData.Axis["Rx"].AxisValue = state.Rx;
                        GUIData.Axis["Ry"].AxisValue = state.Ry;                        
                        GUIData.Axis["Rz"].AxisValue = state.Rz;
                        GUIData.Axis["VRx"].AxisValue = state.VRx;
                        GUIData.Axis["VRy"].AxisValue = state.VRy;
                        GUIData.Axis["VRz"].AxisValue = state.VRz;
                        GUIData.Axis["VX"].AxisValue = state.VX;
                        GUIData.Axis["VY"].AxisValue = state.VY;
                        GUIData.Axis["VZ"].AxisValue = state.VZ;
                        GUIData.Axis["X"].AxisValue = state.X;                        
                        GUIData.Axis["Y"].AxisValue = state.Y;
                        GUIData.Axis["Z"].AxisValue = state.Z;
                        
                        //Capture Buttons.
                        byte[] buttons = state.GetButtons();
                        for (int i = 0; i < buttons.Length; i++)
                        {
                            if (buttons[i] != 0)
                            {
                                GUIData.buttons[i].Pressed = true;                               
                            }
                            else
                            {
                                GUIData.buttons[i].Pressed = false;
                            }
                            
                        }
                    }
                    catch
                    {
                        //MessageBox.Show("ss");
                        //Application.Exit();
                        //stop();
                    }
                }
            }
        }
    
    }

}
