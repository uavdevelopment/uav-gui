﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using MasterClasses;
using MP_Lib;

namespace UAV_GUI.FlightClasses
{
   public class FlightData : INotifyPropertyChanged
    {
        public MPSTDTELEMETRYDATA pStdTlmData = new MPSTDTELEMETRYDATA();

        float throttleValue;

        public float ThrottleValue
        {
            get { return throttleValue; }
            set { throttleValue = value; }
        }
        float sBatValue;

        public float SBatValue
        {
            get { return sBatValue; }
            set { sBatValue = value; }
        }
        float mBatValue;

        public float MBatValue
        {
            get { return mBatValue; }
            set { mBatValue = value; }
        } 
        int joyValue;

        public int JoyValue
        {
            get { return joyValue; }
            set { joyValue = value; }
        } 
        List<int> fcodes = new List<int>();

        public List<int> FCodes
        {
            get { return fcodes; }
            set { fcodes = value; }
        }

        List<int> wcodes = new List<int>();

        public List<int> WCodes
        {
            get { return wcodes; }
            set { wcodes = value; }
        }
        /// <summary>
        /// Check if GPS Locked ornot
        /// </summary>
        private bool gpsLock = false;

        public bool GpsLock
        {
            get { return gpsLock; }
            set
            {
                if (value != GpsLock)
                {
                    gpsLock = value;
                    //Whenever property value is changes
                    //PropertyChanged event is triggered
                    OnPropertyChanged("gpsLock");
                }
            }
        } 

        /// <summary>
        /// Current Angular CVelocity of the airbone 
        /// </summary>
        private double angularVelocity = 0;

        public double AngularVelocity
        {
            get { return angularVelocity; }
            set
            {
                if (value != AngularVelocity)
                {
                    angularVelocity = value;
                    //Whenever property value is changes
                    //PropertyChanged event is triggered
                    OnPropertyChanged("AngularVelocity");
                }
            }
        } 

        /* Speed */
        private float speed = 0;
        public float Speed
        {
            get { return speed; }
            set
            {
                if (value != Speed)
                {
                    speed = value;
                    //Whenever property value is changes
                    //PropertyChanged event is triggered
                    OnPropertyChanged("Speed");
                }
            }
        }


        

       
        /// <summary>
        /// Current Attitude of the airbone 
        /// </summary>
        /// 

        /* Roll  */
        private float roll = 0;
        public float Roll
        {
            get { return roll; }
            set
            {
                if (value != Roll)
                {
                    roll = value;
                    //Whenever property value is changes
                    //PropertyChanged event is triggered
                    OnPropertyChanged("Roll");
                }
            }
        }

        /* Pitch */
        private float pitch = 0;
        public float Pitch
        {
            get { return pitch; }
            set
            {
                if (value != Pitch)
                {
                    pitch = value;
                    //Whenever property value is changes
                    //PropertyChanged event is triggered
                    OnPropertyChanged("Pitch");
                }
            }
        }


        /* YAW */
        private float yaw = 0;
        public float Yaw
        {
            get { return yaw; }
            set
            {
                if (value != Yaw)
                {
                    yaw = value;
                    //Whenever property value is changes
                    //PropertyChanged event is triggered
                    OnPropertyChanged("Yaw");
                }
            }
        }

        /* Heading */
        private float heading = 0;
        public float Heading
        {
            get { return heading; }
            set
            {
                if (value != Heading)
                {
                    heading = value;
                    //Whenever property value is changes
                    //PropertyChanged event is triggered
                    OnPropertyChanged("Heading");
                }
            }
        }


        
       
        /// <summary>
        /// Miscellaneous attributes
        /// </summary>
        ///  /* Distance to next wp */
        private float dTNWP = 0;
        public float DTNWP
        {
            get { return dTNWP; }
            set
            {
                if (value != dTNWP)
                {
                    dTNWP = value;
                    //Whenever property value is changes
                    //PropertyChanged event is triggered
                    OnPropertyChanged("DTNWP");
                }
            }
        }

        /*Air Speed */
        private float airSpeed = 0;

        public float AirSpeed
        {
            get { return airSpeed; }
            set
            {
                if (value != AirSpeed)
                {
                    airSpeed = value;
                    OnPropertyChanged("AirSpeed");
                }
            }
        }

        
        /// <summary>
        /// Current Location Of The Plane 
        /// </summary>
        /// /*Altitude */
        private float altitude = 0;
        public float Altitude
        {
            get { return altitude; }
            set
            {
                if (value != Altitude)
                {
                    altitude = value;
                    OnPropertyChanged("Altitude");
                }
            }
        }

        /// /*Latitude */
        private double latitude = 0;
        public double Latitude
        {
            get { return latitude; }
            set
            {
                if (value != Latitude)
                {
                    latitude = value;
                    OnPropertyChanged("Latitude");
                }
            }
        }

        /// /*Longitude */
        private double longitude = 0;
        public double Longitude
        {
            get { return longitude; }
            set
            {
                if (value != Longitude)
                {
                    longitude = value;
                    OnPropertyChanged("Longitude");
                }
            }
        }


        /*Bank Angle */
        private float bankAngle = 0;
        public float BankAngle
        {
            get { return bankAngle; }
            set
            {
                if (value != BankAngle)
                {
                    bankAngle = value;
                    OnPropertyChanged("BankAngle");
                }
            }
        }
        /* AGL Height */
        private int aglHeight = 0;
        public int AglHeight
        {
            get { return aglHeight; }
            set
            {
                if (value != AglHeight)
                {
                    aglHeight = value;
                    //Whenever property value is changes
                    //PropertyChanged event is triggered
                    OnPropertyChanged("AglHeight");
                }
            }
        }

      

        public GPSTime gpsTime = null;
        public Location Location = null;
        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        //Create OnPropertyChanged method to raise event
        protected void OnPropertyChanged(string PropertyName)
        {
            try
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
            catch (Exception e) { 
            
            }
        }

        #endregion
    }

}
