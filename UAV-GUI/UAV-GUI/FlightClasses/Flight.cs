﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Error_Reporting;
using MasterClasses;
using JoystickPlugin;
using FlightMonitor;
using JoystickPlugin.Enumeration;
using UAV_GUI.Utilities;
using MP_Lib;
using GLobal;
using GUtilities;
namespace UAV_GUI.FlightClasses
{
  public  class Flight
    {
        
        public FlightData flyData = new FlightData();
        
        public Thread dataAcquisitionThread;
        public Thread dataWriteThread;
        ManualResetEvent stopThreadAcq = new ManualResetEvent(false);
        ManualResetEvent stopThreadWrite = new ManualResetEvent(false);
        
        public void Start() {
            fatalErrorsLookUPTable.initialize();
            warningsLookUPTable.initialize();
            
            dataAcquisitionThread = new Thread(new ThreadStart(calculateData));
            stopThreadAcq.Reset();
            dataAcquisitionThread.Start();

            dataWriteThread = new Thread(new ThreadStart(writeData));
            stopThreadWrite.Reset();
            dataWriteThread.Start();
        }
       
        public  void Stop()
        {
            if (dataAcquisitionThread != null)
            {
                stopThreadAcq.Set();
                Thread.Sleep(10);
                dataAcquisitionThread.Abort();
                dataAcquisitionThread = null;
            }
            if (dataWriteThread != null)
            {
                stopThreadWrite.Set();
                Thread.Sleep(10);
                dataWriteThread.Abort();
                dataWriteThread = null;
            }
        }

        public void Suspend()
        {
            stopThreadWrite.Set();
            stopThreadAcq.Set();
            Thread.Sleep(10);
        }

        public  void Resume()
        {
            stopThreadWrite.Reset();
            stopThreadAcq.Reset();
        }

        private void writeData()
        {

            while (true)
            {
                try
                {
                    while (stopThreadWrite.WaitOne(0))
                    {

                    }
                    if (GlobalData.JoyStickMode)
                    {
                        /// Add field List. 

                        flyUAV.writeList(ManualControlData.fieldList);

                        ///For PIC Mode 
                        if (ManualControlData.mode == MP_Mode.UAV_PIC)
                        {
                            flyUAV.flyInPICMode((short)((ManualControlData.aileron == null) ? 0 : ManualControlData.aileron)
                            , (short)((ManualControlData.elevator == null) ? 0 : ManualControlData.elevator),
                              (short)((ManualControlData.rudder == null) ? 0 : ManualControlData.rudder)
                              , (short)((ManualControlData.throttle == null) ? 0 : ManualControlData.throttle));
                        }
                        ///For RPV Mode 
                        else if (ManualControlData.mode == MP_Mode.UAV_RPV)
                        {
                            flyUAV.flyInRPVMode((short)((ManualControlData.heading == null) ? 0 : ManualControlData.heading)
                                , (short)((ManualControlData.altitude == null) ? 0 : ManualControlData.altitude)
                                , (short)((ManualControlData.airSpeed == null) ? 0 : ManualControlData.airSpeed));
                        }

                        ///if There is an Active Pattern
                        if (ManualControlData.currentPattern != null)
                        {
                            flyUAV.startPattern((short)ManualControlData.currentPattern);
                        }



                    }
                }
                  
                catch (Exception e)
                {
                    GlobalData.LogConsole(e.Message + "at " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName() + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());
                    continue;
                }

            }
        }
        private void calculateData()
        {
            
            while (true)
            {
                try
                {
                while (stopThreadAcq.WaitOne(0))
                {

                }
                short pitch;
                short roll;
                int retVal;
                    
                    /*MTN State Data Calculation*/

                MTNState.latitude = AutoPilotUnits.compileGPSPositionFromAutoPilotUnits
                    (flyUAV.getJoy((int)Scratch_Fields.latitude));
                MTNState.longitude = AutoPilotUnits.compileGPSPositionFromAutoPilotUnits
                (flyUAV.getJoy((int)Scratch_Fields.longitude));
                MTNState.Altitude = (flyUAV.getJoy((int)Scratch_Fields.altitude)) / (3.28084);
                MTNState.rightpackets = flyUAV.getJoy((int)Scratch_Fields.RightPacketsCount);
                MTNState.Totalpackets = flyUAV.getJoy((int)Scratch_Fields.TotalPacketsCount);
                MTNState.RateTotalpackets = flyUAV.getJoy((int)Scratch_Fields.TotalRate);
                MTNState.Gps_State = flyUAV.getJoy((int) Scratch_Fields.GPSStatus);
                MTNState.NaV_State = (flyUAV.getJoy((int)Scratch_Fields.NAvigationMode) == 0 ? Navigation_Status.Alignment : Navigation_Status.Navigation);

               

                MTNState.VelocityE = AutoPilotUnits.toVelocity
            (flyUAV.getJoy((int)Scratch_Fields.VELE));

                MTNState.VelocityN = AutoPilotUnits.toVelocity
          (flyUAV.getJoy((int)Scratch_Fields.VELN));

                MTNState.yaw = AutoPilotUnits.toVelocity
          (flyUAV.getJoy((int)Scratch_Fields.VELU));

                MTNState.pitch = AutoPilotUnits.toOrientation
            (flyUAV.getJoy((int)Scratch_Fields.PITCH));

                MTNState.roll = AutoPilotUnits.toOrientation
            (flyUAV.getJoy((int)Scratch_Fields.ROLL));

                MTNState.yaw = AutoPilotUnits.toOrientation
            (flyUAV.getJoy((int)Scratch_Fields.AZIMUTH));

                flyData.pStdTlmData.cbSize = 148;
                retVal = flyUAV.getSTDTelemtry(ref flyData.pStdTlmData);
                /*Progress Bar  Data */

                flyData.ThrottleValue = (flyData.pStdTlmData.sTh*100f)/255f;
                flyData.MBatValue = (flyData.pStdTlmData.batV / 100f);
                flyData.SBatValue = (flyData.pStdTlmData.sbatV / 100f);

                /*Gauges  Data */

                flyData.Speed = flyUAV.getNetSpeed();
                flyData.AglHeight = (int)flyUAV.getAglHeight();
                /*Numeric Data*/

                /*Location Values */

                Location myLoc = flyUAV.getCurrentLocation();
                flyData.Altitude = flyUAV.getAltitude();
               flyData.Latitude = myLoc.Latitude;//flyUAV.getLatitude();
               flyData.Longitude = myLoc.Longitude;//flyUAV.getLongitude();/*
                /*flyData.Latitude = flyUAV.getOrigin().Latitude;

                flyData.Longitude = flyUAV.getOrigin().Longitude;*/
                /*flyData.Latitude =AutoPilotUnits.compileGPSPositionFromAutoPilotUnits(
                    flyData.pStdTlmData.n);
                
                flyData.Longitude = AutoPilotUnits.compileGPSPositionFromAutoPilotUnits(
                    flyData.pStdTlmData.e);

                flyData.Altitude = AutoPilotUnits.toAltitude(
                flyData.pStdTlmData.alt);*/
                    
                /*Attitude Values */
             /*   flyData.Roll = flyUAV.getRoll();
                flyData.Pitch = flyUAV.getPitch();
                flyData.Yaw = flyUAV.getYaw();
                flyData.Heading = flyUAV.getHeading();
                    */

                flyData.Roll = AutoPilotUnits.toOrientation(
                flyData.pStdTlmData.tlmRoll);
                flyData.Pitch = AutoPilotUnits.toOrientation(
                flyData.pStdTlmData.tlmPitch);
                flyData.Yaw = flyUAV.getYaw();
                flyData.Heading = flyUAV.getHeading();

                flyData.AirSpeed = flyUAV.getAirSpeed();
                //flyData.Location = flyUAV.getCurrentLocation();
                flyData.AngularVelocity = flyUAV.getAngularVelocity();
                flyData.DTNWP = flyUAV.getDistanceToNextWayPoint();
                int retValue = flyUAV.checkForFatalError();

                if (retValue == -1)
                {
                    throw new BusinessException(4, "Link to Autopilot was broken ");
                    return;
                }
                else if (retValue == 72)
                {
                    flyData.FCodes = flyUAV.getListofErrorCodes();
                    for (int i = 0; i < flyData.FCodes.Count; i++)
                    {
                        GlobalData.myLogger.WriteError(fatalErrorsLookUPTable.fatalErrors.ElementAt(flyData.FCodes[i] - 2));
                    }

                }
                else if (retValue != 0)
                {

                    flyData.FCodes.Add(retValue);
                    GlobalData.LogConsole("[Debugger : ] Error Code is " + retValue);
                    GlobalData.myLogger.WriteError(fatalErrorsLookUPTable.fatalErrors.ElementAt(retValue - 2));

                }
                flyData.WCodes = flyUAV.getListofWarningsCodes();
                if (flyData.WCodes == null)
                {
                    // throw new BusinessException(4, "Link Broen");
                    continue;
                }
                else
                {

                    for (int i = 0; i < flyData.WCodes.Count; i++)
                    {
                        GlobalData.myLogger.WriteWarning(warningsLookUPTable.warnings.ElementAt(flyData.WCodes[i] - 1));


                    }



                    //Thread.Sleep(300);
                }

                /*  foreach (KeyValuePair<int, int> entry in controller.fieldList.ToList())
                    {
                        flyUAV.setJoy(entry.Key , entry.Value);
                        GlobalData.LogConsole("Key is " + entry.Key + " Value is "+entry.Value);
                  
                    }*/
                //  flyUAV.setJoy(0,0);
                flyData.JoyValue = flyUAV.getJoy(1545);
                }
                 
                catch (Exception e){
                    GlobalData.LogConsole(e.Message + "at " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName() + " " + new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileLineNumber());
            continue;
            }
            }
        }
       
        public Flight() {
            //planFlight = new Plan();
            flyUAV = new UAV(1);
        }
        /*private Plan planFlight;

        internal Plan PlanFlight
        {
            get { return planFlight; }
            set { planFlight = value; }
        }*/
        public UAV flyUAV;
       private FlightMode FlyMode;

       
        public int initializeFly() {
            return 1; // initialized successfully . 
        }

        public void changeMode(FlightMode mode_) {
            // Add the Controls . 
            FlyMode = mode_; 
        }



       



    }
}
