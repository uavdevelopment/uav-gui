﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using GLobal;

namespace Error_Reporting
{
   public static class ErrorsReporting
    {
        public static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            BusinessException Ex = e.Exception as BusinessException;
            if (Ex == null)
            {
                Ex = new BusinessException();

                GlobalData.LogConsole(e.Exception.StackTrace + e.Exception.TargetSite);  

            }
                System.Windows.Forms.MessageBox.Show(Ex.getDescription(), Ex.getTitle(),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            
        }

    }
}
