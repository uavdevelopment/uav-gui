﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.Contracts;
namespace Error_Reporting
{
    public class BusinessException : Exception 
    {
        private  int code;
        private String description;
        
        public String getTitle() {

            return "Error (" + code + ") "; 

        }
        public String getDescription()
        {

            return getTitle () + ": "+ description;

        }
        public BusinessException(int id , String Message_): base(Message_) {
 
            /// Design By Contract requires Message not Empty and id greater than 1000

            Contract.Requires(id >= 1000,"Error Code must be greater than 1000");
            Contract.Requires(Message_ != null);
            
            code = id;
            description = Message_;
        
        }
        public BusinessException()
            
        {

            /// Design By Contract requires Message not Empty and id greater than 1000



            code = -1000;
            description = "Undefined error";

        }

    }
}
