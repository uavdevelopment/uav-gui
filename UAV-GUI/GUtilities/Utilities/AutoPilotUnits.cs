﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterClasses;
using GLobal; 


namespace GUtilities
{
    /// <summary>
    /// This Class is for Converting From and To Autopilot Units
    /// 
    /// </summary>
   public static class AutoPilotUnits
    {
        /// <summary>
        /// Can be used for all values of Pitch, yaw and roll 
        /// </summary>
        public static int toOrientation(int apValue)
        {
            int retValue = (int)Math.Round((apValue / 1024f) * 180f / 3.14f);
            return retValue;
        }

        public static int fromOrientation(int orValue)
        {
            int retValue = (int)Math.Round((orValue * 1024f) * 3.14f / 180f);
            return retValue;
        }
        /// <summary>
        /// Can be used to get the value of Heading
        /// </summary>
        /// <param name="apValue"></param>
        /// <returns></returns>
        public static float toHeading(int apValue)
        {
            float retValue = apValue / 100f;
            if (retValue > 360)
            {
                return 360f;
            }
            if (retValue < 0)
            {
                return 0f;
            }
            return retValue;
        }
        public static int fromHeading(int orValue)
        {
            int retValue = orValue * 100;
            if (retValue > 35999)
            {
                return 35999;
            }
            if (retValue < 0)
            {
                return 0;
            }
            return retValue;
        }

        /// <summary>
        /// Can be used for all values of Velocities
        /// </summary>
        public static float toVelocity(int apValue)
        {
            float retValue = (apValue * 1.09728f);
            return retValue;
        }

        public static int fromVelocity(float orValue)
        {
            int retValue = (int)Math.Round(orValue / 1.09728f);
            return retValue;
        }

        // <summary>
        /// Can be used for all values of Altitude
        /// </summary>
        public static float toAltitude(int apValue)
        {
            float toFeet = (apValue / -8f);
            float toMeter = (toFeet * 0.3048f);
            return toMeter;
        }

        public static int fromAltitude(float orValue)
        {
            float toFeet = (orValue * -8f);
            int toMeter = (int)Math.Round(toFeet / 0.3048f);
            return toMeter;
        }

        // <summary>
        /// Can be used for all values of Distance
        /// </summary>
        public static float toDistance(int apValue)
        {
            float toFeet = (apValue / 8f);
            float toMeter = (toFeet * 0.3048f);
            return toMeter;
        }

        public static int fromDistance(float orValue)
        {
            float toFeet = (orValue * 8f);
            int toMeter = (int)Math.Round(toFeet / 0.3048f);
            return toMeter;
        }

        /// <summary>
        /// Convert The GPS Position From MicroPilot Units to Standart Units 
        /// </summary>
        /// <param name="gpsValue"></param>
        /// <returns></returns>
        public static double compileGPSPositionFromAutoPilotUnits(int gpsValue)
        {

            double y = gpsValue / 5.0;

            double z = y / (100000000.0);

            double f = z * (180.0 / Math.PI);
            return f;

        }

        public static int uncompileGPSPositionFromAutoPilotUnits(double gpsValue)
        {

            double f = gpsValue / (180.0 / Math.PI);

            double z = f * (100000000.0);

            double y = z * 5.0;

            return (int)Math.Round(y);
        }

        // <summary>
        /// Can be used for all values of Longitude & Latitude
        /// </summary>
        public static Location toLocationEstimate(int orValueE_, int orValueN_, int relValueE_, int relValN_)
        {
            double orValueE = compileGPSPositionFromAutoPilotUnits(orValueE_);  //Long Degrees
            double orValueN = compileGPSPositionFromAutoPilotUnits(orValueN_);  //Lat degrees
            double orValueERad = (orValueE * Math.PI) / 180.0;
            double orValueNRad = (orValueN * Math.PI) / 180.0;
            double Esquared = (1 - Math.Pow(((6356752.3142) / (6378137.0)), 2.0));
            double xx = Math.Pow(orValueNRad, 2.0);
            double radN = 6378137.0 / Math.Sqrt(1 - (Esquared * (Math.Sin(xx))));
            double radE = radN * Math.Cos(orValueNRad);


            double relValE = toDistance(relValueE_);
            double relValN = toDistance(relValN_);

            double absVaErad = (relValE) / radE + orValueERad;
            double absVaNrad = (relValN) / radN + orValueNRad;

            MasterClasses.Location n = new MasterClasses.Location(((absVaNrad * 180.0) / Math.PI), ((absVaErad * 180.0) / Math.PI));


            return n;
        }

        public static Location toLocationEstimate(int relValueE_, int relValN_)
        {
            double orValueE = GlobalData.orValEDeg;  //Long Degrees
            double orValueN = GlobalData.orValNDeg;  //Lat degrees
            double orValueERad = GlobalData.orValERad;//(orValueE * Math.PI) / 180.0;
            double orValueNRad = GlobalData.orValNRad;//(orValueN * Math.PI) / 180.0;
            double Esquared = (1 - Math.Pow(((6356752.3142) / (6378137.0)), 2.0));
            double xx = Math.Pow(orValueNRad, 2.0);
            double radN = 6378137.0 / Math.Sqrt(1 - (Esquared * (Math.Sin(xx))));
            double radE = radN * Math.Cos(orValueNRad);


            double relValE = toDistance(relValueE_);
            double relValN = toDistance(relValN_);

            double absVaErad = (relValE) / radE + orValueERad;
            double absVaNrad = (relValN) / radN + orValueNRad;

            MasterClasses.Location n = new MasterClasses.Location(((absVaNrad * 180.0) / Math.PI), ((absVaErad * 180.0) / Math.PI));


            return n;
        }


        public static double toLongitude(int orValue_, int relValue_)
        {
            double orvalue = compileGPSPositionFromAutoPilotUnits(orValue_);

            double relValue = relValue_ / 8.0 / 234186.3592;
            double retValue = orvalue + relValue;
            return retValue;
        }
        public static double toLongitudeAbsoluteOR(double orValueE, double orValueN, int relValE)
        {
            double orValueERad = (orValueE * Math.PI) / 180.0;
            double orValueNRad = (orValueN * Math.PI) / 180.0;
            double Esquared = (1 - Math.Pow(((6356752.3142) / (6378137.0)), 2.0));
            double xx = Math.Pow(orValueNRad, 2.0);
            double radN = 6378137.0 / Math.Sqrt(1 - (Esquared * (Math.Sin(xx))));
            double radE = radN * Math.Cos(orValueNRad);

            double absVaErad = (relValE*0.3048) / radE + orValueERad;
            //double absVaNrad = (relValN) / radN + orValueNRad;

            return (double)((absVaErad * 180.0) / Math.PI);



        }

        public static double toLatitude(int orValue_, int relValue_)
        {
            double orvalue = compileGPSPositionFromAutoPilotUnits(orValue_);
            double relValue = relValue_ / 8.0 / 364829.408;
            double retValue = orvalue + relValue;
            return retValue;
        }
        public static double toLatitudeAbsoluteOR(double orValueE, double orValueN, int relValN)
        {
            double orValueERad = (orValueE * Math.PI) / 180.0;
            double orValueNRad = (orValueN * Math.PI) / 180.0;
            double Esquared = (1 - Math.Pow(((6356752.3142) / (6378137.0)), 2.0));
            double xx = Math.Pow(orValueNRad, 2.0);
            double radN = 6378137.0 / Math.Sqrt(1 - (Esquared * (Math.Sin(xx))));
            double radE = radN * Math.Cos(orValueNRad);

            //double absVaErad = (relValE) / radE + orValueERad;
            double absVaNrad = (relValN *0.3048) / radN + orValueNRad;

            return (double)((absVaNrad * 180.0) / Math.PI);
        }
        public static double fromLatitudeAbsoluteOR(double orValue_, int relValue_)
        {
            double orvalue = (orValue_);

            double relValue = relValue_ / 8.0 / 364829.408;
            double retValue = orvalue + relValue;
            return retValue;
        }

        // the function convert from user value to servo value 
        public static int toservo(int orValue)
        {
            int relvalue;
            relvalue = (int)((orValue * 32767) / 100);

            return relvalue;
        }


        // the function convert from Servo value to User Value 
        public static short fromservo(short orValue)
        {
            short relvalue;
            relvalue = (short)((orValue * 32767) / 100);
            return relvalue;
        }

        // the function convert from user value to servo value 
        public static int toCoarseServo(int orValue)
        {
            int relvalue;
            relvalue = (int)map(orValue, -100, 100, 1000, 5000);

            return relvalue;
        }


        // the function convert from Servo value to User Value 
        public static int fromCoarseServo(int orValue)
        {
            int relvalue;
            relvalue = (int)mapfromCoarseServo(orValue, -100, 100, 1000, 5000);
            return relvalue;

        }

        // the function convert from user value to servo value 
        public static int toCoarseServo(int orValue, int zero, int travel)
        {
            int relvalue;
            relvalue = (int)map(orValue, -100, 100, zero - travel, zero + travel);
            return relvalue;
        }


        // the function convert from Servo value to User Value 
        public static int fromCoarseServo(int orValue, int zero, int travel)
        {
            int relvalue;
            relvalue = (int)mapfromCoarseServo(orValue, -100, 100, zero - travel, zero + travel);
            return relvalue;

        }
        static int map(int x, int in_min, int in_max, int out_min, int out_max)
        {
            double va = (x - in_min) * (out_max - out_min);
            double val = (in_max - in_min);
            val = va / val + out_min;
            return (int)val;
        }

        static int mapfromCoarseServo(int x, int in_min, int in_max, int out_min, int out_max)
        {

            double slop = (double)(out_max - out_min) / (double)(in_max - in_min);
            double val = slop * (x - in_min);
            return (int)val;
        }

    }
}
