﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUtilities
{
    public  static class BitField
    {
        public static void set(int index, ref int num)
        {
            num = num | (1 << index);
            
        }


        public static void set(int index, ref double num_)
        {
            int num = (int)num_;
            num = num | (1 << index);
            num_ = num;

        }
        public static void clear(int index, ref int num)
        {
            num = num & ~(1 << index);
        }

        public static void clear(int index, ref double num_)
        {
            int num = (int)num_;
            num = num & ~(1 << index);
            num_ = num;
        }
        public static bool isSet(int index, int num)
        {
            int tmp = num;
            set(index, ref tmp);
            if (tmp == num)
                return true;
            return false;
        }

        //public static bool isClear(int index, int num)
        //{

        //    return false;
        //}
    }
}
