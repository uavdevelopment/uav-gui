﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUtilities
{

    public class WarningMessage 
    {
        private int code;
        private String description;

        public String getTitle()
        {

            return "Warning (" + code + ") ";

        }
        public String getDescription()
        {

            return getTitle() + ": " + description;

        }
        public WarningMessage(int id, String Message_)
          
        {

            code = id;
            description = Message_;

        }
        public WarningMessage()
        {

            code = -1000;
            description = "Undefined Warning";

        }

    }
    public static class WarningShower
    {
        static void showWarning(int Code,String message) {
            WarningMessage Ex = new WarningMessage(Code, message);
            System.Windows.Forms.MessageBox.Show(Ex.getDescription(), Ex.getTitle(),
                     MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            /*MessageBox.Show(
                    message, 
                "Warning", 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Asterisk //For Info Asterisk
                //  MessageBoxIcon.Exclamation //For triangle Warning 
);*/
        
        }
    }
}
