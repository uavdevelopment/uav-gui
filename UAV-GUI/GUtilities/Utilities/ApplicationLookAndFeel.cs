﻿using System.Windows.Forms;
using System.Drawing;


public  class ApplicationLookAndFeel
{
  static  ToolStripItem item;

    static void ApplyTheme(TextBox c)
    {

        c.Font = new Font("Arial", 10.0f, FontStyle.Bold); c.BackColor = Color.SteelBlue; c.ForeColor = Color.FromArgb(255,236,236,236);

    
        //LightSteelBlue
    }
    static void ApplyTheme(Label c)
    {
        if (c.Tag != null&& c.Tag.Equals("NotImplemented")) {
            return;
        }
        c.Font = new Font("Arial", 10.0f, FontStyle.Bold); c.BackColor = Color.Empty; c.ForeColor = Color.FromArgb(255, 236, 236, 236);
    
    }
    static void ApplyTheme(ComboBox c)
    {
   
        c.Font = new Font("Arial", 10.0f, FontStyle.Bold); c.BackColor = Color.FromArgb(255, 96, 96, 96); c.ForeColor = Color.FromArgb(255, 236, 236, 236);
 
    }
    public static void ApplyTheme(ScrewTurn.DropDownPanel c)
    {
        c.Font = new Font("Arial", 10.0f, FontStyle.Bold);  c.BackColor = Color.FromArgb(255, 96, 96, 96); c.ForeColor = Color.FromArgb(255, 236, 236, 236);
        foreach (Control cont in c.Controls)
        {
            ApplyAllThemes(cont);
        }
        
    }
    static void ApplyTheme(GroupBox c)
    {

        c.Font = new Font("Arial", 10.0f, FontStyle.Bold);
        c.BackgroundImage = Image.FromFile("imgs/formbackground.png");
        //c.BackColor = Color.FromArgb(255,96,96,96);
        c.ForeColor = Color.FromArgb(255, 236, 236, 236);

        foreach (Control cont in c.Controls)
        {
            ApplyAllThemes(cont);
        }

    }
         static void ApplyTheme(Panel c)
    {

        c.Font = new Font("Arial", 10.0f, FontStyle.Bold);
        c.BackColor = Color.FromArgb(44, 57, 84); 
        c.ForeColor = Color.FromArgb(255, 236, 236, 236);

        foreach (Control cont in c.Controls)
        {
            ApplyAllThemes(cont);
        }
       
    }
    static void ApplyTheme(Button c)
    {
        if (c.Name.Equals("altitudepicker") || c.Name.Equals("HeadingB") || c.Name.Equals("Airspeed")) return;
        c.Font = new Font("Arial", 10.0f, FontStyle.Bold);

        c.BackColor = Color.FromArgb(255,144,175,197); c.ForeColor = Color.FromArgb(255, 21, 32, 33);
        //CadetBlue
        c.MouseDown += new MouseEventHandler(changColor);
        c.MouseUp += new MouseEventHandler(changColor_Up);
        // c.Image =  System.Drawing.Image.FromFile( @"C:\\Users\\AbdALrahman\\Desktop\\upload-32.png");
        
    }
    static void ApplyTheme(CheckBox c)
    {
        c.Font = new Font("Arial", 10.0f, FontStyle.Bold);

        c.BackColor = Color.Empty; c.ForeColor = Color.FromArgb(255, 107, 163, 167);
       
    }
    static void ApplyTheme(DataGridView c)
    {
        c.Font = new Font("Arial", 10.0f, FontStyle.Bold);
        c.ForeColor = Color.Black;
        c.BackColor = Color.SteelBlue; c.ForeColor = Color.Black;

    }
    static void ApplyTheme(MenuStrip c)
    {
        c.Font = new Font("Arial", 10.0f, FontStyle.Bold);

        c.BackColor = Color.DimGray; c.ForeColor = Color.FromArgb(255, 236, 236, 236);
        if (c.Items.Count >= 1)
            for (int i = 0; i < c.Items.Count; i++)
            {
                c.Items[i].BackColor = Color.DimGray;
                c.ItemClicked += new ToolStripItemClickedEventHandler(menuItemClicked);
                c.Items[i].MouseEnter += new System.EventHandler(menuEnter);
                c.Items[i].MouseLeave += new System.EventHandler(menuLeave);
                //c.Items[0].MouseClick += new MouseEventHandler(menuClicked);
                //c.Items[i].MouseMove += new System.EventHandler(menuMouseMove);
            }
    }
    
     static void ApplyTheme(TabControl c)
    {
        c.Font = new Font("Arial", 10.0f, FontStyle.Bold);

        c.BackgroundImage = Image.FromFile("imgs/formbackground.png");
        c.ForeColor = Color.FromArgb(255, 236, 236, 236);
        for (int i=0;i<c.TabPages.Count;i++)
        c.TabPages[i].BackgroundImage = Image.FromFile("imgs/formbackground.png");
        foreach (Control cont in c.Controls)
        {
            ApplyAllThemes(cont);
        }
    }
     static void ApplyTheme(TabPage c)
     {
         c.Font = new Font("Arial", 10.0f, FontStyle.Bold);
         c.BackgroundImage = Image.FromFile("imgs/formbackground.png");
         c.BackColor = Color.FromArgb(0, 0, 68, 69); c.ForeColor = Color.FromArgb(255, 236, 236, 236);
        
         foreach (Control cont in c.Controls)
         {
             ApplyAllThemes(cont);
         }
     }
    static void menuItemClicked(object sendr, ToolStripItemClickedEventArgs arg)
    {
        MenuStrip c = sendr as MenuStrip;
        for (int i = 0; i < c.Items.Count; i++)
        {
            c.Items[i].ForeColor = Color.White;
            c.Items[i].BackColor = Color.DimGray;
        }

        arg.ClickedItem.ForeColor = Color.DimGray;
        arg.ClickedItem.BackColor = Color.FromArgb(255, 236, 236, 236);
        item = arg.ClickedItem;
    }

    static void menuLeave(object sendr, System.EventArgs arg)
    {
        ToolStripMenuItem c = sendr as ToolStripMenuItem;
        
            c.ForeColor = Color.FromArgb(255, 236, 236, 236);
            c.BackColor = Color.DimGray;
    }

    static void menuEnter(object sender,System.EventArgs arg)
    {
            ToolStripMenuItem c = sender as ToolStripMenuItem;
            c.ForeColor = Color.DimGray;
            c.BackColor = Color.FromArgb(255, 236, 236, 236);
    }

    static void menuClicked(object sender,System.EventArgs arg)
    {
        MenuStrip c = sender as MenuStrip;
        for (int i = 0; i < c.Items.Count; i++)
        {
            c.Items[i].ForeColor = Color.DimGray;
            c.Items[i].BackColor = Color.FromArgb(255, 236, 236, 236);
        }
    }


    static void menuMouseMove(object sender,System.EventArgs arg)
    {
        MenuStrip c = sender as MenuStrip;
        for (int i = 0; i < c.Items.Count; i++)
        {
            c.Items[i].ForeColor = Color.FromArgb(255, 236, 236, 236);
            c.Items[i].BackColor = Color.DimGray;
        }
    }

    static void changColor(object sendr, System.EventArgs arg)
    {

        Button c = sendr as Button;
        c.BackColor = Color.OliveDrab;
    }
    static void changColor_Up(object sendr, System.EventArgs arg)
    {
        Button c = sendr as Button;
        c.BackColor = Color.FromArgb(255, 144, 175, 197); c.ForeColor = Color.FromArgb(255, 21, 32, 33);
    }

    static void ApplyTheme(Form c)
    {
        //c.BackgroundImage = Image.FromFile("formbackground.jpg");
        c.BackColor = Color.FromArgb(255,2,28,30); c.ForeColor = Color.FromArgb(255, 236, 236, 236);
     //   c.FormBorderStyle = FormBorderStyle.SizableToolWindow;
    //    c.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
        //DarkSlateGray
        c.TransparencyKey = Color.LimeGreen;
        if(c.Name == "MicroPilotSetup")
        {
            c.Font = new Font("Arial", 7.0f, FontStyle.Regular);
        }else
        {
            c.Font = new Font("Arial", 10.0f, FontStyle.Bold);
        }

    }

  

   
    public static void UseTheme(Form form)
    {
       
        ApplyTheme(form);
        foreach (var c in form.Controls)
        {
            ApplyAllThemes(c);

        }

        if (form.Name.Equals("NewMainWindow"))
        {
            form.FormBorderStyle = FormBorderStyle.Sizable;
            form.WindowState = FormWindowState.Maximized;
           
        }
        else if (form.Name.Equals("Form2"))
        {
            form.FormBorderStyle = FormBorderStyle.Sizable;
            form.WindowState = FormWindowState.Maximized;

        }
        else {
            form.FormBorderStyle = FormBorderStyle.FixedToolWindow;

            form.MaximumSize = form.Size;
            form.MinimumSize = form.Size;
        }
        
        
    }

    static void ApplyAllThemes(object c)
    {
        switch (c.GetType().ToString())
        {
            case "System.Windows.Forms.GroupBox":
                ApplyTheme((GroupBox)c);
                break;
            case "System.Windows.Forms.TextBox":
                ApplyTheme((TextBox)c);
                break;
            case "System.Windows.Forms.Label":
                ApplyTheme((Label)c);
                break;
            case "System.Windows.Forms.Button":
                ApplyTheme((Button)c);
                break;
            case "System.Windows.Forms.ComboBox":
                ApplyTheme((ComboBox)c);
                break;
            case "System.Windows.Forms.DropDownPanel":
                ApplyTheme((ScrewTurn.DropDownPanel)c);
                break;
            case "System.Windows.Forms.MenuStrip":
                ApplyTheme((MenuStrip)c);
                break;
            case "System.Windows.Forms.TabControl":
                ApplyTheme((TabControl)c);
                break;
            case "System.Windows.Forms.CheckBox":
                ApplyTheme((CheckBox)c);
                break;
            case "System.Windows.Forms.Panel":
                ApplyTheme((Panel)c);
                break;
            case "System.Windows.Forms.TabPage":
                ApplyTheme((TabPage)c);
                break;
            case "System.Windows.Forms.DataGridView":
                ApplyTheme((DataGridView)c);
                break;
        }

    }
}