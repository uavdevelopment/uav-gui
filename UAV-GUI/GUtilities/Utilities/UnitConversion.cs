﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUtilities
{
   public static class UnitConversion
    {
        // km/h  to f/s 

       public static double kmPerH_fPerS(double value)
        {
          return  (value * 10) / 10.97;
        }

        // f/s to km/h

       public static double fPerS_kmPerH(double value)
      {
          return (value * 10.97) / 10;
      }

       // m to ft
       public static double meters_feet (double value)
       {
           return (value / 0.3048)*8;
           //return (value * 1312341)/50013;
       }

       //ft to m
       public static double feet_meters(double value)
       {
           return (value * 0.3048)/8;
        //  return (value * 50013) / 1312341;
       }
     //  rad to deg
       public static double rad_deg(double value)
       {
           return (value * 57.2957795) / 1024; 
       }
       // deg to rad 
       public static double deg_rad(double value)
       {
           return (value / 57.2957795) *1024;
       }

       // m/min to f/s
       public static double mPerMin_ftPerS (double value)
   {
       return (237 * value) / 542;
   }

       //f/s to  m/min  
       public static double ftPerS_mPerMin(double value)
       {
           return (542 * value) / 237;
       }
    }
}
