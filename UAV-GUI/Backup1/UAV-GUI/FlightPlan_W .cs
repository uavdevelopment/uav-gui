﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET;


namespace UAV_GUI
{
    public partial class Form2 : Form
    {
        GMapOverlay markersOverlay;
        List<PointLatLng> points;
        GMapRoute route;
        GMapOverlay routesOverlay;
        public Form2()
        {
            InitializeComponent();
            markersOverlay = new GMapOverlay(gmap, "markers");
            
            gmap.Overlays.Add(markersOverlay);
           
            points = new List<PointLatLng>();
            route = new GMapRoute(points, "route");
            
        }

       

        private void Form2_Load(object sender, EventArgs e)
        {
           
            gmap.MapProvider = GMap.NET.MapProviders.BingSatelliteMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;
            gmap.Position = new PointLatLng(30.058515, 31.231145);
            route.Points.Add(gmap.Position);
            gmap.UpdateRouteLocalPosition(route);
            route.Stroke.Color = Color.Green;
            route.Stroke.Width = 3;
            routesOverlay = new GMapOverlay(gmap, "route");
            routesOverlay.Routes.Add(route);
            gmap.Overlays.Add(routesOverlay);
            
           
        }

        private void map1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                PointLatLng p = gmap.FromLocalToLatLng(e.X, e.Y);
                GMapMarkerGoogleGreen marker = new GMapMarkerGoogleGreen(p);
                markersOverlay.Markers.Add(marker);
                
                route.Points.Add(p);
                gmap.UpdateRouteLocalPosition(route);
                route.Stroke.Color = Color.Green;
                route.Stroke.Width = 3;
                routesOverlay = new GMapOverlay(gmap, "route");
                routesOverlay.Routes.Add(route);
                //gmap.ZoomAndCenterRoute(route);
                //gmap.Overlays.Clear();
                gmap.Overlays.Add(routesOverlay);
               
               
                
            }
        }
        private void gmap_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            
        }

        private void panel1_CloseClick(object sender, EventArgs e)
        {

        }

        private void flickerFreePanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

       
    }
}
