﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading.Tasks;



namespace UAV_GUI
{
    
    class UAV
    {
        Random rnd = new Random();
        public  UAV (int id) { }
        private int uavId;

        public int UavId
        { 
        get { return uavId; }
        set { uavId = value; }
        }

        /* Basic Connections  functions needed from UAV */

        public Location getCurrentLocation() {
            return null;
        }

        public float getAngularVelocity()
        {
            return rnd.Next(-90,90)/1.0f;
        }

        public float getNetSpeed()
        {
            return rnd.Next(0, 60) / 1.0f;
        }

        public float getAltitude()
        {
            return rnd.Next(1, 45) / 5.0f;
        }


        public float getGPSpostionX() {
         return 0 ;
        }


        public float getGPSpostionY()
        {
            return 0;
        }

        public float getGPSpostionZ()
        {
            return 0;
        }

        public float getGPSVelocityX()
        {
            return 0;
        }

        public float getGPSVelocityY()
        {
            return 0;
        }

        public float getGPSVelocityZ()
        {
            return 0;
        }

        
        public float getHeading()
        {
            return 0;
        }

        public float getTurnRate() {
            return 0; 
        }

        public float getAirSpeed()
        {
            return 0;
        }

        public GPSTime getGPSTime () {
            return null;                 
        }

        public String getConnectionStatues() {
            return "No Connection"; 
        }

        public FlightMode getCurrentMode() {
                return  FlightMode.UAV ;
        } 



        /* May need to be transformed to a flight control class */ 
        public int takeOff() { return  0; }
        public int orbitight() { return  0; }
        public int orbitLeft () { return  0; }
        public int takePicture(){return 0;}
        public int descentHere(){return 0 ;} 
        public int hoverHere(){return 0 ;}
        public int circleRight(){return 0 ;}
        public int circleLeft(){return 0 ;}
        public int flyHome(){return 0 ;}
        public int stopEngine() {return 0 ;}
        public int landAtHome(){return 0 ;}
        public int landHere() { return 0 ; }

    }
}
