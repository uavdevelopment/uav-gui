﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAV_GUI
{
    class Plan
    {
        public Plan() { }
        
        private String vrsFilePath ;
        private String flyFilePath ;
        private Location homeLocation;
        private Location safeLocation;
        
        private List<WayPoint> path;


        //Getter / Setters
        public String VrsFilePath
        {
          get { return vrsFilePath; }
          set { vrsFilePath = value; }
        } 

        public String FlyFilePath
        {
           get { return flyFilePath; }
           set { flyFilePath = value; }
        }

        public  Location HomeLocation
        {
            get { return homeLocation; }
            set { homeLocation = value; }
        }
        

        public  Location SafeLocation
        {
            get { return safeLocation; }
            set { safeLocation = value; }
        }
        

        
        

        public List<WayPoint> Path
        {
            get { return path; }
            set { path = value; }
        } 
    }
}
