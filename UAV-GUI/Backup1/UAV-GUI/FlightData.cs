﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel; 

namespace UAV_GUI
{
    class FlightData : INotifyPropertyChanged
    {

        private float angularVelocity = 0;

        public float AngularVelocity
        {
            get { return angularVelocity; }
            set
            {
                if (value != AngularVelocity)
                {
                    angularVelocity = value;
                    //Whenever property value is changes
                    //PropertyChanged event is triggered
                    OnPropertyChanged("AngularVelocity");
                }
            }
        } 

        /* Speed */
        private float speed = 0;
        public float Speed
        {
            get { return speed; }
            set
            {
                if (value != Speed)
                {
                    speed = value;
                    //Whenever property value is changes
                    //PropertyChanged event is triggered
                    OnPropertyChanged("Speed");
                }
            }
        }


        /* Heading */
        private float heading = 0;
        public float Heading
        {
            get { return heading; }
            set
            {
                if (value != Heading)
                {
                    heading = value;
                    //Whenever property value is changes
                    //PropertyChanged event is triggered
                    OnPropertyChanged("Heading");
                }
            }
        }

        /*Altitude */

        private float altitude = 0;
        public float Altitude
        {
            get { return altitude; }
            set
            {
                if (value != Altitude)
                {
                    altitude = value;
                    OnPropertyChanged("Altitude");
                }
            }
        }

        /*Bank Angle */
        private float bankAngle = 0;
        public float BankAngle
        {
            get { return bankAngle; }
            set
            {
                if (value != BankAngle)
                {
                    bankAngle = value;
                    OnPropertyChanged("BankAngle");
                }
            }
        }
        /*Time */

        /*Air Speed */
        private float airSpeed = 0;

        public float AirSpeed
        {
            get { return airSpeed; }
            set
            {
                if (value != AirSpeed)
                {
                    airSpeed = value;
                    OnPropertyChanged("AirSpeed");
                }
            }
        }

        public GPSTime gpsTime = null;
        public Location Location = null;

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        //Create OnPropertyChanged method to raise event
        protected void OnPropertyChanged(string PropertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }

        #endregion
    }

}
