﻿namespace UAV_GUI
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label23 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.altitudeL = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.yawL = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gmap = new GMap.NET.WindowsForms.GMapControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.flightControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flightPlanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.artificialHorizonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusPanelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.googleSateliteMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bingSatelliteMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.googleGeoMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.googleTerravianMmapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.navigationDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serialPortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flightControlToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownPanel1 = new ScrewTurn.DropDownPanel();
            this.dropDownPanel2 = new ScrewTurn.DropDownPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cameraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownPanel3 = new ScrewTurn.DropDownPanel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button14 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel6 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel7 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel8 = new System.Windows.Forms.ToolStripStatusLabel();
            this.verticalSpeedIndicatorInstrumentControl2 = new UAV_GUI.VerticalSpeedIndicatorInstrumentControl();
            this.turnCoordinatorInstrumentControl2 = new UAV_GUI.TurnCoordinatorInstrumentControl();
            this.headingIndicatorInstrumentControl2 = new UAV_GUI.HeadingIndicatorInstrumentControl();
            this.attitudeIndicatorInstrumentControl2 = new UAV_GUI.AttitudeIndicatorInstrumentControl();
            this.altimeterInstrumentControl2 = new UAV_GUI.AltimeterInstrumentControl();
            this.airSpeedIndicatorInstrumentControl2 = new UAV_GUI.AirSpeedIndicatorInstrumentControl();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.dropDownPanel1.SuspendLayout();
            this.dropDownPanel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.dropDownPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(0, 6);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(757, 199);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(749, 173);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Status";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.35036F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.64964F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 178F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 214F));
            this.tableLayoutPanel1.Controls.Add(this.label23, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.label16, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.label29, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.label26, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label28, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label32, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label19, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label21, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.altitudeL, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label8, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label18, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label17, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.yawL, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label30, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label24, 3, 3);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(19, 15);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.5625F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.4375F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(712, 152);
            this.tableLayoutPanel1.TabIndex = 13;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label23.Location = new System.Drawing.Point(500, 122);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(28, 13);
            this.label23.TabIndex = 26;
            this.label23.Text = "0.00";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label16.Location = new System.Drawing.Point(500, 64);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(28, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "0.00";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(322, 122);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(89, 13);
            this.label29.TabIndex = 24;
            this.label29.Text = "Bank Angle (deg)";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label26.Location = new System.Drawing.Point(151, 122);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(28, 13);
            this.label26.TabIndex = 25;
            this.label26.Text = "0.00";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Altitude(m)";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(322, 64);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(77, 13);
            this.label28.TabIndex = 25;
            this.label28.Text = "Air Speed(m/s)";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(3, 122);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(55, 13);
            this.label32.TabIndex = 23;
            this.label32.Text = "Turn Rate";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label19.Location = new System.Drawing.Point(500, 33);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(28, 13);
            this.label19.TabIndex = 10;
            this.label19.Text = "0.00";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label21.Location = new System.Drawing.Point(151, 64);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(28, 13);
            this.label21.TabIndex = 12;
            this.label21.Text = "0.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(322, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Dist to Wp (m)";
            // 
            // altitudeL
            // 
            this.altitudeL.AutoSize = true;
            this.altitudeL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.altitudeL.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.altitudeL.Location = new System.Drawing.Point(151, 0);
            this.altitudeL.Name = "altitudeL";
            this.altitudeL.Size = new System.Drawing.Size(28, 13);
            this.altitudeL.TabIndex = 6;
            this.altitudeL.Text = "0.00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(322, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Yaw(deg)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label18.Location = new System.Drawing.Point(151, 94);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(28, 13);
            this.label18.TabIndex = 9;
            this.label18.Text = "0.00";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 33);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "Heading (deg)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Vertical Speed(m/s)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label17.Location = new System.Drawing.Point(151, 33);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "0.00";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Ground Speed(m/s)";
            // 
            // yawL
            // 
            this.yawL.AutoSize = true;
            this.yawL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yawL.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.yawL.Location = new System.Drawing.Point(500, 0);
            this.yawL.Name = "yawL";
            this.yawL.Size = new System.Drawing.Size(28, 13);
            this.yawL.TabIndex = 11;
            this.yawL.Text = "0.00";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(322, 94);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(55, 13);
            this.label30.TabIndex = 27;
            this.label30.Text = "GPS Time";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label24.Location = new System.Drawing.Point(500, 94);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(51, 13);
            this.label24.TabIndex = 28;
            this.label24.Text = "hh:mm:ss";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tabControl1);
            this.groupBox2.Location = new System.Drawing.Point(12, 492);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(767, 211);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Menu;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.flightToolStripMenuItem,
            this.modeToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(9, 9);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(265, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // flightToolStripMenuItem
            // 
            this.flightToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.artificialHorizonToolStripMenuItem,
            this.statusPanelToolStripMenuItem,
            this.mapToolStripMenuItem,
            this.flightControlToolStripMenuItem1});
            this.flightToolStripMenuItem.Name = "flightToolStripMenuItem";
            this.flightToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.flightToolStripMenuItem.Text = "View ";
            this.flightToolStripMenuItem.Click += new System.EventHandler(this.flightToolStripMenuItem_Click);
            // 
            // modeToolStripMenuItem
            // 
            this.modeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.navigationDataToolStripMenuItem,
            this.serialPortToolStripMenuItem});
            this.modeToolStripMenuItem.Name = "modeToolStripMenuItem";
            this.modeToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.modeToolStripMenuItem.Text = "Pereference";
            this.modeToolStripMenuItem.Click += new System.EventHandler(this.modeToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.flightControlToolStripMenuItem,
            this.flightPlanToolStripMenuItem,
            this.setupToolStripMenuItem,
            this.cameraToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // gmap
            // 
            this.gmap.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.gmap.Bearing = 0F;
            this.gmap.CanDragMap = false;
            this.gmap.GrayScaleMode = false;
            this.gmap.LevelsKeepInMemmory = 5;
            this.gmap.Location = new System.Drawing.Point(12, 27);
            this.gmap.MarkersEnabled = true;
            this.gmap.MaxZoom = 18;
            this.gmap.MinZoom = 2;
            this.gmap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.ViewCenter;
            this.gmap.Name = "gmap";
            this.gmap.NegativeMode = false;
            this.gmap.PolygonsEnabled = true;
            this.gmap.RetryLoadTile = 0;
            this.gmap.RoutesEnabled = true;
            this.gmap.ShowTileGridLines = false;
            this.gmap.Size = new System.Drawing.Size(767, 465);
            this.gmap.TabIndex = 16;
            this.gmap.Zoom = 10D;
            this.gmap.Load += new System.EventHandler(this.gmap_Load);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBox1.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.ErrorImage")));
            this.pictureBox1.Image = global::UAV_GUI.Properties.Resources.plane;
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(377, 197);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(45, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel4,
            this.toolStripStatusLabel5,
            this.toolStripStatusLabel6,
            this.toolStripStatusLabel7,
            this.toolStripStatusLabel8});
            this.statusStrip1.Location = new System.Drawing.Point(0, 708);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1312, 22);
            this.statusStrip1.TabIndex = 17;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // flightControlToolStripMenuItem
            // 
            this.flightControlToolStripMenuItem.Name = "flightControlToolStripMenuItem";
            this.flightControlToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.flightControlToolStripMenuItem.Text = "Flight Control ";
            this.flightControlToolStripMenuItem.Click += new System.EventHandler(this.flightControlToolStripMenuItem_Click);
            // 
            // flightPlanToolStripMenuItem
            // 
            this.flightPlanToolStripMenuItem.Name = "flightPlanToolStripMenuItem";
            this.flightPlanToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.flightPlanToolStripMenuItem.Text = "Flight Plan";
            this.flightPlanToolStripMenuItem.Click += new System.EventHandler(this.flightPlanToolStripMenuItem_Click);
            // 
            // setupToolStripMenuItem
            // 
            this.setupToolStripMenuItem.Name = "setupToolStripMenuItem";
            this.setupToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.setupToolStripMenuItem.Text = "Setup ";
            this.setupToolStripMenuItem.Click += new System.EventHandler(this.setupToolStripMenuItem_Click);
            // 
            // artificialHorizonToolStripMenuItem
            // 
            this.artificialHorizonToolStripMenuItem.Name = "artificialHorizonToolStripMenuItem";
            this.artificialHorizonToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.artificialHorizonToolStripMenuItem.Text = "Artificial Horizon ";
            this.artificialHorizonToolStripMenuItem.Click += new System.EventHandler(this.artificialHorizonToolStripMenuItem_Click);
            // 
            // statusPanelToolStripMenuItem
            // 
            this.statusPanelToolStripMenuItem.Name = "statusPanelToolStripMenuItem";
            this.statusPanelToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.statusPanelToolStripMenuItem.Text = "Status Panel ";
            // 
            // mapToolStripMenuItem
            // 
            this.mapToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.googleSateliteMapToolStripMenuItem,
            this.bingSatelliteMapToolStripMenuItem,
            this.googleGeoMapToolStripMenuItem,
            this.googleTerravianMmapToolStripMenuItem});
            this.mapToolStripMenuItem.Name = "mapToolStripMenuItem";
            this.mapToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.mapToolStripMenuItem.Text = "Map";
            // 
            // googleSateliteMapToolStripMenuItem
            // 
            this.googleSateliteMapToolStripMenuItem.Name = "googleSateliteMapToolStripMenuItem";
            this.googleSateliteMapToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.googleSateliteMapToolStripMenuItem.Text = "Google Satelite map ";
            // 
            // bingSatelliteMapToolStripMenuItem
            // 
            this.bingSatelliteMapToolStripMenuItem.Name = "bingSatelliteMapToolStripMenuItem";
            this.bingSatelliteMapToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.bingSatelliteMapToolStripMenuItem.Text = "Bing Satellite map ";
            // 
            // googleGeoMapToolStripMenuItem
            // 
            this.googleGeoMapToolStripMenuItem.Name = "googleGeoMapToolStripMenuItem";
            this.googleGeoMapToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.googleGeoMapToolStripMenuItem.Text = "Google Geo map ";
            // 
            // googleTerravianMmapToolStripMenuItem
            // 
            this.googleTerravianMmapToolStripMenuItem.Name = "googleTerravianMmapToolStripMenuItem";
            this.googleTerravianMmapToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.googleTerravianMmapToolStripMenuItem.Text = "Google Terrain map ";
            this.googleTerravianMmapToolStripMenuItem.Click += new System.EventHandler(this.googleTerravianMmapToolStripMenuItem_Click);
            // 
            // navigationDataToolStripMenuItem
            // 
            this.navigationDataToolStripMenuItem.Name = "navigationDataToolStripMenuItem";
            this.navigationDataToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.navigationDataToolStripMenuItem.Text = "Navigation Data ";
            // 
            // serialPortToolStripMenuItem
            // 
            this.serialPortToolStripMenuItem.Name = "serialPortToolStripMenuItem";
            this.serialPortToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.serialPortToolStripMenuItem.Text = "Serial Port ";
            // 
            // flightControlToolStripMenuItem1
            // 
            this.flightControlToolStripMenuItem1.Name = "flightControlToolStripMenuItem1";
            this.flightControlToolStripMenuItem1.Size = new System.Drawing.Size(166, 22);
            this.flightControlToolStripMenuItem1.Text = "Flight Control ";
            // 
            // dropDownPanel1
            // 
            this.dropDownPanel1.AutoCollapseDelay = -1;
            this.dropDownPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dropDownPanel1.Controls.Add(this.tableLayoutPanel2);
            this.dropDownPanel1.EnableHeaderMenu = true;
            this.dropDownPanel1.ExpandAnimationSpeed = ScrewTurn.AnimationSpeed.Medium;
            this.dropDownPanel1.Expanded = true;
            this.dropDownPanel1.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropDownPanel1.HeaderHeight = 20;
            this.dropDownPanel1.HeaderIconNormal = null;
            this.dropDownPanel1.HeaderIconOver = null;
            this.dropDownPanel1.HeaderText = "Patterns ";
            this.dropDownPanel1.HomeLocation = new System.Drawing.Point(779, 101);
            this.dropDownPanel1.HotTrackStyle = ScrewTurn.HotTrackStyle.Both;
            this.dropDownPanel1.Location = new System.Drawing.Point(779, 101);
            this.dropDownPanel1.ManageControls = false;
            this.dropDownPanel1.Moveable = true;
            this.dropDownPanel1.Name = "dropDownPanel1";
            this.dropDownPanel1.RoundedCorners = true;
            this.dropDownPanel1.Size = new System.Drawing.Size(515, 207);
            this.dropDownPanel1.TabIndex = 12;
            this.dropDownPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.dropDownPanel1_Paint);
            // 
            // dropDownPanel2
            // 
            this.dropDownPanel2.AutoCollapseDelay = -1;
            this.dropDownPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dropDownPanel2.Controls.Add(this.verticalSpeedIndicatorInstrumentControl2);
            this.dropDownPanel2.Controls.Add(this.turnCoordinatorInstrumentControl2);
            this.dropDownPanel2.Controls.Add(this.headingIndicatorInstrumentControl2);
            this.dropDownPanel2.Controls.Add(this.attitudeIndicatorInstrumentControl2);
            this.dropDownPanel2.Controls.Add(this.altimeterInstrumentControl2);
            this.dropDownPanel2.Controls.Add(this.airSpeedIndicatorInstrumentControl2);
            this.dropDownPanel2.EnableHeaderMenu = true;
            this.dropDownPanel2.ExpandAnimationSpeed = ScrewTurn.AnimationSpeed.Medium;
            this.dropDownPanel2.Expanded = true;
            this.dropDownPanel2.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropDownPanel2.HeaderHeight = 20;
            this.dropDownPanel2.HeaderIconNormal = null;
            this.dropDownPanel2.HeaderIconOver = null;
            this.dropDownPanel2.HeaderText = "Artificial Horizon ";
            this.dropDownPanel2.HomeLocation = new System.Drawing.Point(782, 64);
            this.dropDownPanel2.HotTrackStyle = ScrewTurn.HotTrackStyle.Both;
            this.dropDownPanel2.Location = new System.Drawing.Point(782, 64);
            this.dropDownPanel2.ManageControls = false;
            this.dropDownPanel2.Moveable = true;
            this.dropDownPanel2.Name = "dropDownPanel2";
            this.dropDownPanel2.RoundedCorners = true;
            this.dropDownPanel2.Size = new System.Drawing.Size(515, 666);
            this.dropDownPanel2.TabIndex = 18;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.48485F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.51515F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 118F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tableLayoutPanel2.Controls.Add(this.button12, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.button11, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.button10, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.button9, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.button8, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.button7, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.button6, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.button5, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.button4, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.button3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.button2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.button1, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 27);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(496, 164);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Location = new System.Drawing.Point(366, 119);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(106, 42);
            this.button12.TabIndex = 12;
            this.button12.Text = "Figure8 ";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(248, 119);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(106, 42);
            this.button11.TabIndex = 10;
            this.button11.Text = "Land here";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(122, 119);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(106, 42);
            this.button10.TabIndex = 9;
            this.button10.Text = "Land at home";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(3, 119);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(106, 42);
            this.button9.TabIndex = 8;
            this.button9.Text = "Stop engine";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(366, 61);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(106, 44);
            this.button8.TabIndex = 7;
            this.button8.Text = "Fly Home";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(248, 61);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(106, 44);
            this.button7.TabIndex = 6;
            this.button7.Text = "Circle Left";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(122, 61);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(106, 44);
            this.button6.TabIndex = 5;
            this.button6.Text = "Circle right";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(3, 61);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(106, 44);
            this.button5.TabIndex = 4;
            this.button5.Text = "Descent here";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(366, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(106, 44);
            this.button4.TabIndex = 3;
            this.button4.Text = "Take Picture";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(248, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(106, 44);
            this.button3.TabIndex = 2;
            this.button3.Text = "Orbit left";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(122, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(106, 44);
            this.button2.TabIndex = 1;
            this.button2.Text = "Orbit Right";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 44);
            this.button1.TabIndex = 0;
            this.button1.Text = "Takeoff";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // cameraToolStripMenuItem
            // 
            this.cameraToolStripMenuItem.Name = "cameraToolStripMenuItem";
            this.cameraToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.cameraToolStripMenuItem.Text = "Camera";
            this.cameraToolStripMenuItem.Click += new System.EventHandler(this.cameraToolStripMenuItem_Click);
            // 
            // dropDownPanel3
            // 
            this.dropDownPanel3.AutoCollapseDelay = -1;
            this.dropDownPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dropDownPanel3.Controls.Add(this.label3);
            this.dropDownPanel3.Controls.Add(this.button14);
            this.dropDownPanel3.Controls.Add(this.comboBox1);
            this.dropDownPanel3.EnableHeaderMenu = true;
            this.dropDownPanel3.ExpandAnimationSpeed = ScrewTurn.AnimationSpeed.Medium;
            this.dropDownPanel3.Expanded = true;
            this.dropDownPanel3.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropDownPanel3.HeaderHeight = 20;
            this.dropDownPanel3.HeaderIconNormal = null;
            this.dropDownPanel3.HeaderIconOver = null;
            this.dropDownPanel3.HeaderText = "Autopilot Control ";
            this.dropDownPanel3.HomeLocation = new System.Drawing.Point(785, 27);
            this.dropDownPanel3.HotTrackStyle = ScrewTurn.HotTrackStyle.Both;
            this.dropDownPanel3.Location = new System.Drawing.Point(785, 27);
            this.dropDownPanel3.ManageControls = false;
            this.dropDownPanel3.Moveable = true;
            this.dropDownPanel3.Name = "dropDownPanel3";
            this.dropDownPanel3.RoundedCorners = true;
            this.dropDownPanel3.Size = new System.Drawing.Size(515, 50);
            this.dropDownPanel3.TabIndex = 13;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "COM 1 ",
            "COM 2 ",
            "COM 3 ",
            "COM 4"});
            this.comboBox1.Location = new System.Drawing.Point(180, 30);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 1;
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.Location = new System.Drawing.Point(393, 29);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(106, 21);
            this.button14.TabIndex = 3;
            this.button14.Text = "Connect";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Port";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(415, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "0.00 ,0.00 ,0.00";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(153, 17);
            this.toolStripStatusLabel1.Text = "Distance to Next Way point ";
            this.toolStripStatusLabel1.Click += new System.EventHandler(this.toolStripStatusLabel1_Click);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(28, 17);
            this.toolStripStatusLabel2.Text = "0.00";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(49, 17);
            this.toolStripStatusLabel3.Text = "Altitude";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(28, 17);
            this.toolStripStatusLabel4.Text = "0.00";
            // 
            // toolStripStatusLabel5
            // 
            this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
            this.toolStripStatusLabel5.Size = new System.Drawing.Size(52, 17);
            this.toolStripStatusLabel5.Text = "Heading";
            // 
            // toolStripStatusLabel6
            // 
            this.toolStripStatusLabel6.Name = "toolStripStatusLabel6";
            this.toolStripStatusLabel6.Size = new System.Drawing.Size(28, 17);
            this.toolStripStatusLabel6.Text = "0.00";
            // 
            // toolStripStatusLabel7
            // 
            this.toolStripStatusLabel7.Name = "toolStripStatusLabel7";
            this.toolStripStatusLabel7.Size = new System.Drawing.Size(49, 17);
            this.toolStripStatusLabel7.Text = "Velocity";
            this.toolStripStatusLabel7.Click += new System.EventHandler(this.toolStripStatusLabel7_Click);
            // 
            // toolStripStatusLabel8
            // 
            this.toolStripStatusLabel8.Name = "toolStripStatusLabel8";
            this.toolStripStatusLabel8.Size = new System.Drawing.Size(28, 17);
            this.toolStripStatusLabel8.Text = "0.00";
            // 
            // verticalSpeedIndicatorInstrumentControl2
            // 
            this.verticalSpeedIndicatorInstrumentControl2.Location = new System.Drawing.Point(39, 459);
            this.verticalSpeedIndicatorInstrumentControl2.Name = "verticalSpeedIndicatorInstrumentControl2";
            this.verticalSpeedIndicatorInstrumentControl2.Size = new System.Drawing.Size(174, 172);
            this.verticalSpeedIndicatorInstrumentControl2.TabIndex = 17;
            this.verticalSpeedIndicatorInstrumentControl2.Text = "verticalSpeedIndicatorInstrumentControl2";
            // 
            // turnCoordinatorInstrumentControl2
            // 
            this.turnCoordinatorInstrumentControl2.Location = new System.Drawing.Point(267, 266);
            this.turnCoordinatorInstrumentControl2.Name = "turnCoordinatorInstrumentControl2";
            this.turnCoordinatorInstrumentControl2.Size = new System.Drawing.Size(198, 196);
            this.turnCoordinatorInstrumentControl2.TabIndex = 16;
            this.turnCoordinatorInstrumentControl2.Text = "turnCoordinatorInstrumentControl2";
            // 
            // headingIndicatorInstrumentControl2
            // 
            this.headingIndicatorInstrumentControl2.Location = new System.Drawing.Point(341, 514);
            this.headingIndicatorInstrumentControl2.Name = "headingIndicatorInstrumentControl2";
            this.headingIndicatorInstrumentControl2.Size = new System.Drawing.Size(124, 128);
            this.headingIndicatorInstrumentControl2.TabIndex = 15;
            this.headingIndicatorInstrumentControl2.Text = "headingIndicatorInstrumentControl2";
            // 
            // attitudeIndicatorInstrumentControl2
            // 
            this.attitudeIndicatorInstrumentControl2.Location = new System.Drawing.Point(22, 202);
            this.attitudeIndicatorInstrumentControl2.Name = "attitudeIndicatorInstrumentControl2";
            this.attitudeIndicatorInstrumentControl2.Size = new System.Drawing.Size(191, 195);
            this.attitudeIndicatorInstrumentControl2.TabIndex = 14;
            this.attitudeIndicatorInstrumentControl2.Text = "attitudeIndicatorInstrumentControl2";
            // 
            // altimeterInstrumentControl2
            // 
            this.altimeterInstrumentControl2.Location = new System.Drawing.Point(320, 37);
            this.altimeterInstrumentControl2.Name = "altimeterInstrumentControl2";
            this.altimeterInstrumentControl2.Size = new System.Drawing.Size(179, 180);
            this.altimeterInstrumentControl2.TabIndex = 13;
            this.altimeterInstrumentControl2.Text = "altimeterInstrumentControl2";
            // 
            // airSpeedIndicatorInstrumentControl2
            // 
            this.airSpeedIndicatorInstrumentControl2.Location = new System.Drawing.Point(22, 32);
            this.airSpeedIndicatorInstrumentControl2.Name = "airSpeedIndicatorInstrumentControl2";
            this.airSpeedIndicatorInstrumentControl2.Size = new System.Drawing.Size(133, 135);
            this.airSpeedIndicatorInstrumentControl2.TabIndex = 12;
            this.airSpeedIndicatorInstrumentControl2.Text = "airSpeedIndicatorInstrumentControl2";
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(734, 27);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBar1.Size = new System.Drawing.Size(45, 465);
            this.trackBar1.TabIndex = 20;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1312, 730);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dropDownPanel3);
            this.Controls.Add(this.dropDownPanel1);
            this.Controls.Add(this.dropDownPanel2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.gmap);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindow";
            this.Text = "UAV -CGS";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.dropDownPanel1.ResumeLayout(false);
            this.dropDownPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.dropDownPanel3.ResumeLayout(false);
            this.dropDownPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flightToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private GMap.NET.WindowsForms.GMapControl gmap;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label altitudeL;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label yawL;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem flightControlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flightPlanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem artificialHorizonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statusPanelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem googleSateliteMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bingSatelliteMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem googleGeoMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem googleTerravianMmapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flightControlToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem navigationDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serialPortToolStripMenuItem;
        private ScrewTurn.DropDownPanel dropDownPanel1;
        private ScrewTurn.DropDownPanel dropDownPanel2;
        private VerticalSpeedIndicatorInstrumentControl verticalSpeedIndicatorInstrumentControl2;
        private TurnCoordinatorInstrumentControl turnCoordinatorInstrumentControl2;
        private HeadingIndicatorInstrumentControl headingIndicatorInstrumentControl2;
        private AttitudeIndicatorInstrumentControl attitudeIndicatorInstrumentControl2;
        private AltimeterInstrumentControl altimeterInstrumentControl2;
        private AirSpeedIndicatorInstrumentControl airSpeedIndicatorInstrumentControl2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem cameraToolStripMenuItem;
        private ScrewTurn.DropDownPanel dropDownPanel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel5;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel6;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel7;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel8;
        private System.Windows.Forms.TrackBar trackBar1;




    }
}

