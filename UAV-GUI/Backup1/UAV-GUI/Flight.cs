﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace UAV_GUI 
{
    class Flight
    {
        
        public FlightData flyData = new FlightData();
        public Thread dataAcquisitionThread;

        private void calculateData() {
            
            while (true) {
                
                
                /*flyData.Altitude+=0.2f;
                if (flyData.Altitude > 8) flyData.Altitude=0; */
                /*Gauges  Data */
                flyData.Altitude = flyUAV.getAltitude();
                flyData.Speed = flyUAV.getNetSpeed();
                flyData.AngularVelocity = flyUAV.getAngularVelocity();
                /*Numeric Data*/ 
                flyData.Heading = flyUAV.getHeading();
                flyData.AirSpeed = flyUAV.getAirSpeed();
                flyData.Location = flyUAV.getCurrentLocation();
                
                Thread.Sleep(1000);
            }
        
        }
       
        public Flight() {
            planFlight = new Plan();
            flyUAV = new UAV(0);
            dataAcquisitionThread = new Thread(new ThreadStart (calculateData)) ;
        }
        private Plan planFlight;

        internal Plan PlanFlight
        {
            get { return planFlight; }
            set { planFlight = value; }
        }
        private UAV flyUAV;
        private FlightMode FlyMode;


        public int initializeFly() {
            return 1; // initialized successfully . 
        }

        public void changeMode(FlightMode mode_) {
            // Add the Controls . 
            FlyMode = mode_; 
        }



       



    }
}
