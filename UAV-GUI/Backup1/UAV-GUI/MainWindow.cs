﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;



namespace UAV_GUI
{
    public partial class MainWindow : Form
    {
        private Flight myFlight;

        public MainWindow()
        {
            InitializeComponent();
            myFlight = new Flight();
            initializebinding();
        }

       private void bindGauges() {
            /*Gauge Bindings*/
          /*  aGauge1.DataBindings.Add(new Binding("Value", myFlight.flyData, "Altitude"));
            aGauge2.DataBindings.Add(new Binding("Value", myFlight.flyData, "AngularVelocity"));
            aGauge3.DataBindings.Add(new Binding("Value", myFlight.flyData, "Speed"));*/
        }

        private void bindLabels()
        {
            altitudeL.DataBindings.Add(new Binding("Text", myFlight.flyData, "Altitude"));
            yawL.DataBindings.Add(new Binding("Text", myFlight.flyData, "AngularVelocity")); 

        }

        private void initializebinding() {
           // bindGauges();
            bindLabels();
        }

        


        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripComboBox1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
                      
        }

        private void gMapControl1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dropDownPanel3.SwitchStatus();
            dropDownPanel1.SwitchStatus();
            dropDownPanel2.SwitchStatus();
            dropDownPanel1.AddManagedControl(dropDownPanel2);
            dropDownPanel3.AddManagedControl(dropDownPanel1);

            myFlight.dataAcquisitionThread.Start();
            //aGauge1.Value = 5;
            gmap.MapProvider = GMap.NET.MapProviders.BingSatelliteMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;
            gmap.Position = new PointLatLng(30.058515, 31.231145);
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void toolStripComboBox1_Click_1(object sender, EventArgs e)
        {
            
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void modeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            
        }

        private void label30_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
           
        }

        private void gmap_Load(object sender, EventArgs e)
        {

        }

        private void toolStripButton10_Click(object sender, EventArgs e)
        {
            AGLGauge_W AGL = new AGLGauge_W();
            AGL.Show();
        }

        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            V_OSD_W window = new V_OSD_W();
            window.Show();
        }

        private void toolStripButton11_Click(object sender, EventArgs e)
        {
            
        }

        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void flightToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void googleTerravianMmapToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void flightPlanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 n = new Form2();
            n.Show();
        }

        private void flightControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FlightControl_W myControl = new FlightControl_W();
            myControl.Show();
        }

        private void setupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Setup setupForm = new Setup();
            setupForm.Show();




        }

        private void dropDownPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cameraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CameraControl_W camControl = new CameraControl_W();
            camControl.Show();
        }

        private void artificialHorizonToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Artifical_Horizon AH = new Artifical_Horizon();
            AH.Show();

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button14_Click(object sender, EventArgs e)
        {

        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripStatusLabel7_Click(object sender, EventArgs e)
        {

        }
    }
}
