﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAV_GUI
{
    enum positionType { absolute , relative} ; 
    class WayPoint
    {
        public WayPoint(positionType wpType_,Location wpLocation_) {
            this.wpType = wpType_;
            this.wpLocation = wpLocation_; 
        }
        private positionType wpType;
        private Location wpLocation;
        
        public Location  getWpLocation (){
            
            return this.wpLocation;
        }

        public Location getWpLocation(WayPoint refWp_ )
        {
            if (refWp_.getWpType() == positionType.absolute) {
                return this.wpLocation;
            }

            return wpLocation + refWp_.getWpLocation();
        }
        public positionType getWpType()
        {
            return this.wpType;
        }
        
    }
}
