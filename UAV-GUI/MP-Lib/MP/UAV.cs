﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using  System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Threading;
using FileClasses.INI_Files;
using Error_Reporting;
using UAV_GUI.Utilities;
using AutoPilotMemory;
using MasterClasses;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using GLobal;
using GUtilities;
namespace MP_Lib
{
    /// <summary>
    /// 
    /// </summary>
   public partial class UAV
    {
         int count = 0;
        Random rnd = new Random();
        
        public  UAV (int id) {
            uavId = id;
            
               
        }

        private int uavId;

        public int UavId
        { 
        get { return uavId; }
        set { uavId = value; }
        }

        /// <summary>
        /// Check for fatal Error Codes
        /// </summary>

        public List<int> getListofErrorCodes() {

            List<int> codes = new List<int>();
            for (int fieldid = 2080; fieldid < 2096; fieldid++) {
                int valueA = 0;
                int retVal = 0;
                retVal = mpReadVar(uavId, fieldid, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                if (retVal != MP_OK) {
                    throw new BusinessException(4, "Link BroKen");
                }
                if (valueA >= 2 && valueA <=110) {
                    codes.Add(valueA);                   
                }
           
            }
            Console.WriteLine("[Debuggger] : Number of Error Codes is " + codes.Count);
             return codes; 
        }

        /// <summary>
        /// GetStandard Telemetry.
        /// </summary>

        public int  getSTDTelemtry(ref MPSTDTELEMETRYDATA myStd)
        {
            short pitch=0;
            short roll=0;
            int retVal; 
            

             retVal = mpResponseStuffServos(uavId, 0, 0, 0, 0, ref pitch, ref roll, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            retVal = mpGetStandardTelemetry(uavId, ref  myStd);

            Console.WriteLine("[Debugger] : return Value : {0} , {1}"+ retVal+myStd.status);
            
            return retVal;
            
        }

        // Check for warnings codes
        public List<int> getListofWarningsCodes()
        {

            List<int> codes = new List<int>();
            for (int fieldid = 2154; fieldid < 2169; fieldid++)
            {
                int valueA = 0;
                int retVal = 0;
                retVal = mpReadVar(uavId, fieldid, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                if (retVal != MP_OK)
                {
                    return null;
                    
                    
                }
                if (valueA >= 1 && valueA <= 19)
                {
                    codes.Add(valueA);
                }

            }
           Console.WriteLine("[Debuggger] : Number of warning Codes is " + codes.Count);

            
            return codes;
        }


          /* Basic Connections  functions needed from UAV */
        /// <summary>
        /// Check the 1295 field id MPFID_FATAL_ERROR  
        /// </summary>
        /// <returns></returns>

        public int checkForFatalError() {
            
            int valueA = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, (int)fieldsID.MPFID_FATAL_ERROR, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            Console.WriteLine("GPS locked or Not : " + valueA);
            if (retVal != MP_OK)
            {
                Console.WriteLine("[Debuggger] : Failed to check for fatal Errors");
                return -1;

            }

            else
            {
                Console.WriteLine("[Debuggger] : Multiple fatal or Singgle is : " + valueA);
                return valueA;
            }
        }


        /// <summary>
        /// Disconnect From The AutoPilot 
        /// </summary>
        /// <returns></returns>

        public bool disConnect()
        {

            int mperr = mpCloseLink(uavId);
            if (mperr != MP_OK)
            {
                GlobalData.LogConsole(".......Could Not Close the Link to the MP Autopilot");
                return false;
            }

            if (mpDelete(uavId) != 0)
            {
                return false;
            }
            else
            {
                GlobalData.apConnected = false;
                return true;
               
            }
        }

        /// <summary>
        /// TemporarlyfaketheGPS.
        /// </summary>
        /// <returns></returns>

        public bool fakeTheGPSLock() {
            int error = SetVariable(uavId, tmpFGPS_Field_ID, 1, NORMAL_MODE);
            if (error!= MP_OK) {
                Console.WriteLine("[Debuggger] : Failed to fake GPS ");
                return false; 
            }
            Console.WriteLine("[Debuggger] : Succedded to fake GPS ");
            return true; 
        
        }
        
        /// <summary>
        /// Get the GPS Lock Status.
        /// </summary>
        /// <returns></returns>

        public int isGPSOK()
        {
            
            int valueA = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId,(int) fieldsID.MPFID_GPS_OK, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            Console.WriteLine("GPS locked or Not : " + valueA);
            if (retVal != MP_OK)
            {

                return -1;

            }
            else return valueA;
            
          
        }
        

       ///

        public static int initializeTheLinkToAP(int planeId, String filename) {

            GlobalData.LogConsole(" Creating UAV " + planeId);
            int retVal = mpCreate(planeId);
            if (retVal != 0)
            {

                GlobalData.LogConsole(" ERROR - could not Create UAV #: " + planeId + " Error: " + retVal);
                return retVal;
            }

            /* This Function will initialize the simulate and load the default.fly */

            retVal = mpInitFly(planeId, @filename);
            if (retVal != 0)
            {
                GlobalData.LogConsole("ERROR - could not Initialize Simulator #:" + planeId + "Error: retVal");
                return retVal;
            }

            /*This function will Initialize the comport to the planeId */
            if (GlobalData.modeSimulatorOrAuto == 0)
            { // for UAV 
               
                retVal = mpInitLink(1, INIData.compPort); /* by leaving comport blank "" we tell it to use the entry from the uav<planeId>.ini */
                if (retVal != 0)
                {
                    GlobalData.LogConsole(" ERROR - could not Initialize Link To Comport Error: " + retVal);
                    return retVal;       
              }
                retVal = mpSetLinkBaud(planeId, (int)INIData.baudRate);
                if (retVal != 0)
                {
                    GlobalData.LogConsole(" ERROR - could not Initialize Link To Comport Error: " + retVal);
                    return retVal;
                }
            }
            return MP_OK;
        }
        /// <summary>
        /// Connects To the autopilot .
        /// </summary>
        /// <returns></returns>

        
        public bool connect(String filePath, short mode)   
        {

            int mperr = CreatePlane(uavId, @filePath,mode);
            GlobalData.LogConsole( "\n retVal is " + mperr);
            if (mperr != 0)
            {
                // Failed
                GlobalData.LogConsole( "\nError no /  :   {0}" + mperr);
                mpDelete(uavId);   // Always delete plane even if creation failed
                return false;
            }
            mperr = mpStartTelemtry(uavId);
            if (mperr != 0)
            {
                // Failed
                GlobalData.LogConsole( "\nError/  :   {0}" + mperr);
                mpDelete(uavId);   // Always delete plane even if creation failed
                return false;
            }
            else
            {
                GlobalData.LogConsole( "\nUAV {0} Created Successfully" + uavId);
                GlobalData.apConnected = true;
                return true;
            }

        }
        /// <summary>
        /// Initialize the flight .
        /// </summary>
        /// <returns></returns>


        public bool Arm_Initialize(short mode)
        {

              int  mperr = InitializeFlyPlane(uavId, mode);
                if (mperr != MP_OK)
                {
                    return false;
                }
                GlobalData.LogConsole("[Debuggger] : Armed ");
                return true;
         }

     /// <summary>
        /// Take off .
        /// </summary>
        /// <returns></returns>


        public bool takeOff( short mode)
        {

              int  mperr = FlyPlane(uavId, mode);
                if (mperr != MP_OK)
                {
                    GlobalData.LogConsole("[Debuggger] : Took off State is  "+ mperr);
                    
                    return false;
                }
                GlobalData.LogConsole("[Debuggger] : Took off ");
                return true;
            }


        /// <summary>
        /// Get Current Location of the airbone
        /// 
        /// </summary>
        /// <returns></returns>
       
        public Location getCurrentLocationNotUsed() {
            
            int valueA = 0;
            int valueLat = 0;
            int valueLong = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, Alt_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            retVal = mpReadVar(uavId, Long_Field_ID, ref valueLat, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            retVal = mpReadVar(uavId, Lat_Field_ID, ref valueLong, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            if (retVal != MP_OK) {

                return null; 

            }

            return new Location(valueA/-8,valueLat,valueLong);
        }

        /// <summary>
        /// This Function Uses The XTender SDK To get The Cuurrent Next Waypoint
        /// </summary>
        /// <returns></returns>



        public float getDistanceToNextWayPoint()
        {
            int valueA = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, DistToNXTWP_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return -1;

            }
            else
            {
                float dtnxp = AutoPilotUnits.toDistance(valueA);
                Console.WriteLine("[Debuggger] : The distance to the ext waypoint is  "+dtnxp);
                return dtnxp;
            }
        }


        /// <summary>
        /// This Function is used to calculate the angular velocity 
        /// Wx = rollrate - yawrate * sin(pitch)
        ///Wy = pitchrate * cos(roll) + yawrate * sin(roll) * cos(pitch) 
        ///Wz = yawrate * cos(roll) * cos(pitch) - pitchrate * sin(roll)
        ///Then Get the magnitude 
        /// </summary>
        /// <returns></returns>

        public double getAngularVelocity()
        {
           ///To Be calculated after confirmation 
            int valueRoll = 0;
            int valuePitch = 0;
            int valuePitchDot = 0;
            int valueYawDot = 0;
            int valueRollDot = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, Roll_Field_ID, ref valueRoll, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            retVal = mpReadVar(uavId, Pitch_Field_ID, ref valuePitch, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            retVal = mpReadVar(uavId, Pitch_dot_Field_ID, ref valuePitchDot, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            retVal = mpReadVar(uavId, Yaw_dot_Field_ID, ref valueYawDot, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            retVal = mpReadVar(uavId, Roll_dot_Field_ID, ref valueRollDot, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            ///ToDo : Add Error Handling 
            
            double Wx = valueRollDot - valueYawDot * Math.Sin(valuePitch);
            double Wy = valuePitchDot * Math.Cos(valueRoll) + valueYawDot * Math.Sin(valueRoll) * Math.Cos(valuePitch);
            double Wz = valueYawDot * Math.Cos(valueRoll) * Math.Cos(valuePitch) - valuePitchDot * Math.Sin(valueRoll);

            double anV = Math.Sqrt(Wx*Wx + Wy * Wy + Wz *Wz);
            Console.WriteLine("[Debuggger] : Angular Velocity is  " + anV);
            return anV;
            
        }

        
        
        /// <This function returns the current altitude>
        /// reads the current field  Alt_Field_ID and then retuns the value
        /// </summary>
        /// <returns></returns>
        public float getAltitude()
        {
            int valueA = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, 1054, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return -1;

            }
            else
            {
                float altitude = AutoPilotUnits.toAltitude(valueA);
                Console.WriteLine("[Debuggger] : Altitiude is  " + altitude);
                return altitude;
            }
            /*
            Random random = new Random();
            return (float) (random.NextDouble() * (20.0 - 0.0) +0.0);*/
        }

        /// <This function returns the current altitude>
        /// reads the current field  Long_Field_ID and then retuns the value
        /// </summary>
        /// <returns></returns>

        public double getLongitude()
        {
            int valueA = 0;
            int valueB = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, Long_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
           // if (GlobalData.modeSimulatorOrAuto == 0) 
                retVal = mpReadVar(uavId, or_Long_Field_ID, ref valueB, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
           // else 
           //valueB = AutoPilotUnits.uncompileGPSPositionFromAutoPilotUnits(INIData.longitude);
            if (retVal != MP_OK)
            {

                return -1;

            }

            double Longitude =AutoPilotUnits.toLongitude(valueB,valueA);
            Console.WriteLine("[Debuggger] : Longitude is  " + Longitude);
            return Longitude;
        }

        /// <This function returns the current altitude>
        /// reads the current field  Origin and then retuns the value
        /// </summary>
        /// <returns></returns>

        public Location getCurrentLocation()
        {
            int valueA = 0;
            int valueB = 0;
            int valueC = 0;
            int valueD = 0;
            int retVal = 0;
            double Longitude=0;
            double Lat=0;
           // if (GlobalData.modeSimulatorOrAuto == 0)
           // {
               // retVal = mpReadVar(uavId, 1065, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                
           // retVal = mpReadVar(uavId, 1066, ref valueB, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            retVal = mpReadVar(uavId, 1012, ref valueC, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            retVal = mpReadVar(uavId, 1013, ref valueD, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

                if (retVal != MP_OK)
                {

                    return null;

                }
                Location myLocation = AutoPilotUnits.toLocationEstimate(valueC,valueD);

                return myLocation;
        }

        public Location getOrigin()
        {
            int valueA = 0;
            int valueB = 0;
            int retVal = 0;
            double Longitude = 0;
            double Lat = 0;
            // if (GlobalData.modeSimulatorOrAuto == 0)
            // {
            retVal = mpReadVar(uavId, or_Long_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            retVal = mpReadVar(uavId, or_Lat_Field_ID, ref valueB, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return null;

            }

            Longitude = AutoPilotUnits.compileGPSPositionFromAutoPilotUnits(valueA);
            Lat = AutoPilotUnits.compileGPSPositionFromAutoPilotUnits(valueB);
            //}
            //     else { 
            // Longitude = INIData.longitude ;
            // Lat = INIData.latitude;
            //  }
            Console.WriteLine("[Debuggger] : Longitude is  " + Longitude);
            return new Location(Lat, Longitude);
        }

        /// <This function returns the current altitude>
        /// reads the current field  Lat_Field_ID and then retuns the value
        /// </summary>
        /// <returns></returns>
        /// 
        public double getLatitude()
        {
            int valueA = 0;
            int retVal = 0;
            int valueB = 0;
            retVal = mpReadVar(uavId, Lat_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
          //  if (GlobalData.modeSimulatorOrAuto == 0)
                retVal = mpReadVar(uavId, or_Lat_Field_ID, ref valueB, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
           // else
            //   valueB = AutoPilotUnits.uncompileGPSPositionFromAutoPilotUnits(INIData.latitude);
            if (retVal != MP_OK)
            {

                return -1;
            }
          
            double Latitude = AutoPilotUnits.toLatitude(valueB, valueA); 
            Console.WriteLine("[Debuggger] : Latitude is  " + Latitude);
            return Latitude;
            
        }
       
        /*----------------------------------------*/
        /// <summary>
        /// DuMMy Test 
        /// </summary>
        /// <returns></returns>
        public int getJoy(int fieldid)
        {
            int valueA = 0;
            int retVal = 0;
            // retVal = mpFly(uavId);
            retVal = mpReadVar(uavId, fieldid, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return -1;

            }

            Console.WriteLine("[Debuggger] : Joy Value is  is  " + valueA);
            return valueA;
        }
       /// <summary>
       /// start simulation of a failure pattern
       /// </summary>
       /// <param name="type"></param>
       /// <returns></returns>
        public int startFailurePattern(short type) {
           return mpStartFailure(uavId, type);
        
        }

        public int getCurrentFailurePattern() {

            return this.getJoy(1262);
        
        }
        /// <summary>
        /// start simulation of a failure pattern
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public int endFailurePattern(short type)
        {
            return mpEndFailure(uavId, type);

        }
        /*----------------------------------------*/
        /// <summary>
        /// Calculate the roll attitude 
        /// </summary>
        /// <returns></returns>
        public int  setJoy(int dField,int dValue)
        {
            return mpWriteVar(uavId, (int)(dField), dValue, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
             
        }
        
        
        /// <summary>
        /// Calculates the attitude of the airbone  
        /// </summary>
        /// <returns></returns>

       /*----------------------------------------*/
        /// <summary>
        /// Calculate the roll attitude 
        /// </summary>
        /// <returns></returns>
        public int getRoll()
        {
            int valueA = 0;
            int retVal = 0;
           // retVal = mpFly(uavId);
            retVal = mpReadVar(uavId, Roll_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return -1;

            }
             double s = valueA / 1024f;


             int Roll = AutoPilotUnits.toOrientation(valueA);
             Console.WriteLine("[Debuggger] : Roll is  " + Roll);
             return Roll;
        }


        /// <summary>
        /// Calculate the Pitch attitude 
        /// </summary>
        /// <returns></returns>
        public int getPitch()
        {
            int valueA = 0;
            int retVal = 0;
            // retVal = mpFly(uavId);
            retVal = mpReadVar(uavId, Pitch_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return -1;

            }
            double s = valueA / 1024f;

            int pitch = AutoPilotUnits.toOrientation(valueA); 
            Console.WriteLine("[Debuggger] : pitch is  " + pitch);
            return pitch;
        }

        
        /// <summary>
        /// Calculate the Yaw attitude 
        /// </summary>
        /// <returns></returns>
       
        public int getYaw()
        {
            int valueA = 0;
            int retVal = 0;
            // retVal = mpFly(uavId);
            retVal = mpReadVar(uavId, Yaw_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return -1;

            }

            else
            {
                int yaw = AutoPilotUnits.toOrientation(valueA);
                Console.WriteLine("[Debuggger] : yaw is  " + yaw);
                return yaw;
            }

            
        }

        /// <summary>
        /// Get The current Heading From Airbone  
        /// </summary>
        /// <returns></returns>
        
        public float getHeading()
        {
            
            int valueA = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, Heading_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return -1;

            }
            else
            {
                float heading = AutoPilotUnits.toHeading(valueA); ;
                Console.WriteLine("[Debuggger] : heading is  " + heading);
                return heading;
            }
             /*
            Random random = new Random();
            return (float)(random.NextDouble() * (20.0 - 0.0) + 0.0);*/
        }

      

        /// <summary>
        /// DONOT Have Existing Formula to calculate the Turn Rate 
        /// </summary>
        /// <returns></returns>
        /// 
        public float getTurnRate() {
            return 0; 
        }

       /// <summary>
       /// Calcultaes THe airspeed 
       /// </summary>
       /// <returns></returns>
        public float getAirSpeed()
        {
            int valueA = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, AirSpeed_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return -1;

            }
            else return AutoPilotUnits.toVelocity(valueA);
        }

        /// <summary>
        /// Is used to Obtain the Cruise speed.   
        /// </summary>
        /// <returns></returns>
        public float getCruiseSpeed()
        {
            int valueA = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, CruiseSpeed_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return -1;

            }
            else return AutoPilotUnits.toVelocity(valueA);
        }
        /// <summary>
        /// Is used to obtain the WindSpeed    
        /// </summary>
        /// <returns></returns>
        public float getWindSpeed()
        {
            int valueA = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, WindSpeed_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return -1;

            }
            else return AutoPilotUnits.toVelocity(valueA);
        }
        /// <summary>
        /// Is used to verify the Preflight Checks about both Wind Speed and cruise speed.   
        /// </summary>
        /// <returns></returns>
        public bool verifyBetweenAirCruiseSpeed()
        {
            float  windS = 0;
            float  cruises = 0;
            int retVal = 0;
            windS = getWindSpeed();
            cruises = getCruiseSpeed();
            if (windS == -1 || cruises == -1) {
                return false; 
            }
            if (retVal != MP_OK)
            {
                return false;
            }
            if (windS > (cruises / 2.0f)) {
                return false; 
            }
            return true; 
        }

        /// <summary>
        /// Change the current Flight Mode 
        /// </summary>
        /// <param name="newFlightMode"></param>
        /// <returns></returns>
        public bool setFlightMode(FlightMode newFlightMode) {

            int retVal = mpSetMode(uavId, (short)newFlightMode, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            if (retVal != MP_OK) {
                return false; 
            }
            return true;
        }

        /// <summary>
        /// Set the Pattern
        /// </summary>
        /// <param name="newFlightMode"></param>
        /// <returns></returns>
        public bool startPattern(Int16 pNo)
        {

            int retVal = mpStartPattern(uavId, pNo, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            if (retVal != MP_OK)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Send the Servos values to the UAV Item. 
        /// </summary>
        /// <param name="aileron"></param>
        /// <param name="elevator"></param>
        /// <param name="rudder"></param>
        /// <param name="throttle"></param>
        /// <returns></returns>
        public bool flyInPICMode(Int16 aileron, Int16 elevator, Int16 rudder, Int16 throttle)
        {
            // you have to change comm mode to QUICK_MODE 
            int retVal = mpStuffServos(uavId, aileron, elevator, rudder, throttle, GlobalData.modeSimulatorOrAuto, SQUICK_MODE);
            if (retVal != MP_OK)
            {
                return false;
            }
            return true;

        }


        /// <summary>
        /// Fly in RPV Mode.. 
        /// </summary>
        /// <param name="aileron"></param>
        /// <param name="elevator"></param>
        /// <param name="rudder"></param>
        /// <param name="throttle"></param>
        /// <returns></returns>
        public bool flyInRPVMode(int heading, int alti,int airSpeeed)
        {

            int retVal = mpWriteVar(uavId,Heading_Field_ID,heading,GlobalData.modeSimulatorOrAuto,NORMAL_MODE);
            retVal = mpWriteVar(uavId, Alt_Field_ID, alti, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            retVal = mpWriteVar(uavId, AirSpeed_Field_ID, airSpeeed, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
            if (retVal != MP_OK)
            {
                return false;
            }
            return true;


        }
        
            
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fieldList"></param>
        /// <returns></returns>
        public bool writeList(Dictionary<int, int> fieldList)
        {

            foreach (KeyValuePair<int, int> entry in fieldList.ToList())
            {
                int retVal = mpWriteVar(uavId, entry.Key, entry.Value, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

                if (retVal != MP_OK)
                {
                    return false;
                }
                
            }
            return true;

        }

        public GPSTime getGPSTime()
        {
            
            int valueA = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, GPS_TIME_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return null;

            }
            else return new GPSTime(valueA);                 
        }

        public String getConnectionStatues() {
            return "No Connection"; 
        }

        public FlightMode getCurrentMode() {
            
            int valueA = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, gcsMode_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return FlightMode.Error;

            }
            else
            {

                if (valueA == 0)
                         return FlightMode.UAV;
                else if (valueA == 1)
                         return FlightMode.RPV;
                else if (valueA == 2)
                    return FlightMode.PIC;
                else if (valueA == 4)
                    return FlightMode.FBC;
                else return FlightMode.Error;
            }
            
        }

        public bool checkLink() {

            try
            {
                int valueA = 0;
                int retVal = 0;
                retVal = mpReadVar(uavId, 1134, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

                if (retVal != MP_OK)
                {

                    return false;

                }
                else return true;
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                return false; 
            }
        }
        /// <summary>
        /// This functions is used to transmit a fly file to Report A Progress
        ///  
        /// </summary>
        /// <returns></returns>
        /// 


        public int transmitFLyFileOnGround(string fileName)
        {
            // Read All Fields


            int mperr = 0;
            short line = 0;
            short start = 0;
            int i = 0;

            if (!GlobalData.apConnected)
            {
                try
                {
                   /* mperr = mpCreate(uavId);
                    mperr = mpInitFly(uavId, null);
                    mperr = mpInitLink(uavId, INIData.compPort);*/
                    mperr = initializeTheLinkToAP(uavId, null);
                    if (mperr != MP_OK) return mperr;
                }
                catch (Exception e)
                {
                    GlobalData.LogConsole(e.Message);
                    return MP_NO_LINK;
                }
            }
            do
            {


                mperr = mpReadVarFile(uavId, fileName, ref line, (short)SKip_Parameters.SKIP_NONE);// Do not skip any Field.

                // Add code to update a progress bar etc.

                if (!(mperr == MP_OK || mperr == MP_MORE))
                {
                    GlobalData.LogConsole("There Were an Error in Reading A Fly File with Error Value " + mperr);
                    return mperr;
                }
                Console.Write("Value No {0} has been read ", i);
                i++;
            } while (mperr == MP_MORE);

            if (mperr == MP_OK)
            {
                // Succeed on reading fields to the Simulator so lets send them
                do
                {
                    mperr = mpTransmitVars(uavId, ref start, (short)SKip_Parameters.SKIP_PROG | (short)SKip_Parameters.SKIP_INVERT);

                    // Add code to update a progress bar etc.
                    if (!(mperr == MP_OK || mperr == MP_MORE))
                    {
                        GlobalData.LogConsole("There Were an Error in Transmitting a Fly with Error Value " + mperr);
                        return mperr;
                    }
                    Console.Write("Value No {0} has been read ", i);
                    i++;
                } while (mperr == MP_MORE);

                if (mperr != MP_OK)
                {
                    GlobalData.LogConsole("There Were an Error in Transmitting a Fly with Error Value " + mperr);
                    return mperr;
                }
            }
           
                mperr = mpSaveToFlash(uavId);
                if (mperr != MP_OK)
                {
                    GlobalData.LogConsole("Error While Saving cchanges into Flash " + mperr);
                    return mperr;
                }
            
           
            
            return mperr;

        }

        /// <summary>
        /// This functions is used to transmit a fly file to Report A Progress
        ///  flyFile : True --> Will Verify the fly File . 
        ///  flyFile : False --> Will Verify the vrs File . 
        ///  Must Be used with Link initialized to the AutoPilot.
        /// </summary>
        /// <returns></returns>
        /// 
        public int verifySimulatorAndAutoPilot(bool flyFile) {
            
            int mperr = 0;
            short start = 0;
            IntPtr reserved = (IntPtr)0;
            int i = 0;

            if (!GlobalData.apConnected)
            {
                try
                {
                    mperr = mpCreate(uavId);
                    mperr = mpInitLink(uavId,INIData.compPort);
                    mperr = mpInitFly(uavId, null);
                    mperr = mpSetLinkBaud(uavId, (short)INIData.baudRate);
                    if (mperr != MP_OK) return mperr;
                }
                catch (Exception e)
                {
                    GlobalData.LogConsole(e.Message);
                    return MP_NO_LINK;
                }
            }
            if (mperr == MP_OK)
            {
                // Succeed on reading fields to the Simulator so lets send them
                do
                {
                    mperr = mpVerifyVars(uavId, ref start, (short)SKip_Parameters.SKIP_PROG
                        | (short)SKip_Parameters.SKIP_INVERT, 0, reserved);

                    // Add code to update a progress bar etc.
                    if (!(mperr == MP_OK || mperr == MP_MORE))
                    {
                        GlobalData.LogConsole("There Were an Error in Verifying the Fly with Error Value " + mperr);
                        return mperr;
                    }
                    Console.Write("Value No {0} has been read ", i);
                    i++;
                } while (mperr == MP_MORE);

                if (mperr != MP_OK)
                {
                    GlobalData.LogConsole("There Were an Error in Verifying a Fly with Error Value " + mperr);
                    return mperr;
                }
            }
            return mperr; 
        }
        /// <summary>
        /// This functions is used to transmit a VRS file to Report A Progress
        ///  
        /// </summary>
        /// <returns></returns>
        /// 
   
      
        public int transmitVRSFileAndReportProgress(string fileName,bool saveToFlas,bool Simulator)
        {
            // Read All Fields
            
            
            int mperr = 0;
            short line = 0;
            short start = 0; 
            int i =0 ;

            if (!GlobalData.apConnected) {
                try
                {
                    mperr = mpCreate(uavId);
                    mperr = mpInitFly(uavId, null);
                    if (!Simulator)
                        mperr = mpInitLink(uavId, INIData.compPort);
                    mperr = mpSetLinkBaud(uavId, (short)INIData.baudRate);
                    if (mperr != MP_OK) return mperr;
                }
                catch(Exception e ){
                    GlobalData.LogConsole(e.Message);
                    return MP_NO_LINK; 
                }
            }
            do
            {
                

                 mperr = mpReadVarFile(uavId, fileName, ref line,(short) SKip_Parameters.SKIP_NONE);// Do not skip any Field.

                // Add code to update a progress bar etc.
                 if (Simulator) {
                     return mperr;
                 }
                if (!(mperr == MP_OK || mperr == MP_MORE))
                {
                    GlobalData.LogConsole("There Were an Error in Reading a VRS Field with Error Value "+mperr);
                    return mperr;
                }
                Console.Write("Value No {0} has been read ", i);
                i++;
            } while (mperr == MP_MORE);

            if (mperr == MP_OK)
            {
                // Succeed on reading fields to the Simulator so lets send them
                do
                {
                    mperr = mpTransmitVars(uavId, ref start, (short)SKip_Parameters.SKIP_PROG | (short)SKip_Parameters.SKIP_INCOMPATIBLE);

                    // Add code to update a progress bar etc.
                    if (!(mperr == MP_OK || mperr == MP_MORE))
                    {
                        GlobalData.LogConsole("There Were an Error in Transmitting a Field with Error Value " + mperr);
                        return mperr;
                    }
                     Console.Write("Value No {0} has been read ",i);
                      i++;
                } while (mperr == MP_MORE);

                if (mperr != MP_OK)
                {
                    GlobalData.LogConsole("There Were an Error in Transmitting a Field with Error Value " + mperr);
                    return mperr;
                }
            }
            if (saveToFlas)
            {
                mperr = mpSaveToFlash(uavId);
                if (mperr != MP_OK)
                {
                    GlobalData.LogConsole("Error While Saving cchanges into Flash " + mperr);
                    return mperr;
                }
            }
            return mperr;
               
        }
        /// <summary>
        /// This functions is used to Receive a VRS file From an AutoPiltot
        ///  
        /// </summary>
        /// <returns></returns>
        /// 


        public int GetVRsFromAutoPilot(string fileName)
        {
            // Read All Fields


            int mperr = 0;
            short start = 0;
            int i = 0;

            if (!GlobalData.apConnected)
            {
                try
                {
                    mperr = mpCreate(uavId);
                    mperr = mpInitFly(uavId, null);
                    mperr = mpInitLink(uavId, INIData.compPort);
                    mperr = mpSetLinkBaud(uavId, (short)INIData.baudRate);
                    if (mperr != MP_OK) return mperr;
                }
                catch (Exception e)
                {
                    GlobalData.LogConsole(e.Message);
                    return MP_NO_LINK;
                }
            }
         
                // Read All Fields
                do
                {
                    mperr = mpReceiveVars(1, ref start, (short)SKip_Parameters.SKIP_NONE);

                    // Add code to update a progress bar etc.

                    if (!(mperr == MP_OK || mperr == MP_MORE))
                    {
                        GlobalData.LogConsole("There Were an Error in Reading a VRS Field with Error Value " + mperr);
                        return mperr;
                    }
                    Console.Write("Value No {0} has been read ", i);
                    i++;
                } while (mperr == MP_MORE);

                
                 if(mperr == MP_OK)
                 {
                    // Succeed add processing code
                     GlobalData.LogConsole("Now Is writing to a file "+fileName);
                    mpWriteVarFile(1,fileName);
                  }
            
                if (mperr != MP_OK)
                {
                    GlobalData.LogConsole("There Were an Error in In Writing To The File  "+ fileName+" "  + mperr);
                    return mperr;
                }
            
            
            return mperr;

        }
       

        /// <summary>
        /// Functions To get The GPS Positions 
        /// Till now not used 
        /// </summary>
        /// <returns></returns>
        public float getGPSpostionN()
        {
            int valueA = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, GpsPosN_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return -1;

            }
            else return valueA;
        }


        public float getGPSpostionE()
        {
            int valueA = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, GpsPosE_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return -1;

            }
            else return valueA;
        }

        public float getGPSpostionU()
        {
            int valueA = 0;
            int retVal = 0;
            retVal = mpReadVar(uavId, GpsPosU_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

            if (retVal != MP_OK)
            {

                return -1;

            }
            else return valueA;
        }

        public float getGPSVelocityX()
        {
            return 0;
        }

        public float getGPSVelocityY()
        {
            return 0;
        }

        public float getGPSVelocityZ()
        {
            return 0;
        }

        public float getNetSpeed()
        {
            return rnd.Next(0, 60) / 1.0f;
        }

        public int getAglHeight()
        {
            return this.getJoy(1089);
        }

     public String getCurrentFailureAsString (ref Color myColor)
     
     {

         int failureId = this.getCurrentFailurePattern();
         myColor = Color.Red;
         if (failureId == 255 || failureId == 0)
         {
             myColor = Color.Green;
             return "None";
         }
         else if (failureId == 0)
         {
             return "Control Failure";
         }
         else if (failureId == 1)
         {
             return "Fatal Error Occured";
         }
         else if (failureId == 2)
         {
             return "GPS Signal Lost";
         }
         else if (failureId == 3)
         {
             return "Lossing Engine Power";
         }
         else if (failureId == 4)
         {
             return "Low battery voltage";
         }
         else if (failureId == 5)
         {
             return "RC Link Failure";
         }
         else if (failureId == 5)
         {
             return "GCS Communication Failure";
         }
         return "None";
     }

     public bool servo_test(short servo1_position, short servo2_position, short servo3_position, short servo4_position)
     {

         int retVal = mpStuffServos(uavId, servo1_position, servo2_position, servo3_position, servo4_position, GlobalData.modeSimulatorOrAuto, SQUICK_MODE);
    
            if (retVal != MP_OK)
            {
                return false;
            }
            return true;    

     }

 

    public bool servo_test2(short servo1_position, short servo2_position, short servo3_position, short servo4_position)
         {

             int valueA = 0;
             int retVal = 0;
             retVal = mpReadVar(uavId, SERVOTYPE_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
             if (retVal != MP_OK)
             {
                 return false;
             }
             else
             {
                  switch (valueA)
                  {
                      case 0:
                          mpWriteVar(uavId, SERVO1_Field_ID, servo1_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO2_Field_ID, servo2_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO3_Field_ID, servo3_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO4_Field_ID, servo4_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          break;

                      case 1:
                          mpWriteVar(uavId, SERVO1_Field_ID, servo1_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO2_Field_ID, servo2_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO3_Field_ID, servo3_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO4_Field_ID, servo4_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          break;
                      case 2:
                          mpWriteVar(uavId, SERVO1_Field_ID, servo1_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO2_Field_ID, servo2_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO3_Field_ID, servo3_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO4_Field_ID, servo4_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          break;
                      case 3:
                          mpWriteVar(uavId, SERVO1_Field_ID, servo1_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO2_Field_ID, servo2_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO3_Field_ID, servo3_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO4_Field_ID, servo4_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          break;
                      case 4:
                          mpWriteVar(uavId, SERVO1_Field_ID, servo1_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO2_Field_ID, servo2_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO3_Field_ID, servo3_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO4_Field_ID, servo4_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          break;

                      case 5:
                          mpWriteVar(uavId, SERVO1_Field_ID, servo1_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO2_Field_ID, servo2_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO3_Field_ID, servo3_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO4_Field_ID, servo4_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          break;
                      case 6:
                          mpWriteVar(uavId, SERVO1_Field_ID, servo1_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO2_Field_ID, servo2_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO3_Field_ID, servo3_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          mpWriteVar(uavId, SERVO4_Field_ID, servo4_position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                          break;

                  }


                  return true;
             }
         }


       
       private int servo1_test(int position )
            {
                int retVal;

                        
              //  retVal = mpWriteVar(uavId, cSERVO1_Field_ID,position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);

                retVal = mpWriteVar(uavId, cSERVO1_Field_ID, position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                return retVal;
            }
       private int servo2_test(int position)
       {
           int retVal;
           retVal = mpWriteVar(uavId, cSERVO2_Field_ID, position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
           return retVal;
       }
       private int servo3_test(int position)
       {
           int retVal;
           retVal = mpWriteVar(uavId, cSERVO3_Field_ID, position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
           return retVal;
       }
       private int servo4_test(int position)
       {
           int retVal;
           retVal = mpWriteVar(uavId, cSERVO4_Field_ID, position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
           return retVal;
       }
       private int servo5_test(int position)
       {
           int retVal;
           retVal = mpWriteVar(uavId, cSERVO5_Field_ID, position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
           return retVal;
       }
       private int servo6_test(int position)
       {
           int retVal;
           retVal = mpWriteVar(uavId, cSERVO6_Field_ID, position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
           return retVal;
       }

       private int servo7_test(int position)
       {
           int retVal;
           retVal = mpWriteVar(uavId, cSERVO7_Field_ID, position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
           return retVal;
       }
       private int servo8_test(int position)
       {
           int retVal;
           retVal = mpWriteVar(uavId, cSERVO8_Field_ID, position, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
           return retVal;
       } 
       // add the fuction that select the servo 
       /// <summary>
       /// Not Used Now (There are a redunduncy check 
       /// Written by Ahmed Samy.
       /// Eliminated by Tariq s.shatat
       /// </summary>
       /// <param name="position"></param>
       /// <param name="type"></param>
       /// <returns></returns>
       public bool change_servo_(int position,int type)
       {


           int valueA = 0;
           int retVal = 0;
           retVal = mpReadVar(uavId, SERVOTYPE_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
           if (retVal != MP_OK)
           {
               return false;
           }
           else
           {
               switch (valueA)
               {
                   case 0:
                       {
                           #region 
                           switch (type)
                          {
                              case 1:
                                  servo1_test(position);
                                  break;
                              case 2:
                                  servo2_test(position);
                                  break;
                              case 3:
                                  servo3_test(position);
                                  break;
                              case 4:
                                  servo4_test(position);
                                  break;

                          }



                       }
                    #endregion 
                     
                       break;

                   case 1:
                       {
                           #region
                           switch (type)
                           {
                               case 1:
                                   servo1_test(position);
                                   break;
                               case 2:
                                   servo2_test(position);
                                   break;
                               case 3:
                                   servo3_test(position);
                                   break;

                               case 4:
                                   servo4_test(position);
                                   break;
                               case 5:
                                   servo5_test(position);
                                   break;
                             

                           }



                       }
                           #endregion 
                       break;
                   case 2:
                       {
                           switch (type)
                           {
                               case 1:
                                   servo1_test(position);
                                   break;
                               case 2:
                                   servo2_test(position);
                                   break;
                               case 3:
                                   servo3_test(position);
                                   break;

                               case 4:
                                   servo4_test(position);
                                   break;
                              
                               case 6:
                                   servo6_test(position);
                                   break;
                            

                           }



                       }
                       break;
                   case 3:
                       {
                           #region
                           switch (type)
                           {
                               case 1:
                                   servo1_test(position);
                                   break;
                               case 2:
                                   servo2_test(position);
                                   break;
                               case 3:
                                   servo3_test(position);
                                   break;

                               case 4:
                                   servo4_test(position);
                                   break;
                               case 5:
                                   servo5_test(position);
                                   break;
                               case 6:
                                   servo6_test(position);
                                   break;
                               case 7:
                                   servo7_test(position);
                                   break;


                           }



                       }
               #endregion 
                       break;
                   case 4:
                       {
                           #region
                           switch (type)
                           {
                               case 1:
                                   servo1_test(position);
                                   break;
                               case 2:
                                   servo2_test(position);
                                   break;
                               case 3:
                                   servo3_test(position);
                                   break;
                                  
                               case 4:
                                   servo4_test(position);
                                   break;
                            

                           }



                       }
#endregion
                       break;

                   case 5:
                       {
                           switch (type)
                           {
                               case 1:
                                   servo1_test(position);
                                   break;
                               case 2:
                                   servo2_test(position);
                                   break;
                               case 3:
                                   servo3_test(position);
                                   break;

                               case 4:
                                   servo4_test(position);
                                   break;
                               
                               case 6:
                                   servo6_test(position);
                                   break;
                           

                           }

                       }
                       break;
                   case 6:
                        {
                          switch(type)
                          {
                              case 1:
                                  servo1_test(position);
                                  break;
                              case 2:
                                  servo2_test(position);
                                  break;
                              case 3:
                                  servo3_test(position);
                                  break;

                              case 4:
                                  servo4_test(position);
                                  break;
                          

                          }



                       }
                       break;
                   case 7:
                       {
                           switch (type)
                           {
                               case 1:
                                   servo1_test(position);
                                   break;
                               case 2:
                                   servo2_test(position);
                                   break;
                               case 3:
                                   servo3_test(position);
                                   break;
                               case 4:
                                   servo4_test(position);
                                   break;


                           }



                       }
                       break;

               }


               return true;
           }
       }
       /// <summary>
       /// Used function 
       /// </summary>
       /// <param name="position"></param>
       /// <param name="type"></param>
       /// <returns></returns>
       public bool change_servo(int position, int type)
       {

          
           int valueA = 0;
           int retVal = 0;
           
          
           retVal = mpReadVar(uavId, SERVOTYPE_Field_ID, ref valueA, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
           if (retVal != MP_OK)
           {
               return false;
           }
           else
           {
                switch (type)
                {
                    case 1:
                        servo1_test(position);
                        break;
                    case 2:
                        servo2_test(position);
                        break;
                    case 3:
                        servo3_test(position);
                        break;
                    case 4:
                        servo4_test(position);
                        break;
                    case 5:
                        servo5_test(position);
                        break;
                    case 6:
                        servo6_test(position);
                         break;
                    case 7:
                        servo7_test(position);
                        break;
                    case 8:
                        servo8_test(position);
                        break;

                }

               return true;
           }
       }

       public int disableAutoCalculationForServos() {

           return SetVariable(uavId, dontCalcServos, 1, 0);
       
       }

       public int enableAutoCalculationForServos()
       {

           return SetVariable(uavId, dontCalcServos,0, 0);

       }
       public int connect_servoform( ref int value ,bool alreadyConnected)
       {
           int retVal =-1;
           if (!GlobalData.apConnected)
           {
               try
               {
                  if(!alreadyConnected)
                   retVal = initializeTheLinkToAP(uavId, null);

                   retVal= mpReadVar(uavId, SERVOTYPE_Field_ID, ref value, 0 , NORMAL_MODE);
                   retVal = disableAutoCalculationForServos();

                   if (retVal != MP_OK) 
                       return retVal;
               }
               catch (Exception e)
               {
                   GlobalData.LogConsole(e.Message);
                   return retVal;
               }
           }
               return retVal;
       }


       public int set_value_variable(int value, int dField )
       {
           int retVal;
          retVal= SetVariable(uavId, (int)(dField), value, GlobalData.modeSimulatorOrAuto);

       // For future using 
       // retVal = UAV.SetVariable(uavId, (int)(dField), value, NORMAL_MODE);
           return retVal;
       }
       public int save_flash()
       {

           int mperr = 0;
           mperr = mpSaveToFlash(uavId);
           if (mperr != MP_OK)
           {
               GlobalData.LogConsole("Error While Saving cchanges into Flash " + mperr);
               return mperr;
           }

           return mperr;



       }
       public int read_value(ref int value, int dField)
       {

           int retVal = 0;
           retVal = mpReadVar(uavId, dField, ref value, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
           if (retVal != MP_OK)
           {
               GlobalData.LogConsole("Error While reading value  " + retVal);
               return retVal;

           }
           return retVal;
       }
        /* May need to be transformed to a flight control class */ 
     public int orbitight() { return  0; }
        public int orbitLeft () { return  0; }
        public int takePicture(){return 0;}
        public int descentHere(){return 0 ;} 
        public int hoverHere(){return 0 ;}
        public int circleRight(){return 0 ;}
        public int circleLeft(){return 0 ;}
        public int flyHome(){return 0 ;}
        public int stopEngine() {return 0 ;}
        public int landAtHome(){return 0 ;}
        public int landHere() { return 0 ; }


        public String getErrorString(Int32 ErrorCode)
        {
            byte[] errorBuffer = new byte[256];
           
            mpGetErrorString(uavId, ErrorCode, errorBuffer, 255);
            String error = System.Text.Encoding.Default.GetString(errorBuffer);
            return error;
        }

       /*Tariq Shatat Get the status*/
        public Int32 checkSimStatus(ref String ErrorMSG)
        {
            Int32 val = mpCheckFly(uavId);
            if (val == MP_MORE || val == MP_OK)
            {
                ErrorMSG = "Simulation is Still Running";
                return MP_MORE;
            }
            else if (val == MP_SIM_OVER)
            {
                ErrorMSG = "Simulation is Over";
                return MP_SIM_OVER;
            }
            else {
                ErrorMSG = getErrorString(val);
                return val; 
            } 
        }      
       

    }
}
