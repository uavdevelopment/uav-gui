﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using  System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Threading;



namespace MP_Lib
{

    public partial class UAV
    {
        /*! @file mperrors.h

Project  : MicroPilot XTENDER

Copyright (c) 2007  MicroPilot

@brief    Error codes returned by MicroPilot XTENDER

****************************************************************************/

        /* error and warning codes returned by original simdll and autopilot */
        public static int MP_OK = 0;    //!< Command successfully completed
        public static int MP_BAD_VAR_ID = 1; //!< Invalid field identifier 
        public static int MP_VAR_READ_ONLY = 2; //!< Field is read only 
        public static int MP_CANT_SAVE_VAR = 3; //!< Field cannot be written public static int o eeprom/flash 
        public static int MP_CANT_CHANGE_VAR_INFLIGHT = 4; //!< Field cannot be changed while in flight 
        public static int MP_CANT_SAVE_VAR_INFLIGHT = 5; //!< Field cannot be written public static int o eeprom/flash while in flight 
        public static int MP_VAR_ID_TOO_LARGE = 6; //!< Field identifier is too large 
        public static int MP_INVALID_EVENT = 7; //!< Invalid Event***
        public static int MP_NO_WAITING_THREAD = 8; //!< Thread is not Waiting 
        public static int MP_INVALID_MODE = 9; //!< Invalid flight mode 
        public static int MP_SIMULATOR_NOT_INIT = 10; //!< The simulator has not been initialized 
        public static int MP_NO_LINK = 11; //!< No response has been received
        public static int MP_INVALID_CMD_INFLIGHT = 12; //!< Command cannot execute while autopilot is in flight
        public static int MP_INVALID_THREAD = 13; //!< Invalid thread specified as a parameter 
        public static int MP_INVALID_COMMAND_INDEX = 14; //!< Command specified does not exist 
        public static int MP_INVALID_PARMS = 15; //!< Invalid command data 
        public static int MP_INVALID_CMD_TYPE = 16; //!< Invalid command buffer modification 
        public static int MP_FILE_ERROR = 17; //!< Unable to open file
        public static int MP_SYNTAX_ERROR = 18; //!< Syntax error encountered while reading file 
        public static int MP_SIM_ERROR = 19; //!< Unable to load fly file in simulator 
        public static int MP_SIM_OVER = 20; //!< Simulator run has completed 
        public static int MP_SIM_CRASH = 21; //!< Simulation crashed
        public static int MP_COM_INUSE = 22; //!< COM port In Use or Not Found
        public static int MP_COM_FAULT = 23; //!< COM port initialization error
        public static int MP_MORE = 24; //!< More data available or processing required, call again
        public static int MP_TOO_MANY_VARS = 25; //!< Attempt to Queue up too many variables
        public static int MP_COM_FAULT_DCB = 26; //!< COM port DCB error
        public static int MP_COM_FAULT_TIMEOUTS = 27; //!< COM port Set Com Timeouts error
        public static int MP_COM_FAULT_SEM = 28; //!< COM port semaphore error
        public static int MP_INVALID_PATTERN = 29; //!< Pattern has not been defined 
        public static int MP_NESTED_PATTERN = 30; //!< Pattern is already executing, cannot start another one
        public static int MP_CANT_PROG_INFLIGHT = 31; //!< AutoPilot is not currently set for in flight re-programming (no fixed command)
        public static int MP_INVALID_MP1 = 32;
        public static int MP_CODE_FAILURE = 33; //!< AutoPilot code transfer error
        public static int MP_CRC_ERROR = 34; //!< The CRC check calculation failed
        public static int MP_VERIFY_DIFFERENCE = 35; //!< The value transmitted was different that the value on the autopilot
        public static int MP_STUCK_IN_RPV_MODE = 36; //!< Autopilot is stuck in RPV mode***
        public static int MP_HWSIM_OVER = 37; //!< Quasi hardware in the loop has completed
        public static int MP_HWSIM_CRASH = 38; //!< Quasi hardware in the loop has crashed
        public static int MP_BUCOPYCMDFIXEDDONE = 39; //!< Copy of cmd buffer complete***
        public static int MP_CANT_EXIT_FAILURE_PATTERN = 40; //!< Cannot exit the failure pattern

        // n;ew error codes not part of the original simdll
        public static int MPERR_RPV_MODE_NO_FAILURE_PATTERN = 41; //!< RPV mode error: no GCS failure pattern in cmd buffer
        public static int MPERR_RPV_MODE_NOT_OFF_GROUND = 42; //!< RPV mode error: not off the ground
        public static int MPERR_RPV_MODE_ALTITUDE_TOO_LOW = 43; //!< RPV mode error: altitude too low
        public static int MPERR_FLY_FILE_CHANGED_VRS = 44; //!< Fly file changed a vrs entry, cannot save to flash

        public static int MP_SIM_CRASH_LGEAR_FAILED = 45; //!< Simulation crashed; landing gear failure
        public static int MP_SIM_CRASH_HIT_GROUND = 46; //!< Simulation crashed; impact with ground
        public static int MP_SIM_INIT_NO_PLANE_TYPE = 47; //!< Simulation failed to init; aircraft definition not found
        public static int MP_SIM_INIT_NO_EQUILIBRIUM = 48; //!< Simulation failed to init; equilibrium not possible
        public static int MPERR_RPV_MODE_IN_CCT = 49; //!< RPV mode error: cannot engage during circuit
        public static int MPERR_RPV_MODE_IN_FLARE = 50; //!< RPV mode error: cannot engage during flare
        public static int MPERR_RPV_MODE_IN_APPROACH = 51; //!< RPV mode error: cannot engage during approach

        // UAV ownership error codes (must be < 256 b/c they can be sent back from autopilot)
        public static int MPERR_UAV_IS_ALREADY_OWNED = 52; //!< UAV is already owned by another GCS
        public static int MPERR_GCS_IS_NOT_OWNER = 53; //!< UAV must be owned by this GCS

        public static int MPERR_NOT_INITIALIZED = 54; //!< Autopilot is not initialized
        public static int MPERR_GCSCMDSDISABLED = 55; //!< Commands from Gcs are disabled
        public static int MPERR_MPMODEDISABLED = 56; //!< Autopilot mode is disabled
        public static int MPERR_RPV_MODE_NO_FAILURE_PATTERN_TIMEOUT = 57; //!< No Timout for GCS failure pattern
        public static int MPERR_NOT_AVAILABLE_ON_HELI = 58; //!< Feature not available on mp2128heli
        public static int MPERR_UNUSED_FATAL_ERROR = 59; //!< Fatal error is no longer used
        public static int MPERR_FIELD_ACCESS_VIOLATION = 60; //!< Field is protected, write and/or read is blocked
        public static int MP_CANT_CHANGE_CMD = 61; //!< Command cannot be changed
        public static int MP_PATTERN_ID_TOO_LARGE = 62; //!< Pattern id is too large
        public static int MP_CODE_MISMATCH = 63; //!< Code mismatch when trying to flash code
        public static int MPERR_NOFLYCMDSTRANSFERRED = 64; //!< No fly files have been transferred recently
        public static int MPERR_FAKE_GPS_AFTER_INIT = 65; //!< Cannot fake GPS after initializing

        public static int MPERR_FLASH_LOADER_COMMAND = 66; //!< Cannot implement FlashLoader command when flashing code
        public static int MPERR_MP3X_BUFFER_FULL = 67; //!< Buffer to send packets from engaged board to other mp3x boards is full

        public static int MPERR_WAYPOINTVERSION_MISMATCH = 68; //!< Cannot insert a command if the waypopublic static int  version doesn't match the waypopublic static int  version on the autopilot
        public static int MPERR_CMDBUF_FULL = 69; //!< The command buffer is full and there is not enough room the insert another command
        public static int MPERR_CANT_DELETE_CURRENT_CMD = 70; //!< The current command in the command buffer can not be deleted
        public static int MPERR_INVALID_NUMCMDS_TO_DELETE = 71; //!< The number of commands to delete must be greater than 0 and less than or equal to 1000
        public static int MPERR_MP3X_COMM_ERROR = 72; //!< There was a comm. error between the 3x boards

        public static int MPERR_SAVE2FLASH_COMMAND = 73; //!< Cannot implement SavetoFlash command

        public static int MPERR_INVALID_DURING_CCT = 74; //!< Operation is invalid during the current stage of a circuit command

        public static int MPERR_BIND_CMD_INVALID = 75; //!< Binding command is invalid
        public static int MPERR_BIND_ENTRY_DOES_NOT_EXIST = 76; //!< The binding entry does not exist
        public static int MPERR_BIND_GCS_ID_ALREADY_EXISTS = 77; //!< An entry with a matching GCS ID already exists
        public static int MPERR_BIND_MAC_ALREADY_EXISTS = 78; //!< An entry with a matching MAC already exists
        public static int MPERR_BIND_KEY_MISMATCH = 79; //!< Bind key doesn't match this autopilot
        public static int MPERR_BIND_TABLE_FULL = 80; //!< Binding table is full
        public static int MPERR_BIND_AUTOPILOT_READY = 81; //!< Can't bind when autopilot is ready
        public static int MPERR_BIND_DISABLED = 82; //!< Can't bind when binding is disabled

        public static int MPERR_CANNOT_ARM_DURING_FATAL_ERROR = 83; //!< Can't arm autopilot when a fatal error is present
        public static int MPERR_CMD_LEADS_TO_INVALID_PATH = 84; //!< Command specified will lead to an invalid fly file command if executed

        public static int MPERR_DFN_FILE_OPEN_FAILED = 100; //!< Failed to open aircraft definition file
        public static int MPERR_DFN_FILE_PARSE_FAILED = 101; //!< Error parsing aircraft definition file
        public static int MPERR_BIN_FILE_TOO_LARGE = 102; //!< The .binfile provided is too large for the autopilot model

        public static int MP_NO_DATA = 110; //!< A check for data was successful, but there was no data to return

        public static int MP_ERR_WPT_MODIFY_DISTANCE_VIOLATION = 120; //!< Waypopublic static int  modification failed because the new location would place it beyond the maximum flight range
        public static int MP_ERR_WPT_MODIFY_FENCE_VIOLATION = 121; //!< Waypopublic static int  modification failed because the new location would place it outside of the geofence
        public static int MP_ERR_WPT_MODIFY_ALTITUDE_VIOLATION = 122; //!< Waypopublic static int  modification failed because the new altitude is above the defined airspace ceiling



        /*error for protocol disabled, 1 build for all types of autopilot*/
        public static int MPERR_PROTOCOL_DISABLED = 350; //!< Protocol is disabled because autopilot type does not allow it.
        /* Simulation errors */
        public static int MP_SIM_CRASH_PROP_HIT_CAT = 400; //!< The propellor hit the catapult and caused the UAV to crash

        /* added MP_API_ERR_* group to fix bugs and improve API error handling */
        public static int MP_API_FAILED = 1000; //!< Unknown API failure***
        public static int MP_API_BAD_PARAM = 1001; //!< Bad API parameter or data
        public static int MP_API_NOT_IMPLEMENTED = 1002; //!< API not implemented or supported
        public static int MP_API_INVALID_DURING_SIM = 1003; //!< API can't be called whilst running a simulation

        /* UTM/LL conversion Errors */
        public static int MPERR_UTM_LAT_RANGE = 1100; //!< UTM conversion failed; latitude not in range

        /* Camera Projection Errors */
        public static int MPERR_CAM_PROJ_RANGE = 1200; //!< Camera projection error; popublic static int  not on ground

        /* warning codes returns by new simdll routines */
        public static int MPWARN_UAV_ID_ZERO = 2000; //!< UAV was assigned ID= 0 
        public static int MPWARN_UAV_EQUAL = 2001; //!< UAV ID# already set***
        public static int MPWARN_SOCKET_ALREADY_OPEN = 2002; //!< TCP/IP Socket is already open***
        public static int MPWARN_TX_SIZE_MAX = 2003; //!< file size is > than 384K not all mp2028's can handle this

        /* error codes returns by new simdll routines */
        public static int MPERR_UAV_ID_NEGATIVE = 3000; //!< Negative UAV ID# not supported
        public static int MPERR_UAV_CANNOT_CHANGE = 3001; //!< UAV ID# cannot change at this time
        public static int MPERR_SOCKET_OPEN = 3002; //!< Failed to open TCP/IP socket or connect to server
        public static int MPERR_SOCKET_CLOSE = 3003; //!< Expected TCP/IP socket to be open
        public static int MPERR_SOCKET_SEND = 3004; //!< Error sending data on TCP/IP socket
        public static int MPERR_SOCKET_RECEIVE = 3005; //!< Error or timed-out receiving data on TCP/IP socket
        public static int MPERR_QUEUE_NO_TLS = 3006; //!< Queue not allocated for current thread
        public static int MPERR_SOCKET_IS_CLOSED = 3007; //!< TCP/IP server is closed
        public static int MPERR_TCP_SERVER_PORTS_IN_USE = 3008; //!< The desired port for the TCP/IP server, and all backup ports, have listeners on them
        public static int MPERR_TCP_CLIENT_NOT_VALIDATED = 3009; //!< The client connection could not validate it was talking to a running UAV TCP/IP server
        public static int MPERR_ENCRYPTED_VRS_NOT_LOCKED = 3010; //!< The VRS File is not locked and therefore can not be used.
        public static int MPERR_COM_PORT_NOT_OPEN = 3011; //!< The Serial Port is not currently open
        public static int MPERR_TCP_SERVER_UAV_ID_MISMATCH = 3012; //!< The UAV ID that is trying to connect is not connecting to the correct UAV ID (verify that multiple UAVs do not use the same TCP Server port)

        /* warning codes returns by new multi uav dll routines */
        public static int MPWARN_MUAV_DUPLICATE_INIT = 3030; //!< Multi-UAV sub-system already initialized

        /* error codes returns by new multi uav dll routines */
        public static int MPERR_MUAV_UNKNOWN_UAV = 3070; //!< An unknown UAV identifier is being used
        public static int MPERR_MUAV_DUPLICATE_UAV = 3071; //!< UAV created already
        public static int MPERR_MUAV_FAIL_UAV = 3072; //!< UAV is in use by another program and could not be created here
        public static int MPERR_MUAV_FAIL_INIT = 3073; //!< Failed to initialize the Multi-UAV sub-system
        public static int MPERR_UAV_DLL_NOT_FOUND = 3074; //!< Could not find UAV DLL in the program's working directory
        public static int MPERR_UAV_DLL_LOAD_ERR = 3075; //!< Error loading UAV DLL
        public static int MPERR_UAV_DLL_VERSION = 3076; //!< Version of UAV DLL does not match Multi-UAV subsystem version
        public static int MPERR_UAV_MODEL_INCOMPATIBLE = 3077; //!< UAV model can not connect to this version of UAV DLL
        public static int MPERR_SIM_NOT_AVAILABLE = 3078; //!< Simulator is not available in this version of UAV DLL
        public static int MPERR_REPLAY_ACTIVE = 3079; //!< Simulator is in replay mode so many commands will return this error code
        public static int MPERR_UAV_DLL_REQUIRES_ADMIN_PRIV = 3080; //!< UAV DLL requires administrator privileges in order to work
        public static int MPERR_FLY_FILE_CONTAINS_INVALID_PATH = 3081; //!< The Fly File that is being loaded contains a command that can lead to an invalid fly file command fatal error

        // VIDEO subsystem
        public static int VIDEO_OK = 3101; //!< The video operation was successfully completed***
        public static int VIDEO_FAIL = 3102; //!< Failed to load video-subsystem
        public static int VIDEO_NO_STREAM = 3103; //!< Specified video stream index invalid or doesn't exist
        public static int VIDEO_NO_SNAPSHOT = 3104; //!< Specified video snapshot index invalid or doesn't exist
        public static int VIDEO_NO_CAPTURE_DEVICE = 3105; //!< Video sub-system failed to find any capture devices
        public static int VIDEO_NO_CAPTURE_DEVICE2 = 3106; //!< Video sub-system could not find audio capture devices
        public static int VIDEO_IN_PROGRESS = 3107; //!< Video snapshot capture is in progress
        public static int VIDEO_GRAPH_FAIL = 3108; //!< Video sub-system failed to create graph
        public static int VIDEO_FAIL_START = 3109; //!< Video sub-system failed to start graph
        public static int VIDEO_FAIL_RUN = 3110; //!< Video sub-system failed to run graph
        public static int VIDEO_FAIL_INIT = 3111; //!< Video sub-system failed to init
        public static int VIDEO_FAIL_FILTER = 3112; //!< Video sub-system failed to init one or more filter components
        public static int VIDEO_NOT_RUNNING = 3113; //!< Video sub-system is not initialized or running
        public static int VIDEO_IN_TRANSITION = 3114; //!< Video sub-system is in transition
        public static int VIDEO_NO_RESPONSE = 3115; //!< No response was received from Video sub-system

        // DEM subsystem
        public static int DEM_ERROR = 3201; //!< Failed to load DEM data set file; init or file error
        public static int DEM_NOT_INITIALIZED = 3202; //!< DEM sub-system not initialized
        public static int DEM_FILE_NOT_FOUND = 3203; //!< DEM file not found
        public static int DEM_FILE_ERROR = 3204; //!< General DEM file error
        public static int DEM_NO_ELEVATION_DATA = 3205; //!< No DEM elevation data available in file
        public static int DEM_UNKNOWN_ELEVATION = 3206; //!< Indeterminate DEM elevation
        public static int DEM_LATLON_OUTSIDE_OF_MAP = 3207; //!< DEM lat/lon location not within loaded map region
        public static int DEM_MAP_ALREADY_LOADED = 3208; //!< DEM warning; map data already loaded
        public static int DEM_TOO_MANY_FILES = 3209; //!< DEM file limit exceeded
        public static int DEM_REQUIRES_WGS84 = 3210; //!< DEM files must use the WGS84 datum and latitude/longitude projection


        // Hardware In The Loop error codes
        public static int MP_HW_INITHIFREQ = 4000; //!< Quasi hardware in the loop simulator failed to find hi-res system timer
        public static int MP_HW_ISSUE_TAKEOFF = 4001; //!< Quasi hardware in the loop simulator failed to issue takeoff command

        // License Manager returns codes
        public static int MPERR_AUTHORIZATION_FAILED = 4100; //!< Software license not valid for product or feature
        public static int MPERR_LICENSE_INIT_FAIL = 4101; //!< Software license failed to initialize
        public static int MPERR_LICENSE_OVERRIDE_FILE_DOES_NOT_EXIST = 4102; //!< Software license override file does not exist
        public static int MPERR_LICENSE_OVERRIDE_INVALID = 4103; //!< Software license override is invalid
        public static int MPERR_LICENSE_OVERRIDE_UNABLE_TO_READ_INFO = 4104; //!< Software license override failed due to a communication error with the autopilot
        public static int MPERR_LICENSE_OVERRIDE_INCOMPATIBLE = 4105; //!< Software license override is not compatible with this autopilot

        /* plugin-related error codes */
        public static int MPERR_PLUGIN_FAIL_MP1K = 4200;

        // MicroPilot application errors and run-time exceptions
        public static int MPERR_APP_UNKNOWN = 5000; //!< Unhandled application error
        public static int MPERR_APP_FATAL = 5001; //!< Fatal application error
        public static int MPERR_APP_UAVID_BAD = 5002; //!< Application does not support specified UAV ID#
        public static int MPERR_APP_CANCELLED_BY_USER = 5003; //!< Operation or command was canceled by the user

        // reserved for future use
        public static int MPERR_NO_PICTURE = 6000; //!< Unexpected error code***
        public static int MPERR_CREATE_WINDOW = 6001; //!< Unexpected error code***

        //fail on reading def.bin or aircraft.dfn or an initialization
        public static int MPERR_AIRCRAFT_DFN = 7000; //!< Could not load def.bin or aircraft.dfn
        public static int MPERR_CRAFT_NOT_FOUND = 7001; //!< Could not find aircraft ID in file def.bin or aircraft.dfn
        public static int MPERR_INITIALIZATION_FAILED = 7002; //!< Failed to initialize simulator

        // failed to load the simulator DLL
        public static int MPERR_LOADSIM = 7100; //!< Failed to load the simulator sub-system
        public static int MPERR_LOADSIM_NOT_FOUND = 7101; //!< Simulator sub-system file could not be found
        public static int MPERR_LOADSIM_COPY_ERR = 7102; //!< Simulator sub-system file is in use
        public static int MPERR_LOADSIM_BAD_EXE = 7103; //!< Simulator sub-system file is corrupted

        public static int MPERR_LOADSIM_BAD_DELTA_T = 7110; //!< Simulator sub-system only supports the default time step of 1/30s

        public static int MPERR_LOADSIM_BAD_VEHICLE_TYPE = 7120; //!< The selected aircraft definition does not match the simulator type

        public static int MPERR_LOADSIM_BAD_3X_SIM_TYPE = 7130; //!< The mp3x simulation must use the mp2128Heli simulator type.  See Horizon Settings Sim tab.

        // Command buffer and range check thread error codes
        public static int MPERR_FAIL_INIT_THREAD = 7200; //!< Failed to initialize command buffer thread
        public static int MPERR_FAIL_START_THREAD = 7201; //!< Failed to start command buffer thread
        public static int MPERR_FAIL_STOP_THREAD = 7202; //!< Failed to stop command buffer thread
        public static int MPERR_FAIL_SHUTDOWN_THREAD = 7203; //!< Failed to shutdown command buffer thread
        public static int MPERR_FAIL_THREAD_NOT_INIT = 7204; //!< Command buffer thread not initialized
        public static int MPERR_FAIL_THREAD_NO_DATA = 7205; //!< Command buffer thread not started
        public static int MPERR_FAIL_INVALID_CMD_TYPE = 7206; //!< Command buffer Invalid type

        // Redundant RC communication error codes
        public static int MP_COM_INUSE_2 = 7300; //!< Com port for redundant RC in use or not found

        // Datalog Error Codes
        public static int MP_DATALOG_END_REACHED = 7400; //!< End of Datalog has been reached
        public static int MP_DATALOG_BUFFER_SIZE_ERROR = 7401; //!< Error with buffer, Increase buffer Size
        public static int MP_DATALOG_VERSION_INVALID = 7402; //!< Datalog Version #'s Do Not Match
        public static int MPERR_DATALOG_DOWNLOAD_CONFIG_INVALID = 7403; //!< The MPDATALOGDOWNLOADCONFIG struct passed to mpDownloadDatalog is NULL or invalid
        public static int MPERR_DATALOG_DOWNLOAD_FAILED_TO_OPEN_FILE = 7404;
        public static int MPERR_DATALOG_DOWNLOAD_VERSION_NOT_SUPPORTED = 7405;
        public static int MPERR_DATALOG_DOWNLOAD_CANCELLED = 7406;

        // Block 7500 - 7599 reserved for mpmapinfo dll (mpmapinfo.h)

        //Create MP Plugin Error Codes
        public static int MPERR_CREATEMPPLUGIN_AP_FILENAME_INVALID = 7600; //!< Invalid Autopilot Code Filename provided
        public static int MPERR_CREATEMPPLUGIN_PLUGIN_FILENAME_INVALID = 7601; //!< Invalid MP Plugin Code Filename provided
        public static int MPERR_CREATEMPPLUGIN_OUTPUT_FILENAME_INVALID = 7602; //!< Invalid Output Filename provided
        public static int MPERR_CREATEMPPLUGIN_AP_FILE_ERROR = 7603; //!< Autopilot Code File Error
        public static int MPERR_CREATEMPPLUGIN_PLUGIN_FILE_ERROR = 7604; //!< MP Plugin Code File Error
        public static int MPERR_CREATEMPPLUGIN_OUTPUT_FILE_ERROR = 7605; //!< Output Code File Error
        public static int MPERR_CREATEMPPLUGIN_CREATION_FAILED = 7606; //!< MP Plugin Creation Failed
        //License Override Tool Error Codes
        public static int MPERR_LICENSEOVERRIDE_FILE_ERROR = 7650; //!< error reading/writing the license override file
        public static int MPERR_LICENSEOVERRIDE_ENTRY_ALREADY_EXISTS = 7651; //!< entry already exists in the license override file

        public static int MPERR_INISTRING_INVALID = 7700;
        public static int MPERR_SEUCRE_INI_NOT_FOUND = 7701;

        public static int MPERR_MP_UPDATE_VERSION_MISMATCH = 7801; //!< .mpupdate file was created with a newer version
        public static int MPERR_MP_UPDATE_CRC_MISMATCH = 7802; //!< .mpupdate file CRC error
        public static int MPERR_MP_UPDATE_MUST_BE_ON_GROUND = 7803; //!< .mpudate files must be applied when the autopilot is on the ground
        public static int MPERR_MP_UPDATE_NEW_CRC_TIMEOUT = 7804; //!< .mpupdate file was transmitted but timed out while waiting for new CRC to be read from autopilot

        public static int MPERR_3X_PARENT_UAV_ID_NOT_FOUND = 7900; //!< public static int ernal error - individual mp3x board could not determine parent UAV #
        /// @cond
        public static int MP_ERROR_ALL = 32766; //!< General public static int ernal error, all boards failed
        public static int MP_ERROR = 32767; //!< General internal error

    }
       
}
