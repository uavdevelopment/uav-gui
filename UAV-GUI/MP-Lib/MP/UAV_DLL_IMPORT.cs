﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using  System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Threading;



namespace MP_Lib
{
        [StructLayout(LayoutKind.Explicit, CharSet = CharSet.Ansi, Pack = 4)]
        public struct MPSTDTELEMETRYDATA
        {
            [FieldOffset(0)]
            public int cbSize;  /*!< @brief ALWAYS init cbSize to the sizeof() this struct before passing to any API */
            [FieldOffset(4)]
            /* original standard telemetry */
            public int speed;
            /*!< @brief Speed (ft/s)				*/
            [FieldOffset(8)]
            public int gpsSpeed;   /*!< @brief GPS speed (ft/s)			*/
            /* ALWAYS use e and n together */
            [FieldOffset(12)]
            public int eTemp;
            [FieldOffset(16)]
            public int e;
            [FieldOffset(20)]
            public int n;
            [FieldOffset(24)]/*!< @brief GPS Latitude (Radians*500000000 - public integer scaled radians) */
            public int alt;
            /*!< @brief Altitude (-8*ft - public integer scaled negative feet) */
            [FieldOffset(28)]
            public int altDot;     /*!< @brief Altitude 1st derivative (-8*ft/s - public integer scaled negative feet per second ) */
            [FieldOffset(32)]
            public int hdg;        /*!< @brief Heading (degrees*100 - public integer scaled degrees) 0..360 degrees */
            [FieldOffset(36)]
            public int err;        /*!< @brief Last fatal error code, or 0xff for low battery (see @ref F_MPOK "Fatal errors") */
            [FieldOffset(40)]
            public int status;     /*!< @brief Status bit fields.  See @ref STAT_GPS_MASK "Status bit field" */
            [FieldOffset(44)]
            public int status2;    /*!< @brief Status bit fields.  See @ref STAT2_OFF_GROUND "Status 2 bit field" */
            [FieldOffset(48)]
            public int batV;       /*!< @brief Main battery voltage (Volts*100 - public integer scaled Volts) */
            [FieldOffset(52)]
            public int sbatV;      /*!< @brief Servo battery voltage (Volts*100 - public integer scaled Volts) */
            [FieldOffset(56)]
            public int sTh;        /*!< @brief Servo throttle position. (FINE-SERVO units mapped to 0..255) */
            [FieldOffset(60)]
            public int tlmPitch;   /*!< @brief Autopilot Pitch (Radians*1024 - public integer scaled radians) */
            [FieldOffset(64)]
            public int tlmRoll;    /*!< @brief Autopilot Roll (Radians*1024 - public integer scaled radians) */
            /* extended standard telemetry data */
            [FieldOffset(68)]
            public int ipStep;
            /*!< @brief Instruction popublic inter position */
            [FieldOffset(72)]
            public int ipCmd;              /*!< @brief Instruction being executed */
            [FieldOffset(76)]
            public int patternId;          /*!< @brief Pattern invoked if applicable */
            [FieldOffset(80)]
            public int failureId;          /*!< @brief Failure pattern invoked if applicable. See @ref FAILURE_CONTROL "Control Failures" */
            [FieldOffset(84)]
            public int targetSpeed_fps;    /*!< @brief Target speed (ft/s - feet per second) */
            [FieldOffset(88)]
            public int targetAlt_ft;       /*!< @brief Target altitude (-8*ft - public integer scaled negative feet) */
            [FieldOffset(92)]
            public int targetHeading_deg;  /*!< @brief Target heading (degrees*100 - public integer scaled degrees) 0..360 degrees */
            [FieldOffset(96)]

            public int waypointversion;   /*!< @brief Waypopublic int Version (incremented on waypopublic int move) */
            [FieldOffset(100)]
            public int mEvent;             /*!< @brief Event warning or non-fatal error status from autopilot */

            //HH:2005-12-13:extended standard telemetry again (GCS-225)
            [FieldOffset(104)]
            public int disableNewOriginSet;    /*!< @brief Disable pattern origin movement for certain command conditions*/

            //TM:2006-06-13:extend standard telemetry
            [FieldOffset(108)]
            public int ownerGcsId;         /*!< @brief GCS id of the owner of this UAV ( 0 = no owner ) */
            [FieldOffset(112)]
            //TM:2006-06-30:extend standard telemetry again
            public int gpsAlt;             /*!< @brief GPS altitude of the plane (-8*ft - public integer scaled negative feet) */
            [FieldOffset(116)]
            public int mpMode;    /*!< @brief Mode of the autopilot, shifted left by 4 bits.  See @ref GCS_UAV "GCS Mode" */
            [FieldOffset(120)]
            public int lrcStatus; /*!< @brief Status of the LRC.  See @ref LRC_STATUS_PIC_FAIL "LRC Status Byte" */
            [FieldOffset(124)]
            public int mp3xStatus; /*!< @brief mp2128 3X board status. See @ref STAT_3X_BOARD_MASK "MP3X Status Word" */
            [FieldOffset(128)]
            public int mp3xPitchLeft;  /*!< @brief mp2128 3X left board pitch (rad*1024) */
            [FieldOffset(132)]
            public int mp3xPitchRight; /*!< @brief mp2128 3X right board pitch (rad*1024) */
            [FieldOffset(136)]
            public int mp3xRollLeft;   /*!< @brief mp2128 3X left board roll (rad*1024) */
            [FieldOffset(140)]
            public int mp3xRollRight;  /*!< @brief mp2128 3X right board roll (rad*1024) */
            [FieldOffset(144)]
            public int warning; /*!< @brief warning error code (see @ref W_MPOK "Warnings") */


        } ;

    public partial class UAV
    {

       

        [DllImport("multi-uav.dll", CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpCreate(int PlaneId);


        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpInitFly(int PlaneId, string filename);

        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpInitLink(int PlaneId, string port);
        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpSetLinkBaud(int PlaneId, int baud);
        // Three comm mode available
        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern int mpReadVar(int PlaneId, int varId, ref int result, short mode, short commmode);

        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpDelete(int PlaneId);
        // Three comm mode available
        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpWriteVar(int PlaneId, int varId, int data, short mode, short commmode);

        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpCloseLink(int PlaneId);

        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpFly(int PlaneId);
                       // QUICK_MODE or NORMAL_MODE
        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpResponseStuffServos(int PlaneId, short servo0, short servo1, short servo2, short servo3,
                      ref short pitch, ref short roll, short mode, short commmode);

        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpGetStandardTelemetry(int PlaneId, ref MPSTDTELEMETRYDATA pStdTlmData);

        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpStartFly(int PlaneId);

        //NORMAL_MODE: Use this mode. It is the only valid mode for this command
        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpSetMode(int PlaneId,Int16 newMode,short mode , short comm_mode );
        // commode must be SQUICK_MODE
        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpStuffServos(int PlaneId, Int16 s0, Int16 s1, Int16 s2, Int16 s3, short mode, short comm_mode);
        //NORMAL_MODE: Use this mode. It is the only valid mode for this command
        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpStartPattern(int PlaneId, Int16 patNo, short mode, short comm_mode);

        
         [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpReadVarFile(int PlaneId, string file, ref short line, short skip);

        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpTransmitVars(int PlaneId, ref short start, int skip);
       
        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpSaveToFlash(int PlaneId);

        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpReceiveVars(int PlaneId,ref short start,short skip);

          [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpWriteVarFile(int PlaneId,string file);

          [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
          [return: MarshalAs(UnmanagedType.I4)]
          public static extern Int32 mpVerifyVars(int PlaneId,ref short start,short skip, int flags,IntPtr reserved);
       
        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
          [return: MarshalAs(UnmanagedType.I4)]
          public static extern Int32 mpStartFailure(int PlaneId, short type);

        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpEndFailure(int PlaneId, short type);
        //commode must be SQUICK_MODE

        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpStartTelemtry(int PlaneId);
        
        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpCheckFly(int PlaneId);
       
        [DllImport("multi-uav.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.I4)]
        public static extern Int32 mpGetErrorString(Int32 PlaneId, Int32 inMpErr, byte [] pErrStr, Int32 size);
    
    
    }
       
}
