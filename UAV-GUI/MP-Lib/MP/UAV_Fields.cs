﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using  System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Threading;
using FileClasses;



namespace MP_Lib
{

    public enum SKip_Parameters { 
        SKIP_NONE=   0,
        SKIP_PROG   =1,
        SKIP_SERVO =  2,
        SKIP_SIM  = 4,
    SKIP_INFLIGHT_PROG  = 8,
 SKIP_SENSORS  = 16,
        SKIP_INVALID_CMDS  = 32,
        SKIP_INCOMPATIBLE  = 64,
 SKIP_INVERT =  128
    }
    public partial class UAV
    {
        static int MPFID_TAKEOFF_STATE = 1115;

        static int MPFID_ARMED = 1937; //!< armed
        static short AUTO_MODE = 0;	//!< Talk to Autopilot 
        static short SIM_MODE = 1;	//!< Talk to Simulator
        static short NO_COMM_MODE = 42;	//!< Processes the command but does not talk to Autopilot or Simulator
        static short UNKNOWN_MODE = (-5555);     //!< used to tell methods to use global mode setting. 

        //! @name Communication modes for Read/Writes
        static short NORMAL_MODE = 0;  //!< Regular communication.  Receive a response.
        static short QUICK_MODE = (-1);   //!< Faster communication.  Doesn't wait for a response. Can fail without letting user know.
        static short SQUICK_MODE = (-2);

        static int TIMETORUN = 1;

        static int MPFID_ENABLE_TELEMETRY = 116;

        const short ENABLETELEMETRY = 1;

        static int MPFID_TEMP_FAKE_GPS = 1273;

        const short ENABLETEMPFAKEGPS = 1;
        static int MPFID_KOUNTER = 1063;

        /// State Fields that would be used for monitoring The flight
        /// Author : Tariq Shatat 
        /// ME-roboVics Inc
        /// 


        const int Roll_dot_Field_ID = 1404;
        const int Pitch_dot_Field_ID = 1403;
        const int Yaw_dot_Field_ID = 1405;

        const int Roll_Field_ID = 1059;
        const int Pitch_Field_ID = 1057;
        const int Yaw_Field_ID = 1060;
        const int Heading_Field_ID = 1053;
        const int GrSpeed_Field_ID = 1064;

        const int DistToNXTWP_Field_ID = 1508;
        const int AirSpeed_Field_ID = 1055;
        const int GPS_TIME_Field_ID = 1224;

        const int CruiseSpeed_Field_ID = 111;
        const int WindSpeed_Field_ID = 1407;

        const int GpsPosN_Field_ID = 1095;
        const int GpsPosE_Field_ID = 1096;
        const int GpsPosU_Field_ID = 1097;

        const int Alt_Field_ID = 1054;
        const int Lat_Field_ID = 1013;
        const int Long_Field_ID = 1012;

        const int or_Lat_Field_ID = 1066;
        const int or_Long_Field_ID = 1065;


        const int gcsMode_Field_ID = 2206;

        const int tmpFGPS_Field_ID = 1273;
        const int GPSOK_Field_ID = 1139;
        /// <summary>
        /// ids fields for servos
        /// </summary>
        const int SERVOTYPE_Field_ID = 99;
        const int SERVO1_Field_ID = 1227;
        const int SERVO2_Field_ID = 1228;
        const int SERVO3_Field_ID = 1229;
        const int SERVO4_Field_ID = 1230;
        const int SERVO5_Field_ID = 1231;
        const int SERVO6_Field_ID = 1232;
        const int SERVO7_Field_ID = 1233;
        const int SERVO8_Field_ID = 1234;

  

        // coarse servo field 
        public const int cSERVO1_Field_ID = 1158;
        const int cSERVO2_Field_ID = 1159;
        const int cSERVO3_Field_ID = 1160;
        const int cSERVO4_Field_ID = 1161;
        const int cSERVO5_Field_ID = 1162;
        const int cSERVO6_Field_ID = 1163;
        const int cSERVO7_Field_ID = 1164;
        const int cSERVO8_Field_ID = 1165;

        const int aileron_Field_ID = 1023;
        const int throttle_Field_ID = 1024;
        const int rudder_Field_ID = 1022;
        const int elevator_Field_ID = 1021;
        const int dontCalcServos = 1145;
       
    }
       
}
