﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using  System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Threading;
using FileClasses.INI_Files;
using GUtilities;
using GLobal;


namespace MP_Lib
{

    public partial class UAV
    {
        /**************************************************************************/
        /*!
            @param planeId             [in]        the Id number of the plane

            @return                    long , error code

            @brief
            This function demonstrates the use of mpCreate(), mpInitFly(), mpInitLink()
        **************************************************************************/


        static int CreatePlane(int planeId,String filename,short mode)
        {
            int retVal = 0;
            int value = 0;
            bool exit = false;
            /* Do...while loop is used so we can break out of them if there is an error */
            do
            {
                /* This Function will create the uav#.dll */

                retVal = initializeTheLinkToAP(planeId, filename);
                if (retVal != MP_OK)
                {
                    break;
                }


                
                /* This Function will initialize the simulate and load the default.fly */
                if (retVal != MP_OK)
                {
                    System.Windows.Forms.MessageBox.Show("" + retVal);
                }
                /*This function will Initialize the comport to the planeId */
                if (GlobalData.modeSimulatorOrAuto == 0)
                { // for UAV 
                    retVal = mpInitLink(planeId, INIData.compPort); /* by leaving comport blank "" we tell it to use the entry from the uav<planeId>.ini */
                    if (retVal != 0)
                    {
                        GlobalData.LogConsole(" ERROR - could not Initialize Link To Comport Error: " + retVal);
                        break;
                    }
                }

                retVal = CheckInitialize(planeId,mode);

            } while (exit);

            return retVal;

        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="planeId"></param>
       /// <returns></returns>
        
        static Int32 CheckInitialize(int planeId,short mode)
        {
            long retVal = 24;
            int value = 0;
            long readCount = 0;
            long p1 = 1036;

            /* Even though in our .ini file there is a read try timeout and attempts, we are doing a bit extra here, just to attempt
            to make sure the autopilot is initialized */
            while ((readCount < 10) && (retVal != 0))
            {
                ++readCount;
                GlobalData.LogConsole(". Checking To See if Board Is Initialized"+ readCount);
                retVal = mpReadVar(planeId, 1036, ref value, mode, NORMAL_MODE);
                GlobalData.LogConsole("\n "+value );
                if (retVal != 0)
                {
                    GlobalData.LogConsole(".......failed Error: "+ retVal);
                    Thread.Sleep(100);
                }  
                else
                {
                    GlobalData.LogConsole(".......passed");
                }
            }

            if (retVal != 0)
            {
                GlobalData.LogConsole("Failed, Board is Not Initialized");
                return (int)retVal;
            }
            else
            {
                GlobalData.LogConsole("Success, Board is Initialized");
                return 0;
            }

        }


        static int SetVariable(int planeId, int mpfieldId, int desiredValue, short mode)
        {
            int retVal = 0;
            int varValue = 0;

            retVal = mpReadVar(planeId, (int)(mpfieldId), ref varValue, mode, NORMAL_MODE);
            if (retVal != MP_NO_LINK)
            {
                Thread.Sleep(1000);
                if (varValue != desiredValue)
                {
                    GlobalData.LogConsole(" Setting MpFieldId: "+ mpfieldId +"to "  + desiredValue);
                    retVal = mpWriteVar(planeId, (int)(mpfieldId), desiredValue, mode, NORMAL_MODE);
                    if (retVal == MP_OK)
                    {
                        GlobalData.LogConsole(".....passed");
                    }
                    else if (retVal == MP_NO_LINK)
                    {
                        GlobalData.LogConsole(".....failed");
                        GlobalData.LogConsole("Error - No Link To the Board");
                    }
                    else
                    {
                        GlobalData.LogConsole(".....failed");
                        GlobalData.LogConsole("Error - Unknown");
                    }
                }
                else
                {
                    GlobalData.LogConsole(".....passed");
                }
            }
            else
            {
                GlobalData.LogConsole("Error - No Link To the Board");
            }
            return retVal;
        }

        static int  InitializeFlyPlane(int planeId, short mode)
        {
            int retVal = 0;
            int value = 0;
           
            /* Do...while loop is used so we can break out of them if there is an error */
            do
            {
                /* Since we dont want the simulator to run in a background thread we use mpFly to start it */
                GlobalData.LogConsole("\n Attempting to Start Flying");
                //retVal = mpFly(planeId);
                if (GlobalData.modeSimulatorOrAuto == 0) // For AP
                {
                    retVal = MP_OK;
                }
                else
                {
                    retVal = mpStartFly(planeId);
                }
                if (retVal != MP_OK)
                {
                    GlobalData.LogConsole("\n Error : mpFly, retVal:"+ retVal);
                    break;
                }

               
                /* retVal != MP_OK is used becuse the last thing we try to do is set the takeoff field to 2
                   if that is good then we can exit */
            } while (retVal != MP_OK);

            return retVal;
        }

        static int FlyPlane(int planeId, short mode)
        {

            int retVal = 0;
            int progressCount = 0;
            int timeIndex = 0;
            MPSTDTELEMETRYDATA pStdTlmData = new MPSTDTELEMETRYDATA();
            short pitch = 0;
            short roll = 0;
            int value = 0;

            pStdTlmData.cbSize = 148;// System.Runtime.InteropServices.Marshal.SizeOf(pStdTlmData);
            GlobalData.LogConsole("\n"+pStdTlmData.cbSize);

            GlobalData.LogConsole("\nFlying");
            
            
            do
            {
                /* Check the take off state, if it is equl to 1 then we are ready to take off, set it to 2 */
                retVal = (int)mpReadVar(planeId, MPFID_TAKEOFF_STATE, ref value, mode, NORMAL_MODE);
                if (retVal != MP_OK)
                {
                    GlobalData.LogConsole("\n Error : mpReadVar, retVal:" + retVal);
                    break;
                }
                if (value == 1)
                {
                    /* set takeoff state to "2" to initiate takeoff */
                    retVal = mpWriteVar(planeId, MPFID_ARMED, 1, SIM_MODE, QUICK_MODE);
                    retVal = mpWriteVar(planeId, MPFID_TAKEOFF_STATE, 2, mode, QUICK_MODE);
                    if (retVal != MP_OK)
                    {
                        GlobalData.LogConsole("\n Error : mpWriteVar, retVal:" + retVal);
                        break;
                    }
                    GlobalData.LogConsole(".......TakeOff Initiated!");
                }
                else
                {
                    GlobalData.LogConsole(".......TakeOff Failed");
                    retVal = MP_BAD_VAR_ID;
                }
                //retVal = mpFly(planeId);

                if (retVal != MP_OK)
                {
                    GlobalData.LogConsole("\n Error : mpFly, retVal:"+ retVal);
                    break;
                }
                 } while (retVal != MP_OK);
            do
            {
                /* becuase this is the simulator we have to include this additional step to get std telemetry */
                /* becuase this is the simulator we have to include this additional step to get std telemetry */
                retVal = mpResponseStuffServos(planeId, 0, 0, 0, 0, ref pitch, ref roll, GlobalData.modeSimulatorOrAuto, NORMAL_MODE);
                retVal = mpGetStandardTelemetry(planeId, ref pStdTlmData);
                Console.Write("\rStatus: {0} Pitch: {1}\tRoll: {2}\tGPSN: {3} GPSE:{4}",
                     pStdTlmData.status, pStdTlmData.tlmPitch, pStdTlmData.tlmRoll, pStdTlmData.n, timeIndex);
               
                /*int val = 0;
                retVal = mpReadVar(planeId, 1057, ref val, mode, NORMAL_MODE);

                GlobalData.LogConsole( "\n Status: " + retVal + " Pitch: "+pStdTlmData.tlmPitch +" "+ val+  "\tRoll: "+ pStdTlmData.tlmRoll +" \tGPSN: "+pStdTlmData.n+" GPSE: "+ pStdTlmData.e);
               */ progressCount++;
                timeIndex++;
            } while (timeIndex < TIMETORUN);

            return retVal;
        }






    }
       
}
