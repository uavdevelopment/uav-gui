﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMap.NET.WindowsForms.Markers;
using System.Threading.Tasks;

namespace MasterClasses
{
    public enum positionType { absolute, relative } ;
    public class WayPoint
    {

     public GMarkerGoogle marker;
     public double longt;
     public double lat;
    public List<Pattern> patternList;
     
     public positionType wpType;
     public  List<Command> commandsList;
     public bool manualAddedWP = false;

     public WayPoint()
     {
         commandsList = new List<Command>();
         patternList = new List<Pattern>();
     }
     public WayPoint(double longt_, double lat_)
     {
         longt = longt_;
         lat = lat_;
         commandsList = new List<Command>();
         patternList = new List<Pattern>();
     }

     public WayPoint(double longt_, double lat_, positionType wpType, List<Command> commandsList)
     {
         longt = longt_;
         lat = lat_;
         this.wpType = wpType;
         this.commandsList = commandsList;
         patternList = new List<Pattern>();
     }
        void convertToAbsoulute(WayPoint origin, ref WayPoint relativeWP)
        {
            // this is done by tariq
        }

        public void print()
        {
            Console.WriteLine("1.txt", longt + "---" + lat + "\n");
            Console.WriteLine();
        }
        
        /// ///////////////////////////////////////////
        /// 
        //Old waypoint class

        //   public Location wpLocation;
        //public WayPoint(double longt_, double lat_, positionType wpType_ , Location wpLocation_)
        //   {
        //       longt = longt_;
        //       lat = lat_;
        //       this.wpType = wpType_; 
        //      this.wpLocation = wpLocation_;
        //   }
        //public Location  getWpLocation (){
            
        //    return this.wpLocation;
        //}

        //public Location getWpLocation(WayPoint refWp_ )
        //{
        //    if (refWp_.getWpType() == positionType.absolute) {
        //        return this.wpLocation;
        //    }

        //    return wpLocation + refWp_.getWpLocation();
        //}
        //public positionType getWpType()
        //{
        //    return this.wpType;
        //}
        
    }
}
