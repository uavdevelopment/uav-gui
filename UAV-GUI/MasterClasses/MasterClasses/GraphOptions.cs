﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using ZedGraph;

namespace MasterClasses
{
    public class GraphOptions
    {
        public ZedGraphControl z1;
        public List<PointPairList> list = new List<PointPairList>();
        public int num_Graphs;
        public List<String> tags = new List<String>(); 

        public GraphOptions() {}

        public GraphOptions(ZedGraphControl z1_, List<PointPairList> list_,int num_Graphs_,List<String> tags_){
            z1 = z1_;
            list = list_;
            num_Graphs = num_Graphs_;
            tags = tags_;
        }
                        
        public void addPointToCurve (int index , PointPair point,Color color,String text){
            list[index].Add(point);
            foreach (CurveItem  n in z1.GraphPane.CurveList ){
                if (n.Label.Text.Equals(text)) {
                    n.AddPoint(point);
                    n.Color = color;
                    break;
                }
            }
            //z1.GraphPane.AddCurve(text, list[index], color, SymbolType.Star);
            z1.AxisChange();
            z1.Invalidate();
        }
        public void initialize()
        {
            z1.GraphPane.CurveList.Clear();
            z1.IsShowPointValues = true;
            z1.GraphPane.Title.Text = "Visual Monitoring";
            double[] x = new double[100];
            double[] y = new double[100];
            double[] yy = new double[100];
            int i;
            for (i = 0; i < 100; i++)
            {
                x[i] = 0;
                y[i] = Math.Sin(x[i]);
                yy[i] = 0;
            }
            //z1.GraphPane.AddCurve ( "Sine Wave", x, y, Color.Red, SymbolType.Square );
            for (int n = 0; n < num_Graphs; n++)
            {
                list.Add(new PointPairList());

                z1.GraphPane.AddCurve(tags[n], x, y, Color.Red, SymbolType.Default);            
            }
            z1.AxisChange();
            z1.Invalidate();
            
           
        }
        
    }
}
