﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterClasses
{
    public class Location
    {
        private double latitude;

        public double Latitude
        {
            get { return latitude; }
            set { latitude = value; }
        }
        private double longitude;

        public double Longitude
        {
            get { return longitude; }
            set { longitude = value; }
        }
        private double altitude;

        public double Altitude
        {
            get { return altitude; }
            set { altitude = value; }
        }

        public Location(double alt_, double lat_, double lngt_)
        {
            this.altitude = alt_;
            this.latitude = lat_;
            this.longitude = lngt_;
        }
        public Location( double lat_, double lngt_)
        {
            
            this.latitude = lat_;
            this.longitude = lngt_;
        }

        public static Location operator +(Location c1, Location c2)
        {
            return new Location(c1.Altitude + c2.Altitude, c1.Latitude + c2.Latitude, c1.Longitude + c2.Longitude);
        }
    }
}
