﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasterClasses
{
   public static class FieldsDefinitions
    {
       public static WayPoint absLoc = null;
       public static WayPoint relLoc = null;
       public static WayPoint rwyLoc = null;
       public static WayPoint prevWpt = null;
       public static WayPoint nextWpt = null;
       public static WayPoint home = null;
       public static WayPoint gcsHomeGPSPos = null;
       public static WayPoint firstWptOffset = null;
    }
}
