﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterClasses
{
    public enum CommandType
    {
        basic, Advanced, Calculation, Thread_Pattern, Skip, Wait, Skip_WaitHeadingFields 

    };

       public enum CommandName
    {
        //Unit Conversions
        metric,
        imperial,
        defaultUnits,
        //Basic : 
        Takeoff ,
        flyTo,
        climb,
        waitclimb,
        pclimb,
        fromTo,
        Initfromto,
        waitfromto,
        approach,
        flare ,
           hoverAt,
        // Advanced :
        circuit,
        circleLeft,
        circleRight,
        buildRelativeWaypoint,
        rotateRelativeWaypoint,
        setcontrol,
        setorigin,
        turn,
        // Calculation Commands
        push,
        pop,
        add,
        sub,
        div,
        mul,
        Assignment ,
        // Thread and Pattern :
        fixed_,
        thread,
        stop ,
        wake ,
        int_,
        int_Negative1,
        definePattern, pattern,  // definepattern & pattern are the same command
        startpattern ,
        switchPattern ,
        repeat ,
        return_,
        dim ,
        save,
        //Skip commands 
        Skipequal ,
        Skipnotequal ,
        skipgreaterthan ,
        skiplessthan,
        skipInRange ,
        skipOutRange ,
       // Wait Commands 
        wait  ,
        waitequal ,
        Waitnotequal ,
        waitgreaterthan ,
        Waitlessthan ,
        waitInRange ,
        waitOutRange ,
        // 7- Skip & Wait Commands for Heading Fields 
        skipHdgInRange ,
        skipHdgOutRange ,
        waitHdgInRange ,
        waitHdgOutRange ,
           //
        fatalErrorFailed ,
        UnParsing

    };
     public struct commandArgs_toValidate
       {
        public CommandName name;
        public int maxArgs;
        public int minArgs;
         public commandArgs_toValidate(CommandName n , int min , int max)
         {
             name = n;
           maxArgs = max;
           minArgs = min;
         }
       }
    
    public class Command
    {
      public List<commandArgs_toValidate> CommandsArgsValidationList;
      public CommandType type;
      public    CommandName name;
      public  List<double> CommandArgs;
      public string comd;

        public Command()
        {
            standerdCommandsArgs();
        }
        public Command(CommandType type, CommandName name, List<double> CommandArgs)
        {
            this.type = type;
            this.name = name;
            this.CommandArgs = CommandArgs;
            //standerdCommandsArgs();
        }

        public Command(string cmd)
        {
            this.name = CommandName.UnParsing;
            comd = String.Copy(cmd);
        }

        public void standerdCommandsArgs()
        {
            CommandsArgsValidationList = new List<commandArgs_toValidate>();
          //  CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.flyTo, 2, 2));
            // flyTo can be flyTo [home]
            CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.Takeoff, 0, 0));
            //CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.climb, 1, 2));
            // climb can be like this climb [currentAltitude]
            CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.waitclimb, 1, 1));
            CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.pclimb, 1, 1));
          //  CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.fromTo, 2, 4));
           // CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.Initfromto, 2, 4));
            // intfromto can be initFromTo [home]
            CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.waitfromto, 0, 0));
            CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.circuit, 0, 4));
            CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.approach, 2, 4));
            CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.flare, 0, 0));
            CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.circleLeft, 1, 6));
            CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.circleRight, 1, 6));
            CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.startpattern, 1, 1));
            //CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.definePattern, 1, 1));
            //define pattern can be like this definePattern rcFailed
            CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.thread, 1, 1));
            CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.fixed_, 0, 0));
            CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.repeat, 0, 1));
            CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.return_, 0, 0));
         //   CommandsArgsValidationList.Add(new commandArgs_toValidate(CommandName.hoverAt, 2, 2));
            //hoverAt can be like this hoverAt [home]
        }
    }
}
