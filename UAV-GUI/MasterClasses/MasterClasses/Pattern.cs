﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterClasses
{
    public class Pattern
    {
        public int pattern_num;
        public int WP_Index;
        public  List<Command> patternCommands;
        public WayPoint WP_On_Map;

        public Pattern() { 
        }
      public Pattern(int pattern_num, int WP_Index)
        {
            this.pattern_num = pattern_num;
            this.WP_Index = WP_Index;
            this.patternCommands = new List<Command>();
            WP_On_Map = new WayPoint();
        }
    }
}
