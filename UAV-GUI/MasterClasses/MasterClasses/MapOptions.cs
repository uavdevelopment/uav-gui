﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using System.Drawing;
using System.Windows.Forms;

namespace MasterClasses
{
  public static  class MapOptions
    {

   public static WayPoint addWP(PointLatLng p, ref GMapRoute route, ref List<GMapRoute> routeList
        , ref GMapOverlay markersOverlay, ref List<GMapMarker> markersList, ref GMapControl gmap)
    {
           
            GMarkerGoogle nMarker = new GMarkerGoogle(p, GMarkerGoogleType.blue_small);
            markersOverlay.Markers.Add(nMarker);
            markersList.Add(nMarker);
            route.Points.Add(p);
            routeList.Add(route);
            gmap.UpdateRouteLocalPosition(route);
            route.Stroke.Color = System.Drawing.Color.Green;
            route.Stroke.Width = 3;
            WayPoint n = new WayPoint(p.Lng, p.Lat);
            n.marker = nMarker;
            return n;
        }
   public static WayPoint addWP(PointLatLng p, ref GMapPolygon route, ref List<GMapPolygon> routeList
    , ref GMapOverlay markersOverlay, ref List<GMapMarker> markersList, ref GMapControl gmap)
   {

       GMarkerGoogle nMarker = new GMarkerGoogle(p, GMarkerGoogleType.blue_small);
       markersOverlay.Markers.Add(nMarker);
       markersList.Add(nMarker);
       route.Points.Add(p);
       routeList.Add(route);
       gmap.UpdatePolygonLocalPosition(route);
       route.Stroke.Color = System.Drawing.Color.Green;
       route.Stroke.Width = 3;
       WayPoint n = new WayPoint(p.Lng, p.Lat);
       n.marker = nMarker;
       return n;
   }

   public static void Drag(ref GMapRoute route, ref GMapMarker selectedMarker, PointLatLng point, ref GMapControl gmap)
        {
      // if(route.Points.Count !=0)
            route.Points[route.Points.IndexOf(selectedMarker.Position)] = point;
            selectedMarker.Position = point;
            gmap.UpdateRouteLocalPosition(route);
        }

   public static void MarkerSelection(ref bool selectedMarkerFlag, ref GMapMarker selectedMarker, 
                                  ref GMapMarker sender , ref bool selectedMarkerDeleted,
                                  ref ScrewTurn.DropDownPanel WayPointPanel,
                                  ref GMapOverlay markersOverlay, ref List<GMapMarker> markersList)
      
      {
          if (selectedMarkerFlag == false && selectedMarker == null)
          {
              selectMarker(ref selectedMarker,  ref  sender, ref WayPointPanel, ref  markersOverlay, ref  markersList);
              selectedMarkerFlag = true;
              selectedMarkerDeleted = false;
          }
          else if (selectedMarkerFlag && sender.Position == selectedMarker.Position)
          {
              deSelectMarker(ref  sender, ref  WayPointPanel, ref  markersOverlay, ref  markersList);
 
              selectedMarker = null;
              selectedMarkerFlag = false;
          }
          else if (selectedMarkerFlag && sender.Position != selectedMarker.Position)
          {
              if (selectedMarkerDeleted != true)
                  deSelectMarker(ref  selectedMarker, ref  WayPointPanel, ref  markersOverlay, ref  markersList);
              selectMarker(ref selectedMarker, ref  sender, ref WayPointPanel, ref  markersOverlay, ref  markersList);
            
              selectedMarkerDeleted = false;
          }
      }
  
    static void selectMarker(ref GMapMarker selectedMarker, ref GMapMarker marker, ref ScrewTurn.DropDownPanel WayPointPanel, 
                               ref GMapOverlay markersOverlay, ref List<GMapMarker> markersList)
   {
        if(WayPointPanel !=null)
       WayPointPanel.Visible = true;
       PointLatLng selectedPos = marker.Position;
       markersList.Remove(marker);
       markersOverlay.Markers.Remove(marker);
       GMarkerGoogle nMarker = new GMarkerGoogle(selectedPos, GMarkerGoogleType.red);
       markersOverlay.Markers.Add(nMarker);
       markersList.Add(nMarker);
       selectedMarker = nMarker;
   }

   static void deSelectMarker(ref GMapMarker marker, ref ScrewTurn.DropDownPanel WayPointPanel,
                              ref GMapOverlay markersOverlay, ref List<GMapMarker> markersList)
   {
       if (WayPointPanel != null)
       WayPointPanel.Visible = false;
       PointLatLng selectedPos = marker.Position;
       markersList.Remove(marker);
       markersOverlay.Markers.Remove(marker);
       GMarkerGoogle nMarker = new GMarkerGoogle(selectedPos, GMarkerGoogleType.blue_small);
       markersOverlay.Markers.Add(nMarker);
       markersList.Add(nMarker);
   }

   public static void deleteMarker(ref GMapMarker selectedMarker, ref List<WayPoint> wayPointList
                                 , ref GMapRoute route, ref List<GMapRoute> routeList
                                 , ref GMapOverlay markersOverlay, ref List<GMapMarker> markersList,
                                  ref GMapControl gmap, ref bool selectedMarkerDeleted
                                  , ref bool selectedMarkerFlag, ref ScrewTurn.DropDownPanel WayPointPanel)
                                       
      {
          if (selectedMarker != null)
          {

              for (int i = 0; i < wayPointList.Count; i++)
              {
                  if (wayPointList[i].marker == null)
                      continue;
                  if (wayPointList[i].marker.Position == selectedMarker.Position)
                  {
                      wayPointList.Remove(wayPointList[i]);
                      break;
                  }
              }
              route.Points.Remove(selectedMarker.Position);
              markersOverlay.Markers.Remove(selectedMarker);
              markersList.Remove(selectedMarker);
              routeList.Remove(route);

          }
          gmap.UpdateRouteLocalPosition(route);
          gmap.Invalidate(false);
          selectedMarkerDeleted = true;
          selectedMarker = null;
          selectedMarkerFlag = false;
       if(WayPointPanel !=null)
          WayPointPanel.Visible = false;

      }

   public static void mapZooming(ref object sender, ref GMapControl gmap)
      {
          gmap.Zoom = ((TrackBar)sender).Value;
      }

      public static void clearMap(ref List<GMapMarker> markersList,ref GMapOverlay markersOverlay,
                              ref GMapRoute route, ref List<GMapRoute> routeList,
                              ref GMapControl gmap, ref GMapMarker selectedMarker, 
                             ref bool selectedMarkerFlag , ref  List<WayPoint> wayPointList)
          
                                    
      {
          for (int i = 0; i < markersList.Count; i++)
          {
             
              bool g = markersOverlay.Markers.Remove(markersList[i]);
              PointLatLng p = new PointLatLng(markersList[i].Position.Lat, markersList[i].Position.Lng);
              routeList[i].Points.Remove(p);
              route.Points.Remove(p);

              gmap.UpdateRouteLocalPosition(route);
          }
          
          markersList.Clear();
          routeList.Clear();
          wayPointList.Clear();
          selectedMarker = null;
          selectedMarkerFlag = false;
          
      }

      public static void clearMap(ref List<GMapMarker> markersList, ref GMapOverlay markersOverlay,
                             ref GMapPolygon route, ref List<GMapPolygon> routeList,
                             ref GMapControl gmap, ref GMapMarker selectedMarker,
                            ref bool selectedMarkerFlag, ref  List<WayPoint> wayPointList)
      {
          for (int i = 0; i < markersList.Count; i++)
          {

              bool g = markersOverlay.Markers.Remove(markersList[i]);
              PointLatLng p = new PointLatLng(markersList[i].Position.Lat, markersList[i].Position.Lng);
              routeList[i].Points.Remove(p);
              route.Points.Remove(p);

              gmap.UpdatePolygonLocalPosition(route);
          }

          markersList.Clear();
          routeList.Clear();
          wayPointList.Clear();
          selectedMarker = null;
          selectedMarkerFlag = false;
      }
      public static void clearMap(ref List<GMapMarker> markersList, ref GMapOverlay markersOverlay,
                              ref GMapRoute route, ref List<GMapRoute> routeList,
                              ref GMapControl gmap, ref GMapMarker selectedMarker,
                             ref bool selectedMarkerFlag, ref  List<WayPoint> wayPointList, ref GMapOverlay routeOverlay,  GMapOverlay pathOverlay=null)
      {
          gmap.Overlays.Clear();
          gmap.Invalidate();
          route.Clear();
              
          for (int i = 0; i < markersList.Count; i++)
          {
              bool g = markersOverlay.Markers.Remove(markersList[i]);
              PointLatLng p = new PointLatLng(markersList[i].Position.Lat, markersList[i].Position.Lng);
              routeList[i].Points.Remove(p);
              route.Points.Remove(p);
              gmap.UpdateRouteLocalPosition(route);
          }
          if (pathOverlay != null) {
              pathOverlay.Routes.Clear();
          }
          gmap.Overlays.Add(markersOverlay);
          gmap.Overlays.Add(routeOverlay);
          gmap.Overlays.Add(pathOverlay);
          markersList.Clear();
          routeList.Clear();
          wayPointList.Clear();
          selectedMarker = null;
          selectedMarkerFlag = false;
      }

      public static void clearMap(ref List<GMapMarker> markersList, ref GMapOverlay markersOverlay,
                            ref GMapPolygon route, ref List<GMapPolygon> routeList,
                            ref GMapControl gmap, ref GMapMarker selectedMarker,
                           ref bool selectedMarkerFlag, ref  List<WayPoint> wayPointList, ref GMapOverlay routeOverlay, GMapOverlay pathOverlay = null)
      {
          for (int i = 0; i < markersList.Count; i++)
          {
              gmap.Overlays.Clear();
              route.Clear();
              bool g = markersOverlay.Markers.Remove(markersList[i]);
              PointLatLng p = new PointLatLng(markersList[i].Position.Lat, markersList[i].Position.Lng);
              routeList[i].Points.Remove(p);
              route.Points.Remove(p);

              gmap.UpdatePolygonLocalPosition(route);
          }
          gmap.Overlays.Add(markersOverlay);
          gmap.Overlays.Add(routeOverlay);
          gmap.Overlays.Add(pathOverlay);
          markersList.Clear();
          routeList.Clear();
          wayPointList.Clear();
          selectedMarker = null;
          selectedMarkerFlag = false;
      }


      public static void clearMap(ref GMapControl gmap)
      {
          foreach (GMapOverlay ovl in gmap.Overlays)
          {
              ovl.Routes.Clear();
              ovl.Markers.Clear();
              ovl.Polygons.Clear();

          }
     }

    }
}
