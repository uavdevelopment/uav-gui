﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightMonitor
{
    public class FatalError {
        int code;

        public int Code
        {
          get { return code; }
          set { code = value; }
        } 

        String description;

        public String Description
        {
          get { return description; }
          set { description = value; }
        }

        public FatalError(int code_ , String description_) {
            code = code_;
            description = description_;
        }

    }
   public static class fatalErrorsLookUPTable
    {
        public static List<FatalError> fatalErrors= new List<FatalError>();
        
        public static void initialize(){
            fatalErrors = new List<FatalError>();
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Presseure airspeed transeducer isn't connected"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "VRS is out of date"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "VRS has been initialized to default values"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "depreciated"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "depreciated"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "depreciated"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "The pressure Altimeter incorrect"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "pitch Gyro Out of Range"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "The Pitch Gyro Zero is Incorrect"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "depreciated"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "The Roll Gyro Zero is Incorrect"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "depreciated"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "The YAw Zero is incorrect"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "AGL out of Range (could be on a bench)"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "inconsitent sensors "));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "fly file invalid command"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "SIN table is corrupted"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "calibration data in flash is corrupt"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "VRS Corrupted"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "autopilot code corrupted in flash"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "gyros are broken "));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "accelerometer failure"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "invalid or missing runway"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Aircraft has flown past the 300km limit"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Waypoint or UAV out of geographical fence range"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "compass is enabled but is not installed"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "CH5 pulse width in PIC mode is outside range of 1.65 to 1.85 msec"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "light duration has exceeded allowed time limit"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "fuel level is low"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Target Licensing System Failed. (Licence Invalid)"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Target drone waiting for a licence"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Target drone licence key has unexpectedly changed"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Target drone flying without a licence"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Autopilot stack size limit was exceeded"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Gyro Temperatures Do Not Change"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "CPU Access Error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "CPU Address Error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "CPU Illegal Instruction"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "CPU Divide by Zero"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "CPU Privilege Violation"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "CPU ISR 6 Error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "CPU ISR 7 Error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "CPU Spurious Interrupt Error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "CPU Unhandled Interrupt Error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "waypoint out of range"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Aircraft is close to the 300km limit"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Sensor error on heli takeoff"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Code not valid"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Code verification failed"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Servo mixing configuration error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "GPS is configured incorrectly or not plugged in"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "ADC Gain is invalid"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Heli code error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Faster loop disabled in code"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Flash chip error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Datalog in flash chip could not be erased"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Z accelerometer adjustment error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "No GPS PPS signal"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Watchdog failure"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Airspeed sensor voltage error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Motor failure"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Logic version error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Circuit board version error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Watchdog version error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Watchdog CRC mismatch"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Hardware mismatch"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Calibration field length mismatch"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "RAM test failed"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Compass set/reset mafunction"));
           // fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "battery voltage is low"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Accelerometer temperature failure"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Multiple Fatal Errors"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Missing Raw Sensor Sample"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Autopilot loaded with non-release code"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Error configuring LRC microcontroller"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "LRC microcontroller does not support binding"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Undefined Communication protocol Selected. Autopilot does not have the protocol selected"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "TPU lines configuration error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Binding key calculation error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Difference between accl temperatures is too large"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Commands assigned to thread ID which is unsupported by this autopilot"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Current Yaw and Compass Heading have diverged"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "The takeoff command completed but the autopilot was not armed"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Maximum flight range from origin must be positive"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Maximum flight altitude must be above initialisation point"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Aircraft is above the maximum altitude limit"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Maximum target altitude must be below maximum flight altitude"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Waypoint is above maximum flight altitude"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Waypoint is outside of geofence"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "pitch gyro broken"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "roll gyro broken"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "yaw gyro broken"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "x accelerometer failure"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "y accelerometer failure"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "z accelerometer failure"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Protocol device initialization failed"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Compass is enabled but not calibrated!"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "More than half of lookup tables failed!"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Simulated fatal error"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Multirotor PIC not Supported"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "400HZ_PIC_not Supported"));
            fatalErrors.Add(new FatalError((fatalErrors.Count + 2), "Missing fatal error code xxx"));

        }
        
    }
}
