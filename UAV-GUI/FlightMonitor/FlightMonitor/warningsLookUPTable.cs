﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightMonitor
{
    public  class Warning
    {
         int code;

        public   int Code
        {
          get { return code; }
          set { code = value; }
        } 

         String description;

        public  String Description
        {
          get { return description; }
          set { description = value; }
        }

        public Warning(int code_ , String description_) {
            code = code_;
            description = description_;
        }
    }

  public static  class warningsLookUPTable 
    {
        public static List<Warning> warnings = new List<Warning>();
        public static void initialize()
        {
            warnings = new List<Warning>();
            warnings.Add(new Warning((warnings.Count + 1), "Datalog not downloaded. The autopilot will not initialise"));
            warnings.Add(new Warning((warnings.Count + 1), "Multiple Warnings Reported"));
            warnings.Add(new Warning((warnings.Count + 1), "Desired heading is now unlocked"));
            warnings.Add(new Warning((warnings.Count + 1), "Waiting for Circuit to abort before starting pattern"));
            warnings.Add(new Warning((warnings.Count + 1), "Waiting for Circuit to abort before exiting pattern"));
            warnings.Add(new Warning((warnings.Count + 1), "Lost GPS during takeoff- runway direction could not be determined"));
            warnings.Add(new Warning((warnings.Count + 1), "Autopilot loaded with non release code"));
            warnings.Add(new Warning((warnings.Count + 1), "Flying backtrack path"));
            warnings.Add(new Warning((warnings.Count + 1), "Backtrack waypoint buffer is 80% full"));
            warnings.Add(new Warning((warnings.Count + 1), "Backtrack waypoint buffer is full - new waypoints will not be added"));
            warnings.Add(new Warning((warnings.Count + 1), "Completed backtrack command cannot be resumed"));
            warnings.Add(new Warning((warnings.Count + 1), "Lost GPS Compass Fix"));
            warnings.Add(new Warning((warnings.Count + 1), "Lost Hemisphere GPS Fix"));
            warnings.Add(new Warning((warnings.Count + 1), "Waiting for initial GPS Compass Heading"));
            warnings.Add(new Warning((warnings.Count + 1), "GPS Speed Accuracy is low"));
            warnings.Add(new Warning((warnings.Count + 1), "Waiting for Heli to exit takeoff command"));
            warnings.Add(new Warning((warnings.Count + 1), "Approaching maximum flight altitude"));
            warnings.Add(new Warning((warnings.Count + 1), "Compass set/reset error was recovered from"));
            warnings.Add(new Warning((warnings.Count + 1), "Data log is full. Flight data no longer being recorded."));
            warnings.Add(new Warning((warnings.Count + 1), "Missing warning code"));
        }
           
    }
}
