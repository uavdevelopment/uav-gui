﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoPilotMemory
{
   public static class PIDFieldsID
    {
        public static int pidNextRecord_1 = 3000;
        public static int pidGnSchaRecord_1 = 3001;
        public static int   pidGnSchbRecord_1 = 3002;
        public static int   pidGnSchcRecord_1 = 3003;
        public static int   pidGnSchdRecord_1 = 3004;
        public static int    pidMultIaRecord_1 = 3005;
        public static int    pidMultPbRecord_1 = 3009;
        public static int    pidMultPcRecord_1 = 3013;
        public static int    pidMultPdRecord_1 = 3017;
        public static int    pidMaxPRecord_1 = 3041;
        public static int    pidMinPRecord_1 = 3045;
        public static int    pidMultIaInteqralRecord_1 = 3006;
        public static int   pidMultIbInteqralRecord_1 = 3010;
        public static int   pidMultIcInteqralRecord_1 = 3014;
        public static int   pidMultIdInteqralRecord_1 = 3018;
        public static int   pidMaxIInteqralRecord_1 = 3042;
        public static int   pidMinIInteqralRecord_1 = 3046;
        public static int pidWindupMaxInteqralRecord_1 = 3049;
        public static int   pidWindupMinInteqralRecord_1 = 3050;
        public static int   pidMultDaRecord_1 = 3007;
        public static int   pidMultDbRecord_1 = 3011;
        public static int   pidMultDcRecord_1 = 3015;
        public static int      pidMultDdRecord_1 = 3019;
        public static int     pidMaxDRecord_1 = 3043;
        public static int    pidMinDRecord_1 = 3047;
        public static int    pidMultDDaRecord_1 = 3008;
        public static int    pidMultDDbRecord_1 = 3012;
        public static int    pidMultDDcRecord_1 = 3016;
        public static int    pidMultDDdRecord_1 = 3020;
        public static int    pidMaxDDRecord_1 = 3044;
        public static int    pidMinDDRecord_1 = 3048;
    }
}
