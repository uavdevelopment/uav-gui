﻿#define Debug
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterClasses;
using System.IO;
using System.Windows.Forms;
using FlightMonitor;


namespace GLobal
{
    public static class GlobalData
    {
       public static short modeSimulatorOrAuto = 0;  // 0 for Autopilot and 1 for Simulator.
       public static Logger myLogger = new Logger();
       private static System.Threading.ReaderWriterLockSlim lock_ = new System.Threading.ReaderWriterLockSlim(); 
       public static bool autoPilotReady = false;
       public static Boolean JoyStickMode = false;
       public static int time = 0;
       public static FlightMode currentMode;
       public static bool apConnected = false;
       public static bool AutoPilotInitialized = false;

       public static double orValERad = 0;
       public static double orValNRad = 0; 
       public static double orValEDeg = 0; 
       public static double orValNDeg = 0;

       public static RadioButton ActivePatternCheckBox = null;
       public static MenuItem ActivePatternMenuItem = null; 

       
  public static void LogConsole(Object consoleIn_){
                   
           String consoleIn = consoleIn_.ToString();
#if Debug
           Console.WriteLine(consoleIn);
#endif
#if !Debug
                   
#endif

       }
    }
}
