﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
namespace FileClasses.VRS_Files
{
   public class VrsFile
    {
       


      public  List<VrsLine> VrsLines;
        string fileName;
        public VrsFile(string fileName)
        {
            this.fileName = fileName;
            VrsLines = new List<VrsLine>();
            ParssVrsFile();
        }

        public void ParssVrsFile()
        {
            string text = File.ReadAllText(fileName);
            string [] LineSplitter = text.Split('\n');
            string [] line ;
            for( int i=0; i<LineSplitter.Length ; i++)
            {

                if (LineSplitter[i] != "" && LineSplitter[i] != "\r")
                {
 
                    line = LineSplitter[i].Split(' ');
                    VrsLine vrsLine = new VrsLine(int.Parse(line[0]), (int) double.Parse(line[1]), line[2], getFieldDescription(line));
                    VrsLines.Add(vrsLine);
                    
                }
            }
       
        }

        public void ParssDefaultsVrsFile()
        {
            string text = File.ReadAllText(@"vrs\defaultsVrsFile.vrs");
            string[] LineSplitter = text.Split('\n');
            string[] line;
            for (int i = 0; i < LineSplitter.Length; i++)
            {

                if (LineSplitter[i] != "" && LineSplitter[i] != "\r")
                {
                    line = LineSplitter[i].Split(' ');
                    VrsLine vrsLine = new VrsLine(int.Parse(line[0]), int.Parse(line[1]), line[2], getFieldDescription(line));
                    VrsLines.Add(vrsLine);

                }
            }

        }

       public void ReWriteVrsFile()
       {
           System.IO.StreamWriter objWritter = new System.IO.StreamWriter(fileName, false);
           String text = "";
           /*Data Related to parallization*/
           String par_line = "";
           int cnt_threads = 20;
           int remainder;
           int partitionSize;
           String[] par_text = null;
           remainder= VrsLines.Count %cnt_threads;
           partitionSize = (VrsLines.Count) / cnt_threads;
           par_text = new String[cnt_threads+1];
           // *Tariq Shatat changed To parallel Computing 
           Parallel.For(0, cnt_threads, index =>
           {
               for (int j = 0; j < partitionSize; j++) {
                   String line = "";
                   int i = j + (index * partitionSize);
                   line = VrsLines[i].Field_ID + " " + VrsLines[i].Parameter_value + " "
                                          + VrsLines[i].Field_name + " " + VrsLines[i].Field_description;
                   par_text[index] += (line.Trim() + "\r\n");
               }
           });
           int x;
           for ( x = 0; x < cnt_threads; x++)
           {
               text += par_text[x];
           }
           for (x = 0; x <  remainder; x++)
           {
               String line = "";
               int i = x + (cnt_threads * partitionSize);
               line = VrsLines[i].Field_ID + " " + VrsLines[i].Parameter_value + " "
                                      + VrsLines[i].Field_name + " " + VrsLines[i].Field_description;
               text += (line.Trim() + "\r\n");
           }
              
               objWritter.Write(text);
           objWritter.Close();
       }

       public void ReWriteVrsFile2()
       {
           System.IO.StreamWriter objWritter = new System.IO.StreamWriter(@"vrs\vrsFile2.vrs", false);
           String text = "";
           for (int i = 0; i < VrsLines.Count; i++)
           {
               text += (VrsLines[i].Field_ID + " " + VrsLines[i].Parameter_value + " "
                   + VrsLines[i].Field_name + " " + VrsLines[i].Field_description + "\r\n");
           }

           //text = System.Text.RegularExpressions.Regex.Replace(text, @"^\s+$[\r*\n]*", "", System.Text.RegularExpressions.RegexOptions.Multiline);
           objWritter.Write(text);
           objWritter.Close();
       }
        private string getFieldDescription(string[] line)
        {
           string FieldDesc ="";
           
            for (int i = 3; i < line.Length; i++)
               FieldDesc += line[i] +" ";

            FieldDesc = System.Text.RegularExpressions.Regex.Replace(FieldDesc, @"[\r]*", "", System.Text.RegularExpressions.RegexOptions.Multiline);
            FieldDesc.Trim();
           return FieldDesc;
        }
    }
}
