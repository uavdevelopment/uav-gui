﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FileClasses.VRS_Files
{
   public class VrsFileHandler
    {
        string fileName;   //@"Heli_sim.vrs";
        public VrsFile vrsFile;

        public VrsFileHandler(string fileName_)
        {
            fileName = fileName_;
            vrsFile = new VrsFile(fileName);
            
        }

       public void restoreDefaults()
       {
           System.IO.File.WriteAllText(fileName, System.IO.File.ReadAllText(@"vrs\defaultsVrsFile.vrs"));
           vrsFile.ParssVrsFile();
       }

        public void fileUIMapping()
        {           
            vrsFile.ReWriteVrsFile();
        }
        public void fileUIMapping2()
        {
            vrsFile.ReWriteVrsFile2();
        }

        public double searchByFieldID_getParameter_value(int FieldID)
        {

            for (int i = 0; i < vrsFile.VrsLines.Count; i++)
            {
                if (vrsFile.VrsLines[i].Field_ID == FieldID)
                    return vrsFile.VrsLines[i].Parameter_value;
            }

            return -1;
        }

        public int searchByFieldID_getListIndex(int FieldID)
        {

            for (int i = 0; i < vrsFile.VrsLines.Count; i++)
            {
                if (vrsFile.VrsLines[i].Field_ID == FieldID)
                    return i;
            }

            return -1;
        }

    }
}
