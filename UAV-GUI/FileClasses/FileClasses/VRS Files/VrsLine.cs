﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FileClasses.VRS_Files
{
   public class VrsLine
    {
        public int Field_ID;
        public int Parameter_value;
        public string Field_name;
        public string Field_description;

        public VrsLine(int Field_ID, int Parameter_value, string Field_name, string Field_description)
        {
            this.Field_ID = Field_ID;
            this.Parameter_value = Parameter_value;
            this.Field_name = Field_name;
            this.Field_description = Field_description;
        }

        // get & set 
    }
}
