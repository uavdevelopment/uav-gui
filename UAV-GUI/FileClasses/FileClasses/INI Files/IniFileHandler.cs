﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileClasses.Ini;
using System.Collections;
using System.Windows.Forms;
using System.Drawing;
namespace FileClasses.INI_Files
{

    /// <summary>
    /// Class to hold data of the INI file. 
    /// Modifcation done by Tariq S. Shatat. 
    /// Not All Features Included. 
    /// </summary>
    public static class  INIData
    {

        /// <summary>
        /// Miscl 
        /// </summary>
        /// 
        public static double spare_latitude;
        public static double spare_longitude;

       #region SIM
        public static  int Heading;
        public static  double latitude;
        public static  double longitude;
        public static  int altitudeAGL;
        public static  int speed;
        public static  int throttle;
       #endregion 

        #region Communication
        public static  string compPort;
        public static  long baudRate ;
        #endregion 

        #region Visulization
        public static  bool HeadingEnabled;
        public static  bool AltEnabled;
        public static  bool ASpeedEnabled;
        public static Color hColor;
        public static  Color aSColor;
        public static  Color aLColor;
        public static int selectedMap; 
        
        #endregion 
        

    };
   public static class IniFileHandler
    {
        public static int filedsNum = 47;
        public static List<string> iniFieldsVaues = new List<string>();
        public static List<string> iniFieldsKey= new List<string>();

        public static void setIniKeys()


        {
            for(int i=0;i<filedsNum;i++)
            iniFieldsKey.Add("");

        iniFieldsKey[0]= "Heading";
        iniFieldsKey[1] = "Latitude";
        iniFieldsKey[2] = "Longitude";
        iniFieldsKey[3] =  "Speed";
        iniFieldsKey[4] =  "Altitude";
        iniFieldsKey[5] = "Throttle";
        iniFieldsKey[6] = "Simulater";
        iniFieldsKey[7] = "Duration";
        iniFieldsKey[8] = "Replacment Sim";
        iniFieldsKey[9] = "Replacment Sim host";
        iniFieldsKey[10] = "Replacment Sim port";
        iniFieldsKey[11] =  "Use COM link to UAV";
        iniFieldsKey[12] ="COM# Port";
        iniFieldsKey[13] ="COM Port Bit Rate";
        iniFieldsKey[14] ="Use TCP/IP link to UAV";
        iniFieldsKey[15] ="Host Name or IP";
        iniFieldsKey[16] ="Host Port #";
        iniFieldsKey[17] ="Enable TCP/IP Server";
        iniFieldsKey[18] ="Server Port #";
        iniFieldsKey[19] = "Units";
        iniFieldsKey[20] = "Use Custom Shape for UAV";
        iniFieldsKey[21] ="Enable Flight Files Text Editor";
        iniFieldsKey[22] ="Enable VRS Field Editor";
        iniFieldsKey[23] ="Disable Takeoff Button";
        iniFieldsKey[24] ="Enable Flight Reply Features (beta)";
        iniFieldsKey[25] ="Installed Horizon Version";
        iniFieldsKey[26] ="Installed Simulator Version";
        iniFieldsKey[27] ="AutoPilot Fimware Version";
        iniFieldsKey[28] ="Enable 2x28g";
        iniFieldsKey[29] ="Enable 1028g";
        iniFieldsKey[30] = "Expected Check1";
        iniFieldsKey[31] ="Expected Check2";
        iniFieldsKey[32] ="Expected text1";
        iniFieldsKey[33] = "Expected text2";
        iniFieldsKey[34] = "AltitudeAllowed";
        iniFieldsKey[35] = "AltitudeR";
        iniFieldsKey[36] = "AltitudeG";
        iniFieldsKey[37] = "AltitudeB";
        iniFieldsKey[38] = "HeadingAllowed";
        iniFieldsKey[39] = "HeadingR";
        iniFieldsKey[40] = "HeadingG";
        iniFieldsKey[41] = "HeadingB";
        iniFieldsKey[42] = "AirSpeedAllowed";
        iniFieldsKey[43] = "AirSpeedR";
        iniFieldsKey[44] = "AirSpeedG";
        iniFieldsKey[45] = "AirSpeedB";
        iniFieldsKey[46] = "MapIndex";
        }


        public static string readSpecificField(string key ) {

            IniFile ini = new IniFile(@"status\iniFields.ini");
            string s = ini.IniReadValue("Info", key);
            return s; 
        
        }


        public static string readSpecificFieldsim(string Section, string Key)
        {
            System.IO.File.Move(@"uav1.ini", @"status\uav1.ini");
            IniFile ini = new IniFile(@"status\uav1.ini");
            string x = ini.IniReadValue(Section, Key);
            System.IO.File.Move(@"status\uav1.ini", @"uav1.ini");
            
           
            //writeSpecificFielduav(Section, Key, x);
            return x;
          
        }

        public static string readSpecificFielduav(string Section, string Key)
        {
            System.IO.File.Move(@"Simulate.ini", @"status\Simulate.ini");
            IniFile ini = new IniFile(@"status\Simulate.ini");
            string x = ini.IniReadValue(Section, Key);
            System.IO.File.Move(@"status\Simulate.ini", @"Simulate.ini");

            
            return x;
        }

        public static void writeSpecificFieldsim(string Section, string Key, string Value)
        {
            System.IO.File.Move(@"Simulate.ini", @"status\Simulate.ini");
            IniFile ini = new IniFile(@"status\Simulate.ini");
           
            ini.IniWriteValue(Section, Key, Value);
            System.IO.File.Move(@"status\Simulate.ini", @"Simulate.ini");
            writeSpecificFielduav(Section, Key, Value);

        }

        public static void writeSpecificFielduav(string Section, string Key, string Value)
        {

            System.IO.File.Move(@"uav1.ini", @"status\uav1.ini");
            IniFile ini = new IniFile(@"status\uav1.ini");

            ini.IniWriteValue(Section, Key, Value);
            System.IO.File.Move(@"status\uav1.ini", @"uav1.ini");
        }


        public static void read()
        {
            iniFieldsVaues.Clear();
            setIniKeys();

            IniFile ini = new IniFile(@"status\iniFields.ini");
            iniFieldsVaues = new List<string>();
            
            for (int i = 0; i < iniFieldsKey.Count; i++)
            {          
               // iniFieldsVaues.Add(int.Parse(
                    string s = ini.IniReadValue("Info", iniFieldsKey[i]);
                    iniFieldsVaues.Add(s);
            }
            fillInHoldedData(); 

        }

        public static void readDefaults()
        {
            iniFieldsVaues.Clear();
            setIniKeys();

            IniFile ini = new IniFile(@"status\defaultsIniFields.ini");
            iniFieldsVaues = new List<string>();

            for (int i = 0; i < iniFieldsKey.Count; i++)
            {
                // iniFieldsVaues.Add(int.Parse(
                string s = ini.IniReadValue("Info", iniFieldsKey[i]);
                iniFieldsVaues.Add(s);
            }
            fillInHoldedData();

        }

        /// <summary>
        /// Apply holded Data. 
        /// Done :Tariq S. Shatat.
        /// </summary>
        private static void fillInHoldedData() {

            /*
             *Tariq S. Shatat
             *SIM 
             */
            //SIM

            try
            {
                INIData.Heading = int.Parse(readSpecificFieldsim("UAV","initial hdg")); // (int.Parse(IniFileHandler.iniFieldsVaues[0].ToString()));
                INIData.latitude = double.Parse(readSpecificFieldsim("UAV", "gps pos N")); //(double.Parse(IniFileHandler.iniFieldsVaues[1].ToString()));
                INIData.longitude = double.Parse(readSpecificFieldsim("UAV", "gps pos E")); //(double.Parse(IniFileHandler.iniFieldsVaues[2].ToString()));

                INIData.speed = int.Parse(readSpecificFieldsim("UAV", "initial speed")); //(int.Parse(IniFileHandler.iniFieldsVaues[3].ToString()));
                INIData.altitudeAGL = int.Parse(readSpecificFieldsim("UAV", "initial altitude")); //(int.Parse(IniFileHandler.iniFieldsVaues[4].ToString()));
                INIData.throttle = int.Parse(readSpecificFieldsim("UAV", "initial throttle")); //(int.Parse(IniFileHandler.iniFieldsVaues[5].ToString()));
                INIData.selectedMap = int.Parse(IniFileHandler.readSpecificField("MapIndex"));

                //Commuinication


                INIData.compPort = IniFileHandler.iniFieldsVaues[12].ToString();
                INIData.baudRate = int.Parse(IniFileHandler.iniFieldsVaues[13]);

                /*  
                 *Tariq S. Shatat
                 *Visualization
                 */
                INIData.AltEnabled = (int.Parse(IniFileHandler.iniFieldsVaues[34]) == 1) ? true : false;
                INIData.HeadingEnabled = (int.Parse(IniFileHandler.iniFieldsVaues[38]) == 1) ? true : false;
                INIData.ASpeedEnabled = (int.Parse(IniFileHandler.iniFieldsVaues[42]) == 1) ? true : false;

                /*  
                 *Tariq S. Shatat
                 *Visualization
                 */
                INIData.AltEnabled = (int.Parse(IniFileHandler.iniFieldsVaues[34]) == 1) ? true : false;
                INIData.HeadingEnabled = (int.Parse(IniFileHandler.iniFieldsVaues[38]) == 1) ? true : false;
                INIData.ASpeedEnabled = (int.Parse(IniFileHandler.iniFieldsVaues[42]) == 1) ? true : false;

                INIData.aSColor = Color.FromArgb((byte)int.Parse(IniFileHandler.readSpecificField("AirSpeedR")),
                      (byte)int.Parse(IniFileHandler.readSpecificField("AirSpeedG")),
                      (byte)int.Parse(IniFileHandler.readSpecificField("AirSpeedB")));

                INIData.hColor = Color.FromArgb((byte)int.Parse(IniFileHandler.readSpecificField("HeadingR")),
                   (byte)int.Parse(IniFileHandler.readSpecificField("HeadingG")),
                   (byte)int.Parse(IniFileHandler.readSpecificField("HeadingB")));

                INIData.aLColor = Color.FromArgb((byte)int.Parse(IniFileHandler.readSpecificField("AltitudeR")),
                   (byte)int.Parse(IniFileHandler.readSpecificField("AltitudeG")),
                   (byte)int.Parse(IniFileHandler.readSpecificField("AltitudeB")));


               // INIData.latitude = 
               // **INIData.longitude = 
                // INIData.selectedMap = int.Parse(IniFileHandler.readSpecificField("MapIndex"));

            }
            catch { 
            
                //Need to restore the defauls but now we will ignore. 
 
            }
        
        }
        static public void write()
        {
            IniFile ini = new IniFile(@"status\iniFields.ini");
            for (int i = 0; i < iniFieldsVaues.Count; i++)
            {
                ini.IniWriteValue("Info", iniFieldsKey[i], iniFieldsVaues[i].ToString());
            }

            writeSpecificFieldsim("UAV", "initial hdg", IniFileHandler.iniFieldsVaues[0]);//= (headingT.Text);
            writeSpecificFieldsim("UAV", "gps pos N", IniFileHandler.iniFieldsVaues[1]);//= (latT.Text);
            writeSpecificFieldsim("UAV", "gps pos E", IniFileHandler.iniFieldsVaues[2]);//= (LngT.Text);
            writeSpecificFieldsim("UAV", "initial speed", IniFileHandler.iniFieldsVaues[3]);//= (speedT.Text);
            writeSpecificFieldsim("UAV", "initial altitude", IniFileHandler.iniFieldsVaues[4]);//= (AltT.Text);
            writeSpecificFieldsim("UAV", "initial throttle", IniFileHandler.iniFieldsVaues[5]);//= (ThrottleT.Text);
        }
        static public void writeTemp()
        {
            IniFile ini = new IniFile(@"status\iniFields2.ini");
            for (int i = 0; i < iniFieldsVaues.Count; i++)
            {
                ini.IniWriteValue("Info", iniFieldsKey[i], iniFieldsVaues[i].ToString());
            }
        }
       
    }
}
